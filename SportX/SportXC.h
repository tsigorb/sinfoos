// SportXC.h ��������� ��� Sport
// version 20.09.11
// ��� ������ � ISDN

/* �������������� ������ �� �-������:
  A-LAW		13���
  ---------------
  0x55 <-> 0xFFFF
  0x8F <-> 0x0350
  0xFF <-> 0x006A
  0x00 <-> 0xFD50
  0x0F <-> 0xFCB0
*/

#ifndef _SPORTXC_H_
#define _SPORTXC_H_

#define LenMKA		 	12   // MAX_MKA_BOARD ������������ ����� ������� ��� == 12
#define LenSport0	 	48   // MAX_MKA_CHANNEL ����� ����� ���������� ������� SPORT0 == 48
#define LenBuf0			64   // ������ ������ SPORT0 � ������

#define LenMKC		  	4		// ������������ ����� ������� ���
#define LenSport1	 	64		// ����� ����� �������� ������� SPORT1 (0-� ����� SPORT1 - ���������)
#define LenBuf1		 	LenSport1//������ ������ SPORT1 � ������ = 64
#define LenCommutMKC    ((LenSport1/2)*(LenMKC+2))

#define LenHard         (LenSport1+LenSport0) //����� ����� ���������� ������� SPORT1 � SPORT0 = 112
#define LenVirt         64 // ����� ����� �����������
#define LenChan         (LenHard+LenVirt+LenVirt) //�������� ������+���������� ������+������ ��+�������� �� (64+48+64+64=240)

#define LenNameGroup 	(LenFileName-4-1) // ����� ����� ������
#define MaxGroup	 	128		// ������������ ����� �����

#define LenMiddle		512		// �������� �������� ���������� ������� �������� (512/8000 = 64��)
#define LenTimeMiddle	47		// �������� ������� ��������� ������ ������ �������� �� ������ (47*LenMiddle/8000~=3000��)
#define LenTimeSpeak	94		// �������� ������� ��������� ������ ������ �������� �� ��������� (94*LenMiddle/8000~=6000��)
#define LenSumChan      ((LenChan*3)/2+(LenChan/2)) // ����� ������� ����������� ������� (112+64+64)*3/2+(112/2)-1 = 415
#define LenParamUKC	  	7		// ����� ������� ���������� �� (0...6)
#define LenSelector		8		// ������������ ����� ������� (����� �����������) ���������
#define LenHeadSound	12+4	// ����� ��������� ������ �������� ����

#define LenPlay			8		// ����� ������� ��� ���������������

#define LenGener		64		// ������������ ����� ���������� ����������� �� �������

#define LenProtocol		100		// ����� ������ ���������

#define	LenInOutData	512		// ����� �������� MassInData �  MassOutData

#define MaxAbonentSelector  90 // ������������ ���������� ����������
#define MasAbntSelSz    ((MaxAbonentSelector+1)*2)

#define TimeCorrClock	2*60*(60*100)	// 2 ��� * 60 ��� * 60 ��� * 100 ���/��� - ������������� ������ 2 ����
//#define TimeCorrClock	1*(60*100)/6	// ������������� ����� ������ 10 ���

#define LenAbon			255		// ����� ���������� ������ ���������
#define LenSNMP			255		// ����� ���������� ������ ��������� SNMP
#define WaitAbon		30000	// ����� �������� � ��/100 ������� ���������� ������ ��������� �� ��������� � (5���)
#define WaitSNMP		30000	// ����� �������� � ��/100 ������� ���������� ������ ��������� SNMP (5���)

#define	LenDataSave		20
#define	LenTlfNumber	MAX_DIGIT_TLFN    // ������������ ����� ������ �������� (� #0 � �����)
#define	LenTlfName		d_NameLen         // ������������ ����� �����  �������� (� #0 � �����)

#define	LevelAnal		1000	// ������� ������� ������� �������
#define	LevelAnalPPSR 	1000	// ������� ������� ������� ������� ����
#define	LevelAnalDTMF 	4000	// ������� ������� ������� ������� ����
#define Max_mid			25000	// ������������ �������� �������� ������� �� ������

#define _9dB2    	32767	//  +9�� - ��������� ��� ��������� ����������� �������� �������� ��������� ��� 2-� ���������� ������
#define _0dB2		11626	//   0�� 	(0x2D6A)
#define _6dB2		23197	//   6�� 	(0x5A9C)
#define	min3dB2	 	8231	//  -3��	(0x2027)
#define	min6dB2	 	5827	//  -6��	(0x16C3)
#define	min9dB2	 	4125	//  -9��	(0x101D)
#define	min12dB2 	2920	// -12��	(0x0B68)
#define	min13dB2 	2603	// -13��	(0x0A2A)
#define	min15dB2 	2067	// -15��	(0x07EB)
#define	min18dB2 	1464	// -18��	(0x05B8)
#define	min21dB2 	1036	// -21��	(0x040C)
#define	min24dB2 	 734	// -24��	(0x02DE)

#define min29dB		  79	// ��������������� ����������� �������� ~ -29 dB (������� �������� ��� 1 �����)

#define _3dB1    	32767	//  +3�� - ��������� ��� ��������� ����������� �������� �������� ��������� ��� ����� �������
#define _0dB1		23252	//   0�� 	(0x5AD4)
#define	min3dB1	 	16462	//  -3��	(0x404E)
#define	min6dB1	 	11654	//  -6��	(0x2D86)
#define	min8dB1	 	 9257	//  -8��	(0x2429)
#define	min9dB1	 	 8250	//  -9��	(0x203A)
#define	min12dB1 	 5840	// -12��	(0x16D0)
#define	min13dB1 	 5205	// -13��	(0x1455)
#define	min15dB1 	 4054	// -15��	(0x0FD6)
#define	min18dB1 	 2928	// -18��	(0x0B70)
#define	min21dB1 	 2072	// -21��	(0x0818)
#define	min24dB1 	 1468	// -24��	(0x05BC)

#define CoefReturnCh  655	// ����������� ������������ ������ �������� � ��������� � �������

#define Count_PPSR	  800	// ����� ����� ������� ��� ���� (100�� - 10��)
#define Count_DTMF	  200	// ����� ����� ������� ��� DTMF (25��  - 40��)

#define MaxShort   	0x7FFF	// ������������ �������� 2-� �������� ����������

#define Mult  		16384	// ���������
#define MinAnaliz	1000	// ����������� ������� ������� �� ����� ��� ������������� �������

#define NoiseIn			200		// ����������� ������� ������� �� ����� ��� ������� ������� ���������
#define SpeakIn			29		// ������������ ������� ��� ������� ������� ���������  128 * abs(In-OldIn) / abs(In+OldIn) = 4��
#define GenerIn			4		// ������������ ������� ��� ������� ������� ���������� 128 * abs(In-OldIn) / abs(In+OldIn) = 0.5��

#define LenInE1		32      // ����� ������� InE1

//  ------------ ��������� ��� ISDN -------------------
//#define COL_CHAN	2		// ����� ������� E1
#define COL_BUF		2		// ���������� ������� ������
#define LEN_BUF		128		// ����� ������� ������
#define Chan16MKC	32		// ����� 16-�� �� 1-�� ������ ��� 
#define Chan16MKC1	01		// ����� 16-�� �� 1-�� ������ ��� 
#define Chan16MKC2	32		// ����� 16-�� �� 2-�� ������ ��� 

//  ----------- ��������� ������� ��������� -------------
#define AmplGenPCDT	min9dB1	// ��������� ��������� ������ PCDT
#define AmplGenTDC	min9dB1	// ��������� ��������� ������ ���
#define AmplCommut	min9dB1	// ��������� ��������� ������ �����������
#define AmplRadio	min9dB1	// ��������� ��������� ������ ������������
#define AmplDTMF1	min8dB1	// ��������� ��������� ����� ����
#define AmplDTMF2	min6dB1	
#define AmplPPSR	min9dB1	// ��������� ��������� �������� ��� ����

//	--------- ���� ��������� ������� ----------------
#define NotaRe		587		// ������� ���� �� 2-� ������
#define NotaCol		784		// ������� ���� ���� 2-� ������
#define	CallPCDT	1		// ����� ��������� ����	(������ ��������� 1...132)
							// ������ ��������� ��������� �������: 1, 12, 13, 25, 37, 49, 61, 73, 85, 97, 109, 121 
#define	CallTDC		2		// ����� ��������� ���	(������ ��������� 1...45)
#define	TestTDC		3		// �������� ����� ��������� ���	(������ ��������� 1...45)
#define	GenTDC		4		// ����������� ����� ��������� ��� ������� �����������
#define	CallInduct	5		// ����� �������� �������� ���������
#define	CallTLF		6		// ����� ������������ (�����������)
#define	Call_PCDT7	7		// ����������� ����� ��������� ����	� ���� 2/7
#define	Call_PCDT8	8		// ����������� ����� ��������� ����	� ���� 2/8
#define	Call_PCDT11	9		// ����������� ����� ��������� ����	� ���� 2/11
#define	Call_PCDT12	10		// ����������� ����� ��������� ����	� ���� 2/12
#define	CallDTMF	11		// ����� ����� ����
#define	GenImitTDC	12		// ����� ��������� TDC (������ ������� / ��������)
#define	InclPPSR	13		// ������� ��������� ���� ������������
#define ExclPPSR	14		// ������� ���������� ���� ������������ ����
#define ExclRD3		14		// ������� ���������� ���� ������������ PD3 (�����)
#define IndivPPSR	15		// ������� ��������������� ��������� ������������ ����
#define InclATCPPSR	16		// ������� ��������������� ����������� ��� ����
#define ExTransPPSR	17		// ������� ���������� ������������ ���� ��� ���������� ���
#define ChanPPSR	18		// ������� ���������� ��������� ������ ������������ ����
#define BusyATCPPSR	19		// ������ ������ �� ��� ����
#define ImitATCPPSR	20		// �������� ��������� ��� ������ ����
#define CallBusy	21		// ������ ������	(3 ����)
#define Halali		22		// ������ ��-��-�� 	(2 ����)
#define SelOtboy	23		// ����� �������� ��������� RDZ
#define SelSluchat	24		// ������� ��������� RDZ � ������ �������� 
#define SelMeneger	25		// ������� ��������� RDZ � ������ ���������� (�������)
#define SelUchast	26		// ������� ��������� RDZ � ������ �������� 
#define CallKPV		27		// ������ ������� ������ ���
#define CallZummer	28		// �������� ������ �� ���

// ------ ������ ---------------
#define	CodP22C_D1	50		// ��������� 1 - ��� ������������ �22�-46
#define	CodP22C_D2	51		// ��������� 2 - ��� ������������ �22�-46
#define	CodPC46_D1	52		// ��������� 1 - ��� ������������ ��-46
#define	CodPC46_D2	53		// ��������� 2 - ��� ������������ ��-46
#define	CodPC6_D1	54		// ��������� 1 - ��� ������������ ��-6
#define	CodPC6_D2	55		// ��������� 2 - ��� ������������ ��-6
#define	Cod43PTC_D1	56		// ��������� 1 - ��� ������������ 43���-�2-��
#define	RadTransPC	57		// ������� �������� ������������ �22�-46, ��-46, ��-6
#define	RadTrans43	58		// ������� �������� ������������ 43���-�2-��
#define	RadRecivPC	59		// ������� ������ ������������ �22�-46, ��-46, ��-6
#define	RadReciv43	60		// ������� ������ ������������ 43���-�2-��
#define	RadBreakPC	61		// ������� ����� ������������ �22�-46, ��-46, ��-6
#define	RadBreak43	62		// ������� ����� ������������ 43���-�2-��
#define	RadContrPC	63		// ������� �������� ������������ �22�-46, ��-46, ��-6
#define	RadContr43	64		// ������� �������� ������������ 43���-�2-��
#define	RadBusyPC	65		// ������� ������ ������������ �22�-46, ��-46, ��-6
#define	RadBusy43	66		// ������� ������ ������������ 43���-�2-��

#define CallSpec1	100		// 1-� ����������� ����� ��������� 
//...............................//
#define CallSpec8	107		// 8-� ����������� ����� ��������� 

#define	GenTest	    255		// �������� ������ ��������� [chan(200��) + ((chan+1) * 3)(400��) + ((chan+2) * 3)(100��) + (chan+3)(300��)]

//	--------- ���� �������� ������� ----------------
#define	AnsATC		1		// ����� �������
#define	AnsTDC		2		// ���
#define	AnsDTMF		3		// ����
#define	AnsSignal	4		// ������� �������
#define	ZanATC		5		// ������ ���
#define	InductATC	6		// ��������
#define	Select		7		// ��������
#define	ContrSel	8		// �������� ���������
#define	ImitTDC		9		// ������� (��������) ���
#define	ImitTelef	10		// ���������� (������������ - 1600 ��)
#define ImitCB      11		// �� - ������ �������
#define	ImitPCDT	12		// ������� (��������) ����
#define	AnsParallel 13		// ������������ ������ 2-� ������
#define	AnsSeries	14		// ���������������� ������ 2-� ������
#define	AnsPPSR		15		// ����
#define SpecAnal	16		// 16...23 ����������� ������ ������
#define	MCC			24		// ����� ���
#define	EndSound	25		// ��������� ��������� �������
#define AnsAdace	26		// ����� ���������� �������� �����
#define EndNabAdace	27		// ��������� ������ ����� ������ �����
#define CallISDN    31      // �������� ����� ISDN
#define CompPult    34      // ������� ������������� ������
#define CallIPPM    35      // �������� ����� IPPM

#define	Speaker		100		// ��������

// ----------- ������������ ������� ������ ��� ��� ---------------------
// ����������� = trunc(2*16383*cos(2*Pi*F/8000)+0.5)    - ������ cos � �������� (� �� ��������) !!!!

#define F1605		10003
#define F317		31756
#define F430 		30917
#define F588		29334
#define F791		26644
#define F882 		25214
#define F1085 		21572
#define F1221		18820
#define F1469		13272
#define F1989		283  
#define F2713 		-17404

#define F316		31764
#define F585 		29370
#define F795 		26585
#define F1080		21669
#define F1470		13250
#define F2000		0
#define F890 		25083
#define F1215 		18947
#define F1360		15786
#define F1620		9635 
#define F2720 		-17558

#define F362		31451
#define F475 		30512
#define F520		30071
#define F633		28799
#define F701 		27924
#define F746		27301
#define F836 		25953
#define F927		24459
#define F972		23673

#define F1017 		22858
#define F1130		20687
#define F1243		18353
#define F1289		17361
#define F1334 		16368
#define F1379 		15355
#define F1424		14323
#define F1515		12182
#define F1560		11099
#define F1650 		8894
#define F1695 		7774
#define F1741		6619
#define F1786		5481
#define F1831		4336 
#define F1876 		3186
#define F1921 		2032

#define F2261 		-6669
#define F2306 		-7798
#define F2351		-8918
#define F2396		-10026
#define F2441		-11122
#define F2487 		-12228
#define F2532 		-13295
#define F2577 		-14345
#define F2622 		-15377
#define F2667		-16389
#define F2758		-18374
#define F2803		-19321
#define F2848 		-20244
#define F2894 		-21161
#define F2939 		-22032
#define F2984 		-22875

#define F3029		-23690
#define F3074		-24475
#define F3120		-25246
#define F3165 		-25968

// ----------- ������������ ������� ������ ��� ����� ---------------------
#define F1200 		19260
#define F1600 		10126

#define d_MaxSelectorWav        (30*8000)

#endif // _SPORTXC_H_

