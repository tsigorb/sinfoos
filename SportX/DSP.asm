
#include "SportXC.h"

//#define OldMKA     1 // ���������� ��� MKA � SinfoBK.bin ������ 4.X �� ������ 14.1

.extern _StopBit;
.extern _RxBuf0;
.extern _RxBuf1;
.extern _TxBuf0;
.extern _TxBuf1;
.extern _CommandInOut;
.extern _InTestData;
.extern _OutTestData;
.extern _Toggl;
.extern _TablSum;
.extern	_TablBlock;
.extern	_TablBlockVar;
.extern	_TablChan;
.extern _SinCoef;
.extern _SumDACMiddle;
.extern _SumADCMiddle;
.extern _SumADCShift;
.extern _Ampl2;

.extern _InDataChan;
.extern _OutDataChan;

.section/DOUBLEANY program;
.align 2;

//								������� ���������� �������
/********************************************************************************************
*					    Sport1(64)    Sport0(48)     Virt(64)        Virt(64)		�������	*
*					    (������ �1)	   (������)	  (����. ����.)	  (����. ����.)	  ���������	*
*						         		     	  � ������ virt   � ������� � ���	  |		*
*						         		     	    ������ ��      �������� ��	 |----|		*
*						 � ������		� ���			|------------------------|------|	*
*							 |(����)	  |(����)		|				|--------|--|	|	*
*				�	 0		\ /	  63 64  \ / 111 112   \ / 	 175 176   \ / (����)|	|	|	*
*	InDataChan:		|===============|===========|===============|===============|=|	|	|	*
*				�	 0		 |	  3F 40   |	  6F 70 	|	  AF B0 	|	  BF F0 |	|	*
*			   Addr			 |	  7E 80   |	  DE E0 	|	 15E 160 	|	 1DE 1E0|	|	*
*							\ /			 \ /		   \ / 			   \ /			|	|	*
*					|-----------------------------------------------------------|	|	|	*
*					|		  ��������� � ���������� ������������ ������		|	|	|	*
*					|-----------------------------------------------------------|	|	|	*
*							 |			  |				|				|			|	|	*
*							\ /			 \ /		   \ / 			   \ /			|	|	*
*	OutDataChan:	|===============|===========|===============|===============|=|	|	|	*
*							 |(�����)	  |(�����)		|				| (�����)	|	|	*
*							\ /			 \ /		   \ / 			   \ /			|	|	*
*						 �� �����		�� ���			|               |___________|___|	*
*														|___________________________|		*
*						                 													*
*	In  Addr		B00000			B00080		B000E0			B00160		B001DE (B001E0)	*
*	Out	Addr		B001E4			B00264		B002C4			B00344		B003C2 (B003C4)	*
********************************************************************************************/

/*		��������� ����������������� ������� ��� � MK� TestBoard (�� 2 ����):
		00 - ������
		01 - ������ �� ��������
		10 - ������ ��������
		11 - ������ ��������.*/

/*******************************************************************************
void CopyData(void) // ����������� �������/�������� ������ // � ��������� OutDataChan
	������� ����� ������ 1180 ��
 *******************************************************************************/
.global _CopyData;
_CopyData:
// ����������� ������� ������ SPORT1 
	I0.L=_RxBuf1;		// � I0 ����� ������� �������
	I0.H=_RxBuf1;
	I1.L=_InDataChan+2;	// � I1 ����� �������� �������
	I1.H=_InDataChan+2;
	I2.L=_CommandInOut;	// � I2 ����� �������
	I2.H=_CommandInOut;
	R0.L=W[I0++];		// ������ �������
	W[I2++]=R0.L;
	P2=LenSport1-1;		// ������� ����� �������� �������
	R0.L=W[I0++];
	LSETUP(StCopy1,EndCopy1) LC0=P2;
StCopy1:
		R0.L=R0.L<<3;	// ����������� 13 �������� � 16
EndCopy1:	
		mnop || W[I1++]=R0.L || R0.L=W[I0++];

// ����������� ������� ������ SPORT0.0 � SPORT0.1
	I0.L=_RxBuf0;		// � I0 ����� ������� ������� ������ SPORT0.0
	I0.H=_RxBuf0;
	P2=LenSport0 / 2;	// ������� ����� ���������� ������� / 2
	R0.L=W[I0++];
	LSETUP(StCopy2,EndCopy2) LC0=P2;
StCopy2:
#ifdef OldMKA
		R0.L=R0.L<<1;	// ����������� 15 �������� � 16
#endif
csync;
ssync;
EndCopy2:
		mnop || W[I1++]=R0.L || R0.L=W[I0++];

	I0.L=_RxBuf0+LenBuf0;	// � I0 ����� ������� ������� ������ SPORT0.1
	I0.H=_RxBuf0+LenBuf0;
	R0.L=W[I0++];
	LSETUP(StCopy3,EndCopy3) LC0=P2;
StCopy3:
#ifdef OldMKA
		R0.L=R0.L<<1;	// ����������� 15 �������� � 16
#endif
EndCopy3:	
		mnop || W[I1++]=R0.L || R0.L=W[I0++];

// ����������� ������� �������� ������ SPORT0.0 � SPORT0.1
	I0.L=_RxBuf0+LenSport0;	// � I0 ����� ������� ������� SPORT0.0
	I0.H=_RxBuf0+LenSport0;
	I1.L=_InTestData;		// � I1 ����� �������� �������
	I1.H=_InTestData;
	R0.L=W[I0++];
	mnop || R0.L=W[I0++] || W[I1++]=R0.L;
	mnop || R0.L=W[I0++] || W[I1++]=R0.L;
	mnop || R0.L=W[I0++] || W[I1++]=R0.L;
	mnop || R0.L=W[I0++] || W[I1++]=R0.L;
	mnop || R0.L=W[I0++] || W[I1++]=R0.L;
	W[I1++]=R0.L;
	I0.L=_RxBuf0+LenBuf0+LenSport0; // � I0 ����� ������� ������� SPORT0.1
	I0.H=_RxBuf0+LenBuf0+LenSport0;
	R0.L=W[I0++];
	mnop || R0.L=W[I0++] || W[I1++]=R0.L;
	mnop || R0.L=W[I0++] || W[I1++]=R0.L;
	mnop || R0.L=W[I0++] || W[I1++]=R0.L;
	mnop || R0.L=W[I0++] || W[I1++]=R0.L;
	mnop || R0.L=W[I0++] || W[I1++]=R0.L;
	W[I1++]=R0.L;

// ����������� �������� �������� (���������) ������ �� SPORT0.0 � SPORT0.1
	I1.L=_TxBuf0+LenSport0;	// � I1 ����� �������� ������� SPORT0.0
	I1.H=_TxBuf0+LenSport0;
	I0.L=_OutTestData;		// � I0 ����� ��������� ������
	I0.H=_OutTestData;
	R0.L=W[I0++];
	mnop || R0.L=W[I0++] || W[I1++]=R0.L;
	mnop || R0.L=W[I0++] || W[I1++]=R0.L;
	mnop || R0.L=W[I0++] || W[I1++]=R0.L;
	mnop || R0.L=W[I0++] || W[I1++]=R0.L;
	mnop || R0.L=W[I0++] || W[I1++]=R0.L;
	W[I1++]=R0.L;
	I1.L=_TxBuf0+LenBuf0+LenSport0; // � I0 ����� �������� ������� SPORT0.1
	I1.H=_TxBuf0+LenBuf0+LenSport0;
	R0.L=W[I0++];
	mnop || R0.L=W[I0++] || W[I1++]=R0.L;
	mnop || R0.L=W[I0++] || W[I1++]=R0.L;
	mnop || R0.L=W[I0++] || W[I1++]=R0.L;
	mnop || R0.L=W[I0++] || W[I1++]=R0.L;
	mnop || R0.L=W[I0++] || W[I1++]=R0.L;
	W[I1++]=R0.L;

// ����������� �������� ������ SPORT1 
	I0.L=_OutDataChan+2;// � I0 ����� ������� �������
	I0.H=_OutDataChan+2;
	I1.L=_TxBuf1;		// � I1 ����� �������� �������
	I1.H=_TxBuf1;
	R0.L=W[I2];			// ������ �������
	W[I1++]=R0.L;
	P2=LenSport1-1;		// ������� ����� �������� ������� ��� 0-��
	R0.L=W[I0++];
	R1.L = 0xF000;		// ----- ���������� ������ ��� ������� ����� 0x8000 ... 0x800F
	R1.H=0;				// ---
	R2.H=0;				// ---
	R2.L=0xF001;		// -----------------------------------------------------
	LSETUP(StCopy4,EndCopy4) LC0=P2;
StCopy4:
		R0 = R0.L (X);	// ���������� �� 32 ���
		R0.L=R0.L>>>3;	// ����������� 16 �������� � 13
		CC = R0 == R1;	// ----- ���������� ������ ��� ������� ����� 0x8000 ... 0x800F
		IF CC R0 = R2;  // -----------------------------------------------------------
EndCopy4:	
		mnop || R0.L=W[I0++] || W[I1++]=R0.L;

// ����������� �������� ������ SPORT0.0 � SPORT0.1
	I1.L=_TxBuf0;		// � I1 ����� �������� �������
	I1.H=_TxBuf0;
	P2=LenSport0/2;		// ������� ����� ������� ���������� ������� / 2
	LSETUP(StCopy5,EndCopy5) LC0=P2;
StCopy5:
#ifdef OldMKA
		R0.L=R0.L>>1;	// ����������� 16 �������� � 15
#endif
EndCopy5:
		mnop || R0.L=W[I0++] || W[I1++]=R0.L;

	I1.L=_TxBuf0+LenBuf0;	// � I1 ����� �������� �������
	I1.H=_TxBuf0+LenBuf0;
	LSETUP(StCopy6,EndCopy6) LC0=P2;
StCopy6:
#ifdef OldMKA
		R0.L=R0.L>>1;	// ����������� 16 �������� � 15
#endif
EndCopy6:
		mnop || R0.L=W[I0++] || W[I1++]=R0.L;

// ����������� ������ ����������� �������� ������� �� ����������� �������
	I0.L=_OutDataChan+LenHard*2;				// � I0 ����� ������� �������
	I0.H=_OutDataChan+LenHard*2;
	I1.L=_InDataChan+(LenHard+LenVirt)*2;		// � I1 ����� �������� �������
	I1.H=_InDataChan+(LenHard+LenVirt)*2;

	P2=LenVirt;									// ������� ����� �������
	R0.L = W[I0++];
	LSETUP(StCopy7,EndCopy7) LC0=P2;
StCopy7:
EndCopy7:
		mnop || W[I1++]=R0.L || R0.L=W[I0++];

	I0.L=_OutDataChan+(LenHard+LenVirt)*2;		// � I0 ����� ������� �������
	I0.H=_OutDataChan+(LenHard+LenVirt)*2;
	I1.L=_InDataChan+LenHard*2;					// � I1 ����� �������� �������
	I1.H=_InDataChan+LenHard*2;

	R0.L = W[I0++];
	LSETUP(StCopy8,EndCopy8) LC0=P2;
StCopy8:
EndCopy8:
		mnop || W[I1++]=R0.L || R0.L=W[I0++];

	I0.L=_Toggl;
	I0.H=_Toggl;
	R1.L=W[I0];
	BITTGL(R1,0);
	W[I0]=R1.L;

	I1.L=_TxBuf0;
	I1.H=_TxBuf0;
	R0.L=W[I1];
#ifdef OldMKA
	BITSET(R0,15); // ��������� ������� � ������� ������� 1-�� �����
#endif
	BITCLR(R0,0);
	I3.L=_StopBit;
	I3.H=_StopBit;
	R2.L=W[I3];
	R2=R2.L (X);
    BITCLR(R2,1);
	CC = R2 == 1;
    IF CC JUMP TestDTPRI; // ������ ��������� ��. ���� �� DTPRI ��� _StopBit&=1
      R0=R0 | R1; // ��������� 0/1 � ������� ���� 1-�� �����
TestDTPRI:
	W[I1]=R0.L;
	I1.L=_TxBuf0+LenBuf0;
	I1.H=_TxBuf0+LenBuf0;
	R0.L=W[I1];
#ifdef OldMKA
	BITSET(R0,15); // ��������� ������� � ������� ������� 1-�� �����
#endif
	BITCLR(R0,0);
	R2.L=W[I3];
	R2=R2.L (X);
    BITCLR(R2,0);
	CC = R2 == 2;
    IF CC JUMP TestDTSEC; // ������ ��������� ��. ���� �� DTSEC ��� _StopBit&=2
      R0=R0 | R1; // ��������� 0/1 � ������� ���� 1-�� �����
TestDTSEC:
	W[I1]=R0.L;

// ��������� �������� ������ 
	P2 = LenChan;
	I0.L=_OutDataChan; // � I0 ����� �������� �������
	I0.H=_OutDataChan;
	R0 = 0;
	LSETUP (StCopy9,EndCopy9) LC0=P2;
StCopy9:
EndCopy9:
      W[I0++] = R0.L;
    rts;
._CopyData.end:

/*******************************************************************************
void SumDataChan (void)
    ��������� ������������ ������ �� ������� �� ������ TablSum
    ������������� ����������
    ����� ������� ������ ���� = (175 / 2) * 3 + 1 (������� ���� � ����� �������)
    ����� ������������ ��� ���������� ������ 175 ������� = 9520 (7.6%)
    ����� ������������ ��� ����������� ������ 175 ������� = 14040 (11.2%)
    ����� ������������ ��� ���������� ������ 174 ������� �� 2 ������� = 16720 (13.3%)
    ����� ������������ ��� ����������� ������ 174 ������� �� 2 ������� = 19140 (15.3%)
*******************************************************************************/
.global          _SumDataChan;
_SumDataChan:
	[--SP] = (R7:5);    	// Preserve R7-5 on the stack
	[--SP] = (P5:4);   		// Preserve P3 on the stack
	P0.L=_TablSum;			// � P0 ����� ������ ������� ���������� TablSum
	P0.H=_TablSum;
	R2.L=_InDataChan;		// � R2 ����� ������� ������� ������
	R2.H=_InDataChan;
	R7.L=_OutDataChan;		// � R7 ����� ������� �������� ������
	R7.H=_OutDataChan;
	I0.L=_TablChan;			// � I0 ����� ��������� ������� �������
	I0.H=_TablChan;
	P5.L=_TablBlock;		// � P5 ����� ������� ����������� �������
	P5.H=_TablBlock;
	R5.H= MaxShort;
StartS1:
    R0 = B[P0++](Z);		// ���� �� ����������� ������?
    P1 = R0;
    CC = R0 == 0;
    if CC jump EndSumS1;
      R1 = B[P5++](Z);		// �������� �� ������� ����������� ������� (��� ������� TablSum � P0)
      R1 = 2;					// ��, ������ �������� ���� 2 ������
      CC = R0 == R1;
      if CC jump NextS7;
        // ������������ ������� ����� 2-� � ������� ��� ��������� �� 0x7FFF;
        P4.L=_TablBlockVar;		// � P4 ����� ��������� ������� ����������� �������
        P4.H=_TablBlockVar;
        I0.L=_TablChan;			// � I0 ����� ��������� ������� �������
        I0.H=_TablChan;
        LSETUP(NextS1,NextS2) LC1=P1;	// ���������� ��������� 2-� �������� ������� �������
NextS1:
          R1 = B[P5++](Z);	// ������ ��������� ����� ������
          B[P4++] = R1;		// ���������
          R1 = B[P0++](Z);	// ������ ����� ������� ������ (������� ������� �������)
          R1 = R1 + R1;		// ��������� ��� ����� (2 ����� �� �����)
NextS2:
          W[I0++] = R1.L;	// ���������� �������� ��� ������� ������

        P2.L=_TablChan;			// � P2 ����� ������� �������
        P2.H=_TablChan;
        P4.L=_TablBlockVar;		// � P4 ����� ��������� ������� ����������� �������
        P4.H=_TablBlockVar;
        A0 = 0;					// ����� �� ������
        R1 = W[P2++](Z);		// ����� ������
        R1 = R1 + R2;
        LSETUP(NextS3,NextS4) LC1=P1;// ��������� ��� ������ ������ ������ ����������
NextS3:
          I1=R1;         // �������� �������� ������ (� ������� InDataChan) 
          R1=W[P2++](Z); // ����� ������
          R0=(LenHard+LenVirt)*2; // ==352 - �������� �� ������ �������� �� � InDataChan
          R0=R1-R0;
          R1=R1+R2;
          R3=B[P4++](Z);
          CC = R3==0;
          IF CC jump NextS4;
            R3.L = W[I1]; // ������ �� ������
            CC = R0<0;    // �������� �� ������������� ����� ������ �������� ��
            IF CC JUMP NextS4;
              R0.L=8;     // � ��������� 1<<3 == 8 �� �������� 
              R3.L=R3.L-R0.L(S);
              W[I1] = R3.L;
NextS4:
          A0+=R3.L*R5.H; // ���������

        A1 = A0;         // ��������� �������� �����
        P2.L=_TablChan;  // � P2 ����� ������� �������
        P2.H=_TablChan;
        P4.L=_TablBlockVar; // � P4 ����� ��������� ������� ����������� �������
        P4.H=_TablBlockVar;
        R1 = W[P2++](Z); // ����� ������ ��� ��������� ��� ��������
        R0 = R1 + R2;
        LSETUP(NextS5,NextS6) LC1=P1;// ������� �������� ������ ��� ���� ������� ������ ������ ����������
NextS5:
          I1 = R0;
          R6 = R1 + R7;     // ����� �������� ������
          I0 = R6;
          R1 = W[P2++](Z);  // ����� ������
          R0 = R1 + R2;     // ����� ������� ������
          A0 = A1; 
          R3 = B[P4++](Z);
          CC = R3 == 0;
          IF CC jump NextS51; // ���� 0 - ����� ������������ �� �����
            R3.L = W[I1];     // ������ �� ������
            A0-=R3.L * R5.H;  // ��������
NextS51:
          R3.H = W[I0];       // ��������� ���������� ���������
          R3.L = (A0+=R3.H * R5.H);
NextS6:
          W[I0] = R3.L;       // ���������� ���������
          jump StartS1;
NextS7:  // ------------- ��������� ��� 2-� ������� ��� ������ ��� ������������ �����-������
      R1 = B[P0++](Z);  // ����� ������� ������
      R1 = R1 + R1;
      R0 = R1 + R2;     // ����� ������� ������ 1-�� ������
      I1 = R0;
      R0 = R1 + R7;     // ����� �������� ������ 1-�� ������
      I2 = R0;
      R1 = B[P0++](Z);  // ����� ������� ������
      R1 = R1 + R1;
      R0 = R1 + R2;     // ����� ������� ������ 2-�� ������
      P1 = R0;
      R0 = R1 + R7;     // ����� �������� ������ 2-�� ������
      P2 = R0;
      R0 = B[P5++](Z);  // ��������� �� ����������� ����
      CC = R0 == 0;
      IF CC JUMP NextS8;
        R0.L = W[I1];   // ������ 1-�� ������
NextS8:
      R1 = B[P5++](Z);  // ��������� �� ����������� ����
      CC = R1 == 0;
      IF CC JUMP NextS9;// ���� 0 - ����� ������������ �� �����
        R1.L = W[P1];   // ������ 2-�� ������
NextS9:
      R3.L = W[I2];     // ������ ������
/*
	A0 = R3.L * R5.H;
	R3.L = (A0+=R1.L*R5.H);	// ��������� �����
	W[I2] = R3.L;			// 2-� ����� � 1-� � �������������
	R3.L = W[P2];			// ������ ������
	A0 = R3.L * R5.H;
	R3.L = (A0+=R0.L * R5.H);// ��������� �����
*/
      R3.L=R1.L + R3.L (s); // ��������� �����
      W[I2] = R3.L;         // 2-� ����� � 1-� � �������������
      R3.L = W[P2];         // ������ ������
      R3.L=R0.L + R3.L (s); // ��������� �����
      W[P2]=R3.L;           // 1-� ����� �� 2-� � �������������
      jump StartS1;
EndSumS1:
    (P5:4) = [SP++];
    (R7:5) = [SP++];
    rts;
_SumDataChan.end:

/*******************************************************************************
Function Name  : _a_compress

Synopsis       : unsigned short a_compress (input);

Description    : This function computes A-law compression of the given
                 input vector.

Operands       : R0 - input data,

Registers Used : R0 - Current input data
                 R1 - ABS(Current input value)
                 R2 - Output value
                 R3 - Exponent of input data
                 R4 - 4095 (maximum input value)
                 R5 -   63 (constant used to test for input data less than 64)
                 R6 -   26 (constant used to convert signbits to chord)
                 R7 - 0x55 (pattern used for inverting bit values)
********************************************************************************/
.global  _a_compress;
_a_compress:
      [--SP] = (R7:4);                      // Preserve R7-R4 on the stack
    /* Initialize Registers */
      R7 = 0x55;                            // Initialize R7 to 0x55
      R6 = 26;                              // Initialize R6 to 26
      R5 = 63;                              // Initialize R5 to 63
      R4 = 4095;                            // Initialize R4 to 4095
      R3 = 0;                               // Initialize R3 to 0

      R0=R0.L (X);
    /* Clip to Maximum */

      R1 = ABS R0;                       // R0 = ABS(input data)
      R1 = MIN(R1, R4);                  // R0 = MIN(ABS(input data),4095)

    /* IF (Input Data < 64) ... */

        R2 = R1 >> 1;

        CC = R1 <= R5;
        IF CC R1 = R2;                     // Drop the least significant bit
        IF CC JUMP GET_SIGN;               // Jump to finalize the output

//GET_CHORD:
      /* ... ELSE Get Chord and Step */

        R3.L = SIGNBITS R1;                // Count redundant sign bits
        R2 = R6 - R3;                      // Chord = 26 - Signbits(input)
        R3 = R1;                           // Step =
        R3 >>= R2;                         //        (ABS(input) >> chord)
        BITCLR(R3,4);                      //        & 0xffffffef

        R1 = R2 << 4;                      // Position Chord
        R1 = R1 | R3;                      // R1 = (Chord<<4) | Step

GET_SIGN:
      /* Get Sign and Invert every other Bit */

        CC = R0 < 0;                       // Test if input data is negative
        IF CC JUMP NEGATIVE;               // Skip if it is
        BITSET(R1,7);

NEGATIVE:
        R1 = R1 ^ R7;                      // Invert every other bit
      R0=R1;   // Return R0
      (R7:4) = [SP++];                      // Restore registers
      rts;  
._a_compress.end:

/******************************************************************************
Function Name  : _a_expand

Synopsis       : unsigned short a_expand (unsigned short input);
                 
Description    : This function computes A-law expansion of the given
                 input vector.

Operands       : R0 - input dsts

Registers Used : R0 - Current input value, and work register
                 R1 - Chord from input value
                 R2 - Step from input value, and output value
                 R3 - 0x0f   (pattern used to extract the step value)
                 R6 - 0x0403 (bit extraction mask for the chord - pos=4, len=3)
                 R7 - 0x55   (pattern used for inverting bit values)
                 I1 - Pointer to current output
                 P0 - Loop counter through input data
                 P1 - Pointer to current input

********************************************************************************/
.global          _a_expand;
_a_expand:
      [--SP] = (R7:5);                      // Preserve R7-R5 on the stack
      /* Initialize Registers */
      R7 = 0x55;                            // Initialize R7 to 0x55
      R6 = 0x0403;                          // Initialize R6 to 0x0403
      R3 = 0x0F;                            // Initialize R3 to 15

      R0 = R0.L (X);
      R0 = R0 ^ R7;                      // Undo bit inversion by a_compress

      /* Extract the Step */

      R2 = R0 & R3;                      // Extract Step (bits 0,1,2,3)

      /* Prepare Step */

      R2 <<= 1;                          // Step = Step << 1;
      R2 += 1;                           // Step = Step + 1;

      /* Extract the Chord */
      R1 = EXTRACT(R0,R6.L) (Z);         // Extract Chord (bits 4,5,6)

      /* IF (Chord \= 0) ... */

      CC = R1 == 0;
      R1 += -1;
      R5 = R2; 
      BITSET(R5,5);
      R5 <<= R1;
      IF !CC R2 = R5;

      /* Check Sign bit in Input */

      CC = BITTST(R0,7);
      R1 = -R2;
      IF !CC R2 = R1;

      R0 = R2;                    // Store the result in output array
     (R7:5) = [SP++];             // Restore registers
     rts;
._a_expand.end:

/**************************************************************
  label name  :  _sin2F

  Description :  ��������� ������� Sin(F1,F2,Ampl) �� ���� ��������� ������� ��������

  Sine Approximation: y = sin (x1, x2, ampl)

  Synopsis       : signed short sin2F (signed short F1, signed short F2, unsigned short Ampl);
  
  Registers used :
  
  ����� ������� : 160 �� �� �����

  R0, R1 -> INPUT value,
  R1,R2,R3,P0,P1,P2,A0
***************************************************************/

.global _sin2F;
_sin2F:
      [--SP] = (R7:6);	           	// Preserve R7, R6 on the stack
	  R6 = R1;					  	// 2-e ������� ������
	  R7 = R2 >> 1;					// ��������� / 2

	  R2 = 0x8000 (Z);				// ����������� ��������� sin
	  CC = R2 <= R0;				// ������� ������ � R0
	  IF ! CC JUMP sin21;
	  R1 = 0xC000 (Z);
	  CC = R1 <= R0;
	  IF ! CC JUMP sin11;
	  R0 += 1;
	  R0 = -R0;
	  R0 <<= 1;
	  JUMP sinst1;
sin11:R0 = R2 + R0;
	  R0 <<= 1;
	  JUMP sinst1;
sin21:R1 = 16384 (X);
	  CC = R0 < R1;
	  IF ! CC JUMP sin31;
	  R0 = -R0;
	  R0 <<= 1;
	  JUMP sinst1;
sin31:R1 = -32768 (X);
	  R0 = R1 + R0;
	  R0 <<= 1;
	  
sinst1:
	  R0 = R0.L (X);
	  P0 = 2;                     	// STORE LOOP COUNTER VALUE
      P1.L = _SinCoef;            	// POINTER TO SIN COEFFICIENT
      P1.H = _SinCoef;
      P2 = R0;
      R1 = ABS R0;                	// GET THE ABSOLUTE VALUE OF INPUT
      R3 = R1;                    	// COPY R1 TO R3 REG (Y = X)

      A0 = 0 || R0 = W[P1++] (Z);   // CLEAR ACCUMULATOR AND GET FIRST COEFFICIENT
      LSETUP(SINSTRT1,SINEND1) LC0 = P0;
                                  	// SET A LOOP FOR LOOP COUNTER VALUE
SINSTRT1:  R1.H = R1.L * R3.L;              // EVEN POWERS OF X
          A0 += R3.L * R0.L || R0 = W[P1++] (Z);
          R3.L = R1.L * R1.H;              // ODD POWERS OF X)
SINEND1:   A0 += R1.H * R0.L || R0 = W[P1++] (Z);

      R1 = 0x7fff;                	// INITIALISE R1 TO 0X7FFF
      R2 = (A0 += R3.L * R0.L);
      R2 = R2 >> 15;              	// SAVE IN FRACT16
      CC = R1 < R2;               	// IF R2 > 0X7FFF
      IF CC R2 = R1;              	// IF TRUE THEN INITIALISE R2 = 0X7FFF
      CC = P2 < 0;                	// CHECK WHETHER INPUT IS LESS THAN ZERO
      R0 = -R2;                   	// OUTPUT IS NEGATED
      IF !CC R0 = R2;             	// USE NON_NEGATED OUTPUT IF INPUT >= ZERO
      NOP;
	  R0.L = R0.L * R7.L;			// ��������� ���������
      
// ��������� �������
	  P1.L = _Ampl2;				// ��������� 2-� ������� ����������
	  P1.H = _Ampl2;
	  R7 = W[P1] (Z);
	  R7 >>= 1;
	  R2 = 0x8000 (Z);				// ����������� ��������� sin
	  CC = R2 <= R6;				// ������� ������ � R6
	  IF ! CC JUMP sin22;
	  R1 = 0xC000 (Z);
	  CC = R1 <= R6;
	  IF ! CC JUMP sin12;
	  R6 += 1;
	  R6 = -R6;
	  R6 <<= 1;
	  JUMP sinst2;
sin12:R6 = R2 + R6;
	  R6 <<= 1;
	  JUMP sinst2;
sin22:R1 = 16384 (X);
	  CC = R6 < R1;
	  IF ! CC JUMP sin32;
	  R6 = -R6;
	  R6 <<= 1;
	  JUMP sinst2;
sin32:R1 = -32768 (X);
	  R6 = R1 + R6;
	  R6 <<= 1;
	  
sinst2:
	  R6 = R6.L (X);
	  P0 = 2;                     	// STORE LOOP COUNTER VALUE
      P1.L = _SinCoef;            	// POINTER TO SIN COEFFICIENT
      P1.H = _SinCoef;
      P2 = R6;
      R1 = ABS R6;                	// GET THE ABSOLUTE VALUE OF INPUT
      R3 = R1;                    	// COPY R1 TO R3 REG (Y = X)

      A0 = 0 || R6 = W[P1++] (Z);   // CLEAR ACCUMULATOR AND GET FIRST COEFFICIENT
      LSETUP(SINSTRT2,SINEND2) LC0 = P0;
                                  	// SET A LOOP FOR LOOP COUNTER VALUE
SINSTRT2: R1.H = R1.L * R3.L;       // EVEN POWERS OF X
          A0 += R3.L * R6.L || R6 = W[P1++] (Z);
          R3.L = R1.L * R1.H;       // ODD POWERS OF X)
SINEND2:  A0 += R1.H * R6.L || R6 = W[P1++] (Z);

      R1 = MaxShort;               	// INITIALISE R1 TO 0X7FFF
      R2 = (A0 += R3.L * R6.L);
      R2 = R2 >> 15;              	// SAVE IN FRACT16
      CC = R1 < R2;               	// IF R2 > 0X7FFF
      IF CC R2 = R1;              	// IF TRUE THEN INITIALISE R2 = 0X7FFF
      CC = P2 < 0;                	// CHECK WHETHER INPUT IS LESS THAN ZERO
      R6 = -R2;                   	// OUTPUT IS NEGATED
      IF !CC R6 = R2;             	// USE NON_NEGATED OUTPUT IF INPUT >= ZERO
	  NOP;
      R6.L = R6.L * R7.L;			// ��������� ���������

      R0 = R6 + R0;
//      R0 >>= 1;
//      R2 = _0dB (Z);				// 0 ��, ����� ��������
//	  NOP;
//	  R0.L = R0.L * R7.L;
      (R7:6) = [SP++];
      rts;
._sin2F.end:

/************************************************************************
 IirFull.asm

 Title:        16-bit,  IIR filter for Blackfin
        
Synopsis       :  short IIRFull	{short data_in, short *delay, short *coeff}
                 
{coeff : ����� ������,(B2,B1,B0,A2,A1,shift ������)*����� ������, ����� shift, ����� ��������� (3+6*N)
 delay : (Xn-2,Xn-1,Yn-2,Yn-1)*����� ������ (4*N)
 Yn = ((Xn-2 * B2) + (Xn-1 * B1) + (Xn * B0) + ((Yn-2 * A2) + (Yn-1 * A1))*2)*2}
******************************************************************************/
.global _IIRFull;
_IIRFull:
	// ������� ������ � R0
        I2 = R1;                // I2 = ����� ������� �������� Xn-2,Xn-1
		I0 = R2;                // I0 = ����� ������������� N,B2,B1,B0
        R1+= 4;
        I3 = R1;                // I3 = ����� ������� �������� Yn-2,Yn-1
		R2+= 8;
        I1 = R2;                // I1 = ����� ������������� A2,A1
        R3.L = W[I0++];			// ����� ������ �������
        R3.H = 0;
		P1 = R3;
		m1 = -4;
		m2 = 4;
		m3 = 6;
		nop;
        LSETUP( iir_Block_Start, iir_Block_End) LC0 = P1; //������ == ����� ������
iir_Block_Start:
			A1=A0=0 || R1.L=W[I0++] || R1.H=W[I1++];	// ������ B2->R1.L � A2->R1.H, 0->A0,A1
			R2.L = W[I2++] || R2.H=W[I3++];				// ������ Xn-2->R0.L � Yn-2->R0.H
			A1 = R2.L*R1.L, A0=R2.H*R1.H || R2.L=W[I2--] || R2.H=W[I3--];	
			R1.L = W[I0++] ||	R1.H=W[I1++];			// ������ B1->R1.L � A1->R1.H
			W[I2++] = R2.L;								// Xn-1 -> delay �� ����� Xn-2			
			W[I3++] = R2.H;								// Yn-1 -> delay �� ����� Yn-2
			A1+= R2.L*R1.L, A0+=R2.H*R1.H || R1.L=W[I0++] || R2.L=W[I1++];
// A1+=B1*Xn-1, A0+=A1*Yn-1 � ������ B0->R1.L ����� -> R2.L
			W[I2++] = R0.L;								// Xn -> delay �� ����� Xn-1			
			A1+= R1.L*R0.L;								// A1+=B0*Xn
			A0+= A1;									// A0+=A1
			A0 = ASHIFT A0 BY R2.L;						// ����� ������
			R0.L = A0;
			W[I3++] = R0.L;								// Yn -> delay �� ����� Yn-1
			I0+= m3; 
			I1+= m3; 
			I2+= m2; 
iir_Block_End: I3+= m2;
		R2.L = W[I0++];									// ����� �����
        R2.H = W[I0];									// ��������� �����
        A0 = R2.H*R0.L;
        A0 = ASHIFT A0 BY R2.L;
		R0.L = A0;										// ������� ��������
        rts;
._IIRFull.end:

/************************************************************************
 IirSport1.asm

 Title:        16-bit,  IIR filter ��� ��� ������� SPORT1
        
 Synopsis       :  short IIRSport1(short data_in);
                 
{����� ������ = 1;
 shift ������ = 1;
 shift �����  = 1;
 ���������    = 0x4000;
	coeff : B2,B1,B0,A2,A1
 	delay : Xn-2,Xn-1,Yn-2,Yn-1
 Yn = ((Xn-2 * B2) + (Xn-1 * B1) + (Xn * B0) + ((Yn-2 * A2) + (Yn-1 * A1))*4}
******************************************************************************/
.global _IIRSport1;
_IIRSport1:
		I0.L = _coef300;    // I0 = ����� ������������� B2,B1,B0
		I0.H = _coef300;
        I1.L = _coef300+6;  // I1 = ����� ������������� A2,A1
        I1.H = _coef300+6;
        I2.L = _del300;     // I2 = ����� ������� �������� Xn-2,Xn-1
        I2.H = _del300;
        I3.L = _del300+4;   // I3 = ����� ������� �������� Yn-2,Yn-1
        I3.H = _del300+4;   // I3 = ����� ������� �������� Yn-2,Yn-1

		A1=A0=0 || R1.L=W[I0++] || R1.H=W[I1++];	// ������ B2->R1.L � A2->R1.H, 0->A0,A1
		R2.L = W[I2++] || R2.H=W[I3++];				// ������ Xn-2->R0.L � Yn-2->R0.H
		A1 = R2.L*R1.L, A0=R2.H*R1.H || R2.L=W[I2--] || R2.H=W[I3--];	
		R1.L = W[I0++] ||	R1.H=W[I1++];			// ������ B1->R1.L � A1->R1.H
		W[I2++] = R2.L;								// Xn-1 -> delay �� ����� Xn-2			
		W[I3++] = R2.H;								// Yn-1 -> delay �� ����� Yn-2
		A1+= R2.L*R1.L, A0+=R2.H*R1.H || R1.L=W[I0++] || R2.L=W[I1++];
// A1+=B1*Xn-1, A0+=A1*Yn-1 � ������ B0->R1.L ����� -> R2.L
		W[I2++] = R0.L;								// Xn -> delay �� ����� Xn-1			
		A1+= R1.L*R0.L;								// A1+=B0*Xn
		A0+= A1;									// A0+=A1
		A0 = ASHIFT A0 BY R2.L;						// ����� ������
		R0.L = A0;
		W[I3] = R0.L;								// Yn -> delay �� ����� Yn-1

		R2.L = W[I1++];								// ����� �����
		R2.H = W[I1];								// ��������� �����
		A0 = R2.H * R0.L;
		A0 = ASHIFT A0 BY R2.L;
		R0.L = A0; 
        rts;
._IIRSport1.end:

/*******************************************************************************
void CalcMiddle(void) ------------------- ���������� ������� ��������
	� SumDacMiddle (��� + ���) � � SumMiddle (���)
	������� ����� ������ ??? �� 
 *******************************************************************************/
.global _CalcMiddle;
_CalcMiddle:
    // ���������� �������� ��� ���������� ������� � ����� �����������
	P0.L=_OutDataChan  + (LenSport1 * 2);	// � P0 ����� �������� ������ SPORT0
	P0.H=_OutDataChan  + (LenSport1 * 2);
	I2.L=_SumDACMiddle;						// � I2 ����� SumDACMiddle
	I2.H=_SumDACMiddle;
	P2 = LenSport0;
    LSETUP( Calc_Start1, Calc_End1) LC0 = P2;
Calc_Start1:
		R1 = W[P0++] (X);					// R1 = InDataChan
		R1 = ABS R1 || R2 = [I2] || NOP;	// R1 = ABS R1, R2 = SumDACMiddle
		R2 = R2 + R1 ( NS );				// SumMiddle += |InDataChan|
		[I2] = R2 ;
Calc_End1:
		I2 += 4;

	P0.L=_InDataChan  + (LenHard * 2);		// � P0 ����� �������� ������ ����� ����������� 
	P0.H=_InDataChan  + (LenHard * 2);
	P2 = LenVirt;
    LSETUP( Calc_Start2, Calc_End2) LC0 = P2;
Calc_Start2:
		R1 = W[P0++] (X);					// R1 = InDataChan
		R1 = ABS R1 || R2 = [I2] || NOP;	// R1 = ABS R1, R2 = SumDACMiddle
		R2 = R2 + R1 ( NS );				// SumMiddle += |InDataChan|
		[I2] = R2 ;
Calc_End2:
		I2 += 4;

    // ���������� �������� ��� ���������� ������� � ����� �����������
	P0.L=_InDataChan  + (LenSport1 * 2);	// � P0 ����� ������� ������ SPORT0
	P0.H=_InDataChan  + (LenSport1 * 2);
	I2.L=_SumADCMiddle;						// � I2 ����� SumADCMiddle
	I2.H=_SumADCMiddle;
	P2 = LenSport0;
    LSETUP( Calc_Start3, Calc_End3) LC0 = P2;
Calc_Start3:
		R1 = W[P0++] (X);					// R1 = InDataChan
		R1 = ABS R1 || R2 = [I2] || NOP;	// R1 = ABS R1, R2 = SumADCMiddle
		R2 = R2 + R1 ( NS );				// SumMiddle += |InDataChan|
		[I2] = R2 ;
Calc_End3:
		I2 += 4;

	P0.L=_OutDataChan  + (LenHard * 2);		// � P0 ����� ������� ������ ����� ����������� 
	P0.H=_OutDataChan  + (LenHard * 2);
	P2 = LenVirt;
    LSETUP( Calc_Start4, Calc_End4) LC0 = P2;
Calc_Start4:
		R1 = W[P0++] (X);					// R1 = OutDataChan
		R1 = ABS R1 || R2 = [I2] || NOP;	// R1 = ABS R1, R2 = SumADCMiddle
		R2 = R2 + R1 ( NS );				// SumMiddle += |OutDataChan|
		[I2] = R2 ;
Calc_End4:
		I2 += 4;

	I2.L=_SumADCShift;						// � I2 ����� SumADCShift
	I2.H=_SumADCShift;
	P0.L=_OutDataChan  + (LenHard * 2);		// � P0 ����� ������� ������ ����� ����������� 
	P0.H=_OutDataChan  + (LenHard * 2);
	P2 = LenVirt;
    LSETUP( Calc_Start5, Calc_End5) LC0 = P2;
Calc_Start5:
		R1 = W[P0++] (X);					// R1 = OutDataChan
		R2 = [I2];							// R2 = SumADCMShift
		R2 = R2 + R1 ( NS );				// SumADCMShift += OutDataChan
		[I2] = R2 ;
Calc_End5:
		I2 += 4;

    rts;
._CalcMiddle.end:

// _______ ������ 10��(-40��) - 300��(0.5��) ______________
.section/DOUBLEANY data1;
// +1 - ��� ��������� ������� ����� �������������

.byte2 _coef300[3+2*6+1] = {
#include "30_300Hz.dat"
};

.byte2 _coef3200[3+4*6+1] = {
#include "3200_3390_8.dat"
};
.byte2 _del300[2*4] = {0,0,0,0,0,0,0,0};
.byte2 _del3200[4*4] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
.global _coef300;
.global _del300;
.global _coef3200;
.global _del3200;

