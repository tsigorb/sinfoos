// ��������� �������� �� ������ � �� � �������� �
#include "..\Sinfo.h"
#include "SportXFn.h"
#include <fract.h>

//--------------------------------------------------------------------------//
void *pvPortMalloc(size_t xSize);
void vPortFree(void *pv);

//--------------------------------------------------------------------------//
extern unsigned short DataSave[LenDataSave]; // !!! �������

extern xListGenChanItem MasGenChanItem[LenGener];

extern sListSpec ListSpec; // ��������� ��������� ������������ ������

extern short ErrMallocGener; // ������ �������������� ������ ��� �������� ������� ���������
extern short ErrMallocSpec; // ������ �������������� ������ ��� �������� ������� ��������� ����������

//--------------------------------------------------------------------------//
unsigned char FoundPlaceGen(unsigned char chan)
{ // ����� ���������� ����� � ������� ��������� ��� ������� ��������� �� ������� ������
  unsigned char IndChan;

  if((chan<LenSport1) || (chan>=LenHard+LenVirt+LenVirt)) return 0xff;
  for(IndChan=0;IndChan<LenGener;IndChan++)
  {
    if(MasGenChanItem[IndChan].chan==chan) return IndChan;
  }
  for(IndChan=0;IndChan<LenGener;IndChan++)
  {
    if(!MasGenChanItem[IndChan].chan) return IndChan;
  }
  return 0xFF;
}

unsigned char FoundGenerator(unsigned char type,unsigned char chan)
{ // ����� ������� ��������� �� ������� ������
  unsigned short i;

  i=FoundPlaceGen(CalcRealChannel(type,chan));
  if(i==0xFF) return false;
  if(MasGenChanItem[i].chan) return true;
  return false;
}

unsigned char GeneratorMU(unsigned short chan, unsigned short F1, unsigned short F2,
                          unsigned short Ton, unsigned short Tof, unsigned short Ampl1, unsigned short Ampl2)
{ // ����� ��������� �������� � ������ ����������
  unsigned char IndChan;
  xListGenChanItem *AdrOutItem, *AdrItem;

/*{
  if((Ton==80) && (Tof==80))
  { // ��� ����
    SendStr2IpLogger("$DEBUG$GeneratorMU:chan=%d, F1=%d, F2=%d, Ton=%d, Toff=%d",
                     chan,F1,F2,Ton,Tof);
  }
}*/
  if((chan<LenSport1) || (chan>=LenHard+LenVirt+LenVirt)) return false;
  IndChan=FoundPlaceGen(chan); // ����� ���������� ����� � ������� ��������� ��� ������� ��������� �� ������� ������
  if(IndChan==0xFF) return false;
  if(MasGenChanItem[IndChan].chan)
  { // ��� ���� ��������� �� ������
    AdrOutItem=pvPortMalloc(sizeof(xListGenChanItem)); // �������� �������� ������� ���������
    if(!AdrOutItem)
    { ErrMallocGener++; return false; }
    vPortEnableSwitchTask(false);
    AdrOutItem->chan=0; // ��� ���������� ��������� � ���������� ��� �� ������� ���������!
    AdrItem=MasGenChanItem[IndChan].pxNextGen;
    if(AdrItem)
    { // ����� ���������
      while(AdrItem->pxNextGen) AdrItem=AdrItem->pxNextGen;
      AdrItem->pxNextGen=AdrOutItem;  
    }
    else MasGenChanItem[IndChan].pxNextGen=AdrOutItem;
  }
  else
  { // ��� ��������� �� ������
    vPortEnableSwitchTask(false);
    AdrOutItem=MasGenChanItem+IndChan;
  }
  AdrOutItem->F1=(unsigned short)(((unsigned int)F1*65536)/8000); // �������� �������
  AdrOutItem->F2=(unsigned short)(((unsigned int)F2*65536)/8000);
  if(!Ton) AdrOutItem->Ton=0xFFFF; // � ������������ (��� Ton == 0 - ����������� ���������)
  else AdrOutItem->Ton=(Ton / 5); // ������ ������� ��������� ������������ ������ 5 ����
  AdrOutItem->Tof=(Tof / 5); // ������ ������� ��������� ������������ ������ 5 ����
  AdrOutItem->Ampl1=Ampl1;
  AdrOutItem->Ampl2=Ampl2;
  AdrOutItem->sum1=0;
  AdrOutItem->sum2=0;
  AdrOutItem->pxNextGen=0;
  AdrOutItem->chan=chan;
  vPortEnableSwitchTask(true);
  return true;
}

void AnalizeGenerator(void)
{ // ������ ��������� ������� ��������� � ������ ���������
  unsigned char i;
  xListGenChanItem *Adr, *AdrNext;

  for(i=0,Adr=MasGenChanItem;i<LenGener;i++,Adr++)
  {
    if(!Adr->chan || (Adr->Ton==0xFFFF)) continue;
    if(Adr->Ton)
    {
      unsigned int dwIrq=cli();
      Adr->Ton--; // ���������� ������� ���������
      sti(dwIrq);
    }
    else
    {
      if(Adr->Tof) Adr->Tof--; // ���������� ������� �����
      else
      { // ��������� ���������
        AdrNext=(xListGenChanItem *)Adr->pxNextGen;
        if(AdrNext)
        { // �������� ��������� ���������
          unsigned int dwIrq=cli();
          memcpy(Adr,AdrNext,sizeof(xListGenChanItem));
          sti(dwIrq);
          vPortFree(AdrNext);
        }
        else Adr->chan=0; // �������� ���������
      }
    }
  }
}

void ExclAllGenerator(void)
{ // ���������� ��������� �� ���� �������
  unsigned short i;
  xListGenChanItem *Adr,*AdrNext;

  vPortEnableSwitchTask(false);
  for(i=0;i<LenGener;i++)
  {
    if(MasGenChanItem[i].chan)
    {
      MasGenChanItem[i].chan=0;
      Adr=MasGenChanItem[i].pxNextGen;
      while(Adr)
      {
        AdrNext=Adr->pxNextGen;
        vPortFree(Adr);
        Adr=AdrNext;
      }
    }
  }
  vPortEnableSwitchTask(true);
}

unsigned char ExclGen(unsigned char chan)
{ // ���������� ��������� �� ������ ������
  unsigned char IndChan;
  xListGenChanItem *Adr,*AdrNext;

  IndChan=FoundPlaceGen(chan);
  if(IndChan==0xFF) return false;
  if(!MasGenChanItem[IndChan].chan) return false;
  vPortEnableSwitchTask(false);
  MasGenChanItem[IndChan].chan=0;
  Adr=MasGenChanItem[IndChan].pxNextGen;
  while(Adr)
  { // ������� ��� ��������� �� ������
    AdrNext=Adr->pxNextGen;
    vPortFree(Adr);
    Adr=AdrNext;
  }
  vPortEnableSwitchTask(true);
  return true;
}

unsigned char ExclGenerator(unsigned char type,unsigned char chan)
{ // ���������� ��������� �� ������
  return ExclGen(CalcRealChannel(type,chan));
}
void Swinging(unsigned char Chan)
{ // ��������� ���������� �������
  unsigned short i;
  xListGenChanItem *Adr;
  static unsigned short TimeSwing=0; // ����� ����� ���������� ���������� �������

  i=FoundPlaceGen(Chan);
  if(i!=0xFF)
  {
    Adr=MasGenChanItem+i;
#define _ Adr ->
    if(_ chan)
    { // ��������� �� ������ �������
      if(_ F1 || _ F2)
      { // ������� �����������
        if(TimeSwing) TimeSwing--;
        else
        {
          if(_ F1 >= 27858)
          { _ F1=_ F2=2458; TimeSwing=200; } // 300 * 8.192=2458 � 2 ��� ��������
          else
            if(_ F1 < 8192) 
            { _ F1=_ F2 += 8; } // 300-1000��: (8 / 8.192) * 200=195 �� / ���
            else
            { _ F1=_ F2 += 16; }// 1000-3400��: (16 / 8.192) * 200=390 �� / ���
        }
        if((_ F1 == 4098) || (_ F1 == 6554) || (_ F1 == 8370)  || // 500, 800, 1022 ��
           (_ F1 == 13114) || (_ F1 == 19666) || (_ F1 == 24570) || (_ F1 == 27858)) // 1601, 2401, 2999, 3401 ��
        { _ F1=_ F2 += 8; TimeSwing=200; } // �������� 2 ��� ��� 500��, 800��, 1022��, 1601��, 2401��, 2999��, 3401��
      }
      else
      { // �������������� ��������� ������ � ������� ���
        _ F1=_ F2=2458; TimeSwing=200; // 300 * 8.192=2458 � 2 ��� ��������
      }
    }
#undef _  
  }
}

// ----!!! ????????---   ����� �� ��������   ---????????????????????????????===================
unsigned char CallSpecial(unsigned short NChan)
{ // ��������� ������������ ������ ���������
extern unsigned char MassInG[];
  unsigned char i;
  unsigned short F1,F2,Ton,Tof;
  for (i=0;i<MassInG[4];i++)
  {
    F1 =(unsigned short)MassInG[5+(i*8)]+((unsigned short)MassInG[6+(i*8)]<<8);
    F2 =(unsigned short)MassInG[7+(i*8)]+((unsigned short)MassInG[8+(i*8)]<<8);
    Ton =(unsigned short)MassInG[9+(i*8)]+((unsigned short)MassInG[10+(i*8)]<<8);
    Tof =(unsigned short)MassInG[11+(i*8)]+((unsigned short)MassInG[12+(i*8)]<<8);
    GeneratorMU(NChan,F1,F2,Ton,Tof,AmplGenTDC,AmplGenTDC);
  }
  return true;
}

unsigned char InitSpecGen(unsigned char *data)
{ // ���������� ��������� ����������
  xListSpecItem *AdrInItem, *AdrOutItem, *AdrItem;
  unsigned short i;

	if (!(AdrOutItem=pvPortMalloc(sizeof (xListSpecItem)+(((*(data+2))-1)*8))))
	{ ErrMallocSpec++; return false; }
	else
	{
#define _ AdrOutItem ->
	  _ NSpec=* data++; // ����� ����������
	  _ NAbon=* data++; // ����� ��������
	  _ NGen	=* data++; // ����� ������������ �������������������
	  _ pxPrevious=0;
	  _ pxNext=0;
	  for (i=0;i<(_ NGen*8);i++) _ Buf[i]=* data++; // ������ ���������� ���������
#undef _
     AdrItem=(xListSpecItem *) ListSpec.pxHead;// ����� ������ ���������� ��������
	 if(AdrItem)
	 {
	   do
	   {
	     AdrInItem=AdrItem;
	     AdrItem=AdrItem->pxNext;
	   }
	   while(AdrItem);
	   AdrInItem->pxNext=AdrOutItem;
	   AdrOutItem->pxPrevious=AdrInItem;
	}
	else ListSpec.pxHead=AdrOutItem;
  }
  return true;
}

