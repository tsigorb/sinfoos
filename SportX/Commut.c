#include "..\Sinfo.h"
#include <signal.h>
#include <fract.h>
#include <math.h>
#include "SportXFn.h"
#include "FuncSportXCmds.h"
//#include <VLAN\VLAN.h>

///////////////////////////////////////////////////////////////////////////////
void *pvPortMalloc(size_t xSize);
void vPortFree(void *pv);

void CommandSportX(void);

///////////////////////////////////////////////////////////////////////////////
extern unsigned char ChanSwing; // ����� ��� ��������� ��������� �������

extern unsigned char YesSpeak[]; // ������� ������� ��������� 0-���, 1...100 - ����

extern unsigned char 	MassInData[LenInOutData];	// ������ ��� ������   ������
extern unsigned char 	MassOutData[LenInOutData];	// ������ ��� �������� ������

extern unsigned char 	TablSum[LenSumChan];		// ������� ������� ����������� �� ������� ���� ���������� (�������������)
extern unsigned char 	TablBlock[LenSumChan];		// ������� ������� ����������� �� �����
extern unsigned char 	CopyTablBlock[]; // ����� ������� ������� ����������� �� �����

extern short			ErrQueueSNMP;				// ������������ ������� ������ SNMP
extern short			ErrMallocE1;				// ������ �������������� ������ ��� �������� ������� ������ ��� E1
extern short			ErrMallocRecord;			// ������ �������������� ������ ��� �������� ������� �����������
extern short			ErrMallocPlay;				// ������ �������������� ������ ��� �������� ������� ��������������������
extern short			ErrSPORT0;					// ������ ������ �� SPORT0
extern short			MeazFreq;					// ��������� ������� �� ������
extern short			TimeFreq;					// ����� ��������� ������� �� ������
extern short			SignFreq;					// ���� ������� �� ������

extern unsigned int TestBoard; // �������� ����������������� (�� 2 ����) ���������� ���� (��.) � ������� �1(��.)
extern unsigned short ParamUKC[]; // ��������� (�� 1 ����) ���������� ������ ������� ���(��.) � E1(��.), ������������ ��������� � SNMP, N����.
extern unsigned char FailureE1[4][10]; // ��������� ��������� ������ ������� E1

extern int 				TimeW;                      // ������ ������� ��������� SportXWait

extern unsigned short	Middle[];					// ������� �������� �� ���������� ������� � ������ ������������
extern unsigned char	NomAbonPCDT[12][12];


extern const unsigned char Comb_TDC[]; 				// �������� ���������� ���

extern unsigned int CountErrTest1, CountErrTest2;

extern unsigned short DataSave[LenDataSave]; // �������

extern unsigned char USER;					// ������� ������������ ���������� �������
extern volatile unsigned int	A1;			// ����� ����������
extern volatile unsigned short	PORT1;		// ���� ����������

extern short CountErrQueueReply;
extern xQueueHandle xQueueReply[];

extern sListRecord ListRecord; // ��������� ������ �����������
extern sListPlay ListPlay; // ��������� ������ ��������������������

extern unsigned char MaxARUTP[]; // �������� ���
extern unsigned char ARUTP[]; // ��������� ���
extern unsigned short KInARU[]; // ��������� ���

extern xQueueHandle xQueueCommand; // �� ��������� ������
extern unsigned short Count10mc;

extern int DeltaTime; // ������ ����� �� 2 ���
extern unsigned int MaxDeltaTime; // ������ ����� �� 2 ���
extern unsigned int MinDeltaTime; // ������ ����� �� 2 ���

extern unsigned short InTestData[LenMKA]; // �������� ������ � ���� ���������� �������
extern unsigned int MKAUARTSTATE;

// ----------- ���������� ��� ��������������� ������� ��������� �������� -----
extern char KeepFile[];
extern unsigned int LenKeep;
extern unsigned short BufPlayKeep[LenSound][2];
extern unsigned short IndBufPlayKeep;
extern unsigned char NumBufPlayKeep;
extern unsigned char NextPlayKeep;

extern unsigned short PlayWavS;
extern unsigned short ChanWavS;

extern char strInfoSpXCmd[];

///////////////////////////////////////////////////////////////////////////////
section("bigdata") char FileName[LenFileName]; // ��� �����
section("bigdata") static unsigned char s_btBuf512[512];

GROUPINFO_TP MasGroupInfo[MaxGroup];
unsigned short TablSport1[LenSport1]; // ������� ���������� ������� SPORT1
unsigned char TablMKC[LenMKC][LenCommutMKC]; // ������� ���������� ������� ���

unsigned char VedSelect[NumberKeyboard]; // ����� ������ �������� ���������
unsigned char Tikker[NumberKeyboard]; // ������� ��������� �������
unsigned short TimeTikker[NumberKeyboard]; // ����� �������
unsigned short PeriodTikker; // ������ �������
unsigned int FTik; // ������� �������

unsigned char TimARU[LenVirt]; // ������ ���
unsigned char CountARU[LenVirt]; // ������� ���

// ---------------------- ����������� �� �� -------------
/*unsigned short LevelNoise0[LenVirt];
unsigned short LevelNoise1[LenVirt];
unsigned short Detector0[LenVirt];
unsigned short Detector1[LenVirt];
unsigned short Detector2[LenVirt];
unsigned short Detector3[LenVirt];*/
// -----------------------------------------------------

unsigned short CountErrSPORT; // ������� ������ ������ �� SPORT
unsigned char VirtualPoints[LenVirt]; // ������� ������� ����� �����������

struct tm *RealTime;
time_t ExistTime;

HANDLE hFileS;
unsigned int StartWavS=0; // ����� �������������� �����
char *AdrOutSound, *SaveAdrOutSound;

section("bigdata") xListSNMP *AdrTempBufSNMP[QueueReplyCnt]; // ������ ��������� ������� ��������� SNMP

char ConstCom[5] =".swt"; // ���������� ����� ����� ��� ����� ���������� ����������
char ConstCom1[5]=".sw1"; // ���������� - ������� 1
char ConstCom2[5]=".sw2"; // ���������� - ������� 2
char ConstCom3[5]=".sw3"; // ���������� - ������� 3

unsigned short StopBit=0; // ������� ������� ��������� �������� ���� � ������ �� Sport0; (==1 ��� ==2)
unsigned short TimeStopBit=0; // ����� �������� ������� ��������� �������� ���� � ������ �� Sport0;

int TimeW; // ������ ������� ��������� SportXWait

///////////////////////////////////////////////////////////////////////////////
unsigned char CalcRealChannel(unsigned char btType,unsigned char btChanNumb)
{ // ����� ������ ���������� � 0!!
  unsigned short chan=btChanNumb;
  if(btType==0)
  { // ���������� �����
    if(chan & 0x80) chan=(chan & 0x7f);
    else chan+=LenSport1;
  }
  else
  {
    if(chan>=LenVirt) chan=LenVirt-1;
    if(btType==1) chan += LenHard+LenVirt; // ���� ��
    else chan += LenHard; // ����� ��
  }
  return(chan);
}

unsigned char IsRecordChannel(unsigned char chan)
{
  xListRecord *AdrRecord=(xListRecord *) ListRecord.pxHead;
#define _ AdrRecord ->
  while (AdrRecord)
  { // ���������� ��� ������ �������
    if(_ chan == chan) return true; 
    AdrRecord=(xListRecord *) _ pxNextRec;
  }
#undef _
 return false;
}

unsigned char IsPlayChannel(unsigned char chan)
{
#define _ AdrPlay ->
  xListPlay *AdrPlay;
  AdrPlay=(xListPlay *) ListPlay.pxHead;
  while(AdrPlay)
  {
    if(_ ChanPlay == chan) return true; 
    AdrPlay=(xListPlay *) _ pxNextPlay;
  }
#undef _
  return false;
}

unsigned short ReversShort(unsigned short data)
{ return ((data >> 8) & 0x00FF) | ((data << 8) & 0xFF00); }

unsigned int ReversInt(unsigned int data)
{return ((data >> 24) & 0x000000FF) | ((data >> 8) & 0x0000FF00) | ((data << 8) & 0x00FF0000) | ((data << 24) & 0xFF000000);}

unsigned char ExitRecord(unsigned char typechan, unsigned char chan0)
{ // ����������� ����������� �� ������
  unsigned char chan;
  unsigned short i;
  xListRecord  *AdrInRecord, *AdrRecord, *AdrOutRecord;

  if(!ListRecord.pxHead) return false; // ��� �������
  if(chan0==0xff)
  {
SendStr2IpLogger("$DEBUG$ExitRecord error: chan0=0xff");
    return false;
  }
  chan=CalcRealChannel(typechan,chan0);
SendStr2IpLogger("$DEBUG$ExitRecord: chan0=0x%x (real chan=%d),",chan0,chan);
  AdrInRecord=AdrRecord=ListRecord.pxHead; // ������ � ������
  while(AdrRecord)
  { // ���� ������ �� ��������� ������ � ��������
    if(AdrRecord->chan == chan)
    { // ����� �����
      if(!(AdrRecord->pxPrevious))
      {
        AdrOutRecord=AdrRecord->pxNextRec;// ��, ����� ����������
        if(AdrOutRecord) AdrOutRecord->pxPrevious=0;
        ListRecord.pxHead=AdrOutRecord;
        vPortFree(AdrRecord);
        return true;
      }
      else
      {
        AdrOutRecord =AdrRecord->pxNextRec;
        AdrInRecord->pxNextRec=AdrOutRecord;
        AdrOutRecord->pxPrevious=AdrInRecord;
        vPortFree(AdrRecord);
        return true;
      }
    }
    else
    { // �� �����, ���� ����������
      AdrInRecord=AdrRecord;
      AdrRecord=AdrRecord->pxNextRec;
    }
  }
  return false;
}

unsigned char GetRecordTimeStamp(unsigned char *pBUF)
{ // �������� ������ �������������
  *((unsigned int *)pBUF)=xTaskGetTickCount();
  return sizeof(unsigned int);
}

//--------------------------------------------------------------------------//
// �������� ������� ������ �������� ������ �� ������						//
//
// block - ������� ���������� ������
// 0 - �������������� ���������
// 1 - ������������� - �������� �����
// 2 - �������������� ��� ������� ������� � ����������
//--------------------------------------------------------------------------//
unsigned int InitRecord(unsigned char typechan, unsigned char chan0, unsigned char Type, 
                        unsigned char block, unsigned char *IP_Addr, unsigned short Port,
                        unsigned int SSRC)
{
  unsigned char chan;
  xListRecord *AdrInRecord, *AdrOutRecord, *AdrRecord;
  unsigned int RecTime;
  unsigned short i, SaveChan;

  if((!Port) || !(IP_Addr[0] || IP_Addr[1] || IP_Addr[2] || IP_Addr[3]))
  {
{
  SendStr2IpLogger("$DEBUG$InitRecord ERROR: Ip=0x%02x%02x%02x%02x, port=%d, chan=%d, block=%d",
                   IP_Addr[0],IP_Addr[1],IP_Addr[2],IP_Addr[3],Port,chan0,block);
}
    return false;
  }
  SaveChan=((unsigned short)chan0 + 101) << 8; // ��� ��������� ������ ������ �������� ��� ����������
  chan=CalcRealChannel(typechan,chan0);
SendStr2IpLogger("$DEBUG$InitRecord: Ip=0x%02x%02x%02x%02x, port=%d, chan0=0x%x (real=%d), block=%d",
                 IP_Addr[0],IP_Addr[1],IP_Addr[2],IP_Addr[3],Port,chan0,chan,block);
  if(IsRecordChannel(chan)) return false; // ����������� ��� ����������
  AdrOutRecord=pvPortMalloc(sizeof (xListRecord));
  if(!AdrOutRecord)
  { ErrMallocRecord++; return 0xFFFFFFFF; }
  else
  {
#define _ AdrOutRecord ->
	  GetRecordTimeStamp((unsigned char *)&RecTime);
	  _ chan=chan;
	  _ NumBuf=0;
	  if(block==0x2) block=0x21;
	  _ block=block;
	  _ Octet12_1=_ Octet12_2=ReversShort(0x9008);
	  _ Number_1=ReversShort(0x0002); _ Number_2=ReversShort(0x0001);
	  _ Timer_1=ReversInt(RecTime + LenSound); _ Timer_2=ReversInt(RecTime);
	  _ IndBuf1=_ IndBuf2=0;
	  if(!SSRC)
	  {
	    SSRC=RecTime ^ ReadClock(); // �������� ���������� �������������� ���������
	    if(SSRC==0xFFFFFFFF) SSRC=SSRC+0xA55A33CC; // ������������� SSRC ���� ����� 0xFFFFFFFF;
	  }
	  _ SSRC_1=_ SSRC_2=SSRC;
	  _ ProFile_1=_ ProFile_2=typechan + SaveChan;
	  _ Len_1=_ Len_2=0;
	  _ pxNextRec=_ pxPrevious=0;
	  _ TypeRecord=Type;
	  memcpy(_ IPRecord, IP_Addr, 4);
	  _ IPPort=Port;
	  AdrInRecord=ListRecord.pxHead;
	  if(!AdrInRecord) ListRecord.pxHead=AdrOutRecord;
	  else
	  { // �� ������ �������
	    do
	    {
	      AdrRecord=AdrInRecord;
	      AdrInRecord=AdrInRecord->pxNextRec;
	    }
	    while(AdrInRecord);
	    AdrRecord->pxNextRec=AdrOutRecord;
	    _ pxPrevious=AdrRecord;
	  }
#undef _
  }
  return AdrOutRecord->SSRC_1;
}

bool IsSelectorRecord(unsigned char SN)
{
  bool bRes;
  unsigned char wChan;

  vPortEnableSwitchTask(false);
  wChan=_xGetSelectorCP(SN);
  if(wChan)
  {
    wChan=CalcRealChannel(1,wChan-1);
    bRes=IsRecordChannel(wChan);
  }
  else bRes=false;
  vPortEnableSwitchTask(true);
  return(bRes);
}

unsigned char InitPlay(unsigned char TypeChan,unsigned char NumbChan,unsigned char *SSRCadr)
{ // ������ �������������������� �� ������
  xListPlay *AdrInPlay, *AdrOutPlay, *AdrPlay;
  unsigned int IP;
  unsigned char i, j;
  unsigned short chan;

  if(TypeChan) TypeChan^=3; // ������ ��� ����� ����������� � ������ �� �������� ��� ��������
  chan=CalcRealChannel(TypeChan,NumbChan);
  if(IsPlayChannel(chan)) return false;
  if(AdrOutPlay=pvPortMalloc(sizeof (xListPlay)))
  {
    if(AdrOutPlay->AdrBufPlay=pvPortMalloc(sizeof (xBufPlay)))
    { // ������������� ������ ��������
#define _ AdrOutPlay ->
      _ ChanPlay=chan;
      _ NewAudioData=_ NumBufPlay=_ IndBufPlay=_ NumOutPlay=_ SyncTime=0;
      _ NumOutPlay=LenPlay / 2;
      _ AdrBufPlay->TimerIn=_ AdrBufPlay->TimerOut=_ AdrBufPlay->DeltaTimer=0;
      _ pxNextPlay=_ pxPrevious=0;
      memcpy(&(_ SSRC),SSRCadr,4);
SendStr2IpLogger("$DEBUG$InitPlay: chan=%x (real=%d)",NumbChan,chan);
      for (i=0;i<LenSound;i++)
        for (j=0;j<LenPlay;j++) _ AdrBufPlay->BufPlay[i][j]=a_compress(0);
      for (i=0;i<LenSound;i++)
        for (j=0;j<2;j++) _ BufPlay[i][j]=a_compress(0);
#undef _
      AdrInPlay=(xListPlay *) ListPlay.pxHead;
      if(!AdrInPlay) ListPlay.pxHead=AdrOutPlay;
      else
      {
        while(AdrInPlay)
        { // ����� ������ ���������� ��������
          AdrPlay=AdrInPlay;
          AdrInPlay=(xListPlay *) AdrInPlay->pxNextPlay;
        }
        AdrPlay->pxNextPlay=AdrOutPlay;
        AdrOutPlay->pxPrevious=AdrPlay;
      }
      return true;
    }
    else
    { vPortFree(AdrOutPlay); return 0xFF; }
  }
  else 
  { ErrMallocPlay++; return 0xFF;}
}

unsigned char ExitPlay(unsigned char TypeChan,unsigned char chan0)
{ // ����������� �������������������� �� ������
 xListPlay  *AdrInPlay, *AdrPlay, *AdrOutPlay;
 unsigned short i;
 unsigned short chan;

 if(!ListPlay.pxHead) return false;
 if(chan0==0xff)
 {
SendStr2IpLogger("$DEBUG$ExitPlay error: chan0=0xff");
   return false;
 }
 if(TypeChan) TypeChan^=3; // ������ ��� ����� ����������� � ������ �� �������� � ��������
 chan=CalcRealChannel(TypeChan,chan0);
SendStr2IpLogger("$DEBUG$ExitPlay: chan0=0x%02x (real chan=%d)",chan0,chan);
 AdrInPlay=AdrPlay=(xListPlay *) ListPlay.pxHead;
 do
 {
  if(AdrPlay->ChanPlay == chan)
  { // ����� �����
   if(AdrPlay->pxPrevious)
   { // �� ������
     AdrOutPlay=(xListPlay *) AdrPlay->pxNextPlay; // ����� ����������
     AdrInPlay->pxNextPlay=AdrOutPlay; // ����������->�� ���������
     AdrOutPlay->pxPrevious=AdrInPlay; // ���������->�� ����������
     vPortFree((void *) AdrPlay->AdrBufPlay);
     vPortFree(AdrPlay);
     return true;
   }
   else
   { // ������ ���� ���������������
     AdrOutPlay=(xListPlay *) AdrPlay->pxNextPlay; // ����� ����������
     if(AdrOutPlay) AdrOutPlay->pxPrevious=0;
     ListPlay.pxHead=AdrOutPlay;
     vPortFree((void *)AdrPlay->AdrBufPlay);
     vPortFree(AdrPlay);
     return true;
   }
  }
  else
  {
    AdrInPlay=AdrPlay; // �� �����, ���� ����������
    AdrPlay=(xListPlay *) AdrPlay->pxNextPlay;
  }
 }
 while(AdrPlay);
 return 0xFF;
}

//--------------------------------------------------------------------------//
//  ����� ���������� �� SPORT1 ��� ������ ��� (KI==0)					    //
//  ��� ������������ ���������� ��                                          //
//--------------------------------------------------------------------------//
unsigned char FoundSport1(unsigned char KI)
{
  unsigned short i,len;

  if(KI) return KI;
  if(ParamUKC[6] & 0x63) len=LenSport1 / 2; // ��������� ������� ������� ���
  else len=LenSport1; // ��� ������� ��� - ��� ���������� �� Sport1 ��������
  i=1; // ����� ���������� �� ������� � 1-�� ��� ��� (�� �������� ������ 0-� � 16-� ��)
  while(TablSport1[i] && (i<len)) 
  {
    i++;
    if(i==16) i++; // 16KI Sport1 ��� ��� �������� ������!
  }
  if(i>=len) return 0;
  return i;
}

void ClearTablCommut(void)
{ // ������� ������� ����������
  unsigned short i,j;
  for(i=0;i<LenVirt;i++)
  { // ��1 ������ ��� ������� ���������, ���� LenKeep!=0
    if(!LenKeep || i) VirtualPoints[i]=0;
  }
  for(i=0;i<LenSumChan;i++) TablSum[i]=TablBlock[i]=0;
  for(i=0;i<LenSport1;i++) TablSport1[i]=0;
  memset(MasGroupInfo,0,sizeof(MasGroupInfo));
  for(i=0;i<LenMKC;i++)
    for(j=0;j<LenCommutMKC;j++) TablMKC[i][j]=0xFF;
  // ������� ���������� ���
  WriteE1(0,0xCD,0x04);
  WriteE1(1,0xCD,0x04);
  WriteE1(2,0xCD,0x04);
  WriteE1(3,0xCD,0x04);
}

//--------------------------------------------------------------------------//
// ������� ��������� ����� ����������� � �� �������, ���� ����������
// busy - ������� ������������� ������� �����
//--------------------------------------------------------------------------//
unsigned char FoundTP(unsigned char chan,unsigned char busy)
{
  if(VirtualPoints[chan]) return true;
  { // ����� ����� ����������� � ������� ���������� (�� ����� ���� ������ ���������� �����������)
    unsigned short i,num,chanR;
    chan+=LenHard;        // ������ ��
    chanR=chan+LenVirt;   // �������� ��
    i=0; 
    while(i<LenSumChan)
    {
      num=TablSum[i]; // ����� ���������������� �������
      if(!num) break; 
      while(num)
      {
        i++;
        num--;
        if((chan==TablSum[i]) || (chanR==TablSum[i]))
        {
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$FoundTP: TP=%d,TP_R=%d,i=%d,TablSum[i]=%d",
          chan,chanR,i,TablSum[i]);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
          return(true);
        }
      }
    }
    if(busy) VirtualPoints[chan-LenHard]=true; 
  }
  return(false);
}

unsigned char _xGetCP(void)
{
  unsigned char i;
  i=LenVirt;
  do
  { 
    i--;
    if(!FoundTP(i,true)) return(i+1);
  }
  while(i);
  return(0);
}

bool _xFreeCP(unsigned char btCP)
{
//SendStr2IpLogger("$DEBUG$_xFreeCP: CP=%d",btCP);
  if(!btCP || (btCP>LenVirt)) return(false);
  btCP--;
  if(VirtualPoints[btCP])
  {
    VirtualPoints[btCP]=0;
    return(true);
  }
  return(false);
}
  
unsigned char FoundChanCommut(unsigned char chan)
{ // ����������� ������� �� (������ � ��������) � ������� ����������: 1-����, 0-���
  unsigned short i,num,savenum;
  unsigned char result;

  i=0;
  while(i<LenSumChan)
  {
    savenum=i++;
    num=TablSum[savenum]; // ����� ���������� �������
    if(!num) return 0;
    while(num)
    {
      result=0;
      if(chan==TablSum[i]) result=1;
      else
        if(chan>=LenHard)
        { // ����� ������ � �������� ��
          if((chan>=(LenHard+LenVirt)) && ((chan-LenVirt)==TablSum[i])) result=2; 
          else
            if((chan+LenVirt)==TablSum[i]) result=3;
        }
      if(result && (TablBlock[savenum]==0x55)) return 1;
      else
        if(result && (TablBlock[savenum]==0xAA) && ((savenum+1)==i)) return 1;
        else
          if(result && (TablBlock[savenum]==0x00)) return 1;
          else
          { i++; num--; }
    }
  }
  return 0;
}
  
unsigned short StrToWord(unsigned char *Adr, unsigned char len)
{ // ������ ��������� ������ (2 ��� 3 �������)
  if(len==3) return ((Adr[0]-'0')*100) + ((Adr[1]-'0')*10) + (Adr[2]-'0');
  else
    if(len==2) return (((Adr[0]-'0') & 0x7)*10) + (Adr[1]-'0');
    else return 0;
}
 
unsigned char FoundAdrGroup(unsigned char *Adr)
{ // ����� ������ ������ � ������ ���� �����
  unsigned char IndName;
  for(IndName=0;IndName<MaxGroup;IndName++) 
  {
    if(!strncmp((char *)MasGroupInfo[IndName].NameGroup,(char *)Adr,LenNameGroup)) return(IndName+1);
  }
  return 0; 
}

unsigned char HighBitChanE1 (unsigned char potok)
{ // ���������� ������� ��� ������ ������ ������ �1 �� ������ ������
  return (((potok-1)%4)<<5)+0x40;
}

// ������ ��������� ������ ���������� �������
/*
unsigned char ReadTransit(unsigned char *Adr0)
{ //!!! � ����������� �������� ������ ���� !!!
  unsigned char data,data1,count,AdrMod,
                *Adr;
  unsigned short Chan, Chan1; // ����� � ������ ������ �1

  Adr=Adr0+8;
  count=StrToWord(Adr,3); // ����� ������� � ������
  if(count==2)
  { // ���������� ������� ����� ���� ������ 2
    data=StrToWord(Adr+3,2) & 0x7F; // ���������� ��� 1-�� ������
    if((data<=16) && data) 
    {
      data1=StrToWord(Adr+7,2) & 0x7F; // ���������� ��� 2 - �� ������
      if((data1<=16) && data1)
      { // ��� ������ ��������
        if(((data-1)/4)==((data1-1)/4))
        { // ��� ��������� � ����� ������, �� ������ ������ �� �������
          Chan=StrToWord(Adr+5,2)+HighBitChanE1(data); // ����� 1 - �� ������
          Chan1=StrToWord(Adr+9,2)+HighBitChanE1(data1); // ����� 2 - �� ������
          AdrMod=data>>2;
          MassOutData[21]=Chan;
          MassOutData[22]=Chan1; // ��������� ������ ���������� �������
          //RemoveE1[0]=2; 
          //if((ReadE1(AdrMod,Chan)==1) && (ReadE1(AdrMod,Chan1)==1))
          return true;
        }
      }
    }
  }
  return false;
}*/

void _xDelCommuteGroup(unsigned short DelGroup)
{ // �������� ������ ����������
  unsigned short i,j;
  unsigned int dwIRQ=cli();

  j=DelGroup; // ������ ���������� �������
  i=TablSum[j]+DelGroup+1; // ������ ���������� ������� 
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$_xDelCommuteGroup: j=%d, i=%d",
          j,i);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
  while(i<sizeof(TablSum))
  { // ������ ������ ����������
    TablSum[j]=TablSum[i];
    TablBlock[j]=TablBlock[i];
    CopyTablBlock[j]=CopyTablBlock[i];
    TablSum[i]=TablBlock[i]=CopyTablBlock[i]=0;
    j++; i++;
  }
  sti(dwIRQ);
}

unsigned char ExclGroup(unsigned char *Adr0)
{ // �������� ������ ���������� �������
  unsigned short i,IndName,index,len,count,NMod,Chan,Chan1;

  IndName=FoundAdrGroup(Adr0); // ������ ����� ������
  if(!IndName)
  { // ��� �� �������, ��������� �� ���������� ������
    /*if(ReadTransit(Adr0)) return true;
    else */return false;
  }
  i=IndName;
  IndName--;
  index=MasGroupInfo[IndName].wOff; // ������ � ������� ����������
  len=TablSum[index]+1; // ����� ���������� �������
  while(i<MaxGroup)
  { // ������ ������� ����
    MasGroupInfo[IndName]=MasGroupInfo[i];
    MasGroupInfo[IndName].wOff-=len;
    IndName++; i++;
  }
  MasGroupInfo[IndName].NameGroup[0]=0;
  MasGroupInfo[IndName].wOff=0;
  IndName=index;
  if(ParamUKC[0] & 0xF000)
  { // ���� ������ ���
    count=TablSum[index++]; // ����� �������
    for(i=0;i<count;i++,index++)
    { // ����� ������� SPORT1
      Chan=TablSum[index];
      if(Chan && (Chan<LenSport1))
      { // ����� SPORT1 (0-� ����� SPORT1 ���������. ������: � ���� ��� �� ���, � ���???)
//        if((ParamUKC[6] & 0x01) && (Chan>=LenSport1/2)) continue; // ����� ����������� ���2.�
        NMod=TablSport1[Chan] >> 8; // ����� ������ �1
        Chan1=(TablSport1[Chan] & 0xFF) + HighBitChanE1(NMod); // ����� ���������� ���������
        TablSport1[Chan]=0;
        NMod--; NMod >>= 2; // ����� ������ �1
        WriteE1(NMod,Chan,Chan1);
        WriteE1(NMod,Chan1,Chan);
      }
    }
  }
  _xDelCommuteGroup(IndName);
  return true;
}

//--------------------------------------------------------------------------//
// ���������� ������ ����������
// ��� KI!=0 - ������� ������, ��� KI==0->KI SPORT1
//--------------------------------------------------------------------------//
unsigned char InclGroup(unsigned char *Adr0, unsigned char KI)
{
  unsigned char *Adr,AdrMod;
  unsigned char NAnalog, NCifra, NModul, NVirt, Sport1;
  unsigned char count,Chan,Chan1;
  unsigned short i,index,data,IndName;
  unsigned int dwIRQ;

  IndName=FoundAdrGroup(Adr0); // ������ ����� ������
  if(IndName) return 253; // ��� �������, �� ������� ������
  Adr=Adr0+LenNameGroup+1; // ������ �� ����, ������� ����� 
  count=StrToWord(Adr,3); // ����� ������� � ������
  Adr+=3;

  // ����������� ������������� SPORT1. �� ���������, ����:
  // 1. ����� ������� ��� > 2
  // 2. 2 ������ ���, �� ��� �� ������ �������
  // 3. ����� ��� � ���������� ����� �/��� ����� �����������
  // ����� ������� Sport1 ������ ���� ����� ����� �������� ������� ???
  NAnalog=NCifra=NVirt=0;
  NModul=Sport1=0;
  for(i=0;i<count;i++)
  { // ������������� ��� ������
    if(*(Adr+1) == 'C') NVirt++; // ����������� �����
    else
    {
      data=StrToWord(Adr,2); // ���������� ��� ������
      if(!data) NAnalog++; // ���������� �����
      else
      { // �������� �����
        NCifra++;
        data--;
        if((NCifra==2) && (NModul!=data/4)) Sport1=true; // 2 ������ �� ������ ������ E1 - ��������� SPORT1
        NModul=data / 4; 
      }
    }
    Adr+=4;
  }
  if(NCifra>2) Sport1=true;
  else
    if(NCifra && (NAnalog || NVirt)) Sport1=true;

  if(Sport1 || NAnalog || NVirt)
  { //------ !! �������� �� ���������� ��� ������ ���������� �������
    IndName=0;
    while(MasGroupInfo[IndName].NameGroup[0] && (IndName<MaxGroup)) IndName++; // ����� ���������� ����� � ������� ���� �����
    if(IndName>=MaxGroup) return 252; // �������, ���� ��� �����
    Adr=Adr0;
    for(i=0;i<LenNameGroup;i++) MasGroupInfo[IndName].NameGroup[i]=*Adr++; // ��������� ��� ������
  }
  index=0;
  while((i=TablSum[index]) && (index<sizeof(TablSum))) index+=i+1; // ����� ���������� ����� � ������� ����������
  if(index+count+1>=sizeof(TablSum))
  { // ��� �����
    MasGroupInfo[IndName].NameGroup[0]=0;
    return 251;
  }
  Adr=Adr0+LenNameGroup+1+3; // ���������� ������������ ������ � ����� ������� � ������
  for(i=1;i<=count;i++)
  { // ������������� ��� ������
    Chan=StrToWord(Adr+2,2);
    if(Adr[1]=='C')
    { // ����������� �����
      if(((Adr[0]-'0') & 0x1)==0x1)
      { // ���������� ����������� �����
        TablSum[index+i]=Chan+LenHard+LenVirt-1;
      }
      else
      { // ����� ������� ����������� �����
        TablSum[index+i]=Chan + LenHard - 1;
      }
    }
    else
    {
      data=StrToWord(Adr,2) & 0x7F; // ��� ������
      if(!data)
      { // ���������� �����
        if(!Chan) TablSum[index+i]=0; // ����������� �����
        else TablSum[index+i]=Chan+LenSport1-1; 
      }
      else
      { // �������� �����
        AdrMod=(data - 1) >> 2; // ���������� ����� ������
        Chan+=HighBitChanE1(data); // ���������� ������ �����
        if(!Sport1)
        { // ���������� �� ������ ���
          Chan1=StrToWord(Adr+6,2) + HighBitChanE1(StrToWord(Adr+4,2) & 0x7F);
          if((ReadE1(AdrMod,Chan)==1) && (ReadE1(AdrMod,Chan1)==1)) 
          { // ������ ������� ���, �������
            WriteE1(AdrMod,Chan,Chan1);
            WriteE1(AdrMod,Chan1,Chan);
            MasGroupInfo[IndName].NameGroup[0]=0;
            return 200; 
          }
          else
          { // ���������� �� ��� ���������� (��� ����� �������)
            MasGroupInfo[IndName].NameGroup[0]=0;
            return 250;
          }
        }
        else
          if(Chan1=FoundSport1(KI))
          { // ���������� ���������� ����� Sport1, ������� ��������� �����
            TablSum[index+i]=Chan1;
            TablSport1[Chan1]=(Chan & 0x1F) + (data<<8); // �������� ����� Sport1
            if(!KI)
            { // ������ ���
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$InclGroup: AdrMod=%d, Chan=%d, Chan1=%d",
          AdrMod,Chan,Chan1);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
              WriteE1(AdrMod,Chan,Chan1);
              WriteE1(AdrMod,Chan1,Chan);
            }
          }
          else
          { // ��� ��������� ������� SPORT1
            MasGroupInfo[IndName].NameGroup[0]=0;
            return 249;
          }
      }
    }
    // ���������� ������� ����������� �������
    if(Adr[0]-'0'>7) CopyTablBlock[index+i]=TablBlock[index+i]=0; 
    else CopyTablBlock[index+i]=TablBlock[index+i]=0xFF;
    Adr+=4;
  }
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$InclGroup: index=%d, count=%d",
          index,count);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
  MasGroupInfo[IndName].wOff=index; // ������ � �������� TablSum � TablBlock
  dwIRQ=cli();
  TablSum[index]=count; // ����� ������� � ������
  sti(dwIRQ);
  return true;
}

char *ReadFileName(char *ext)
{ // ������ ����� ����� �� MassInData � ���������� ����������
  unsigned char i, j;

  i=0;
  while((FileName[i]=MassInData[i+2]) && (i<LenFileName-4)) i++; // ������ ��� �����
  for(j=0;j<4;j++) FileName[i++]=*ext++; // ��������� ���������� ����� (4 �����)
  while(i<LenFileName) FileName[i++]=0;  // �������� ���������� ����� �����
  return FileName;
}

//--------------------------------------------------------------------------//
// ������ ������ ���������� �� Flash �� ����� ������ � Adr			//
//		��� SetFail==true - �������� �����							//
//		��� KI==0 - ������� ������, KI!=0 - KI SPORT1				//
// !! ��� ������ ����� �� ������ Adr �� ������������ !!
//--------------------------------------------------------------------------//
unsigned char SaveGroupFlash(unsigned char *Adr, unsigned char SetFile, unsigned char KI)
{
  unsigned short i, j, len;
  HANDLE hFileOut;
  MYFILE stFile;

  len=(((*(Adr+LenNameGroup+1+2)-'0')*100)+
       ((*(Adr+LenNameGroup+1+3)-'0')*10)+(*(Adr+LenNameGroup+1+4)-'0'))*4+3;   // ����� ������������ ����� � ������
  for (i=0;i<len*4;i++)
  { // ����������� ������ ��� ������������ ����� �����������: �(���)=�(���)=c(���)=C(���)
    if((*(Adr+i+LenNameGroup+1+2) == 0x63) || (* (Adr+i+LenNameGroup+1+2) == 0xD1) ||
       (*(Adr+i+LenNameGroup+1+2) == 0xF1) || (*(Adr+i+LenNameGroup+1+2) == 'C')) *(Adr+i+LenNameGroup+1+2)='C';
  }

  if(SetFile)
  { // ��������� ���������� ����� (4 �����)
    switch(ParamUKC[3])
    {
      case 0: ReadFileName(ConstCom);  break;
      case 1: ReadFileName(ConstCom1); break;
      case 2: ReadFileName(ConstCom2); break;
      case 3: ReadFileName(ConstCom3); break;
    }
    if(FindFile(FileName,&stFile)!=(unsigned)-1) return 255; // ����� ���� ��� ����
    else
    {
      hFileOut=CreateFile(FileName, CREATE_ALWAYS, len);
      if(hFileOut<0) return 254;
      else
      {
        WriteFile(hFileOut,Adr+LenNameGroup+1+2, len);
        CloseHandle(hFileOut);
      }
    }
  }
  i=InclGroup(Adr+2,KI);
  if(i!=1) i=0;
  return i;
}

// ������ ������� ���������� ������ �������
unsigned char ReadGroup(unsigned char *Adr)
{
 unsigned short i,j,virt,number[4];
 unsigned short count,data,data1,index,index1;
 unsigned char *pbtBuf;
 section("bigdata") static sInfoKadr sInfoCommut;

 index=FoundAdrGroup(Adr); // ����� ������ � ������ ���� �����
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$ReadGroup: group=%s, ind=%d",
          Adr,index);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
 if(!index)
 { // ��� ������
/*   if(ReadTransit(Adr))
   { // ���������� true ���� ��� ���������� ������
     //!!!! ��� �� ��������
     return true;
   }
   else*/
   { // ������ ���� ��� ������
     return false;
   }
 }
 sInfoCommut.Source=USER;
 sInfoCommut.A1=A1;
 sInfoCommut.PORT1=PORT1;
 sInfoCommut.L=0;
 sInfoCommut.ByteInfoKadr[0]=11;
 sInfoCommut.ByteInfoKadr[1]=112;
 pbtBuf=sInfoCommut.ByteInfoKadr+2;
 memset(pbtBuf,0,LenNameGroup);
 strncpy((char *)pbtBuf,(char *)Adr,LenNameGroup);
 index=MasGroupInfo[index-1].wOff; // ������ � ������� ����������
 data=count=TablSum[index++]; // ����� ������� ������ ������
 pbtBuf[LenNameGroup]=(data/100)+'0';
 data=data-((data/100)*100); // ������ ����� �������
 pbtBuf[LenNameGroup+1]=(data/10)+'0';
 data=data-((data/10)*10);
 pbtBuf[LenNameGroup+2]=data+'0';
 index1=LenNameGroup+3; // ������ ������� ��� ������ ������� �������
 for(i=1;i<=count;i++)
 {
   virt=0;
   data=TablSum[index];
   if(data<LenSport1)
   { // ����� SPORT1
     data=TablSport1[data];
     data=(data>>8)*100+(data & 0xFF);
   }
   else
   {
     if(data<LenHard) data-=LenSport1-1; // ���������� �����
     else
     { // ����� �����������
       data-=LenHard-1;
       if(data<=LenVirt) virt=2; // �������� ����� �����������
       else  
       { // ������ ����� �����������
         virt=1; data-=LenVirt;
       }
     }
   }
   data1=1000; 
   for(j=0;j<4;j++)
   {
     number[j]=(data/data1)+'0'; 
     data=data-((data/data1)*data1);
     data1/=10;
   }
   if(virt)
   {
     number[0]='0'+virt;
     number[1]='C';
   }
   if(!TablBlock[index++])
   {
     if(number[0]=='2') number[0]='A';
     else number[0]+=8;
   }
   for(j=0;j<4;j++,index1++) pbtBuf[index1]=number[j];
   if(i==50)
   { // ����.����� ��������� ���������� ������ � IP-������ ==50
     sInfoCommut.L=214+2;
     xQueueSend(xQueueReply[USER],&sInfoCommut,0,0);
     i=0; count-=50; index1=1; // ��� ������������ ������� ������ ���������� ������
   }
 }
 pbtBuf[index1]=0;
 if(sInfoCommut.L)
 { // ����� ��������� ���������� ������ >=50
   if(count)
   { // ������ IP-����� ��������� ���������� ������
     j=count*4;
     pbtBuf[0]=j;
     sInfoCommut.L=j+3;
     xQueueSend(xQueueReply[USER],&sInfoCommut,0,0);
   }
 }
 else
 { // ����� ��������� ���������� ������ <50
   sInfoCommut.L=(count*4)+LenNameGroup+3+2;
   xQueueSend(xQueueReply[USER],&sInfoCommut,0,0);
/*{
  static char NameGroup[30];
  LockPrintf();
  memset(NameGroup,0,sizeof(NameGroup));
  strncpy(NameGroup,(char *)(sInfoCommut.ByteInfoKadr+2),LenNameGroup);
  sprintf(strInfoSpXCmd,"$DEBUG$ReadGroup: L=%d, Name=%s, commut=%s",
          sInfoCommut.L,NameGroup,sInfoCommut.ByteInfoKadr+2+LenNameGroup);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
 }
 return true;
}
  
unsigned char FoundChanGroup(unsigned char IndName,unsigned char *Adr)
{ // ����� ������ � ������
  unsigned short i,data,len,chan,index;

  for(i=0;(i<LenNameGroup) && (*Adr);i++,Adr++);
  data=(Adr[3]-'0')*10 + Adr[4]-'0'; // ���������� ����� ������
  if(Adr[2]=='C')
  { // ����������� �����
    chan=data+LenHard-1;
    if(Adr[1]=='A') Adr[1]='2';
    if((Adr[1]-'0') & 0x1) chan+=LenVirt;
  }
  else
  {
    i=StrToWord(Adr+1,2) & 0x0F; // ������� ���� ������
    if(i) chan=data+(i<<8); // �������� �����
    else chan=data+LenSport1-1; // ���������� �����
  }
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$FoundChanGroup: Chan0=%c%c%02d, chanReal=%d",
          Adr[1],Adr[2],data,chan);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
  index=IndName; // ������� ������ ������ �������
  len=TablSum[index++]; // ����� ������� � ������
  i=0;
  if(chan<0x0100)
  { // ���������� ����� ��� ��
    while(i<len)
    {
      if(TablSum[index++]==chan) return index;
      i++;
    }
  }
  else
  { // ����� ���
    while(i<LenSport1)
    {
      if(TablSport1[i]==chan) break;
      i++;
    }
    if(i<LenSport1)
    { // ����� ����� SPORT1
      chan=i; i=0;
      while(i<len)
      {
        if(TablSum[index++]==chan) return index;
        i++;
      }
    }
  }
  return 0;
}

unsigned char BlockInput(unsigned char *Adr,unsigned btState)
{ // ����������/������������� ����� ������ � ������
  unsigned char btRes;
  unsigned short ind,chan;

  btRes=0; // ���� ������
  ind=FoundAdrGroup(Adr); // ������ ����� ������
  if(ind)
  { // ������ �������
    ind=MasGroupInfo[ind-1].wOff;
    chan=FoundChanGroup(ind,Adr); // ����������� �������������� ������ � ������� TablSum
    if(!chan &&
       (Adr[0]=='T') && (Adr[1]=='r') && (Adr[2]=='a') && (Adr[3]=='n') && (Adr[4]=='s') && (Adr[5]=='i') && (Adr[6]=='t'))
    { // �������� �� ����� ������ Transit
      chan=ind+2;
    }
    if(chan)
    { // ����� ������
      chan--;
      CopyTablBlock[chan]=TablBlock[chan]=btState;
      btRes=true;
    }
  }
  return btRes;
}

unsigned char DelChanFromGroup(unsigned char *Adr)
{ // �������� ������ �� ������
 unsigned char btRes;
 unsigned short ind,chan,chan1;

 btRes=0;
 ind=FoundAdrGroup(Adr); // ������ ����� ������
 if(ind)
 { // ������ �������
   ind=MasGroupInfo[ind-1].wOff;
   ind=FoundChanGroup(ind,Adr);
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$DelChanFromGroup: Group=%s, ind=%d",
          Adr,ind);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
   if(ind)
   { // ����� ������
	 ind--;
	 chan=TablSum[ind];
	 if(chan<LenSport1)
	 { // ������ SPORT1 - ����������������
	   chan1=TablSport1[chan];
	   WriteE1(chan1>>10,chan,0xFF); 
	   WriteE1(chan1>>10,(chan1&0x1F) + HighBitChanE1(chan1>>8),0xFF);
	   TablSport1[chan]=0;
	 }
	 TablSum[ind]=TablBlock[ind]=CopyTablBlock[ind]=0;
	 btRes=true;
   }
 }
 return btRes;
}

unsigned char AddChan2Group(unsigned char *Adr, unsigned short StInd)
{ // �������� ����� � ������ (StInd - ��������� �������� ������)
  unsigned char btRes;
  unsigned short ind,data,len,i,
                 indexChan,indexSport1,chan,chan1,AdrMod;

  ind=FoundAdrGroup(Adr);  // ������ ����� ������
  if(ind==0) btRes=false;  // �� ������� ������
  else
  { // ������ �������
    btRes=1;
    ind--;
    indexChan=MasGroupInfo[ind].wOff;   // ������ � ������� ����������
    len=TablSum[indexChan++];           // ����� ������� ������
    if(StInd>=len) btRes=0xF0;
    else
    {
      len-=StInd; indexChan+=StInd;
      i=0;
      while(i<len) 
      {
        if(TablSum[indexChan])
        {
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$AddChan2Group: indexChan=%d, TablSum[indexChan]=0x%x",
          indexChan,TablSum[indexChan]);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
          indexChan++; i++;
        }
        else break;
      }
      if(i>=len) btRes=0xFF; // ��� ���������� ������ � ������
      else
      { // ��������� ����� � ������ ������
        for(i=0;i<LenNameGroup;i++)
        {
          if(*Adr) Adr++;
          else break;
        }
        chan=StrToWord(Adr+3,2); // ���������� ����� ������
        if(Adr[2]=='C') 
        { // ����������� �����
          if((Adr[1]-'0') & 1) chan+=LenHard+LenVirt-1;
          else chan+=LenHard-1;
          TablSum[indexChan]=chan;
        }
        else 
        { // ������ ��� �����
          data=StrToWord(Adr+1,2) & 0x7F; // ������� ���� ������
          if(data)
          { // �������� �����
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$AddChan2Group: data=%d, chan=0x%x",
          data,chan);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
            chan += data<<8; 
            indexSport1=FoundSport1(0); // ����� ���������� ������ SPORT1
            if(!indexSport1) btRes=0xFE; // ��� ���������� ������ SPORT1
            else
            { // ��������� ����� SPORT1 ������
              TablSum[indexChan]=indexSport1;
              TablSport1[indexSport1]=chan; 
              AdrMod=chan>>10;
              chan1=(chan & 0x1F) + HighBitChanE1(chan >> 8);
              WriteE1(AdrMod,chan1,indexSport1);
              WriteE1(AdrMod,indexSport1,chan1);
            }
          }
          else
          { // ���������� �����
            chan+=LenSport1-1;
            TablSum[indexChan]=chan;    
          }
        } 
        if(btRes==1)
        {
          if((Adr[1]-'0')<7) TablBlock[indexChan]=0xFF; // ����� �������������
          else TablBlock[indexChan]=0; // ����� ������������
          CopyTablBlock[indexChan]=TablBlock[indexChan];
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$AddChan2Group: indexChan=%d, wChan=0x%x, TablSum[indexChan-1]=0x%x",
          indexChan,chan,TablSum[indexChan-1]);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
        }
      }
    }
  }
  return btRes;
}

unsigned char InclTempComm(unsigned char chan1,unsigned char chan2,unsigned char No55)
{ // �������� ��������� ���������� ������� � ���� �������
  unsigned short i,len,i1,len1,chan01,chan02;

  i=0;
  while((i<(LenSumChan-2)) && (len=TablSum[i]))
  { // ����� ��������� ���������� 2-� �������
    chan01=TablSum[i+1];
    chan02=TablSum[i+2]; 
    if((len!=2) || ((TablBlock[i]!=0x55) && (TablBlock[i]!=0xAA))) i=i+len+1;
    else 
    { // ��������� ���������� 2-� ������� (����� ������� 0x55 (0xAA) � TablBlock)
      if(((chan01==chan1) || (chan01==chan2)) && 
         ((chan02==chan1) || (chan02==chan2)))
      { // ������ ������� ���������
        // ������������� ����� ������ ���������
        if(chan1==chan01) TablBlock[i+2]=0xFF;
        else TablBlock[i+1]=0xFF;
        return true; // ��������� ���������� ��� ���� - �������
      }
      else i=i+len+1;
    }
  }
  i1=0;
  while(len1=TablSum[i1])
  { // ����� ������� ���������� ����������
    i1++; 
    while(len1)
    {
      if((chan1==TablSum[i1]) || (chan2==TablSum[i1])) // ��������, ��� �� ������ ������ � ������� ���� ����������
      { // ����� �����, ���� ������
        i1++; len1--; 
        while(len1) 
        {
          if((chan1==TablSum[i1]) || (chan2==TablSum[i1])) return true;  // ���������� ����� �������� ���� - �������
          i1++; len1--;
        }
      } 
      else 
      { i1++; len1--; }
    }
  }
  if((!len) && (i<(LenSumChan-3)))
  { // ��� ���������� ����� �������� - �������, ���� ���� �����
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$InclTempComm: chan1=%d, chan2=%d, i=%d",
          chan1,chan2,i);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
    TablSum[i]=2;
    TablSum[i+1]=chan1;
    TablSum[i+2]=chan2;  
    if(No55) TablBlock[i]=0xAA;
    else TablBlock[i]=0x55;
    TablBlock[i+1]=0x00;
    TablBlock[i+2]=0xFF;
    return true;
  }
  return false;
}

unsigned char ExclTempComm(unsigned char chan1,unsigned char chan2)
{ // �������� ��������� ���������� �������
  unsigned short i, j, len;
  unsigned char chan01,chan02;
  i=0;
  while((i<(LenSumChan-2)) && (len=TablSum[i])) 
  { // ����� �������
    if((len==2) && ((TablBlock[i]==0x55) || (TablBlock[i]==0xAA)))
    { // ��������� ���������� ������ 2-� ������� � ����� ������� 0x55 | (0xAA) � TablBlock
      chan01=TablSum[i+1];
      chan02=TablSum[i+2]; 
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$ExclTempComm: chan1=%d, chan2=%d, chan01=%d, chan02=%d",
          chan1,chan2,chan01,chan02);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
      if(((chan01==chan1) || (chan01==chan2)) && ((chan02==chan1) || (chan02==chan2)))
      { // ������ ������� ���������
        for(j=0;j<MaxGroup;j++)
        {
          if(MasGroupInfo[j].wOff>i) MasGroupInfo[j].wOff-=3; // �������� �������� �����
        }
        _xDelCommuteGroup(i);
        return true;
      } 
    }
    i+=len+1;
  }
  return true;// ��� ����� �������, ���� ������
}

unsigned char FoundDelTempComm(unsigned char type, unsigned char chan)
{ // ����� � �������� ��������� ���������� ������
  unsigned short i, len, chan2;

  if((type==0x80) || (chan==0)) return true; // ��������� �����
  if(!type) chan+=LenSport1-1;
  else chan+=LenHard+LenVirt-1;
  i=0;
  while(len=TablSum[i])
  {
    if((len!=2) || ((TablBlock[i]!=0x55) && (TablBlock[i]!=0xAA))) i+=len+1;
    else
    {
      if(chan==TablSum[i+1])
      {
        chan2=TablSum[i+2];
        ExclTempComm(chan,chan2);
        return true;
      }
      else
        if(chan==TablSum[i+2])
        {
          chan2=TablSum[i+1];
          ExclTempComm(chan,chan2);
          return true;
        }
      i+=3;
    }
  }
  return false;
}

unsigned short ReadAllTempCommute(unsigned char *MasOut)
{ // ����� ���������� (1����)+�� 2 ������ �� ������ ���������� (2 ����� �� ����� - ���+(0x80-���� ��������) � N ������)
  unsigned short i,j,k,ii;

  i=0; j=1;
  MasOut[0]=0;
  do 
  {
    if(((TablBlock[i]==0x55) || (TablBlock[i]==0xAA)) && (TablSum[i]==2)) 
    {
      for(ii=0;ii<2;ii++)
      {
        k=TablSum[++i];
        if(k<LenHard)
        {
          if(!TablBlock[i]) MasOut[j++]=0x80;
          else MasOut[j++]=0;
          MasOut[j++]=k+1;
        }
        else
        {
          if(!TablBlock[i]) MasOut[j++]=0x81;
          else MasOut[j++]=1;
          MasOut[j++]=k-LenHard+1;
        }
        if(j>=d_MaxReplyBufSz-12)
        { // ���� ����� ����� ��������� ����������!
          j>>=2;
          MasOut[0]=j;
          return(j*4+1);
        }
      }
      i++;
    }
    else
    {
      if(TablSum[i]) i+=TablSum[i]+1;
    }
  }
  while((i<LenSumChan) && TablSum[i]);
  if(j<5) j=0;
  MasOut[0]=j >> 2;
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$ReadAllTempCommute: MasOut[0]=%d, j=%d",
          MasOut[0],j);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
  return(j);
}
  
unsigned char ReadTempComm(unsigned char chan)
{ // ������ �������, �������� ������������ � ���������
  unsigned short i, len;
  unsigned char chan01, count, index, type;

//SendStr2IpLogger("$DEBUG$ReadTempComm: started!");

  i=count=0;
  index=1; // ����� �������
  while((i<(LenSumChan-2)) && (len=TablSum[i]))
  {
    if((len==2) && ((TablBlock[i]==0x55) || (TablBlock[i]==0xAA)))
    { // ��������� ���������� ������ ���� ������� � ����� ������� 0x55|0xAA � TablBlock
      if(((chan01=TablSum[i+1])==chan) && (TablBlock[i+2]==0xFF)) 
      { // ����� ������, ����������� ��� ������ �� ����
        count++;
        chan01=TablSum[i+2]; 
        if(chan01<LenHard)
        { chan01=chan01-LenSport1+1; type=0; }
        else
        { chan01=chan01-(LenHard+LenVirt)+1; type=1; }
        MassOutData[index++]=type;
        MassOutData[index++]=chan01;
     }
     else
       if(((chan01=TablSum[i+2])==chan) && (TablBlock[i+1]==0xFF)) 
       { // ����� ������, ����������� ��� ������ �� ����
         count++;
         chan01=TablSum[i+1]; 
         if(chan01<LenHard)
         { chan01=chan01-LenSport1+1; type=0; }
         else
         { chan01=chan01-(LenHard+LenVirt)+1; type=1; }
         MassOutData[index++]=type;
         MassOutData[index++]=chan01;
       }
    }
    i=i+len+1;
  }
  MassOutData[0]=count;
  if(count) return(index+1);
  else return 0;
}

bool ReadInChan(unsigned char type, unsigned char chan)
{ // ������ ������� �������, ������������ � ���������
  unsigned char InChan,i,len,indGroup;
  unsigned short index;
  bool bExist;

//SendStr2IpLogger("$DEBUG$ReadGroup: ReadInChan!");

  if(!type) InChan=chan+LenSport1-1;
  else
    if(type==1) InChan=chan+LenHard-1; 
    else
      if(type==2) InChan=chan+LenHard+LenVirt-1; 
  bExist=false;
  indGroup=0; 
  index=0;
  while((index<LenSumChan) && (len=TablSum[index++]))
  { // ����� ��������� ������
    for(i=0;i<len;i++)
    {
      if(InChan==TablSum[index++]) 
      {
        if(ReadGroup(MasGroupInfo[indGroup].NameGroup)); bExist=true;
      }
    }
    indGroup++;
  }
  return(bExist);
}

char *SetVarComm(void)
{ // ����������� ��������� ���������� ��
  switch(ParamUKC[3])
  {
    case 1: return ConstCom1;
    case 2: return ConstCom2;
    case 3: return ConstCom3;
  }
  return ConstCom;
}

void RestartTempBuf(void)
{ // ����������������� ��������� �������
  unsigned short i,j;

  ParamUKC[1] &= 0x40; // ����������� ������������ ���������
  ParamUKC[2] &= 0x40; // ����������� ������������ SNMP
  j=1;
  for(i=0;i<QueueReplyCnt;i++) // �������������� ��������� ������� ���������
  {
    if(ParamUKC[2] & j)
    {
      if(!AdrTempBufSNMP[i]) 
      {
        AdrTempBufSNMP[i]=pvPortMalloc(sizeof(xListSNMP));// �������� ������
        if(!AdrTempBufSNMP[i]) BlockWatchDog();
        AdrTempBufSNMP[i]->countIn=0;
        AdrTempBufSNMP[i]->countOut=0;
        AdrTempBufSNMP[i]->timer=0;
      }
    }
    else
    {
      if(AdrTempBufSNMP[i])
      { // ������������ ����� �������� ������
        vPortFree(AdrTempBufSNMP[i]);
        AdrTempBufSNMP[i]=0;
      }
    }
    j<<=1;
  }
}

void SoundKeep(unsigned short chan)
{ // ��������� ������� �������� �� ���������
  if(LenKeep)
  {
    unsigned char btChanInd;
    vPortEnableSwitchTask(false);
    btChanInd=CalcRealChannel(chan>>8,(chan & 0xFF)-1);
    InclTempComm(btChanInd,LenHard+LenVirt+LenVirt,0); // ���������� � ��������� �� ��� ������� ���������
    vPortEnableSwitchTask(true);
  }
}

void StopKeep(unsigned short chan)
{ // ���������� ������� �������� �� ���������
  if(LenKeep)
  {
    unsigned char btChanInd;
    vPortEnableSwitchTask(false);
    btChanInd=CalcRealChannel(chan>>8,(chan & 0xFF)-1);
    ExclTempComm(btChanInd,LenHard+LenVirt+LenVirt); // ���������� � ��������� �� ��� ������� ��������� 
    vPortEnableSwitchTask(true);
  }
}

//--------------------------------------------------------------------------//
// �������� ����������������� SPORT0
void RequestRestart6MKA(void)
{ // �������� �� ������������� �������� 6-�� ���� ���
  unsigned int dwInt=cli();

  if(!TimeStopBit)   // �������� �� ���� UART ������ ���
  {
    if(MKAUARTSTATE & 0x000003F)
    { // ���� 1...6 ������ ���
      StopBit=1;
      TimeStopBit=5;
    }
    else
    {
      if(MKAUARTSTATE & 0x0000FC0)
      { // ���� 7...12 ������ ���
        StopBit=2;
        TimeStopBit=5;
      }
    }
  }
  else
  {
    TimeStopBit--;
    if(!TimeStopBit)
    {
      if(StopBit==1) MKAUARTSTATE &= ~0x000003F;
      else MKAUARTSTATE &= ~0x0000FC0;
      StopBit=0;
    }
   }
   sti(dwInt);
}

#define d_MaxWaitChangeTestCode     50

typedef struct
{
  unsigned short wTestCode,wTestCodeNoChage;
  unsigned int dwPauseBeforeTest;
} TEST_MKA_TP;

unsigned char TestSPORT0(unsigned short wMKA)
{
  unsigned int dwMask=cli();
  unsigned short wTestCode=InTestData[wMKA];
  sti(dwMask);
/*  static TEST_MKA_TP sMasMKAAnalize[LenMKA];
  if(sMasMKAAnalize[wMKA].dwPauseBeforeTest)
  {
    if(IsCurrentTickCountGT(sMasMKAAnalize[wMKA].dwPauseBeforeTest))
    {
      sMasMKAAnalize[wMKA].dwPauseBeforeTest=0;
      sMasMKAAnalize[wMKA].wTestCodeNoChage=0;
      sMasMKAAnalize[wMKA].wTestCode=wTestCode;
    }
    return(1);
  }*/
  if((wTestCode==0x2A6A) || (wTestCode==0x5565) ||
     (wTestCode==0x0505) || (wTestCode==0x0A0A) || (wTestCode==0x20A0)) return(1);
  else
  {
    if(wTestCode && (wTestCode<=(0x80+LenSport0))) return(1);
/*{
  static char str[80];
  LockPrintf();
  sprintf(str,"$DEBUG$TestSPORT0: wTestCode=%04x",wTestCode);
  UnlockPrintf();
  SendStr2IpLogger(str);
}*/
/*    sMasMKAAnalize[wMKA].wTestCodeNoChage++;
    if(sMasMKAAnalize[wMKA].wTestCodeNoChage>d_MaxWaitChangeTestCode)
    { sMasMKAAnalize[wMKA].dwPauseBeforeTest=xTaskGetTickCount()+500; }*/
    return(0);
  }
}

void AnalizeMKA(void)
{
  unsigned short i,wMKAInd;
  unsigned char btSPORT0_Ok=0;

  if(ParamUKC[0] & 0x0FFF)
  {
    for(i=0,wMKAInd=1;i<LenMKA;i++,wMKAInd<<=1)
    {
      if(ParamUKC[0] & wMKAInd) btSPORT0_Ok|=TestSPORT0(i);
    }
    if(btSPORT0_Ok==0)
    {
      CountErrSPORT++;
      if(CountErrSPORT>150)
      {
{
  section("bigdata") static char str[80];
  static int iCntRestart=0;
  LockPrintf();
  sprintf(str,"$DEBUG$AnalizeMKA: RestartSPORT0=%d",++iCntRestart);
  UnlockPrintf();
  SendStr2IpLogger(str);
}
        RestartSPORT0();
        CountErrSPORT=0;
      }
    }
  }
}

void Tikker4Selector(void)
{
  unsigned short i,j;
  for(i=0;i<NumberKeyboard;i++)
  {
    j=FoundPlaceSelector(i); // ������ ���������� ��������� � TablSum
    if((j!=0xffff) && Tikker[i])
    { // ���� �������� � �������� ������
      j=TablSum[j+1]; // �������� ����� ����������� ��������� � TablSum. ������ �� ��������� ���� ������!
       // ����������� ������� ��������� �� ����� �����������
      if((TimeTikker[i]<(PeriodTikker-50)) && (YesSpeak[j-LenHard-LenVirt+LenSport0]==1)) TimeTikker[i]=3000;
      if(TimeTikker[i]) TimeTikker[i]--;
      else
      {
/*{
extern char strInfoSpXCmd[];
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$Tikker4Selector: Chan=%d",
          j-LenVirt);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
        GeneratorMU(j-LenVirt,FTik,FTik+1,400,0,min12dB2,min12dB2); // ������ ������� �� ����� �� ���������
        TimeTikker[i]=PeriodTikker; // ������ ����� 15 ���
      }
    }
  }
}

void ARU4CP(void)
{ // ��� �� ��
  unsigned short i;
  for(i=0;i<LenVirt;i++)
  {
    if(ARUTP[i])
    {
      if(Middle[LenSport0+i]>200) 
      {
        if(Middle[LenSport0+i]>6000)
        {
          if(CountARU[i])
          { CountARU[i]--; KInARU[i]*=0.4; }
        } // ���� 0.84
        else
          if(Middle[LenSport0+i]<2500)
          {
            if(CountARU[i]<MaxARUTP[i])
            {
              if(TimARU[i]<16) TimARU[i]++;
              else
              { TimARU[i]=0; CountARU[i]++; KInARU[i]*=1.1904; }
            }
          }
      }
      if(KInARU[i]<1100)
      { KInARU[i]=1000; CountARU[i]=0; }
    }
    else KInARU[i]=1000;
  }
}

void PlaySoundKeep(void)
{
  unsigned short i;
  static unsigned int IndBufKeep=0;

  if(LenKeep)
  { // ������� ���������
    if(NextPlayKeep) 
    {
      for(i=0;i<LenSound;i++)
      {
        BufPlayKeep[i][(NumBufPlayKeep+1) & 1]=a_expand(KeepFile[IndBufKeep++]) << 3;
        IndBufKeep%=LenKeep;
      }
    }
    NextPlayKeep=false;
  }
}

void DelOpovMessageFromPlayList(void)
{
#ifdef SINFO_ENERGO
    { // �������� ��������� ��� ���������� �� ������ ���������������
extern AUDIO_MESSAGE_LIST_TP *g_psAMessList;
      AUDIO_MESSAGE_LIST_TP *psAMess=g_psAMessList,
                            *psPrev=NULL,*psNext;

      while(psAMess)
      {
        psNext=psAMess->psNext;
        if(psAMess->dwACur>=psAMess->dwACnt)
        {
          unsigned int dwIRQ=cli();
          if(psAMess==g_psAMessList) g_psAMessList=psNext;
          else
          {
            if(psPrev) psPrev->psNext=psNext;
          }
          sti(dwIRQ);
          vPortFree(psAMess);
        }
        else psPrev=psAMess;
        psAMess=psNext;
      }
    }
#endif
}

void ProcessListRecord(void)
{
  xListRecord *AdrRecord=ListRecord.pxHead;
  while(AdrRecord)
  {
    if(AdrRecord->block & 0x20)
    { // 0x20 - �������������� �������� (��� ������� ������� � ����������)
      if(FoundChanCommut(AdrRecord->chan))
      {
 DataSave[0]++;
 AdrRecord->block=0x20;
      }
      else AdrRecord->block=0x21;
    }
    AdrRecord=AdrRecord->pxNextRec;
  }
}

void EndSound4Selector(void)
{
  if(ChanWavS && (!StartWavS))
  { // ����� ������������� ��������� �������� �������
    if(ChanWavS>=LenHard) ChanWavS-=LenHard + LenVirt - 0x100;
    else ChanWavS-=LenSport1 - 1;
    MassOutData[0]= ChanWavS>>8; // ��� ������
    MassOutData[1]= ChanWavS & 0xFF; // ����� ������
    MassOutData[2]= EndSound; // ��� �������� - ����� ������ �������
    OutAbon(MassOutData,3);
    ChanWavS=0;
    vPortFree(SaveAdrOutSound);
  }
}

void ProcessMKC(void)
{
  if((ParamUKC[0] & 0x1000) && (FailureE1[0][8] & 0x10) && (FailureE1[0][8]!=0x07)) InitMKC(0);
  if((ParamUKC[0] & 0x2000) && (FailureE1[1][8] & 0x10) && (FailureE1[1][8]!=0x07)) InitMKC(1);
  if((ParamUKC[0] & 0x4000) && (FailureE1[2][8] & 0x10) && (FailureE1[2][8]!=0x07)) InitMKC(2);
  if((ParamUKC[0] & 0x8000) && (FailureE1[3][8] & 0x10) && (FailureE1[3][8]!=0x07)) InitMKC(3);
  // ������ ������� ���������� ���� ���
  if((((TestBoard & 0xC0000000) == 0x80000000) && (ParamUKC[0] & 0x8000)) ||
     (((TestBoard & 0x30000000) == 0x20000000) && (ParamUKC[0] & 0x4000)) ||
     (((TestBoard & 0x0C000000) == 0x08000000) && (ParamUKC[0] & 0x2000)) ||
     (((TestBoard & 0x03000000) == 0x02000000) && (ParamUKC[0] & 0x1000))) LedOn(ledMKC);
  else LedOff(ledMKC);
}

bool IsCommute(unsigned short chan1, unsigned short chan2)
{ // �������� ������� ��������� ���������� �������
	unsigned short i, len;
	unsigned char chan01,chan02;
	
	chan1=CalcRealChannel(chan1>>8,chan1 & 0xFF);
	chan2=CalcRealChannel(chan2>>8,chan2 & 0xFF);
	i=0;
	while ((i<(LenSumChan-2)) && (len=TablSum[i]))
	{
	  chan01=TablSum[i+1]; chan02=TablSum[i+2]; 
	  if((len>2) || ((TablBlock[i]!=0x55) || (TablBlock[i]!=0xAA))) i=i+len+1;
	  else 
	  { // ��������� ���������� ������ 2-� ������� � ����� ������� 0x55 ��� 0xAA � TablBlock
	    if(((chan01==chan1) || (chan01==chan2)) &&
	       ((chan02==chan1) || (chan02==chan2))) return true;
	    else i=i+len+1;
	  }
	}
	return false;
}

void RestartCommut(MYFILE *pstFile)
{ // ���c���������� ������� ���������� ����������
  unsigned short i,j;
  HANDLE hFileIn;

  hFileIn=CreateFile(pstFile->szFileName,GENERIC_READ,0);
  if(hFileIn>=0) 
  {
    ReadFile(hFileIn,s_btBuf512+LenNameGroup+1,sizeof(s_btBuf512)-LenNameGroup-1);
    CloseHandle(hFileIn);
    for(j=0;j<=LenNameGroup;j++) s_btBuf512[j]=0; // ������� ����� �����
    i=strrchr(pstFile->szFileName,'.')-pstFile->szFileName; // ���������� �������������� ��������� ����� � ����� �����
    if(i>LenNameGroup) i=LenNameGroup;
    for(j=0;j<i;j++) s_btBuf512[j]=pstFile->szFileName[j]; // ����������� ����� ����� ��� ����������
    i=InclGroup(s_btBuf512,0); // ������� ������
    if((i!=true) && (i!=200)) OutSNMP(0,0xFF,i);
  }
}

unsigned char PlaySound2ConnectionPoint(unsigned short wNChan,char * FName)
{ // ������ ������� �� �����
  unsigned int testS, len, i;
  unsigned char chan;
  section("bigdata") static char WavBuf[1024];

	if(!StartWavS)
	{ // ����� �� ������ ���� �������
		hFileS=CreateFile(FName, GENERIC_READ,0); // �������� �����
		if(!(AdrOutSound=pvPortMalloc(len=GetFileSize(hFileS))))
		{ ErrMallocRecord++; return 3; }
		SaveAdrOutSound=AdrOutSound;
		if(hFileS>=0)
		{
			do
			{
				testS=len-StartWavS;
				if(testS>1024) testS=1024;
				testS=ReadFile(hFileS,WavBuf,testS);
				for (i=0;i<testS;i++) *AdrOutSound++=WavBuf[i];
				if(testS>0)
				{ StartWavS+=testS; vTaskDelay(5); }
			}
			while(testS==1024);
			CloseHandle(hFileS);
			if(wNChan > 0xFF) ChanWavS=(wNChan & 0xFF) + LenHard + LenVirt - 1; 
			else ChanWavS=wNChan + LenSport1 - 1;
			AdrOutSound=SaveAdrOutSound;
			PlayWavS=0x38;
			return 0;
		}
		return 1;
	}
	return 2;
}

bool IsLevelPresent(unsigned short wConnectionPoint)
{ // ������ ������� ��������� �� ������
	wConnectionPoint--; // ����� ���������� � 1
	if(wConnectionPoint & 0xFF00)
	{ // ����� �����������
	  if(YesSpeak[(wConnectionPoint & 0x00FF) + LenSport0]) return true;
	  else return false;
	}
	else
	{ // ���������� �����
	  if(YesSpeak[wConnectionPoint]) return true;
	  else return false;
	}
}

void AutoAnswerSignal(unsigned short wNChan)
{ // ��������� ������� ����������� � ���������� � ������ ��������� ����
  vPortEnableSwitchTask(false);
  wNChan=CalcRealChannel(wNChan>>8,(wNChan & 0xFF)-1);
/*{
static char strInfo[80];
LockPrintf();
sprintf(strInfo,"$DEBUG$AutoAnswerSignal: wNChan=%d",wNChan);
UnlockPrintf();
SendStr2IpLogger(strInfo);
}*/
  GeneratorMU(wNChan, 0, 0, 1, 200, 0, 0);
  GeneratorMU(wNChan, 800, 800, 200, 0, min9dB1, min9dB1);
  GeneratorMU(wNChan, 1200, 1200, 200, 0, min9dB1, min9dB1);
  vPortEnableSwitchTask(true);
}

//==========================================================================//
//--------------------------------------------------------------------------//
// ������ ��������� SportX.c �� ��������� ���������� �� ������� ���������   //
// ------------------ ������ ����������� ������ 10 �� ----------------------//
//--------------------------------------------------------------------------//
//==========================================================================//
void vWaitSportTask(void *pvParameters)
{ // ������� �������������� ������ �� SPORT
  unsigned short i;
  portTickType dwTm;
  unsigned int dwIrq,TimeWStart;
  int iTm;
 
  CountErrSPORT=0;
  for(i=0;i<NumberKeyboard;i++) VedSelect[i]=0;
  
  InitUK(); // ������������� ��

#ifdef __ISDN__
  #ifdef __TEST_ISDN__
    DbgPriInitBuffer();
  #endif
#endif// ISDN

  TimeWStart=xTaskGetTickCount();
  dwTm=xTaskGetTickCount();
  while(1)
  {
    dwIrq=cli();
    Count10mc=0;
    sti(dwIrq);
    iTm=xTaskGetTickCount()-TimeWStart; // ����� ���������� ������ ������ � ������
    if(iTm>TimeW) TimeW=iTm;
    TimeWStart=xTaskGetTickCount();
    ProcessMKC();
    CommandSportX();
    if(ChanSwing) Swinging(ChanSwing); // ��������� ���������� �������
    Tikker4Selector();
    SendTone2ISDN();
    ARU4CP();
    PlaySoundKeep();
    DelOpovMessageFromPlayList();
    ProcessListRecord();
    EndSound4Selector();
    AnalizeMKA();
    RequestRestart6MKA();
    SetWDState(d_WDSTATE_SportXCmd);
    vTaskDelayUntil(&dwTm,10); // ������ ������ 10 ��, �� ����
  }
}

