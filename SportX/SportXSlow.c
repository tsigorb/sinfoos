#include "..\Sinfo.h"
#include "SportXFn.h"

///////////////////////////////////////////////////////////////////////////////
void *pvPortMalloc(size_t xSize);
void vPortFree(void *pv);

unsigned char HighBitChanE1(unsigned char potok);

///////////////////////////////////////////////////////////////////////////////
extern xQueueHandle xQueueCommand;

extern unsigned short SpeakInSel; // ����� ���������� ��������� ������ �������� ���������
extern unsigned char PorogMiddle[]; // ����� �������� �������� �� ������ ����������� (==0 - ������ �������)
extern unsigned short Revers[]; // ������� �������� �� ������ ����������� (==0 - ������ �������)

extern unsigned char ARUTP[]; // ��������� ���
extern unsigned char MaxARUTP[]; // �������� ���
extern unsigned short KInARU[]; // ��������� ���

extern unsigned int FTik; // ������� �������
extern unsigned short PeriodTikker; // ������ �������

extern unsigned int TestBoard; // �������� ����������������� (�� 2 ����) ���������� ���� (��.) � ������� �1(��.)
extern unsigned short ParamUKC[]; // ��������� (�� 1 ����) ���������� ������ ������� ���(��.) � E1(��.), ������������ ��������� � SNMP, N����.
extern unsigned char FailureE1[4][10]; // ��������� ��������� ������ ������� E1

extern unsigned char TablMKC[LenMKC][LenCommutMKC]; // ������� ���������� ������� ���

extern time_t Time_Sys; 

extern unsigned char E1_EDSS;

extern unsigned int LenKeep;

///////////////////////////////////////////////////////////////////////////////
unsigned short LenSNPult; // ����� �������� ������� �� ��� �������
unsigned short LenLicPult=0; // ����� �������� �������
unsigned short LenSNSound; // ����� �������� ������� �� ��� �����������
unsigned short LenLicSound=0; // ����� �������� �����������
unsigned short LenSNIPPM; // ����� �������� ������� �� ��� IPPM
unsigned short LenLicIPPM=0; // ����� �������� IPPM

char AnalTDC[5]  =".an1"; // ���������� ����� ������ ������������� 2-� ���������� ������� �������� 
char AnalPCDT[5] =".an2"; // ���������� ����� ������ ����������������� 2-� ���������� ������� �������� 
char SCall[5]="Call_"; // ��� ����� �����������
char SpecGen[5]=".gen"; // ���������� ����� ����� �����������

unsigned char OldComm; // ���������� �������� �������� ����������

unsigned char FilePOS[FileNameLEN];
unsigned char FileREC[FileNameLEN];
unsigned char FileIPPM[FileNameLEN];

unsigned char WaitUserSNMP[QueueReplyCnt];// ����� �������� ������������

char KeepFile[30*8000]; // ����� �������� ����� - �� 30 ��� (240��)

///////////////////////////////////////////////////////////////////////////////
unsigned char GetLenLic(unsigned char data)
{ // ����������� ����� ��������
  unsigned char i;
  static unsigned char LicCodeArr[128]=
  {  0, 87, 83, 89, 65, 75,127,109, 29, 15, 59,209,201,227,135,165,
    69,103,  3, 41,241,155,175,125, 13,223,235,161,121, 51,247,181,
   117, 55,243,185, 97, 43,159, 77, 61,239, 91, 49,233, 67, 39,133,
   101,199,163,  9,145,123,207, 93, 45,191, 11,129, 25,147, 23,149,
    21,151, 19,153,  1,139, 63,173,221, 79,251, 17,137, 35, 71,229,
     5,167,195,105,177,219,111,189,205, 31,171,225, 57,115,183,245,
    53,119,179,249, 33,107, 95,141,253, 47, 27,113,169,131,231,197,
    37,  7, 99, 73, 81,187,143,157,237,255,203,193,217,211,215,213 };
  for(i=0;i<128;i++)
  {
    if(LicCodeArr[i]==data) return i;
  }
  return 0;
}

void TestLic(char *strSignature)
{ // �������� ������� �������� �� ����� ��� �����������
 unsigned short i,j,k;
 unsigned char btDevice,LenSN,*DataFile;
 unsigned int AddrMU; // ������� ����� �� ��������
 unsigned int data, il, jl;
 bool Result;
 HANDLE hFileIn;
 MYFILE stFile;

 btDevice=0;
 if(!strcmp(strSignature,"POS.lic"))
 {
   btDevice=1;
   LenSNPult=0;
   LenLicPult=0;
 }
 if(!strcmp(strSignature,"REC.lic"))
 {
   btDevice=2;
   LenSNSound=0;
   LenLicSound=0;
 }
 if(!strcmp(strSignature,"IPPM.lic"))
 {
   btDevice=3;
   LenSNIPPM=0;
   LenLicIPPM=0;
 }
 if(!btDevice) return;
	il=0;
	while(FindNextFile(il,&stFile)!=0xFFFFFFFF)
	{
	  if(strstr(stFile.szFileName,strSignature))
	  {
		if(btDevice==1) memcpy(FilePOS,stFile.szFileName,FileNameLEN);
		if(btDevice==2) memcpy(FileREC,stFile.szFileName,FileNameLEN);
		if(btDevice==3) memcpy(FileIPPM,stFile.szFileName,FileNameLEN);
    	hFileIn=CreateFile(stFile.szFileName,GENERIC_READ,0);
		if(hFileIn>=0) 
		{
		  DataFile=pvPortMalloc(1024);
		  if(DataFile)
		  {
			ReadFile(hFileIn,DataFile,1024);
			i=(unsigned short)(*(DataFile+4)*256) + (unsigned short)(*(DataFile+5)); // ��������� �����
			LenSN=(unsigned short)(DataFile[i]); // ����� �������� ������� ��
			if(btDevice==1) LenSNPult=LenSN;
			if(btDevice==2) LenSNSound=LenSN;
			if(btDevice==3) LenSNIPPM=LenSN;
		    Result=false;
			for(j=0;j<LenSN;j++)
			{
				AddrMU=0; // ����� �� � �����
				i=(unsigned short)DataFile[i+1]*256 + (unsigned short)DataFile[i+2];	// ����� 1-�� �����
				AddrMU += (unsigned int)DataFile[i] << 24;	// 1-� ���� ������
				i=(unsigned short)DataFile[i+1]*256 + (unsigned short)DataFile[i+2];	// ����� 2-�� �����
				AddrMU += (unsigned int)DataFile[i] << 16;	// 2-� ���� ������
				i=(unsigned short)DataFile[i+1]*256 + (unsigned short)DataFile[i+2];	// ����� 3-�� �����
				AddrMU += (unsigned int)DataFile[i] << 8;	// 3-� ���� ������
				i=(unsigned short)DataFile[i+1]*256 + (unsigned short)DataFile[i+2];	// ����� 4-�� �����
				AddrMU += (unsigned int)DataFile[i];	// 4-� ���� ������
				data=AddrMU;
				AddrMU=0;
				il=jl=0x00000001;
				for(k=0;k<32;k++) 
				{
					if(k == 16) jl=0x00010000;
					if(k & 1) 
					{
						if(data & il) AddrMU |= jl << 8;
						jl <<= 1;
					} 
					else if(data & il) AddrMU |= jl;
					il <<= 1;
				}
				data=(RdFlash(AdrLic+2) << 16) + RdFlash(AdrLic);
				if(data==AddrMU)
				{ Result=true; break; }
			}
			if(Result)
			{
				i=(unsigned short)DataFile[i+1]*256 + (unsigned short)DataFile[i+2]; // ����� ����� ��������
				j=GetLenLic(DataFile[i]);
				if(btDevice==1) LenLicPult=j;
				if(btDevice==2) LenLicSound=j;
				if(btDevice==3) LenLicIPPM=j;
			}
			vPortFree(DataFile);
		  }
		  CloseHandle(hFileIn);
		}
		return;
	  }
	  il++;
	}
}

#ifdef __ISDN__
//--------------------------------------------------------------------------//
// ���� ������� ������ ���													//
// ������� �� ���� ��������� � ������ ��� � ������ ������ ������			//
// ��� 0 - ���� ��������� � ������ ���                                      //
//--------------------------------------------------------------------------//
unsigned char TestModulMKC(unsigned char E1, unsigned char KI)
{
  unsigned short FirstE1, Stream;

  if((ParamUKC[6] & 0x63)==0) return 0; // ��� ������� ���
  FirstE1=E1-ParamUKC[4]; // ������� ����� ������ ������� � 0
  Stream=0;
  if(FirstE1==0) // ��� ���������� ������
  {
    if(ParamUKC[6] & 0x42) Stream=KI;
  }
  else
    if(FirstE1==1)
    {
      if(ParamUKC[6] & 0x21) Stream=KI+32;
    }
    else
      if(FirstE1==2)
      {
        if(ParamUKC[6] & 0x42) Stream=KI+16;
      }
      else
        if(FirstE1==3)
        {
          if(ParamUKC[6] & 0x21) Stream=KI+48;
        }
  return Stream;
}

/*void CreatGroupStream(unsigned char TP,unsigned char Stream)
{ // �������� ������ ��� 16-�� �� ������ 1 ��� 2 Stream
  VirtualPoints[TP-1]=1; // �������� ����� �����������
  LockPrintf();
  sprintf((char *)(MassInData+2),"DSS1_KI16_%d@002%02d160C%02d",
          Stream,ParamUKC[4]+Stream-1,TP);
  UnlockPrintf();
{
extern char strInfoSpXCmd[];
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$CreatGroupStream: str=%s",
          MassInData+2);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}
  SaveGroupFlash(MassInData,false,TestModulMKC(ParamUKC[4]+Stream-1,0));
}*/

void CommutMKC2Sport1(unsigned char NumbE1,unsigned char MKC_KI,unsigned char Sport1KI)
{
extern unsigned short TablSport1[LenSport1]; // ������� ���������� ������� SPORT1
  unsigned char AdrMod;

  AdrMod=(NumbE1 - 1) >> 2; // ���������� ����� ������
  MKC_KI += HighBitChanE1(NumbE1); // ���������� ������ �����
  TablSport1[Sport1KI]=(MKC_KI & 0x1F) + (NumbE1<<8); // �������� ����� Sport1
/*{
extern char strInfoSpXCmd[];
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$CommutMKC2Sport1: AdrMod=%d, Chan=%d, Sport1KI=%d",
          AdrMod,MKC_KI,Sport1KI);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
  WriteE1(AdrMod,MKC_KI,Sport1KI);
  WriteE1(AdrMod,Sport1KI,MKC_KI);
}
          
void InitISDN4MKC(void)
{
  if(!ParamUKC[4]) return; // ��� ISDN �� ������� ��� �/��� ���
  if(ParamUKC[5]==1)
  { // ���� ������ ��� � 1 ������� EDSS
//    CreatGroupStream(LenVirt,1); // ���������� 1-� ����� � 1-� ��
    CommutMKC2Sport1(ParamUKC[4],16,1); // 16KI Sport1 ��� ��� �������� ������!
  }
  else 
  {
    if(ParamUKC[5]==2) 
    { // ���� ������ ��� � 2 �������� EDSS
      CommutMKC2Sport1(ParamUKC[4],16,1); // 16KI Sport1 ��� ��� �������� ������!
      CommutMKC2Sport1(ParamUKC[4]+1,16,2); // 16KI Sport1 ��� ��� �������� ������!
//      CreatGroupStream(LenVirt,1); // ���������� 1-� ����� Sport1
//      CreatGroupStream(LenVirt-1,2); // � ���������� 2-� ����� Sport1
    }
  }
} 
#endif

void InitUK(void)
{ // ������������� ��
 unsigned short F1,F2,Type,dF,chan;
 unsigned short i,j;
 signed short TimA;
 HANDLE hFileIn;
 MYFILE stFile;
 section("bigdata") static unsigned char MassData[LenInOutData];
 
 for(i=0;i<QueueReplyCnt;i++) WaitUserSNMP[i]=0;
 for(i=0;i<LenVirt;i++) Revers[i]=0; // ������� "���������" �� ���� ��
 ClearTablCommut(); // ������� ������ ����������

 SpeakInSel=RdFlash(AdrSpeakInSel); // ����� ������� ��������� ��� �������� ���������
 if((SpeakInSel <21) || (SpeakInSel > 109))
 { SpeakInSel=SpeakIn; ReWrFlash(SpeakInSel,AdrSpeakInSel); }

 if(ParamUKC[3]>3) // �������� �������� ����������
 { ParamUKC[3]=0; ReWrFlash(0,AdrParamUKC+6); }

 OldComm=ParamUKC[3];
 E1_EDSS=ParamUKC[4];

 // ���������� (����������) �� ���������� ������� ���
 if(!(ParamUKC[0] & 0x1000)) TestBoard|=0x03000000;
 if(!(ParamUKC[0] & 0x2000)) TestBoard|=0x0C000000;
 if(!(ParamUKC[0] & 0x4000)) TestBoard|=0x30000000;
 if(!(ParamUKC[0] & 0x8000)) TestBoard|=0xC0000000; 

// !�������������� ���,ISDN �� ���, � ������� ���������� ���������� � ������� ReinitConnectionPoint!
 i=0;
 while((FindNextFile(i,&stFile))!=(unsigned long)-1)
 { // ������������� �������� ��������
     if(FindInitFile(&stFile,AnalTDC) || FindInitFile(&stFile,AnalPCDT))
     { // ������������� ������ 
       hFileIn=CreateFile(stFile.szFileName, GENERIC_READ, 0);
       if(hFileIn >= 0) 
       {
         ReadFile(hFileIn,MassData,sizeof(MassData));
         CloseHandle(hFileIn);
         chan=MassData[1]; // ������ ����� ������
         // ������������� ������ ������
         if(MassData[0]==0) chan+=(LenSport1-1);
         else
           if(MassData[0]==1) chan+=(LenHard-1)+LenVirt;
         F1 =(unsigned short)MassData[3]+((unsigned short)MassData[4]<<8);
         F2 =(unsigned short)MassData[5]+((unsigned short)MassData[6]<<8);
         dF =(unsigned short)MassData[7]+((unsigned short)MassData[8]<<8);
         TimA =(unsigned short)MassData[9]+((unsigned short)MassData[10]<<8);
         Type =(unsigned short)MassData[11]+((unsigned short)MassData[12]<<8);
         InitAnalMU(chan,MassData[2],F1,F2,dF,TimA,Type,MassData[13])+48;
       }
     }    
     else
       if(FindInitFile(&stFile,SpecGen))
       { // �������������� �������������
         hFileIn=CreateFile(stFile.szFileName, GENERIC_READ, 0);
         if(hFileIn>=0)
         {
           ReadFile(hFileIn,MassData,sizeof(MassData)-2);
           CloseHandle(hFileIn);
           InitSpecGen(MassData); // �������� �������� ���������
         }
       }
   i++;
 }

 // �������������� ����������� ����� �������� �� ������ �����������
 MassData[1]=1;
 for(i=0;i<LenVirt;i++)
 {
    MassData[2]=i;
    j=RdFlash(AdrTypeCall + (LenSport0 + i)*4);
    MassData[3]=j;
    MassData[4]=j >> 8;
    chan=RdFlash(AdrTypeCall + ((LenSport0 + i)*4) + 2);
    MassData[5]=chan;
    if((MassData[6]=(chan >> 8)) || // ��������� ���� ������ ���� ����� ����
       (!FoundTP(i,0)))// && (!ReversFoundPoint(i)))) // ��� �� �� ������������, ����� �������� flash
    {
        ReWrFlash(0,AdrTypeCall + ((LenSport0 + i)*4) + 0);
        ReWrFlash(0,AdrTypeCall + ((LenSport0 + i)*4) + 2);
        ReWrFlash(0,AdrAnalPorog + ((LenSport0 + i)*2));
        ReWrFlash(0,AdrReturnCoef + ((LenSport0 + i)*2));
        ReWrFlash(0,AdrARUTP + (i*2));
    }
    else SetParamPoint(MassData); // �� ������������� ���������
    j=RdFlash(AdrAnalPorog + (LenSport0*2) + (i*2)); // �������������� ������ ������� �� ������
    if(j>50) j=0;
    PorogMiddle[i]=j;
    j=RdFlash(AdrReturnCoef + (LenSport0+i)*2); // �������������� ������ ��������
    if(j>100) j=0;
    Revers[i]=j * CoefReturnCh; 
    j=RdFlash(AdrARUTP + (i*2)); // �������������� ��������� ��� �� ��
    if(j>2) j=0;
    ARUTP[i]=j; 
    j=RdFlash(AdrMinARUTP + (i*2)); // �������������� ������ ���
    if(j>28) j=0;
    MaxARUTP[i]=j;
    KInARU[i]=1000; 
 }
 
 RestartTempBuf(); // ����������������� ��������� �������
 Time_Sys=ReadClock();

/* ---------------�� ��������������� ���� ???
 ExistTime=RdFlash(AdrSysTime) + (RdFlash(AdrSysTime+2) << 16);
 if((Time_Sys-ExistTime>7200) && (Time_Sys-ExistTime<604800))	// �������� �� ���������� �� �� 2 ����� �� 1 ������
 {Time_Sys=Time_Sys - (((Time_Sys-ExistTime) / 7200) * (signed short)RdFlash(AdrErrTime));	// �������� ����� �� ����� ����������
  TimeSet=TimeCorrClock+100;}	// ������� � ������������� �������� ���� */
 
 FTik=RdFlash(AdrFTikker); // ������ ������� �������
 if((FTik>3400) || (FTik<300))
 { FTik=800; ReWrFlash(FTik,AdrFTikker); }
 PeriodTikker=RdFlash(AdrPeriodTikker); // ������ ������� �������
 if((PeriodTikker>2000) || (PeriodTikker<200))
 { PeriodTikker=1500; ReWrFlash(PeriodTikker,AdrPeriodTikker); }

 hFileIn=CreateFile("KeepFile.wav", GENERIC_READ, 0);
 if(hFileIn>=0) 
 {
    unsigned int TekKeep=0;
	memset(KeepFile,0,sizeof(KeepFile));
	LenKeep=0;
	do
	{
		TekKeep=sizeof(KeepFile)-LenKeep;
		if(TekKeep>1024) TekKeep=1024;
		TekKeep=ReadFile(hFileIn,KeepFile+LenKeep,TekKeep);
		if(TekKeep>0)
		{ LenKeep+=TekKeep; vTaskDelay(1); }
	}
	while(TekKeep==1024);
	CloseHandle(hFileIn); 
	for (i=0;i<56;i++) KeepFile[i]=0xD5; // ��������� ��������� ����� (a_expand(0)=0xD5)
 }
 InitSelectorInfo();
 InitConfInfo();
}

// �������� ����������� ����������� ����. ������
// ����� 0 - ��� ������
//       1 - ���������� �������� ������ ����� �������
//       2 - ��� ����� �������� ��� ������� ��
//       3 - ��������� ����� ������
unsigned char CheckRegistration(unsigned char SN,unsigned char btCht)
{
extern unsigned short LenLicPult; // ����� �������� �������

  if(LenLicPult==0xFF) return (0xFF);
  if((SN<16) || (SN>99)) return(3);
  if(!LenLicPult) return 2;
  if(LenLicPult<btCht) return 1;
  return 0;
}

bool ListenDispatcher (unsigned short ChanTechnic, unsigned short ChanDS, unsigned short ChanRS, unsigned char Incl)
{ // ��������� ������������� �������� ���������� ����������
  unsigned short ChanT, ChanDS1, ChanRS1;

  if(ChanTechnic<0x100) ChanTechnic+=LenSport1-1; // � ������� ���������� �����
  else ChanTechnic=(ChanTechnic & 0xFF)+LenHard+LenVirt-1; // � ������� ������������ �����

  if(ChanDS<0x100)
  { // � ���������� ���������� �����
    ChanDS+=LenSport1-1; // ����� ������� ������ ����������
    ChanRS+=LenSport1-1;
    if(Incl)
    { // ��������� ���������
      if(!InclTempComm(ChanTechnic,ChanDS,1)) return false; 
      if(!InclTempComm(ChanTechnic,ChanRS,1)) 
      {
        ExclTempComm(ChanTechnic,ChanDS);
        return false;
      }
    } 
    else
    { // ���������� ���������
      ExclTempComm(ChanTechnic,ChanDS); ExclTempComm(ChanTechnic,ChanRS);
    }
  }
  else
  { // ������������ ����� - ����� ����������� (����� ������� � ���������� � ���������)
    ChanDS&=0xFF; ChanRS&=0xFF;
    ChanDS1=ChanDS+LenHard-1;
    ChanRS1=ChanRS+LenHard-1;
    ChanDS+=LenVirt+LenHard-1;
    ChanRS+=LenVirt+LenHard-1;
    if(Incl)
    { // ��������� ���������
      if((!InclTempComm(ChanTechnic,ChanDS,1))  || (!InclTempComm(ChanTechnic,ChanRS,1)) ||
         (!InclTempComm(ChanTechnic,ChanDS1,1)) || (!InclTempComm(ChanTechnic,ChanRS1,1)))
      {
        ExclTempComm(ChanTechnic,ChanDS); ExclTempComm(ChanTechnic,ChanRS);
        ExclTempComm(ChanTechnic,ChanDS1); ExclTempComm(ChanTechnic,ChanRS1);
        return false;
      }
    }
    else
    { // ���������� ���������
      ExclTempComm(ChanTechnic,ChanDS); ExclTempComm(ChanTechnic,ChanRS);
      ExclTempComm(ChanTechnic,ChanDS1); ExclTempComm(ChanTechnic,ChanRS1);
    }
  }
  return true;
}

void WriteAddrMU(unsigned char year, unsigned char mon, unsigned char day)
{ // ��������� ��������� ������ ������ ����������
  unsigned int data;
  data=(RdFlash(AdrLic+2) << 16) + RdFlash(AdrLic);
  if(data == 0xFFFFFFFF)
  {
    //��������� �����
    data=(*pRTC_STAT & 0x1FFFF) + ((unsigned int)year << 26) + ((unsigned int)mon << 22) + ((unsigned int)day << 17);
    WrFlash((unsigned short) data, AdrLic);
    data >>= 16;
    WrFlash((unsigned short) data, AdrLic+2);
  }
}

bool FindInitFile(MYFILE *pstFile,char *Ext)
{ // ����� ����� �������������
  char *pStr;

  pStr=strrchr(pstFile->szFileName,'.');
  if(!pStr) return false;
  strlwr(pStr);
  return(strcmp(pStr,Ext)==0);
}

void InitMKC(unsigned char NMod)
{
  unsigned short i;
  unsigned char MasRegs[5];

  if(ParamUKC[0] & (0x1000 << NMod))
  { 
    for(i=0;i<LenCommutMKC;i++)
    { // �������� ������� ���������� ������ ���1
      if(TablMKC[NMod][i]!=0xFF)
      {
        if(!WriteE1(NMod,i,TablMKC[NMod][i])) break;
      }
    }
    i=RdFlash(AdrParamMKC+ParamUKC[3]*4*8+NMod*8+0);
    MasRegs[0]=i;
    MasRegs[1]=i >> 8;
    i=RdFlash(AdrParamMKC+ParamUKC[3]*4*8+NMod*8+2);
    MasRegs[2]=i; 
    MasRegs[3]=i >> 8; 
    MasRegs[4]=RdFlash(AdrParamMKC+ParamUKC[3]*4*8+NMod*8+4); 
    FailureE1[NMod][8]=0x07;
    for(i=0;i<5;i++)
    { // �������� ���������� ������� � �������� ������������� ������ ���
      if(!WriteE1(NMod,i+0xC0,MasRegs[i])) break;
    }
    WriteE1(NMod,0xCD,0);
  }
}

unsigned char GetFirstE1(void)
{ // ������� ������ ������ �������
  if(ParamUKC[5])
  { // ����� ������� E1 ��� >=1
    return(0);
  }
  if((ParamUKC[6] & 0x02) || (ParamUKC[6] & 0x40)) return 1;
  else
  {
    if((ParamUKC[6] & 0x01) || (ParamUKC[6] & 0x20)) return 2;
  }
  return 0xFF;
}

unsigned char GetNextE1(unsigned char btE1code)
{ // ������� ���������� ������ ������ E1
  if(ParamUKC[5]>1)
  { // ����� ������� E1 ��� >1
    return(0xff);
  }
  switch(btE1code)
  {
    case 0:
      if((ParamUKC[6] & 0x01) || (ParamUKC[6] & 0x20)) return 2;
      break;
    case 1:
      if((ParamUKC[6] & 0x01) || (ParamUKC[6] & 0x20)) return 2;
      if((ParamUKC[6] & 0x02) && (ParamUKC[6] & 0x40)) return 3;
      break;
    case 2:
      if((ParamUKC[6] & 0x02) && (ParamUKC[6] & 0x40)) return 3;
      if((ParamUKC[6] & 0x01) && (ParamUKC[6] & 0x20)) return 4;
      break;
    case 3:
      if((ParamUKC[6] & 0x01) && (ParamUKC[6] & 0x20)) return 4;
      break;
    case 4:
      break;
  }
  return(0xff);
}

