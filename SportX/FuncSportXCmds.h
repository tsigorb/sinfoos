void *pvPortMalloc(size_t xSize);
void vPortFree(void *pv);

unsigned char _xGetCP(void);
bool _xFreeCP(unsigned char btCP);

unsigned char ReadE1(unsigned char NMod, unsigned char Reg);
bool IsPultConfigRead();
void InitUK(void);
void AnswerTestMKC(unsigned char *MasOut);
void InitISDN4MKC(void);

unsigned char FoundDelTempComm(unsigned char type, unsigned char chan);
unsigned short ReadAllTempCommute(unsigned char *MasOut);

unsigned char SelectorCreatX(unsigned char *MasOut);
unsigned char SelectorDelX(unsigned char *MasOut);
unsigned char SelectorChanBlockX(unsigned char InCmd,unsigned char *MasOut);
unsigned char SelectorDelChanX(unsigned char *MasOut);
unsigned char SelectorAddChanX(unsigned char *MasOut);
unsigned char SelectorSetVedX(void);
unsigned char SelectorTikkerX(void);
void SelectorRecordOnOff(unsigned char SN,unsigned char btState);
unsigned char SelectorGen4ChanOffX(void);
unsigned char FindNKlavFromSN2Sel(unsigned char SN);
bool IsSelectorMessage(unsigned char NKlav);

unsigned char ConferenceCreatX(unsigned char *MasOut);
unsigned char ConferenceDelX(unsigned char *MasOut);
unsigned char ConferenceAbntX(unsigned char *MasOut);
unsigned char ConferenceOperatorX(unsigned char *MasOut);

void SendTone2ISDN(void);

void Command201(void);
void Command202(void);
void Command203(void);
void Command204(void);
void Command205(void);
void Command206(void);
int _xpri_call_on_incoming(sCommand *psCmd);
void _xpri_call_on_info(void);
void _xpri_call_on_alert(void);
int _xpri_call_on_progress(void);
void _xpri_call_on_speaker_enable(void);
void _xpri_call_on_answer(void);
void _xpri_call_on_disconnect(void);
void _xpri_call_on_release(void);

unsigned char TestModulMKC(unsigned char E1,unsigned char KI);
unsigned char Answer(sInfoKadr *psInfo,unsigned char command, unsigned char Len);

void SendBlockPPS(unsigned char chan, unsigned char type);

unsigned char ExclGen(unsigned char chan);

