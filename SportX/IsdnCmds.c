#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FuncSportXCmds.h"

#ifdef __ISDN__
#define __Control__ // ���������� ������ ��������� ������ ��� EDSS

#include "..\edss\pri_call.h"
#include "..\EDSS\pri_channel.h"

///////////////////////////////////////////////////////////////////////////////
extern unsigned int A1;      // ����� ����������
extern unsigned short PORT1; // ���� ����������
extern unsigned char MassInData[LenInOutData];
extern unsigned char MassOutData[LenInOutData];

extern unsigned short ParamUKC[];

extern unsigned char VirtualPoints[LenVirt];        // ������� ������� ����� �����������
extern unsigned char TablSum[LenSumChan]; // ������� ������� ����������� �� ������� ���� ���������� (�������������)
extern unsigned char TablBlock[]; // ������� ������� ����������� �� �����
extern unsigned char CopyTablBlock[]; // ����� ������� ������� ����������� �� �����
extern GROUPINFO_TP MasGroupInfo[MaxGroup];

extern unsigned char E1_EDSS; // ��������� ����� ������ EDSS

extern unsigned short PlayWav[LenVirt];
extern unsigned short ChanWav[LenVirt];

extern xQueueHandle xQueueAnswYuri;

extern char strInfoSpXCmd[512];

///////////////////////////////////////////////////////////////////////////////
// --------------- ������ ��������� -------------------------
unsigned short E1KI[LenVirt];     // N������*256 + ��
unsigned short CallerID[LenVirt];   // owner
unsigned short ChannelOperator[LenVirt];  // ��� � ����� ������ ���������
unsigned char  NTelefon[LenVirt][LenTlfNumber+1];  // ������ ������� ��������� �� ������ �����������
unsigned char  NameTelefon[LenVirt][LenTlfName+1]; // ������ ���� �������� ��������� �� ������ ����������
unsigned char  TypeCall[LenVirt];   // ��� ������� ������ (��������, ������)
unsigned char  SelectEDSS[LenVirt]; // ������� ��������� (������ ������ � ������� ����������+1) ��� ��� (0)

///////////////////////////////////////////////////////////////////////////////
static sInfoKadr sInfoAnswer;

// ��������� ���������� ��� �-� ISDN (���� �������� �� ���������!!!)
unsigned char MassDataISDN[LenInOutData];
int src;      // ��� ��������, ��� �������� ������
int trunkno;  // ����� ������ E1
char *dest;   // ����� ���������� ������ � ������� type_numberb
int complete; // ���������� ����� B ���� (==1) ��� ����� ����� ����� (==0)
char *cid;    // ����� ������ ��������� � ������� type_numbera
char *cname;  // ����� ����� ���������
int channel;  // ����� ���������� ��������� � ������ E1
int owner;    // ��� �������, ��� ����� ���������� ������
int cause;    // �������: �����, ������������ ����� � �.�.
int inband;   // ���� �� �������� ��� (==1)

char *GetNameGroupEDSS(char *pStr,unsigned char E1,unsigned char KI,unsigned char TP)
{ // 
  LockPrintf();
  sprintf(pStr,"E1_%02d%02dCP%02d@",E1,KI,TP);
  UnlockPrintf();
  return(pStr+strlen(pStr));
}

void ClearGroupEDSS(unsigned short TP)
{ // �������� ���������� � ������ �����������
  unsigned char E1,KI;

  KI=E1KI[TP] & 0xFF;
  if(KI)
  {
    E1=E1KI[TP]>>8;
    GetNameGroupEDSS((char *)MassDataISDN,E1,KI,TP+1);
//    MassDataISDN[LenNameGroup]=0;
    ExclGroup(MassDataISDN); // �������� ���������� E1KI � ��
  }
}

void CreateCommuteTP_E1KI(unsigned short TP, unsigned short E1_KI)
{ // �������� ���������� �� � ��������� ���������� ������ E1
  char *pStr;
  unsigned char E1,KI;

  E1=E1_KI >> 8;
  KI=E1_KI & 0xff;
  pStr=GetNameGroupEDSS((char *)(MassDataISDN+2),E1,KI,TP+1);
  LockPrintf();
  sprintf(pStr,"002%02d%02d0c%02d",E1,KI,TP+1);
  UnlockPrintf();
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$CreateCommuteTP_E1KI: str=%s",
          (char *)(MassDataISDN+2));
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
//  MassDataISDN[2+LenNameGroup]=0;
  MassOutData[0]=SaveGroupFlash(MassDataISDN,false,TestModulMKC(E1,KI));
  E1KI[TP]=E1_KI;
}

void ReGroupTP(unsigned short NewE1KI, unsigned short TP)
{ // ������������ ���������� � ������ ����������� ��� ����� ��
  if(NewE1KI!=E1KI[TP])
  {
    ClearGroupEDSS(TP); // �������� ������ ����������
    CreateCommuteTP_E1KI(TP,NewE1KI);
  }
}

void ClearTP4ISDN(unsigned short TP)
{ // ������� TP, ��������� ���������� � ��������
  unsigned short i;
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$ClearTP4ISDN: TP=%d,CallerID[TP]=%d,ChannelOperator[TP]=%d",
          TP,CallerID[TP],ChannelOperator[TP]);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
  if(CallerID[TP])
  { // ������� IP ��� ISDN
    ClearGroupEDSS(TP);
    if(ChannelOperator[TP])
    { // �������� ��������� ���������� �� � ������ ���������
      MassDataISDN[3]=TP+LenHard+LenVirt; // ����� �����������
      MassDataISDN[4]=ChannelOperator[TP]>>8;
      MassDataISDN[5]=ChannelOperator[TP];
      if(MassDataISDN[4]) MassDataISDN[5]+=LenHard+LenVirt-1;
      else MassDataISDN[5]+=LenSport1-1; // ���������� �����
      ExclTempComm(MassDataISDN[3],MassDataISDN[5]); 
    }
    for(i=0;i<LenTlfNumber;i++) NTelefon[TP][i]=0; // ������� ��������
    for(i=0;i<LenTlfName;i++) NameTelefon[TP][i]=0;
    E1KI[TP]=0;
    CallerID[TP]=0;
    ChannelOperator[TP]=0;
    TypeCall[TP]=0;
    VirtualPoints[TP]=0;
    PlayWav[TP]=0;
    ChanWav[TP]=0;
    SelectEDSS[TP]=0;
  }
}

void SendTone2ISDN(void)
{ // �������� ���������� ������� ��� �������������
  unsigned short i,j;
  for(i=0;i<LenVirt;i++)
  {
    j=TypeCall[i];
    if(j)
    {
      if(j>0x80)
      {
        if(!FoundGenerator(1,i)) CallAbon(1,i,CallKPV,0,2);
        j=CallZummer;
      }
      // ����� ������ � ChannelOperator ���������� � 1, � � FoundGenerator � CallAbon � 0!!
      if(ChannelOperator[i] && !FoundGenerator(ChannelOperator[i]>>8,ChannelOperator[i]-1))
      { CallAbon(ChannelOperator[i]>>8,ChannelOperator[i]-1,(char)j,0,2); }
    }
  }
}

// ------------------ ������� �� PRI EDSS -------------------------
void Command201(void)
{ // ��������� ����� ��������
  unsigned short i, TP;
  int trunkno,Result_Pri;
  char NKlav,*savedest;

  // ����� 2,3-��; 4,5-����� ���������; 6-����� ������, 7..10-����.����������� ������
  // 11-��� �� ����� �������; 12..15-����. ������ �����������; 16..19-����.����� �����������;
  // 20-SN ������ ��� �������� ��������� ��� 0, ���� ��� ������� �������
  TP=MassInData[3]-1;
  // owner (��� �����������)
  src=((unsigned short)g_IpParams.ucDataSIPR[3]<<6)+MassInData[3];
  // ����� ����������� ������ 
  savedest=dest=(char *)((unsigned int)MassInData[7]<<24 | (unsigned int)MassInData[8]<<16 |
                (unsigned int)MassInData[9]<<8 | (unsigned int)MassInData[10]);
  // ������� ������ ������ ���������
  complete=MassInData[11];

  // ����� ������ ���������
  cid=(char *)((unsigned int)MassInData[12]<<24 | (unsigned int)MassInData[13]<<16 |
      (unsigned int)MassInData[14]<<8 | (unsigned int)MassInData[15]);

  // ����� ����� ���������
  cname=(char *)((unsigned int)MassInData[16]<<24 | (unsigned int)MassInData[17]<<16 |
        (unsigned int)MassInData[18]<<8 | (unsigned int)MassInData[19]);
      
  if(ParamUKC[5]==2) trunkno=0xff;
  else
  {
    trunkno=MassInData[6];
    if(trunkno) trunkno--;
  }
  if((Result_Pri=pri_call_setup(src,trunkno,dest,complete,cid,cname))==-1)
  { VirtualPoints[TP]=MassOutData[0]=0; } // ��� ������������� ������
  else 
  { // ��� ������������� ������ �������� ���������� � ���������� E1KI (N ������ � ��)
    E1KI[TP]=((E1_EDSS+trunkno) << 8) | Result_Pri; // ����� � �� 
    CallerID[TP]=src; // owner
    ChannelOperator[TP]=(((unsigned short)MassInData[4])<<8)+MassInData[5]; // ����� ���������
    SelectEDSS[TP]=0;
    if(MassInData[20])
    {
      NKlav=FindNKlavFromSN2Sel(MassInData[20]); 
      if(NKlav!=0xff) SelectEDSS[TP]=NKlav+1;
    }
    // ������ ������ ����������� � ������ ��� � ������ ��������� �������
    for(i=0;i<LenTlfNumber;i++) NTelefon[TP][i]=*dest++;
    CreateCommuteTP_E1KI(TP,E1KI[TP]);
    MassOutData[0]=1; 
  }
  Answer(&sInfoAnswer,MassInData[1],2);  
//SendStr2IpLogger("$DEBUG$Command201: wChan=%d, wOper=%d",TP,ChannelOperator[TP]);
#ifdef __Control__
 MassOutData[26]=MassOutData[0];
 MassOutData[0]=2; // ������� 2
 MassOutData[1]=1; // ��� ����� ����������� � � �����
 MassOutData[2]=TP;
 MassOutData[3]=31; // ��� ������-EDSS
 MassOutData[4]=201; // ��� ������
 MassOutData[5]=MassInData[4];// ��� ������ ��������� � ����� ������
 MassOutData[6]=MassInData[5];
 cid=(char *)atol((const char *)cid);
 MassOutData[7]=(unsigned int)cid>>24; // ����� �����������
 MassOutData[8]=(unsigned int)cid>>16;
 MassOutData[9]=(unsigned int)cid>>8;
 MassOutData[10]=(unsigned int)cid;
 MassOutData[11]=(unsigned int)cname>>24; // ����� ����� �����������
 MassOutData[12]=(unsigned int)cname>>16;
 MassOutData[13]=(unsigned int)cname>>8;
 MassOutData[14]=(unsigned int)cname;
 cid=(char *)atol(savedest);
 MassOutData[15]=(unsigned int)cid>>24; // ����� �����������
 MassOutData[16]=(unsigned int)cid>>16;
 MassOutData[17]=(unsigned int)cid>>8;
 MassOutData[18]=(unsigned int)cid;
 MassOutData[19]=complete; // ��� �� ����� ��������
 if(Result_Pri==-1)
 {
   MassOutData[20]=trunkno;
   if(trunkno!=0xff) MassOutData[20]+=E1_EDSS; //����� ������
   MassOutData[21]=0xff; // ����� ��
 }
 else
 {
   MassOutData[20]=E1KI[TP]>>8; //����� ������
   MassOutData[21]=E1KI[TP]; // ����� ��
 }
 MassOutData[22]=Result_Pri>>24; // ��������� �������
 MassOutData[23]=Result_Pri>>16;
 MassOutData[24]=Result_Pri>>8;
 MassOutData[25]=Result_Pri;
 SendDiagnostics2DspCom(MassOutData,27);
#endif
}

void Command202(void)
{ // ���������� ������� �������� � ��� ������� �����
  unsigned short TP, i;
  int Result_Pri;

  // ����� 2,3-��; 4,5-����� ���������; 6-���� �� �������� ����� ���
  MassOutData[0]=false;
  if(MassInData[2]==1)
  {
    TP=MassInData[3]-1; // ����� ����� �����������
    if(CallerID[TP])
    { // ���� ������������
      if(!(Result_Pri=pri_call_alert((E1KI[TP]>>8)-E1_EDSS,E1KI[TP]&0xFF,CallerID[TP]))) 
      {
        ChannelOperator[TP]=(((unsigned short)MassInData[4])<<8) | (MassInData[5]-1);
        if(MassInData[6]) 
        { // ������ ������� "�����" 
          if(TypeCall[TP]>0x80)
          {
            TypeCall[TP]-=0x80;
            while(ExclGenerator(1,TP));
          }
          TypeCall[TP]=0;
          if(ChannelOperator[TP])
          { while(ExclGenerator(ChannelOperator[TP]>>8,ChannelOperator[TP])); }
          TypeCall[TP]=CallKPV+0x80; 
        }
        MassOutData[0]=true;
      }
   }
 }
 Answer(&sInfoAnswer,MassInData[1],3);
#ifdef __Control__
  MassOutData[0]=2;   // ������� 2
  MassOutData[1]=1;   // ��� ����� ����������� � � �����
  MassOutData[2]=TP;
  MassOutData[3]=31;  // ��� ������-EDSS
  MassOutData[4]=202; // ��� ������
  MassOutData[5]=ChannelOperator[TP]>>8; // ��� ������ ��������� � ����� ������
  MassOutData[6]=ChannelOperator[TP];
  MassOutData[7]=E1KI[TP]>>8; // ����� ������ E1
  MassOutData[8]=E1KI[TP];    // ����� ��
  MassOutData[9]=Result_Pri>>24; // ��������� �������
  MassOutData[10]=Result_Pri>>16;
  MassOutData[11]=Result_Pri>>8;
  MassOutData[12]=Result_Pri;
  MassOutData[13]=MassOutData[0];
  SendDiagnostics2DspCom(MassOutData,14); 
#endif
}

void Command203(void)
{ // ���������� ������� �������� � ������ ������
  unsigned short TP, i;
  int Result_Pri;

  // ����� 2,3-��; 4,5-����� ���������
  if(MassInData[2]==1)
  {
    TP=MassInData[3]-1; // ����� ����� �����������
    if(CallerID[TP])
    { // ���� ���� ������������
      MassOutData[0]=false;
      if(!(Result_Pri=pri_call_answer((E1KI[TP]>>8)-E1_EDSS,E1KI[TP]&0xFF,CallerID[TP]))) 
      {
        ChannelOperator[TP]=(((unsigned short)MassInData[4])<<8) | MassInData[5];
        if(ChannelOperator[TP] && MassInData[6])
        { // ������� ��������� ���������� ����� ����������� � ������ ���������
          MassInData[3]= TP+LenHard+LenVirt; // ����� �����������
          // ����� ���������
          if(MassInData[4]) MassInData[5]+=LenHard+LenVirt-1;
          else MassInData[5]+=LenSport1-1;
          MassOutData[0]=InclTempComm(MassInData[3],MassInData[5],0) && InclTempComm(MassInData[5],MassInData[3],0);
        }
        else MassOutData[0]=1; // ��� ������ ���������(������� �����������!) ��� ����� ���������������
      }
    }
    Answer(&sInfoAnswer,203,1);
    if(TypeCall[TP]>0x80)
    {
      TypeCall[TP]-=0x80;
      while(ExclGenerator(1,TP));
    }
    TypeCall[TP]=0;
    if(ChannelOperator[TP])
    { // ������� ��� � ���������
      while(ExclGenerator(ChannelOperator[TP]>>8,ChannelOperator[TP]));
    }
  }
#ifdef __Control__
 MassOutData[0]=2;               // ������� 2
 MassOutData[1]=1;               // ��� ����� ����������� � � �����
 MassOutData[2]=TP;
 MassOutData[3]=31;              // ��� ������-EDSS
 MassOutData[4]=203;             // ��� ������
 MassOutData[5]=ChannelOperator[TP]>>8;// ��� ������ ��������� � ����� ������
 MassOutData[6]=ChannelOperator[TP];
 MassOutData[7]=E1KI[TP]>>8;     // ����� ������ E1
 MassOutData[8]=E1KI[TP];        // ����� ��
 MassOutData[9]=Result_Pri>>24;  // ��������� �������
 MassOutData[10]=Result_Pri>>16;
 MassOutData[11]=Result_Pri>>8;
 MassOutData[12]=Result_Pri;
 MassOutData[13]=MassOutData[0];
 SendDiagnostics2DspCom(MassOutData, 14);
#endif
}

void Command204(void)
{ // ������� �� ����� ���� ������ �� �������
  unsigned short TP, i;
  int Result_Pri;

  TP=MassInData[3]-1; // ����� ����� �����������
  cause=MassInData[6];
  if(CallerID[TP])
  { // ���� ���� ������������
    ChannelOperator[TP]=(((unsigned short)MassInData[4])<<8) | MassInData[5];
    if(!(Result_Pri=pri_call_busy((E1KI[TP]>>8)-E1_EDSS,E1KI[TP]&0xFF,CallerID[TP],cause))) MassOutData[0]=true;
  }
  MassOutData[14]=MassOutData[0];
  Answer(&sInfoAnswer,204,1);
#ifdef __Control__
 MassOutData[0]=2; // ������� 2
 MassOutData[1]=1; // ��� ����� ����������� � � �����
 MassOutData[2]=TP;
 MassOutData[3]=31; // ��� ������-EDSS
 MassOutData[4]=204; // ��� ������
 MassOutData[5]=ChannelOperator[TP]>>8; // ��� ������ ��������� � ����� ������
 MassOutData[6]=ChannelOperator[TP];
 MassOutData[7]=E1KI[TP]>>8; // ����� ������ E1
 MassOutData[8]=E1KI[TP]; // ����� ��
 MassOutData[9]=cause; // �������
 MassOutData[10]=Result_Pri>>24; // ��������� �������
 MassOutData[11]=Result_Pri>>16;
 MassOutData[12]=Result_Pri>>8;
 MassOutData[13]=Result_Pri;
 SendDiagnostics2DspCom(MassOutData, 15); 
#endif
}

void Command205(void)
{ // ��������� ���������
  unsigned short i,TP,trunkno;
  int Result_Pri=0;

  TP=MassInData[3]-1;
  if(MassInData[3])
  {
    if(CallerID[TP])
    {
      cause=MassInData[6];
      if(cause!=0xFF) Result_Pri=pri_call_hangup((E1KI[TP]>>8)-E1_EDSS,E1KI[TP]&0xFF,CallerID[TP],cause);
    }
    if(TypeCall[TP]>0x80)
    {
      TypeCall[TP]-=0x80;
      while(ExclGenerator(1,TP));
    }
    TypeCall[TP]=0;
    if(ChannelOperator[TP])
    {
      while(ExclGenerator(ChannelOperator[TP]>>8,ChannelOperator[TP]));
    }
  }
#ifdef __Control__
 MassOutData[0]=2; // ������� 2
 MassOutData[1]=1; // ��� ����� ����������� � � �����
 MassOutData[2]=TP;
 MassOutData[3]=31; // ��� ������-EDSS
 MassOutData[4]=205; // ��� ������
 MassOutData[5]=ChannelOperator[TP]>>8; // ��� ������ ��������� � ����� ������
 MassOutData[6]=ChannelOperator[TP];
 MassOutData[7]=E1KI[TP]>>8; // ����� ������ E1
 MassOutData[8]=E1KI[TP]; // ����� ��
 MassOutData[9]=cause; // �������
 MassOutData[10]=Result_Pri>>24; // ��������� �������
 MassOutData[11]=Result_Pri>>16;
 MassOutData[12]=Result_Pri>>8;
 MassOutData[13]=Result_Pri;
 MassOutData[14]=CallerID[TP]>>8; // owner
 MassOutData[15]=CallerID[TP];
 SendDiagnostics2DspCom(MassOutData, 16);
#endif
  if(TP<LenVirt) ClearTP4ISDN(TP); 
  MassOutData[0]=0;
  Answer(&sInfoAnswer,205,1);
}

// �������������� ����� �� pri ----!!!!!!! ---- �� �������� -------------- 
void Command206(void)
{
  unsigned short TP, i;
  int Result_Pri=0;

  TP=MassInData[3]-1;
 if(CallerID[TP])
 {
   // ����� ����������� ������ 
   dest=(char *)((unsigned int)MassInData[4]<<24 | (unsigned int)MassInData[5]<<16 |
        (unsigned int)MassInData[6]<<8 | (unsigned int)MassInData[7]);
   // ������� ������ ������ ���������
   complete=MassInData[8]; 
   if (!(Result_Pri=pri_call_info((E1KI[TP]>>8)-E1_EDSS,E1KI[TP]&0xFF,CallerID[TP],dest,complete))) MassOutData[0]=true;
   else MassOutData[0]=false;
 }
 Answer(&sInfoAnswer,206,1);
#ifdef __Control__
 MassOutData[0]=2; // ������� 2
 MassOutData[1]=1; // ��� ����� ����������� � � �����
 MassOutData[2]=TP;
 MassOutData[3]=31; // ��� ������-EDSS
 MassOutData[4]=206; // ��� ������
 MassOutData[5]=ChannelOperator[TP]>>8; // ��� ������ ��������� � ����� ������
 MassOutData[6]=ChannelOperator[TP];
 MassOutData[7]=E1KI[TP]>>8;// ����� ������ E1
 MassOutData[8]=E1KI[TP]; // ����� ��
 MassOutData[9]=Result_Pri>>24; // ��������� �������
 MassOutData[10]=Result_Pri>>16;
 MassOutData[11]=Result_Pri>>8;
 MassOutData[12]=Result_Pri;
 dest=(char *)atol((const char *)dest);
 MassOutData[13]=(int)dest>>24; // dest
 MassOutData[14]=(int)dest>>16;
 MassOutData[15]=(int)dest>>8;
 MassOutData[16]=(int)dest;
 MassOutData[17]=complete;
 SendDiagnostics2DspCom(MassOutData, 18);
#endif
}
 
// ��� 207-������� �������� ����� �� EDSS
int pri_call_on_incoming(int in_trunkno, int in_channel, char *in_cid_num, char *in_cid_name, 
                         char *in_rdnis, char *in_callednum, int in_complete)
{ // �-��� ���������� �� ������ EDSS
  unsigned char *pbtBuf;
  static sCommand sCmd;

  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[1]=207;
  pbtBuf=SaveInt2Buf(sCmd.BufCommand+2,in_trunkno);
  pbtBuf=SaveInt2Buf(pbtBuf,in_channel);
  pbtBuf=SaveInt2Buf(pbtBuf,(int)in_cid_num);
  pbtBuf=SaveInt2Buf(pbtBuf,(int)in_cid_name);
  pbtBuf=SaveInt2Buf(pbtBuf,(int)in_rdnis);
  pbtBuf=SaveInt2Buf(pbtBuf,(int)in_callednum);
  pbtBuf=SaveInt2Buf(pbtBuf,(int)in_complete);
  sCmd.L=(int)(pbtBuf-sCmd.BufCommand);
  sCmd.Source=UserRS485;
  sCmd.A1=0;
  while(xQueueReceive(xQueueAnswYuri,&sCmd,0));
  if(xQueueSend(xQueueSportX,&sCmd,0,0))
  {
    if(xQueueReceive(xQueueAnswYuri,&sCmd,100)) return(sCmd.A1);
  }
  return 0;
}
 
// ���������� ���� 207
int _xpri_call_on_incoming(sCommand *psCmd)
{
 unsigned short TP, i, j;
 int in_trunkno,in_channel,in_complete;
 char *pStr,*in_cid_num,*in_cid_name,*in_rdnis,*in_callednum;
 unsigned char *pbtBuf;

 /* in_trunkno   - ����� ������ �1
	in_channel   - ����� ��
	in_cid_num   - ����. �� ����� ���������
	in_cid_name  - ����. �� ��� ���������
	in_rdnis     - ����. �� ����� ����������������
	in_callednum - [0]='s', [1]='\0'
	in_complete  - ����� ������ ��������� */
 pbtBuf=RestoreIntFromBuf(MassInData+2,&in_trunkno);
 pbtBuf=RestoreIntFromBuf(pbtBuf,&in_channel);
 pbtBuf=RestoreIntFromBuf(pbtBuf,(int *)&in_cid_num);
 pbtBuf=RestoreIntFromBuf(pbtBuf,(int *)&in_cid_name);
 pbtBuf=RestoreIntFromBuf(pbtBuf,(int *)&in_rdnis);
 pbtBuf=RestoreIntFromBuf(pbtBuf,(int *)&in_callednum);
 RestoreIntFromBuf(pbtBuf,&in_complete);
 psCmd->A1=0;
 if(in_complete)
 { // ����� ������ ���������
   for(TP=0;TP<LenVirt;TP++)
   { // ����� ��������� ����� �����������
    if(!FoundTP(TP,true)) break;
   }
   in_trunkno+=E1_EDSS;
   if(TP<LenVirt)
   { // ����� �������
     CallerID[TP]=(0x0100+((unsigned short)g_IpParams.ucDataSIPR[3]<<8)) | (TP+1); // ��� ��������� (������� - owner)
     ChannelOperator[TP]=0; // ��� � ����� ������ �������� (���� ����������)
     E1KI[TP]=(in_trunkno<<8) | in_channel;
     CreateCommuteTP_E1KI(TP,E1KI[TP]);
     psCmd->A1=CallerID[TP];
     xQueueSend(xQueueAnswYuri,psCmd,0,0);

     MassOutData[0]=1;// ��� ����� ����������� � � �����
     MassOutData[1]=TP;
     MassOutData[2]=31; // ��� ������ - EDSS
     MassOutData[3]=207; // ��� ������
     pStr=(char *)NTelefon[TP];
     strcpy(pStr,in_cid_num);
     i=strlen(pStr);
     if(i>7) pStr+=i-7;
     in_cid_num=(char *)atol(pStr); // ����� ����������� (������� �� 7 ������� ��������)
     pbtBuf=SaveInt2Buf(MassOutData+4,(int)in_cid_num);
     strcpy((char *)NameTelefon[TP],in_cid_name);
     pbtBuf=SaveInt2Buf(pbtBuf,(int)in_cid_name);
     pbtBuf=SaveInt2Buf(pbtBuf,(int)in_callednum);
     MassOutData[16]=in_complete; // ��� �� ����� ��������
     MassOutData[17]=in_trunkno; // ����� ������ E1
     MassOutData[18]=in_channel; // ����� ��
     in_rdnis=(char *)atol((const char *)in_rdnis);
     pbtBuf=SaveInt2Buf(MassOutData+19,(int)in_rdnis);
     in_callednum=(char *)atol((const char *)in_callednum);
     SaveInt2Buf(pbtBuf,(int)in_callednum);
     memcpy(MassOutData+27,NTelefon[TP],MAX_DIGIT_TLFN+1); // ������ ����� ��������� ��������
     OutAbon(MassOutData,27+MAX_DIGIT_TLFN+1);
   }
   else SendStr2IpLogger("$ERROR$pri_call_on_incoming: No free CP!!!");
 }
 if(!psCmd->A1) xQueueSend(xQueueAnswYuri,psCmd,0,0);
 return psCmd->A1;
}
 
// ��� 208 �������� �������������� ����� � EDSS - �� �������� !!!!!!
int pri_call_on_info(int in_trunkno, int in_channel, int in_owner, char *in_dest)
{
  unsigned char *pbtBuf;
  static sCommand sCmd;

  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[1]=208;
  pbtBuf=SaveInt2Buf(sCmd.BufCommand+2,in_trunkno);
  pbtBuf=SaveInt2Buf(pbtBuf,in_channel);
  pbtBuf=SaveInt2Buf(pbtBuf,in_owner);
  pbtBuf=SaveInt2Buf(pbtBuf,(int)in_dest);
  sCmd.L=(int)(pbtBuf-sCmd.BufCommand);
  sCmd.Source=UserRS485;
  sCmd.A1=0;
  xQueueSend(xQueueSportX,&sCmd,20,0);
  return(true);
}

// ���������� ���� 208
void _xpri_call_on_info(void)
{
  unsigned short TP,i;
  int in_trunkno,in_channel,in_owner;
  char *in_dest;
  unsigned char *pbtBuf;

 /* in_trunkno   - ����� ������ �1
	in_channel   - ����� ��
	in_cid_num   - ����. �� ����� ���������
	in_cid_name  - ����. �� ��� ���������
	in_rdnis     - ����. �� ����� ����������������
	in_callednum - [0]='s', [1]='\0'
	in_complete	 - ����� ������ ��������� */
  pbtBuf=RestoreIntFromBuf(MassInData+2,&in_trunkno);
  pbtBuf=RestoreIntFromBuf(pbtBuf,&in_channel);
  pbtBuf=RestoreIntFromBuf(pbtBuf,&in_owner);
  RestoreIntFromBuf(pbtBuf,(int *)&in_dest);
/*  in_trunkno   - ����� ������ �1
	in_channel   - ����� ��
	in_owner	 - �������, �������� �� ����� �� ������ � � �������� � pri_call_alert
	in_dest		 - ����. �� ��������� ����� char[256] */
  for(TP=0;TP<LenVirt;TP++)
    if (in_owner==CallerID[TP]) break;
  in_trunkno+=E1_EDSS;
  if(TP<LenVirt) 
  {
    ReGroupTP((in_trunkno<<8) | in_channel, TP); // ������������ ���������� ��� ������������ ��
    MassOutData[16]=MassOutData[0];
    MassOutData[0]=1;// ��� ����� ����������� � � �����
    MassOutData[1]=TP;
    MassOutData[2]=31; // ��� ������ - EDSS
    MassOutData[3]=208; // ��� ������
    MassOutData[4]=ChannelOperator[TP]>>8;// ��� ������ ��������� � ����� ������
    MassOutData[5]=ChannelOperator[TP];
    MassOutData[6]=in_trunkno; // ����� ������ E1
    MassOutData[7]=in_channel; // ����� ��
    pbtBuf=SaveInt2Buf(MassOutData+8,in_owner);
    in_dest=(char *)atol((const char *)in_dest);
    SaveInt2Buf(pbtBuf,(int)in_dest);
    OutAbon(MassOutData, 17); 
  }
}

// ��� 209 ������� Alert � EDSS (���������� ������� ��������-����� �������� �������)
// (����� ���������� ����, �� ��� ����� ��� inband==1)
int pri_call_on_alert(int in_trunkno, int in_channel, int in_owner, int in_inband)
{
  unsigned char *pbtBuf;
  static sCommand sCmd;

  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[1]=209;
  pbtBuf=SaveInt2Buf(sCmd.BufCommand+2,in_trunkno);
  pbtBuf=SaveInt2Buf(pbtBuf,in_channel);
  pbtBuf=SaveInt2Buf(pbtBuf,in_owner);
  pbtBuf=SaveInt2Buf(pbtBuf,in_inband);
  sCmd.L=(int)(pbtBuf-sCmd.BufCommand);
  sCmd.Source=UserRS485;
  sCmd.A1=0;
  xQueueSend(xQueueSportX,&sCmd,20,0);
  return(true);
}

void RegroupTPAndStopGenerator(unsigned short TP,int in_trunkno,int in_channel)
{
  ReGroupTP((in_trunkno<<8) | in_channel, TP); // ������������ ���������� ��� ������������ ��   
  if(TypeCall[TP]>0x80)
  {
    TypeCall[TP]-=0x80;
    while(ExclGenerator(1,TP));
  }
  TypeCall[TP]=0;
  if(ChannelOperator[TP])
  {
    while(ExclGenerator(ChannelOperator[TP]>>8,ChannelOperator[TP]));
  }
}

// ���������� ���� 209
void _xpri_call_on_alert(void)
{
  unsigned short TP;
  unsigned char i,j,k;
  int in_trunkno,in_channel,in_owner,in_band;
  unsigned char *pbtBuf;
/*  in_trunkno   - ����� ������ �1
	in_channel   - ����� �� (�� ���� ��� setup, �� �.�. ������������ � ����. ����������
	in_owner     - �������, �������� �� ����� �� ������ � � �������� � pri_call_alert
	in_inband    - ����=1, �� ������� ��������, �� ���� ��� */

  pbtBuf=RestoreIntFromBuf(MassInData+2,&in_trunkno);
  pbtBuf=RestoreIntFromBuf(pbtBuf,&in_channel);
  pbtBuf=RestoreIntFromBuf(pbtBuf,&in_owner);
  RestoreIntFromBuf(pbtBuf,&in_band);

  for(TP=0;TP<LenVirt;TP++)
  { if(in_owner==CallerID[TP]) break; }
  in_trunkno+=E1_EDSS;
  if(TP<LenVirt) 
  {
    RegroupTPAndStopGenerator(TP,in_trunkno,in_channel);
    if(!SelectEDSS[TP])
    { // ��������� ���������� 2-� ������� �� � ���������
      i=TP+LenHard+LenVirt; // ����� �����������
      if(ChannelOperator[TP])
      { // ����� ��������� ���� - ���� �������������� � ������ �����������
        j=ChannelOperator[TP]>>8;
        k=ChannelOperator[TP];
        if(j) k+=LenHard+LenVirt-1;
        else k+=LenSport1-1;
        InclTempComm(i,k,0);
        InclTempComm(k,i,0);
      }
      if(!in_band) TypeCall[TP]=CallKPV | 0x80; // �������� ���
    }
//SendStr2IpLogger("$DEBUG$PRI_209: wChan=%d, wOper=%d",TP,ChannelOperator[TP]);
    MassOutData[16]=MassOutData[0];
    MassOutData[0]=1;   // ��� ����� ����������� � � �����
    MassOutData[1]=TP;
    MassOutData[2]=31;  // ��� ������ - EDSS
    MassOutData[3]=209; // ��� ������
    MassOutData[4]=ChannelOperator[TP]>>8; // ��� ������ ��������� � ����� ������
    MassOutData[5]=ChannelOperator[TP];
    MassOutData[6]=in_trunkno; // ����� ������ E1
    MassOutData[7]=in_channel; // ����� ��
    pbtBuf=SaveInt2Buf(MassOutData+8,in_owner);
    SaveInt2Buf(pbtBuf,in_band);
    OutAbon(MassOutData, 17); 
  }
}

// ��� 210 ������� Progress
int pri_call_on_progress(int in_trunkno, int in_channel, int in_owner, int in_cause, int in_inband)
{
  unsigned char *pbtBuf;
  static sCommand sCmd;

  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[1]=210;
  pbtBuf=SaveInt2Buf(sCmd.BufCommand+2,in_trunkno);
  pbtBuf=SaveInt2Buf(pbtBuf,in_channel);
  pbtBuf=SaveInt2Buf(pbtBuf,in_owner);
  pbtBuf=SaveInt2Buf(pbtBuf,in_cause);
  pbtBuf=SaveInt2Buf(pbtBuf,in_inband);
  sCmd.L=(int)(pbtBuf-sCmd.BufCommand);
  sCmd.Source=UserRS485;
  sCmd.A1=0;
  xQueueSend(xQueueSportX,&sCmd,20,0);
  return true;
}

// ���������� ���� 210
int _xpri_call_on_progress(void)
{
  unsigned short TP;
  unsigned char i,j,k;
  int in_trunkno,in_channel,in_owner,in_cause,in_band;
  unsigned char *pbtBuf;

// in_trunkno   - ����� ������ �1
// in_channel   - ����� �� (�� ���� ��� setup, �� �.�. ������������ � ����. ����������
// in_owner  - �������, �������� �� ����� �� ������ � � �������� � pri_call_alert
// in_cause  - ������� - �����, ������������ ����� � �.�.
// in_inband  - ����=1, ���� "������" 
  pbtBuf=RestoreIntFromBuf(MassInData+2,&in_trunkno);
  pbtBuf=RestoreIntFromBuf(pbtBuf,&in_channel);
  pbtBuf=RestoreIntFromBuf(pbtBuf,&in_owner);
  pbtBuf=RestoreIntFromBuf(pbtBuf,&in_cause);
  RestoreIntFromBuf(pbtBuf,&in_band);

  for(TP=0;TP<LenVirt;TP++)
    if (in_owner==CallerID[TP]) break;
  in_trunkno+=E1_EDSS;
  if(TP<LenVirt)
  {
    RegroupTPAndStopGenerator(TP,in_trunkno,in_channel);
/*    if(!in_band && (in_cause>1)) TypeCall[TP]=CallBusy; // ������ ������� "������" ���� �� �� ������
    else
    {
      if(!SelectEDSS[TP] && ChannelOperator[TP])
      { // ��������� ���������� 2-� ������� �� � ��������� 
        i= TP+LenHard+LenVirt; // ����� �����������
        j=ChannelOperator[TP]>>8;
        k=ChannelOperator[TP];
        if(j) k+=LenHard+LenVirt-1;
        else k+=LenSport1-1;// ����� ���������
        if(in_cause==0x12) InclTempComm(k,i,0);// �������� ������
      }
    }*/
  }
  MassOutData[21]=MassOutData[0];
  MassOutData[0]=1; // ��� ����� ����������� � � �����
  MassOutData[1]=TP;
  MassOutData[2]=31; // ��� ������ - EDSS
  MassOutData[3]=210; // ��� ������
  MassOutData[4]=ChannelOperator[TP]>>8; // ��� ������ ��������� � ����� ������
  MassOutData[5]=ChannelOperator[TP];
  MassOutData[6]=in_cause;
  MassOutData[7]=in_trunkno; // ����� ������ E1
  MassOutData[8]=in_channel; // ����� ��
  pbtBuf=SaveInt2Buf(MassOutData+9,in_owner);
  pbtBuf=SaveInt2Buf(pbtBuf,in_band);
  SaveInt2Buf(pbtBuf,in_cause);
  OutAbon(MassOutData, 22);
  return true;
}
  
// ��� 211  �������� ���������  � EDSS (����� ���������� ���� �� ��� ����� ��� ��� ���-�� ��������)
int pri_call_on_speaker_enable(int in_trunkno, int in_channel, int in_owner)
{
  unsigned char *pbtBuf;
  static sCommand sCmd;

  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[1]=211;
  pbtBuf=SaveInt2Buf(sCmd.BufCommand+2,in_trunkno);
  pbtBuf=SaveInt2Buf(pbtBuf,in_channel);
  SaveInt2Buf(pbtBuf,in_owner);
  sCmd.L=(int)(pbtBuf-sCmd.BufCommand);
  sCmd.Source=UserRS485;
  sCmd.A1=0;
  xQueueSend(xQueueSportX,&sCmd,20,0);
  return(true);
}

// ���������� ���� 211
void _xpri_call_on_speaker_enable(void)
{
  unsigned short TP, i;
  int in_trunkno,in_channel,in_owner;
  unsigned char *pbtBuf;

 /* in_trunkno   - ����� ������ �1
    in_channel   - ����� �� (�� ���� ��� setup, �� �.�. ������������ � ����. ����������
    in_owner     - �������, �������� �� ����� �� ������ � � �������� � pri_call_alert */
  pbtBuf=RestoreIntFromBuf(MassInData+2,&in_trunkno);
  pbtBuf=RestoreIntFromBuf(pbtBuf,&in_channel);
  RestoreIntFromBuf(pbtBuf,&in_owner);

  for(TP=0;TP<LenVirt;TP++)
    if (in_owner==CallerID[TP]) break;
  in_trunkno+=E1_EDSS;
  if(TP<LenVirt)
  {
    RegroupTPAndStopGenerator(TP,in_trunkno,in_channel);
    MassOutData[12]=MassOutData[0];
    MassOutData[0]=1; // ��� ����� ����������� � � �����
    MassOutData[1]=TP;
    MassOutData[2]=31; // ��� ������ - EDSS
    MassOutData[3]=211; // ��� ������
    MassOutData[4]=ChannelOperator[TP]>>8;// ��� ������ ��������� � ����� ������
    MassOutData[5]=ChannelOperator[TP];
    MassOutData[6]=in_trunkno; // ����� ������ E1
    MassOutData[7]=in_channel; // ����� ��
    SaveInt2Buf(MassOutData+8,in_owner); // ��� (�����) ������������
    OutAbon(MassOutData, 13); 
  }
}

// ��� 212  ������� ����� � EDSS (���������� ������� ������ ������ - ���������� ����)
int pri_call_on_answer(int in_trunkno, int in_channel, int in_owner)
{
  unsigned char *pbtBuf;
  static sCommand sCmd;

  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[1]=212;
  pbtBuf=SaveInt2Buf(sCmd.BufCommand+2,in_trunkno);
  pbtBuf=SaveInt2Buf(pbtBuf,in_channel);
  pbtBuf=SaveInt2Buf(pbtBuf,in_owner);
  sCmd.L=(int)(pbtBuf-sCmd.BufCommand);
  sCmd.Source=UserRS485;
  sCmd.A1=0;
  xQueueSend(xQueueSportX,&sCmd,20,0);
  return true;
}

// ���������� ���� 212
void _xpri_call_on_answer(void)
{
  unsigned short TP;
  int in_trunkno,in_channel,in_owner;
  unsigned char *pbtBuf;

/* in_trunkno   - ����� ������ �1
   in_channel   - ����� �� (�� ���� ��� setup, �� �.�. ������������ � ����. ����������
   in_owner     - �������, �� �������� ������ setup  */
  pbtBuf=RestoreIntFromBuf(MassInData+2,&in_trunkno);
  pbtBuf=RestoreIntFromBuf(pbtBuf,&in_channel);
  RestoreIntFromBuf(pbtBuf,&in_owner);

  for(TP=0;TP<LenVirt;TP++)
  {
    if(in_owner==CallerID[TP]) break;
  }
  if(TP<LenVirt)
  {
    in_trunkno+=E1_EDSS;
    RegroupTPAndStopGenerator(TP,in_trunkno,in_channel);
    if(!SelectEDSS[TP])
    { // ��������� ���������� 2-� ������� �� � ��������� �� ������, ���� ��� ������� 209 (���������� ����� ��������)
      if(ChannelOperator[TP])
      { // ����� ��������� ���� - ���� �������������� � ������ �����������
        unsigned char i,k;
        i=TP+LenHard+LenVirt; // ����� �����������
        k=ChannelOperator[TP];
        if(ChannelOperator[TP]>>8) k+=LenHard+LenVirt-1;
        else k+=LenSport1-1;
        InclTempComm(i,k,0);
        InclTempComm(k,i,0);
      }
    }
//SendStr2IpLogger("$DEBUG$PRI_212: wChan=%d, wOper=%d",TP,ChannelOperator[TP]);
    MassOutData[0]=1; // ��� ����� ����������� � � �����
    MassOutData[12]=MassOutData[0];
    MassOutData[1]=TP;
    MassOutData[2]=31; // ��� ������ - EDSS
    MassOutData[3]=212; // ��� ������
    MassOutData[4]=ChannelOperator[TP]>>8; // ��� ������ ��������� � ����� ������
    MassOutData[5]=ChannelOperator[TP];
    MassOutData[6]=in_trunkno; // ����� ������ E1
    MassOutData[7]=in_channel; // ����� ��
    SaveInt2Buf(MassOutData+8,in_owner); // ��� (�����) ������������
    OutAbon(MassOutData,13);
    if(IsSelectorMessage(SelectEDSS[TP]))
    {
      ChanWav[TP]=TP+LenHard+LenVirt;
      PlayWav[TP]=0x38; // ������ �������� �����
    }
  }
}

// ��� 213 ������� ����� � EDSS
int pri_call_on_disconnect(int in_trunkno,int in_channel,
                           int in_owner,unsigned int in_band,int in_cause)
{
  unsigned char *pbtBuf;
  static sCommand sCmd;
  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[1]=213;
  pbtBuf=SaveInt2Buf(sCmd.BufCommand+2,in_trunkno);
  pbtBuf=SaveInt2Buf(pbtBuf,in_channel);
  pbtBuf=SaveInt2Buf(pbtBuf,in_owner);
  pbtBuf=SaveInt2Buf(pbtBuf,in_band);
  pbtBuf=SaveInt2Buf(pbtBuf,in_cause);
  sCmd.L=(int)(pbtBuf-sCmd.BufCommand);
  sCmd.Source=UserRS485;
  sCmd.A1=0;
  xQueueSend(xQueueSportX,&sCmd,20,0);
  return true;
}

// ���������� ���� 213
void _xpri_call_on_disconnect(void)
{
  unsigned short i, TP;
  int in_trunkno,in_channel,in_owner,in_cause,in_band;
// in_trunkno   - ����� ������ �1
// in_channel   - ����� �� (�� ���� ��� setup, �� �.�. ������������ � ����. ����������
// in_owner  - �������, �������� �� ����� �� ������ � � �������� � pri_call_alert
// in_cause  - ������� - �����, ������������ ����� � �.�.
// in_inband  - ����=1, ���� "������" 
  unsigned char *pbtBuf;

  pbtBuf=RestoreIntFromBuf(MassInData+2,&in_trunkno);
  pbtBuf=RestoreIntFromBuf(pbtBuf,&in_channel);
  pbtBuf=RestoreIntFromBuf(pbtBuf,&in_owner);
  pbtBuf=RestoreIntFromBuf(pbtBuf,&in_band);
  RestoreIntFromBuf(pbtBuf,&in_cause);

  for(TP=0;TP<LenVirt;TP++)
    if(in_owner==CallerID[TP]) break;
  in_trunkno+=E1_EDSS;
  if(TP<LenVirt)
  {
    RegroupTPAndStopGenerator(TP,in_trunkno,in_channel);
    if(ChannelOperator[TP] && (!in_band) && (!SelectEDSS[TP])) TypeCall[TP]=CallBusy;
  }
  MassOutData[0]=1; // ��� ����� ����������� � � �����
  MassOutData[1]=TP;
  MassOutData[2]=31; // ��� ������ - EDSS
  MassOutData[3]=213; // ��� ������
  MassOutData[4]=ChannelOperator[TP]>>8; // ��� ������ ��������� � ����� ������
  MassOutData[5]=ChannelOperator[TP];
  MassOutData[6]=in_cause;
  MassOutData[7]=in_trunkno; // ����� ������ E1
  MassOutData[8]=in_channel; // ����� ��
  pbtBuf=SaveInt2Buf(MassOutData+9,in_owner); // ��� (�����) ������������
  pbtBuf=SaveInt2Buf(pbtBuf,in_band);
  pbtBuf=SaveInt2Buf(pbtBuf,in_cause); // ������� (������)
  in_owner=(int)atol((const char *)&NTelefon[TP][0]);
  SaveInt2Buf(pbtBuf,in_owner); // ����� �����������
  OutAbon(MassOutData, 25);
}

// ��� 214 ����� - ����� ��� ����������
int pri_call_on_release(int in_trunkno, int in_channel, int in_owner, int in_cause)
{
  unsigned char *pbtBuf;
  static sCommand sCmd;

  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[1]=214;
  pbtBuf=SaveInt2Buf(sCmd.BufCommand+2,in_trunkno);
  pbtBuf=SaveInt2Buf(pbtBuf,in_channel);
  pbtBuf=SaveInt2Buf(pbtBuf,in_owner);
  pbtBuf=SaveInt2Buf(pbtBuf,in_cause);
  sCmd.L=(int)(pbtBuf-sCmd.BufCommand);
  sCmd.Source=UserRS485;
  sCmd.A1=0;
  xQueueSend(xQueueSportX,&sCmd,20,0);
  return true;
}

// ���������� ���� 214
void _xpri_call_on_release(void)
{
  unsigned short TP, i;
  int in_trunkno,in_channel,in_owner,in_cause;
  unsigned char *pbtBuf;

// in_trunkno   - ����� ������ �1
// in_channel   - ����� �� (�� ���� ��� setup, �� �.�. ������������ � ����. ����������
// in_owner  - �������, �������� �� ����� �� ������ � � �������� � pri_call_alert
// in_cause  - ������� - �����, ������������ ����� � �.�.
  pbtBuf=RestoreIntFromBuf(MassInData+2,&in_trunkno);
  pbtBuf=RestoreIntFromBuf(pbtBuf,&in_channel);
  pbtBuf=RestoreIntFromBuf(pbtBuf,&in_owner);
  RestoreIntFromBuf(pbtBuf,&in_cause);
  
 for(TP=0;TP<LenVirt;TP++)
   if (in_owner==CallerID[TP]) break;
 in_trunkno+=E1_EDSS;
 if(TP<LenVirt)
 {
   RegroupTPAndStopGenerator(TP,in_trunkno,in_channel);
   MassOutData[17]=MassOutData[0];
   MassOutData[0]=1; // ��� ����� ����������� � � �����
   MassOutData[1]=TP;
   MassOutData[2]=31;  // ��� ������ - EDSS
   MassOutData[3]=214; // ��� ������
   MassOutData[4]=ChannelOperator[TP]>>8; // ��� ������ ��������� � ����� ������
   MassOutData[5]=ChannelOperator[TP];
   MassOutData[6]=in_cause;
   MassOutData[7]=in_trunkno; // ����� ������ E1
   MassOutData[8]=in_channel; // ����� ��
   pbtBuf=SaveInt2Buf(MassOutData+9,in_owner); // ��� (�����) ������������
   SaveInt2Buf(pbtBuf,in_cause); // ������� (������)
   OutAbon(MassOutData, 18); 
 }
}
#endif // end __ISDN__

