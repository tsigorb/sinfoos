#include "..\Sinfo.h"

///////////////////////////////////////////////////////////////////////////////
void *pvPortMalloc(size_t xSize);
void vPortFree(void *pv);
unsigned char IsCurrentTickCountGT(unsigned int dwTick0);
unsigned char IsCurrentTickCountLT(unsigned int dwTick0);

///////////////////////////////////////////////////////////////////////////////
extern unsigned char TablMKC[LenMKC][LenCommutMKC];// ������� ���������� ������� ���
extern unsigned char IndOutE1;			// ������� ���������� ���� �� �1
extern unsigned short OutE1[];			// ������ ������������ ������ �� �1 (������� ������ �������� � OutE1[0])
										// [count, Addr, Command, Len, N Addr, (N Data)]
extern unsigned short InE1[LenInE1+1];	// ������ ����������� ������ �� �1 (������� ������ ������ � InE1[0])
										// [����� �������� ����, Addr, Command, Len, N Data]

extern unsigned short ParamUKC[];		// ��������� (�� 1 ����) ���������� ������ ������� ���(��.) � E1(��.) 
										// � ������� ������������� ��� �������� ������� � ��������� SNMP

/*		��������� ����������������� ������� ��� � MK� TestBoard (�� 2 ����):
		00 - ������
		01 - ������ �� ��������
		10 - ������ ��������
		11 - ������ ��������.*/
extern unsigned int TestBoard; // �������� ����������������� (�� 2 ����) ���������� ���� (��.) � ������� �1(��.)

///////////////////////////////////////////////////////////////////////////////
unsigned char FailureE1[4][10]; // ��������� ��������� ������ ������� E1
unsigned short ErrMallocE1; // ������ �������������� ������ ��� �������� ������� ������ ��� E1
unsigned short ErrAnswerE1; // ���������� ������ ������ �1
unsigned short ErrSwapE1; // ������ ������ � ������� �1

static sListWrite ListWrite; // ������ ������ ��� �������� �� ���

static unsigned short CommandTestE1[]= // ������� ������ ��������� ������ �1 �� ������� �5...�D
		{0,0,2,9,0xC5,0xC7,0xC9,0xCB,0xC6,0xC8,0xCA,0xCC,0xCD};
static unsigned short CmdRdMKC_C0_C4[]= // ������� ������ ������ �1 �� ������� C0...C4
		{0,0,2,5,0xC0,0xC1,0xC2,0xC3,0xC4};
static unsigned short CommandDataE1[]= // ������� ������ ��������� ���������� ��������� ����������
		{0,0,2,8,0,0,0,0,0,0,0,0};

static unsigned char StartE1; // ������� ������ �� SPORT1
static unsigned int TOutCmdMKC=0;
static unsigned short IndexE1=0x1000;
static unsigned char TestE1=0, // ��������� ������� �������� ������� �1
                     CountRestart=10; // ������������ ���������� ������������ Sport ��� �������� MKC

static char strMKC[250];

///////////////////////////////////////////////////////////////////////////////
static unsigned char CalcCRC4MKC(unsigned short *Addr,unsigned char count)
{
  unsigned char i;
  unsigned short CRC=0;

  for(i=1;i<=count;i++) CRC-=Addr[i]; // CRC � 0-� ������� ������� �� ���������
  return CRC;
}

unsigned char InAnswerE1(unsigned char *pbtCmdSportX)
{
  unsigned int dwIRQ=cli();
  unsigned short count=InE1[0];
  sti(dwIRQ);

  if(count>0)
  {
/*
if((InE1[2]!=2) || (count!=22))
{ // �� ������� ����� �� ������� ������������ ������
char i;
static char strBuf[20];
LockPrintf();
strcpy(strMKC,"$DEBUG$InAnswerE1");
sprintf(strBuf," %04x",count);
strcat(strMKC,strBuf);
for(i=1;i<count;i++)
{
  sprintf(strBuf," %04x",InE1[i]);
  strcat(strMKC,strBuf);
}
sprintf(strBuf," %04x",CalcCRC4MKC(InE1,count));
strcat(strMKC,strBuf);
UnlockPrintf();
SendStr2IpLogger(strMKC);
}
*/
    if(CalcCRC4MKC(InE1,count)) count=0;
    else StartE1=0;
    InE1[0]=0; // ����� �������� �������� ����
  }
  if(IsCurrentTickCountGT(TOutCmdMKC))
  {
    *pbtCmdSportX=0;
    StartE1=0;
    InE1[LenInE1]=0xff; // ��������� �����
    if(TestE1)
    { // ������� ����� �� ������� -> ������ �� �������
      unsigned char NMod;

      TestE1=0;
      ErrAnswerE1++;
      NMod=CommandTestE1[1]; // ����� ������������ ������
      FailureE1[NMod][9]=1; // ��������� �������� ���������� ������ ������
      if(!(ParamUKC[6] & 0x60))
      {
        if(CountRestart)
        { CountRestart--; RestartSPORT1(); }
      }
      if(NMod==0) TestBoard=TestBoard & 0xFCFFFFFF;
      else if(NMod==1) TestBoard=TestBoard & 0xF3FFFFFF;
      else if(NMod==2) TestBoard=TestBoard & 0xCFFFFFFF;
      else if(NMod==3) TestBoard=TestBoard & 0x3FFFFFFF;
    }
  }
  return count;
}

void OutCommandE1(unsigned short *AdrCom)
{ // ���������� ������� ��� �������� �� ���
  unsigned char i,count;

  count=AdrCom[3]; // ����� ���� ������ � �������
  if(AdrCom[2]==1)
  { // ��� ������� ������ ���������� ���� �������-������
    count<<=1;
  }
  count+=3;
  if(count>LenInE1-3) count=LenInE1-3;
  for(i=1;i<=count;i++) OutE1[i]=AdrCom[i];
  OutE1[i]=CalcCRC4MKC(OutE1,count); // ������ ����������� ����� 
  count++;
/*
if((AdrCom[2]!=2) || (count!=0x0d))
{ // �� ������� ������� ������������ ������
static char strBuf[20];
LockPrintf();
strcpy(strMKC,"$DEBUG$OutCommandE1");
sprintf(strBuf," %04x",count);
strcat(strMKC,strBuf);
for(i=1;i<=count;i++)
{
  sprintf(strBuf," %04x",OutE1[i]);
  strcat(strMKC,strBuf);
}
UnlockPrintf();
SendStr2IpLogger(strMKC);
}
*/
  for(i=1;i<=count;i++) OutE1[i]=a_expand(OutE1[i]); // ������������� �� �-������
  InE1[0]=0; // �������� ������ ��� ������ ������ �� ��
  IndOutE1=0;
  OutE1[0]=count;
  StartE1=1; // ��������� �������� ������ �������� �������
  TOutCmdMKC=xTaskGetTickCount()+200;
}

unsigned char WriteE1(unsigned char NMod, unsigned char Reg, unsigned char data)
{ // �������� ������� ������ �� ������ ���
 unsigned short i;
 xListWrE1  *AdrInI, *AdrI, *AdrOutI;

 switch(NMod) 
 {
 	case 0: i=0x1000; break;
 	case 1: i=0x2000; break;
 	case 2: i=0x4000; break;
 	case 3: i=0x8000; break;
 	default: return 0; 
 }
 if(i & ParamUKC[0]) 
 {
    AdrOutI=pvPortMalloc(sizeof (xListWrE1));
    if(!AdrOutI)
    { ErrMallocE1++; return false; }
	AdrOutI -> NumMod=NMod;
	AdrOutI -> Command=1;
	AdrOutI -> Addr=Reg;
	AdrOutI -> Data=data;
	AdrOutI -> pxNextCom=0;
	AdrInI=(xListWrE1 *)ListWrite.pxHead;
	if(!AdrInI) ListWrite.pxHead=AdrOutI;
	else 
	{
	  do 
	  {
	    AdrI=AdrInI;
	    AdrInI=(xListWrE1 *)AdrInI->pxNextCom;
	  }
	  while(AdrInI);
	  AdrI->pxNextCom=AdrOutI; // ������ ������ ������ ��������
	} 
    if(Reg<LenCommutMKC) TablMKC[NMod][Reg]=data; // ������ � ������� ���������� ��� �������������� ��� ����������� ������ ���
	return 1;
 }
 return 0;
}

unsigned char ReadE1(unsigned char NMod, unsigned char Reg)
{ // �������� ������� ������ �� ������ ���
  unsigned short i;
  unsigned char cmd107=0;
  xListWrE1 *AdrInI,*AdrI,*AdrOutI;

  if(NMod>=107)
  { NMod-=107; cmd107=107; }
  switch(NMod)
  {
    case 0: i=0x1000; break;
    case 1: i=0x2000; break;
    case 2: i=0x4000; break;
    case 3: i=0x8000; break;
    default: return 0; 
  }
  if(i & ParamUKC[0])
  {
    AdrOutI=pvPortMalloc(sizeof (xListWrE1));
    if(!AdrOutI)
    { ErrMallocE1++; return false; }
    AdrOutI -> NumMod=NMod;
    AdrOutI -> Command=2;
    AdrOutI -> Addr=Reg;
    AdrOutI -> Data=cmd107;
    AdrOutI -> pxNextCom=0;
    AdrInI=(xListWrE1 *) ListWrite.pxHead;
	if(!AdrInI) ListWrite.pxHead=AdrOutI;
	else 
	{
	  do
	  {
	    AdrI=AdrInI;
	    AdrInI=(xListWrE1 *) AdrInI -> pxNextCom;
	  }
	  while (AdrInI);
	  AdrI->pxNextCom=AdrOutI;  // ������ ������ ������ ��������
	}
    return 1;
  }
  return 0;
}

void InitVarsMKC(void)
{
  unsigned char i;
  ListWrite.pxHead=NULL; 
  InE1[0]=OutE1[0]=0;
  InE1[LenInE1]=0xff;
  TestBoard=TestBoard & 0x00FFFFFF;
  for(i=0;i<LenMKC;i++)
  {
    FailureE1[i][8]=0x10; // ������ ��� �� ��������
    FailureE1[i][9]=0x3;
  }
}

void PeriodicalTestMKC(void)
{
  unsigned char NMod,i;
  static unsigned int SysTime=0;

  if(!SysTime) SysTime=xTaskGetTickCount();
  if(!(ParamUKC[0] & 0xF000) || TestE1 || StartE1) return;
  if(IsCurrentTickCountGT(SysTime))
  { // ����� ������ �������, ����������� �������� ������ �����
    if(IndexE1==0x1000)
    { // ������ ������� ������ ��������� ��������� �������
      for(NMod=0;NMod<LenMKC;NMod++)
      {
        if(!(ParamUKC[0] & (0x1000 << NMod))) FailureE1[NMod][9]=0x03; // ��������� �������� - ������ ��������
        else
          if(FailureE1[NMod][9]==0x03) FailureE1[NMod][9]=0x00; // �������������� �������� - ������ �������
      }
    }
    switch(IndexE1 & ParamUKC[0])
    { // ����������� �������� ����������� ������� �1
      case 0x1000: CommandTestE1[1]=0; break; // 0-� �����
      case 0x2000: CommandTestE1[1]=1; break; // 1-� �����
      case 0x4000: CommandTestE1[1]=2; break; // 2-� �����
      case 0x8000: CommandTestE1[1]=3; break; // 3-� �����
      default: CommandTestE1[1]=0xFF; break;
    }
    if(CommandTestE1[1]<4) 
    { // �������� ������� ������ ���������
      OutCommandE1(CommandTestE1);
      TestE1=3;
/*{
LockPrintf();
sprintf(strMKC,"$DEBUG$Send cmdTEST for MKC=%d",
        CommandTestE1[1]);
UnlockPrintf();
SendStr2IpLogger(strMKC);
}*/
    }
    IndexE1<<=1; // ��������� ������
    if(IndexE1==0) IndexE1=0x1000;
    SysTime=xTaskGetTickCount()+250; // ���� ����������������� ������� �1 ������ 250 �� �� ������
  }
}

void _xDelCmdMKC(xListWrE1 *AdrI)
{
  if(ListWrite.pxHead==AdrI) ListWrite.pxHead=AdrI->pxNextCom;
  else
  {
    xListWrE1 *AdrP=ListWrite.pxHead;
    while(AdrP->pxNextCom!=AdrI) AdrP=AdrP->pxNextCom;
    AdrP->pxNextCom=AdrI->pxNextCom;
  }
  vPortFree((void *)AdrI);
}

unsigned short ProcessBoardMKC(unsigned char *MasOut,unsigned char *pbtCmdSportX)
{ // ������ � �������� ���
  unsigned short i,NMod,wSz;
  unsigned int j;
  xListWrE1 *AdrI;

  wSz=0;
  if(ParamUKC[0] & 0xF000)
  { // ���� �� ����������� ����� �1
    if(StartE1)
    { // ���� �������� ������� �� �1
      static sInfoKadr sInfo;
      if(InAnswerE1(pbtCmdSportX))
      { // ��� ����� �� ������ �1
        if(TestE1)
        { // ��� ����� �� �������� �������
          TestE1=0;
          NMod=InE1[1];
          for(i=0;i<9;i++) FailureE1[NMod][i]=(unsigned char)InE1[i+13]; // ������ ��������� �5...�D
          FailureE1[NMod][9]&=0xFE; // ����� �������� ���������� ������

          // ��������� ��������� E1
          if(NMod==0) TestBoard=(TestBoard & 0xFCFFFFFF) | 0x02000000;
          else if(NMod==1) TestBoard=(TestBoard & 0xF3FFFFFF) | 0x08000000;
          else if(NMod==2) TestBoard=(TestBoard & 0xCFFFFFFF) | 0x20000000;
          else if(NMod==3) TestBoard=(TestBoard & 0x3FFFFFFF) | 0x80000000;
          // ���� ������ �������� - ��������� ��������
          if(!(ParamUKC[0] & 0x1000)) TestBoard |= 0x03000000;
          if(!(ParamUKC[0] & 0x2000)) TestBoard |= 0x0C000000;
          if(!(ParamUKC[0] & 0x4000)) TestBoard |= 0x30000000;
          if(!(ParamUKC[0] & 0x8000)) TestBoard |= 0xC0000000;
//SendStr2IpLogger("$DEBUG$ProcessBoardMKC test OK!");
        }
        else
        { // ����� �� ������� �������
          switch(*pbtCmdSportX)
          {
            case 102:
            case 103:
            case 105:
            case 106:
              MasOut[0]=0x1;        // ������ - �����
              MasOut[1]=InE1[1]+13; // ����� ������ + 13
              MasOut[2]=InE1[2];    // ��� �������
              MasOut[3]=1;          // ���������� ���������
              MasOut[4]=InE1[5];    // ��� ������
              wSz=5;
              *pbtCmdSportX=0;
              break;
            case 107: // ������ ����� �� ������� ������ ��������� �0...�4
              NMod=InE1[1];
              MasOut[0]=0x1; // ������
              for(i=1;i<6;i++) MasOut[i]=InE1[i+8]; // ������ � ��������� C0...C4
              for(i=6;i<15;i++) MasOut[i]=FailureE1[NMod][i-6]; // ����������� ������ o ��������� C5...CD
              wSz=15;
              *pbtCmdSportX=0;
              break;
            case 112: // ������� ������ ���������� ������ ����������
//!!! ������ ���������� ������ ���������� �� �����������!
/*                MasOut[0]=0;
                wSz=1;
                  MasOut[18+(RemoveE1[0]--)]=InE1[5];
                  if(!RemoveE1[0]) 
                  {
                    MasOut[0]=0;
                    if((MasOut[19] == MasOut[21]) && 
                       (MasOut[20] == MasOut[22]))
                    { // ���� ����� ������
                      for (i=0;i<19;i++) MasOut[i]=MassInData[i+2];
                      Answer(&sInfo,112,19);
                    }
                    else
                    { 
                      MasOut[0]=0;
                      Answer(&sInfo,112,1);
                    }
                  }*/
              break;
            case 117: // ������� ������ ���������� ������� ������ ���
/*                !!!! ���� �������� 128 ������� �� ������� ������ ��������� ���������� ��� � ������ 1 �����!
                  MasOut[100+126-RemoveE1[0]]=InE1[5];
                  if(!RemoveE1[0]) 
                  {
                    MasOut[0]=AdrTransitMod+13;
                    MasOut[1]=0;
                    j=2;
                    for(i=1;i<127;i++)
                    {
                      if((i!=32) && (i!=64) && (i!=96)) 
                      {
                        data=MasOut[i+99];
                        if((data>0x40) && (data<0xF0))
                        {
                          MasOut[1]++; // ���� ���������� � �� SPORT1, ��������� 2-� ����� 
                          MasOut[j++]=i + 0x40;
                          MasOut[j++]=data;
                          MasOut[data-0x40+99]=0xF0;
                        }
                      }
                    }
                    Answer(&sInfo,117,MasOut[1]*2+2);
                  }*/
              break;
          }
        }
      }
    }
    else
    { // ����� ���������� ������� �� ���
      AdrI=ListWrite.pxHead;
      while(AdrI)
      { // ���� �������, ������� �� ����� �������� �� ���������� ������ ���
        switch(AdrI->NumMod) 
        {
          case 0: NMod=0x1000; j=0x03000000; break;
          case 1: NMod=0x2000; j=0x0C000000; break;
          case 2: NMod=0x4000; j=0x30000000; break;
          case 3: NMod=0x8000; j=0xC0000000; break;
          default: NMod=0; break;
        }
        if(!NMod || !(NMod & ParamUKC[0]))
        { // ������� �� ������� ������� ��� ��������������� ��� �������������� ������
          AdrI=AdrI->pxNextCom;
          _xDelCmdMKC(AdrI);
        }
        else
        {
          if(TestBoard & j) break;
          AdrI=AdrI->pxNextCom;
        }
      }
      if(AdrI)
      {
        if((AdrI->Command==2) && (AdrI->Data==107))
        { // �������� ������� ������ C0...C4
          CmdRdMKC_C0_C4[1]=AdrI->NumMod;
          OutCommandE1(CmdRdMKC_C0_C4);
        }
        else
        { // �������� ������� �������
          static unsigned short DataE1[6]; // ������ (������� / ����� �� �������)

          DataE1[1]=AdrI->NumMod;  // N ������
          DataE1[2]=AdrI->Command; // ������� (1 ��� 2)
          DataE1[3]=1;             // �-�� ��������� ��� ������� 2 � ���������-������ ��� ������� 1
          DataE1[4]=AdrI->Addr;    // ����� ��������
          DataE1[5]=AdrI->Data;    // ������
          OutCommandE1(DataE1);
        }
        _xDelCmdMKC(AdrI);
      }
    }
  }
  else StartE1=0;
  return wSz;
}

