// SportX.h  ����������� �������� ��� SportX
// version 23.08.07

#include "SportXC.h"

extern unsigned int g_dwTimer125us;

typedef struct xList_Gen_Chan_Item
{
  unsigned short chan; // ����� ������ ��� ���������
  unsigned short F1; // 1-� ������������ �������
  unsigned short F2; // 2-� ������������ �������
  unsigned short Ton; // ����� ��������� � ��
  unsigned short Tof; // ����� ����� � ��
  unsigned short Ampl1; // ��������� ��������� F1
  unsigned short Ampl2; // ��������� ��������� F2
  short sum1,sum2; // ������� �����
  struct xList_Gen_Chan_Item *pxNextGen; // ��������� �� ��������� ������� ��������� �� ������� ������
} xListGenChanItem;

typedef struct xList_Spec_Item
{	struct xList_Spec_Item *pxPrevious;// ����� ����������� ��������
	struct xList_Spec_Item *pxNext;	// ����� ���������� ��������
	unsigned char 	NSpec;						// ����� ����������
	unsigned char 	NAbon;						// ����� ��������
	unsigned char	NGen;						// ����� ������������ �������������������
	unsigned char	Buf[8];						// ����� �� 1 ������������������ (F1, F2, Ton, Toff)
} xListSpecItem;

typedef struct xList_Spec						// ��������� ��� �������� ��������� �����������
{	//unsigned char Number;						// ����� ��������� 
	xListSpecItem *pxHead;				// ����� ������� �������
} sListSpec;

typedef struct xList_Anal_Item					// 200 �� + 42 ���� �� ������ 2 ������
{
	struct xList_Anal_Item *pxPrevious;// ����� ����������� ��������
	struct xList_Anal_Item *pxNextChan;// ����� ���������� ��������
    unsigned int	middle;						// ������ ������� ��������
	int   Q11,Q21,Q12,Q22;						// ������� �������� ��� ���������� (��������� F1 � F2)
    signed 	short 	mult;						// ��������� ���
	unsigned char  	chan;						// �����
    unsigned char  	NAbon;						// ����� ��������
	unsigned char  	NF;							// ������� ����� ������������� ������� (0 - ������������)
	unsigned char  	Type;						// ��� ������� (0 - ������������, ���� ����� ������� ������ ������� ��� LTimA � 10��)
 	unsigned char  	LTimA;						// ����� ������� �� 5 �� (LTimA = LTimA / (50 / dF) (dF - �� �����������))
	unsigned char  	TimA;						// ����� ������� (���. ��������)
	unsigned char  	half;						// ������� ��������� �������� �� �������� ������� �������
	unsigned char	Nspec;						// ����� ����������� (1...8)
	  signed short 	F1;							// 1-� ������������� ������� (F1 = 32767*cos(2*Pi*F1/8000))
	  signed short 	F2;							// 2-� ������������� ������� (F2 = 32767*cos(2*Pi*F2/8000))
	unsigned short 	LdF;						// ������ ������ ����������� � �� (dF = 8000/(dF))
	unsigned short 	dF;							// ������ ������ ����������� (���. �������� �������)
} xListAnalItem;

typedef struct xList_Anal						// ��������� ��� �������� ������� ��������� �������������������
{	//unsigned char Number;						// ����� ���������
	xListAnalItem *pxHead;				// ����� ������� �������
} sListAnal;

typedef struct xList_Anal_DTMF					// 380 �� �� ������ 8 ������ - 96 ����
{	unsigned int   middle;						// ������� ������� ��������
	int   Q11,Q21,Q12,Q22,						// ������� �������� ��� ���������� (��������� F1...F8)
		  Q13,Q23,Q14,Q24,
		  Q15,Q25,Q16,Q26,
		  Q17,Q27,Q18,Q28;
	signed 	 short mult;						// ��������� ���
	unsigned char  count;						// ������� ������� ���������� (200 �������� -> ������ 40�� (25��))
	unsigned char  half;						// ������� ��������� �������� �� �������� ������� �������
	unsigned char  chan;						// �����
	unsigned char  code;						// ������� ��� ���������� �����
	unsigned short	timer;						// ������ �� ��������� ���������� �����
	struct xList_Anal_DTMF *pxPrevious;// ����� ����������� ��������
	struct xList_Anal_DTMF *pxNextChan;// ����� ���������� ��������
} xListAnalDTMF;

typedef struct xList_AnalDTMF					// ��������� ��� �������� ������� ��������� �������������������
{	//unsigned char Number;						// ����� ���������
	xListAnalDTMF *pxHead;				// ����� ������� �������
} sListAnalDTMF;

typedef struct xList_Anal_PPSR					//  �� + ���� �� ������ 8 ������ - 72 �����
{	unsigned int   middle;						// ������� ������� ��������
	int   Q11,Q21,Q12,Q22,						// ������� �������� ��� ���������� (��������� F1...F4,F8)
		  Q13,Q23,Q14,Q24,
		  Q18,Q28;
	signed 	 short mult;						// ��������� ���
//	unsigned char  half;						// ������� ��������� �������� �� �������� ������� �������
	unsigned char  chan;						// �����
	unsigned char  code;						// ������� ��� ���������� �����
	unsigned short count;						// ������� ������� ���������� (1200 �������� -> ������ 7.5�� (150��))
	unsigned char  yescode;						// ������� �����������
	struct xList_Anal_PPSR *pxPrevious;// ����� ����������� ��������
	struct xList_Anal_PPSR *pxNextChan;// ����� ���������� ��������
} xListAnalPPSR;

typedef struct xList_AnalPPSR					// ��������� ��� �������� ������� ��������� �������������������
{	//unsigned char Number;						// ����� ���������
	xListAnalPPSR *pxHead;				// ����� ������� �������
} sListAnalPPSR;

typedef struct xList_Anal_TDCF					// 1720 (1780) �� - 544 (536) ����
{	struct xList_Anal_TDCF *pxPrevious;// ����� ����������� ��������
	struct xList_Anal_TDCF *pxNextChan;// ����� ���������� ��������
	unsigned int	middle;						// ������� ������� ��������
	int   Q101,Q201,Q102,Q202,					// ������� �������� ��� ����������
		  Q103,Q203,Q104,Q204,
		  Q105,Q205,Q106,Q206,
		  Q107,Q207,Q108,Q208,
		  Q109,Q209,Q110,Q210,
		  Q111,Q211,/*Q112,Q212,
		  Q113,Q213,Q114,Q214,*/
		  Q115,Q215,Q116,Q216,
		  Q117,Q217,Q118,Q218,
		  Q119,Q219,Q120,Q220,
		  Q121,Q221,Q122,Q222,
		  Q123,Q223,Q124,Q224,
		  Q125,Q225,Q126,Q226,
		  Q127,Q227,Q128,Q228,
		  Q129,Q229,Q130,Q230,
		  Q131,Q231,Q132,Q232,
		  Q133,Q233,Q134,Q234,
		  Q135,Q235,Q136,Q236,
		  Q137,Q237,Q138,Q238,
		  Q139,Q239,Q140,Q240,
		  Q141,Q241,Q142,Q242,
		  Q143,Q243,Q144,Q244,
		  Q145,Q245,Q146,Q246,
		  Q147,Q247,Q148,Q248,
		  Q149,Q249,Q150,Q250,
		  Q151,Q251,Q152,Q252,
		  Q153,Q253,Q154,Q254,
		  Q155,Q255,Q156,Q256,
		  Q157,Q257,Q158,Q258,
		  Q159,Q259;
	short 	mult;						// ��������� ���
	unsigned short  count800;					// ������� ������� ���������� (800 �������� (100��) -> ������ 10��)
	unsigned char  	chan;						// �����
	unsigned char  	type;						// ��� �������: 0 - ������ ��������, 1 - ������
	char YesFreq[59];				// �������� ���������� ������� ������
} xListAnalTDCF;

typedef struct xList_AnalTDCF					// ��������� ��� �������� ������� ������� ���
{	//unsigned char Number;						// ����� ���������
	xListAnalTDCF *pxHead;				// ����� ������� �������
} sListAnalTDCF;

typedef struct xList_Anal_TDCL					// ___ �� - 128 ����
{	struct xList_Anal_TDCL *pxPrevious;// ����� ����������� ��������
	struct xList_Anal_TDCL *pxNextChan;// ����� ���������� ��������
	unsigned int	middle;						// ������� ������� ��������
	signed short 	mult;						// ��������� ���
	unsigned short  count800;					// ������� ������� ���������� (800 �������� (100��) -> ������ 10��)
	int   Q101,Q201,Q102,Q202,					// ������� �������� ��� ����������
		  Q103,Q203,Q104,Q204,
		  Q105,Q205,Q106,Q206,
		  Q107,Q207,Q108,Q208,
		  Q109,Q209,Q110,Q210,
		  Q111,Q211;
	unsigned char  	chan;						// �����
	unsigned char  	type;						// ��� �������: 0 - ������ ��������, 1 - ������
	char YesFreq[11];				// �������� ���������� ������� ������
} xListAnalTDCL;

typedef struct xList_AnalTDCL					// ��������� ��� �������� ���������� (���������) ������� ���
{	//unsigned char Number;						// ����� ���������
	xListAnalTDCL *pxHead;				// ����� ������� �������
} sListAnalTDCL;

typedef struct xList_Anal_PCDT					// ___ �� - 136 ����
{	struct xList_Anal_PCDT *pxPrevious;// ����� ����������� ��������
	struct xList_Anal_PCDT *pxNextChan;// ����� ���������� ��������
	unsigned int	middle;						// ������� ������� ��������
	signed short 	mult;						// ��������� ���
	unsigned short  count800;					// ������� ������� ���������� (800 �������� (100��) -> ������ 10��)
	int   Q101,Q201,Q102,Q202,					// ������� �������� ��� ����������
		  Q103,Q203,Q104,Q204,
		  Q105,Q205,Q106,Q206,
		  Q107,Q207,Q108,Q208,
		  Q109,Q209,Q110,Q210,
		  Q111,Q211,Q112,Q212;
	unsigned char  	chan;						// �����
	unsigned char  	Timer[2];					// ������ [0] - N �������, [1] - ����� �� 100 ��
	signed char 	YesFreq[12];				// �������� ���������� ������� ������
} xListAnalPCDT;

typedef struct xList_AnalPCDT					// ��������� ��� �������� ������� ������� ����
{	//unsigned char Number;						// ����� ���������
	xListAnalPCDT *pxHead;				// ����� ������� �������
} sListAnalPCDT;

typedef struct xList_Anal_ADACE					// 380 �� �� ������ 8 ������ - 96 ����
{	unsigned int   middle;						// ������� ������� ��������
	int   Q11,Q21;         						// ������� �������� ��� ���������� (��������� F1)
	signed 	 short mult;						// ��������� ���
	unsigned char  count;						// ������� ������� ���������� (200 �������� -> ������ 40�� (25��))
	unsigned char  half;						// ������� ��������� �������� �� �������� ������� �������
	unsigned char  chan;						// �����
	unsigned char  code;						// ������� ��� ���������� �����
	unsigned short	timer;						// ������ �� ��������� ���������� �����
	volatile struct xList_Anal_ADACE *pxPrevious;// ����� ����������� ��������
	volatile struct xList_Anal_ADACE *pxNextChan;// ����� ���������� ��������
} xListAnalADACE;

typedef struct xList_AnalADACE					// ��������� ��� �������� ������� ��������� �������������������
{	//unsigned char Number;						// ����� ���������
	xListAnalADACE *pxHead;			// ����� ������� �������
} sListAnalADACE;

typedef struct xList_Write_E1					// ~ 1900 �� � 6 ����
{	unsigned char	NumMod;						// ����� ������
	unsigned char	Addr;						// ����� ��������
	unsigned char	Command;					// �������
	unsigned char	Data;						// ������
	struct xList_Write_E1 *pxNextCom;	// ����� ���������� ��������
} xListWrE1;

typedef struct xList_Write
{
  xListWrE1 *pxHead;
} sListWrite;

typedef struct xList_Record						// ������� ����� RTP ��� �������� ������ �� �����������
{	unsigned char  	chan;						// ����� ������
	unsigned char  	NumBuf;						// ������� ����� ������ ������
	unsigned char 	IndBuf1;					// ������ ������ ������������ ������
	unsigned char 	IndBuf2;
	unsigned short 	Octet12_1;					// ��������� ������ RTP
	unsigned short 	Number_1;
	unsigned int	Timer_1;
	unsigned int	SSRC_1;
	unsigned short	ProFile_1;
	unsigned short	Len_1;
	unsigned char  	BufSpeak1 [LenSound];		// ����� ������� ������
	unsigned short 	Octet12_2;					// ��������� ������ RTP
	unsigned short 	Number_2;
	unsigned int	Timer_2;
	unsigned int	SSRC_2;
	unsigned short	ProFile_2;
	unsigned short	Len_2;
	unsigned char  	BufSpeak2 [LenSound];
	unsigned char	TypeRecord;					// ��� ������ (0-������ ����, 1-����-�����)
	unsigned char	block;						// ������� ���������� ������
	unsigned char	KillPause;					// ������� ���������� �����
	unsigned portCHAR IPRecord[4];				// IP ����� ���������� �����������
	unsigned short IPPort;						// ���� ����������
	struct xList_Record *pxPrevious;	// ����� ����������� ��������
	struct xList_Record *pxNextRec;	// ����� ���������� ��������
} xListRecord;

typedef struct sList_Record					// ��������� ��� �������� �������� ������� ��� ������
{	//unsigned char Number;					// ����� ���������
	xListRecord *pxHead;			// ����� ������� �������
} sListRecord;

typedef struct xBuf_Play
{	// ------------ ��������� ������� ������ ������� ������ ---------------------------------------
	unsigned char	BufPlay[LenSound][LenPlay];
	unsigned int	TimerIn;				// ��������� ����� ������ �������� ������
	unsigned int	TimerOut;				// ��������� ����� �������
	int DeltaTimer;				// ������� ��������� �����
} xBufPlay;

typedef struct xList_Play					// ������ ��������������� �� ������
{	// ------------- ������� ������� ������ � ���������� LenPlay ------------------------------------
	unsigned int	SSRC;					// ������������� ��������� �������������
	volatile struct	xBuf_Play *AdrBufPlay;	// ����� �������� ������ ������ BufInPlay[LenSound][LenPlay]
	unsigned short	TekTime;				// ������� ������
	unsigned char	ChanPlay;				// ����� ������ ���������������
	unsigned char	NumOutPlay;				// ������� ����� ��������� (��������������) ������ ������
	unsigned char	NewAudioData;			// ������� �������� ���������� ������
	unsigned char	NumBufPlay;				// ����� �������� ������ ���������������
	unsigned short	IndBufPlay;				// ������ �������� ������ ���������������
	signed char     SyncTime;				// ������� ������������� �������������
	// ------------- ������� ������ ��������������� ������ ------------------------------------------
	unsigned char	BufPlay[LenSound][2];	// ������� ����� ��������������� �� ������
//	unsigned portCHAR IPPlay[4];			// IP ����� ���������� ��������������������
	struct xList_Play *pxPrevious;	// ����� ����������� ��������
	struct xList_Play *pxNextPlay;	// ����� ���������� ��������
} xListPlay;

typedef struct sList_Play					// ��������� ��� �������� �������� ������� ��� ���������������
{	//unsigned char Number;					// ����� ���������
	xListPlay *pxHead;				// ����� ������� �������
} sListPlay;

typedef struct xList_Abon					// ��������� ���������� ������ ���������
{	unsigned short countIn;					// ������� ��������� ��������
	unsigned short countOut;				// ������� ��������� ���������
	unsigned short timer;					// ������ ������������ ������
	unsigned char  abon[LenAbon][28];		// ����� ��������� (6 � +22 ��� EDSS)
} xListAbon;

typedef struct xList_SNMP					// ��������� ���������� ������ ��������� SNMP
{	unsigned short countIn;					// ������� ��������
	unsigned short countOut;				// ������� ���������
	unsigned short timer;					// ������ ������������ ������
	unsigned char  abon[LenSNMP][3];		// ����� SNMP
} xListSNMP;

typedef struct
{
  unsigned char SN,CP,
                MasChan[MasAbntSelSz];
  unsigned short NAbon;
  unsigned char *pWav;
  unsigned int dwWavSz;
} SelectorInfoTP;

typedef struct
{
  unsigned int dwASize;
  unsigned char btRepeat;
  unsigned char *pbtWav;
} Audio4ChannelTP;

typedef struct
{
  unsigned char NameGroup[LenNameGroup+1];
  unsigned short wOff;
} GROUPINFO_TP;

/*		��������� ����������������� ������� ��� � ��� TestBoard (�� 2 ����):
		00 - ������
		01 - ������ �� ��������
		10 - ������ ��������
		11 - ������ ��������.
		������������ ������ ����� �������� ���������� �� ��������� ������� ��������� ������ LenSumChan ����.
		� 1-� ����� ������� ����������� ����� �����, ������ ����� ����������� �������.
		� ��������� ������, ����� ���������� ������� ������ ������� ��������� ���������� �� 2, 
	������� ���������� �����������.
		����, ��������� �� ������� ���������� ������ ��������� � ������� ������ ���� ������ ����� 
	����.
	
		� ������ ������ ������� ������ ���������� �������� ���������� ResetTablSum().
	
		������������ �������� ������ ������ unsigned char AddChan2Group(unsigned char reseiver,StInd):
		- ��������� ����� ����� ������ � ����� �������;
		- ������� ���� ��������� �� ������ ���������� ������ ������ � ����������� ������ 
		  ���� ������� �� �������;
		- ���������� ����� ������� ���������� �� ������ ������ ���������.
		
		������������ ������������ ������ unsigned char FreeChan(unsigned char reseiver,transmitter):
		- ��������� (�� ����) ������ ���� ������� ����������;
		- ������� � �������� ����� ���������� ������ ����������� �� ����� ���������� ������
		  � ������� ���������� (���� �� ����) � �������� ����� ���������� ������ � �������;
		- ���������� ����� ������� ���������� �� ������ ������ ���������.

		������������ ���������� ������ CopyInData() ��� ������������ �������� ������ �� �������� 
	������ DMA ��������� InSPORT0 � InSPORT1 � InDataChan, ��� ����:
		- 	�� 0-�� ������ � InDataChan ������ ������ ����� 0, ��������� ������ �������� ����������
			��������� ������ 0 SPORT1 ������������ � ��������� ������ ������ CommandIn;
		- 	�� ������� 1...63 ����������� ������ � ������� 1...63 SPORT1;
		- 	�� ������� 64...112 ����������� ������ � ������� 0...47 SPORT0.

		������������ ���������� ������ CopyOutData() �������� ������ ��� �������� �� ��������
	OutDataChan � ������ DMA OutSPORT0 � OutSPORT1.
	
		��������� SumDataChan(void) ��������� ������ �� InDataChan �� ���� �������
	� ��������� ���������� � OutDataChan.

	    ��������� short CalcNum(void) ������������ ����� ����� ������������ �������
*/

