// Call.c	��������� ������ ��������� �� ������ ����������
// 16.08.06
#include "..\Sinfo.h"
#include "SportXFn.h"
#include <signal.h>
#include <fract.h>
#include <math.h>

///////////////////////////////////////////////////////////////////////////////
extern unsigned short DataSave[LenDataSave];

extern unsigned char MassInData[];

extern sListSpec ListSpec; // ��������� ��������� ������������ ������

///////////////////////////////////////////////////////////////////////////////
unsigned char MassInG[LenInOutData];

const unsigned short F_DTMF[] = // �������� ������� ����
 {697,770,852,941,1209,1336,1477,1633};

const unsigned char Comb_DTMF[] = // �������� ���������� ���� (0,1,2...9,A...F)
 { 4, 6,   1, 5,   1, 6,   1, 7,   2, 5,   2, 6,   2, 7,   3, 5,   3, 6,   3,7,
   1, 8,   2, 8,   3, 8,   4, 8,   4, 5,   4, 7};

const unsigned char Comb_Sel[] = // ������������� ���������� ������� �������� ��������� (0,1,2...9,A,B,C,D,E,F)
 { 4, 8,   1, 5,   1, 6,   1, 7,   2, 5,   2, 6,   2, 7,   3, 5,   3, 6,   3,7,      // (D,1,2...9,0,E,F,A,B,C)
   4, 6,   4, 5,   4, 7,   1, 8,   2, 8,   3, 8};

const unsigned short F_PCDT[] = // �������� ������� ����
 {316,430,585,795,1080,1470,2000,890,1215,1360,1620,2720};

const unsigned char Comb_PCDT[] = // �������� ���������� ����
 { 1, 2,   1, 3,   1, 4,   1, 5,   1, 6,   1, 7,   1, 8,   1, 9,   1,10,   1,11,   1,12,
   2, 1,   2, 3,   2, 4,   2, 5,   2, 6,   2, 7,   2, 8,   2, 9,   2,10,   2,11,   2,12,
   3, 1,   3, 2,   3, 4,   3, 5,   3, 6,   3, 7,   3, 8,   3, 9,   3,10,   3,11,   3,12,
   4, 1,   4, 2,   4, 3,   4, 5,   4, 6,   4, 7,   4, 8,   4, 9,   4,10,   4,11,   4,12,
   5, 1,   5, 2,   5, 3,   5, 4,   5, 6,   5, 7,   5, 8,   5, 9,   5,10,   5,11,   5,12,
   6, 1,   6, 2,   6, 3,   6, 4,   6, 5,   6, 7,   6, 8,   6, 9,   6,10,   6,11,   6,12,
   7, 1,   7, 2,   7, 3,   7, 4,   7, 5,   7, 6,   7, 8,   7, 9,   7,10,   7,11,   7,12,
   8, 1,   8, 2,   8, 3,   8, 4,   8, 5,   8, 6,   8, 7,   8, 9,   8,10,   8,11,   8,12,
   9, 1,   9, 2,   9, 3,   9, 4,   9, 5,   9, 6,   9, 7,   9, 8,   9,10,   9,11,   9,12,
  10, 1,  10, 2,  10, 3,  10, 4,  10, 5,  10, 6,  10, 7,  10, 8,  10, 9,  10,11,  10,12,
  11, 1,  11, 2,  11, 3,  11, 4,  11, 5,  11, 6,  11, 7,  11, 8,  11, 9,  11,10,  11,12,
  12, 1,  12, 2,  12, 3,  12, 4,  12, 5,  12, 6,  12, 7,  12, 8,  12, 9,  12,10,  12,11};

const unsigned short F_TDC[] = // �������� ������� TDC
 {317,430,588,791,882,1085,1221,1469,1989,2713};			// ������������ �� �.�.
// {316,430,585,795,882,1080,1221,1470,2000,2720};			// ��� (� �����������)
 
const unsigned char Comb_TDC[] = // �������� ���������� ���
 { 1, 2,   1, 3,   1, 4,   1, 5,   1, 6,   1, 7,   1, 8,   1, 9,   1,10,
   2, 3,   2, 4,   2, 5,   2, 6,   2, 7,   2, 8,   2, 9,   2,10,
   3, 4,   3, 5,   3, 6,   3, 7,   3, 8,   3, 9,   3,10,
   4, 5,   4, 6,   4, 7,   4, 8,   4, 9,   4,10,
   5, 6,   5, 7,   5, 8,   5, 9,   5,10,
   6, 7,   6, 8,   6, 9,   6,10,
   7, 8,   7, 9,   7,10,
   8, 9,   8,10,
   9,10};
   
const unsigned short Comb_ImitTDC[] = // �������������� ���������� ���
 {  362,  475,  520,  633,  701,  746,  836,  927,  972, 1017,
   1130, 1243, 1289, 1334, 1379, 1424, 1515, 1560, 1650, 1695,
   1741, 1786, 1831, 1876, 1921, 2261, 2306, 2351, 2396, 2441,
   2487, 2532, 2577, 2622, 2667, 2758, 2803, 2848, 2894, 2939,
   2984, 3029, 3074, 3120, 3165};
   
const unsigned short F_Radio[] = // �������� ������� ������������ �22�-46, ��-46, ��-6, 43���-�2-��
 {1071,1207,1241,1309,1411,1479,1547,1581,1649,1683};
   
const unsigned char CombRadioD1[] = // �������� ���������� ������������ ��� 1-�� ����������
 { 1, 3,   1, 9,   1,10,   2, 3,   2, 6,   2, 9,   2,10,   3, 1,   3, 2,   3, 6,
   3, 9,   3,10,   6, 2,   6, 3,   6, 9,   6,10,   9, 1,   9, 2,   9, 3,   9, 6,
   9,10,  10, 1,  10, 2,  10, 3,  10, 6,  10, 9,   1, 6,   6, 1,   1, 2,   2, 1};
   
const unsigned char CombRadioD2[] = // �������� ���������� ������������ ��� 2-�� ����������
 { 7, 1,   7, 2,   7, 3,   7, 4,   7, 5,   7, 6,   7, 8,   7, 9,   7,10,   8, 1,
   8, 2,   8, 3,   8, 4,   8, 5,   8, 6,   8, 7,   8, 9,   8,10,   5, 1,   5, 2,
   5, 3,   5, 6,   5, 9,   5,10,   4, 2,   4, 6,   4, 9,   4,10};

#ifdef __ADACE__   
const unsigned short F_Adace[] = {1200, 1600};
//bool AdaceStart[LenSport0];

void AdaceATC(unsigned short NChan)
{ // ������� ��������� ��� �����
    if (NChan>>8) NChan=(NChan & 0xFF) + LenVirt + LenHard - 1;
    else NChan=(NChan & 0xFF) + LenSport1 - 1;
    GeneratorMU(NChan,F_Adace[0],0,230,0,min13dB1,0);
}

void AdaceDisp(unsigned short NChan)
{ // ������� �������������� ����������� �����
    if (NChan>>8) NChan=(NChan & 0xFF) + LenVirt + LenHard - 1;
    else NChan=(NChan & 0xFF) + LenSport1 - 1;
    GeneratorMU(NChan,F_Adace[1],0,230,0,min13dB1,0);
}

void AdaceAnswer(unsigned short NChan)
{ // ����� �����
    if (NChan>>8) NChan=(NChan & 0xFF) + LenVirt + LenHard - 1;
    else NChan=(NChan & 0xFF) + LenSport1 - 1;
    GeneratorMU(NChan,F_Adace[1],0,230,0,min13dB1,0);
}

void Adace ���(unsigned short NChan)
{ // ��� �����
    if (NChan>>8) NChan=(NChan & 0xFF) + LenVirt + LenHard - 1;
    else NChan=(NChan & 0xFF) + LenSport1 - 1;
    GeneratorMU(NChan,430,0,1000,0,min13dB1,0);
}

void AdaceRingOff(unsigned short NChan)
{ // ����� ������ �����
    if (NChan>>8) NChan=(NChan & 0xFF) + LenVirt + LenHard - 1;
    else NChan=(NChan & 0xFF) + LenSport1 - 1;
    GeneratorMU(NChan,F_Adace[0],F_Adace[1],230,0,min13dB2,min13dB2);
}

void AdaceNumber(unsigned short NChan, unsigned short Number)
{ // ����� ����� ������ �������� �����
  unsigned short i, chan;

  chan = NChan & 0xFF;
  if (NChan>>8) NChan=(NChan & 0xFF) + LenVirt + LenHard - 1;
  else NChan=(NChan & 0xFF) + LenSport1 - 1;
  if (!Number) Number=10;
  while(--Number) GeneratorMU(NChan,F_Adace[0],0,55,45,min13dB1,0);
  GeneratorMU(NChan,F_Adace[0],0,55,500,min13dB1,0);
//    AdaceStart[chan-1]=true;
}
#endif

//--------------------------------------------------------------------------//
// ��������� ����� � ���� 2/7												//
//--------------------------------------------------------------------------//
unsigned char CircPCDT7(unsigned short NChan)
{ // ��������� ����� ��2/7
  unsigned char i;
  i = GeneratorMU(NChan,430,0,1600,0,AmplGenPCDT,AmplGenPCDT);
  if (i && GeneratorMU(NChan,316,0,1600,0,AmplGenPCDT,AmplGenPCDT)) i++;
  if ((i==2) && GeneratorMU(NChan,430,0,1600,0,AmplGenPCDT,AmplGenPCDT)) i++;
  if ((i==3) && GeneratorMU(NChan,585,0,1600,0,AmplGenPCDT,AmplGenPCDT)) i++;
  if ((i==4) && GeneratorMU(NChan,795,0,1600,0,AmplGenPCDT,AmplGenPCDT)) i++;
  if ((i==5) && GeneratorMU(NChan,1080,0,1600,0,AmplGenPCDT,AmplGenPCDT)) i++;
  if ((i==6) && GeneratorMU(NChan,1470,0,1600,0,AmplGenPCDT,AmplGenPCDT)) i++;
  if ((i==7) && GeneratorMU(NChan,2000,0,1600,0,AmplGenPCDT,AmplGenPCDT)) i++;
  if (i == 8) return 1;
  else return 0;
}

unsigned char CircPCDT8(unsigned short NChan)
{ // ��������� ����� �� ���� 2/8 (��2/8)
  unsigned char i;
 i = CircPCDT7(NChan); // 2-1-2-3-4-5-6-7
 if (i && GeneratorMU(NChan,2720,0,1600,0,AmplGenPCDT,AmplGenPCDT)) i++; // 8
 if (i == 2) return 1;
 else return 0;
}

unsigned char CircPCDT11(unsigned short NChan)	
{ // ��������� ����� ��2/11
  unsigned char i;
  i = CircPCDT7(NChan);
  if (i && GeneratorMU(NChan,890,0,1600,0,AmplGenPCDT,AmplGenPCDT)) i++;
  if ((i==2) && GeneratorMU(NChan,1215,0,1600,0,AmplGenPCDT,AmplGenPCDT)) i++;
  if ((i==3) && GeneratorMU(NChan,1360,0,1600,0,AmplGenPCDT,AmplGenPCDT)) i++;
  if ((i==4) && GeneratorMU(NChan,1620,0,1600,0,AmplGenPCDT,AmplGenPCDT)) i++;
  if (i == 5) return 1;
  else return 0;
}

unsigned char CircPCDT12(unsigned short NChan)
{ // ��������� ����� ��2/12
  unsigned char i;
  i = CircPCDT11(NChan);
  if(i && GeneratorMU(NChan,2720,0,1600,0,AmplGenPCDT,AmplGenPCDT)) i++;
  if(i == 2) return 1;
  else return 0;
}

unsigned char Gen_ImitTDC(unsigned short NChan, unsigned char NAbon)
{ // ����� ��������� ��� (������ ������� / ��������)
  unsigned short F1, F2;

  if(NAbon >= 100)
  {
    F1=Comb_ImitTDC[NAbon-100];
    F2= F_TDC[Comb_TDC[(NAbon-100) * 2 + 1]-1];  // !!! ������ +1 ???
  }
  else
  {
    F1 = Comb_ImitTDC[NAbon];
    F2 =  F_TDC[Comb_TDC[NAbon * 2]-1];
  }
 return GeneratorMU(NChan,F1,F2,1000,0,AmplGenTDC,AmplGenTDC);
}

unsigned char CallAbon(unsigned char TypeChan, unsigned char NChan, // NChan ���������� � 0!
                       unsigned char TypeCall, unsigned char NAbon, unsigned char len)
{ // ����� ��������� ���� �����
  unsigned short F1, F2;
  unsigned char i, j;
  xListSpecItem *AdrInItem, *AdrOutItem, *AdrItem; // ������� ������

 NChan=CalcRealChannel(TypeChan,NChan);
 if((NChan<LenSport1) || (NChan>=LenHard+LenVirt+LenVirt)) return false;
 switch(TypeCall)
 { // ����������� ���� ������ 
   case CallPCDT:
   { // ����� ����
     F1 = Comb_PCDT[NAbon  * 2]; // ����������� �������� ����������
     F2 = Comb_PCDT[(NAbon * 2)+1];
     F1 = F_PCDT[F1-1]; // ����������� �������� ������
     F2 = F_PCDT[F2-1];
     if(GeneratorMU(NChan,F1,F1,800,0,AmplGenPCDT,AmplGenPCDT)) return GeneratorMU(NChan,F2,F2,1600,200,AmplGenPCDT,AmplGenPCDT);
     return false;
   }
   case CallTDC:
   { // ����� ���
     F1 = Comb_TDC[NAbon * 2];
     F2 = Comb_TDC[(NAbon * 2) + 1];
     F1 = F_TDC[F1-1];
     F2 = F_TDC[F2-1];
/*{
  static char str[80];
  LockPrintf();
  sprintf(str,"$DEBUG$CallAbon(TDC): NChan=%d, NAbon=%d,F1=%d, F2=%d",
          NChan,NAbon,F1,F2);
  UnlockPrintf();
  SendStr2IpLogger(str);
}*/
     return GeneratorMU(NChan,F1,F2,1000,200,AmplGenTDC,AmplGenTDC);
   }
   case TestTDC:
   { // ����� �������� ���
     F1 = Comb_TDC[NAbon * 2];
     F2 = Comb_TDC[(NAbon * 2) + 1];
     F1 = F_TDC[F1-1];
     F2 = F_TDC[F2-1];
     if(GeneratorMU(NChan,1605,1605,1000,0,(AmplGenTDC*2)/3,(AmplGenTDC*2)/3)) // 1-� ������� *2/3 ��� ��� ��� ����
     { return (GeneratorMU(NChan,F1,F2,1000,200,AmplGenTDC,AmplGenTDC)); }
     return false;
   }
   case GenTDC: // ����� ����������� ���
     return (GeneratorMU(NChan,1605,317,1000,200,AmplGenTDC,AmplGenTDC));
   case CallTLF: // ����� ����������� (����� ������������)
     return (GeneratorMU(NChan,1600,0,2000,200,AmplCommut,AmplCommut));
   case Call_PCDT7: // ����������� ����� ���� � ���� 2/7
     return CircPCDT7(NChan);
   case Call_PCDT8: // ����������� ����� ���� � ���� 2/8
     return CircPCDT8(NChan);
   case Call_PCDT11: // ����������� ����� ���� � ���� 2/11
     return CircPCDT11(NChan);
   case Call_PCDT12: // ����������� ����� ���� � ���� 2/12
     return CircPCDT12(NChan);
   case CallDTMF:
   { // ����� ��������� ����
     if (len)
     {
       i = 5;
       do 
       {
         // ����������� �������� ����������
         F1 = Comb_DTMF[(MassInData[i]) * 2];
         F2 = Comb_DTMF[((MassInData[i++]) * 2) + 1];
         // ����������� �������� ������
         F1 = F_DTMF[F1-1];
         F2 = F_DTMF[F2-1];
         if(!GeneratorMU(NChan,F1,F2,80,80,AmplDTMF1,AmplDTMF2)) return false;
         len--;
       }
       while(len);
       return true;
     }
     else 
     {
       F1 = Comb_DTMF[NAbon * 2];
       F2 = Comb_DTMF[(NAbon * 2) + 1];
       F1 = F_DTMF[F1-1];
       F2 = F_DTMF[F2-1];
       return GeneratorMU(NChan,F1,F2,300,300,AmplDTMF1,AmplDTMF2);
     }
   }
   case GenImitTDC: // ����� ��������� ��� (������ ������� / ��������)
     return Gen_ImitTDC(NChan, NAbon);
   case CodP22C_D1:
   { // ����� 1-� ����������� ������������ �22�-46
     // ����������� �������� ����������
     F1 = CombRadioD1[NAbon * 2];
     F2 = CombRadioD1[(NAbon * 2) + 1];
     // ����������� �������� ������
     F1 = F_Radio[F1-1];
     F2 = F_Radio[F2-1];
     if(GeneratorMU(NChan,F1,0,250,0,AmplRadio,AmplRadio)) 
     { return GeneratorMU(NChan,F2,0,250,100,AmplRadio,AmplRadio); }
     return false;
   }
   case CodP22C_D2:
   { // ����� 2-� ����������� ������������ �22�-46
     F1 = CombRadioD2[NAbon * 2];
     F2 = CombRadioD2[(NAbon * 2) + 1];
     F1 = F_Radio[F1-1];
     F2 = F_Radio[F2-1];
     if(GeneratorMU(NChan,F1,0,250,0,AmplRadio,AmplRadio)) 
     { return GeneratorMU(NChan,F2,0,250,100,AmplRadio,AmplRadio); }
     return false;
   }
   case CodPC46_D1:
   { // ����� 1-� ����������� ������������ ��-46
     F1 = CombRadioD1[NAbon * 2];
     F2 = CombRadioD1[(NAbon * 2) + 1];
     F1 = F_Radio[F1-1];
     F2 = F_Radio[F2-1];
     if (GeneratorMU(NChan,F1,0,250,0,AmplRadio,AmplRadio)) 
     { return GeneratorMU(NChan,F2,0,250,100,AmplRadio,AmplRadio); }
     return false;
   }
   case CodPC46_D2:
   { // ����� 2-� ����������� ������������ ��-46
     F1 = CombRadioD2[NAbon * 2];
     F2 = CombRadioD2[(NAbon * 2) + 1];
     F1 = F_Radio[F1-1];
     F2 = F_Radio[F2-1];
     if (GeneratorMU(NChan,F1,0,250,0,AmplRadio,AmplRadio)) 
     { return GeneratorMU(NChan,F2,0,250,100,AmplRadio,AmplRadio); }
     return false;
   }
   case CodPC6_D1:
   { // ����� 1-� ����������� ������������ ��-6
     F1 = CombRadioD1[NAbon * 2];
     F2 = CombRadioD1[(NAbon * 2) + 1];
     F1 = F_Radio[F1-1];
     F2 = F_Radio[F2-1];
     if (GeneratorMU(NChan,F1,0,250,0,AmplRadio,AmplRadio)) 
     { return GeneratorMU(NChan,F2,0,250,100,AmplRadio,AmplRadio); }
     return false;
   }
   case CodPC6_D2:
   { // ����� 2-� ����������� ������������ ��-6
     F1 = CombRadioD2[NAbon * 2];
     F2 = CombRadioD2[(NAbon * 2) + 1];
     F1 = F_Radio[F1-1];
     F2 = F_Radio[F2-1];
     if (GeneratorMU(NChan,F1,0,250,0,AmplRadio,AmplRadio)) 
     { return GeneratorMU(NChan,F2,0,250,100,AmplRadio,AmplRadio); }
     return false;
   }
   case Cod43PTC_D1:
   { // ����� 1-� ����������� ������������ 43���-�2-��
     F1 = CombRadioD1[NAbon * 2];
     F2 = CombRadioD1[(NAbon * 2) + 1];
     F1 = F_Radio[F1-1];
     F2 = F_Radio[F2-1];
     if (GeneratorMU(NChan,F1,0,750,0,AmplRadio,AmplRadio)) 
     { return (GeneratorMU(NChan,F2,0,750,100,AmplRadio,AmplRadio)); }
     return false;
   }
   case RadTransPC: // ������� �������� ������������ �22�-46, ��-46, ��-6
     if (GeneratorMU(NChan,2227,0,250,0,AmplRadio,AmplRadio)) return GeneratorMU(NChan,2295,0,250,100,AmplRadio,AmplRadio);
     return false;
   case RadTrans43: // ������� �������� ������������ 43���-�2-��
     if (GeneratorMU(NChan,2227,0,750,0,AmplRadio,AmplRadio)) return GeneratorMU(NChan,2295,0,750,100,AmplRadio,AmplRadio);
     else return false;
   case RadRecivPC: // ������� ������ ������������ �22�-46, ��-46, ��-6
     if (GeneratorMU(NChan,2295,0,250,0,AmplRadio,AmplRadio)) return GeneratorMU(NChan,2227,0,250,100,AmplRadio,AmplRadio);
     return false;
   case RadReciv43: // ������� ������ ������������ 43���-�2-��
     if (GeneratorMU(NChan,2295,0,750,0,AmplRadio,AmplRadio)) return GeneratorMU(NChan,2227,0,750,100,AmplRadio,AmplRadio);
     return false;
	case RadBreakPC:					// ----- ������� ����� ������������ �22�-46, ��-46, ��-6
 		if (GeneratorMU(NChan,1071,0,250,0,AmplRadio,AmplRadio)) 
 			return (GeneratorMU(NChan,1207,0,250,100,AmplRadio,AmplRadio));
		else return false;
	case RadBreak43:					// ----- ������� ����� ������������ 43���-�2-��
 		return GeneratorMU(NChan,1139,3300,2000,0,AmplRadio,AmplRadio);
	case RadContrPC:					// ----- ������� �������� ������������ �22�-46, ��-46, ��-6
 		if (GeneratorMU(NChan,1207,0,250,0,AmplRadio,AmplRadio)) 
 			return (GeneratorMU(NChan,1071,0,250,100,AmplRadio,AmplRadio));
  		else return false;
 	case RadContr43:					// ----- ������� �������� ������������ 43���-�2-��
 		if (GeneratorMU(NChan,1207,0,750,0,AmplRadio,AmplRadio)) 
 			return (GeneratorMU(NChan,1071,0,750,100,AmplRadio,AmplRadio));
  		else return false;
	case RadBusyPC:						// ----- ������� ������ ������������ �22�-46, ��-46, ��-6
 		if (GeneratorMU(NChan,1309,0,250,0,AmplRadio,AmplRadio)) 
 			return (GeneratorMU(NChan,1547,0,250,100,AmplRadio,AmplRadio));
  		else return false;
	case RadBusy43:						// ----- ������� ������ ������������ 43���-�2-��
 		if (GeneratorMU(NChan,1309,0,750,0,AmplRadio,AmplRadio)) 
 			return (GeneratorMU(NChan,1547,0,750,100,AmplRadio,AmplRadio));
  		else return false;
	case InclPPSR:						// ----- ������� ��������� ���� ������������ ����
  		return (GeneratorMU(NChan,697,1633,300,100,AmplPPSR,AmplPPSR)); 
	case ExclPPSR:						// ----- ������� ���������� ���� ������������ ���� � ��3 ("�����. �����")
//	case ExclRD3:
		return (GeneratorMU(NChan,317,1605,1000,400,AmplPPSR,AmplPPSR));  
	case IndivPPSR:		{				// ----- ������� ��������������� ��������� ������������ ���� 		("*9")
 		F1 = Comb_TDC[NAbon * 2];
  		F2 = Comb_TDC[(NAbon * 2) + 1];
  		F1 = F_TDC[F1-1];		
  		F2 = F_TDC[F2-1];
  		return (GeneratorMU(NChan,F1,F2,1000,400,AmplPPSR,AmplPPSR) && 
  				GeneratorMU(NChan,941,1209,300,100,AmplPPSR,AmplPPSR) &&
				GeneratorMU(NChan,852,1477,300,100,AmplPPSR,AmplPPSR));
	}
	case InclATCPPSR:		{			// ----- ������� ��������������� ��������� ��� ����				("*6")
 			F1 = Comb_TDC[NAbon * 2];				// ����������� �������� ����������
  			F2 = Comb_TDC[(NAbon * 2) + 1];
  			F1 = F_TDC[F1-1];						// ����������� �������� ������
  			F2 = F_TDC[F2-1];
  			return (GeneratorMU(NChan,F1,F2,1000,400,AmplPPSR,AmplPPSR) && 
  				GeneratorMU(NChan,941,1209,300,100,AmplPPSR,AmplPPSR) &&
				GeneratorMU(NChan,770,1477,300,100,AmplPPSR,AmplPPSR));
	}
	case ExTransPPSR:					// ----- ������� ���������� ������������ ���� ��� ���������� ��� 	("D")
  		return GeneratorMU(NChan,941,1633,300,100,AmplPPSR,AmplPPSR);
	case ChanPPSR:		{				// ----- ������� ���������� ��������� ������ ������������ ����	("*5 + Nchan")
 		F1 = Comb_TDC[NAbon * 2];	
  		F2 = Comb_TDC[(NAbon * 2) + 1];
  		F1 = F_TDC[F1-1];			
  		F2 = F_TDC[F2-1];
  		if (GeneratorMU(NChan,F1,F2,1000,400,AmplPPSR,AmplPPSR) && 
  			GeneratorMU(NChan,941,1209,300,100,AmplPPSR,AmplPPSR) &&
  			GeneratorMU(NChan,770,1336,300,100,AmplPPSR,AmplPPSR))
  			{
  				NAbon=MassInData[5]; // �������� ������ ������ ������������
				if (NAbon<10) GeneratorMU(NChan,941,1336,300,100,AmplPPSR,AmplPPSR); 
   				else {GeneratorMU(NChan,697,1209,300,100,AmplPPSR,AmplPPSR); NAbon-=10;}
   				F1 = Comb_DTMF[NAbon * 2];
   				F2 = Comb_DTMF[(NAbon * 2) + 1];
   				F1 = F_DTMF[F1-1];	
   				F2 = F_DTMF[F2-1];
   				return GeneratorMU(NChan,F1,F2,300,400,AmplPPSR,AmplPPSR);
  			}
  			else return false;
	}
	case BusyATCPPSR:					// ----- ������ "������" �� ��� ���� ("C")
  		return  GeneratorMU(NChan,852,1633,300,100,AmplPPSR,AmplPPSR);
	case ImitATCPPSR:					// ----- �������� ��������� ��� ������ ���� ("B")
		return  GeneratorMU(NChan,770,1633,300,100,AmplPPSR,AmplPPSR);
   case CallKPV: // ----- ������ ���
     GeneratorMU(NChan,0,0,1,1000,min9dB1,min9dB1);
     return GeneratorMU(NChan,425,0,1000,3000,min9dB1,min9dB1);
   case CallZummer: // ----- ������ ������ ���
     GeneratorMU(NChan,NotaRe,NotaRe,100,0,min3dB1,min3dB1); 
     GeneratorMU(NChan,NotaCol,NotaCol,100,0,min3dB1,min3dB1); 
     GeneratorMU(NChan,NotaRe,NotaRe,100,0,min3dB1,min3dB1); 
     GeneratorMU(NChan,NotaCol,NotaCol,100,0,min3dB1,min3dB1); 
     GeneratorMU(NChan,NotaRe,NotaRe,100,0,min3dB1,min3dB1);
     GeneratorMU(NChan,NotaCol,NotaCol,100, 1000,min3dB1,min3dB1);
     return true;
   case CallBusy: // ----- ������ "������"
     return GeneratorMU(NChan,425,0,350,350,min3dB1,min9dB1);
   case Halali: // ----- ������ "��-��-��"
     if(GeneratorMU(NChan,950,0,330,0,min9dB1,min9dB1) && 
        GeneratorMU(NChan,1400,0,330,0,min9dB1,min9dB1)) return GeneratorMU(NChan,1800,0,330,1000,min9dB1,min9dB1);
     return false;
   case GenTest: // ----- ������� �������� ��������� �� �������
     GeneratorMU(NChan,1200,0,450,150,AmplPPSR,AmplPPSR);
     GeneratorMU(NChan+1,1300,0,200,0,AmplPPSR,AmplPPSR);
     GeneratorMU(NChan+1,1310,0,200,200,AmplPPSR,AmplPPSR);
     GeneratorMU(NChan+1,1320,0,200,0,AmplPPSR,AmplPPSR);
     GeneratorMU(NChan+2,1400,0,130,0,AmplPPSR,AmplPPSR);
     GeneratorMU(NChan+2,1410,0,130,110,AmplPPSR,AmplPPSR);
     GeneratorMU(NChan+2,1420,0,130,0,AmplPPSR,AmplPPSR);
     GeneratorMU(NChan+3,1500,0,400,0,AmplPPSR,AmplPPSR);
     return true;
   default:
	{
		if((TypeCall >= SelOtboy) && (TypeCall <= SelUchast))
 		{ // ----- ��������� ������� ��������� ��������� RDZ
 			TypeCall = (TypeCall - SelOtboy) * 4;	// ����������� ���������� ������ ���� �������
			if (NAbon < 0x10) ;	// �� ��������� �������� � ����
  			else if (NAbon < 0x20) {TypeCall++; NAbon -= 0x10;}	// ��������� �������� ����� ������ 16 �������
  			else if (NAbon < 0x30) {TypeCall+=2; NAbon -= 0x20;}
  			else {TypeCall+=3; NAbon -= 0x30;}
  			if (GeneratorMU(NChan,F_DTMF[Comb_Sel[NAbon * 2]-1],F_DTMF[Comb_Sel[(NAbon * 2) + 1]-1],60,80,min9dB2,min9dB2) && 
   				GeneratorMU(NChan,F_DTMF[Comb_Sel[TypeCall * 2]-1],F_DTMF[Comb_Sel[(TypeCall * 2) + 1]-1],60,470,min9dB2,min9dB2))
   			return true; 
 		}
		else
		  if ((TypeCall >= CallSpec1) && (TypeCall <= CallSpec8))
 		  { // ����������� ����� ���������
 			TypeCall = TypeCall - (CallSpec1 - 1); // ����� ����������
			NAbon++; // ����� ��������
  			AdrInItem = ListSpec.pxHead; // ����� ������� �������	
			while(AdrInItem) 
			{
#define _ AdrInItem ->
			  if ((_ NSpec == TypeCall) && (_ NAbon == NAbon-1))
			  {
				MassInG[2] = _ NSpec;
		 		MassInG[3] = _ NAbon;
		 		MassInG[4] = _ NGen;
		 		for (i=0;i<(_ NGen *8);i++) MassInG[i+5] = _ Buf[i];
		 		return CallSpecial(NChan);
			  }
#undef _
			  AdrInItem=AdrInItem -> pxNext;
   			}
 		  }
   		return false;
	}
 }
}

