unsigned char GeneratorMU(unsigned short chan, unsigned short F1, unsigned short F2,
                          unsigned short Ton, unsigned short Tof, unsigned short Ampl1, unsigned short Ampl2);

void InitAnalizeList(void);
void AnalizeTDC();
void AnalizeTDCL();
void Analize_PPSR_RSDT_DTMF_ETC(void);

/* unsigned char 	InitAnalDTMF(unsigned char chan);
 unsigned char 	ExitAnalDTMF(unsigned char chan);
 unsigned char 	InitAnalPPSR(unsigned char chan);
 unsigned char 	ExitAnalPPSR(unsigned char chan);
 unsigned char 	ExitAnal(unsigned char chan, unsigned char Nabon);
 unsigned char 	InitAnalTDCFull(unsigned char chan);
 unsigned char 	InitAnalTDCSmall(unsigned char chan);
*/
unsigned short RdFlash(unsigned short AdrFl);
bool WrFlash(unsigned short  DanFl,unsigned short  AdrFl);
void ReWrFlash(unsigned short  DanFl,unsigned short  AdrFl);

void OutAbon(unsigned char *Abon, unsigned char Len); // �������� ������ � ������� ��������� ������
unsigned char InitSpecGen(unsigned char * data); // ������������� �������� ��������� ������������
void AnswerTestMKC();
void OutSNMP(unsigned char NMod, unsigned char Port, unsigned char Cod);
unsigned char SetParamPoint(unsigned char *MassData); // ��������� ���������� �� ����� ����������� (3 �����)
unsigned short a_compress(unsigned short input);
unsigned short a_expand(unsigned short input);
time_t ReadClock(void);

unsigned char CallSpecial(unsigned short NChan); // ��������� ������������ ������ ���������
unsigned char FoundGenerator(unsigned char type, unsigned char chan); // ����� ��������� �� ������

unsigned char InitAnalMU(unsigned char chan, unsigned char NAbon,
                         signed short F1, signed short F2,
                         unsigned short dF, unsigned short TimA, unsigned short Type, unsigned char Nspec);
unsigned char InitAnalDTMF(unsigned char chan);
unsigned char InitAnalPPSR(unsigned char chan);
unsigned char InitAnalTDCFull(unsigned char chan);
unsigned char InitAnalTDCSmall(unsigned char chan);
unsigned char InitAnalPCDT(unsigned char chan);
unsigned char ExitAnalDTMF(unsigned char chan);
unsigned char ExitAnalPPSR(unsigned char chan);
unsigned char ExitAnalTDC(unsigned char chan);
unsigned char ExitAnalTDCL(unsigned char chan);
unsigned char ExitAnalPCDT(unsigned char chan);
unsigned char ExitAnal(unsigned char chan, unsigned char Nabon);
unsigned char ExclGenerator(unsigned char type, unsigned char chan);	// ���������� ��������� �� ������
unsigned char FoundTP (unsigned char chan, unsigned char busy);
unsigned short RdFlash( unsigned short  AdrFl);
bool WrFlash(unsigned short DanFl,unsigned short  AdrFl);
unsigned char InitE1(void); // �������� � �������� ������������� ������ ���
char *SetVarComm(void);
bool FindInitFile(MYFILE *ptFile,char *Ext);
void RestartCommut(MYFILE *pstFile);
void InitMKC(unsigned char nMod); // �������� ������� ���������� � ������������� ������ ���
void RestartSPORT0(void);   // ����������������� SPORT0
void RestartSPORT1(void);   // ����������������� SPORT1

unsigned char CallAbon(unsigned char TypeChan, unsigned char NChan, unsigned char TypeCall, 
                       unsigned char NAbon, unsigned char len);
unsigned char ExclGroup (unsigned char *Adr);
unsigned char ReadGroup(unsigned char *Adr);
unsigned char BlockInput(unsigned char *Adr,unsigned btState);
unsigned char DelChanFromGroup(unsigned char *Adr);
unsigned char AddChan2Group (unsigned char *Adr, unsigned short StInd);
char *ReadFileName(char *ext);

unsigned char InclTempComm(unsigned char chan1,unsigned char chan2,unsigned char No55);
unsigned char ExclTempComm(unsigned char chan1,unsigned char chan2);
unsigned char ReadTempComm(unsigned char chan);
bool ReadInChan(unsigned char type, unsigned char chan);
void ClearTablCommut(void);

unsigned char ReadAnal(unsigned char chan);
unsigned char ReadAnalDTMF(unsigned char chan);
unsigned char ReadAnalTDC(unsigned char chan);
unsigned char ReadAnalTDCL(unsigned char chan);
unsigned char ReadAnalPCDT(unsigned char chan);
unsigned char ExitRecord(unsigned char typechan, unsigned char chan);
unsigned int InitRecord(unsigned char typechan,unsigned char chan,unsigned char Type, 
                        unsigned char block,unsigned char *IP_Addr,unsigned short Port,
                        unsigned int SSRC);
unsigned char ExitPlay(unsigned char TypeChan,unsigned char NumbChan);
unsigned char InitPlay(unsigned char TypeChan,unsigned char NumbChan,unsigned char *IPadr);
unsigned char ExclGroup (unsigned char *Adr);
void ReadAllTransit(unsigned char AdrModE1);
void Swinging(unsigned char chan); // ��������� ���������� �������

unsigned char WriteE1(unsigned char NMod, unsigned char Reg, unsigned char data);
unsigned char ReadE1(unsigned char NMod, unsigned char Reg);

void RestartTempBuf(void); // ������� �������
unsigned short FoundPlaceSelector(unsigned char Nklav);

unsigned short ReversShort(unsigned short data);
unsigned int   ReversInt  (unsigned int data);

void AnalizeGenerator(void);
void ExclAllGenerator(void); // ���������� ��������� �� ���� �������
unsigned char FoundAdrGroup(unsigned char *Adr);
unsigned char SaveGroupFlash(unsigned char *Adr, unsigned char SetFile, unsigned char KI);
unsigned short GetSelectorConnectionPoint(unsigned char PultNb);
unsigned char PlaySound2ConnectionPoint(unsigned short wNChan,char * FName);
bool IsCommut(unsigned short chan1, unsigned short chan2);
unsigned short GetWiznetReinitCounter(void); // ����� ������������ Wiznet;
void ReinitConnectionPoint(void);    // ����������������� ������� ���������� � ����� �����������
unsigned char GetNextE1(unsigned char btE1code);   // ������� ������ ������ ����������
unsigned char GetFirstE1(void);   // ������� ������ ������ �������
unsigned char FoundDelTempComm(unsigned char type, unsigned char chan); // ����� � �������� ��������� ���������� ������
void AutoAnswerSignal(unsigned short wNChan);
void SendCommandMKA(unsigned char command, unsigned char data); // �������� ������� �� ���
unsigned char CalcRealChannel(unsigned char btType,unsigned char btChanNumb);
unsigned char _xGetSelectorCP(unsigned char SN);
bool IsSelectorRecord(unsigned char SN);
bool IsLeaderSelector(unsigned char SN,unsigned short wCP);

// FunctionMKC.c
void InitVarsMKC(void);
void PeriodicalTestMKC(void);
unsigned short ProcessBoardMKC(unsigned char *MasOut,unsigned char *pbtCmdSportX);

void InitSelectorInfo(void);
void InitConfInfo(void);

#ifdef __ADACE__
// ---------- ����� ----------------
void AdaceATC(unsigned short NChan);                                // 	������� ��������� ��� �����
void AdaceCommut(unsigned short NChan);                             // 	������� �������������� ����������� �����
void AdaceRingOff(unsigned short NChan);                            // 	������� ������ �����
void AdaceNumber(unsigned short NChan, unsigned short Number);      // 	����� ����� ������ �������� �����
#endif

