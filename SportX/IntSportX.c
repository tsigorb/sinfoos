// IntSportX.c ��������� ��������� ���������� 8 ��� �� Sport0 � SPORT1
#include "..\Sinfo.h"
#include <signal.h>
#include <fract.h>
#include <math.h>
#include <stdlib.h>

#ifdef __ISDN__
  #include "..\edss\pri_channel.h"
#endif

// ------------------ ������� ������� -------------------------------
void CopyData(void);
void SumDataChan(void);
short IIRFull(short data_in, short *delay, short *coeff);
short IIRSport1(short data_in);
short sin2F(unsigned short F1, unsigned short F2, unsigned short Ampl);
unsigned short a_compress(unsigned short input); // ������������ ������ ������ �� �-������
unsigned short a_expand (unsigned short input); // ������������ �������� ������ �� �-������
void CalcMiddle(void); // ���������� ������� ��������

///////////////////////////////////////////////////////////////////////////////
extern short del300[];
extern short coef300[];

extern short del3200[];
extern short coef3200[];

extern section("bigdata") char WavFile[];
extern unsigned short StartWavS;

extern char *AdrOutSound;

extern unsigned char SelectEDSS[LenVirt];  // ������� ��������� (������ ������ � ������� ����������+1) ��� ��� (0)
extern SelectorInfoTP MasSelInfo[NumberKeyboard];

extern unsigned short DataSave[LenDataSave];

///////////////////////////////////////////////////////////////////////////////
xListGenChanItem MasGenChanItem[LenGener];

sListSpec		ListSpec;				// ��������� ��������� ������������ ������
sListAnalTDCF	ListAnalTDCF;			// ��������� ������� ����������� �������� TDC
sListAnalTDCL	ListAnalTDCL;			// ��������� ���������� ����������� �������� TDC
sListAnalDTMF	ListAnalDTMF;			// ��������� ����������� �������� DTMF
sListAnalPPSR	ListAnalPPSR;			// ��������� ����������� �������� PPSR
sListAnalPCDT	ListAnalPCDT;			// ��������� ����������� �������� PCDT
sListSpec		ListSpec;				// ��������� ��������� ������������ ������
sListAnal		ListAnal;				// ��������� ����������� ������������� ��������
sListRecord		ListRecord;				// ��������� ������ �����������
sListPlay		ListPlay;				// ��������� ������ ��������������������

section("fastdata") short InDataChan[LenChan+1]; // +1 - ��� ������� ���������
section("fastdata") short OutDataChan[LenChan+1]; // ������ �������� � ���������� �������
section("fastdata") unsigned short g_wMasDisOut[LenSport0];

section("fastdata") unsigned int SumADCMiddle[LenSport0+LenVirt]; // ������� ����� ������� �������� �� ������� � ������ ������������
section("fastdata") unsigned int SumDACMiddle[LenSport0+LenVirt]; // ������� ����� ������� �������� �� �������, ������ ����������� � �� ���

section("fastdata") unsigned char TablSum[LenSumChan]; // ������� ������� ����������� �� ������� ���� ���������� (�������������)
section("fastdata") unsigned char TablBlock[LenSumChan]; // ������� ������� ����������� �� �����
section("fastdata") unsigned char TablBlockVar[LenChan]; // ��������� ������ ������� ����������� �������

int SumADCShift[LenVirt]; // ������� �������� �� ������ ������������

unsigned char ChanFreq=0; // ����� ������, �� �������� ���������� ������� �������
short MeazFreq; // ��������� ������� �� ������
short TimeFreq; // ����� ��������� ������� �� ������
short SignFreq; // ���� ������� �� ������

unsigned char ChanMiddle=0; // ����� ��, �� �������� ���������� ������� ��������
unsigned short BigCountMiddle; // ������� 256��
unsigned short SumDataChanMiddle=0; // ������� �������� �� ��
unsigned int DataChanMiddle=0; // ����������� �������� �� ��

// ������ ������
unsigned char	hw_txbuf0[COL_BUF][LEN_BUF];
unsigned char	hw_txbuf1[COL_BUF][LEN_BUF];
unsigned char	hw_txbuf2[COL_BUF][LEN_BUF];
unsigned char	hw_txbuf3[COL_BUF][LEN_BUF];

unsigned char	hw_rxbuf0[COL_BUF][LEN_BUF];
unsigned char	hw_rxbuf1[COL_BUF][LEN_BUF];
unsigned char	hw_rxbuf2[COL_BUF][LEN_BUF];
unsigned char	hw_rxbuf3[COL_BUF][LEN_BUF];

unsigned int	TestBoard={0xFFFFFFFF}; // �������� ����������������� (�� 2 ����) ���������� ���� (��.) � ������� �1(��.)

unsigned int	Time8Hold=0;			// ����� ��������� ���������� 8 ��� � �������� SCLK=49.152 ��� (20,356 ��)


short			RxBuf0[LenBuf0];		// SPORT0 DMA1 receive buffer
short			TxBuf0[LenBuf0];		// SPORT0 DMA2 transmit buffer
short			RxBuf1[LenBuf1];		// SPORT1 DMA3 receive buffer
short			TxBuf1[LenBuf1];		// SPORT1 DMA4 transmit buffer

unsigned short  TimeWait125;			// ����� �������� ���������� ��������� IntSportX

unsigned short	InTestData[LenMKA], OldInTestData[LenMKA]; // �������� ������ � ���� ���������� �������
unsigned short	OutTestData[LenMKA];	// �������� (���������) ������ �� ����� ���������� �������
unsigned short	OutE1[LenInE1];			// ������ ������������ ������ �� �1 (������� ������ �������� � OutE1[0])
										// [count, Addr, Command, Len, N Addr, (N Data)]
short 			CommandInOut[]={0x006A,0x006A}; // ������� ������ ������ ����� � ������ �� SPORT1
unsigned short InE1[LenInE1+1];         // ������ ����������� ������ �� �1 (������� ������ ������ � InE1[0])
unsigned short	LoadCoef;				// ������� ������������� �������� ������������� (1 ��� �� �����)
unsigned short	CountMiddle;			// ������� ���������� ������� �������� 64��

unsigned short TablChan[LenChan]; // ��������� ������ ������� ����������� �������

unsigned short ParamUKC[LenParamUKC]=
				{0,0,0,0,0,0,0};// ��������� (�� 1 ����) ���������� ������ ������� ���(��.) � E1(��.) 
								// ������� ������������� ��� �������� �������
								// ������� ������������� ��� ��������� SNMP
								// ������� ������� ����������
								// ��������� ����� ������� EDSS
								// ����� ������� EDSS (1 ��� 2)
								// ������� ��������� (��������������) �� � ������ ������ ������ �� N ��������

unsigned short  SinCoef[]={0x6480,0x0059,0xD54D,0x0252,0x0388}; // ������������ ��� 1-� ������� ����. ���������

unsigned char	MassCallMKA[LenMKA]; // ������ �������� ������� �� ���� ���

unsigned char	CopyTablBlock[LenSumChan]; // ����� ������� ������� ����������� �� �����
unsigned char	IndOutE1; // ������� ���������� ���� �� �1

unsigned char	Toggl=0; // ������ � ���������� 0-� ����� ��� ������ � ��� N1 �� ������ N1

unsigned short  Ampl2; // ��������� 2-� ������� ����������

time_t Time_Sys; // ���� ���������� ������� � ���;
unsigned int Sys_Sec=0;

unsigned short KInARU[LenVirt]; // ��������� ���
unsigned char  ARUTP[LenVirt]; // ��������� ���
unsigned short KDupIn[LenVirt]; // ��������� ������������ �� �����
unsigned short KDupOut[LenVirt]; // ��������� ������������ �� ������

unsigned short ReversA[LenSport0]; //������� �������� �� ���������� �������
unsigned short Revers[LenVirt]; //������� �������� �� ������ �����������
unsigned short PlayWav[LenVirt];
unsigned short ChanWav[LenVirt];
unsigned short PlayWavS;
unsigned short ChanWavS;

// ----------- ���������� ��� ��������������� ������� ��������� �������� -----
unsigned int LenKeep=0;
unsigned short BufPlayKeep[LenSound][2];
unsigned short IndBufPlayKeep;
unsigned char NumBufPlayKeep;
unsigned char NextPlayKeep=true;
//unsigned int StartKeep=0;

// ------------------------------------------------------
unsigned short Count5mc, Count10mc;

#ifdef __ISDN__
  volatile unsigned char IND_BUF=0; // ������ ������� ������
  volatile unsigned char NUM_BUF=0; // ����� ������� ������
  volatile unsigned char GET_BUF=0; // ������� ���������� ������
  volatile unsigned short InTestMKS1=0, OutTestMKS1=0, InTestMKS1_2=0, OutTestMKS1_2=0,
                 InTestMKS2=0, OutTestMKS2=0, InTestMKS2_2=0, OutTestMKS2_2=0;
#endif

short DataMin,DataMax;

///////////////////////////////////////////////////////////////////////////////
/*
// -------------- ��� ����� ������� ��� -----------------
!!! ���� ���� ����� ������ !!!
char StartTestMKC;
unsigned int DataInTest1, DataOutTest, DataALowTest1, CountErrTest1, TestWait1, TestWaitSet1;
unsigned int DataInTest2, DataALowTest2, CountErrTest2, TestWait2, TestWaitSet2;
unsigned long CountTest;

void AnswerTestMKC(unsigned char *MasOut)
{ // ����� �� ������� ����� ��� !!! ����� ������ !!!
  MasOut[0]=DataInTest1;
  MasOut[1]=DataInTest2;
  MasOut[2]=(DataOutTest-1) & 0x00FF;
  MasOut[3]=CountTest >> 24;
  MasOut[4]=CountTest >> 16;
  MasOut[5]=CountTest >> 8;
  MasOut[6]=CountTest;
  MasOut[7]=(CountErrTest1-TestWait1-2) >> 8;
  MasOut[8]=CountErrTest1-TestWait1-2;
  MasOut[9]=(CountErrTest2-TestWait2-2) >> 8;
  MasOut[10]=CountErrTest2-TestWait2-2;
  MasOut[11]=TestWait1;
  MasOut[12]=TestWait2;
}

void TestMKC(void)
{ 
  if(StartTestMKC)
  { // ���� ��� !!!����� ����������������!!!
    if(StartTestMKC==3)
    {
      CountErrTest1=CountTest=TestWait1=CountErrTest2=TestWait2=0;
      StartTestMKC=2;
    }
	OutDataChan[LenHard+LenVirt+64-1]=OutDataChan[LenHard+LenVirt+63-1]=a_expand(DataOutTest) << 3;
	DataInTest1=a_compress(InDataChan[LenHard+LenVirt+63-1] >> 3);
    DataInTest2=a_compress(InDataChan[LenHard+LenVirt+64-1] >> 3);

	if(!CountErrTest1)
	{ TestWaitSet1=DataInTest1; CountErrTest1++; TestWait1=0; }
	if(!TestWait1)
	{
	  if(TestWaitSet1 != DataInTest1) TestWait1=CountErrTest1-1;
	  if(CountErrTest1<30000) CountErrTest1++;
	}
	else
	  if(((DataInTest1+TestWait1) & 0x00FF) != DataOutTest)
	    if(CountErrTest1<30000) CountErrTest1++;

	if(!CountErrTest2)
	{ TestWaitSet2=DataInTest2; CountErrTest2++; TestWait2=0; }
	if(!TestWait2)
	{
	  if(TestWaitSet2 != DataInTest2) TestWait2=CountErrTest2-1;
	  if(CountErrTest2<30000) CountErrTest2++;
	}
	else
	  if(((DataInTest2+TestWait2) & 0x00FF) != DataOutTest)
	    if(CountErrTest2<30000) CountErrTest2++;

	DataOutTest=(DataOutTest + 1) & 0x00FF;
	CountTest++;
  }
}
*/

void ExchangMKC(void)
{ // �������� / ����� ������ � ������ ��� � ���������� 8 ���
 static unsigned short i;

 // ----- �������� ----
 if(OutE1[0])
 { // �������� ����� ������� �� ���
   CommandInOut[1]=OutE1[++IndOutE1];
   OutE1[0]--;
   if(!OutE1[0]) InE1[LenInE1]=0; // ��������� ����� ����������
 }
 else CommandInOut[1]=0x006A; // �������� ���� 0xFF 

 // ----- ����� ------
 if(InE1[LenInE1]!=0xff)
 { // ����� ������ ��� �� �������
   if(InE1[LenInE1])
   {
     i=a_compress(CommandInOut[0]);
     if(InE1[LenInE1]==1)
     { // ���� ������ ������� � ��������� 1..3
       if(i && (i<=3))
       {
         InE1[2]=i;
         InE1[LenInE1]=2;
       }
       else InE1[LenInE1]=0;
     }
     else
     { // ����� ��������� ����
       InE1[++InE1[LenInE1]]=i;
       if(InE1[LenInE1]>3)
       {
         switch(InE1[2])
         {
           case 2: // ����� ������� � ��������� ���������
             if((InE1[LenInE1]==InE1[3]*2+4) || (InE1[LenInE1]>=LenInE1-1))
             { // �������� �����
               InE1[0]=InE1[LenInE1];
               InE1[LenInE1]=0xff; // ��������� �����
             }
             break;
           case 1: // ����� ������������� ������� ������
           case 3: // �������� ����� ������
             InE1[0]=InE1[LenInE1];
             InE1[LenInE1]=0xff;  // ��������� �����
             break;
         }
       }
     }
   }
   else
   {
     if(CommandInOut[0]!=0x006A)
     { // �������� �� ������ ������
       i=a_compress(CommandInOut[0]);
       if(i<=3)
       { // ����� 1-�� ����� c ��������� �� ����� ������ � ��������� 0...3 
         InE1[LenInE1]=1;
         InE1[1]=i;
       }
     }
   }
 }
}

void TestMKABoard(void)
{ // �������� ����������������� ���������� ����
 unsigned short i,j1, j2;
 unsigned int testLet,test00,test01,test10,test11;
 unsigned int OldTestBoard;
 static unsigned short s_ErrMKA[LenMKA]={0,0,0,0,0,0,0,0,0,0,0,0};
 
 testLet=0x1; // ��� ���������� ������ ����� � ParamUKC[0]
 // ������ ������� � TestBoard
 test00=0x0; // ��� ������ (������)
 test01=0x1; // ������ �� �������� (������ � InTestData 0x0505 � 0x0A0A -> ��� �������� ���� ����� 0x20A0)
 test10=0x2; // ������ �������� (������ � InTestData 0x5565 � 0x2A6A)
 test11=0x3; // ������ ��������
 OldTestBoard=TestBoard;
 TestBoard&=0xFF000000; 
 for(i=0;i<LenMKA;i++) 
 {
   if(ParamUKC[0] & testLet)
   { // ������ �������
     j1=InTestData[i];
     j2=j1+OldInTestData[i];
     if(j2==0x0F0F)
     {
       s_ErrMKA[i]=0;
       TestBoard|=test01;    // ������ �� �������� 0x0505+0x0A0A == 0xFFFF
     }
     else
     {
       if((j2==0x7FCF) || (j1==0x20A0))   // 0x5565+0x2A6A == 0x7FCF ��� 0x20A0
       {
         s_ErrMKA[i]=0;
         TestBoard|=test10;               // ������ ��������
         if(j1==0x20A0) LoadCoef|=testLet;// ����� ������� ��������� ��� (�� ��������!)
       }
       else
       { // ������ ���� � �������� ������: ���� 0..6 - ���� ����� � ������, ��� 7 - ������ ����� �� ������� 200
         if(j1 && (j1<=(0x80+LenSport0)) && ((OldTestBoard & test11)==test10))
         { // �������� �����
           s_ErrMKA[i]=0;
           TestBoard|=test10; // ������ ��������, ���� �������� �����
           MassCallMKA[i]|=j1;
//if(j1 & 0x80) DataSave1[7]++;
         }
         else
         {
           s_ErrMKA[i]++;
           if(s_ErrMKA[i]<8000)
           {
             s_ErrMKA[i]++;
             TestBoard|=(OldTestBoard & test11);
           }
         }
       }
     }
     if((j1==0x0505) || (j1==0x0A0A) || (j1==0x5565) || (j1==0x2A6A)) OldInTestData[i]=j1;
   }
   else TestBoard|=test11;  // ������ ��������
   test01<<=2; test10<<=2; test11<<=2; testLet<<=1;
 }
}

//--------------------------------------------------------------------------//
//ParamUKC[5] - ���������� ����� ������� ISDN ������ ���
//   ==0 - ��� ������� �� ������� ���
//   ==1 - ���� ������ ��� � ����� ������� E1 (����� ���� ������ ��� �� 2-� �����)
//   ==2 - ���� ������ ��� � ����� �������� �1 (������ ��� - �����������)
//   
//ParamUKC[6] - ������ ���� ������� ����� 0x0XX���XX ���������� ����� � ����� ������ ������� ���
//   ==X0XXXX0X - ��� ������ ���1 �� 1-� ������ Sport1
//   ==X1XXXX0X - ������ ���1 �������� ���� 1-� ����� Sport1
//   ==X0XXXX1X - ������ ���1.1 �������� 0..15 �� 1-�� ������ Sport1
//   ==X1XXXX1X - ������ ���1.1 ������� 0...15 �� 1-�� ������, ���1.2 �������� 16..31 �� 1-�� ������ Sport1
//   ==XX0XXXX0 - ��� ������ ���2 �� 2-� ������ Sport1
//   ==XX1XXXX0 - ������ ���2 �������� ���� 2-� ����� Sport1
//   ==XX0XXXX1 - ������ ���2.1 �������� 0..15 �� 2-�� ������ Sport1
//   ==XX1XXXX1 - ������ ���2.1 ������� 0...15 �� 2-�� ������, ���2.2 �������� 16..31 �� 2-�� ������ Sport1
//--------------------------------------------------------------------------// 
void MKS1_DataDChannel(void)
{
  if(ParamUKC[6] & 0x02)
  { // ���1.1
    OutDataChan[15]=a_expand(hw_txbuf0[NUM_BUF][IND_BUF]) << 3;
    hw_rxbuf0[NUM_BUF][IND_BUF]=a_compress(InDataChan[15] >> 3);
    // ���� ����������������� ���1.1
    CommandInOut[1]=((short)OutTestMKS1) >> 3;  
    InTestMKS1=CommandInOut[0] << 3;
    if(ParamUKC[6] & 0x40)
    { // ���1.2
       OutDataChan[31]=a_expand(hw_txbuf2[NUM_BUF][IND_BUF]) << 3;
       hw_rxbuf2[NUM_BUF][IND_BUF]=a_compress(InDataChan[31] >> 3);
       // ���� ����������������� ���1.2
      OutDataChan[16]=OutTestMKS1_2;
      InTestMKS1_2=InDataChan[16];
    }
  }
  else
  {
    if(ParamUKC[6] & 0x40)
    { // ���1
       OutDataChan[16]=a_expand(hw_txbuf0[NUM_BUF][IND_BUF]) << 3;
       hw_rxbuf0[NUM_BUF][IND_BUF]=a_compress(InDataChan[16] >> 3);
       // ���� ����������������� ���1
       CommandInOut[1]=((short)OutTestMKS1) >> 3;  
       InTestMKS1=CommandInOut[0] << 3;
    }
  }
}

void MKS2_DataDChannel(void)
{
  if(ParamUKC[6] & 0x01)
  { // ���2.1
    OutDataChan[32+15]=a_expand(hw_txbuf1[NUM_BUF][IND_BUF]) << 3;
    hw_rxbuf1[NUM_BUF][IND_BUF]=a_compress(InDataChan[32+15] >> 3);
    // ���� ����������������� ���2.1
    OutDataChan[32]=OutTestMKS2;
    InTestMKS2=InDataChan[32];
    if(ParamUKC[6] & 0x20)
    { // ���2.2
      OutDataChan[32+31]=a_expand(hw_txbuf3[NUM_BUF][IND_BUF]) << 3;
      hw_rxbuf3[NUM_BUF][IND_BUF]=a_compress(InDataChan[32+31] >> 3);
      // ���� ����������������� ���2.2
      OutDataChan[32+16]=OutTestMKS2_2;
      InTestMKS2_2=InDataChan[32+16];
    }
  }
  else
  {
    if(ParamUKC[6] & 0x20)
    { // ���2
      OutDataChan[32+16]=a_expand(hw_txbuf1[NUM_BUF][IND_BUF]) << 3;
      hw_rxbuf1[NUM_BUF][IND_BUF]=a_compress(InDataChan[32+16] >> 3);
      // ���� ����������������� ���2
      OutDataChan[32]=OutTestMKS2;
      InTestMKS2=InDataChan[32];
    }
  }
}

///////////////////////////////////////////////////////////////////////////////
void Sport_Int(int val)
{ // ��������� ���������� �� 8 ��� �� SPORT
  /*static */short i, j;
  /*static */unsigned int ii,TimeIn;

  TimeIn=*pTIMER0_COUNTER; // ������ ������� ������� ��������� �� 8 ��� (20 ��)
  Count5mc++;
  Count10mc++;

  CopyData(); // ����������� ������� ������

  ExchangMKC(); // 120 ��  - ����� �������� ������ �� � �� ������ ���

  // ���������� ������������ ���������� ������� - 540 ��
  j=LenSport1;
  ii=TestBoard;
  for(i=0;i<LenMKA;i++)
  {
    if((ii & 0x3)!=0x2) InDataChan[j]=InDataChan[j+1]=InDataChan[j+2]=InDataChan[j+3]=0;
    j+=4; ii>>=2;
  }

  for(i=0;i<LenVirt;i++) 
  {
    if(ARUTP[i])
    { // ��� �� ������ �����������
      j=i+LenHard+LenVirt;
      InDataChan[j]=((int)InDataChan[j]*(int)KInARU[i])/1000;
    }
    // ����������� �� ������ ����������� (�� ������������!!)
    // if(DupTP[i]) InDataChan[i+LenHard+LenVirt]=((int)InDataChan[i+LenHard+LenVirt]*(int)KDupIn[i])/1000;
  }

  SumDataChan(); // ���������� �������

#ifdef __ISDN__
    if(ParamUKC[5]==1)
    {
      OutDataChan[1]=a_expand(hw_txbuf0[NUM_BUF][IND_BUF]) << 3;
      hw_rxbuf0[NUM_BUF][IND_BUF]=a_compress(InDataChan[1] >> 3);
      MKS2_DataDChannel();
    }
    else
    {
      if(ParamUKC[5]==2)
      {
/*DataSave[16]=*/        OutDataChan[1]=a_expand(hw_txbuf0[NUM_BUF][IND_BUF]) << 3;
/*DataSave[17]=*/        hw_rxbuf0[NUM_BUF][IND_BUF]=a_compress(InDataChan[1] >> 3);
/*DataSave[18]=*/        OutDataChan[2]=a_expand(hw_txbuf0[NUM_BUF][IND_BUF]) << 3;
/*DataSave[19]=*/        hw_rxbuf0[NUM_BUF][IND_BUF]=a_compress(InDataChan[2] >> 3);
      }
      else
      {
        MKS1_DataDChannel();
        MKS2_DataDChannel();
      }
    }

    IND_BUF++;
    if(IND_BUF >= LEN_BUF) 
    { NUM_BUF ^= 1; IND_BUF=0; GET_BUF=true; }
#endif

    TestMKABoard();// 580 �� - �������� ����������������� ����

    if(TimeFreq) 
    { // ��������� ������� �������� �������
      if(ChanFreq<LenSport1) i=OutDataChan[ChanFreq+LenSport1]; // ����� ����������� ������
      else
      {
        if(ChanFreq<LenHard) i=InDataChan[ChanFreq]; // ���� ����������� ������
        else i=OutDataChan[ChanFreq]; // ���� ��� ����� ��
      }
      if(TimeFreq==8000)
      {
        SignFreq=i; MeazFreq=0;
        DataMin=32767;
        DataMax=-32768;
      }
      else
      {
        if(i>DataMax) DataMax=i;
        if(i<DataMin) DataMin=i;
        if(((i>0) && (SignFreq<0)) ||
           ((i<0) && (SignFreq>0)))
        { MeazFreq++; SignFreq=i; }
      }
      TimeFreq--;
      if(!TimeFreq) MeazFreq|=0x4000; // ��������� ������� ���������
    }

    if(BigCountMiddle)
    { // ������ �������� �� �� ��� ���������
      if(BigCountMiddle==256*8)
      {
        SumDataChanMiddle=0;
        DataChanMiddle=0;
        DataMin=32767;
        DataMax=-32768;
      }
      i=OutDataChan[ChanMiddle]; // �������� ��� ��
      if(i>DataMax) DataMax=i;
      if(i<DataMin) DataMin=i;
      DataChanMiddle+=abs(i);
      BigCountMiddle--;
      if(!BigCountMiddle) SumDataChanMiddle=(DataChanMiddle >> 11) | 0x8000; // ������ �������� ��������
    }

    if(CountMiddle)
    { // ���������� ������� �������� 64�� �� ���� �������
      CountMiddle--; 
      CalcMiddle();
    }

// ------------------- "�������" �� ���������� ������� ------------------------------------------
    for(i=LenSport1;i<LenHard;i++) OutDataChan[i]+=((int)InDataChan[i]*(int)ReversA[i-LenSport1]) >> 16;

// ------------------- "�������" �� ������ ����������� ------------------------------------------
    for(i=LenHard+LenVirt;i<LenChan;i++) OutDataChan[i]+=((int)InDataChan[i]*(int)Revers[i-LenHard-LenVirt]) >> 16;

#ifdef __ISDN__
    for(i=0;i<LenVirt;i++)
    {
      if(PlayWav[i])
      {
        j=SelectEDSS[i];
        if(j && MasSelInfo[j-1].dwWavSz)
        { // ������ ������ ���������
          j--;
          OutDataChan[ChanWav[i]]=a_expand(MasSelInfo[j].pWav[PlayWav[i]++])<<3;
          if(PlayWav[i]>=MasSelInfo[j].dwWavSz) PlayWav[i]=0;
        }
      }
    }
#endif

#ifdef SINFO_ENERGO
    { // �������� ��������� ������������ ��������
extern AUDIO_MESSAGE_LIST_TP *g_psAMessList;
      AUDIO_MESSAGE_LIST_TP *psAMess=g_psAMessList;
      while(psAMess)
      {
        if(psAMess->dwACur<psAMess->dwACnt)
        {
          OutDataChan[psAMess->wNChan]=a_expand(psAMess->pbtAudio[psAMess->dwACur++]) << 3;
          if(psAMess->dwACur>=psAMess->dwACnt)
          {
            if(psAMess->btCycle)
            {
              psAMess->btCycle--;
              psAMess->dwACur=0;
            }
          }
        }
        psAMess=psAMess->psNext;
      }
    }
#else
	if(StartWavS)
	{ // ������������ ��������� �����
	  if(PlayWavS)
	  {
	    OutDataChan[ChanWavS]=a_expand(*AdrOutSound++)<<3;
	    PlayWavS++;
	    if(PlayWavS>=StartWavS) PlayWavS=StartWavS=0;
	  }
    }
#endif

  { // ------------------------------------- 2-x ��������� ������ ~ 140 �� !!!----------------------------
	xListAnalItem *AdrAnal=ListAnal.pxHead;
#define _ AdrAnal->
	while(AdrAnal)
	{
	  if(_ dF)
	  { // ���������� ���������� �������
	    if(_ chan < LenHard) i=InDataChan[_ chan];
	    else i=OutDataChan[_ chan]; 
	    _ middle+= abs (i); // ������� ������� ��������
	    i=(i * _ mult) >> 15;				//	  data=(data * _ mult) / 32767;
	    ii=_ Q11; _ Q11=((_ Q11 * _ F1) >> 14) - _ Q21 + i; _ Q21=ii; // 1-� �������
	    if(_ F2)
	    { ii=_ Q12; _ Q12=((_ Q12 * _ F2) >> 14) - _ Q22 + i; _ Q22=ii;} // 2-� �������
	    _ dF--;
	  } 
	  AdrAnal=_ pxNextChan;
	}
#undef _
  }

  { // --------------------------- ��������� ������ DTMF ~ 280 �� !!!------------------------------------------
	xListAnalDTMF *AdrAnalDTMF=ListAnalDTMF.pxHead;
#define _ AdrAnalDTMF ->
	while(AdrAnalDTMF)
	{
	  if(_ count)
	  { // ���������� ���������� �������
	    if(_ chan < LenHard) i=InDataChan[_ chan];
	    else i=OutDataChan[_ chan]; 
	    _ middle+= abs (i); // ������� ������� ��������
	    i=(i * _ mult) >> 15;					//	  data=(data * _ mult) / 32767;
	    ii=_ Q11; _ Q11=((_ Q11 * 27978) >> 14) - _ Q21 + i; _ Q21=ii; //  697 ��
	    ii=_ Q12; _ Q12=((_ Q12 * 26955) >> 14) - _ Q22 + i; _ Q22=ii; //  770 ��
	    ii=_ Q13; _ Q13=((_ Q13 * 25700) >> 14) - _ Q23 + i; _ Q23=ii; //  852 ��
	    ii=_ Q14; _ Q14=((_ Q14 * 24217) >> 14) - _ Q24 + i; _ Q24=ii; //  941 ��
	    ii=_ Q15; _ Q15=((_ Q15 * 19072) >> 14) - _ Q25 + i; _ Q25=ii; // 1209 ��
	    ii=_ Q16; _ Q16=((_ Q16 * 16324) >> 14) - _ Q26 + i; _ Q26=ii; // 1336 ��
	    ii=_ Q17; _ Q17=((_ Q17 * 13083) >> 14) - _ Q27 + i; _ Q27=ii; // 1477 ��
	    ii=_ Q18; _ Q18=((_ Q18 *  9314) >> 14) - _ Q28 + i; _ Q28=ii; // 1633 ��
	    _ count--;
	  }
	  AdrAnalDTMF=_ pxNextChan;
	}
#undef _
  }

  { // --------------------------- ��������� ������ PPSR ~ 220 �� !!!------------------------------------------
	xListAnalPPSR *AdrAnalPPSR=ListAnalPPSR.pxHead;
#define _ AdrAnalPPSR ->
	while (AdrAnalPPSR)
	{
	  if(_ count)
	  { // ���������� ���������� �������
	    if(_ chan < LenHard) i=InDataChan[_ chan];
	    else i=OutDataChan[_ chan]; 
	    _ middle+= abs (i); // ������� ������� ��������
	    i=(i * _ mult) >> 15;
	    ii=_ Q11; _ Q11=((_ Q11 * 27978) >> 14) - _ Q21 + i; _ Q21=ii; //  697 ��
	    ii=_ Q12; _ Q12=((_ Q12 * 26955) >> 14) - _ Q22 + i; _ Q22=ii; //  770 ��
	    ii=_ Q13; _ Q13=((_ Q13 * 25700) >> 14) - _ Q23 + i; _ Q23=ii; //  852 ��
	    ii=_ Q14; _ Q14=((_ Q14 * 24217) >> 14) - _ Q24 + i; _ Q24=ii; //  941 ��
	    ii=_ Q18; _ Q18=((_ Q18 *  9314) >> 14) - _ Q28 + i; _ Q28=ii; // 1633 ��
	    _ count--;
	  }
	  AdrAnalPPSR=_ pxNextChan;
	}
#undef _
  }

  { // ------------- ������ ��������� ������ PCDT ~  400 ��  --------
	xListAnalPCDT *AdrAnalPCDT=(xListAnalPCDT *) ListAnalPCDT.pxHead;
#define _ AdrAnalPCDT ->
	while (AdrAnalPCDT)
	{
	  if(_ count800)
	  { // ���������� ���������� �������
	    // ---!!!!!!!!----- ������������ ������ ����� ������ �� ������ ������������  ---------------------------
	    if(_ chan < LenHard) i=InDataChan[_ chan];
	    else i=OutDataChan[_ chan];  
	    _ middle+= abs(i); // ������� ������� ��������
	    i=(i * _ mult) >> 15;
	    ii=_ Q101; _ Q101=((_ Q101 * F316 ) >> 14) - _ Q201 + i; _ Q201=ii; //316 ��
	    ii=_ Q102; _ Q102=((_ Q102 * F430 ) >> 14) - _ Q202 + i; _ Q202=ii; //430
	    ii=_ Q103; _ Q103=((_ Q103 * F585 ) >> 14) - _ Q203 + i; _ Q203=ii; //585
	    ii=_ Q104; _ Q104=((_ Q104 * F795 ) >> 14) - _ Q204 + i; _ Q204=ii; //795
	    ii=_ Q105; _ Q105=((_ Q105 * F1080) >> 14) - _ Q205 + i; _ Q205=ii; //1080
	    ii=_ Q106; _ Q106=((_ Q106 * F1470) >> 14) - _ Q206 + i; _ Q206=ii; //1470
	    ii=_ Q107; _ Q107=0                        - _ Q207 + i; _ Q207=ii; //2000
	    ii=_ Q108; _ Q108=((_ Q108 * F890 ) >> 14) - _ Q208 + i; _ Q208=ii; //890 
	    ii=_ Q109; _ Q109=((_ Q109 * F1215) >> 14) - _ Q209 + i; _ Q209=ii; //1215
	    ii=_ Q110; _ Q110=((_ Q110 * F1360) >> 14) - _ Q210 + i; _ Q210=ii; //1360
	    ii=_ Q111; _ Q111=((_ Q111 * F1620) >> 14) - _ Q211 + i; _ Q211=ii; //1620
	    ii=_ Q112; _ Q112=((_ Q112 * F2720) >> 14) - _ Q212 + i; _ Q212=ii; //2720
	    _ count800--;
	  }
	  AdrAnalPCDT=_ pxNextChan;
	}
#undef _
  }

#ifdef __ADACE__
// ------------------------------------- 2-x ��������� ������ ����� ~ 140 �� !!!----------------------------
#define _ AdrAnalADACE ->
	AdrAnalADACE=ListAnalADACE.pxHead;
	while (AdrAnalADACE)
	{ // ���������� ��� ������ �������
	  if(_ count)
	  { // ���������� ���������� �������
	    if(_ chan < LenHard) data=InDataChan[_ chan];
	    else data=OutDataChan[_ chan]; 
	    _ middle+=abs(data); // ������� ������� ��������
	    data=(data * _ mult) >> 15; // data=(data * _ mult) / 32767;
	    ii=_ Q11; _ Q11=((_ Q11 * F1200) >> 14) - _ Q21 + data; _ Q21=ii; // 1-� �������
	    if(_ F2)
	    { ii=_ Q12; _ Q12=((_ Q12 * F1600) >> 14) - _ Q22 + data; _ Q22=ii;} // 2-� �������
	    _ count--;
	  } 
	  AdrAnalADACE=_ pxNextChan;
	}
#undef _
#endif

  { // --------------------------- ������ ��������� ������ ��� ~  1720 �� !!! (1780 � ��������������� ���������) --------
	xListAnalTDCF *AdrAnalTDC=ListAnalTDCF.pxHead;
#define _ AdrAnalTDC ->
	while (AdrAnalTDC)
	{ // ���������� ��� ������ �������
	  if(_ count800)
	  { // ���������� ���������� �������
	    if(_ chan < LenHard) i=InDataChan[_ chan];
	    else i=OutDataChan[_ chan];  
	    _ middle+= abs(i);	// ������� ������� ��������
	    i=(i * _ mult) >> 15;
  
	    ii=_ Q101; _ Q101=((_ Q101 * F1605) >> 14) - _ Q201 + i; _ Q201=ii; //1605 ��

	    ii=_ Q102; _ Q102=((_ Q102 * F317)  >> 14) - _ Q202 + i; _ Q202=ii; //317
	    ii=_ Q103; _ Q103=((_ Q103 * F430 ) >> 14) - _ Q203 + i; _ Q203=ii; //430
	    ii=_ Q104; _ Q104=((_ Q104 * F588 ) >> 14) - _ Q204 + i; _ Q204=ii; //588
	    ii=_ Q105; _ Q105=((_ Q105 * F791 ) >> 14) - _ Q205 + i; _ Q205=ii; //791
	    ii=_ Q106; _ Q106=((_ Q106 * F882 ) >> 14) - _ Q206 + i; _ Q206=ii; //882
	    ii=_ Q107; _ Q107=((_ Q107 * F1085) >> 14) - _ Q207 + i; _ Q207=ii; //1085
	    ii=_ Q108; _ Q108=((_ Q108 * F1221) >> 14) - _ Q208 + i; _ Q208=ii; //1221
	    ii=_ Q109; _ Q109=((_ Q109 * F1469) >> 14) - _ Q209 + i; _ Q209=ii; //1469
	    ii=_ Q110; _ Q110=((_ Q110 * F1989) >> 14) - _ Q210 + i; _ Q210=ii; //1989
	    ii=_ Q111; _ Q111=((_ Q111 * F2713) >> 14) - _ Q211 + i; _ Q211=ii; //2713

	    // ������ ���� ������ ����� ������ ��� ������������� -------------------------------
//	    ii=_ Q112; _ Q112=- _ Q212 + data; _ Q212=ii; 						//2000
//	    ii=_ Q113; _ Q113=((_ Q113 * F890 ) >> 14) - _ Q213 + i; _ Q213=ii; //890
//	    ii=_ Q114; _ Q114=((_ Q114 * F1215) >> 14) - _ Q214 + i; _ Q214=ii; //1215
	    // ---------------------------------------------------------------------------------

	    ii=_ Q115; _ Q115=((_ Q115 * F362 ) >> 14) - _ Q215 + i; _ Q215=ii; //362
	    ii=_ Q116; _ Q116=((_ Q116 * F475 ) >> 14) - _ Q216 + i; _ Q216=ii; //475
	    ii=_ Q117; _ Q117=((_ Q117 * F520 ) >> 14) - _ Q217 + i; _ Q217=ii; //520
	    ii=_ Q118; _ Q118=((_ Q118 * F633 ) >> 14) - _ Q218 + i; _ Q218=ii; //633
	    ii=_ Q119; _ Q119=((_ Q119 * F701 ) >> 14) - _ Q219 + i; _ Q219=ii; //701
	    ii=_ Q120; _ Q120=((_ Q120 * F746 ) >> 14) - _ Q220 + i; _ Q220=ii; //746
	    ii=_ Q121; _ Q121=((_ Q121 * F836 ) >> 14) - _ Q221 + i; _ Q221=ii; //836
	    ii=_ Q122; _ Q122=((_ Q122 * F927 ) >> 14) - _ Q222 + i; _ Q222=ii; //927
	    ii=_ Q123; _ Q123=((_ Q123 * F972 ) >> 14) - _ Q223 + i; _ Q223=ii; //972

	    ii=_ Q124; _ Q124=((_ Q124 * F1017) >> 14) - _ Q224 + i; _ Q224=ii; //1017
	    ii=_ Q125; _ Q125=((_ Q125 * F1130) >> 14) - _ Q225 + i; _ Q225=ii; //1130
	    ii=_ Q126; _ Q126=((_ Q126 * F1243) >> 14) - _ Q226 + i; _ Q226=ii; //1243
	    ii=_ Q127; _ Q127=((_ Q127 * F1289) >> 14) - _ Q227 + i; _ Q227=ii; //1289
	    ii=_ Q128; _ Q128=((_ Q128 * F1334) >> 14) - _ Q228 + i; _ Q228=ii; //1334
	    ii=_ Q129; _ Q129=((_ Q129 * F1379) >> 14) - _ Q229 + i; _ Q229=ii; //1379
	    ii=_ Q130; _ Q130=((_ Q130 * F1424) >> 14) - _ Q230 + i; _ Q230=ii; //1424
	    ii=_ Q131; _ Q131=((_ Q131 * F1515) >> 14) - _ Q231 + i; _ Q231=ii; //1515
	    ii=_ Q132; _ Q132=((_ Q132 * F1560) >> 14) - _ Q232 + i; _ Q232=ii; //1560
	    ii=_ Q133; _ Q133=((_ Q133 * F1650) >> 14) - _ Q233 + i; _ Q233=ii; //1650
	    ii=_ Q134; _ Q134=((_ Q134 * F1695) >> 14) - _ Q234 + i; _ Q234=ii; //1695
	    ii=_ Q135; _ Q135=((_ Q135 * F1741) >> 14) - _ Q235 + i; _ Q235=ii; //1741
	    ii=_ Q136; _ Q136=((_ Q136 * F1786) >> 14) - _ Q236 + i; _ Q236=ii; //1786
	    ii=_ Q137; _ Q137=((_ Q137 * F1831) >> 14) - _ Q237 + i; _ Q237=ii; //1831
	    ii=_ Q138; _ Q138=((_ Q138 * F1876) >> 14) - _ Q238 + i; _ Q238=ii; //1876
	    ii=_ Q139; _ Q139=((_ Q139 * F1921) >> 14) - _ Q239 + i; _ Q239=ii; //1921

	    ii=_ Q140; _ Q140=((_ Q140 * F2261) >> 14) - _ Q240 + i; _ Q240=ii; //2261
	    ii=_ Q141; _ Q141=((_ Q141 * F2306) >> 14) - _ Q241 + i; _ Q241=ii; //2306 
	    ii=_ Q142; _ Q142=((_ Q142 * F2351) >> 14) - _ Q242 + i; _ Q242=ii; //2351
	    ii=_ Q143; _ Q143=((_ Q143 * F2396) >> 14) - _ Q243 + i; _ Q243=ii; //2396
	    ii=_ Q144; _ Q144=((_ Q144 * F2441) >> 14) - _ Q244 + i; _ Q244=ii; //2441
	    ii=_ Q145; _ Q145=((_ Q145 * F2487) >> 14) - _ Q245 + i; _ Q245=ii; //2487
	    ii=_ Q146; _ Q146=((_ Q146 * F2532) >> 14) - _ Q246 + i; _ Q246=ii; //2532
	    ii=_ Q147; _ Q147=((_ Q147 * F2577) >> 14) - _ Q247 + i; _ Q247=ii; //2577
	    ii=_ Q148; _ Q148=((_ Q148 * F2622) >> 14) - _ Q248 + i; _ Q248=ii; //2622
	    ii=_ Q149; _ Q149=((_ Q149 * F2667) >> 14) - _ Q249 + i; _ Q249=ii; //2667
	    ii=_ Q150; _ Q150=((_ Q150 * F2758) >> 14) - _ Q250 + i; _ Q250=ii; //2758
	    ii=_ Q151; _ Q151=((_ Q151 * F2803) >> 14) - _ Q251 + i; _ Q251=ii; //2803
	    ii=_ Q152; _ Q152=((_ Q152 * F2848) >> 14) - _ Q252 + i; _ Q252=ii; //2848
	    ii=_ Q153; _ Q153=((_ Q153 * F2894) >> 14) - _ Q253 + i; _ Q253=ii; //2894
	    ii=_ Q154; _ Q154=((_ Q154 * F2939) >> 14) - _ Q254 + i; _ Q254=ii; //2939
	    ii=_ Q155; _ Q155=((_ Q155 * F2984) >> 14) - _ Q255 + i; _ Q255=ii; //2984

	    ii=_ Q156; _ Q156=((_ Q156 * F3029) >> 14) - _ Q256 + i; _ Q256=ii; //3029
	    ii=_ Q157; _ Q157=((_ Q157 * F3074) >> 14) - _ Q257 + i; _ Q257=ii; //3074
	    ii=_ Q158; _ Q158=((_ Q158 * F3120) >> 14) - _ Q258 + i; _ Q258=ii; //3120
	    ii=_ Q159; _ Q159=((_ Q159 * F3165) >> 14) - _ Q259 + i; _ Q259=ii; //3165

	  _ count800--;
	 }
	 AdrAnalTDC=_ pxNextChan;
	}
#undef _
  }

  {// ------------- ��������� ��������� ������ ��� ~  400 ��  --------
	xListAnalTDCL *AdrAnalTDCL=ListAnalTDCL.pxHead;
#define _ AdrAnalTDCL ->
	while (AdrAnalTDCL)
	{ // ���������� ��� ������ �������
	 if(_ count800)
	 { // ���������� ���������� �������
	  if(_ chan < LenHard) i=InDataChan[_ chan];
	  else i=OutDataChan[_ chan];  
	  _ middle+= abs(i);	// ������� ������� ��������
	  i=(i * _ mult) >> 15;
  
	  ii=_ Q101; _ Q101=((_ Q101 * F1605) >> 14) - _ Q201 + i; _ Q201=ii; //1605 ��

	  ii=_ Q102; _ Q102=((_ Q102 * F317)  >> 14) - _ Q202 + i; _ Q202=ii; //317
	  ii=_ Q103; _ Q103=((_ Q103 * F430 ) >> 14) - _ Q203 + i; _ Q203=ii; //430
	  ii=_ Q104; _ Q104=((_ Q104 * F588 ) >> 14) - _ Q204 + i; _ Q204=ii; //588
	  ii=_ Q105; _ Q105=((_ Q105 * F791 ) >> 14) - _ Q205 + i; _ Q205=ii; //791
	  ii=_ Q106; _ Q106=((_ Q106 * F882 ) >> 14) - _ Q206 + i; _ Q206=ii; //882
	  ii=_ Q107; _ Q107=((_ Q107 * F1085) >> 14) - _ Q207 + i; _ Q207=ii; //1085
	  ii=_ Q108; _ Q108=((_ Q108 * F1221) >> 14) - _ Q208 + i; _ Q208=ii; //1221
	  ii=_ Q109; _ Q109=((_ Q109 * F1469) >> 14) - _ Q209 + i; _ Q209=ii; //1469
	  ii=_ Q110; _ Q110=((_ Q110 * F1989) >> 14) - _ Q210 + i; _ Q210=ii; //1989
	  ii=_ Q111; _ Q111=((_ Q111 * F2713) >> 14) - _ Q211 + i; _ Q211=ii; //2713

	  _ count800--;
	 }
	 AdrAnalTDCL=_ pxNextChan;
	}
#undef _
  }

  {
// --------------------------- ����������� �� ������� ~ 345 �� / ����� ------------------------------
	xListRecord *AdrRecord=ListRecord.pxHead;
#define _ AdrRecord->
	while(AdrRecord)
	{
	  if(_ block & 1)
	  {  // ���������� ����, ���� ������ ������������� _ block == 1
	    if(_ NumBuf)
	    {
	      _ BufSpeak2[_ IndBuf2++]=0x5D;
	      if(_ IndBuf2 >= LenSound)
	      { _ NumBuf=0; _ IndBuf1=0; }
	    }
	    else
	    {
	      _ BufSpeak1[_ IndBuf1++]=0x5D;
	      if(_ IndBuf1 >= LenSound)
	      { _ NumBuf=1; _ IndBuf2=0; }
	    }
	  }
	  else
	  {
	    if(_ NumBuf)
	    {
	      if(_ TypeRecord) _ BufSpeak2[_ IndBuf2++]=a_compress(((int)InDataChan[_ chan] + (int)OutDataChan[_ chan]) >> 3); // ������ ����� � ������
	      else _ BufSpeak2[_ IndBuf2++]=a_compress(InDataChan[_ chan]>>3); // ��� ������ �����
	      if(_ IndBuf2 >= LenSound)
	      {_ NumBuf=0; _ IndBuf1=0; }
	    }
	    else
	    {
	      if(_ TypeRecord) _ BufSpeak1[_ IndBuf1++]=a_compress(((int)InDataChan[_ chan] + (int)OutDataChan[_ chan]) >> 3);  // ������ ����� � ������
	      else _ BufSpeak1[_ IndBuf1++]=a_compress(InDataChan[_ chan]>>3); // ��� ������ �����
	      if(_ IndBuf1 >= LenSound)
	      { _ NumBuf=1; _ IndBuf2=0; }
	    }
	  }
	  AdrRecord=_ pxNextRec;
	}
#undef _
  }

  { // --------------------------- �������������������� �� ������ ------------------------------------
	xListPlay *AdrPlay=ListPlay.pxHead;
#define _ AdrPlay ->
	while(AdrPlay)
	{
	  if(_ SyncTime < 0)
	  { // ������� �������
	    OutDataChan[_ ChanPlay]=a_expand (_ BufPlay[_ IndBufPlay++][_ NumBufPlay]) << 3;
	    if(_ IndBufPlay >= LenSound)
	    { 
	       _ IndBufPlay=0;
	       _ NumBufPlay=(_ NumBufPlay + 1) % 2;
	       _ NewAudioData=true;
	    }
	    _ SyncTime=0;
	 }
	 if(!_ SyncTime)
	 { // == 0 - �� ��������� �������, ���, �� ���������� ������� 
	   OutDataChan[_ ChanPlay]=a_expand (_ BufPlay[_ IndBufPlay++][_ NumBufPlay]) << 3;
	   if(_ IndBufPlay >= LenSound)
	   {
	     _ IndBufPlay=0;
	     _ NumBufPlay=(_ NumBufPlay + 1) % 2;
	     _ NewAudioData=true;
	   }
	 }
	 _ TekTime++; // ������� �����
	 _ SyncTime=false;
	 AdrPlay=(xListPlay *) _ pxNextPlay;
	}
#undef _
  }

  if(LenKeep)
  { // ����� ������� ��������� �������� � ��������� ��
    InDataChan[LenHard+LenVirt+LenVirt]=BufPlayKeep[IndBufPlayKeep++][NumBufPlayKeep];
    if(IndBufPlayKeep >= LenSound)
    { IndBufPlayKeep=0; NumBufPlayKeep=(NumBufPlayKeep + 1) & 1; NextPlayKeep=true;}
  }

  { // ��������� ��������� ������� �� ������� ���������
    xListGenChanItem *Adr=MasGenChanItem;
    for(i=0;i<LenGener;i++,Adr++)
    {
#define _ Adr->
      if(_ chan)
      {
        if(_ Ton) 
        { // ���������� ��������� �������
          _ sum1 += _ F1;
          _ sum2 += _ F2;
          Ampl2=_ Ampl2; // ���������� ���������� Ampl2 ������������ � sin2F
          OutDataChan[_ chan]=sin2F(_ sum1,_ sum2,_ Ampl1);
        }
        else OutDataChan[_ chan]=0;
      }
#undef _
    }
  }
  
  Sys_Sec++;
  if(Sys_Sec>=8000)
  { Sys_Sec=0; Time_Sys++; }

  // ���������� ������ �� ���������� ������
  for(i=0;i<LenSport0;i++) OutDataChan[LenSport1+i]&=g_wMasDisOut[i];

  TimeIn=*pTIMER0_COUNTER-TimeIn; // ���������� ������� ��������� ����������
  if(TimeIn>Time8Hold) Time8Hold=TimeIn;
  *pDMA1_IRQ_STATUS|=DMA_DONE; // ����� �������� ����������
  ssync();
}

