#include "..\Sinfo.h"
#include <signal.h>
#include <fract.h>
#include <math.h>

///////////////////////////////////////////////////////////////////////////////
void *pvPortMalloc(size_t xSize);
void vPortFree(void *pv);

///////////////////////////////////////////////////////////////////////////////
extern sListAnalTDCF ListAnalTDCF; // ��������� ����������� �������� TDC
extern sListAnalTDCL ListAnalTDCL; // ��������� ����������� �������� TDC
extern sListAnalPCDT ListAnalPCDT; // ��������� ����������� �������� PCDT
extern sListAnalDTMF ListAnalDTMF; // ��������� ����������� �������� DTMF
extern sListAnalPPSR ListAnalPPSR; // ��������� ����������� �������� PPSR
extern sListSpec ListSpec; // ��������� ��������� ������������ ������
extern sListAnal ListAnal; // ��������� ����������� ������������� ��������

extern const unsigned char Comb_TDC[]; // ������ ������ ��������� ���

///////////////////////////////////////////////////////////////////////////////
const signed short F_Indiv_TDC[]={362,475,520,633,701,746,836,927,972,1017,1130,1243,1289,1334,1379,1424,1515,1560,
								  1650,1695,1741,1786,1831,1876,1921,2261,2306,2351,2396,2441,2487,2532,2577,
								  2622,2667,2758,2803,2848,2894,2939,2984,3029,3074,3120,3165};

const unsigned char NomAbonPCDT[12][12]={
{  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11},
{ 12,  0, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22},
{ 23, 24,  0, 25, 26, 27, 28, 29, 30, 31, 32, 33},
{ 34, 35, 36,  0, 37, 38, 39, 40, 41, 42, 43, 44},
{ 45, 46, 47, 48,  0, 49, 50, 51, 52, 53, 54, 55},
{ 56, 57, 58, 59, 60,  0, 61, 62, 63, 64, 65, 66},
{ 67, 68, 69, 70, 71, 72,  0, 73, 74, 75, 76, 77},
{ 78, 79, 80, 81, 82, 83, 84,  0, 85, 86, 87, 88},
{ 89, 90, 91, 92, 93, 94, 95, 96,  0, 97, 98, 99},
{100,101,102,103,104,105,106,107,108,  0,109,110},
{111,112,113,114,115,116,117,118,119,120,  0,121},
{122,123,124,125,126,127,128,129,130,131,132,  0}};

short ErrMallocAnal; // ������ �������������� ������ ��� �������� ������ ���������� �������

unsigned char MasOutAnalize[LenInOutData]; // ������ ��� �������� ������ �� ���������

///////////////////////////////////////////////////////////////////////////////
//--------------------------------------------------------------------------//
// ������ ������� ������� ������� ��� �� ������							//
//--------------------------------------------------------------------------//
unsigned char ReadAnalTDC(unsigned char chan)
{
  xListAnalTDCF *AdrAnal=ListAnalTDCF.pxHead;
#define _ AdrAnal->
	while(AdrAnal)
	{// ���������� ��� ������ �������
	  if(_ chan == chan) return 1; 
	  AdrAnal = _ pxNextChan;
	}
#undef _
  return 0;
}

//--------------------------------------------------------------------------//
// ������ ������� ���������� ������� ��� �� ������							//
//--------------------------------------------------------------------------//
unsigned char ReadAnalTDCL(unsigned char chan)
{
  xListAnalTDCL *AdrAnal=ListAnalTDCL.pxHead;
#define _ AdrAnal->
	while(AdrAnal)
	{ // ���������� ��� ������ �������
	  if(_ chan == chan) return 1; 
	  AdrAnal= _ pxNextChan;
	}
#undef _
 return 0;
}

//--------------------------------------------------------------------------//
// ������ ������� �������� ��� �� ������� � ������ ����������				//
//--------------------------------------------------------------------------//
unsigned char InitAnalTDCFull(unsigned char chan)
{
   xListAnalTDCF *AdrInTDC, *AdrOutTDC, *AdrTDC;
	if(ReadAnalTDC(chan)) return 0xFF; // ������ ��� ����������
    // �������� �������� ������� �������
	if(!(AdrOutTDC = pvPortMalloc(sizeof (xListAnalTDCF)))) // �������������� ������ ��� Item
	{ ErrMallocAnal++; return false; }
	else
	{
#define _ AdrOutTDC ->
	   _ chan = chan;		// ������������� ���������� ������ ��������
	 _ Q101 = _ Q201 = _ Q102 = _ Q202 = _ Q103 = _ Q203 = _ Q104 = _ Q204 = _ Q105 = _ Q205 = 
	 _ Q106 = _ Q206 = _ Q107 = _ Q207 = _ Q108 = _ Q208 = _ Q109 = _ Q209 = _ Q110 = _ Q210 =
	 _ Q111 = _ Q211 = /*_ Q112 = _ Q212 = _ Q113 = _ Q213 = _ Q114 = _ Q214 = */_ Q115 = _ Q215 = 
	 _ Q116 = _ Q216 = _ Q117 = _ Q217 = _ Q118 = _ Q218 = _ Q119 = _ Q219 = _ Q120 = _ Q220 =
	 _ Q121 = _ Q221 = _ Q122 = _ Q222 = _ Q123 = _ Q223 = _ Q124 = _ Q224 = _ Q125 = _ Q225 = 
	 _ Q126 = _ Q226 = _ Q127 = _ Q227 = _ Q128 = _ Q228 = _ Q129 = _ Q229 = _ Q130 = _ Q230 =
	 _ Q131 = _ Q231 = _ Q132 = _ Q232 = _ Q133 = _ Q233 = _ Q134 = _ Q234 = _ Q135 = _ Q235 = 
	 _ Q136 = _ Q236 = _ Q137 = _ Q237 = _ Q138 = _ Q238 = _ Q139 = _ Q239 = _ Q140 = _ Q240 =
	 _ Q141 = _ Q241 = _ Q142 = _ Q242 = _ Q143 = _ Q243 = _ Q144 = _ Q244 = _ Q145 = _ Q245 = 
	 _ Q146 = _ Q246 = _ Q147 = _ Q247 = _ Q148 = _ Q248 = _ Q149 = _ Q249 = _ Q150 = _ Q250 =
	 _ Q151 = _ Q251 = _ Q152 = _ Q252 = _ Q153 = _ Q253 = _ Q154 = _ Q254 = _ Q155 = _ Q255 = 
	 _ Q156 = _ Q256 = _ Q157 = _ Q257 = _ Q158 = _ Q258 = _ Q159 = _ Q259 =
 	   _ middle = 0;
 	  _ pxPrevious = _ pxNextChan = 0;
 	 _ count800 = 800; 
#undef _
	 AdrInTDC=ListAnalTDCF.pxHead;
	 if(AdrInTDC)
	 { // �� ������ �������
	   do
	   { // ����� ������ ���������� ��������
	     AdrTDC = AdrInTDC;
	     AdrInTDC = AdrInTDC -> pxNextChan;
	   }
	   while(AdrInTDC);
	   AdrTDC -> pxNextChan = AdrOutTDC; // ����� ������ �������� � ������� ����������� ��������
	   AdrOutTDC -> pxPrevious = AdrTDC; // ����� ����������� �������� � ������� ������ ��������
	 }
	 else ListAnalTDCF.pxHead = AdrOutTDC; // ��������� ������ ���������
    }
  return true;
}

//--------------------------------------------------------------------------//
// ������ ���������� ������� �������� ��� �� ������� � ������ ����������	//
//--------------------------------------------------------------------------//
unsigned char InitAnalTDCSmall(unsigned char chan)
{
  xListAnalTDCL *AdrInTDC, *AdrOutTDC, *AdrTDC;
  if(ReadAnalTDCL(chan)) return 0xFF; // ������ ��� ����������
  if(!(AdrOutTDC = pvPortMalloc(sizeof (xListAnalTDCL)))) // �������������� ������ ��� Item
  { ErrMallocAnal++; return false; }
  else
  {
#define _ AdrOutTDC ->
    _ chan = chan; // ������������� ���������� ������ ��������
	 _ Q101 = _ Q201 = _ Q102 = _ Q202 = _ Q103 = _ Q203 = _ Q104 = _ Q204 = _ Q105 = _ Q205 = 
	 _ Q106 = _ Q206 = _ Q107 = _ Q207 = _ Q108 = _ Q208 = _ Q109 = _ Q209 = _ Q110 = _ Q210 =
	 _ Q111 = _ Q211 = _ middle = 0;
 	  _ pxPrevious = _ pxNextChan = 0;
 	 _ count800 = 800; 
#undef _
	 AdrInTDC=ListAnalTDCL.pxHead; // ����� ������� �������
	 if(AdrInTDC)
	 { // �� ������ �������
	   AdrInTDC = ListAnalTDCL.pxHead;// ����� ������ ���������� ��������
	   do
	   {
	     AdrTDC = AdrInTDC;
	     AdrInTDC = AdrInTDC -> pxNextChan;
	   }
	   while (AdrInTDC);
	   AdrTDC -> pxNextChan = AdrOutTDC; // ����� ������ �������� � ������� ����������� ��������
	   AdrOutTDC -> pxPrevious = AdrTDC; // ����� ����������� �������� � ������� ������ ��������
	 }
	 else ListAnalTDCL.pxHead = AdrOutTDC; // ��������� ������ ���������
  }
  return true;
}

void AnalizeTDC(void)
{ // ������ ��������� ������ ���
  unsigned char i,j,Nab;
  xListAnalTDCF *AdrAnalTDC;

#define _ AdrAnalTDC->
  AdrAnalTDC=ListAnalTDCF.pxHead;
  while (AdrAnalTDC)
  { // ���������� ��� ������ �������
      if(!_ count800)
      { // ���������� ���������� �������
//	  _ Q101 = sqrt(abs((float)_ Q101 * (float) _ Q101 + (float)_ Q201 * (float) _ Q201 -
//       ((float)_ Q101 * (float)_ Q201 * (float)F1605) / Mult));
       
	  _ Q102 = sqrt(abs((float)_ Q102 * (float) _ Q102 + (float)_ Q202 * (float) _ Q202 -
       ((float)_ Q102 * (float)_ Q202 * (float)F317 ) / Mult));
	  _ Q103 = sqrt(abs((float)_ Q103 * (float) _ Q103 + (float)_ Q203 * (float) _ Q203 -
       ((float)_ Q103 * (float)_ Q203 * (float)F430 ) / Mult));
	  _ Q104 = sqrt(abs((float)_ Q104 * (float) _ Q104 + (float)_ Q204 * (float) _ Q204 -
       ((float)_ Q104 * (float)_ Q204 * (float)F588 ) / Mult));
	  _ Q105 = sqrt(abs((float)_ Q105 * (float) _ Q105 + (float)_ Q205 * (float) _ Q205 -
       ((float)_ Q105 * (float)_ Q205 * (float)F791 ) / Mult));
	  _ Q106 = sqrt(abs((float)_ Q106 * (float) _ Q106 + (float)_ Q206 * (float) _ Q206 -
       ((float)_ Q106 * (float)_ Q206 * (float)F882 ) / Mult));
	  _ Q107 = sqrt(abs((float)_ Q107 * (float) _ Q107 + (float)_ Q207 * (float) _ Q207 -
       ((float)_ Q107 * (float)_ Q207 * (float)F1085) / Mult));
	  _ Q108 = sqrt(abs((float)_ Q108 * (float) _ Q108 + (float)_ Q208 * (float) _ Q208 -
       ((float)_ Q108 * (float)_ Q208 * (float)F1221) / Mult));
	  _ Q109 = sqrt(abs((float)_ Q109 * (float) _ Q109 + (float)_ Q209 * (float) _ Q209 -
       ((float)_ Q109 * (float)_ Q209 * (float)F1469) / Mult));
	  _ Q110 = sqrt(abs((float)_ Q110 * (float) _ Q110 + (float)_ Q210 * (float) _ Q210 -
       ((float)_ Q110 * (float)_ Q210 * (float)F1989) / Mult));
	  _ Q111 = sqrt(abs((float)_ Q111 * (float) _ Q111 + (float)_ Q211 * (float) _ Q211 -
       ((float)_ Q111 * (float)_ Q211 * (float)F2713) / Mult));
       
//	  _ Q112 = sqrt(abs((float)_ Q112 * (float) _ Q112 + (float)_ Q212 * (float) _ Q212)); 	// -
//	  // ((float)_ Q112 * (float)_ Q212 * (float)F2000) / Mult));	   
//	  _ Q113 = sqrt(abs((float)_ Q113 * (float) _ Q113 + (float)_ Q213 * (float) _ Q213 -
//       ((float)_ Q113 * (float)_ Q213 * (float)F890 ) / Mult));
//	  _ Q114 = sqrt(abs((float)_ Q114 * (float) _ Q114 + (float)_ Q214 * (float) _ Q214 -
//       ((float)_ Q114 * (float)_ Q214 * (float)F1215) / Mult));
       
	  _ Q115 = sqrt(abs((float)_ Q115 * (float) _ Q115 + (float)_ Q215 * (float) _ Q215 -
       ((float)_ Q115 * (float)_ Q215 * (float)F362 ) / Mult));
	  _ Q116 = sqrt(abs((float)_ Q116 * (float) _ Q116 + (float)_ Q216 * (float) _ Q216 -
       ((float)_ Q116 * (float)_ Q216 * (float)F475 ) / Mult));
	  _ Q117 = sqrt(abs((float)_ Q117 * (float) _ Q117 + (float)_ Q217 * (float) _ Q217 -
       ((float)_ Q117 * (float)_ Q217 * (float)F520 ) / Mult));
	  _ Q118 = sqrt(abs((float)_ Q118 * (float) _ Q118 + (float)_ Q218 * (float) _ Q218 -
       ((float)_ Q118 * (float)_ Q218 * (float)F633 ) / Mult));
	  _ Q119 = sqrt(abs((float)_ Q119 * (float) _ Q119 + (float)_ Q219 * (float) _ Q219 -
       ((float)_ Q119 * (float)_ Q219 * (float)F701 ) / Mult));
	  _ Q120 = sqrt(abs((float)_ Q120 * (float) _ Q120 + (float)_ Q220 * (float) _ Q220 -
       ((float)_ Q120 * (float)_ Q220 * (float)F746 ) / Mult));
	  _ Q121 = sqrt(abs((float)_ Q121 * (float) _ Q121 + (float)_ Q221 * (float) _ Q221 -
       ((float)_ Q121 * (float)_ Q221 * (float)F836 ) / Mult));
	  _ Q122 = sqrt(abs((float)_ Q122 * (float) _ Q122 + (float)_ Q222 * (float) _ Q222 -
       ((float)_ Q122 * (float)_ Q222 * (float)F927 ) / Mult));
	  _ Q123 = sqrt(abs((float)_ Q123 * (float) _ Q123 + (float)_ Q223 * (float) _ Q223 -
       ((float)_ Q123 * (float)_ Q223 * (float)F972 ) / Mult));
       
	  _ Q124 = sqrt(abs((float)_ Q124 * (float) _ Q124 + (float)_ Q224 * (float) _ Q224 -
       ((float)_ Q124 * (float)_ Q224 * (float)F1017) / Mult));
	  _ Q125 = sqrt(abs((float)_ Q125 * (float) _ Q125 + (float)_ Q225 * (float) _ Q225 -
       ((float)_ Q125 * (float)_ Q225 * (float)F1130) / Mult));
	  _ Q126 = sqrt(abs((float)_ Q126 * (float) _ Q126 + (float)_ Q226 * (float) _ Q226 -
       ((float)_ Q126 * (float)_ Q226 * (float)F1243) / Mult));
	  _ Q127 = sqrt(abs((float)_ Q127 * (float) _ Q127 + (float)_ Q227 * (float) _ Q227 -
       ((float)_ Q127 * (float)_ Q227 * (float)F1289) / Mult));
	  _ Q128 = sqrt(abs((float)_ Q128 * (float) _ Q128 + (float)_ Q228 * (float) _ Q228 -
       ((float)_ Q128 * (float)_ Q228 * (float)F1334) / Mult));
	  _ Q129 = sqrt(abs((float)_ Q129 * (float) _ Q129 + (float)_ Q229 * (float) _ Q229 -
       ((float)_ Q129 * (float)_ Q229 * (float)F1379) / Mult));
	  _ Q130 = sqrt(abs((float)_ Q130 * (float) _ Q130 + (float)_ Q230 * (float) _ Q230 -
       ((float)_ Q130 * (float)_ Q230 * (float)F1424) / Mult));
	  _ Q131 = sqrt(abs((float)_ Q131 * (float) _ Q131 + (float)_ Q231 * (float) _ Q231 -
       ((float)_ Q131 * (float)_ Q231 * (float)F1515) / Mult));
	  _ Q132 = sqrt(abs((float)_ Q132 * (float) _ Q132 + (float)_ Q232 * (float) _ Q232 -
       ((float)_ Q132 * (float)_ Q232 * (float)F1560) / Mult));
	  _ Q133 = sqrt(abs((float)_ Q133 * (float) _ Q133 + (float)_ Q233 * (float) _ Q233 -
       ((float)_ Q133 * (float)_ Q233 * (float)F1650) / Mult));
	  _ Q134 = sqrt(abs((float)_ Q134 * (float) _ Q134 + (float)_ Q234 * (float) _ Q234 -
       ((float)_ Q134 * (float)_ Q234 * (float)F1695) / Mult));
	  _ Q135 = sqrt(abs((float)_ Q135 * (float) _ Q135 + (float)_ Q235 * (float) _ Q235 -
       ((float)_ Q135 * (float)_ Q235 * (float)F1741) / Mult));
	  _ Q136 = sqrt(abs((float)_ Q136 * (float) _ Q136 + (float)_ Q236 * (float) _ Q236 -
       ((float)_ Q136 * (float)_ Q236 * (float)F1786) / Mult));
	  _ Q137 = sqrt(abs((float)_ Q137 * (float) _ Q137 + (float)_ Q237 * (float) _ Q237 -
       ((float)_ Q137 * (float)_ Q237 * (float)F1831) / Mult));
	  _ Q138 = sqrt(abs((float)_ Q138 * (float) _ Q138 + (float)_ Q238 * (float) _ Q238 -
       ((float)_ Q138 * (float)_ Q238 * (float)F1876) / Mult));
	  _ Q139 = sqrt(abs((float)_ Q139 * (float) _ Q139 + (float)_ Q239 * (float) _ Q239 -
       ((float)_ Q139 * (float)_ Q239 * (float)F1921) / Mult));
       
	  _ Q140 = sqrt(abs((float)_ Q140 * (float) _ Q140 + (float)_ Q240 * (float) _ Q240 -
       ((float)_ Q140 * (float)_ Q240 * (float)F2261) / Mult));
	  _ Q141 = sqrt(abs((float)_ Q141 * (float) _ Q141 + (float)_ Q241 * (float) _ Q241 -
       ((float)_ Q141 * (float)_ Q241 * (float)F2306) / Mult));
	  _ Q142 = sqrt(abs((float)_ Q142 * (float) _ Q142 + (float)_ Q242 * (float) _ Q242 -
       ((float)_ Q142 * (float)_ Q242 * (float)F2351) / Mult));
	  _ Q143 = sqrt(abs((float)_ Q143 * (float) _ Q143 + (float)_ Q243 * (float) _ Q243 -
       ((float)_ Q143 * (float)_ Q243 * (float)F2396) / Mult));
	  _ Q144 = sqrt(abs((float)_ Q144 * (float) _ Q144 + (float)_ Q244 * (float) _ Q244 -
       ((float)_ Q144 * (float)_ Q244 * (float)F2441) / Mult));
	  _ Q145 = sqrt(abs((float)_ Q145 * (float) _ Q145 + (float)_ Q245 * (float) _ Q245 -
       ((float)_ Q145 * (float)_ Q245 * (float)F2487) / Mult));
	  _ Q146 = sqrt(abs((float)_ Q146 * (float) _ Q146 + (float)_ Q246 * (float) _ Q246 -
       ((float)_ Q146 * (float)_ Q246 * (float)F2532) / Mult));
	  _ Q147 = sqrt(abs((float)_ Q147 * (float) _ Q147 + (float)_ Q247 * (float) _ Q247 -
       ((float)_ Q147 * (float)_ Q247 * (float)F2577) / Mult));
	  _ Q148 = sqrt(abs((float)_ Q148 * (float) _ Q148 + (float)_ Q248 * (float) _ Q248 -
       ((float)_ Q148 * (float)_ Q248 * (float)F2622) / Mult));
	  _ Q149 = sqrt(abs((float)_ Q149 * (float) _ Q149 + (float)_ Q249 * (float) _ Q249 -
       ((float)_ Q149 * (float)_ Q249 * (float)F2667) / Mult));
	  _ Q150 = sqrt(abs((float)_ Q150 * (float) _ Q150 + (float)_ Q250 * (float) _ Q250 -
       ((float)_ Q150 * (float)_ Q250 * (float)F2758) / Mult));
	  _ Q151 = sqrt(abs((float)_ Q151 * (float) _ Q151 + (float)_ Q251 * (float) _ Q251 -
       ((float)_ Q151 * (float)_ Q251 * (float)F2803) / Mult));
	  _ Q152 = sqrt(abs((float)_ Q152 * (float) _ Q152 + (float)_ Q252 * (float) _ Q252 -
       ((float)_ Q152 * (float)_ Q252 * (float)F2848) / Mult));
	  _ Q153 = sqrt(abs((float)_ Q153 * (float) _ Q153 + (float)_ Q253 * (float) _ Q253 -
       ((float)_ Q153 * (float)_ Q253 * (float)F2894) / Mult));
	  _ Q154 = sqrt(abs((float)_ Q154 * (float) _ Q154 + (float)_ Q254 * (float) _ Q254 -
       ((float)_ Q154 * (float)_ Q254 * (float)F2939) / Mult));
	  _ Q155 = sqrt(abs((float)_ Q155 * (float) _ Q155 + (float)_ Q255 * (float) _ Q255 -
       ((float)_ Q155 * (float)_ Q255 * (float)F2984) / Mult));
       
	  _ Q156 = sqrt(abs((float)_ Q156 * (float) _ Q156 + (float)_ Q256 * (float) _ Q256 -
       ((float)_ Q156 * (float)_ Q256 * (float)F3029) / Mult));
	  _ Q157 = sqrt(abs((float)_ Q157 * (float) _ Q157 + (float)_ Q257 * (float) _ Q257 -
       ((float)_ Q157 * (float)_ Q257 * (float)F3074) / Mult));
	  _ Q158 = sqrt(abs((float)_ Q158 * (float) _ Q158 + (float)_ Q258 * (float) _ Q258 -
       ((float)_ Q158 * (float)_ Q258 * (float)F3120) / Mult));
	  _ Q159 = sqrt(abs((float)_ Q159 * (float) _ Q159 + (float)_ Q259 * (float) _ Q259 -
       ((float)_ Q159 * (float)_ Q259 * (float)F3165) / Mult));
//      if(_ Q101>LevelAnal) {if(_ YesFreq[ 0]<10) _ YesFreq[ 0]++; if(_ YesFreq[0]>=8) _ YesFreq[0]=30;} else {_ YesFreq[0]-=2; 
//       if(_ YesFreq[0]<0) _ YesFreq[0]=0;}
      if(_ Q102>LevelAnal) {if(_ YesFreq[ 1]<10) _ YesFreq[ 1]++;} else {_ YesFreq[ 1]-=2; if(_ YesFreq[ 1]<0) _ YesFreq[ 1]=0;}
      if(_ Q103>LevelAnal) {if(_ YesFreq[ 2]<10) _ YesFreq[ 2]++;} else {_ YesFreq[ 2]-=2; if(_ YesFreq[ 2]<0) _ YesFreq[ 2]=0;}
      if(_ Q104>LevelAnal) {if(_ YesFreq[ 3]<10) _ YesFreq[ 3]++;} else {_ YesFreq[ 3]-=2; if(_ YesFreq[ 3]<0) _ YesFreq[ 3]=0;}
      if(_ Q105>LevelAnal) {if(_ YesFreq[ 4]<10) _ YesFreq[ 4]++;} else {_ YesFreq[ 4]-=2; if(_ YesFreq[ 4]<0) _ YesFreq[ 4]=0;}
      if(_ Q106>LevelAnal) {if(_ YesFreq[ 5]<10) _ YesFreq[ 5]++;} else {_ YesFreq[ 5]-=2; if(_ YesFreq[ 5]<0) _ YesFreq[ 5]=0;}
      if(_ Q107>LevelAnal) {if(_ YesFreq[ 6]<10) _ YesFreq[ 6]++;} else {_ YesFreq[ 6]-=2; if(_ YesFreq[ 6]<0) _ YesFreq[ 6]=0;}
      if(_ Q108>LevelAnal) {if(_ YesFreq[ 7]<10) _ YesFreq[ 7]++;} else {_ YesFreq[ 7]-=2; if(_ YesFreq[ 7]<0) _ YesFreq[ 7]=0;}
      if(_ Q109>LevelAnal) {if(_ YesFreq[ 8]<10) _ YesFreq[ 8]++;} else {_ YesFreq[ 8]-=2; if(_ YesFreq[ 8]<0) _ YesFreq[ 8]=0;}

      if(_ Q110>LevelAnal) {if(_ YesFreq[ 9]<10) _ YesFreq[ 9]++;} else {_ YesFreq[ 9]-=2; if(_ YesFreq[ 9]<0) _ YesFreq[ 9]=0;}
      if(_ Q111>LevelAnal) {if(_ YesFreq[10]<10) _ YesFreq[10]++;} else {_ YesFreq[10]-=2; if(_ YesFreq[10]<0) _ YesFreq[10]=0;}
//      if(_ Q112>LevelAnal) {if(_ YesFreq[11]<10) _ YesFreq[11]++;} else {_ YesFreq[11]-=2; if(_ YesFreq[11]<0) _ YesFreq[11]=0;}
//      if(_ Q113>LevelAnal) {if(_ YesFreq[12]<10) _ YesFreq[12]++;} else {_ YesFreq[12]-=2; if(_ YesFreq[12]<0) _ YesFreq[12]=0;}
//      if(_ Q114>LevelAnal) {if(_ YesFreq[13]<10) _ YesFreq[13]++;} else {_ YesFreq[13]-=2; if(_ YesFreq[13]<0) _ YesFreq[13]=0;}
      if(_ Q115>LevelAnal) {if(_ YesFreq[14]<10) _ YesFreq[14]++;} else {_ YesFreq[14]-=2; if(_ YesFreq[14]<0) _ YesFreq[14]=0;}
      if(_ Q116>LevelAnal) {if(_ YesFreq[15]<10) _ YesFreq[15]++;} else {_ YesFreq[15]-=2; if(_ YesFreq[15]<0) _ YesFreq[15]=0;}
      if(_ Q117>LevelAnal) {if(_ YesFreq[16]<10) _ YesFreq[16]++;} else {_ YesFreq[16]-=2; if(_ YesFreq[16]<0) _ YesFreq[16]=0;}
      if(_ Q118>LevelAnal) {if(_ YesFreq[17]<10) _ YesFreq[17]++;} else {_ YesFreq[17]-=2; if(_ YesFreq[17]<0) _ YesFreq[17]=0;}
      if(_ Q119>LevelAnal) {if(_ YesFreq[18]<10) _ YesFreq[18]++;} else {_ YesFreq[18]-=2; if(_ YesFreq[18]<0) _ YesFreq[18]=0;}

      if(_ Q120>LevelAnal) {if(_ YesFreq[19]<10) _ YesFreq[19]++;} else {_ YesFreq[19]-=2; if(_ YesFreq[19]<0) _ YesFreq[19]=0;}
      if(_ Q121>LevelAnal) {if(_ YesFreq[20]<10) _ YesFreq[20]++;} else {_ YesFreq[20]-=2; if(_ YesFreq[20]<0) _ YesFreq[20]=0;}
      if(_ Q122>LevelAnal) {if(_ YesFreq[21]<10) _ YesFreq[21]++;} else {_ YesFreq[21]-=2; if(_ YesFreq[21]<0) _ YesFreq[21]=0;}
      if(_ Q123>LevelAnal) {if(_ YesFreq[22]<10) _ YesFreq[22]++;} else {_ YesFreq[22]-=2; if(_ YesFreq[22]<0) _ YesFreq[22]=0;}
      if(_ Q124>LevelAnal) {if(_ YesFreq[23]<10) _ YesFreq[23]++;} else {_ YesFreq[23]-=2; if(_ YesFreq[23]<0) _ YesFreq[23]=0;}
      if(_ Q125>LevelAnal) {if(_ YesFreq[24]<10) _ YesFreq[24]++;} else {_ YesFreq[24]-=2; if(_ YesFreq[24]<0) _ YesFreq[24]=0;}
      if(_ Q126>LevelAnal) {if(_ YesFreq[25]<10) _ YesFreq[25]++;} else {_ YesFreq[25]-=2; if(_ YesFreq[25]<0) _ YesFreq[25]=0;}
      if(_ Q127>LevelAnal) {if(_ YesFreq[26]<10) _ YesFreq[26]++;} else {_ YesFreq[26]-=2; if(_ YesFreq[26]<0) _ YesFreq[26]=0;}
      if(_ Q128>LevelAnal) {if(_ YesFreq[27]<10) _ YesFreq[27]++;} else {_ YesFreq[27]-=2; if(_ YesFreq[27]<0) _ YesFreq[27]=0;}
      if(_ Q129>LevelAnal) {if(_ YesFreq[28]<10) _ YesFreq[28]++;} else {_ YesFreq[28]-=2; if(_ YesFreq[28]<0) _ YesFreq[28]=0;}

      if(_ Q130>LevelAnal) {if(_ YesFreq[29]<10) _ YesFreq[29]++;} else {_ YesFreq[29]-=2; if(_ YesFreq[29]<0) _ YesFreq[29]=0;}
      if(_ Q131>LevelAnal) {if(_ YesFreq[30]<10) _ YesFreq[30]++;} else {_ YesFreq[30]-=2; if(_ YesFreq[30]<0) _ YesFreq[30]=0;}
      if(_ Q132>LevelAnal) {if(_ YesFreq[31]<10) _ YesFreq[31]++;} else {_ YesFreq[31]-=2; if(_ YesFreq[31]<0) _ YesFreq[31]=0;}
      if(_ Q133>LevelAnal) {if(_ YesFreq[32]<10) _ YesFreq[32]++;} else {_ YesFreq[32]-=2; if(_ YesFreq[32]<0) _ YesFreq[32]=0;}
      if(_ Q134>LevelAnal) {if(_ YesFreq[33]<10) _ YesFreq[33]++;} else {_ YesFreq[33]-=2; if(_ YesFreq[33]<0) _ YesFreq[33]=0;}
      if(_ Q135>LevelAnal) {if(_ YesFreq[34]<10) _ YesFreq[34]++;} else {_ YesFreq[34]-=2; if(_ YesFreq[34]<0) _ YesFreq[34]=0;}
      if(_ Q136>LevelAnal) {if(_ YesFreq[35]<10) _ YesFreq[35]++;} else {_ YesFreq[35]-=2; if(_ YesFreq[35]<0) _ YesFreq[35]=0;}
      if(_ Q137>LevelAnal) {if(_ YesFreq[36]<10) _ YesFreq[36]++;} else {_ YesFreq[36]-=2; if(_ YesFreq[36]<0) _ YesFreq[36]=0;}
      if(_ Q138>LevelAnal) {if(_ YesFreq[37]<10) _ YesFreq[37]++;} else {_ YesFreq[37]-=2; if(_ YesFreq[37]<0) _ YesFreq[37]=0;}
      if(_ Q139>LevelAnal) {if(_ YesFreq[38]<10) _ YesFreq[38]++;} else {_ YesFreq[38]-=2; if(_ YesFreq[38]<0) _ YesFreq[38]=0;}

      if(_ Q140>LevelAnal) {if(_ YesFreq[39]<10) _ YesFreq[39]++;} else {_ YesFreq[39]-=2; if(_ YesFreq[39]<0) _ YesFreq[39]=0;}
      if(_ Q141>LevelAnal) {if(_ YesFreq[40]<10) _ YesFreq[40]++;} else {_ YesFreq[40]-=2; if(_ YesFreq[40]<0) _ YesFreq[40]=0;}
      if(_ Q142>LevelAnal) {if(_ YesFreq[41]<10) _ YesFreq[41]++;} else {_ YesFreq[41]-=2; if(_ YesFreq[41]<0) _ YesFreq[41]=0;}
      if(_ Q143>LevelAnal) {if(_ YesFreq[42]<10) _ YesFreq[42]++;} else {_ YesFreq[42]-=2; if(_ YesFreq[42]<0) _ YesFreq[42]=0;}
      if(_ Q144>LevelAnal) {if(_ YesFreq[43]<10) _ YesFreq[43]++;} else {_ YesFreq[43]-=2; if(_ YesFreq[43]<0) _ YesFreq[43]=0;}
      if(_ Q145>LevelAnal) {if(_ YesFreq[44]<10) _ YesFreq[44]++;} else {_ YesFreq[44]-=2; if(_ YesFreq[44]<0) _ YesFreq[44]=0;}
      if(_ Q146>LevelAnal) {if(_ YesFreq[45]<10) _ YesFreq[45]++;} else {_ YesFreq[45]-=2; if(_ YesFreq[45]<0) _ YesFreq[45]=0;}
      if(_ Q147>LevelAnal) {if(_ YesFreq[46]<10) _ YesFreq[46]++;} else {_ YesFreq[46]-=2; if(_ YesFreq[46]<0) _ YesFreq[46]=0;}
      if(_ Q148>LevelAnal) {if(_ YesFreq[47]<10) _ YesFreq[47]++;} else {_ YesFreq[47]-=2; if(_ YesFreq[47]<0) _ YesFreq[47]=0;}
      if(_ Q149>LevelAnal) {if(_ YesFreq[48]<10) _ YesFreq[48]++;} else {_ YesFreq[48]-=2; if(_ YesFreq[48]<0) _ YesFreq[48]=0;}

      if(_ Q150>LevelAnal) {if(_ YesFreq[49]<10) _ YesFreq[49]++;} else {_ YesFreq[49]-=2; if(_ YesFreq[49]<0) _ YesFreq[49]=0;}
      if(_ Q151>LevelAnal) {if(_ YesFreq[50]<10) _ YesFreq[50]++;} else {_ YesFreq[50]-=2; if(_ YesFreq[50]<0) _ YesFreq[50]=0;}
      if(_ Q152>LevelAnal) {if(_ YesFreq[51]<10) _ YesFreq[51]++;} else {_ YesFreq[51]-=2; if(_ YesFreq[51]<0) _ YesFreq[51]=0;}
      if(_ Q153>LevelAnal) {if(_ YesFreq[52]<10) _ YesFreq[52]++;} else {_ YesFreq[52]-=2; if(_ YesFreq[52]<0) _ YesFreq[52]=0;}
      if(_ Q154>LevelAnal) {if(_ YesFreq[53]<10) _ YesFreq[53]++;} else {_ YesFreq[53]-=2; if(_ YesFreq[53]<0) _ YesFreq[53]=0;}
      if(_ Q155>LevelAnal) {if(_ YesFreq[54]<10) _ YesFreq[54]++;} else {_ YesFreq[54]-=2; if(_ YesFreq[54]<0) _ YesFreq[54]=0;}
      if(_ Q156>LevelAnal) {if(_ YesFreq[55]<10) _ YesFreq[55]++;} else {_ YesFreq[55]-=2; if(_ YesFreq[55]<0) _ YesFreq[55]=0;}
      if(_ Q157>LevelAnal) {if(_ YesFreq[56]<10) _ YesFreq[56]++;} else {_ YesFreq[56]-=2; if(_ YesFreq[56]<0) _ YesFreq[56]=0;}
      if(_ Q158>LevelAnal) {if(_ YesFreq[57]<10) _ YesFreq[57]++;} else {_ YesFreq[57]-=2; if(_ YesFreq[57]<0) _ YesFreq[57]=0;}
      if(_ Q159>LevelAnal) {if(_ YesFreq[58]<10) _ YesFreq[58]++;} else {_ YesFreq[58]-=2; if(_ YesFreq[58]<0) _ YesFreq[58]=0;}

      if(_ middle < (min29dB * 800)) _ mult = 0; else	// ��� ��������������� - 29 ��
      _ mult = 409587500 / _ middle;	// _ mult = (Max mid = 25000 / N ����� = 200) * (32767 / _ middle);

      // ����������� ������� ��������� ��������
      Nab = 0;
      for(i=14;i<59;i++)		// ���������� ������� Fi
       if(_ YesFreq[i]>=8)		// ���������� ������� Fi ����� 800 ��
       { Nab=i-13; break; }		// ���� ������, ������� �� �����

      if(Nab)
      {
        j = Comb_TDC[(Nab-1)*2]; 	// ����� �������� ������� �� ��������� ���
       if(_ YesFreq[j]<8) j = Comb_TDC[(Nab-1)*2 + 1];  
       if(_ YesFreq[j]<8) Nab = 0;
       if(Nab)
       {
         _ YesFreq[i]-=3;
         _ YesFreq[j]-=3;
// !!!!!!!!!!!!!!!!!!!!!!!!1
		 if(_ chan >= LenHard)
		 {
	 	  MasOutAnalize[0]= 1;					// ��� ������
	 	  MasOutAnalize[1]= _ chan - LenHard ;	// ����� ������
		 }
		 else
		   if(_ chan >= LenSport1)
		   {
	 	     MasOutAnalize[0]= 0;					// ��� ������
	 	     MasOutAnalize[1]= _ chan - LenSport1 ;	// ����� ������
		   }
		 MasOutAnalize[2]= AnsTDC;				// ��� �������� - ���
 		 MasOutAnalize[3]= Nab;		 			// ����� ��������
	 	 MasOutAnalize[4]= j;						// �������� �������
		 MasOutAnalize[5]= 0;
    	 OutAbon(MasOutAnalize,6);
        }
      } 
      _ Q101=_ Q201=_ Q102=_ Q202=_ Q103=_ Q203=_ Q104=_ Q204=_ Q105=_ Q205=_ Q106=_ Q206=_ Q107=_ Q207=_ Q108=_ Q208=_ Q109=_ Q209=_ Q110=_ Q210=0;
      _ Q111=_ Q211=_ Q115=_ Q215=_ Q116=_ Q216=_ Q117=_ Q217=_ Q118=_ Q218=_ Q119=_ Q219=_ Q120=_ Q220=0;
      _ Q121=_ Q221=_ Q122=_ Q222=_ Q123=_ Q223=_ Q124=_ Q224=_ Q125=_ Q225=_ Q126=_ Q226=_ Q127=_ Q227=_ Q128=_ Q228=_ Q129=_ Q229=_ Q130=_ Q230=0;
      _ Q131=_ Q231=_ Q132=_ Q232=_ Q133=_ Q233=_ Q134=_ Q234=_ Q135=_ Q235=_ Q136=_ Q236=_ Q137=_ Q237=_ Q138=_ Q238=_ Q139=_ Q239=_ Q140=_ Q240=0;
      _ Q141=_ Q241=_ Q142=_ Q242=_ Q143=_ Q243=_ Q144=_ Q244=_ Q145=_ Q245=_ Q146=_ Q246=_ Q147=_ Q247=_ Q148=_ Q248=_ Q149=_ Q249=_ Q150=_ Q250=0;
      _ Q151=_ Q251=_ Q152=_ Q252=_ Q153=_ Q253=_ Q154=_ Q254=_ Q155=_ Q255=_ Q156=_ Q256=_ Q157=_ Q257=_ Q158=_ Q258=_ Q159=_ Q259=_ middle=0;

        _ count800 = 800; // ����������� ������� ������ ���� ������ 100 ��
      }
      AdrAnalTDC =_ pxNextChan;
  }
#undef _
}

void AnalizeTDCL(void)
{ // ��������� (��������) ��������� ������ ���
  unsigned char i,j,Nab,code,code1;
  xListAnalTDCL *AdrAnalTDCL;

#define _ AdrAnalTDCL->
	AdrAnalTDCL = ListAnalTDCL.pxHead;
    while (AdrAnalTDCL)
	{// ���������� ��� ������ �������
	  if(!_ count800)
	 { // ���������� ���������� �������
	   _ Q101 = sqrt(abs((float)_ Q101 * (float) _ Q101 + (float)_ Q201 * (float) _ Q201 -
       ((float)_ Q101 * (float)_ Q201 * (float)F1605) / Mult));
       
	  _ Q102 = sqrt(abs((float)_ Q102 * (float) _ Q102 + (float)_ Q202 * (float) _ Q202 -
       ((float)_ Q102 * (float)_ Q202 * (float)F317 ) / Mult));
	  _ Q103 = sqrt(abs((float)_ Q103 * (float) _ Q103 + (float)_ Q203 * (float) _ Q203 -
       ((float)_ Q103 * (float)_ Q203 * (float)F430 ) / Mult));
	  _ Q104 = sqrt(abs((float)_ Q104 * (float) _ Q104 + (float)_ Q204 * (float) _ Q204 -
       ((float)_ Q104 * (float)_ Q204 * (float)F588 ) / Mult));
	  _ Q105 = sqrt(abs((float)_ Q105 * (float) _ Q105 + (float)_ Q205 * (float) _ Q205 -
       ((float)_ Q105 * (float)_ Q205 * (float)F791 ) / Mult));
	  _ Q106 = sqrt(abs((float)_ Q106 * (float) _ Q106 + (float)_ Q206 * (float) _ Q206 -
       ((float)_ Q106 * (float)_ Q206 * (float)F882 ) / Mult));
	  _ Q107 = sqrt(abs((float)_ Q107 * (float) _ Q107 + (float)_ Q207 * (float) _ Q207 -
       ((float)_ Q107 * (float)_ Q207 * (float)F1085) / Mult));
	  _ Q108 = sqrt(abs((float)_ Q108 * (float) _ Q108 + (float)_ Q208 * (float) _ Q208 -
       ((float)_ Q108 * (float)_ Q208 * (float)F1221) / Mult));
	  _ Q109 = sqrt(abs((float)_ Q109 * (float) _ Q109 + (float)_ Q209 * (float) _ Q209 -
       ((float)_ Q109 * (float)_ Q209 * (float)F1469) / Mult));
	  _ Q110 = sqrt(abs((float)_ Q110 * (float) _ Q110 + (float)_ Q210 * (float) _ Q210 -
       ((float)_ Q110 * (float)_ Q210 * (float)F1989) / Mult));
	  _ Q111 = sqrt(abs((float)_ Q111 * (float) _ Q111 + (float)_ Q211 * (float) _ Q211 -
       ((float)_ Q111 * (float)_ Q211 * (float)F2713) / Mult));

      if(_ Q101>LevelAnal) {if(_ YesFreq[ 0]<10) _ YesFreq[ 0]++; if(_ YesFreq[0]>=8) _ YesFreq[0]=30;} else {_ YesFreq[0]-=2; 
       if(_ YesFreq[0]<0) _ YesFreq[0]=0;}
      if(_ Q102>LevelAnal) {if(_ YesFreq[ 1]<10) _ YesFreq[ 1]++;} else {_ YesFreq[ 1]-=2; if(_ YesFreq[ 1]<0) _ YesFreq[ 1]=0;}
      if(_ Q103>LevelAnal) {if(_ YesFreq[ 2]<10) _ YesFreq[ 2]++;} else {_ YesFreq[ 2]-=2; if(_ YesFreq[ 2]<0) _ YesFreq[ 2]=0;}
      if(_ Q104>LevelAnal) {if(_ YesFreq[ 3]<10) _ YesFreq[ 3]++;} else {_ YesFreq[ 3]-=2; if(_ YesFreq[ 3]<0) _ YesFreq[ 3]=0;}
      if(_ Q105>LevelAnal) {if(_ YesFreq[ 4]<10) _ YesFreq[ 4]++;} else {_ YesFreq[ 4]-=2; if(_ YesFreq[ 4]<0) _ YesFreq[ 4]=0;}
      if(_ Q106>LevelAnal) {if(_ YesFreq[ 5]<10) _ YesFreq[ 5]++;} else {_ YesFreq[ 5]-=2; if(_ YesFreq[ 5]<0) _ YesFreq[ 5]=0;}
      if(_ Q107>LevelAnal) {if(_ YesFreq[ 6]<10) _ YesFreq[ 6]++;} else {_ YesFreq[ 6]-=2; if(_ YesFreq[ 6]<0) _ YesFreq[ 6]=0;}
      if(_ Q108>LevelAnal) {if(_ YesFreq[ 7]<10) _ YesFreq[ 7]++;} else {_ YesFreq[ 7]-=2; if(_ YesFreq[ 7]<0) _ YesFreq[ 7]=0;}
      if(_ Q109>LevelAnal) {if(_ YesFreq[ 8]<10) _ YesFreq[ 8]++;} else {_ YesFreq[ 8]-=2; if(_ YesFreq[ 8]<0) _ YesFreq[ 8]=0;}
      if(_ Q110>LevelAnal) {if(_ YesFreq[ 9]<10) _ YesFreq[ 9]++;} else {_ YesFreq[ 9]-=2; if(_ YesFreq[ 9]<0) _ YesFreq[ 9]=0;}
      if(_ Q111>LevelAnal) {if(_ YesFreq[10]<10) _ YesFreq[10]++;} else {_ YesFreq[10]-=2; if(_ YesFreq[10]<0) _ YesFreq[10]=0;}

      if(_ middle < (min29dB * 800)) _ mult = 0; else	// ��� ��������������� - 29 ��
      _ mult = 409587500 / _ middle;	// _ mult = (Max mid = 25000 / N ����� = 200) * (32767 / _ middle);

      Nab = 0;	// ����������� ������� ��������� ��������
      j = code = 0; code1 = 9;	// ����� ���������
      for (i=1;i<10;i++)
      { // ����� 1-� �������
        if(_ YesFreq[i]>=8)
        { j=i; break; }
        code+=code1; code1--;
      }
      if(j)
        for (i=j+1;i<=10;i++)
        { // ����� 2-� �������
          if(_ YesFreq[i]>=8)
          {
            Nab = code + i - j;
            _ YesFreq[i]=0;
            _ YesFreq[j]=0;
            break;
          }
        }
      if(Nab)
      { // ����� ��������� ��� ��� �������� �����
        if(_ YesFreq[0]>=8) // �������� �����?
	    { Nab += 200; _ YesFreq[0] = 0; }
      }
      else
        if((_ YesFreq[0]>=8) && (_ YesFreq[1]>=8))
        { // �������� �� ����������� �����
          Nab = 46;
          _ YesFreq[0] = _ YesFreq[1] = 0;
        }
      if(Nab)
      {
// !!!!!!!!!!!!!!!!!!!!!!!!1
		 if(_ chan >= LenHard)
		 {
	 	  MasOutAnalize[0]= 1;					// ��� ������
	 	  MasOutAnalize[1]= _ chan - LenHard ;	// ����� ������
		 }
		 else
		   if(_ chan >= LenSport1)
		   {
	 	     MasOutAnalize[0]= 0;					// ��� ������
	 	     MasOutAnalize[1]= _ chan-LenSport1;	// ����� ������
		   }
		 MasOutAnalize[2]= ImitTDC;				// ��� �������� - �������� ���
 		 MasOutAnalize[3]= Nab;		 			// ����� ��������
	 	 MasOutAnalize[4]=j;
		 MasOutAnalize[5]=i;
    	 OutAbon(MasOutAnalize,6);
      }
      
      _ Q101=_ Q201=_ Q102=_ Q202=_ Q103=_ Q203=_ Q104=_ Q204=_ Q105=_ Q205=_ Q106=_ Q206=_ Q107=_ Q207=_ Q108=_ Q208=_ Q109=_ Q209=_ Q110=_ Q210=
      _ Q111=_ Q211 = _ middle=0;

      _ count800 = 800; // ����������� ������� ������ ���� ������ 100 ��
	 }
     AdrAnalTDCL =_ pxNextChan;
    }
#undef _
}

//--------------------------------------------------------------------------//
//  �������� ������� � �����������								            //
//--------------------------------------------------------------------------//
#define d_CalcFK_Koef   (2.*3.141592654/8000.)
signed short CalcFK(signed short F)
{ return (2 * (Mult-1) *  cosf(d_CalcFK_Koef*F))+0.5; }

//--------------------------------------------------------------------------//
//  �������� ������������ � �������								            //
//--------------------------------------------------------------------------//
#define d_CalcKoef_FK   (8000./(2*3.141592654))
signed short CalcKF(signed short K)
{ return d_CalcKoef_FK * acosf(K/(2.*(Mult-1)))+0.5; }

//--------------------------------------------------------------------------//
// ������ ������� �������������� ������� �� ������							//
//--------------------------------------------------------------------------//
/*unsigned char ReadAnal(unsigned char chan)
{
  unsigned short count, data, data1;
  unsigned char index, i;
  xListAnalItem *AdrAnal;
  
  count = 0;
  index = 1;
#define _ AdrAnal->
	AdrAnal=ListAnal.pxHead; 
	while(AdrAnal)
	{ // ������� ������ ��� ��������
	  if(_ chan == chan) 
	  {
	    count++;
	    if(_ Type) i = AnsSeries;
	    else i= AnsParallel;
	    MasOutAnalize[index++] = i; 
	    MasOutAnalize[index++] = _ NAbon;
	    data = CalcKF(_ F1);
	    MasOutAnalize[index++] = data;
	    MasOutAnalize[index++] = data >> 8;
	    data = CalcKF(_ F2);
	    MasOutAnalize[index++] = data;
	    MasOutAnalize[index++] = data >> 8;
	    data = 8000 / _ LdF;
	    MasOutAnalize[index++] = data;
	    data1 = _ LTimA * 1250 / data;
	    MasOutAnalize[index++] = data1;
	    MasOutAnalize[index++] = data1 >> 8;
	    if(i==AnsSeries)
	    {
	      data1 = _ Type  * 1250 / data;
	      MasOutAnalize[index++] = data1;
	      MasOutAnalize[index++] = data1 >> 8;
	    }
	    if(count==23) break;
	  }
	  AdrAnal = _ pxNextChan;
	}
#undef _
   return count;
}
*/

//--------------------------------------------------------------------------//
// ������ ������� ������� ���� �� ������									//
//--------------------------------------------------------------------------//
unsigned char ReadAnalDTMF(unsigned char chan)
{
  xListAnalDTMF *AdrAnalDTMF=ListAnalDTMF.pxHead;

#define _ AdrAnalDTMF ->
	while(AdrAnalDTMF)
	{ // ���������� ��� ������ �������
	  if(_ chan == chan) return 1; 
	  AdrAnalDTMF = _ pxNextChan;
	}
#undef _
  return 0;
}

//--------------------------------------------------------------------------//
// ������ ������� ������� ���� �� ������									//
//--------------------------------------------------------------------------//
unsigned char ReadAnalPPSR(unsigned char chan)
{
  xListAnalPPSR *AdrAnalPPSR=ListAnalPPSR.pxHead;

#define _ AdrAnalPPSR ->
	while(AdrAnalPPSR)
	{ // ���������� ��� ������ �������
	  if(_ chan == chan) return 1; 
	  AdrAnalPPSR = _ pxNextChan;
	}
#undef _
  return 0;
}

//--------------------------------------------------------------------------//
// ������ ������� ������� ������� PCDT �� ������							//
//--------------------------------------------------------------------------//
unsigned char ReadAnalPCDT(unsigned char chan)
{
  xListAnalPCDT *AdrAnalPCDT=ListAnalPCDT.pxHead;

#define _ AdrAnalPCDT ->
	while (AdrAnalPCDT)
	{ // ���������� ��� ������ �������
	  if(_ chan == chan) return 1; 
	  AdrAnalPCDT = _ pxNextChan;
	}
#undef _
  return 0;
}

//--------------------------------------------------------------------------//
// ������ ������� 2-� ��������� �������� �� ������� � ������ ���������� 	//
//--------------------------------------------------------------------------//
unsigned char InitAnalMU(unsigned char chan, unsigned char NAbon,
                         short F1, short F2,
                         unsigned short dF, unsigned short TimA, unsigned short Type, unsigned char Nspec)
{
  xListAnalItem *AdrInItem, *AdrOutItem, *AdrItem;

  if(!(AdrOutItem = pvPortMalloc(sizeof (xListAnalItem))))
  { ErrMallocAnal++; return false; }
  else
  {
#define _ AdrOutItem ->
	   _ chan = chan; // ������������� ���������� ������ ��������
	 _ NAbon = NAbon; // ����� � ����� ��������
	 _ F1 = CalcFK(F1);
	 if(F2) _ F2 = CalcFK(F2); // �������� ������� � �����������
	 else _ F2 = 0;
	 _ LTimA = TimA / (1250 / dF) ; // ������� (�����) ���������� 1-� ������� (0.8 �� ��������� �������)
	 _ Type  = Type / (1250 / dF) ; // ������� (�����) ���������� 2-� ������� (0.8 �� ��������� �������)
	 _ TimA = 0;
	 _ LdF = 8000 / dF; // ������ ����������� (����� �������� �� 8 ���)
	 _ dF  = 0;
	 _ Q11 = 0;
	 _ Q21 = 0;
	 _ Q12 = 0;
	 _ Q22 = 0;
	 _ middle = 0;
	 _ pxPrevious = 0;
	 _ pxNextChan = 0;
	 _ Nspec = Nspec;
	 if(_ Type) _ NF = 1;
	 else _ NF = 0;
#undef _
	 if(ListAnal.pxHead)
	 { // �� ������ �������
	   AdrInItem=ListAnal.pxHead;
	   do
	   {
	     AdrItem = AdrInItem;
	     AdrInItem = AdrInItem -> pxNextChan;
	   }
	   while(AdrInItem);
	   AdrItem -> pxNextChan = AdrOutItem;
	   AdrOutItem -> pxPrevious = AdrItem;
	 }
	 else ListAnal.pxHead = AdrOutItem;
  }
  return true;
}

//--------------------------------------------------------------------------//
// ������ ������� �������� DTMF �� ������� � ������ ����������				//
//--------------------------------------------------------------------------//
unsigned char InitAnalDTMF(unsigned char chan)
{
  xListAnalDTMF *AdrInDTMF, *AdrOutDTMF, *AdrDTMF;
  if(ReadAnalDTMF(chan)) return 0xFF; // ������ ��� ����������
  if(!(AdrOutDTMF = pvPortMalloc(sizeof (xListAnalDTMF))))
  { ErrMallocAnal++; return false; }
  else
  {
#define _ AdrOutDTMF ->
    _ chan = chan;
    _ Q11 = _ Q21 = _ Q12 = _ Q22 = _ Q13 = _ Q23 = _ Q14 = _ Q24 =
    _ Q15 = _ Q25 = _ Q16 = _ Q26 = _ Q17 = _ Q27 = _ Q18 = _ Q28 = _ middle = 0; 
    _ pxPrevious = _ pxNextChan = 0;
    _ count = 200;
    _ half = 1;
#undef _
	 if(ListAnalDTMF.pxHead)
	 {
	   AdrInDTMF = ListAnalDTMF.pxHead;
	  do
	  {
	    AdrDTMF = AdrInDTMF;
	    AdrInDTMF = AdrInDTMF -> pxNextChan;
	  }
	  while (AdrInDTMF);
	  AdrDTMF -> pxNextChan = AdrOutDTMF;
	  AdrOutDTMF -> pxPrevious = AdrDTMF;
	 }
	 else ListAnalDTMF.pxHead = AdrOutDTMF;
  }
  return true;
}

//--------------------------------------------------------------------------//
// ������ ������� �������� ���� �� ������� � ������ ����������				//
//--------------------------------------------------------------------------//
unsigned char InitAnalPPSR(unsigned char chan)
{
  xListAnalPPSR *AdrInPPSR, *AdrOutPPSR, *AdrPPSR;

  if(ReadAnalPPSR(chan)) return 0xFF; // ������ ��� ����������
  if(!(AdrOutPPSR = pvPortMalloc(sizeof (xListAnalPPSR))))
  { ErrMallocAnal++; return false; }
  else
  {
#define _ AdrOutPPSR ->
    _ chan = chan;
    _ Q11 = _ Q21 = _ Q12 = _ Q22 = _ Q13 = _ Q23 = _ Q14 = _ Q24 =
    _ Q18 = _ Q28 = _ middle = 0; 
    _ pxPrevious = _ pxNextChan = 0;
    _ count = Count_PPSR;
#undef _
	if(ListAnalPPSR.pxHead)
	{
	  AdrInPPSR = ListAnalPPSR.pxHead;
	  do
	  {
	    AdrPPSR = AdrInPPSR;
	    AdrInPPSR = AdrInPPSR -> pxNextChan;
	  }
	  while (AdrInPPSR);
	  AdrPPSR -> pxNextChan = AdrOutPPSR;
	  AdrOutPPSR -> pxPrevious = AdrPPSR;
	}
	else ListAnalPPSR.pxHead = AdrOutPPSR;
  }
  return true;
}

//--------------------------------------------------------------------------//
// ������ ������� ������� �������� PCDT �� ������� � ������ ����������  	//
//--------------------------------------------------------------------------//
unsigned char InitAnalPCDT(unsigned char chan)
{
  xListAnalPCDT *AdrInPCDT, *AdrOutPCDT, *AdrPCDT;

  if(ReadAnalPCDT(chan)) return 0xFF;
  if(!(AdrOutPCDT = pvPortMalloc(sizeof(xListAnalPCDT))))
  { ErrMallocAnal++; return false; }
  else
  {
#define _ AdrOutPCDT ->
    _ chan = chan;
    _ Q101 = _ Q201 = _ Q102 = _ Q202 = _ Q103 = _ Q203 = _ Q104 = _ Q204 = _ Q105 = _ Q205 = 
    _ Q106 = _ Q206 = _ Q107 = _ Q207 = _ Q108 = _ Q208 = _ Q109 = _ Q209 = _ Q110 = _ Q210 =
    _ Q111 = _ Q211 = _ Q112 = _ Q212 = _ middle = 0;
    _ pxPrevious = _ pxNextChan = 0;
    _ count800 = 800; 
#undef _
	if(ListAnalPCDT.pxHead)
	{
	  AdrInPCDT = ListAnalPCDT.pxHead;
	  do
	  {
	    AdrPCDT = AdrInPCDT;
	    AdrInPCDT = AdrInPCDT -> pxNextChan;
	  }
	  while(AdrInPCDT);
	  AdrPCDT -> pxNextChan = AdrOutPCDT;
	  AdrOutPCDT -> pxPrevious = AdrPCDT;
	}
	else ListAnalPCDT.pxHead = AdrOutPCDT;
  }
  return true;
}

//--------------------------------------------------------------------------//
// ����������� ������� ������� TDC �� ��������� ������  					//
//--------------------------------------------------------------------------//
unsigned char ExitAnalTDC(unsigned char chan)
{
  xListAnalTDCF  *AdrPrev, *Adr, *AdrNext;

  Adr = ListAnalTDCF.pxHead;
  while(Adr)
  { // ���� ������ �� ��������� ������
    if(Adr->chan == chan)
    { // ����� �����
      if(Adr->pxPrevious)
      { // �� ������ ������
        AdrPrev = Adr->pxPrevious;
        AdrNext = Adr->pxNextChan;
        AdrPrev->pxNextChan = AdrNext;
        if(AdrNext) AdrNext->pxPrevious = AdrPrev;
      }
      else
      { // ������ ������
        AdrNext=Adr->pxNextChan;
        if(AdrNext) AdrNext->pxPrevious = 0;
        ListAnalTDCF.pxHead=AdrNext;
      }
      vPortFree(Adr);
      return true;
    } 
    Adr=Adr->pxNextChan;
  }
  return false;
}

//--------------------------------------------------------------------------//
// ����������� ���������� ������� TDC �� ��������� ������				//
//--------------------------------------------------------------------------//
unsigned char ExitAnalTDCL(unsigned char chan)
{
  xListAnalTDCL *AdrPrev,*Adr,*AdrNext;

  Adr = ListAnalTDCL.pxHead;
  while(Adr)
  { // ���� ������ �� ��������� ������
    if(Adr->chan == chan)
    { // ����� �����
      if(Adr->pxPrevious)
      { // �� ������ ������
        AdrPrev = Adr->pxPrevious;
        AdrNext = Adr->pxNextChan;
        AdrPrev->pxNextChan = AdrNext;
        if(AdrNext) AdrNext->pxPrevious = AdrPrev;
      }
      else
      { // ������ ������
        AdrNext=Adr->pxNextChan;
        if(AdrNext) AdrNext->pxPrevious = 0;
        ListAnalTDCL.pxHead=AdrNext;
      }
      vPortFree(Adr);
      return true;
    } 
    Adr=Adr->pxNextChan;
  }
  return false;
}

//--------------------------------------------------------------------------//
// ����������� ������� DTMF �� ��������� ������ 							//
//--------------------------------------------------------------------------//
unsigned char ExitAnalDTMF(unsigned char chan)
{
  xListAnalDTMF  *AdrPrev, *Adr, *AdrNext;

  Adr = ListAnalDTMF.pxHead;
  while(Adr)
  { // ���� ������ �� ��������� ������
    if(Adr->chan == chan)
    { // ����� �����
      if(Adr->pxPrevious)
      { // �� ������ ������
        AdrPrev = Adr->pxPrevious;
        AdrNext = Adr->pxNextChan;
        AdrPrev->pxNextChan = AdrNext;
        if(AdrNext) AdrNext->pxPrevious = AdrPrev;
      }
      else
      { // ������ ������
        AdrNext=Adr->pxNextChan;
        if(AdrNext) AdrNext->pxPrevious = 0;
        ListAnalDTMF.pxHead=AdrNext;
      }
      vPortFree(Adr);
      return true;
    } 
    Adr=Adr->pxNextChan;
  }
  return false;
}

//--------------------------------------------------------------------------//
// ����������� ������� ���� �� ��������� ������ 							//
//--------------------------------------------------------------------------//
unsigned char ExitAnalPPSR(unsigned char chan)
{
  xListAnalPPSR *AdrPrev, *Adr, *AdrNext;

  Adr = ListAnalPPSR.pxHead;
  while(Adr)
  { // ���� ������ �� ��������� ������
    if(Adr->chan == chan)
    { // ����� �����
      if(Adr->pxPrevious)
      { // �� ������ ������
        AdrPrev = Adr->pxPrevious;
        AdrNext = Adr->pxNextChan;
        AdrPrev->pxNextChan = AdrNext;
        if(AdrNext) AdrNext->pxPrevious = AdrPrev;
      }
      else
      { // ������ ������
        AdrNext=Adr->pxNextChan;
        if(AdrNext) AdrNext->pxPrevious = 0;
        ListAnalPPSR.pxHead=AdrNext;
      }
      vPortFree(Adr);
      return true;
    } 
    Adr=Adr->pxNextChan;
  }
  return false;
}

//--------------------------------------------------------------------------//
//  ����������� ������� ������� ���� �� ��������� ������    				//
//--------------------------------------------------------------------------//
unsigned char ExitAnalPCDT(unsigned char chan)
{
  xListAnalPCDT *AdrPrev, *Adr, *AdrNext;

  Adr = ListAnalPCDT.pxHead;
  while(Adr)
  { // ���� ������ �� ��������� ������
    if(Adr->chan == chan)
    { // ����� �����
      if(Adr->pxPrevious)
      { // �� ������ ������
        AdrPrev = Adr->pxPrevious;
        AdrNext = Adr->pxNextChan;
        AdrPrev->pxNextChan = AdrNext;
        if(AdrNext) AdrNext->pxPrevious = AdrPrev;
      }
      else
      { // ������ ������
        AdrNext=Adr->pxNextChan;
        if(AdrNext) AdrNext->pxPrevious = 0;
        ListAnalPCDT.pxHead=AdrNext;
      }
      vPortFree(Adr);
      return true;
    } 
    Adr=Adr->pxNextChan;
  }
  return false;
}

//--------------------------------------------------------------------------//
// ����������� ������� ������� �� ��������� ������ � ��������				//
//--------------------------------------------------------------------------//
unsigned char ExitAnal(unsigned char chan, unsigned char Nabon)
{
  xListAnalItem  *AdrInItem, *AdrItem, *AdrOutItem;

  AdrItem = ListAnal.pxHead;
  while(AdrItem)
  { // ���� ������ �� ��������� ������ � ��������
    if(AdrItem->chan == chan)
    {
      if((Nabon==0) || (AdrItem ->NAbon==Nabon)) // ��� ������ ��������, ���� ��� ����� == 0
      { // ����� � �������� �����
        if(AdrItem->pxPrevious)
        { // �� ������
          AdrInItem=AdrItem->pxPrevious;
          AdrOutItem=AdrItem->pxNextChan;
          AdrInItem->pxNextChan=AdrOutItem;
          if(AdrOutItem) AdrOutItem->pxPrevious=AdrInItem;
        }
        else
        {
          AdrOutItem = AdrItem->pxNextChan;
          if(AdrOutItem) AdrOutItem->pxPrevious = 0;
          ListAnal.pxHead=AdrOutItem;
        }
        vPortFree(AdrItem);
        return true;
      }
    }
    AdrItem=AdrItem -> pxNextChan;
  }
  return false;
}

void Analize_PPSR_RSDT_DTMF_ETC(void)
{
  unsigned char i,j,Nab,code;
  unsigned int ii,ij;

  { // --------------------------- 2-x ��������� ������ --------------------------------
    xListAnalItem *AdrAnal;
#define _ AdrAnal ->
	AdrAnal = ListAnal.pxHead;
    while(AdrAnal)
	{ // ���������� ��� ������ �������
	    if ((_ dF < (_ LdF / 2)) && _ half)			// �������� �� ����������� �������� �� �������� ����� ��������
       // ���������� ���������
        {
            if (_ middle < (min29dB * _ LdF)) _ mult = 0;	// ��� ��������������� - 29 ��
            else _ mult = 409587500 / _ middle;	// _ mult = (Max_mid = 25000 * 32767) / _ middle);
            _ middle = _ half = 0;
        }
	    if (!_ dF) 			// ���������� ���������� ������� ?
        {
            _ Q11 = sqrt(abs((float)_ Q11 * (float) _ Q11 + (float)_ Q21 * (float) _ Q21 -
                ((float)_ Q11 * (float)_ Q21 * (float)_ F1) / Mult));
            _ Q12 = sqrt(abs((float)_ Q12 * (float) _ Q12 + (float)_ Q22 * (float) _ Q22 -
                ((float)_ Q12 * (float)_ Q22 * (float)_ F2) / Mult));
            if (_ middle < (min29dB * _ LdF)) _ mult = 0;	// ��� ��������������� - 29 ��
            else _ mult = 819175000 / _ middle;	// _ mult = (Max_mid = 25000 * 32767) / _ middle);
            _ middle = _ half = 0;
            if (!_ Type)	// 2-� ��������� ������ ?
            {
                if ((_ Q11 > LevelAnal) && (_ Q12 > LevelAnal))	// ��  
                {
                    if (_ TimA) _ TimA--;
                    else 
                    {// ���� ������� � ������������� �������� 
		                 _ TimA = _ LTimA; _ half = 1;
                		 if (_ chan >= LenHard) 
                		 {
	 	                     MasOutAnalize[0]= 1;				 // ��� ������
	 	                     MasOutAnalize[1]= _ chan - LenHard;  // ����� ������
		                  } 
		                  else 
		                  if (_ chan >= LenSport1) 
		                  {
	 	                     MasOutAnalize[0]= 0;				 // ��� ������
	 	                     MasOutAnalize[1]= _ chan - LenSport1;// ����� ������
		                  }
		                  MasOutAnalize[2]=_ Nspec - 1 + SpecAnal;// ��� �������� ������������ ������������ 2 �������
 		                  MasOutAnalize[3]=_ NAbon;	 			 // ����� ��������
	 	                  MasOutAnalize[4]=0;
		                  MasOutAnalize[5]=0;
    	                  OutAbon(MasOutAnalize, 6);
		            }
                   _ Q11 = _ Q21 = _ Q12 = _ Q22 = _ middle = 0; _ dF = _ LdF;
                }
                else 
                {   	// �������������� �������� ���� ��� ������
                    _ TimA+=2; 
                    if (_ TimA > _ LTimA) _ TimA = _ LTimA;
                }
            } 
            else 
            {   // ������� � ���������������� �������� ������
                if (_ NF == 1)	// 1-� �������
                {
       	            if ((_ Q11 > (LevelAnal*2)) && (_ Q12 < (LevelAnal*2)))
                    {
                        if (_ TimA) _ TimA--;	// ���������� ������
                        else 
                        {
                            _ NF++; _ TimA = _ Type;	// ������ ������� ����������, �������� ������������� 2-�
                        }
                    } 
                    else 
                    {	// �������������� �������� ���� ��� ������
                        _ TimA+=2; 
                        if (_ TimA > _ LTimA) _ TimA = _ LTimA;
                    }
                } 
                else 
                if (_ NF == 2)		// 2-� �������
                {
                    if ((_ Q12 > LevelAnal) && (_ Q11 < LevelAnal))
                    {
                        if (_ TimA) 
                            _ TimA--;
                        else 
                        { // ���� �������
                             _ TimA = _ LTimA; _ NF = _ half = 1;
                    		 if (_ chan >= LenHard) 
		                     {
	 	                         MasOutAnalize[0]= 1;				 // ��� ������
	 	                         MasOutAnalize[1]= _ chan - LenHard;	 // ����� ������
		                     } 
		                     else 
		                     if (_ chan >= LenSport1) 
		                     {
	 	                         MasOutAnalize[0]= 0;				 // ��� ������
	 	                         MasOutAnalize[1]= _ chan - LenSport1;// ����� ������
		                     }
		                     MasOutAnalize[2]=_ Nspec - 1 + SpecAnal; // ��� �������� ������������ ��������������� 2 �������
 		                     MasOutAnalize[3]= _ NAbon; 				 // ����� ��������
	 	                     MasOutAnalize[4]=0;
		                     MasOutAnalize[5]=0;
    	                     OutAbon(MasOutAnalize, 6);
                        }
                    } 
                    else 
                    {
                        _ TimA+=1; 
                        if (_ TimA > _ Type + 8) // 8 - ��� �������� 8 * 5 = + 40��
                        {// ��� 2-� �������
                            _ TimA = _ LTimA; 
                            _ NF = _ half = 1;
                        } 
                    }
                }
            }
            _ Q11 = _ Q21 = _ Q12 = _ Q22 = _ middle = 0; _ dF = _ LdF;
        }
    	AdrAnal = _ pxNextChan;
	}
#undef _
  }


  { // --------------------------- ��������� ������ DTMF --------------------------------
    xListAnalDTMF *AdrAnalDTMF;
#define _ AdrAnalDTMF ->
	AdrAnalDTMF =ListAnalDTMF.pxHead;
	while (AdrAnalDTMF)
	{ // ���������� ��� ������ �������
	  if (_ timer < 65535) _ timer += 5;				// ���� ������� �� ���������� �����
	 if ((_ half && (_ count  < (Count_DTMF - (Count_DTMF / 4))) && (_ middle < (min29dB * (Count_DTMF / 4)))))  {		// �������� �� ������� ������� ����� 5 ��
	  _ Q11 = _ Q21 = _ Q12 = _ Q22 = _ Q13 = _ Q23 = _ Q14 = _ Q24 =	// �� ����������� ���� ������ ������
	  _ Q15 = _ Q25 = _ Q16 = _ Q26 = _ Q17 = _ Q27 = _ Q18 = _ Q28 = _ middle = 0;
 	  _ count = Count_DTMF; _ half = 1;} 
	 else {if ((_ count < Count_DTMF / 2) && _ half)					// �������� �� ���������� �������� �� �������� ����� ��������
	 // ���������� ��������� �� ������ ��������� - 12.5 mc
      {ii = (((100 * Max_mid)/Count_DTMF)<<15) / _ middle; _ half = 0;
       ij = cli(); _ mult = ii; sti(ij);}
	 else if (!_ count) 			// ���������� ���������� ������� ?
	 {_ Q11 = sqrt(abs((float)_ Q11 * (float) _ Q11 + (float)_ Q21 * (float) _ Q21 -
       ((float)_ Q11 * (float)_ Q21 * (float)27978) / Mult));
	  _ Q12 = sqrt(abs((float)_ Q12 * (float) _ Q12 + (float)_ Q22 * (float) _ Q22 -
       ((float)_ Q12 * (float)_ Q22 * (float)26955) / Mult));
	  _ Q13 = sqrt(abs((float)_ Q13 * (float) _ Q13 + (float)_ Q23 * (float) _ Q23 -
       ((float)_ Q13 * (float)_ Q23 * (float)25700) / Mult));
	  _ Q14 = sqrt(abs((float)_ Q14 * (float) _ Q14 + (float)_ Q24 * (float) _ Q24 -
       ((float)_ Q14 * (float)_ Q24 * (float)24217) / Mult));
	  _ Q15 = sqrt(abs((float)_ Q15 * (float) _ Q15 + (float)_ Q25 * (float) _ Q25 -
       ((float)_ Q15 * (float)_ Q25 * (float)19072) / Mult));
	  _ Q16 = sqrt(abs((float)_ Q16 * (float) _ Q16 + (float)_ Q26 * (float) _ Q26 -
       ((float)_ Q16 * (float)_ Q26 * (float)16324) / Mult));
	  _ Q17 = sqrt(abs((float)_ Q17 * (float) _ Q17 + (float)_ Q27 * (float) _ Q27 -
       ((float)_ Q17 * (float)_ Q27 * (float)13083) / Mult));
	  _ Q18 = sqrt(abs((float)_ Q18 * (float) _ Q18 + (float)_ Q28 * (float) _ Q28 -
       ((float)_ Q18 * (float)_ Q28 * (float) 9314) / Mult));
	   // ������ ��������� ����� ������ �������� ��������� ����� 25 ��
      _ mult = (((100 * Max_mid)/Count_DTMF)<<15) / _ middle;
// ---------------------- ����������� ���� ����
	  code = 0;
      if ((_ Q11 > LevelAnalDTMF) && ((_ Q12 + _ Q13 + _ Q14) < LevelAnalDTMF)) code = 0x01;	// ����� 1-� �������
      else if ((_ Q12 > LevelAnalDTMF) && ((_ Q11 + _ Q13 + _ Q14) < LevelAnalDTMF)) code = 0x02;
      else if ((_ Q13 > LevelAnalDTMF) && ((_ Q11 + _ Q12 + _ Q14) < LevelAnalDTMF)) code = 0x03;
      else if ((_ Q14 > LevelAnalDTMF) && ((_ Q11 + _ Q12 + _ Q13) < LevelAnalDTMF)) code = 0x04;
      if (code)
      if ((_ Q15 > LevelAnalDTMF) && ((_ Q16 + _ Q17 + _ Q18) < LevelAnalDTMF)) code|= 0x10;	// ����� 2� �������
      else if ((_ Q16 > LevelAnalDTMF) && ((_ Q15 + _ Q17 + _ Q18) < LevelAnalDTMF)) code|= 0x20;
      else if ((_ Q17 > LevelAnalDTMF) && ((_ Q15 + _ Q16 + _ Q18) < LevelAnalDTMF)) code|= 0x30;
      else if ((_ Q18 > LevelAnalDTMF) && ((_ Q15 + _ Q16 + _ Q17) < LevelAnalDTMF)) code|= 0x40;
      else code = 0;
	  if (code)	{	// ���� ���
	   switch (code) {		// ������������� � ������ DTMF
	   	case 0x11 : code = 2;  break;
	   	case 0x21 : code = 3;  break;
	   	case 0x31 : code = 4;  break;
	   	case 0x41 : code = 11; break;
	   	case 0x12 : code = 5;  break;
	   	case 0x22 : code = 6;  break;
	   	case 0x32 : code = 7;  break;
	   	case 0x42 : code = 12; break;
	   	case 0x13 : code = 8;  break;
	   	case 0x23 : code = 9;  break;
	   	case 0x33 : code = 10; break;
	   	case 0x43 : code = 13; break;
	   	case 0x14 : code = 15; break;
	   	case 0x24 : code = 1;  break;
	   	case 0x34 : code = 16; break;
	   	case 0x44 : code = 14; break;
	   } if (code && (code != _ code))  {
// !!!!!!!!!!!!!!!!!!!!!!!!  ���� ��� ����
		 if (_ chan >= LenHard) {
	 	  MasOutAnalize[0]= 1;					// ��� ������
	 	  MasOutAnalize[1]= _ chan - LenHard ;	// ����� ������
		 } else if (_ chan >= LenSport1) {
	 	  MasOutAnalize[0]= 0;					// ��� ������
	 	  MasOutAnalize[1]= _ chan - LenSport1 ;// ����� ������
		 }
		MasOutAnalize[2]= AnsDTMF;	// ��� �������� ����
		MasOutAnalize[3]=	code-1; 	// ����� ��������
	 	MasOutAnalize[4]= _ timer;
		MasOutAnalize[5]= _ timer >> 8;
		_ timer = 0;
    	OutAbon(MasOutAnalize, 6);
// !!!!!!!!!!!!!!!!!!!!!!!
	   }
	  } _ code = code;
	 _ Q11 = _ Q21 = _ Q12 = _ Q22 = _ Q13 = _ Q23 = _ Q14 = _ Q24 =
 	 _ Q15 = _ Q25 = _ Q16 = _ Q26 = _ Q17 = _ Q27 = _ Q18 = _ Q28 = _ middle = 0;
 	 _ count = Count_DTMF; _ half = 1;
	 }
	 }
     AdrAnalDTMF = _ pxNextChan;
    }
#undef _
  }

  { // --------------------------- ��������� ������ PPSR --------------------------------
    xListAnalPPSR *AdrAnalPPSR;

#define _ AdrAnalPPSR ->
	AdrAnalPPSR = ListAnalPPSR.pxHead;
	while (AdrAnalPPSR)
	{ // ���������� ��� ������ �������
	  if ((_ count  < (Count_PPSR-(Count_PPSR/4)) && (_ middle < (min29dB * (Count_PPSR/4))))) // �������� �� ������� ������� ����� 37.5 ��
	 {_ Q11 = _ Q21 = _ Q12 = _ Q22 = _ Q13 = _ Q23 = _ Q14 = _ Q24 =	// �� ����������� ���� ������ ������
	  _ Q18 = _ Q28 = _ middle = _ yescode = 0;
 	  _ count = Count_PPSR;} 
	 else if (!_ count) 			// ���������� ���������� ������� ?
	 {_ Q11 = sqrt(abs((float)_ Q11 * (float) _ Q11 + (float)_ Q21 * (float) _ Q21 -
       ((float)_ Q11 * (float)_ Q21 * (float)27978) / Mult));
	  _ Q12 = sqrt(abs((float)_ Q12 * (float) _ Q12 + (float)_ Q22 * (float) _ Q22 -
       ((float)_ Q12 * (float)_ Q22 * (float)26955) / Mult));
	  _ Q13 = sqrt(abs((float)_ Q13 * (float) _ Q13 + (float)_ Q23 * (float) _ Q23 -
       ((float)_ Q13 * (float)_ Q23 * (float)25700) / Mult));
	  _ Q14 = sqrt(abs((float)_ Q14 * (float) _ Q14 + (float)_ Q24 * (float) _ Q24 -
       ((float)_ Q14 * (float)_ Q24 * (float)24217) / Mult));
	  _ Q18 = sqrt(abs((float)_ Q18 * (float) _ Q18 + (float)_ Q28 * (float) _ Q28 -
       ((float)_ Q18 * (float)_ Q28 * (float) 9314) / Mult));
	   // ������ ��������� 
	   _ mult = (((100 * Max_mid)/Count_PPSR)<<15) / _ middle;	// _ mult = 100 * (Max mid = 25000 / N ����� = 800) * (32767 / _ middle);
      // ---------------------- ����������� ���� PPSR
	  code = 0;
      if ((_ Q11 > LevelAnalPPSR) && ((_ Q12 + _ Q13 + _ Q14) < LevelAnalPPSR)) code = 0x01;	// ����� 1-� �������
      else if ((_ Q12 > LevelAnalPPSR) && ((_ Q11 + _ Q13 + _ Q14) < LevelAnalPPSR)) code = 0x02;
      else if ((_ Q13 > LevelAnalPPSR) && ((_ Q11 + _ Q12 + _ Q14) < LevelAnalPPSR)) code = 0x03;
      else if ((_ Q14 > LevelAnalPPSR) && ((_ Q11 + _ Q12 + _ Q13) < LevelAnalPPSR)) code = 0x04;
      if ((code) && (_ Q18 > LevelAnalPPSR)) 
      {code|= 0x40;	// 2-� �������
	   switch (code) {		// ������������� � ������ DTMF
	   	case 0x41 : code = 10; 	break;
	   	case 0x42 : code = 11;	break;
	   	case 0x43 : code = 12;	break;
	   	case 0x44 : code = 13; break;
	   }
	   if (code == _ code) 
	   {_ yescode++;
	    if (_ yescode > 1) _ yescode = 3; else
	   {
// !!!!!!!!!!!!!!!!!!!!!!!!  ���� ��� ����
		 if (_ chan >= LenHard) {
	 	  MasOutAnalize[0]= 1;				// ��� ������
	 	  MasOutAnalize[1]= _ chan - LenHard ;// ����� ������
		 } else if (_ chan >= LenSport1) {
	 	  MasOutAnalize[0]= 0;				// ��� ������
	 	  MasOutAnalize[1]= _ chan - LenSport1 ;// ����� ������
		 }
		MasOutAnalize[2]= AnsPPSR;			// ��� �������� PPSR
		MasOutAnalize[3]=	code; 				// ����� ��������
    	OutAbon(MasOutAnalize, 4);
// !!!!!!!!!!!!!!!!!!!!!!!
	   }
	  }
	 } else _ yescode = 0; 
	 _ code = code;
	 _ Q11 = _ Q21 = _ Q12 = _ Q22 = _ Q13 = _ Q23 = _ Q14 = _ Q24 =
 	 _ Q18 = _ Q28 = _ middle = 0;
 	 _ count = Count_PPSR;
	 }
     AdrAnalPPSR =  _ pxNextChan;
    }
#undef _
  }
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`


  { // --------------------------- ������ (��������) ��������� ������ PCDT --------------------------------
    xListAnalPCDT *AdrAnalPCDT;

#define _ AdrAnalPCDT ->	
	AdrAnalPCDT = ListAnalPCDT.pxHead;
	while (AdrAnalPCDT)
	{ // ���������� ��� ������ �������
	  if (!_ count800) 		// ���������� ���������� �������
	 {_ Q101 = sqrt(abs((float)_ Q101 * (float) _ Q101 + (float)_ Q201 * (float) _ Q201 -
       ((float)_ Q101 * (float)_ Q201 * (float)F316)  / Mult));       
	  _ Q102 = sqrt(abs((float)_ Q102 * (float) _ Q102 + (float)_ Q202 * (float) _ Q202 -
       ((float)_ Q102 * (float)_ Q202 * (float)F430 ) / Mult));
	  _ Q103 = sqrt(abs((float)_ Q103 * (float) _ Q103 + (float)_ Q203 * (float) _ Q203 -
       ((float)_ Q103 * (float)_ Q203 * (float)F585 ) / Mult));
	  _ Q104 = sqrt(abs((float)_ Q104 * (float) _ Q104 + (float)_ Q204 * (float) _ Q204 -
       ((float)_ Q104 * (float)_ Q204 * (float)F795 ) / Mult));
	  _ Q105 = sqrt(abs((float)_ Q105 * (float) _ Q105 + (float)_ Q205 * (float) _ Q205 -
       ((float)_ Q105 * (float)_ Q205 * (float)F1080) / Mult));
	  _ Q106 = sqrt(abs((float)_ Q106 * (float) _ Q106 + (float)_ Q206 * (float) _ Q206 -
       ((float)_ Q106 * (float)_ Q206 * (float)F1470) / Mult));
	  _ Q107 = sqrt(abs((float)_ Q107 * (float) _ Q107 + (float)_ Q207 * (float) _ Q207 -
       ((float)_ Q107 * (float)_ Q207 * (float)F2000) / Mult));
	  _ Q108 = sqrt(abs((float)_ Q108 * (float) _ Q108 + (float)_ Q208 * (float) _ Q208 -
       ((float)_ Q108 * (float)_ Q208 * (float)F890 ) / Mult));
	  _ Q109 = sqrt(abs((float)_ Q109 * (float) _ Q109 + (float)_ Q209 * (float) _ Q209 -
       ((float)_ Q109 * (float)_ Q209 * (float)F1215) / Mult));
	  _ Q110 = sqrt(abs((float)_ Q110 * (float) _ Q110 + (float)_ Q210 * (float) _ Q210 -
       ((float)_ Q110 * (float)_ Q210 * (float)F1360) / Mult));
	  _ Q111 = sqrt(abs((float)_ Q111 * (float) _ Q111 + (float)_ Q211 * (float) _ Q211 -
       ((float)_ Q111 * (float)_ Q211 * (float)F1620) / Mult));
	  _ Q112 = sqrt(abs((float)_ Q112 * (float) _ Q112 + (float)_ Q212 * (float) _ Q212 -
       ((float)_ Q112 * (float)_ Q212 * (float)F2720) / Mult));

      if (_ Q101>LevelAnal) {if (_ YesFreq[ 0]<17) _ YesFreq[ 0]++;} else {_ YesFreq[ 0]-=2; if (_ YesFreq[ 0]<0) _ YesFreq[ 0]=0;}
      if (_ Q102>LevelAnal) {if (_ YesFreq[ 1]<17) _ YesFreq[ 1]++;} else {_ YesFreq[ 1]-=2; if (_ YesFreq[ 1]<0) _ YesFreq[ 1]=0;}
      if (_ Q103>LevelAnal) {if (_ YesFreq[ 2]<17) _ YesFreq[ 2]++;} else {_ YesFreq[ 2]-=2; if (_ YesFreq[ 2]<0) _ YesFreq[ 2]=0;}
      if (_ Q104>LevelAnal) {if (_ YesFreq[ 3]<17) _ YesFreq[ 3]++;} else {_ YesFreq[ 3]-=2; if (_ YesFreq[ 3]<0) _ YesFreq[ 3]=0;}
      if (_ Q105>LevelAnal) {if (_ YesFreq[ 4]<17) _ YesFreq[ 4]++;} else {_ YesFreq[ 4]-=2; if (_ YesFreq[ 4]<0) _ YesFreq[ 4]=0;}
      if (_ Q106>LevelAnal) {if (_ YesFreq[ 5]<17) _ YesFreq[ 5]++;} else {_ YesFreq[ 5]-=2; if (_ YesFreq[ 5]<0) _ YesFreq[ 5]=0;}
      if (_ Q107>LevelAnal) {if (_ YesFreq[ 6]<17) _ YesFreq[ 6]++;} else {_ YesFreq[ 6]-=2; if (_ YesFreq[ 6]<0) _ YesFreq[ 6]=0;}
      if (_ Q108>LevelAnal) {if (_ YesFreq[ 7]<17) _ YesFreq[ 7]++;} else {_ YesFreq[ 7]-=2; if (_ YesFreq[ 7]<0) _ YesFreq[ 7]=0;}
      if (_ Q109>LevelAnal) {if (_ YesFreq[ 8]<17) _ YesFreq[ 8]++;} else {_ YesFreq[ 8]-=2; if (_ YesFreq[ 8]<0) _ YesFreq[ 8]=0;}
      if (_ Q110>LevelAnal) {if (_ YesFreq[ 9]<17) _ YesFreq[ 9]++;} else {_ YesFreq[ 9]-=2; if (_ YesFreq[ 9]<0) _ YesFreq[ 9]=0;}
      if (_ Q111>LevelAnal) {if (_ YesFreq[10]<17) _ YesFreq[10]++;} else {_ YesFreq[10]-=2; if (_ YesFreq[10]<0) _ YesFreq[10]=0;}
      if (_ Q112>LevelAnal) {if (_ YesFreq[11]<17) _ YesFreq[11]++;} else {_ YesFreq[11]-=2; if (_ YesFreq[11]<0) _ YesFreq[11]=0;}

      if (_ middle < (min29dB * 800)) _ mult = 0; else	// ��� ��������������� - 29 ��
      _ mult = 409587500 / _ middle;	// _ mult = (Max mid = 25000 / N ����� = 200) * (32767 / _ middle);
//_ mult = 809587500 / _ middle;	// _ mult = (Max mid = 25000 / N ����� = 200) * (32767 / _ middle);

	  // ����������� ������� ��������� ��������
      // Timer[0] - 1-� �������, Timer[1] - ������ �� 100 �� �������� 2-� �������
      if (_ Timer[1])		// ���� �� ���������� ������ �������?
      {_ Timer[1]--;		// ��, ����� 2-� �������
       for (i=0;i<12;i++) 	// ��, ����� 2-� �������
       {if (_ YesFreq[_ Timer[0]] > 6) {_ YesFreq[_ Timer[0]] = 8; _ Timer[1] = 15;}// �� ���������� ������� 1-� �������
        if ((i != _ Timer[0]) && (_ YesFreq[i]>13))// ������� 2-� ������� � ��� �� ����� 1-�
        {Nab = NomAbonPCDT[_ Timer[0]][i]; _ YesFreq[i] = 8;	// ����������� ������ �������� � ��������� ������ 2-� �������
// !!!!!!!!!!!!!!!!!!!!!!!!1
		 if (_ chan >= LenHard) {
	 	  MasOutAnalize[0]= 1;					// ��� ������
	 	  MasOutAnalize[1]= _ chan - LenHard ;	// ����� ������
		 } else if (_ chan >= LenSport1) {
	 	  MasOutAnalize[0]= 0;					// ��� ������
	 	  MasOutAnalize[1]= _ chan - LenSport1 ;	// ����� ������
		 }
		 MasOutAnalize[2]= ImitPCDT;				// ��� �������� - �������� PCDT
 		 MasOutAnalize[3]= Nab;		 			// ����� ��������
	 	 MasOutAnalize[4]=_ Timer[0];
		 MasOutAnalize[5]=i;
    	 OutAbon(MasOutAnalize,6);
// !!!!!!!!!!!!!!!!!!!!!!!11
		 _ Timer[0] = i; break;// ������ ��� 1-� �������
       }
      } 
     } else // �� ���� ������ �������
     {for (i=0;i<12;i++) 	// ��, ����� 1-� �������
       if (_ YesFreq[i]>6) {_ Timer[0] = i; _ Timer[1] = 15; break;}}
       
      _ Q101=_ Q201=_ Q102=_ Q202=_ Q103=_ Q203=_ Q104=_ Q204=_ Q105=_ Q205=_ Q106=_ Q206=_ Q107=_ Q207=_ Q108=_ Q208=_ Q109=
      _ Q209=_ Q110=_ Q210=_ Q111=_ Q211=_ Q112=_ Q212=_ middle=0;

      _ count800 = 800;		// ����������� ������� ������ ���� ������ 100 ��
	 }
     AdrAnalPCDT = _ pxNextChan;
    }
#undef _
  }
}

void InitAnalizeList(void)
{
  ListAnalTDCF.pxHead=NULL;
  ListAnalTDCL.pxHead=NULL;
  ListAnal.pxHead=NULL;
  ListAnalDTMF.pxHead=NULL;
  ListAnalPPSR.pxHead=NULL;
  ListAnalPCDT.pxHead=NULL;
  ListSpec.pxHead=NULL;
}

