#include "..\Sinfo.h"
#include <signal.h>
#include <fract.h>
#include <math.h>

///////////////////////////////////////////////////////////////////////////////
void *pvPortMalloc(size_t xSize);
void vPortFree(void *pv);

unsigned char IsCurrentTickCountGT(unsigned int dwTick0);
unsigned char IsCurrentTickCountLT(unsigned int dwTick0);

void vSportTask(void *pvParameters);
void vWaitSportTask(void *pvParameters); // ������� �������������� ������ �� SPORT

///////////////////////////////////////////////////////////////////////////////
#ifdef __ISDN__
#include "..\EDSS/pri_channel.h" 
#include "..\edss\pri_call.h"

extern volatile unsigned char NUM_BUF; // ������� ����� ������� ������ (��� ������������� - ������������)
extern volatile unsigned char GET_BUF; // ������� ���������� ������

extern unsigned char hw_txbuf0[COL_BUF][LEN_BUF]; // ����� �������� 1-�� ������ E1 (COL_BUF - ����� ������)
extern unsigned char hw_txbuf1[COL_BUF][LEN_BUF]; // ����� �������� 2-�� ������ E1 (LEN_BUF - ����� ������)
extern unsigned char hw_txbuf2[COL_BUF][LEN_BUF]; // ����� �������� 3-�� ������ E1 (COL_BUF - ����� ������)
extern unsigned char hw_txbuf3[COL_BUF][LEN_BUF]; // ����� �������� 4-�� ������ E1 (LEN_BUF - ����� ������)

extern unsigned char hw_rxbuf0[COL_BUF][LEN_BUF]; // ����� ������ 1-�� ������ E1
extern unsigned char hw_rxbuf1[COL_BUF][LEN_BUF]; // ����� ������ 2-�� ������ E1
extern unsigned char hw_rxbuf2[COL_BUF][LEN_BUF]; // ����� ������ 3-�� ������ E1
extern unsigned char hw_rxbuf3[COL_BUF][LEN_BUF]; // ����� ������ 4-�� ������ E1
#endif

extern unsigned char TablSum[LenSumChan]; // ������� ������� ����������� �� ������� ���� ���������� (�������������)
extern unsigned char TablBlock[LenSumChan]; // ������� ������� ����������� �� �����
extern unsigned char CopyTablBlock[LenSumChan]; // ����� ������� ������� ����������� �� �����

extern unsigned short ParamUKC[]; // ��������� (�� 1 ����) ���������� ������ ������� ���(��.) � E1(��.) 
                                  // � ������� ������������� ��� �������� ������� � ��������� SNMP

extern unsigned int SumADCMiddle[LenSport0+LenVirt]; // ������� ����� ������� �������� �� ������ ������������
extern unsigned int SumDACMiddle[LenSport0+LenVirt]; // ������� ����� ������� �������� �� �������, ������ ����������� � �� ���
extern signed int SumADCShift[LenVirt]; // ������� �������� �������� �� ������ ������������
extern unsigned short RxBuf0[]; // SPORT0 DMA1 receive buffer
extern unsigned short TxBuf0[]; // SPORT0 DMA2 transmit buffer
extern short RxBuf1[]; // SPORT1 DMA3 receive buffer
extern short TxBuf1[]; // SPORT1 DMA4 transmit buffer

extern short InDataChan[LenChan+1], OutDataChan[LenChan+1];// ������ �������� � ���������� �������
extern unsigned short g_wMasDisOut[LenSport0];
extern unsigned short PlayWav[LenVirt];

/*		��������� ����������������� ������� ��� � MK� TestBoard (�� 2 ����):
		00 - ������
		01 - ������ �� ��������
		10 - ������ ��������
		11 - ������ ��������.*/
extern unsigned int TestBoard; // �������� ����������������� (�� 2 ����) ���������� ���� (��.) � ������� �1(��.)

extern unsigned short CountMiddle; // ������� ���������� ������� ��������

extern unsigned short Count5mc;

extern unsigned char VedSelect[NumberKeyboard]; // ����� ������ �������� ���������

extern sListRecord ListRecord; // ��������� ������ �����������
extern sListPlay ListPlay; // ��������� ������ ��������������������
extern xListSNMP *AdrTempBufSNMP[]; // ������ ��������� ������� ��������� SNMP

extern xQueueHandle xQueueSoundIpIn; // ������� ��� ������ �������� ������� ����� IP
extern xQueueHandle xQueueSoundIpOut; // ������� ��� �������� �������� ������� ����� IP
extern xQueueHandle xQueueCommand; // �� ��������� ������
extern xQueueHandle xQueueReply[];

extern unsigned short DataSave[LenDataSave],DataSave1[LenDataSave];

///////////////////////////////////////////////////////////////////////////////
unsigned int /*Time1,Time2,*/TimeM; // ��� ������� ������� ���������� ������ SportX

unsigned char FailureMKA[LenMKA]; // ��������� ������� ���
unsigned char OldFailureMKA[LenMKA]; // ���������� ��������� ������� ���

unsigned char TimeMiddle[LenVirt]; // ������ ���������� �������� ������ �������� �� ������

unsigned short Middle[LenSport0+LenVirt];   // ������� �������� �� ���������� ������� � ������ ������������
unsigned short OldMiddle[LenSport0+LenVirt]; // ���������� ������� �������� �� ���������� ������� � ������ ������������
unsigned short MiddleMaxT[LenSport0+LenVirt]; // ������� ������������ ������� �������� �� ���������� ������� � ������ ������������
unsigned short MiddleMax[LenSport0+LenVirt]; // ������������ ������� �������� �� ���������� ������� � ������ ������������
unsigned char CountYesMiddle[LenVirt]; // ������� ������������ �� ������
unsigned char PorogMiddle[LenVirt]; // ����� �������� �������� �� ������ ������������
extern unsigned short Revers[LenVirt]; // ������� �������� �� ������ ������������
unsigned char YesPorogMiddle[LenVirt]; // ���������� ������� �������� �������� �� ������ ������������ (==0 - ������ �������)

unsigned short SumADCDAC[LenSport0+LenVirt]; // ������� �������� �� ���������
unsigned short SumDAC[LenSport0+LenVirt]; // ������� �������� �� ���������
unsigned short OldSumADCDAC[LenSport0+LenVirt]; // ������ ������� �������� �� ���������
unsigned char YesSpeak[LenSport0+LenVirt]; // ������� ������� ��������� 0-���, 1...100 - ����
char YesSpeakSel[LenSport0+LenVirt]; // ������� ������� ��������� �� ��������� 0-���, 1...100 - ����
unsigned char TimeSelect[NumberKeyboard]; // ������ ��� ��������� ������� ��������� � ��������
unsigned short SpeakInSel; // ����� ����������� ��������� ������ �������� ���������

unsigned char DataSound[LenHeadSound+LenSound];

// -------------- �������� ������ -----------------------
short ErrMallocGener; // ������ �������������� ������ ��� �������� ������� ���������
short ErrMallocAns; // ������ �������������� ������ ��� �������� ������� ������ � ������� ���������
short ErrMallocCom; // ������ �������������� ������ ��� �������� ������� ������ ���������� ���
short ErrMallocRecord; // ������ �������������� ������ ��� �������� ������� �����������
short ErrMallocPlay; // ������ �������������� ������ ��� �������� ������� ��������������������
short ErrMallocSpec; // ������ �������������� ������ ��� �������� ������� ��������� ����������
short ErrQueueAns; // ������������ ������� ������ � ������� ���������
short ErrQueueSNMP; // ������������ ������� ������ SNMP
short ErrQueueEDSS; // ������������ ������� ������ EDSS
short Err5ms; // ������ ��������� ������� ��������� � ������� 5 ��
short ErrSPORT0; // ������ ������ �� SPORT0
short CountErr; // ������ �������
// ------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
void OutSNMP(unsigned char NMod, unsigned char Port, unsigned char Cod)
{ // ������ � ����� ������ �� ��������� ��������� �������
/*  unsigned short user, shuser, i;
  user = 0;
  shuser = 1;
  while(user<QueueReplyCnt) 
  {
    if(ParamUKC[2] & shuser) 
    {
      i = AdrTempBufSNMP[user] -> countIn;					// ������ ������
      AdrTempBufSNMP[user] -> abon[i][0] = NMod; 			// ����� ������
      AdrTempBufSNMP[user] -> abon[i][1] = Port;			// ����� �����
      AdrTempBufSNMP[user] -> abon[i][2] = Cod;				// ���
      AdrTempBufSNMP[user] -> timer = 0;
      i = AdrTempBufSNMP[user] -> countIn = (i + 1)  % LenSNMP; // ���������� ������ ������
      if(i==AdrTempBufSNMP[user]->countOut)
      { // "������� ��������" - ���������� ������ ��������, ������ ������������ ���������
         AdrTempBufSNMP[user] -> countOut = (i + 1) % LenSNMP;
      }
    }
    user++; shuser <<= 1;
  }*/
}

void Init_Sport0(void)
{
// Sport0, �����
	*pSPORT0_RCLKDIV=0; 					// ������� CLK
	*pSPORT0_RFSDIV=0;						// ������� RFS
	*pSPORT0_RCR1 = 0;						// ������� CLK, ������� RFS
	*pSPORT0_RCR2 = SLEN_16 | RXSE;			// 16 ���, ��. ������
	ssync();

	*pSPORT0_TCLKDIV=5;						// ���������� CLK ((SCLK/2) / (TCLKDIV+1) (49152/2) / (5+1) = 4096 ���
	*pSPORT0_TFSDIV=0x01FF;					// ���������� TFS (CLK / (TFSDIV+1))         4096 / (511+1) = 8 ���
	*pSPORT0_TCR1 = DITFS | ITCLK | ITFS;	// ���������. ����. �����. �� ������, ���������� CLK, ���������� TFS
	*pSPORT0_TCR2 = SLEN_16 | TXSE;			// 16 ���, ��. ������
  ssync();
}

// �������� ������ ������������ ������ � ��������������� ������
//#define MFD_0       0x0000
//#define MFD_1       0x1000  // =1

void Init_Sport1(void)
{
  // Set SPORT1 in Multichannel mode
  *pSPORT1_MCMC2=0x0000;    // Multichannel disable
  ssync();
  *pSPORT1_MCMC1=0x3000;    // Window Size 32, Offset 0
  ssync();

  // set 32 channels TX
  *pSPORT1_MTCS0=0xffffffff;
  *pSPORT1_MTCS1=0x00000000;
  *pSPORT1_MTCS2=0x00000000;
  *pSPORT1_MTCS3=0x00000000;
  ssync();

  // set 32 channels RX
  *pSPORT1_MRCS0=0xffffffff;
  *pSPORT1_MRCS1=0x00000000;
  *pSPORT1_MRCS2=0x00000000;
  *pSPORT1_MRCS3=0x00000000;
  ssync();

  *pSPORT1_RFSDIV=0;
  *pSPORT1_RCLKDIV=0;
  *pSPORT1_TFSDIV=0;
  *pSPORT1_TCLKDIV=0;
  *pSPORT1_RCR1=RDTYPE; // Sport1 ����� ������� CLK, ������� RFS, 8 ���, ��. ������, �-�����
  ssync();

//  *pSPORT1_RFSDIV=0xFF; // ���������� RFS (CLK / (RFSDIV+1))           2048 / (255+1) = 8 ���
//  *pSPORT1_RCR1=IRFS | RDTYPE; // ������� CLK, ���������� RFS, 8 ���, ��. ������, �-�����
*pSPORT1_RCLKDIV=11;  // ���������� CLK ((SCLK/2) / (RCLKDIV+1) (49152/2) / (11+1)  = 2048 ���
*pSPORT1_RCR1=IRCLK | IRFS | RDTYPE; // ���������� CLK, ���������� RFS, 8 ���, ��. ������, �-�����
  *pSPORT1_RCR2 = SLEN_8 | RXSE;
  ssync();

//  *pSPORT1_TCR1=TDTYPE;
*pSPORT1_TFSDIV=0xFF; // ���������� TFS (CLK / (RFSDIV+1))           2048 / (255+1) = 8 ���
*pSPORT1_TCLKDIV=11;  // ���������� CLK ((SCLK/2) / (RCLKDIV+1) (49152/2) / (11+1)  = 2048 ���
*pSPORT1_TCR1=ITCLK | ITFS | DITFS | TDTYPE; //���������� CLK,���������� RFS,8 ���,��.������, �-�����, ���������.����.�����.�� ������
  ssync();

  *pSPORT1_TCR2 = SLEN_8 | TXSE;
  ssync();
}

void Init_DMA12(void)
{
// ��������� DMA1 �� ����� �� Sport0
	// ���������������, ������. �� ���������� ������� ����� ������, 2-������ �����
	// 16-�� ��������� ��������, ������ � ������ �� ����������
	*pDMA1_PERIPHERAL_MAP = 0x1000;
	*pDMA1_CONFIG = FLOW_1 | DMA2D | WDSIZE_16 | WNR | DI_EN;
	*pDMA1_START_ADDR = RxBuf0;	// ��������� �����
	*pDMA1_X_COUNT = 2;			// ���������� ������� ����
	*pDMA1_X_MODIFY	= 64;		// ����������� ������ ����������� �������� � ������
	*pDMA1_Y_COUNT = 32;		// ������� ������� ����
	*pDMA1_Y_MODIFY	= -62;		// ����������� ������ �������� �������� � ������

// ��������� DMA2 �� �������� �� Sport0
	// ���������������, 2-������ �����
	// 16-�� ��������� ��������
	*pDMA2_PERIPHERAL_MAP = 0x2000;
	*pDMA2_CONFIG = FLOW_1 | DMA2D | WDSIZE_16;// | DI_EN;
	*pDMA2_START_ADDR = TxBuf0;	// ��������� �����
	*pDMA2_X_COUNT = 2;			// ���������� ������� ����
	*pDMA2_X_MODIFY	= 64;	// ����������� ������ ����������� �������� � ������
	*pDMA2_Y_COUNT = 32;// ������� ������� ����
	*pDMA2_Y_MODIFY	= -62;		// ����������� ������ �������� �������� � ������
	ssync();
}

void Init_DMA34(void)
{
// ��������� DMA3 �� ����� �� Sport1
	// ���������������, /*������. �� ���������� ������� ����� ������,*/ 2-������ �����
	// 16-�� ��������� ��������, ������ � ������ �� ����������
	*pDMA3_PERIPHERAL_MAP = 0x3000;
	*pDMA3_CONFIG = FLOW_1 | DMA2D | WDSIZE_16 | WNR | DI_EN;
	*pDMA3_START_ADDR = RxBuf1;	// ��������� �����
	*pDMA3_X_COUNT = 2;			// ���������� ������� ����
	*pDMA3_X_MODIFY	= 64;		// ����������� ������ ����������� �������� � ������
	*pDMA3_Y_COUNT = 32;		// ������� ������� ����
	*pDMA3_Y_MODIFY	= -62;		// ����������� ������ �������� �������� � ������
	
// ��������� DMA4 �� �������� �� Sport1
	// ���������������, 2-������ �����
	// 16-�� ��������� ��������
	*pDMA4_PERIPHERAL_MAP = 0x4000;
	*pDMA4_CONFIG = FLOW_1 | DMA2D | WDSIZE_16;// | DI_EN;
	*pDMA4_START_ADDR = TxBuf1;	// ��������� �����
	*pDMA4_X_COUNT = 2;			// ���������� ������� ����
	*pDMA4_X_MODIFY	= 64;		// ����������� ������ ����������� �������� � ������
	*pDMA4_Y_COUNT = 32;		// ������� ������� ����
	*pDMA4_Y_MODIFY	= -62;		// ����������� ������ �������� �������� � ������
	ssync();
}

void Enable_DMA_Sport0(void)
{
  unsigned int dwIRQ=cli();
  // enable DMAs
  *pDMA2_CONFIG |= DMAEN;
  *pDMA1_CONFIG |= DMAEN;
  ssync();

  //���������� ������ /�������� SPORT0
  *pSPORT0_TCR1 |= TSPEN;
  *pSPORT0_RCR1 |= RSPEN;
  ssync();
  sti(dwIRQ);
}

void Disable_DMA_Sport0(void)
{
  unsigned int dwIRQ=cli();
  // disable SPORT
  *pSPORT0_TCR1 &= ~TSPEN;
  *pSPORT0_RCR1 &= ~RSPEN;
  ssync();
  // disable DMAs
  *pDMA2_CONFIG &= ~DMAEN;
  *pDMA1_CONFIG &= ~DMAEN;
  ssync();
  sti(dwIRQ);
}

void RestartSPORT0(void)
{
//SendStr2IpLogger("RestartSPORT0");
  Disable_DMA_Sport0();
  Enable_DMA_Sport0();
}

void Enable_DMA_Sport1(void)
{
  unsigned int dwIRQ=cli();
  // enable DMAs
  *pDMA4_CONFIG |= DMAEN;
  *pDMA3_CONFIG |= DMAEN;
  ssync();

  //���������� ������ /�������� SPORT1
  *pSPORT1_TCR1 |= TSPEN;
  *pSPORT1_RCR1 |= RSPEN;
  ssync();
  sti(dwIRQ);
}

void Disable_DMA_Sport1(void)
{
  unsigned int dwIRQ=cli();
  // disable SPORT
  *pSPORT1_TCR1 &= ~TSPEN;
  *pSPORT1_RCR1 &= ~RSPEN;
  ssync();
  // disable DMAs
  *pDMA3_CONFIG &= ~DMAEN;
  *pDMA4_CONFIG &= ~DMAEN;
  ssync();
  sti(dwIRQ);
}

void RestartSPORT1(void)
{
//SendStr2IpLogger("RestartSPORT1");
  Disable_DMA_Sport1();
  Enable_DMA_Sport1();
}

void StartSPORTs(void)
{ // ����������������� SPORT
  *pSPORT0_TCR1=0; ssync();
  *pSPORT0_RCR1=0; ssync();
  *pSPORT1_TCR1=0; ssync();
  *pSPORT1_RCR1=0; ssync();

  *pDMA1_CONFIG=0; ssync();
  *pDMA2_CONFIG=0; ssync();
  *pDMA3_CONFIG=0; ssync();
  *pDMA4_CONFIG=0; ssync();

  Init_Sport0();
  Init_DMA12();

  Init_Sport1();
  Init_DMA34();

  Enable_DMA_Sport0();
  Enable_DMA_Sport1();
}

void InitTaskSport(void)
{ // ������������� SPORT0 � SPORT1
  unsigned char i;

  // ------------------------ ������������� ���������� �� (������� ���������� ������� � �������������)

 if((RdFlash(AdrLic)==0xFFFF) && (RdFlash(AdrLic+2)==0xFFFF)) 
 { // ----- ������������� Flash � ������ ���������� ������� ��� ��������������� �������� ������ �� -----
 	ReWrFlash(0x0000,AdrParamUKC+0); // ��������� ��
 	ReWrFlash(0x0040,AdrParamUKC+2);
 	ReWrFlash(0x0000,AdrParamUKC+4);
 	ReWrFlash(0x0000,AdrParamUKC+6);
 	ReWrFlash(0x0000,AdrParamUKC+8);
 	ReWrFlash(0x0000,AdrParamUKC+10);
 	ReWrFlash(0x0000,AdrMinARU); // ������������ �������� ���
 	ReWrFlash(0x0000,AdrARU); // ��� ���������
 	ReWrFlash(0x0000,AdrZagrBK); // �������� �� � ����������
 }

 ParamUKC[0] = RdFlash(AdrParamUKC); // ���������� ������
 ParamUKC[1] = RdFlash(AdrParamUKC+2) & 0x42; // ������������ �������� ���������
 ParamUKC[2] = RdFlash(AdrParamUKC+4) & 0x42; // ������������ SNMP
 ParamUKC[3] = RdFlash(AdrParamUKC+6); // ������� ������� ����������
#ifdef __ISDN__
 if(RdFlash(AdrParamUKC+8)>16) ReWrFlash(5,AdrParamUKC+8);
 ParamUKC[4] =  RdFlash(AdrParamUKC+8); // ��������� ����� ������ ISDN (0-���, 1...16 - ������ �������)
 ParamUKC[5] = RdFlash(AdrParamUKC+10); // ����� ������� ISDN (1 ��� 2)
 ParamUKC[6] = RdFlash(AdrParamUKC+12); // ��� �� �������:
                                        // 100000 - ������� ������� ������ ��� �� 1-� �����, 
                                        // 010000 - ������� ������� ������ ��� �� 2-� �����, 
                                        // 001000 - �� ������������ (TestISDN==1), 
                                        // 000100 ����� ������ �� ��. ����� ������ ���, 00010 �������� ��, 00001 ������.
 if(ParamUKC[5]>2)
 { ParamUKC[5]=0; ParamUKC[6]=0; }
 if(ParamUKC[6] & 0x08) ; // �� � ������ ������������� ?
#else
 ParamUKC[4] = ParamUKC[5] = ParamUKC[6] = 0;
#endif
 // ------------- ������������� ���������� ----------------------------
 RestartTempBuf();

 TestBoard=0xFFFFFFFF; // ��������� ��������� - ��� ������ ���������

 for(i=0;i<LenMKA;i++)
 { FailureMKA[i]=0x3; OldFailureMKA[i]=0x0; }

 ClearTablCommut(); 

 InitVarsMKC();

 memset(g_wMasDisOut,0xff,sizeof(g_wMasDisOut));
 memset(PlayWav,0,sizeof(PlayWav));

 InitAnalizeList();
 
#ifdef __ADACE__
 ListAnalADACE.Number=0;
#endif

  ListRecord.pxHead=NULL;
  ListPlay.pxHead=NULL;

  extern unsigned short OutTestData[LenMKA]; // �������� (���������) ������ �� ����� ���������� �������
  for (i=0;i<LenMKA;i++) OutTestData[i]=i+0xF000;

  // �������� ������������ ������ �� ������, ������� � �������
  if(xTaskCreate(vSportTask,"SportX",STACK_SportX,NULL,tskIDLE_PRIORITY+4,thSportX)!=pdPASS)
  { BlockWatchDog(); }
}

void OutWriteSound(unsigned short LengthS, unsigned int IPAddr,unsigned short PortSound, unsigned char *SoundBuf)
{
  unsigned char btInd,
                btHI=IPAddr & 0xff;
  static sSoundIp sInfo; // ��������� ��� ������ ������� �����������

  if(LengthS>sizeof(sSoundIp)) LengthS=sizeof(sSoundIp);
  sInfo.L = LengthS; // ���������� ���� � �������
  sInfo.A1 =IPAddr;
  sInfo.PORT1 = PortSound;
  memcpy(sInfo.ByteInfo,SoundBuf,LengthS);
  xQueueSend(xQueueSoundIpOut,&sInfo,0,0);
}

void TestBoardMKA(void)
{
  unsigned char i,j;
  unsigned int ii;

  // ���� ������ ������� ���
  ii = TestBoard;
  for(i=0;i<LenMKA;i++)
  {
    FailureMKA[i]=ii & 0x3; // ��������� ������
    if(FailureMKA[i]!=OldFailureMKA[i]) 
    {
      if(FailureMKA[i] == 0x00) j = 1;
      else
        if(FailureMKA[i] ==0x01) j = 0;
        else j = FailureMKA[i];
      if(OldFailureMKA[i]!=0x01)
      { // �� �������� "�� ��������" ���� ������ ����������
        OutSNMP(i+1,0xFF,j);
      }
      OldFailureMKA[i]=FailureMKA[i];
    }
    ii>>=2;
  }
}

void AnalizeLevel(void)
{ // ������ ������ �������� ������ 64 �� (� ��� ������ 8 ��)
  register unsigned char i,j;
  register unsigned short data,dataM;
  static char CycleMidleMax=0;

  if(!CountMiddle)
  { // �������� �� ������ �������� �������� ������ 64��
    for(i=0;i<(LenSport0+LenVirt);i++)
    {
      data = false; 
      Middle[i]=SumADCMiddle[i] >> 9; // ������ �������� ��������
      if(i<LenSport0)
      { // ���������� ������
        dataM=((128*abs(Middle[i]-OldMiddle[i]))/(Middle[i]+OldMiddle[i]+1));
      }
      else
      { // ����� �����������
        j=i-LenSport0;
        Middle[i] -= SumADCShift[j]>>9;  // ���� �������� �� Sport1
        dataM = ((128 * abs(Middle[i]-OldMiddle[i])) / (Middle[i]+OldMiddle[i]+1));
        SumADCShift[j]=0;
        if(YesPorogMiddle[j])
        { // ���� ������������� ������� �������� ��� ����� �����������
          if((Middle[i]>NoiseIn) && (dataM>PorogMiddle[j])) data=true;
          if(data)
          {
            if(CountYesMiddle[j]<6) CountYesMiddle[j]++;
          }
          else
            if(CountYesMiddle[j]) CountYesMiddle[j]--;
          if(TimeMiddle[j]) TimeMiddle[j]--;
          else
          {
            if(CountYesMiddle[j]>3)
            { // � ��� > 22, ��� ��� ����� � 8 ��� ����
              section("bigdata") static unsigned char MasOut[LenInOutData]; // ������ ��� �������� ������ �� ���������
              TimeMiddle[j] = LenTimeMiddle;// �������� �����      
              MasOut[0] = 1; // ����� �����������
              MasOut[1] = j; // ����� ������
              MasOut[2] = AnsSignal; // ��� ������� - �� ������
              MasOut[3] = 0;
              MasOut[4] = 0;
              MasOut[5] = 0;
              OutAbon(MasOut,6);
            }
          }
        }
      }
      if(Middle[i]>MiddleMaxT[i]) MiddleMaxT[i]=Middle[i]; // ������������ ������� ��� ���������
      OldMiddle[i] = Middle[i]; // ���������� ������ ��������

      // ----------- ������ ������� ��������� ----------------
      SumADCDAC[i]=(SumADCMiddle[i]+SumDACMiddle[i]) >> 9;  // ������� �������� ��� + ��� �� ������
      SumDAC[i]=SumDACMiddle[i] >> 9;                       // ������� �������� ��� �� ������
      data=(128*abs(SumADCDAC[i]-OldSumADCDAC[i])) / (SumADCDAC[i]+OldSumADCDAC[i]+1);
      if(SumADCDAC[i]>NoiseIn)
      { // ���� ������
        if(data>SpeakIn) YesSpeak[i]=1;  // ��������
        else
          if(YesSpeak[i] && (data<=GenerIn)) YesSpeak[i]=4;  // ���������
          else
            if(YesSpeak[i]) YesSpeak[i]--;
      }
      else YesSpeak[i] = 0;
    
      // ------------- ������ ��� ��������� ----------------
      if(Middle[i]>NoiseIn)
      { // ���� ������
        if(dataM>SpeakInSel) YesSpeakSel[i]=1;  // ��������
        else 
          if(YesSpeakSel[i] && (dataM <= GenerIn)) YesSpeakSel[i]=4; // ���������
          else 
            if(YesSpeakSel[i]) YesSpeakSel[i]--;
      }
      else YesSpeakSel[i]=0;
      OldSumADCDAC[i]=SumADCDAC[i]; // ���������� ������ ��������
      SumADCMiddle[i]=SumDACMiddle[i]=0; // ��������� ���������� 
    }
    CountMiddle=LenMiddle;
    CycleMidleMax++;
    if(CycleMidleMax==3)
    {
      CycleMidleMax=0;
      for(i=0;i<(LenSport0+LenVirt);i++)
      { // ��������� ������������ ������� �������� �� 192 ��
        MiddleMax[i] = MiddleMaxT[i];
        MiddleMaxT[i] = 0;
      }
    }
  }
}

void SendData4Record(void)
{ // �������� ������ �� ����������� �� �������, � ����� IPPM � IP-������
  unsigned char i;
  unsigned char *adr;
  xListRecord *AdrRecord;

  union
  {
	unsigned int TypeInt;
	unsigned char MassByte[4];
	unsigned short PORT;
  } TekIP;

	AdrRecord=ListRecord.pxHead;
	while(AdrRecord)
	{
#define _ AdrRecord->
	  if(_ IndBuf1>=LenSound)
	  {
	    if(!(_ block & 0x01))
	    {
		  adr = (unsigned char *)AdrRecord+4;
		  memcpy(DataSound,adr,LenHeadSound+LenSound);
		  for(i=0;i<4;i++) TekIP.MassByte[i]=_ IPRecord[i];
		  OutWriteSound(LenHeadSound+LenSound, TekIP.TypeInt, _ IPPort, (uint8 *)DataSound);
	      _ Number_1 = ReversShort(ReversShort(_ Number_2) + 1);
	    }
	    _ Timer_1  = ReversInt(ReversInt(_ Timer_1) + LenSound + LenSound);
	    _ IndBuf1 = 0;
	  }
	  else
	  {
	    if(_ IndBuf2>=LenSound)
	    {
	      if(!(_ block & 0x01))
	      {
		    adr = (unsigned char *)AdrRecord+4+LenHeadSound+LenSound;
		    memcpy(DataSound,adr,LenHeadSound+LenSound);
		    for (i=0;i<4;i++) TekIP.MassByte[i]=_ IPRecord[i];
		    OutWriteSound(LenHeadSound+LenSound, TekIP.TypeInt, _ IPPort, (uint8 *)DataSound);
	        _ Number_2 = ReversShort(ReversShort(_ Number_1) + 1);
	      }
	      _ Timer_2  = ReversInt(ReversInt(_ Timer_2) + LenSound + LenSound);
	      _ IndBuf2 = 0;
	    }
	  }
	  AdrRecord = _ pxNextRec;
#undef _
	}
}

void GetData4Play(void)
{ // ���������� ��������������� ����� �� �������
  unsigned short i,j,count;
  xListPlay *AdrPlay;
  static xListRTP TempBuf;

#define _ AdrPlay->
  if(xQueueReceive(xQueueSoundIpIn,&TempBuf,0))
  { // ����� ������ ��� ��������������������
    unsigned char TypeChan=TempBuf.ProFile & 0x00FF;
    unsigned char chan=TempBuf.ProFile >> 8;
/*{
  static char str[80];
  LockPrintf();
  sprintf(str,"$DEBUG$xQueueSoundIpIn: TempBuf.SSRC=%d, TypeChan=%d, chan=%d",
          TempBuf.SSRC,TypeChan,chan);
  UnlockPrintf();
  SendStr2IpLogger(str);
}*/
    if(TypeChan) TypeChan^=3; // ������ ��� ����� ����������� � ������ �� �������� ��� ��������
    chan=CalcRealChannel(TypeChan,chan);
    AdrPlay=ListPlay.pxHead;
    while(AdrPlay) 
    {
	      if(/*(_ SSRC == TempBuf.SSRC) && */(_ ChanPlay == chan))
	      { // �������� ������������� � ����� ������ ������� � ����������
	        count = ReversShort(TempBuf.Number) % LenPlay; // ���������� ����� ������ ������� ������
	        for (i=0;i<LenSound;i++) _ AdrBufPlay -> BufPlay[i][count] = TempBuf.BufSpeak[i]; // ������������ ������
		    if(ReversInt(TempBuf.Timer) - _ AdrBufPlay -> TimerOut > LenSound * 8)
		    { // �������������� �������� ��� �����o� ��������
		      _ AdrBufPlay -> TimerOut = ReversInt(TempBuf.Timer);
		    }
		    else
		      if(!(_ AdrBufPlay -> TimerIn)) // ������ ���������� ������?
		        _ AdrBufPlay -> TimerOut = ReversInt(TempBuf.Timer)+(LenSound/2);	// �� ������������� TimerOut
	          else _ AdrBufPlay -> TimerOut += _ TekTime;			// ����� ��������� ������� �����
	        _ TekTime = 0;											// ��������� �������� �������� �����
	        _ AdrBufPlay -> TimerIn = ReversInt(TempBuf.Timer);	// ���������� ����� 
	        if(_ AdrBufPlay -> TimerOut > _ AdrBufPlay -> TimerIn) 
	        { _ AdrBufPlay -> DeltaTimer ++; _ AdrBufPlay -> TimerOut --;}// ����������� ��������� ������
	        else
	          if(_ AdrBufPlay -> TimerOut < _ AdrBufPlay -> TimerIn) 
	          {_ AdrBufPlay -> DeltaTimer --; _ AdrBufPlay -> TimerOut ++;}
		    if(_ AdrBufPlay -> DeltaTimer > 10)
		    {_ SyncTime = true; _ AdrBufPlay -> DeltaTimer -= 1;}// �������� �� �����. �������������
		    else
		      if(_ AdrBufPlay -> DeltaTimer < -10)
		      {_ SyncTime = -1; _ AdrBufPlay -> DeltaTimer += 1;}
		      else
		        if(_ AdrBufPlay -> DeltaTimer > 0) _ AdrBufPlay -> DeltaTimer -= 1;
		        else
		          if(_ AdrBufPlay -> DeltaTimer < 0) _ AdrBufPlay -> DeltaTimer += 1;
	      } // ��������� ������ ��������� ���������
	      AdrPlay = _ pxNextPlay;
	    }
	  }
      // --------------- ���������� ������� ������� ������� ��������������� --------------------
	  AdrPlay = ListPlay.pxHead;
	  while(AdrPlay)
	  {
	    if(_ NewAudioData)
	    {
	      _ NewAudioData = false; 
	      for(i=0;i<LenSound;i++)
	      {
	        _ BufPlay[i][(_ NumBufPlay+1)%2]=_ AdrBufPlay->BufPlay[i][_ NumOutPlay];
	        _ AdrBufPlay -> BufPlay[i][_ NumOutPlay]=0xd5; // ��������� �������� ������ �� ������ a-Low
	      }
	      _ NumOutPlay = (_ NumOutPlay + 1) % LenPlay; // ��������� ����� ���������������
	    }
	    AdrPlay = _ pxNextPlay;
      }
#undef _
}

void ProcessPereboi(void)
{ // �������
  unsigned short j;
  unsigned char i,ii,btCnt,btChan;

  for(i=0;i<NumberKeyboard;i++)
  {
    j=FoundPlaceSelector(i); // ������ � TablSum
    btChan=VedSelect[i]; // ����� ������ ��������
    if((j!=0xffff) && btChan)
    {

      btCnt=TablSum[j++]; // ����� ������� ������ ������
      if(btChan>=LenHard+LenVirt) ii=btChan-LenHard-LenVirt+LenSport0;
      else ii=btChan-LenSport1;
/*{
SendStr2IpLogger("$DEBUG$PEREBOI: NKlav=%d, VedOff=%d, ii=%d, YesSpeakSel=%d",
          i,btChan,ii,YesSpeakSel[ii]);
}*/
      if(YesSpeakSel[ii]>0)
      { // ���� �������� ��� ��������� �� ������ ��������
        TimeSelect[i]=50; // ������ �� 250 ��
        for(ii=0;ii<btCnt;ii++,j++) 
        { // ��������� ��� ������ ���������, ����� ������ ��������
          if(TablSum[j]!=btChan) TablBlock[j]=0;
        }
      } 
      else 
      {
        if(TimeSelect[i])
        {
          if(TimeSelect[i]==1) 
          { // ��������������� ������� ��������������� ������� �� �� �����
            for(ii=0;ii<btCnt;ii++,j++) TablBlock[j]=CopyTablBlock[j];
          }
          TimeSelect[i]--;
        }
      }
    }
  }
}

#ifdef __ISDN__
// ------------------------------------------------------------------------------------------------------
extern volatile unsigned short InTestMKS1,OutTestMKS1,InTestMKS1_2,OutTestMKS1_2,
                               InTestMKS2,OutTestMKS2,InTestMKS2_2,OutTestMKS2_2;
unsigned int g_dwErrMKS1=0,g_dwErrMKS1_2=0,
             g_dwErrMKS2=0,g_dwErrMKS2_2=0;

void AnalizeMKS1(void)
{
  static unsigned short s_wSend2MKS1=0x77;
  static unsigned int s_dwTmMKS1=0,s_dwTmPut=0;

  if(!s_dwTmMKS1) s_dwTmMKS1=xTaskGetTickCount()+100;
  OutTestMKS1=a_expand(s_wSend2MKS1) << 3;
  if(OutTestMKS1==InTestMKS1)
  {
    s_wSend2MKS1++;
    s_wSend2MKS1&=0xff;
    s_dwTmMKS1=xTaskGetTickCount()+100;
  }
  else
  {
    if(IsCurrentTickCountGT(s_dwTmMKS1))
    {
      RestartSPORT1();
      g_dwErrMKS1++;
      if(IsCurrentTickCountGT(s_dwTmPut))
      {
        SendStr2IpLogger("$ERROR$AnalizeMKS1: Restart=%d",g_dwErrMKS1);
        s_dwTmPut=xTaskGetTickCount()+10000;
      }
    }
  }
}

void AnalizeMKS1_2(void)
{
  static unsigned short s_wSend2MKS1_2=0x77;
  static unsigned int s_dwTmMKS1_2=0,s_dwTmPut=0;

  if(!s_dwTmMKS1_2) s_dwTmMKS1_2=xTaskGetTickCount()+100;
  OutTestMKS1_2=a_expand(s_wSend2MKS1_2) << 3;
  if(OutTestMKS1_2==InTestMKS1_2)
  {
    s_wSend2MKS1_2++;
    s_wSend2MKS1_2&=0xff;
    s_dwTmMKS1_2=xTaskGetTickCount()+100;
  }
  else
  {
    if(IsCurrentTickCountGT(s_dwTmMKS1_2))
    {
      RestartSPORT1();
      g_dwErrMKS1_2++;
      if(IsCurrentTickCountGT(s_dwTmPut))
      {
        SendStr2IpLogger("$ERROR$AnalizeMKS1_2: Restart=%d",g_dwErrMKS1_2);
        s_dwTmPut=xTaskGetTickCount()+10000;
      }
    }
  }
}

void AnalizeMKS2(void)
{
  static unsigned short s_wSend2MKS2=0x77;
  static unsigned int s_dwTmMKS2=0,s_dwTmPut=0;

  if(!s_dwTmMKS2) s_dwTmMKS2=xTaskGetTickCount()+100;
  OutTestMKS2=a_expand(s_wSend2MKS2) << 3;
  if(OutTestMKS2==InTestMKS2)
  {
    s_wSend2MKS2++;
    s_wSend2MKS2&=0xff;
    s_dwTmMKS2=xTaskGetTickCount()+100;
  }
  else
  {
    if(IsCurrentTickCountGT(s_dwTmMKS2))
    {
      RestartSPORT1();
      g_dwErrMKS2++;
      if(IsCurrentTickCountGT(s_dwTmPut))
      {
        SendStr2IpLogger("$ERROR$AnalizeMKS2: Restart=%d",g_dwErrMKS2);
        s_dwTmPut=xTaskGetTickCount()+10000;
      }
    }
  }
}

void AnalizeMKS2_2(void)
{
  static unsigned short s_wSend2MKS2_2=0x77;
  static unsigned int s_dwTmMKS2_2=0,s_dwTmPut=0;

  if(!s_dwTmMKS2_2) s_dwTmMKS2_2=xTaskGetTickCount()+100;
  OutTestMKS2_2=a_expand(s_wSend2MKS2_2) << 3;
  if(OutTestMKS2_2==InTestMKS2_2)
  {
    s_wSend2MKS2_2++;
    s_wSend2MKS2_2&=0xff;
    s_dwTmMKS2_2=xTaskGetTickCount()+100;
  }
  else
  {
    if(IsCurrentTickCountGT(s_dwTmMKS2_2))
    {
      RestartSPORT1();
      g_dwErrMKS2_2++;
      if(IsCurrentTickCountGT(s_dwTmPut))
      {
        SendStr2IpLogger("$ERROR$AnalizeMKS2_2: Restart=%d",g_dwErrMKS2_2);
        s_dwTmPut=xTaskGetTickCount()+10000;
      }
    }
  }
}

void AnalizeMKS(void)
{
  static unsigned int s_dwTmStart=0;

  if(!s_dwTmStart) s_dwTmStart=xTaskGetTickCount()+20000;
  if(IsCurrentTickCountLT(s_dwTmStart)) return;

  if(ParamUKC[6] & 0x02)
  {
     AnalizeMKS1();
     if(ParamUKC[6] & 0x40) AnalizeMKS1_2();
  }
  else
  {
    if(ParamUKC[6] & 0x40) AnalizeMKS1();
  }

  if(ParamUKC[6] & 0x01)
  {
     AnalizeMKS2();
     if(ParamUKC[6] & 0x20) AnalizeMKS2_2();
  }
  else
  {
    if(ParamUKC[6] & 0x20) AnalizeMKS2();
  }
}

void ProcessISDN(void)
{  
  static unsigned char bIsdnResume=false;  
    
  if(GET_BUF)
  {
extern unsigned char E1_EDSS;
    GET_BUF=false;
    if(E1_EDSS)
    {
      unsigned int i;
      int iIrq=cli();

      i = NUM_BUF ^ 0x01; // ����� ������ � �������� �������
      sti(iIrq);

      soft_hdlc_chan_get_data(hdlc_ctrl, hw_txbuf0[i], LEN_BUF);
      soft_hdlc_chan_get_data(hdlc_ctrl+1, hw_txbuf1[i], LEN_BUF);
      soft_hdlc_chan_get_data(hdlc_ctrl+2, hw_txbuf2[i], LEN_BUF);
      soft_hdlc_chan_get_data(hdlc_ctrl+3, hw_txbuf3[i], LEN_BUF);

      soft_hdlc_chan_put_data(hdlc_ctrl, hw_rxbuf0[i], LEN_BUF);
      soft_hdlc_chan_put_data(hdlc_ctrl+1, hw_rxbuf1[i], LEN_BUF);
      soft_hdlc_chan_put_data(hdlc_ctrl+2, hw_rxbuf2[i], LEN_BUF);
      soft_hdlc_chan_put_data(hdlc_ctrl+3, hw_rxbuf3[i], LEN_BUF);

      bIsdnResume=true;
      AnalizeMKS();
    }
  }
  if(bIsdnResume && xIsTaskSuspend(thISDN))
  {
    bIsdnResume=false;
    vTaskResume(thISDN); // ����� ����� ISDN
  }
}
#endif

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
void vSportTask(void *pvParameters)
{ // ������� ������������ ������ �� SPORT (5 ����)
  static unsigned int i,dwWait,dwIrq;
  portTickType dwTm;
  static sInfoKadr sInfo;

#ifdef __ISDN__
  { // -------------- ������������� ISDN -------------------------------
    unsigned char btMKS,
                  MasMaxIsdnChannels[NUM_TRUNKS]={MAX_CHANNELS,MAX_CHANNELS,16,16};

    soft_hdlc_init();
    for(i=0;i<NUM_TRUNKS;i++) soft_hdlc_chan_init(hdlc_ctrl+i,0);
 
    // ���������� ������� �������
    soft_hdlc_chan_get_data(hdlc_ctrl, hw_txbuf0[0], LEN_BUF);
    soft_hdlc_chan_get_data(hdlc_ctrl+1, hw_txbuf1[0], LEN_BUF);
    soft_hdlc_chan_get_data(hdlc_ctrl+2, hw_txbuf2[0], LEN_BUF);
    soft_hdlc_chan_get_data(hdlc_ctrl+3, hw_txbuf3[0], LEN_BUF);
    // -----------------------------------------------------------------
    btMKS=ParamUKC[6];
    if(ParamUKC[5]) btMKS&=~0x42;
    if(btMKS & 0x2)
    { // ���1.1 � ���1.2
      MasMaxIsdnChannels[0]=16;
      if(btMKS & 0x40) MasMaxIsdnChannels[2]=16;
    }
    if(btMKS & 0x1)
    { // ���2.1 � ���2.2
       MasMaxIsdnChannels[1]=16;
      if(btMKS & 0x20) MasMaxIsdnChannels[3]=16;
    }
    pri_chan_start(0,MasMaxIsdnChannels);
    vTaskDelay(200); // �������� ��� ������� ������ EDSS
  }
#endif

  // �������� ������ �� ��������� ������ � �������� �������
  if(xTaskCreate(vWaitSportTask,"WaitSportX",STACK_WaitSportX,NULL,tskIDLE_PRIORITY+2,thSportXCmd)!=pdPASS)
  { BlockWatchDog(); }

  StartSPORTs();
  TimeM=0;
  dwTm=xTaskGetTickCount();
  *pIMASK |= 0x0100; // ���������� ������������ ���������� 8
  while(1) 
  { // ����������� ���� ���������� ������
    vPortEnableSwitchTask(false);
/*{ // ��� ������� ������� ������� ���������� ������
    dwIrq=cli();
    Count5mc=0;
    sti(dwIrq);
}*/
    TestBoardMKA();
    AnalizeGenerator();
    AnalizeLevel();
    AnalizeTDC();
    AnalizeTDCL();
#ifndef SINFO_ENERGO
    Analize_PPSR_RSDT_DTMF_ETC();
#endif
#ifdef __ISDN__
    ProcessISDN();
#endif
    ProcessPereboi();
    SendData4Record();
    GetData4Play();

    if(xQueueReceive(xQueueReply[UserSportX],&sInfo,0)) // ������ �� ������� �������
    { // ����� �� ������� 7 - ������� 8
      if(sInfo.ByteInfoKadr[0] == 8)
      { // ������� ����� 8-� �������
        sInfo.ByteInfoKadr[0] = 0; // ��� ����� ��� ������ �������� ��� OutAbon
        if(sInfo.ByteInfoKadr[2]!=24) OutAbon(sInfo.ByteInfoKadr,sInfo.L);
        else
        {
          if(sInfo.ByteInfoKadr[3]<3) OutSNMP(sInfo.ByteInfoKadr[1]/4+1,0xFF,sInfo.ByteInfoKadr[3]+4);
          else OutSNMP(sInfo.ByteInfoKadr[1]/4+1,0xFF,sInfo.ByteInfoKadr[3]*2+1);
        }
      }
    }
    SetWDState(d_WDSTATE_SportX);
    { // ������ ������� ���������� ������ ������ � vTaskDelayUntil
      dwIrq=cli();
      dwWait=Count5mc;
      Count5mc=0;
      sti(dwIrq);
      if(TimeM<(dwWait >> 3)) TimeM=dwWait >> 3;
    }
    vTaskDelayUntil(&dwTm,5); // vPortEnableSwitchTask(true) ���������� ������!
  }
}

