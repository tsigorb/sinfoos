#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FuncSportXCmds.h"

///////////////////////////////////////////////////////////////////////////////
unsigned short GetOperatorChannel(unsigned char SN,unsigned char btDS_RS);

///////////////////////////////////////////////////////////////////////////////
extern xQueueHandle xQueueCommand;
extern xQueueHandle xQueueReply[];
extern xQueueHandle xQueueSportX;
extern unsigned short TimeWait125; // ����� �������� ���������� ��������� IntSportX
extern IPPARAMS_TP g_IpParams;

extern unsigned char TablMKC[LenMKC][LenCommutMKC]; // ������� ���������� ������� ���
extern unsigned short TablSport1[LenSport1]; // ������� ���������� ������� SPORT1

extern unsigned char TablSum[LenSumChan]; // ������� ������� ����������� �� ������� ���� ���������� (�������������)
extern unsigned char TablBlock[]; // ������� ������� ����������� �� �����
extern unsigned char CopyTablBlock[]; // ����� ������� ������� ����������� �� �����
extern GROUPINFO_TP MasGroupInfo[MaxGroup];

extern unsigned short MiddleMax[LenSport0+LenVirt]; // ������������ ������� �������� �� ���������� ������� � ������ ������������

extern char AnalTDC[]; // ���������� ����� ������ ������������� 2-� ���������� ������� �������� 
extern char AnalPCDT[]; // ���������� ����� ������ ����������������� 2-� ���������� ������� �������� 

extern int TimeM; // ������ ������� ��������� SportX
extern int TimeW; // ������ ������� ��������� SportXWait

extern unsigned char CountYesMiddle[LenVirt]; // ������� ������������ �� ������
extern unsigned char PorogMiddle[LenVirt]; // ����� �������� �������� �� ������ ������������
extern unsigned short ReversA[LenSport0]; // ������� �������� �� ���������� �������
extern unsigned short Revers[LenVirt]; // ������� �������� �� ������ ������������
extern unsigned char YesPorogMiddle[LenVirt]; // ���������� ������� �������� �������� �� ������ ������������ (==0-������ �������)

extern char Extend[]; // ��������� ���������� ����� �����
extern unsigned char SCall[]; // ��� ����� �����������
extern unsigned char SpecGen[]; // ���������� ����� ����� �����������

extern unsigned char OldComm; // ���������� �������� �������� ����������

extern unsigned char ChanFreq; // ����� ������, �� �������� ���������� ������� �������
extern short MeazFreq; // ��������� ������� �� ������
extern short TimeFreq; // ����� ��������� ������� �� ������

extern unsigned short PeriodTikker;				// ������ �������

extern unsigned char VirtualPoints[LenVirt];    // ������� ������� ����� �����������

extern unsigned short ParamUKC[];				// ��������� �� 

extern unsigned int	 Time8Hold;					// ����� ��������� ���������� 8 ��� � �������� SCLK=49.152 ��� (20,356 ��)

extern unsigned char ChanMiddle; // ����� ��, �� ������� ���������� ������� ��������
extern unsigned short BigCountMiddle;
extern unsigned short SumDataChanMiddle; // ������� �������� �� ��

extern unsigned char	NextPlay;				// ������� �������� ���������� ������
extern unsigned char	ChanPlay;				// ����� ������ ���������������
extern unsigned char	NumBufPlay;				// ������� ������ �������� ������ ���������������
extern unsigned char	IndBufPlay;				// ������ ������ ���������������
extern signed short		BufPlay[LenSound][2];	// ����� ��������������� �� ������

extern const unsigned char Comb_TDC[];			// ������ ������ ��������� ���
extern unsigned short	SumDAC[LenSport0+LenVirt]; // ������� �������� �� ���������
extern 					time_t Time_Sys; //���� ���������� ������� � ���;
extern 					time_t ExistTime;

extern unsigned short LenSNPult; // ����� �������� ������� �� ��� �������
extern unsigned short LenLicPult; // ����� �������� �������
extern unsigned short LenSNSound; // ����� �������� ������� �� ��� �����������
extern unsigned short LenLicSound; // ����� �������� �����������
extern unsigned short LenSNIPPM; // ����� �������� ������� �� ��� IPPM
extern unsigned short LenLicIPPM; // ����� �������� IPPM

extern short CountErrQueueReply;
extern unsigned char FailureE1[4][10]; // ��������� ��������� ������ ������� E1

extern unsigned char ARUTP[]; // ��������� ���

extern sListAnalTDCF ListAnalTDCF; // ��������� ����������� �������� TDC
extern sListAnalTDCL ListAnalTDCL; // ��������� ����������� �������� TDC
extern sListAnalDTMF ListAnalDTMF; // ��������� ����������� �������� DTMF
extern sListAnalPPSR ListAnalPPSR; // ��������� ����������� �������� PPSR
extern sListAnalPCDT ListAnalPCDT; // ��������� ����������� �������� PCDT
extern sListRecord  ListRecord; // ��������� ������ �����������
extern sListPlay ListPlay; // ��������� ������ ��������������������

extern unsigned short SpeakInSel; // ����� ����������� ��������� ������ �������� ���������

extern char StartTestMKC;

extern unsigned char FilePOS[],FileREC[];

extern unsigned int FTik; // ������� �������

///////////////////////////////////////////////////////////////////////////////
static sInfoKadr sInfoAnswer;

unsigned char InCommand=0;
unsigned char USER;   // ������� ������������ ���������� ������� (0xFF-�����������)
unsigned int A1;      // ����� ����������
unsigned short PORT1; // ���� ����������
unsigned char LEN1;   // ����� �������

unsigned char MassInData[LenInOutData]; // ������ ��� ������   ������ ��� ������
unsigned char MassOutData[LenInOutData]; // ������ ��� �������� ������ ��� ������

unsigned char StartCorrTim=0; // ������� ������ ���������� ��������� �����
int DeltaTime=0; // ������ ����� �� 1 ���
unsigned int MaxDeltaTime=0; // ������ ����� �� 2 ���
unsigned int MinDeltaTime=60; // ������ ����� �� 2 ���
long long int SystemTime,ClockTime;
unsigned long TimeSet;

unsigned char MaxARUTP[LenVirt]; // �������� ���

unsigned char ChanSwing=0; // ����� ��� ��������� ��������� �������

unsigned short InclCall[4]; // ������� ���/���� ������

unsigned char E1_EDSS=0; // ��������� ����� ������ EDSS

unsigned short ResetWait125; // �������
unsigned int ReStart=0,ReStart1=0;

section("bigdata") unsigned short DataSave[LenDataSave]; // �������
section("bigdata") unsigned short DataSave1[LenDataSave]; // �������

///////////////////////////////////////////////////////////////////////////////
void OutAbon(unsigned char *Abon, unsigned char Len)
{ // ����� ������ � ������� ��������� ������
  section("bigdata") static sInfoKadr sVxCallT; // ��������� ������ � �������� ������

  vPortEnableSwitchTask(false);
  // ----- �������� �� �� ��� ���� ���������� ������, �� �� ��������
  if((*Abon==1) || // ��� ������=����� ����������� ��� ���������� ����� (==0) ��� �������� (==2)
     (((TestBoard >> ((*(Abon+1)/4)*2)) & 0x3)==0x2)) // 2 ���� ����������������� ���������� ����� (��������������� �����==0x2)
  { // �������� ������
    sVxCallT.L=Len+1;
    sVxCallT.ByteInfoKadr[0]=2;
    memcpy(sVxCallT.ByteInfoKadr+1,Abon,Len);
    sVxCallT.A1=A1;
    sVxCallT.PORT1=PORT1;
    xQueueSend(xQueueVxCall,&sVxCallT,0,0);
    SendDiagnostics2DspCom(sVxCallT.ByteInfoKadr,sVxCallT.L);
  }
  vPortEnableSwitchTask(true);
}

unsigned char Answer(sInfoKadr *psInfo,unsigned char command, unsigned char Len)
{ // ����� �� �������
  if((USER<QueueReplyCnt) && (USER!=UserSportX))
  {
    unsigned int count;

    psInfo->Source=USER;
    psInfo->L=Len+2;
    psInfo->A1=A1;
    psInfo->PORT1=PORT1;
    psInfo->ByteInfoKadr[0] = 11;
    psInfo->ByteInfoKadr[1] = command;
    if((command==110) && (USER==UserRS485) && (MassOutData[0]!=1)) MassOutData[0]=0; // ����� ������ ��� ���������� ��� ���������
    for(count=2;count<Len+2;count++) psInfo->ByteInfoKadr[count]=MassOutData[count-2];
    if(xQueueSend(xQueueReply[USER],psInfo,0,0)==errQUEUE_FULL) // ���������� � ������� �������
    { // ������� �����������
      CountErrQueueReply++;
      return false;
    }
    return true;
  }
  return false;
}

unsigned char Answer15(sInfoKadr *psInfo)
{ // ����� �� ������� 15 �� ����� �����������
  unsigned char data,chan,TypeChan;
  unsigned short test, i, j;

  TypeChan=MassInData[1];

  psInfo->Source=USER;
  psInfo->L=17;
  psInfo->A1=A1;
  psInfo->PORT1=PORT1;
  psInfo->ByteInfoKadr[0]=16; // ��� ������ �� ������� 15
  psInfo->ByteInfoKadr[1]=TypeChan; // ��� ������
  psInfo->ByteInfoKadr[2]=MassInData[2]; // ����� ������
  psInfo->ByteInfoKadr[3]=3; // ������� ����������� �������
  psInfo->ByteInfoKadr[4]=6; // �������� ����������� �������
  psInfo->ByteInfoKadr[5]=0; // ������ �������� �� ������� 3000��
  psInfo->ByteInfoKadr[6]=0; // ������

  data=0; // ����������� �������� ��������� ��������� ������
  if(FoundGenerator(TypeChan,MassInData[2])) data |= 0x4; // ���� ���������
    
  if(ARUTP[MassInData[2]]==2) data |= 0x80; // �������� �� �������� ���
  psInfo->ByteInfoKadr[7]=data; // �������� ��������� ��������� ������

/*{
  section("bigdata") static char str[80];
  LockPrintf();
  sprintf(str,"$DEBUG$Cmd15: tp=%d, ch=%d, data=%x",
          TypeChan,MassInData[2],data);
  UnlockPrintf();
  SendStr2IpLogger(str);
}*/

  chan=CalcRealChannel(TypeChan,MassInData[2]);

  // ����������� ���� ��������� ������ (���� 1)
  data=0;
  {
    xListAnalTDCF *AdrInTDC=ListAnalTDCF.pxHead;
    while(AdrInTDC)
    {
      if(AdrInTDC->chan == chan)
      { data |= 2; break; }
      AdrInTDC=AdrInTDC->pxNextChan;
    }
  }
  {
    xListAnalDTMF *AdrInDTMF=ListAnalDTMF.pxHead;
    while(AdrInDTMF)
    {
      if(AdrInDTMF->chan == chan)
      { data |= 8; break; }
      AdrInDTMF=AdrInDTMF->pxNextChan;
    }
  }
  if(YesPorogMiddle[MassInData[2]]) data |= 16; // �� ������
  psInfo->ByteInfoKadr[8]=data;

  // ����������� ���� ��������� ������ (���� 2)
  data=0;
  {
    xListAnalTDCL *AdrInTDCL=ListAnalTDCL.pxHead;
    while(AdrInTDCL)
    {
      if(AdrInTDCL->chan == chan)
      { data |= 8; break; }
      AdrInTDCL->pxNextChan;
    }
  }
  {
    xListAnalPCDT *AdrInPCDT=ListAnalPCDT.pxHead;
    while(AdrInPCDT)
    {
      if(AdrInPCDT->chan == chan)
      { data |= 16; break; }
      AdrInPCDT=AdrInPCDT->pxNextChan;
    }
  }
  {
    xListAnalPPSR *AdrInPPSR=ListAnalPPSR.pxHead;
    while (AdrInPPSR)
    {
      if(AdrInPPSR->chan == chan)
      { data |= 32; break; }
      AdrInPPSR=AdrInPPSR->pxNextChan;
    }
  }
//  if(TypeChan)
  {
    test=MassInData[2] % 16; // ������ ������, ����������� ���� � ����� � j
    for(i=1,j=1;i<=test;i++) j<<=1;
    i=InclCall[MassInData[2]/16]; // ������ �����
    if (i & j) data |= 64;
    psInfo->ByteInfoKadr[9]=data;
    psInfo->ByteInfoKadr[10]=PorogMiddle[MassInData[2]]; // ������� ������� �� ������
    psInfo->ByteInfoKadr[11]=Revers[MassInData[2]] / CoefReturnCh; // ������� ��������
    psInfo->ByteInfoKadr[12]=MaxARUTP[MassInData[2]]; // ���-���
  }
/*  else
  {
    psInfo->ByteInfoKadr[9]=data;
    psInfo->ByteInfoKadr[10]=0; // ������� ������� �� ������
    psInfo->ByteInfoKadr[11]=ReversA[MassInData[2]] / CoefReturnCh; // ������� ��������
    psInfo->ByteInfoKadr[12]=0;
  }*/
  psInfo->ByteInfoKadr[13]=0; // ����� ��������������
  psInfo->ByteInfoKadr[14]=0; // ������
  psInfo->ByteInfoKadr[15]=0; // ����� ���������
  psInfo->ByteInfoKadr[16]=0; // ����. ������� �������������
  if(xQueueSend(xQueueReply[USER],psInfo,0,0)==errQUEUE_FULL)
  { // ������� �����������
    CountErrQueueReply++;
    return false;
  }
  return true;
}

void Answer50(sInfoKadr *psInfo) 
{ // ����� �� ������� 50 �� ����� �����������
  if((USER<QueueReplyCnt) && (USER!=UserSportX))
  {
    unsigned short Lvl;
    psInfo->Source=USER;
    psInfo->L=6;
    psInfo->A1=A1;
    psInfo->PORT1=PORT1;
    psInfo->ByteInfoKadr[0]=51; // ��� ������ �� ������� 50
    psInfo->ByteInfoKadr[1]=1; // ��� ������
    psInfo->ByteInfoKadr[2]=MassInData[2]; // ����� ������
    psInfo->ByteInfoKadr[3]=0;
    if(SumDataChanMiddle & 0x8000) Lvl=SumDataChanMiddle & 0x7fff; // ������� �������� ��������
    else Lvl=0;
    psInfo->ByteInfoKadr[4]=Lvl >> 8;
    psInfo->ByteInfoKadr[5]=Lvl;
/*{
extern short DataMin,DataMax;
  section("bigdata") static char str[80];
  LockPrintf();
  sprintf(str,"$DEBUG$Cmd50: ch=%d, chan=%d, Lvl=%d, DataMin=%d, DataMax=%d",
          MassInData[2],ChanMiddle,Lvl,DataMin,DataMax);
  UnlockPrintf();
  SendStr2IpLogger(str);
}*/
    if(xQueueSend(xQueueReply[USER],psInfo,0,0)==errQUEUE_FULL) CountErrQueueReply++;
  }
}

bool SendReplyXCmd(unsigned char codeCommand,unsigned char Result)
{
  section("bigdata") static sInfoKadr sInfoS1;

  sInfoS1.Source=USER;
  sInfoS1.A1=A1;
  sInfoS1.PORT1=PORT1;
  sInfoS1.L=2;
  sInfoS1.ByteInfoKadr[0]=codeCommand;
  sInfoS1.ByteInfoKadr[1]=Result;
  if(xQueueSend(xQueueReply[sInfoS1.Source],&sInfoS1,1,0)==errQUEUE_FULL)
  { CountErrQueueReply++; return false; }
  return true;
}

void ReWrFlash(unsigned short  DanFl,unsigned short  AdrFl)
{ // ������ ������ �� flash, ���� ��� �� ���������
  if(DanFl!=RdFlash(AdrFl)) WrFlash(DanFl,AdrFl);
}

unsigned char TestMKC(unsigned char adr,unsigned char ans)
{ // ����� ������ ��� �������� �� 13 !!
  switch(adr)
  {
	case 0:
	  if(ParamUKC[0] & 0x1000) return true;
	  break;
	case 1:
	  if(ParamUKC[0] & 0x2000) return true;
	  break;
	case 2:
	  if(ParamUKC[0] & 0x4000) return true;
	  break;
	case 3:
	  if(ParamUKC[0] & 0x8000) return true;
	  break;
  }
  if(ans)
  {
    MassOutData[0]=0xFF;
    Answer(&sInfoAnswer,MassInData[1],1);
  }
  return false;
}

unsigned char SetParamPoint(unsigned char *MassData)
{ // ��������� ���������� �������� �� ����� ����������� (4 �����)
  unsigned char bRes, chan, test;
  unsigned short i,j;

// 1.1-���, 1.2-���, 1.4-��������, 1.8-����, 1.16-�� ������, 1.32-���/�������, 1.64-��������, 1.128-�����������
// 2.1-��, 2.2-����������(1600��), 2.4-48/96�, 2.8-�������� ���, 2.16-�������� ����, 2.32-����, 2.64-���/���� ������
 bRes=true;
 chan=MassData[2];
 if(MassData[1]==0) chan += LenSport1;
 else
   if(MassData[1]==1) chan += LenHard;

 if(MassData[3] & 2)
 { // ��������� ������� ������� ���
   test=InitAnalTDCFull(chan); // ==1-��� ����, ==0xFF-��, ==0 ��� ����� ��� �������. �������
   if (!test) bRes=false;
 }
 else ExitAnalTDC(chan);

 if(MassData[3] & 8)
 { // ��������� ������� ����
  test=InitAnalDTMF(chan); // ==1-��� ����, ==0xFF-��, ==0 ��� ����� ��� �������. �������
  if(!test) bRes=false;
  
 }
 else ExitAnalDTMF(chan);

 if(MassData[3] & 16) YesPorogMiddle[MassData[2]]=true; // ��������� ������� �� ������
 else YesPorogMiddle[MassData[2]]=false; // ���������� �������

 if(MassData[4] & 8)
 { // ��������� ������� ��������� ���
   test=InitAnalTDCSmall(chan); // ==1-��� ����, ==0xFF-��, ==0 ��� ����� ��� �������. �������
   if(!test) bRes=false;
 }
 else ExitAnalTDCL(chan);

 if(MassData[4] & 16)
 { // ��������� ������� ��������� PCDT
   test=InitAnalPCDT(chan); // ==1-��� ����, ==0xFF-��, ==0 ��� ����� ��� �������. �������
   if(!test) bRes=false;
 }
 else ExitAnalPCDT(chan);

 if(MassData[4] & 32)
 {// ��������� ������� ����
   test=InitAnalPPSR(chan); // ==1-��� ����, ==0xFF-��, ==0 ��� ����� ��� �������. �������
   if(!test) bRes=false;
 }
 else ExitAnalPPSR(chan);

 test=MassData[2] % 16; // ��������� ������, ����������� ���� � ����� � j
 for (i=1,j=1;i<=test;i++) j<<=1;
 i=InclCall[MassData[2]/16];
 if(MassData[4] & 64) i=i | j;
 else i=i & (~j);
 InclCall[MassData[2]/16]=i;
 return bRes;
}

void SendBlockPPS(unsigned char chan, unsigned char type)
{ // ������� ������� ����������(==1) / �������������(==0) �������� ���-�3
  section("bigdata") static sCommand OutCom; // ��������� ��� �������� �������

  if(!chan || (chan>MAX_MKA_CHANNELS)) return;
  OutCom.Source=UserSportX;			// �������� ������� SportX
  OutCom.L=3; 					// ���������� ���� � �������
  OutCom.A1=A1;
  OutCom.PORT1=PORT1;
  OutCom.BufCommand[0]=55;			// �������
  OutCom.BufCommand[1]=chan;		// N �����������
  OutCom.BufCommand[2]=type;		// ����������=1 / �������������=0
  xQueueSend(xQueueCommand,&OutCom,0,0);
}

void SendClearCommut(void)
{ // ���������� ������� ������� ������ ���������� � RS485
  section("bigdata") static sCommand sCmd; // ��������� ��� �������� �������

  sCmd.Source=UserSportX;
  sCmd.L=2;
  sCmd.A1=A1;
  sCmd.PORT1=PORT1;
  sCmd.BufCommand[0]=113; // �������
  sCmd.BufCommand[1]=0xff; // ��� ������
  xQueueSend(xQueueCommand,&sCmd,0,0);
}

void CreateConstCommut(bool bSave2Flash)
{
  { // ����������� � �������� ��������� ���������� ������������� ������� � ���������� ��� �������� ���������  
    unsigned char i,count,code,code1;

    count=((MassInData[14]-'0')*100)+((MassInData[15]-'0')*10)+MassInData[16]-'0'; // ����� �������
    i=18; // 1-� ����� ������ ���� ������� ���������
    code=((MassInData[i+1]-'0') * 10)+(MassInData[i+2]-'0');
    if (MassInData[i]=='0') code+=LenSport1-1;
    else code+=LenHard+LenVirt-1;
    i+=4;
    count--;
    while(count)
    {
      code1=((MassInData[i+1]-'0')*10)+(MassInData[i+2]-'0');
      if(MassInData[i]=='0') code1+=LenSport1-1;
      else code1+=LenHard+LenVirt-1;
      ExclTempComm(code,code1);
      count--;
      i+=4;
    }
  }
  MassOutData[0]=SaveGroupFlash(MassInData,bSave2Flash,0);
}

void SendCommandMKA(unsigned char command, unsigned char data)
{ // �������� ������������ ������� �� ���
  section("bigdata") static sCommand sCmd; // ��������� ��� �������� �������

  sCmd.Source=UserSportX;
  sCmd.L=2;
  sCmd.A1=A1;
  sCmd.PORT1=PORT1;
  sCmd.BufCommand[0]=command;
  sCmd.BufCommand[1]=data;
  xQueueSend(xQueueCommand,&sCmd,0,0);
}

bool RewriteFile(char *FName,unsigned char *Buf,int len)
{
  MYFILE stFile;

  HANDLE hFile=CreateFile(FName,CREATE_ALWAYS,len);
  if(hFile>=0)
  {
    WriteFile(hFile,Buf,len);
    CloseHandle(hFile);
    return true;
  }
  return false;
}

void ReinitConnectionPoint(void)
{ // ����������������� ������� ���������� � ����� �����������
  unsigned short i, j;
  char *Extend; // ��������� ���������� ����� �����
  MYFILE stFile;

  vPortEnableSwitchTask(false);
  for(i=0;i<LenVirt;i++) Revers[i]=0; // ������� "���������" �� ���� ��
  ClearTablCommut(); // ������� ������ ����������
  vPortEnableSwitchTask(true);

  InitMKC(0);
  InitMKC(1);
  InitMKC(2);
  InitMKC(3);
  
#ifdef __ISDN__
  {
extern unsigned short E1KI[LenVirt];       // N ������*256 + ��
extern unsigned short CallerID[LenVirt];   // owner
extern unsigned short ChannelOperator[LenVirt];  // ��� � ����� ������ ���������
extern unsigned char  NTelefon[LenVirt][LenTlfNumber+1];  // ������ ������� ��������� �� ������ �����������
extern unsigned char  NameTelefon[LenVirt][LenTlfName+1]; // ������ ���� �������� ��������� �� ������ ����������
extern unsigned char  TypeCall[LenVirt];   // ��� ������� ������ (��������, ������)
extern unsigned char  SelectEDSS[LenVirt]; // ������� ��������� (������ ������ � ������� ����������+1) ��� ��� (0)

    for(i=0;i<LenVirt;i++)
    {
      for(j=0;j<=LenTlfNumber;j++) NTelefon[i][j]=0;
      for(j=0;j<=LenTlfName;j++) NameTelefon[i][j]=0;
      E1KI[i]=CallerID[i]=ChannelOperator[i]=TypeCall[i]=SelectEDSS[i]=0;
    }
  }
  InitISDN4MKC(); // ������� ������������� ISDN �� ���
#endif

  // ����� �������������� ���������� ����������
  i=0;
  Extend=SetVarComm();
  while(FindNextFile(i,&stFile)!=(unsigned long)-1)
  {
    i++;
    if(FindInitFile(&stFile,Extend)) RestartCommut(&stFile);
  }
  if(ParamUKC[3]!=OldComm) OldComm=ParamUKC[3];
}

bool IsMKCCommand(unsigned char cmd4MKC)
{
  if((cmd4MKC>=102) && (cmd4MKC<=107)) return true;
  if((cmd4MKC>=110) && (cmd4MKC<=112))return true;
  if(cmd4MKC==117) return true;
  return false;
}

void GetIncomingCallInfoMKA(void)
{ // ������ ���� � �������� ������: ���� 0..6 - ���� ����� � ������, ��� 7 - ������ ����� �� ������� 200
void ClearMKAUartState(unsigned char btMKA);
extern unsigned char MassCallMKA[LenMKA];  // ������ �������� ������� �� ���

  unsigned char btCall;
  static unsigned char s_btMKA=0;
  section("bigdata") static sCommand sCmd;

  unsigned int dwMask=cli();
  btCall=MassCallMKA[s_btMKA];
  MassCallMKA[s_btMKA]=0;
  sti(dwMask);

  if(btCall & 0x80)
  { // ������ ����� �� ������� 200
DataSave1[8]++;
    ClearMKAUartState(s_btMKA);
  }
  btCall&=0x7f;
  if(btCall)
  { // ������� ������� 7 ������ ������ � ������ ����������� ��������
/*{
  section("bigdata") static char str[80];
  LockPrintf();
  sprintf(str,"$DEBUG$GetIncomingCall: channel #%d",btCall);
  UnlockPrintf();
  SendStr2IpLogger(str);
}*/
    if(btCall<=MAX_MKA_CHANNELS)
    {
      sCmd.Source = UserSportX; // �������� ������� SportX
      sCmd.L = 2; // ���������� ���� � �������
      sCmd.A1 = A1;
      sCmd.PORT1 = PORT1;
      sCmd.BufCommand[0] = 7; // �������
      sCmd.BufCommand[1] = btCall-1; // N ������ � 0
      xQueueSend(xQueueCommand,&sCmd,0,0);
    }
  }
  s_btMKA++;
  if(s_btMKA>=LenMKA) s_btMKA=0;
}

// ------------------------------------------------------------------------------------------------------
// ------------------------ ��������� ��������� ������ SportX -------------------------------------------
// ------------------------------------------------------------------------------------------------------
void CommandSportX(void)
{
  unsigned short data,chan,i,j;
  char *FName;
  static unsigned char InCommandTOut=0;
  static sInfoKadr sInfo;
  static sCommand sCmd;

  GetIncomingCallInfoMKA(); // ����������� ����������� �������� ���
  PeriodicalTestMKC();
  j=InCommand;
  i=ProcessBoardMKC(MassOutData,&InCommand);
  if(i) Answer(&sInfoAnswer,j,i);
  if(InCommand)
  {
    if(InCommandTOut) InCommandTOut--;
    else InCommand=0;
  }
  else
  {
    if(xQueueReceive(xQueueSportX,&sCmd,0))
    {
	  USER=sCmd.Source;
	  A1=sCmd.A1;
	  PORT1=sCmd.PORT1;
	  LEN1=sCmd.L;
	  memcpy(MassInData,sCmd.BufCommand,LEN1);
	  if(MassInData[0]==10)
	  {
	    InCommand=MassInData[1]; // ���������� ���� �������
	    InCommandTOut=30; // ���� �� 30*10=300 ����
		MassOutData[0]=false; // ����� ���� �������������
		sInfoAnswer.L=0;
		switch(InCommand)
		{
			case 100: // ������ ������� ��������� �� 8 ���
			    {
			      unsigned int dwIrq=cli();
			      MassOutData[0]=(unsigned char)(Time8Hold>>8); 
			      MassOutData[1]=(unsigned char)Time8Hold;
			      Time8Hold=0;
			      sti(dwIrq);
			    }
				Answer(&sInfoAnswer,100,2);
				break;
		 	case 101: // ����� ������� ������������ ������
		 	    {
		 	      unsigned int test=_space_unused();
				  MassOutData[0]=(unsigned char)(test >> 24);
				  MassOutData[1]=(unsigned char)(test >> 16);
				  MassOutData[2]=(unsigned char)(test >> 8);
				  MassOutData[3]=(unsigned char)(test);
				  test=(unsigned int)pvPortMalloc(1000);
				  vPortFree((void *)test);
                  MassOutData[4]=(unsigned char)(test >> 24);
				  MassOutData[5]=(unsigned char)(test >> 16);
				  MassOutData[6]=(unsigned char)(test >> 8);
				  MassOutData[7]=(unsigned char)(test);
		 	    }
				Answer(&sInfoAnswer,101,8);
				break;
			case 102: // �������� ������� ������ �� E1
					i=MassInData[2]-13;
					if(TestMKC(i,true)) WriteE1(i,MassInData[3],MassInData[4]);
					// ��� ������->������ (����� ��� ��������� � ������)
					break;
			case 103: // �������� ������� ������ � E1
					i=MassInData[2]-13;
					if(TestMKC(i,true)) ReadE1(i,MassInData[3]);
					// ��� ������->������
					break;
		 	case 104: // ������������� ��� 
					i=MassInData[2]-13;
					if(TestMKC(i,true))
					{ // ������ ��������
					  ReWrFlash(MassInData[3]+(MassInData[4]<<8),AdrParamMKC+ParamUKC[3]*4*8+i*8+0);
					  ReWrFlash(MassInData[5]+(MassInData[6]<<8),AdrParamMKC+ParamUKC[3]*4*8+i*8+2);
					  ReWrFlash(MassInData[7],AdrParamMKC+ParamUKC[3]*4*8+i*8+4);
					  InitMKC(i);
		 			  MassOutData[0]=true;
	 				  Answer(&sInfoAnswer,MassInData[1],2);
	 				}
	 				break;
			case 105: // �������� ������� ������ ������ � �������� C0...C3 E1
					j=MassInData[2]-13;
					if(TestMKC(j,true))
					{ // ������ ��������
					  i=RdFlash(AdrParamMKC+ParamUKC[3]*4*8+(MassInData[2]-13)*8+((MassInData[3]-1)/2)*2);
					  if (MassInData[3] & 1) i=(i & 0xFF00) | MassInData[4]; 
					  else i=(i & 0x00FF) | (MassInData[4] << 8); 
					  ReWrFlash(i,AdrParamMKC+ParamUKC[3]*4*8+(MassInData[2]-13)*8+((MassInData[3]-1)/2)*2);
					  WriteE1(j,MassInData[3]+0xC0-1,MassInData[4]);
					}
					// ��� ������->������ (����� ��� ��������� � ������)
					break;
			case 106: // �������� ������� ������ ������ � ������� C4, ����� CD E1
					i=MassInData[2]-13;
					if(TestMKC(i,true))
					{ // ������ ��������
					  ReWrFlash(MassInData[3],AdrParamMKC+ParamUKC[3]*4*8+i*8+4); 
					  WriteE1(i,0xC4,MassInData[3]);
					  WriteE1(i,0xCD,0);
					  // ��� ������->������ (����� ��� ��������� � ������)
					}
					break;
			case 107: // �������� ������� ������ ��������� C0...C4,C5,C7,C9,CB,C6,C8,CA,CC,CD ������ E1
					i=MassInData[2]-13;
					if(i<4)
					{
					  if(TestMKC(i,false))
					  { // �������� �� ��, ��� ������ ��������
                        if(FailureE1[i][9]==0x01)
                        { // ����� ��� ������������ ������
                          MassOutData[0]=MassOutData[1]=0;
                          Answer(&sInfoAnswer,107,2);
                        }
                        else ReadE1(i+107,0); // �������� ����������� ������� ������ ��������� ������
					  }
					  else
					  {
					    MassOutData[0]=1;
					    for(i=1;i<7;i++) MassOutData[i]=RdFlash(AdrParamMKC+ParamUKC[3]*4*8+(MassInData[2]-13)*8+i-1);
					    Answer(&sInfoAnswer,107,9);
					  }
					}
					break;
			case 108: // �������� ������ ���������� ��
			    {
			        unsigned int test;
					MassOutData[0]=ParamUKC[0]; // ������� ���������� ������� ��� � M�� ������� 2 �����
					MassOutData[1]=ParamUKC[0] >> 8;
					MassOutData[2]=ParamUKC[1];	// ������������ ���������
					MassOutData[3]=ParamUKC[2];	// ������������ SNMP
#ifdef __ISDN__
					MassOutData[4]=ParamUKC[4];		// ��������� ����� ������ ISDN (�� �.�. == 0 ��� ISDN)
					MassOutData[5]=ParamUKC[5];		// ����� ������� ISDN (1 ��� 2)
					MassOutData[6]=ParamUKC[6] & 0x7F;// ��� �� ��� ISDN �������:
//-������ ������	if ((!(ParamUKC[0]&0xF000)) && (ParamUKC[6] & 0x60)) MassOutData[1] |= 0x10; // �������� ���1 ��� ������� ���
 // 0x40-������� ������ ��� �� 1-� ������
 // 0x20-������� ������ ��� �� 2-� ������
 // 0x10-�������������� ����������� �� ��������� ���������, 0x08-�� ������������ ?,
 // 0x04 ����� ������ �� ��. ����� ������ ���, 0x02 �������� ��, 0x01 ������.
#else
					MassOutData[4]=MassOutData[5]=ParamUKC[4]=ParamUKC[5]=0;
					MassOutData[6]=ParamUKC[6] & 0x10;
#endif
					test=(RdFlash(AdrLic+2) << 16)+RdFlash(AdrLic); // �������� ������ � SN, ����� �������� � ����� ������� ��
					MassOutData[7]=test>>24;
					MassOutData[8]=test>>16;
					MassOutData[9]=test>>8;
					MassOutData[10]=test;
					MassOutData[11]=LenSNPult;
					MassOutData[12]=LenLicPult;
					MassOutData[13]=LenSNSound;
					MassOutData[14]=LenLicSound;
					data=GetWiznetReinitCounter();
					MassOutData[15]=data>>8;
					MassOutData[16]=data;
			    }
				Answer(&sInfoAnswer,108,17);
				break;
		 	case 109: // ������ ���������� �� (������� ���������� ������� � �.�.)
					ParamUKC[0]=(unsigned short)MassInData[2]+((unsigned short)MassInData[3]<<8);
/*{
  SendStr2IpLogger("$DEBUG$SportX_Cmd109: ParamUKC[0]=%x",
          ParamUKC[0]);
}*/
					if(MassInData[4])
					{ // ��������� ���� ����������
					  ParamUKC[1]=(unsigned short)MassInData[4] & 0x42;	// ������������ ��������� ����� UserLoader
					  ParamUKC[2]=(unsigned short)MassInData[5] & 0x42;	// ������������ SNMP ����� UserLoader;
					  E1_EDSS=ParamUKC[4]=(unsigned short)MassInData[6] & 0x0F;	// ��������� ����� ������ ISDN
					  if(ParamUKC[4] || (MassInData[8] & 0x60))
					  {
					    ParamUKC[5]=(unsigned short)MassInData[7] & 0x03; // ����� ������� ISDN
					    ParamUKC[6]=(unsigned short)MassInData[8] & 0x7F; // ��� ��
					  } 
					  else
					  { ParamUKC[5]=0; ParamUKC[6]=MassInData[8] & 0x10; }
					} 
					ReWrFlash(ParamUKC[0],AdrParamUKC);
					ReWrFlash(ParamUKC[1],AdrParamUKC+2);
					ReWrFlash(ParamUKC[2],AdrParamUKC+4);
					ReWrFlash(ParamUKC[4],AdrParamUKC+8);
					ReWrFlash(ParamUKC[5],AdrParamUKC+10);
					ReWrFlash(ParamUKC[6],AdrParamUKC+12);
					MassOutData[0]=ParamUKC[0]; MassOutData[1]=ParamUKC[0] >> 8; // ����� ����������
					MassOutData[2]=ParamUKC[1]; MassOutData[3]=ParamUKC[2]; 
					MassOutData[4]=ParamUKC[4]; MassOutData[5]=ParamUKC[5];
					MassOutData[6]=ParamUKC[6];
					InitMKC(0);
					InitMKC(1);
					InitMKC(2);
					InitMKC(3);
#ifdef __ISDN__
					InitISDN4MKC();
#endif
					Answer(&sInfoAnswer,109,7);
	 				break;
			case 110: // �������� ������ ���������� �������
			        CreateConstCommut(true);
                    Answer(&sInfoAnswer,110,3);
			        break;
			case 111: // �������� ������ ���������� �������
					MassOutData[0]=ExclGroup(MassInData+2);
					Answer(&sInfoAnswer,111,1);
					break;
			case 112: // ������ ������� ���������� ������ �������
//SendStr2IpLogger("$DEBUG$cmd10_112: executed!");
					if(!ReadGroup(MassInData+2)) Answer(&sInfoAnswer,112,1);
					break;
			case 113: // ���������� ����� ������ � ������
			case 114: // ������������� ����� ������ � ������
					 MassOutData[0]=BlockInput(MassInData+2,(InCommand==114) ? 0xff: 0);
					 Answer(&sInfoAnswer,InCommand,1);
					break;
			case 115: // �������� ������ � ������ �� ����� ������
					DelChanFromGroup(MassInData+2);
					break;
			case 116: // �������������� ������ � ������ �� ����� ������
					AddChan2Group(MassInData+2,0);
					break;
			case 117: // ������ ���� ���������� ������� � ������
//SendStr2IpLogger("$DEBUG$cmd10_117: executed...");
			        if(TestMKC(MassInData[2],false))
			        { // ������ ��������
			          j=MassInData[2]-13;
					  //for(i=0;i<128;i++) ReadE1(j,0x40+127-i);
			        }
					break;
			case 118:  // ��������� �������
			    {
			      unsigned short F1,F2,Ton,Tof,Ampl1,Ampl2;
 				  F1 =(unsigned short)MassInData[4]+((unsigned short)MassInData[5]<<8);
 				  F2 =(unsigned short)MassInData[6]+((unsigned short)MassInData[7]<<8);
 				  Ton =(unsigned short)MassInData[8]+((unsigned short)MassInData[9]<<8);
				  Tof =(unsigned short)MassInData[10]+((unsigned short)MassInData[11]<<8);
				  Ampl1 =(unsigned short)MassInData[12]+((unsigned short)MassInData[13]<<8);
				  Ampl2 =(unsigned short)MassInData[14]+((unsigned short)MassInData[15]<<8);
				  chan=CalcRealChannel(MassInData[2],MassInData[3]);
				  if(GeneratorMU(chan,F1,F2,Ton,Tof,Ampl1,Ampl2)) MassOutData[0]=true;
			    }
			    Answer(&sInfoAnswer,118,1);
		 	    break;
			case 119:  // 2-� ��������� ������
			    {
			      unsigned short F1,F2;
                  signed short dF,TimA,Type;

				  chan=CalcRealChannel(MassInData[18],MassInData[19]);
				  F1 =(unsigned short)MassInData[21]+((unsigned short)MassInData[22]<<8);
				  F2 =(unsigned short)MassInData[23]+((unsigned short)MassInData[24]<<8);
				  dF =(unsigned short)MassInData[25]+((unsigned short)MassInData[26]<<8);
				  TimA =(unsigned short)MassInData[27]+((unsigned short)MassInData[28]<<8);
				  Type =(unsigned short)MassInData[29]+((unsigned short)MassInData[30]<<8);
				  if(InitAnalMU(chan,MassInData[20],F1,F2,dF,TimA,Type,MassInData[31]))
	 	 		  { // ������ 2-� ���������� ������� �������� �� ������
	 	 		    if(MassInData[29] | MassInData[30]) FName=ReadFileName(AnalPCDT);
	 	 		    else FName=ReadFileName(AnalTDC);
	 			    MassOutData[0]=RewriteFile(FName,MassInData+18,14);
	 			  }
				  else MassOutData[0]=false;
			    }
				Answer(&sInfoAnswer,119,1);
		 	    break;
		 	case 120: // �������� ������� ������� �� ��������� ������
			 	chan=CalcRealChannel(MassInData[2],MassInData[3]);
                MassOutData[0]=ExitAnal(chan,(unsigned char)MassInData[4]);
		 		Answer(&sInfoAnswer,120,1);
		 		break;
			case 121:  // ������� ���� ������� ���������� � "���������" �� ���� ��
			    SpeakInSel=SpeakIn;
			    ReWrFlash(SpeakInSel,AdrSpeakInSel); // �������������� ������ ����������� ������ ��������
			    for(i=0;i<LenVirt;i++) Revers[i]=0; // ������� "���������" �� ���� ��
				ClearTablCommut();
                SendClearCommut();
	 			MassOutData[0]=true;
				Answer(&sInfoAnswer,121,1);
		 	    break;
		 	case 122: // �������������� ������� ���������� �� ������
					if(TablSport1[0] || TablSum[0] || MasGroupInfo[0].NameGroup[0])
					{ // ���������� �� ������ ����
				 	  MassOutData[0]=0xFF;
					}
					else
		 			{
		 			  if(MassInData[2] > 3) MassOutData[0]=0xFE;
	 				  else
		 			  {
                        char *Extend; // ��������� ���������� ����� �����
		 			    MYFILE stFile;

					    if(ParamUKC[0] & 0x1000) InitMKC(0);
					    if(ParamUKC[0] & 0x2000) InitMKC(1);
					    if(ParamUKC[0] & 0x4000) InitMKC(2);
					    if(ParamUKC[0] & 0x8000) InitMKC(3);
#ifdef __ISDN__
					    InitISDN4MKC();
#endif
		 			    i=0;
					    ParamUKC[3]=MassInData[2];
					    ReWrFlash(ParamUKC[3],AdrParamUKC+6); // ���������� �������� ����������
					    Extend=SetVarComm();
	 				    while(FindNextFile(i,&stFile) != (unsigned long)-1)// ������ ���������� �� ������
					    {
					      if(FindInitFile(&stFile,Extend)) RestartCommut(&stFile);
					      i++;
					    }
				        if(ParamUKC[3]!=OldComm) OldComm=ParamUKC[3];
					    MassOutData[0]=ParamUKC[3]; // N ������� ����������
					  }
					}
	 				Answer(&sInfoAnswer,122,1);
		 			break;
		 	case 123: // ������ �������� ����������
		 	  MassOutData[0]=ParamUKC[3];
		 	  Answer(&sInfoAnswer,123,1);
		 	  break;
			case 124: // �������� ���������� ����. �������
			{
              char FileName[LenFileName]; // ��� �����
			  MYFILE stFile;

			  for(i=0;i<5;i++) FileName[i]=SCall[i]; // ��� �����
			  j=MassInData[2]; // ����� ����������
			  if(j<10) FileName[i++]='0';
			  else FileName[i++]=(j/10)+'0';
			  FileName[i++]=(j%10)+'0';
			  FileName[i++]='_'; // ����� ��������
			  if(MassInData[3]>=100) FileName[i++]=(MassInData[3]/100)+'0';
			  if((MassInData[3] % 100)<10) FileName[i++]='0';
			  else FileName[i++]=((MassInData[3] % 100)/10)+'0';
			  FileName[i++]=(MassInData[3]%10)+'0';
			  for(j=0;j<4;j++) FileName[i++]=SpecGen[j]; // ���������� �����
			  while(i<sizeof(FileName)) FileName[i++]=0;
			  if(FindFile(FileName, &stFile)==(unsigned)-1)
			  {
			    HANDLE hFile=CreateFile(FileName,CREATE_ALWAYS,3+MassInData[4]*8); // ���
			    WriteFile(hFile,MassInData+2,3+MassInData[4]*8);
			    CloseHandle(hFile);
			    InitSpecGen(MassInData+2);
			    MassOutData[0]=true;
			  }
			  Answer(&sInfoAnswer,124,1);
			  break;
			}
			case 125: // ��������� ������
			  *pFIO_FLAG_T=0x8;
			  if(*pFIO_FLAG_D & 0x8) MassOutData[0]=true;
		 	  Answer(&sInfoAnswer,125,1);
			  break;
			case 126: // ������ ������� ��������� ������� ���������
			  vPortEnableSwitchTask(false);
			  MassOutData[0]=(unsigned char)(TimeM>>8); 
			  MassOutData[1]=(unsigned char)TimeM;
			  TimeM=0;
			  MassOutData[2]=(unsigned char)(TimeW>>8); 
			  MassOutData[3]=(unsigned char)TimeW;
			  TimeW=0;
			  vPortEnableSwitchTask(true);
			  Answer(&sInfoAnswer,126,4);
			  break;
			case 127: // ������ ���� ����� ����������
//SendStr2IpLogger("$DEBUG$cmd10_127: executed...");
			  j=0;
              while((j<MaxGroup) && MasGroupInfo[j].NameGroup[0])
              {
                for(i=0;i<LenNameGroup;i++) MassOutData[i]=MasGroupInfo[j].NameGroup[i];
                MassOutData[i]=0;
                Answer(&sInfo,127,i+2);
                j++;
              }
              MassOutData[0]=0; // ��� ������� ������ ������ ������������� ��������� 0!
              Answer(&sInfo,127,1);
			  break;
			case 128: // ������ ������� �������, ������������ � ���������
//SendStr2IpLogger("$DEBUG$cmd10_128: executed...");
			  if(!ReadInChan(MassInData[2], MassInData[3]))
			  {
			    MassOutData[0]=0;
			    Answer(&sInfo,128,1);
			  }
			  break;
			case 129: // ��������� ���������� 2-� ������� � ���� ������� (0-������, 1-�������� ����� �����������, 2-������ �� 
			  if (MassInData[2]==1) MassInData[3]+=LenHard+LenVirt-1; 
			  else
			    if (MassInData[2]==2) MassInData[3]+=LenHard-1;
			    else MassInData[3]+=LenSport1-1;
			  if (MassInData[4]==1) MassInData[5]+=LenHard+LenVirt-1; 
			  else
			    if (MassInData[4]==2) MassInData[5]+=LenHard-1;
			    else MassInData[5]+=LenSport1-1;
			  if (InclTempComm(MassInData[3],MassInData[5],0)) MassOutData[0]=1;
			  else MassOutData[0]=0;
			  Answer(&sInfoAnswer,129,1);
			  if((!MassInData[2]) && (RdFlash(AdrTypeCall+((MassInData[3]-LenSport1)*4))) & 0x4) SendBlockPPS(MassInData[3]-LenSport1,0);
			  if((!MassInData[4]) && (RdFlash(AdrTypeCall+((MassInData[5]-LenSport1)*4))) & 0x4) SendBlockPPS(MassInData[5]-LenSport1,0);
			  break;
			case 130: // �������� ��������� ���������� 2-� �������
			  if (MassInData[2]) MassInData[3]+=LenHard+LenVirt-1;
			  else MassInData[3]+=LenSport1-1;
			  if (MassInData[4]) MassInData[5]+=LenHard+LenVirt-1;
			  else MassInData[5]+=LenSport1-1;
			  MassOutData[0]=ExclTempComm(MassInData[3],MassInData[5]);
			  Answer(&sInfoAnswer,130,1);
			  if((!MassInData[2]) && (RdFlash(AdrTypeCall+((MassInData[3]-LenSport1)*4))) & 0x4) SendBlockPPS(MassInData[3]-LenSport1,1);
			  if((!MassInData[4]) && (RdFlash(AdrTypeCall+((MassInData[5]-LenSport1)*4))) & 0x4) SendBlockPPS(MassInData[5]-LenSport1,1);
			  break;
			case 131: // ������ �������� ������������ ������� � ���������
//SendStr2IpLogger("$DEBUG$cmd10_131: executed...");
			  if(MassInData[2]) MassInData[3]+=LenHard+LenVirt-1;
			  else MassInData[3]+=LenSport1-1;
			  i=ReadTempComm(MassInData[3]);
			  if(i) Answer(&sInfo,131,i); 
			  else Answer(&sInfo,131,1);
			  break;
			case 132: // ���������� �������� ��������� �� ���� ������� ����� ��������� �� ���
			  ChanSwing=0;
			  ExclAllGenerator();
			  MassOutData[0]=true;
			  Answer(&sInfoAnswer,132,1);
			  break;
			case 133: // ������ ���� ���� ��������� ������
		 	    // !!! ���������� ������� !!!
/*		 	    {
		 	      MYFILE stFile;
					i=0; j=0;
					while(FindNextFile(i,&stFile)!=(unsigned long)-1)
					{
					  memcpy(MassOutData+1+(j*16),stFile.szFileName,16); 
					  if(j==14)
					  {
					    MassOutData[0]=j;
					    Answer(&sInfoAnswer,133,((j+1)*16)+2);
					    j=0xFFFF; 
					  }
					  i++; j++;
					}
					MassOutData[0]=j;
		 	    }*/
				Answer(&sInfoAnswer,133,(j*16)+1);
		 		break;
		 	case 134: // �������� ���� ����� �������� � ���������� � �������������� �������� � ���������� �� ������
			// !!!!!!!!!!!! ��������� ������� ��� ������������� !!!!!!!!!!!!!!!!!!!!!!1
	 		  for(i=LenHard;i<(LenHard+LenVirt);i++)
	 		  {
	 		    ExitAnalDTMF(i); // �������� ������� ���� � �� ������ �� ���� �������
	 		    ExitAnal(i,0);
			    PorogMiddle[i-LenHard]=0;
			  }
			  ExclAllGenerator();
			  InitUK(); // ������������� ��
			  ReinitConnectionPoint();
			  MassOutData[0]=true;
			  Answer(&sInfoAnswer,134,1);
			  break;
			case 135: // �������� ����� �� ����� (��� ���������� ����������)
              if(DeleteFile((char *)MassInData+2)) MassOutData[0]=1;
			  else MassOutData[0]=0;
			  Answer(&sInfoAnswer,135,1);
			  break;
			case 136: // ���������� �������� ��������� �� ������
			  ChanSwing=0;
			  chan=CalcRealChannel(MassInData[2],MassInData[3]);
			  MassOutData[0]=ExclGen(chan);
			  Answer(&sInfoAnswer,136,1);
 			  break;
			case 137: // ������ �������� �������������� �������
//SendStr2IpLogger("$DEBUG$cmd10_137: executed...");
			  i=ReadAllTempCommute(MassOutData);
			  Answer(&sInfoAnswer,137,i);
			  break;
			case 138: // ��������� ������� �������������� ������� �� ������
			  if(!TimeFreq)
			  {
			    ChanFreq=CalcRealChannel(MassInData[2],MassInData[3]);
			    if(MeazFreq & 0x4000)
			    { // ���� ���������� ������ �������
			      i=((MeazFreq & 0x3fff)+1) >> 1;
/*{
extern short DataMin,DataMax;
  static char str[80];
  LockPrintf();
  sprintf(str,"$DEBUG$Cmd10_138: tp=%d, ch=%d,chan=%d, Freq=%d, DataMin=%d, DataMax=%d",
          MassInData[2],MassInData[3],ChanFreq,i,DataMin,DataMax);
  UnlockPrintf();
  SendStr2IpLogger(str);
}*/
			    }
			    else i=0;
			    MassOutData[0]=i >> 8;
			    MassOutData[1]=i;
	 		    Answer(&sInfoAnswer,138,2);
			    TimeFreq=8000; // ��������� ����� ������ �������
		 	  }
		 	  break;
		 	case 139: // ������ ������ ����� (!!! �� ������ �������������� !!!)
/*
					if(FindFile((char *)MassInData+2,&stFile)!=(unsigned)-1)
					{
					  HANDLE hFile;
					  if(stFile.FileLen>200) MassOutData[0]=200;
					  else MassOutData[0]=stFile.FileLen;
					  MassOutData[1]=0;
					  hFile=CreateFile(stFile.szFileName, GENERIC_READ, 0);
					  if(hFile>=0)
					  {
					    ReadFile(hFile,MassOutData+2, sizeof(MassOutData)-2);
					    CloseHandle(hFile);
					    Answer(&sInfoAnswer,139,MassOutData[0]+2);
					    break;
					  }
					  else MassOutData[0]=0;
					}*/
MassOutData[0]=0;
					Answer(&sInfoAnswer,139,1);
		 			break;
		 	case 140: // ���������/���������� ������� ���������
		 	    MassOutData[0]=SelectorTikkerX();
				Answer(&sInfoAnswer,140,1);
	 			break;
		 	case 141: // ������� ��������� ��
		 	    i=_xGetCP();
		 	    if(!i)
		 	    { MassOutData[0]=MassOutData[1]=0xFF; }
		 	    else
		 	    { MassOutData[0]=1; MassOutData[1]=i; }
				Answer(&sInfoAnswer,141,2);
	 			break;
		 	case 142: // ������������ ��
		 	    _xFreeCP(MassInData[3]);
		 		break;
		 	case 143: // �������� �����������
	 			MassOutData[0]=ConferenceCreatX(MassOutData);
	 			Answer(&sInfoAnswer,143,1);
				break;
		 	case 144: // �������� ����������� (N ����.(1�.)+N ����.(1�.)
		 	    MassOutData[0]=ConferenceDelX(MassOutData);
			    Answer(&sInfoAnswer,144,1);
				break;
		 	case 145: // ��������/���������� ��������� �����������
		 	    MassOutData[0]=ConferenceAbntX(MassOutData);
			    Answer(&sInfoAnswer,145,1);
			    break;
			case 146: // ����������/����������� ���������
			    MassOutData[0]=ConferenceOperatorX(MassOutData);
				Answer(&sInfoAnswer,146,1);
				break;
		 	case 147: // !!! �� ������������ !!!
				Answer(&sInfoAnswer,147,1);
				break;
		 	case 148: // ��������� �������� �� ������ �� ���������� 8 ��� (125 ���)
				if(MassInData[2]<125)
				{ ResetWait125=16000; TimeWait125=MassInData[2] * (1000 / 20)-12; }
				MassOutData[0]=(TimeWait125+12) / (1000 / 20);
		 		Answer(&sInfoAnswer,148,1);
		 		break;
		 	case 149: // �������� ������� ����������� �� ������
		 	    {
		 	      unsigned int test;
				  test=InitRecord(MassInData[2],MassInData[3],MassInData[4],MassInData[5],
				                  g_IpParams.ucDataSoundIP,g_IpParams.wPortSound+g_IpParams.ucDataSoundIP[3],0);
		          for(i=0;i<4;i++) 
		          { MassOutData[i]=test; test>>=8; }
		 	    }
			 	Answer(&sInfoAnswer,149,1+4);
		        break;
		 	case 150: // �������� ������� ����������� �� ������
                MassOutData[0]=ExitRecord(MassInData[2],MassInData[3]);
		 		Answer(&sInfoAnswer,150,1);
 		    	break;
		 	case 151: // ������ ������� ��������������� ����� �����������
				data=1;
				for(i=0;i<LenVirt;i++)
				{ 
				  if(FoundTP(i,false))
				  { MassOutData[data++]=1; MassOutData[data++]=i; }
				}
				MassOutData[0]=data / 2;
		 		Answer(&sInfoAnswer,151,data+1);
		 		break;
			case 152: // ������� DataSave � ����������������� SPORT
                vPortEnableSwitchTask(false);
				RestartSPORT0(); // ����������������� SPORT
				RestartSPORT1(); // ����������������� SPORT
#ifdef __ISDN__
//		pri_chan_start(1);	// ����������������� EDSS
#endif
		 		for (i=0;i<LenDataSave;i++) DataSave[i]=DataSave1[i]=0; // DataSave2[i]=0;
                vPortEnableSwitchTask(true);
//#define __ClearAll__ 1
#ifdef __ClearAll__
    for (i=0;i<32768;i+=2) 
    {
        WrFlash(0xFFFF,i);   // �������� ������ DD7
        ResetWatchDog(true);
    }
#endif
				Answer(&sInfoAnswer,152,1);
		 		break;
		 	case 153: // ��������� ���
				ChanSwing=CalcRealChannel(MassInData[2],MassInData[3]);
		 		GeneratorMU(ChanSwing,0,0,0,0,_0dB2,_0dB2);
		 		MassOutData[0]=true;
		 		Answer(&sInfoAnswer,153,1);
		 		break;
			case 154: // ������ ������� �������� �� ���������� ������� � �.�.
				sInfoAnswer.Source=USER;
	 			sInfoAnswer.L=2*(LenSport0+LenVirt)+2;
	 			sInfoAnswer.A1=A1;
				sInfoAnswer.PORT1=PORT1;
	 			{
	  			  sInfoAnswer.ByteInfoKadr[0]=11;
	  			  sInfoAnswer.ByteInfoKadr[1]=154;
	  			  for(i=0;i<LenSport0+LenVirt;i++)
	  			  {
	  			    sInfoAnswer.ByteInfoKadr[2*i+2]=MiddleMax[i] >> 8;
	  			    sInfoAnswer.ByteInfoKadr[2*i+3]=MiddleMax[i];
	  			  }
 				  xQueueSend(xQueueReply[USER],&sInfoAnswer,0,0);
				}
				break;
		 	case 155: // �������� ������ ��� ���������� ���������
		 		MassOutData[0]=SelectorCreatX(MassOutData);
		 		Answer(&sInfoAnswer,155,1);
				break;
	 		case 156: // �������� ������������ ���������
	 		    MassOutData[0]=SelectorDelX(MassOutData);
				Answer(&sInfoAnswer,156,1);
				break;
			case 157: // ���������� ������ ������� ������ ������������ ���������
			case 158: // ������������� ������ ������� ������ ������������ ���������
			    MassOutData[0]=SelectorChanBlockX(InCommand,MassOutData);
				Answer(&sInfoAnswer,InCommand,1);
				break;
			case 159: // �������� ������� �� ���������
			    MassOutData[0]=SelectorDelChanX(MassOutData);
				Answer(&sInfoAnswer,159,1);
				break;
			case 160:
			    MassOutData[0]=SelectorAddChanX(MassOutData);
				Answer(&sInfoAnswer,160,1);
				break;
			case 161: // ������ ������� ������������ ������� �� �����������
			      {
					xListRecord *AdrRecord=(xListRecord *)ListRecord.pxHead;
					data=0;
					i=0;
					while(AdrRecord)
					{
#define _ AdrRecord ->
					  if (_ chan < LenHard) {MassOutData[++data]=0; MassOutData[++data]=_ chan-LenSport1+1;}
					  else
					    if(_ chan < (LenHard+LenVirt))
					    { MassOutData[++data]=2; MassOutData[++data]=_ chan-LenHard+1; }
					    else
					    { MassOutData[++data]=1; MassOutData[++data]=_ chan-(LenHard+LenVirt)+1; }
					  MassOutData[++data]=_ TypeRecord;
					  j=_ block;
					  if(j & 0x20) j=2;
					  MassOutData[++data]=j;
					  AdrRecord=(xListRecord *) _ pxNextRec;
					  i++;
#undef _
					}
					MassOutData[0]=i;
					Answer(&sInfoAnswer,161,data+1);
					break;
			      }
			case 162: // �������� ������� �������������������� �� ������
				MassOutData[0]= InitPlay(MassInData[2],MassInData[3],MassInData+4);
				Answer(&sInfoAnswer,162,1);
			    break;
			case 163: // �������� ������� �������������������� �� ������
			    MassOutData[0]= ExitPlay(MassInData[2],MassInData[3]);
				Answer(&sInfoAnswer,163,1);
			    break;
			case 164: // ������ ������� ������� ��������������������
				data=0;
				{
				  xListPlay *AdrPlay=(xListPlay *) ListPlay.pxHead;
				  while(AdrPlay)
				  {
				    #define _ AdrPlay ->
				    if(_ ChanPlay < LenHard)
				    { MassOutData[++data]=0; MassOutData[++data]=_ ChanPlay-LenSport1+1; }
				    else
				      if(_ ChanPlay < (LenHard+LenVirt))
				      { MassOutData[++data]=2; MassOutData[++data]=_ ChanPlay-LenHard+1; }
				      else
				      { MassOutData[++data]=1; MassOutData[++data]=_ ChanPlay-(LenHard+LenVirt)+1; }
				    AdrPlay=(xListPlay *) _ pxNextPlay;
				    #undef _
				  }
				}
				MassOutData[0]=data / 2;
				Answer(&sInfoAnswer,164,data+1);
				break;
			case 165: // ������ ������������ ��������� ����� �� 2 ����
				MassOutData[0]=(TimeCorrClock-TimeSet) >> 24;	// ����� �� ������������� � ���
				MassOutData[1]=(TimeCorrClock-TimeSet) >> 16;
				MassOutData[2]=(TimeCorrClock-TimeSet) >> 8;
				MassOutData[3]=TimeCorrClock-TimeSet;
				MassOutData[4]=TimeCorrClock / (60*60*100);		// ����� ������������� � �����
				MassOutData[5]=DeltaTime >> 8;					// ������ �� ����� �������������
				MassOutData[6]=DeltaTime;
				MassOutData[7]=MaxDeltaTime >> 8;				// max ������ �� ����� �������������
				MassOutData[8]=MaxDeltaTime;
				MassOutData[9]=MinDeltaTime >> 8;				// min ������ �� ����� �������������
				MassOutData[10]=MinDeltaTime;
				MassOutData[11]=ReStart >> 8;					// ����� ������������ ������� �����
				MassOutData[12]=ReStart;
				MassOutData[13]=ReStart1 >> 8;					// ����� ������������ �����������
				MassOutData[14]=ReStart1;					  
				MassOutData[15]=Time_Sys >> 24;					// ����� ��������� ���
				MassOutData[16]=Time_Sys >> 16;
				MassOutData[17]=Time_Sys >> 8;
				MassOutData[18]=Time_Sys;
				ExistTime=RdFlash(AdrErrTime);
				MassOutData[19]=ExistTime >> 8;
				MassOutData[20]=ExistTime;
				ExistTime=RdFlash(AdrSysTime)+(RdFlash(AdrSysTime+2) << 16);
				MassOutData[21]=ExistTime >> 24;
				MassOutData[22]=ExistTime >> 16;
				MassOutData[23]=ExistTime >> 8;
				MassOutData[24]=ExistTime;
				Answer(&sInfoAnswer,165,25);
				break;
			case 166: // ������ ����� ������ ���
//				StartTestMKC=3;
//				MassOutData[0]=1;
//				Answer(&sInfoAnswer,166,1);
				break;
			case 167: // ��������� ����� ������ ���
//				if(StartTestMKC) StartTestMKC--;
//				AnswerTestMKC(MassOutData);
//				Answer(&sInfoAnswer,167,13);
//				Answer(&sInfoAnswer,167,1);
				break;
			case 168: // �������� ������ ���������� ���������� ��� �������� �����
				MassOutData[0]=SaveGroupFlash(MassInData,false,0);
				Answer(&sInfoAnswer,168,3);
				break;
			case 169: // ������ ������� � ������� �������
				MassOutData[0]=FTik >> 8;
				MassOutData[1]=FTik;
				MassOutData[2]=PeriodTikker >> 8;
				MassOutData[3]=PeriodTikker;
				Answer(&sInfoAnswer,169,5);
				break;
			case 170: // ������ ������� � ������� �������
				FTik=MassInData[2]*256+MassInData[3];
				ReWrFlash(FTik,AdrFTikker);
				PeriodTikker=MassInData[4]*256+MassInData[5];
				ReWrFlash(PeriodTikker,AdrPeriodTikker);
				MassOutData[1]=1;
				Answer(&sInfoAnswer,170,1);
				break;
#ifdef __VLAN__
			case 171: // ������ �������� MARVELL
				test=ReadSwitch(MassInData[2],MassInData[3]);
				MassOutData[0]=(unsigned char)(test>>8); 
				MassOutData[1]=(unsigned char)test;
			    Answer(&sInfoAnswer,171,2);
			    break;
			case 172: // ������ � ������� MARVELL
				WriteSwitch(MassInData[2],MassInData[3],MassInData[4]);
				test=ReadSwitch(MassInData[2],MassInData[3]);
				MassOutData[0]=(unsigned char)(test>>8); 
				MassOutData[1]=(unsigned char)test;
			    Answer(&sInfoAnswer,172,2);
			    break;
#endif
			case 173: // ������ ������ ������� ��������� ��������
				MassOutData[0]=(unsigned char)(SpeakInSel>>8); 
				MassOutData[1]=(unsigned char)SpeakInSel;
			    Answer(&sInfoAnswer,173,2);
			    break;
			case 174: // ��������� ������ ������� ��������� ��������
				if((MassInData[2]<109) && (MassInData[2]>21)) ReWrFlash(SpeakInSel=MassInData[2],AdrSpeakInSel); 
				MassOutData[0]=(unsigned char)(SpeakInSel>>8); 
				MassOutData[1]=(unsigned char)SpeakInSel;
			    Answer(&sInfoAnswer,174,2);
			    break;
			case 175: // ���������� ����������� �� ������� ���������
			    MassOutData[0]=SelectorGen4ChanOffX();
			    Answer(&sInfoAnswer,175,1);
			    break;
			case 176: // ����������/������� �������� � ���������
			    MassOutData[0]=SelectorSetVedX();
				Answer(&sInfoAnswer,176,1);
				break;
			case 177: // ��������� �������� ����� �� ����� ��� ((SN ��� ������ � ������� �������)+(2+4 ���� IP)+MAC (6 ����)
{
/*  SendStr2IpLogger("$DEBUG$Audio send for pult SN=%d, wPort=%d, Op1=%d, Op2=%d",
                   MassInData[2],g_IpParams.wPortSound,
                   (GetOperatorChannel(MassInData[2],0) & 0xFF)-1,
                   (GetOperatorChannel(MassInData[2],1) & 0xFF)-1);*/
}
				if(g_IpParams.wPortSound) 
				{
                  data=(GetOperatorChannel(MassInData[2],0) & 0xFF)-1;
				  InitRecord(2,data,0,2,MassInData+5,g_IpParams.wPortSound,0); 
                  data=(GetOperatorChannel(MassInData[2],1) & 0xFF)-1;
				  InitRecord(2,data,0,2,MassInData+5,g_IpParams.wPortSound,0);
				}
				break;
            case 178: // ���/���� �������� ����� �� IPPM
              chan=MassInData[3]-1;
              if(MassInData[2])
              {
                if(g_IpParams.wPortSound)
                {
//SendStr2IpLogger("$DEBUG$Audio send for IPPM: wPort=%d, wCP=%x",g_IpParams.wPortSound,chan);
                  InitRecord(2,chan,0,0,MassInData+4,g_IpParams.wPortSound,0);
                }
              }
              else ExitRecord(2,chan); // ���������� �������� ����� �� IPPM
              break;
			case 180: // ���� �������� ������ c ������
			  break;
			case 181: // ����� ������ �� DataSave0[] �� DspCom � 16-�� ������ �������
				for(i=0;i<20;i++)
				{
				  MassOutData[i*2]=DataSave[i];
				  MassOutData[i*2+1]=DataSave[i]>>8;
				}
				Answer(&sInfoAnswer,181,42);
			    break;
			case 182: // ����� ����� ����� POS.lic
				for(i=0;i<FileNameLEN;i++) MassOutData[i]=FilePOS[i]; 
				Answer(&sInfoAnswer,182,19);
			    break;
			case 183: // ����� ����� ����� REC.lic
				for (i=0;i<FileNameLEN;i++) MassOutData[i]=FileREC[i]; 
				Answer(&sInfoAnswer,183,19);
			    break;
			case 184: // ���������� ������������� ������ (��� ������)
			    chan=(GetOperatorChannel(MassInData[2],0) & 0xff)-1; // ����� ��
			    ExitRecord(2,chan);
			    ExitPlay(1,chan);
			    chan=(GetOperatorChannel(MassInData[2],1) & 0xff)-1; // ����� ��
			    ExitRecord(2,chan);
			    ExitPlay(1,chan);
			    break;
			case 185:
			    CreateConstCommut(false);
		        Answer(&sInfoAnswer,185,3);
			    break;
			case 199: // �������� ���� �����������
				break;

// ------------------------------------------------- EDSS -----------------------------------------
#ifdef __ISDN__
			case 201:
			  Command201();
			  break;
			case 202:
			  Command202();
			  break;
			case 203:
			  Command203();
			  break;
			case 204:
			  Command204();
			  break;
			case 205:
			  Command205();
			  break;
			case 206:
			  Command206();
			  break;
			case 207:
			  _xpri_call_on_incoming(&sCmd);
			  break;
			case 208:
			  _xpri_call_on_info();
			  break;
			case 209:
			  _xpri_call_on_alert();
			  break;
		    case 210:
			  _xpri_call_on_progress();
			  break;
			case 211:
			  _xpri_call_on_speaker_enable();
			  break;
			case 212:
			  _xpri_call_on_answer();
			  break;
			case 213:
			  _xpri_call_on_disconnect();
			  break;
			case 214:
			  _xpri_call_on_release();
			  break;
/*		 	case 220: Command201a();// ���������� ��������� �����
		 			break;*/
#ifndef SINFO_ENERGO
		 	case 221: // ��������/��������� ����������� ���������
		 	  SelectorRecordOnOff(MassM1[MassInData[2]],MassInData[3]);
		 	  break;
#endif
#ifdef SINFO_ENERGO
			case 222: // ���������/���������� ������ ���������� ��������
			  if(g_IpParams.wPortSound) 
			  {
/*{
  SendStr2IpLogger("$DEBUG$Cmd10_222: ConnId=%d, wCP=%d, btOn=%d, wPort=%d",
          GET_DW(MassInData+8),MassInData[3],MassInData[2],g_IpParams.wPortSound+MassInData[7]);
}*/
                if(MassInData[2])
                {
                  InitRecord(1,MassInData[3]-1,0,0,
                             MassInData+4,g_IpParams.wPortSound+MassInData[7],
                             GET_DW(MassInData+8));
                }
                else ExitRecord(1,MassInData[3]-1);
			  }
			  break;
#endif
#endif // ISDN
		}
        if(*((unsigned int *)g_IpParams.ucDataSNMPrIP) && sInfoAnswer.L &&
           (((InCommand<179) || (InCommand>214)) && (InCommand!=154)))        
		{ // �� ������� ��������� � �� ������ ������� �� ���� ������� � �� ����� DataSave
			sCmd.BufCommand[0]=2;
        	sCmd.BufCommand[1]=1;
        	sCmd.BufCommand[2]=100; // ����� �������
        	sCmd.BufCommand[3]=31;
        	sCmd.BufCommand[4]=InCommand; // ��� �������
        	for(i=0;i<sInfoAnswer.L;i++) sCmd.BufCommand[i+5]=MassOutData[i]; // ��� ������          
        	SendDiagnostics2DspCom(sCmd.BufCommand,sInfoAnswer.L+3);
        }
        if(!IsMKCCommand(InCommand)) InCommand=0; // ��� ������ ��� InCommand �� ����������!!
      }
      else
      {
		switch(MassInData[0])
		{ // ����������� ���� ������� �������
			case 1: // ����� ��������� �� ������� ������� ����������
			  if(MassInData[2])
			  {
			    if(CallAbon(MassInData[1],MassInData[2]-1,MassInData[3],MassInData[4],LEN1-4)) // 4-����� ������� ��� N ��������
			    { SendReplyXCmd(1,CodeStatusTrue); break; }
			  }
			  SendReplyXCmd(1,CodeStatusFalse);
			  break;
			case 15: // ������ ���������� ����� �����������
			  Answer15(&sInfoAnswer);
			  break;
			case 20: // ��������� ���������� �� ����� ����������� (4 �����)
				// 1.1-���, 1.2-���, 1.4-��������, 1.8-����, 1.16-�� ������, 1.32-���/�������, 1.64-��������, 1.128-�����������
				// 2.1-��, 2.2-����������(1600��), 2.4-48/96�, 2.8-�������� ���, 2.16-�������� ����, 2.32-����, 2.64-���/���� ������
				if (MassInData[1]==1)
				{ // ��� ������ ���� ����� ����������� ��� ������
				  if (SetParamPoint(MassInData)) SendReplyXCmd(MassInData[0],CodeStatusTrue);
				  else SendReplyXCmd(MassInData[0],CodeStatusFalse);
				}
				break;
			case 24: // ��������� ������ ��� �� ����� �����������
				if(MassInData[1]==1)
				{ // ��� ������ ���� ����� �����������
				  MaxARUTP[MassInData[2]]=MassInData[3];
				  ReWrFlash(MassInData[3],AdrMinARUTP+(MassInData[2] * 2));
				  SendReplyXCmd(MassInData[0],CodeStatusTrue);
				}
				break;
			case 28: // ��������� ������ �������� �� ����� �����������
					if (MassInData[1]==1)	// ��� ������ ���� ����� �����������
					{Revers[MassInData[2]]=(short)MassInData[3] * CoefReturnCh;
					 SendReplyXCmd(MassInData[0],CodeStatusTrue);}
					break;
			case 29: // ��������� ������ ������� �������� �������� �� ����� �����������
					if (MassInData[1]==1)	// ��� ������ ���� ����� �����������
					{PorogMiddle[MassInData[2]]=MassInData[3];
					 SendReplyXCmd(MassInData[0],CodeStatusTrue);}
					break;
			case 37: // ��������� / ���������� ��� �� ����� �����������
					if (MassInData[1]==1)	// ��� ������ ���� ����� �����������
					{ARUTP[MassInData[2]]=MassInData[3];
					 ReWrFlash(MassInData[3],AdrARUTP+(MassInData[2] * 2));
					 SendReplyXCmd(MassInData[0],CodeStatusTrue);}
					break;
			case 38: // ����������/������������� ������ TP
{
extern unsigned short g_wMasDisOut[LenSport0];
  MassOutData[0]=CodeStatusTrue;
  if(MassInData[2]>63) MassOutData[0]=CodeStatusFalse;
  else
  { g_wMasDisOut[MassInData[2]]=(MassInData[3] ? 0xffff : 0); }
}
/*					MassInData[2]+=LenHard;
					i=0; MassOutData[0]=CodeStatusFalse; 
					while(i<LenSumChan)
					{
					  j=TablSum[i++];
					  if(!j) break;
					  if(j==2) 
					  {
					    if(MassInData[2]!=TablSum[i]) i++;
					    if(MassInData[2]==TablSum[i]) 
					    {
					      if(MassInData[3]) TablBlock[i]=0xFF;
					      else TablBlock[i]=0;
					      MassOutData[0]=CodeStatusTrue;
					      break;
					    }
					    else i++;
					  }
					  else i+=j;
					}*/
				    SendReplyXCmd(MassInData[0],MassOutData[0]);
					break;
			case 40: // ��������� 2-� ���������� ����������
			    {
			      unsigned short F1,F2,Ton,Tof,Ampl1,Ampl2;

				  chan=CalcRealChannel(MassInData[1],MassInData[2]);
				  F1 =(unsigned short)MassInData[4]+((unsigned short)MassInData[3]<<8);
				  F2 =(unsigned short)MassInData[6]+((unsigned short)MassInData[5]<<8);
				  Ton=0;
				  Tof=0;
				  Ampl1=_6dB2; // ���������� �� 6 �� ��� ��
				  if(GeneratorMU(chan,F1,F2,Ton,Tof,Ampl1,Ampl1)) SendReplyXCmd(MassInData[0],CodeStatusTrue);
				  else SendReplyXCmd(MassInData[0],CodeStatusFalse);
			    }
				break;
			case 41: // ���������� 2-� ���������� ����������
                chan=CalcRealChannel(MassInData[1],MassInData[2]);
                ExclGen(chan);
                SendReplyXCmd(MassInData[0],CodeStatusTrue);
                break;
			case 50: // ������ �������� �������� �� ����� �����������
              if(!BigCountMiddle)
              {
                ChanMiddle=CalcRealChannel(MassInData[1] ^ 3,MassInData[2]);
                Answer50(&sInfoAnswer);
                BigCountMiddle=256*8; // ��������� ����� ������ �������� (256 ����)
              }
              break;
		}
	  }
    }
  }
}

