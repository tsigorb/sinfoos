#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FuncSportXCmds.h"

///////////////////////////////////////////////////////////////////////////////
extern unsigned int A1;      // ����� ����������
extern unsigned short PORT1; // ���� ����������
extern unsigned char MassInData[LenInOutData];

extern unsigned short ParamUKC[];
extern unsigned char TablSum[LenSumChan]; // ������� ������� ����������� �� ������� ���� ���������� (�������������)
extern unsigned char TablBlock[]; // ������� ������� ����������� �� �����
extern unsigned char CopyTablBlock[]; // ����� ������� ������� ����������� �� �����
extern GROUPINFO_TP MasGroupInfo[MaxGroup];

extern unsigned char VedSelect[NumberKeyboard]; // ����� ������ �������� ���������

extern unsigned char Tikker[NumberKeyboard]; // ������� ��������� ������� ���������
extern unsigned short TimeTikker[NumberKeyboard]; // ����� �������

#ifdef __ISDN__
extern unsigned char  SelectEDSS[LenVirt]; // ������� ��������� (������ ������ � ������� ����������+1) ��� ��� (0)
#endif

extern char strInfoSpXCmd[512];

///////////////////////////////////////////////////////////////////////////////
static unsigned char NameSelector[LenFileName];

///////////////////////////////////////////////////////////////////////////////
void Word2Str(unsigned char *Adr, unsigned short data, unsigned char len)
{ // �������������� Word2Str (2, 3 ��� 4 �������)
  if(len==4)
  {*Adr++ = data/1000 + '0'; data = data - ((data/1000)*1000);} 
  if(len==3)
  {*Adr++ = data/100 + '0';  data = data - ((data/100 )*100 );}
  *Adr++ = data/10 + '0';
  data = data - ((data/10 )*10 );
  *Adr++ = data + '0';
}

///////////////////////////////////////////////////////////////////////////////
SelectorInfoTP MasSelInfo[NumberKeyboard];

void InitSelectorInfo(void)
{ memset(MasSelInfo,0,sizeof(MasSelInfo)); }

unsigned char FindNKlavFromSN2Sel(unsigned char SN)
{
  unsigned char btInd;

  if(!SN) return(0xff);
  for(btInd=0;btInd<NumberKeyboard;btInd++)
  {
    if(MasSelInfo[btInd].SN==SN) return(btInd);
  }
  return(0xff);
}

bool IsSelector2Pult(unsigned char SN)
{
  vPortEnableSwitchTask(false);
  if(FindNKlavFromSN2Sel(SN)!=0xff)
  {
    vPortEnableSwitchTask(true);
    return true;
  }
  vPortEnableSwitchTask(true);
  return false;
}

unsigned char AddPultSN2MasSelector(unsigned char SN)
{
  unsigned char i,btInd;

  if(FindNKlavFromSN2Sel(SN)!=0xff) return(0xfe);
  for(btInd=0;btInd<NumberKeyboard;btInd++)
  {
    if(MasSelInfo[btInd].SN==0)
    {
      MasSelInfo[btInd].SN=SN;
      MasSelInfo[btInd].CP=0;
      MasSelInfo[btInd].NAbon=0;
      for(i=0;i<MasAbntSelSz;i+=2)
      {
        MasSelInfo[btInd].MasChan[i]=0x80;
        MasSelInfo[btInd].MasChan[i+1]=0x00;
      }
      return(btInd);
    }
  }
  return(0xff);
}

void GetNameSelector(unsigned char SN) 
{ // ��������� ����� ������ ���������
  unsigned char i;
  static char NSelector[LenNameGroup]="Selector_XX";

  for(i=0;i<sizeof(NameSelector);i++) NameSelector[i]=0;
  for(i=0;i<LenNameGroup;i++) NameSelector[i]=NSelector[i];
  Word2Str(NameSelector+9,SN,2);
}

unsigned short FoundPlaceSelector(unsigned char Nklav) 
{ //  ����������� �������������� ��������� � ������� ����������
  unsigned short wInd=0xffff;

  vPortEnableSwitchTask(false);
  if(Nklav<NumberKeyboard)
  {
    GetNameSelector(MasSelInfo[Nklav].SN);
    wInd=FoundAdrGroup(NameSelector);
    if(wInd) wInd=MasGroupInfo[wInd-1].wOff;
    else wInd=0xffff;
  }
  vPortEnableSwitchTask(true);
  return(wInd);
}

void SelectorRecordOnOff(unsigned char SN,unsigned char btState)
{
  unsigned char Chan,Buf[3];

  Chan=_xGetSelectorCP(SN);
/*{
  static char str[80];
  LockPrintf();
  sprintf(str,"$DEBUG$SelectorRecordOnOff: State=%d, SN=%d, chan #%d",btState,SN,Chan);
  UnlockPrintf();
  SendStr2IpLogger(str);
}*/
  if(!Chan) return;
  if(btState)
  {
    InitRecord(1,Chan-1,1,0,
               g_IpParams.ucDataSoundIP,g_IpParams.wPortSound+g_IpParams.ucDataSoundIP[3],0);
    Buf[0]=1;     // ��� ����� �����������
    Buf[1]=Chan;  // ����� ��
    Buf[2]=32;    // ��� ������
    OutAbon(Buf,3);
  }
  else ExitRecord(1,Chan-1);
}

unsigned short GetSelectorConnectionPoint(unsigned char SN)
{
  unsigned short Chan;

  Chan=_xGetSelectorCP(SN);
  if(!Chan) return(0xff);
  return(0x100+Chan);
}

bool IsLeaderSelector(unsigned char SN,unsigned short wCP)
{
  unsigned char NKlav;

  if(wCP>0xff) wCP=(wCP & 0xff)+LenHard-1; // ����� �����������
  else wCP+=LenSport1-1; // ���������� �����
  NKlav=FindNKlavFromSN2Sel(SN);
  if((NKlav!=0xff) && VedSelect[NKlav]==wCP) return(true);
  return(false);
}

void SetVedSelect4MKA(unsigned char btChan)
{ // ���������/������ �������� �������� ��������� ��� ������ ���
  static sCommand sCmd;

  sCmd.Source=UserSportX;
  sCmd.L=4;
  sCmd.A1=0;
  sCmd.PORT1=0;
  sCmd.BufCommand[0]=19;
  if(btChan>128)
  { // �������� ��������
    sCmd.BufCommand[3]=0;
    btChan-=128;
  }
  else sCmd.BufCommand[3]=2; // ��������� ��������
  sCmd.BufCommand[1]=0;
  sCmd.BufCommand[2]=btChan-1; // N ������
  xQueueSend(xQueueCommand,&sCmd,0,0);
}

#ifdef __ISDN__ 
void ReadSound4Sel(HANDLE hFile,unsigned char NKlav)
{
  unsigned int dwSz=0,dwTek;

  MasSelInfo[NKlav].pWav=pvPortMalloc(d_MaxSelectorWav);
  if(MasSelInfo[NKlav].pWav) memset(MasSelInfo[NKlav].pWav,0,d_MaxSelectorWav);
  else return;
  do
  {
    dwTek=d_MaxSelectorWav-dwSz;
    if(dwTek>1024) dwTek=1024;
    if(dwTek) dwTek=ReadFile(hFile,MasSelInfo[NKlav].pWav+dwSz,dwTek);
    if(dwTek>0)
    {
      dwSz+=dwTek;
      vTaskDelay(1);
    }
  }
  while(dwTek==1024);
  MasSelInfo[NKlav].dwWavSz=dwSz;
}

bool LoadSoundForSelector(unsigned char SN,unsigned char NSel)
{
  unsigned char NKlav;
  char FName[20];
  HANDLE hFile;

  NKlav=FindNKlavFromSN2Sel(SN);
  if(NKlav==0xff) return false;
  MasSelInfo[NKlav].dwWavSz=0;
  if(((ParamUKC[0] & 0xF000) || (ParamUKC[6] & 0x60)) && ParamUKC[4])
  { // ���� ����������� ����� �1 ��� ��� � ���������� ��������� ����� ������ ��� EDSS
    LockPrintf();
    sprintf(FName,"Sel_%02d_%02d.wav",MasSelInfo[NKlav].SN,NSel+1);
    UnlockPrintf();
    hFile=CreateFile(FName,GENERIC_READ,0);
    if(hFile<0) hFile=CreateFile("Selector00.wav",GENERIC_READ,0);
    if(hFile>=0)
    {
      ReadSound4Sel(hFile,NKlav);
      CloseHandle(hFile);
    }
    return true;
  }
  return false;
}

void DelSound4Sel(unsigned char Nklav)
{
  MasSelInfo[Nklav].dwWavSz=0;
  if(MasSelInfo[Nklav].pWav)
  {
    vPortFree(MasSelInfo[Nklav].pWav);
    MasSelInfo[Nklav].pWav=0;
  }
}
#endif

///////////////////////////////////////////////////////////////////////////////
unsigned char SelectorCreatX(unsigned char *MasOut)
{
  unsigned char SN,Nklav,NAbon,data,
                i,j,k,*ptr,
                btRes=0;

  SN=MassInData[2]; // �������� ����� ����������
  Nklav=AddPultSN2MasSelector(SN);
  if(Nklav==0xff) return 0; // ��� ����� ��� ���������
  if(Nklav==0xfe) return 0; // �������� �� ���������� ��� ����
  NAbon=MassInData[4]+1;    // ����� ���������+��������
  GetNameSelector(SN);
  k=LenNameGroup+1;
  memcpy(MasOut+2,NameSelector,k);
  ptr=MasOut+2+k;
  Word2Str(ptr,NAbon+1,3); // ����� ���������+��������+����� ����������� ��� ���������
  ptr+=3;
  i=_xGetCP(); // �������� �� ��� ���������
  if(i)
  { // ������� ����� �����������
    MasSelInfo[Nklav].CP=i;
    Tikker[Nklav]=0;
    VedSelect[Nklav]=0;
    MasSelInfo[Nklav].NAbon=NAbon;
    memcpy(MasSelInfo[Nklav].MasChan,MassInData+5,NAbon*2); // ����������� ����������
    *ptr++='1'; *ptr++='C';
    Word2Str(ptr,i,2);
    ptr+=2;
    for(j=0;j<(NAbon*2);j+=2)
    { // ���������� ���� ���������
      data=MassInData[j+5];
      if((data & 0x7F)==0)
      { // ���������� �����
        if(data & 0x80) *ptr++='8'; // ���� ��������
        else *ptr++='0'; // ���� �������
        *ptr++='0';
      }
      else
      { // ����� �����������
        if(data & 0x80) *ptr++='9'; // ���� ��������
        else *ptr++='1'; // ���� �������
        *ptr++='C';
      }
      Word2Str(ptr,MassInData[j+6],2);
      ptr+=2;
/*{
  if((MassInData[j+5] & 0x80) && (MassInData[j+6]!=0))
  {
    LockPrintf();
    sprintf(strInfoSpXCmd,"$DEBUG$SportXCmd155: Nklav=%d, wChan=0x%04x",
            Nklav,MassInData[j+5]*256+MassInData[j+6]);
    UnlockPrintf();
    SendStr2IpLogger(strInfoSpXCmd);
  }
}*/
    }

/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$SportXCmd155: Selector name=%s",MasOut+2);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$SportXCmd155: Commute str=%s",MasOut+2+k);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/

    // !! ��� ������ ����� MasOut �� ������������ � ������� SaveGroupFlash !!
    btRes=SaveGroupFlash(MasOut,false,0);
    if(btRes)
    {
      if((ParamUKC[6] & 0x10) && (SN<16))
      { // �������������� ��������� �����������
        SelectorRecordOnOff(SN,1);
      }
      for(j=0;j<(NAbon*2);j+=2)
      { // ���� ��������� ���������� � ���������
        data=MassInData[j+6];
        if(data)
        {
          if((MassInData[j+5] & 0x7F)==0)
          { // ���������� �����
            if(RdFlash(AdrTypeCall+((data-1)*4)) & 0x04) SendBlockPPS(data,1);
          }
#ifdef __ISDN__
          else SelectEDSS[data-1]=Nklav+1; // ������������� ��� �������� ��������� ��� ���������� "������"
#endif
        }
      }
    }
  }
  return btRes;
}

unsigned char SelectorDelX(unsigned char *MasOut)
{
  unsigned char Nklav,NAbon,chan,
                j,btRes=0;

  Nklav=FindNKlavFromSN2Sel(MassInData[2]);
  if(Nklav!=0xff)
  {
#ifdef __ISDN__ 
    DelSound4Sel(Nklav);
#endif
    NAbon=MasSelInfo[Nklav].NAbon;
    for(j=0;j<NAbon;j++)
    { // ���������� ���� ���������
      chan=MasSelInfo[Nklav].MasChan[j];
      if(((chan & 0x100)==0) && (chan & 0xff))
      { // ���������� �����
        chan=chan & 0xff;
        if(RdFlash(AdrTypeCall+((chan-1)*4)) & 0x04) SendBlockPPS(chan,1);
      }
    } 
    SelectorRecordOnOff(MasSelInfo[Nklav].SN,0);
    _xFreeCP(MasSelInfo[Nklav].CP);
    chan=VedSelect[Nklav];
    if((chan>=LenSport1) && (chan<LenHard))
    { // �������� �������� �� ������ ���
      SetVedSelect4MKA(chan-LenSport1+1+128);
    }
    VedSelect[Nklav]=Tikker[Nklav]=0;
    GetNameSelector(MasSelInfo[Nklav].SN); // ������ ����� ������
    memcpy(MasOut,NameSelector,LenNameGroup+1);
    btRes=ExclGroup(MasOut);
    MasSelInfo[Nklav].SN=0;
  }
  return btRes;
}

unsigned char SelectorChanBlockX(unsigned char InCmd,unsigned char *MasOut)
{
  unsigned char Nklav,NAbon,chan,
                j,*ptr,
                btRes=0;

  Nklav=FindNKlavFromSN2Sel(MassInData[2]);
  if(Nklav!=0xff)
  {
    unsigned char btPPS=(InCmd==158) ? 0: 1;
    DelSound4Sel(Nklav);
    NAbon=MassInData[3];
    GetNameSelector(MasSelInfo[Nklav].SN);
    memcpy(MasOut,NameSelector,LenNameGroup+1);
    for(j=0;j<(NAbon*2);j+=2)
    { // ���������� ���� ���������
      ptr=MasOut+LenNameGroup+1;
      if((MassInData[j+4] & 0x7F)==0)
      { // ���������� �����
        *ptr++='0'; *ptr++='0';
        chan=MassInData[j+5];
        if(chan)
        {
          if(RdFlash(AdrTypeCall+((chan-1)*4)) & 0x04) SendBlockPPS(chan,btPPS);
        }
      }
      else
      { // ����� �����������
        *ptr++='1'; *ptr++='C';
      }
      Word2Str(ptr,MassInData[j+5],2);
      btRes=BlockInput(MasOut,(InCmd==158) ? 0xff: 0);
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$SportXCmd%d: cmdStr=%s, btRes=%d",InCmd,MasOut,btRes);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
    }
  }
  return btRes;
}

unsigned char SelectorDelChanX(unsigned char *MasOut)
{
  unsigned char Nklav,NAbon,chan,
                i,j,k,*ptr;

  Nklav=FindNKlavFromSN2Sel(MassInData[2]);
  if(Nklav!=0xff)
  {
    NAbon=MassInData[3];
    GetNameSelector(MasSelInfo[Nklav].SN);
    k=LenNameGroup+1;
    memcpy(MasOut,NameSelector,k);
    for(j=0;j<(NAbon*2);j+=2)
    {
      { // �������� ��������, ���� ��������� ����� ��������
        chan=MassInData[j+5];
        if(MassInData[j+4] & 0x7F) chan+=LenHard+LenVirt-1;
        else chan+=LenSport1-1;
        if(chan==VedSelect[Nklav])
        {
          if(!MassInData[j+4]) SetVedSelect4MKA(chan-LenSport1+1+128); // �������� �������� �� ������ ���
          VedSelect[Nklav]=0;
        }
      }
      ptr=MasOut+k;
      if((MassInData[j+4] & 0x7F)==0)
      { // ���������� �����
        *ptr++='0'; *ptr++='0';
        chan=MassInData[j+5];
        if(chan)
        {
          if(RdFlash(AdrTypeCall+((chan-1)*4))) SendBlockPPS(chan,1);
        }
      }
      else 
      { // ����� �����������
        *ptr++='1'; *ptr++='C';
      }
      Word2Str(ptr,MassInData[j+5],2);
      DelChanFromGroup(MasOut);
      for(i=0;i<MasAbntSelSz;i+=2)
      {
        if((MasSelInfo[Nklav].MasChan[i]==MassInData[j+4]) &&
           (MasSelInfo[Nklav].MasChan[i+1]==MassInData[j+5]))
        { // ������� ����� �� ������ ������� ���������
          MasSelInfo[Nklav].MasChan[i]=0x80;
          MasSelInfo[Nklav].MasChan[i+1]=0;
          return 1;
        }
      }
    }
  }
  return 0;
}

unsigned char SelectorAddChanX(unsigned char *MasOut)
{ // ����������� ����� ������� ��� ���������� ���������
  unsigned char Nklav,NAbon,chan,
                i,j,k,*ptr,
                btRes=0;

  Nklav=FindNKlavFromSN2Sel(MassInData[2]);
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$SportXCmd160: SN=%d, Nklav=%d",MassInData[2],Nklav);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
  if(Nklav!=0xff)
  {
    NAbon=MassInData[3];  // ����� ���������
    GetNameSelector(MasSelInfo[Nklav].SN);
    k=LenNameGroup+1;
    memcpy(MasOut,NameSelector,k);
    for(j=0;j<(NAbon*2);j+=2)
    { // ���������� ���� ���������
      ptr=MasOut+k;
      if((MassInData[j+4] & 0x7F)==0)
      { // ���������� �����
        if(MassInData[j+4] & 0x80) 
        { *ptr++='8'; i=1; }
        else 
        { *ptr++='0'; i=0; }
        *ptr++='0';
        chan=MassInData[j+5];
        if(chan)
        {
          if(RdFlash(AdrTypeCall+((chan-1)*4)) & 0x04) SendBlockPPS(chan,i);
        }
      }
      else
      { // ����� �����������
        if(MassInData[j+4] & 0x80) *ptr++='9';
        else *ptr++='1'; 
        *ptr++='C';
#ifdef __ISDN__
        SelectEDSS[MassInData[j+5]-1]=Nklav+1; // ������������� ��� �������� ��������� ��� ���������� "������"
#endif
      }
      Word2Str(ptr,MassInData[j+5],2);
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$SportXCmd160: Selector name =%s",MasOut);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$SportXCmd160: Commute str=%s",MasOut+k);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
      if(AddChan2Group(MasOut,0)!=1) break;
      for(i=0;i<MasAbntSelSz;i+=2)
      {
        if(MasSelInfo[Nklav].MasChan[i+1]==0)
        { // ��������� ����� �� ��������� ����� � ������ ������� ���������
          MasSelInfo[Nklav].MasChan[i]=MassInData[j+4];
          MasSelInfo[Nklav].MasChan[i+1]=MassInData[j+5];
          break;
        }
      }
    }
    if(j<(NAbon*2)) btRes=0;
    else btRes=1;
  }
  return btRes;
}

unsigned char SelectorGen4ChanOffX(void)
{
  unsigned char Nklav,NAbon,chan,i;
  static sCommand sCmd;

  Nklav=FindNKlavFromSN2Sel(MassInData[2]);
  if(Nklav!=0xff)
  {
    NAbon=MasSelInfo[Nklav].NAbon;
    for(i=0;i<(NAbon*2);i+=2)
    {
      chan=MasSelInfo[Nklav].MasChan[i+1];
      if(chan)
      {
        sCmd.Source=UserSportX;           // �������� ������� SportX
        sCmd.L=3;                    // ���������� ���� � �������
        sCmd.A1=A1;
        sCmd.PORT1=PORT1;
        sCmd.BufCommand[0]=41;            // ������� ���������� ��������� �� ������
        sCmd.BufCommand[1]=MasSelInfo[Nklav].MasChan[i] & 0x7F;   // ��� ������
        sCmd.BufCommand[2]=chan-1;        // ����� ������
        xQueueSend(xQueueCommand,&sCmd,0,0);
      }
    }
    return true;
  }
  return false;
}

unsigned char SelectorSetVedX(void)
{
  unsigned char Nklav,chan,count,i;
  unsigned short j;

  Nklav=FindNKlavFromSN2Sel(MassInData[2]);
  if(Nklav==0xff) return false;
  j=FoundPlaceSelector(Nklav); // ������ � TablSum
  if(j==0xffff) return false;
  DelSound4Sel(Nklav);
  count=TablSum[j++]; // ����� ������� ������ ������
  chan=VedSelect[Nklav];
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$SelectorSetVedX: Nklav=%d, off=%d, oldVed=%d",
          Nklav,j-1,chan);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
  if(chan)
  { // ��� �������
    if(MassInData[6]) return false;
    if((chan>=LenSport1) && (chan<LenHard))
    { // �������� �������� �� ������ ���
      SetVedSelect4MKA(chan-LenSport1+1+128);
    }
    VedSelect[Nklav]=0; // �������� ������ ��������
    for(i=0;i<count;i++,j++)
    { // �������������� ������
      TablBlock[j]=CopyTablBlock[j];
    }
    return true;
  }
  else 
  { // �������� �� ����
    if(MassInData[6])
    { // ���� ����� ����� ��������
      if(MassInData[5]) VedSelect[Nklav]=MassInData[6]+LenHard+LenVirt-1;
      else
      {
        VedSelect[Nklav]=MassInData[6]+LenSport1-1;
        SetVedSelect4MKA(MassInData[6]); // ������� �������� �� ������ ���
      }
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$SelectorSetVedX: off=%d, VedSelect=%d, count=%d",
          j-1,VedSelect[Nklav],count);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
      for(i=0;i<count;i++,j++)
      { // ������������� ����� ��������
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$SelectorSetVedX: TablSum[j]=%d",
          TablSum[j]);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
        if(TablSum[j]==VedSelect[Nklav]) 
        {
          TablBlock[j]=CopyTablBlock[j]=0xFF;
          break;
        } 
      }
    }
    return true;
  }
}

unsigned char SelectorTikkerX(void)
{
  unsigned char Nklav=FindNKlavFromSN2Sel(MassInData[2]);
  if(Nklav!=0xff)
  {
    Tikker[Nklav]=MassInData[3];
    TimeTikker[Nklav]=10;
    return true;
  }
  return false;
}

unsigned char _xGetSelectorCP(unsigned char SN)
{
  unsigned char Nklav;

  Nklav=FindNKlavFromSN2Sel(SN); // ����� ����������
  if(Nklav==0xff) return(0);
  if(!MasSelInfo[Nklav].SN) return(0);
  return MasSelInfo[Nklav].CP;
}

bool IsSelectorMessage(unsigned char NKlav)
{
  if(NKlav && MasSelInfo[NKlav-1].dwWavSz) return true;
  return false;
}

///////////////////////////////////////////////////////////////////////////////
static unsigned char MasSN4Conf[NumberKeyboard][NumberKonf+1];

void InitConfInfo(void)
{ memset(MasSN4Conf,0,sizeof(MasSN4Conf)); }

unsigned char *CreatGroupNameConf(unsigned char *MasOut,unsigned char SN,unsigned char Nconf)
{ // �������� ����� ������ ��� ����������� � MasOut
  unsigned char i;
  static unsigned char ConfName[LenNameGroup]={"Conf_XX_XX "};

  for(i=0;i<LenNameGroup;i++) MasOut[i]=ConfName[i];
  Word2Str(MasOut+5,SN,2);
  Word2Str(MasOut+8,Nconf,2);
  MasOut[11]=0; // ��������� ������ ������ ����=0
  return(MasOut+12);
}

unsigned char FindNKlavFromSN2Conf(unsigned char SN,unsigned char NConf)
{
  unsigned char btInd;

  for(btInd=0;btInd<NumberKeyboard;btInd++)
  {
    if(MasSN4Conf[btInd][NConf]==SN) return(btInd);
  }
  return(0xff);
}

unsigned char AddPultSN2MasConf(unsigned char SN,unsigned char NConf)
{
  unsigned char btInd;

  btInd=FindNKlavFromSN2Conf(SN,NConf);
  if(btInd!=0xff) return(btInd);
  for(btInd=0;btInd<NumberKeyboard;btInd++)
  {
    if(MasSN4Conf[btInd][NConf]==0)
    {
      MasSN4Conf[btInd][NConf]=SN;
      return(btInd);
    }
  }
  return(0xff);
}

unsigned char ConfId2Numb(unsigned char Id)
{
  if(Id<NumberKonf) return Id;
  return NumberKonf;
}

///////////////////////////////////////////////////////////////////////////////
// �������� ����������� (N ����.(1�.)+N ����.(1�.)+����� ��������� (2�.) +
//                       ���. ���������(1�.) * {��������(�����������)(�� 2�.)}
//  2      3     4       5               6        7        8-������ MassInData
//N����||N����||���+����� ���������||N���������||���+������ ���������|| ...
unsigned char ConferenceCreatX(unsigned char *MasOut)
{
  unsigned char Nconf,Nklav,Chan,Nabon,
                i,k,btTpChan,*ptr,
                btRes=0;

  Nconf=ConfId2Numb(MassInData[3]);
  Nklav=AddPultSN2MasConf(MassInData[2],Nconf);
  if(Nklav!=0xff)
  {
    Nabon=MassInData[6]; // ����� ���������
    ptr=CreatGroupNameConf(MasOut+2,MassInData[2],Nconf);
    Word2Str(ptr,Nabon+1,3);
    ptr+=3; // ����� ���������
    // ����� ���������
    if(MassInData[4]==0x01) // ��� ������ ���������
    { *ptr++='1'; *ptr++='C'; } // ����� �����������
    else
    { *ptr++='0'; *ptr++='0'; } // ���������� ���� 
    Word2Str(ptr,MassInData[5],2);   // ����� ������ ���������
    ptr+=2;
    i=7;
    for(k=0;k<Nabon;k++,i+=2) 
    {
      btTpChan=MassInData[i];
      Chan=MassInData[i+1];
      FoundDelTempComm(btTpChan,Chan); // ����� � �������� ��������� ���������� ������
      if(btTpChan==0x01)
      { // ����� �����������
        *ptr++='1'; *ptr++='C';
      }
      else
      {
        if(btTpChan==0x80)
        { *ptr++='8'; *ptr++='0'; } // ��������� �����
        else
        { // ���������� ����
          *ptr++='0'; *ptr++='0';
        }
      }
      Word2Str(ptr,Chan,2); // ����� ������
      ptr+=2; 
    }
    *ptr=0;
    btRes=SaveGroupFlash(MasOut,false,0); // �������� ������ �����������
  }
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$CreateConf: SN=%d, Nconf=%c, Result=%d",
          MassInData[2],Nconf+'1',btRes);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
  return btRes;
}

unsigned char ConferenceDelX(unsigned char *MasOut)
{
  unsigned char Nconf,Nklav;

  Nconf=ConfId2Numb(MassInData[3]);
  Nklav=FindNKlavFromSN2Conf(MassInData[2],Nconf);
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$DeleteConf: SN=%d, Nconf=%c, Nklav=%d",
          MassInData[2],Nconf+'1',Nklav);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
  if(Nklav!=0xff)
  {
    CreatGroupNameConf(MasOut,MassInData[2],Nconf);
    MasSN4Conf[Nklav][Nconf]=0;
    return ExclGroup(MasOut);
  }
  return 0;
}

unsigned char ConferenceAbntX(unsigned char *MasOut)
{
  unsigned char Nconf,Nklav,Chan,
                k,*ptr,
                btRes=0;

  Nconf=ConfId2Numb(MassInData[3]);
  Nklav=FindNKlavFromSN2Conf(MassInData[2],Nconf);
  if(Nklav!=0xff)
  {
    Chan=MassInData[5]; // ����� �����
    ptr=CreatGroupNameConf(MasOut,MassInData[2],Nconf);
    if(MassInData[4]) // ��� ������
    { *ptr++='1'; *ptr++='C'; }
    else
    { *ptr++='0'; *ptr++='0'; }
    Word2Str(ptr,Chan,2);
    *(ptr+2)=0;
    if(MassInData[6])
    { // ���������
      FoundDelTempComm(MassInData[4],Chan); // ����� � �������� ��������� ���������� ������
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$AddAbntConf: SN=%d, Nconf=%c, wChan=0x%x, Group=%s, CommuteStr=%s",
          MassInData[2],Nconf+'1',MassInData[4]*256+Chan,MasOut,MasOut+12);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
      btRes=AddChan2Group(MasOut,0);
    }
    else
    { // �������
      btRes=DelChanFromGroup(MasOut);
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$DelAbntConf: SN=%d, Nconf=%c, wChan=0x%x, Result=%d",
          MassInData[2],Nconf+'1',MassInData[4]*256+Chan,btRes);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
    }
  }
  return btRes;
}

unsigned char ConferenceOperatorX(unsigned char *MasOut)
{
  unsigned short Nconf,Nklav,Chan,Key,
                 i,j;

  Nconf=ConfId2Numb(MassInData[3]);
  Nklav=FindNKlavFromSN2Conf(MassInData[2],Nconf);
  if(Nklav!=0xff)
  {
    Key=MassInData[4]; // �������� (1), ��������� (0) ��� ������ �������� (2)
    CreatGroupNameConf(MasOut,MassInData[2],Nconf);
    j=FoundAdrGroup(MasOut); // ����� ������� ������
    if(j)
    { // ������ �������
      i=MasGroupInfo[j-1].wOff+1; // �������� ������ ��������� � �������� TablSum � TablBlock
/*{
  LockPrintf();
  sprintf(strInfoSpXCmd,"$DEBUG$ConferenceOperatorX: SN=%d, Nconf=%c, wOff=%d, Key=%d",
          MassInData[2],Nconf+'1',i,Key);
  UnlockPrintf();
  SendStr2IpLogger(strInfoSpXCmd);
}*/
      if(Key)
      {
        if(!TablSum[i] || !TablBlock[i])
        {
          if(MassInData[5]) Chan=MassInData[6]+LenHard+LenVirt-1;
          else Chan=MassInData[6]+LenSport1-1;
          TablSum[i]=Chan;
          if(Key==1) TablBlock[i]=0xFF; // �������� ��������� ����� �� ��������� ������ �� Key==1
        }
      }
      else
      { TablSum[i]=TablBlock[i]=0; }
      return true;
    }
  }
  return false;
}

