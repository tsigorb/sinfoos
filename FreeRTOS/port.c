#include <cdefBF533.h>
#include <ccblkfn.h>
#include <signal.h>
#include "FreeRTOS.h"
#include "task.h"

#define IVG_PLL_WAKEUP	P0_IVG
#define IVG_DMA_ERROR	P1_IVG
#define IVG_PPI_ERROR	P2_IVG
#define IVG_SPT0_ERROR	P3_IVG
#define IVG_SPI_ERROR	P4_IVG
#define IVG_SPT1_ERROR	P5_IVG
#define IVG_UART_ERROR	P6_IVG
#define IVG_RTC			P7_IVG
#define IVG_PPI			P8_IVG
#define IVG_SPT0_RX		P9_IVG
#define IVG_SPT0_TX		P10_IVG
#define IVG_SPT1_RX		P11_IVG
#define IVG_SPT1_TX		P12_IVG
#define IVG_SPI			P13_IVG
#define IVG_UART_RX		P14_IVG
#define IVG_UART_TX		P15_IVG
#define IVG_TIMER0		P16_IVG
#define IVG_TIMER1		P17_IVG
#define IVG_TIMER2		P18_IVG
#define IVG_PFA			P19_IVG
#define IVG_PFB			P20_IVG
#define IVG_MEMDMA0		P21_IVG
#define IVG_MEMDMA1		P22_IVG
#define IVG_SWDT		P23_IVG
//����� ��� SIC_IMASK 
#define IRQ_PLL_WAKEUP	0x00000001
#define IRQ_DMA_ERROR	0x00000002
#define IRQ_PPI_ERROR	0x00000004
#define IRQ_SPT0_ERROR	0x00000008
#define IRQ_SPT1_ERROR	0x00000010
#define IRQ_SPI_ERROR	0x00000020
#define IRQ_UART_ERROR	0x00000040
#define IRQ_RTC			0x00000080
#define IRQ_PPI			0x00000100
#define IRQ_SPT0_RX		0x00000200
#define IRQ_SPT0_TX		0x00000400
#define IRQ_SPT1_RX		0x00000800
#define IRQ_SPT1_TX		0x00001000
#define IRQ_SPI			0x00002000
#define IRQ_UART_RX		0x00004000
#define IRQ_UART_TX		0x00008000
#define IRQ_TIMER0		0x00010000
#define IRQ_TIMER1		0x00020000
#define IRQ_TIMER2		0x00040000
#define IRQ_PFA			0x00080000
#define IRQ_PFB			0x00100000
#define IRQ_MEMDMA0		0x00200000
#define IRQ_MEMDMA1		0x00400000
#define IRQ_SWDT		0x00800000

#define portNO_CRITICAL_NESTING     ((unsigned portLONG)0)

/////////////////////////////////////////////////////////////////////
static void portInit_Timers(void); // Setup Timer0 for the RTOS tick at the requested frequency
void vPortYield(void); // The ISR used by portYIELD(). This is installed as a trap handler

/////////////////////////////////////////////////////////////////////
extern volatile tskTCB * volatile pxCurrentTCB;
volatile bool g_bSwitchTask=true;

/////////////////////////////////////////////////////////////////////
volatile unsigned portLONG ulCriticalNesting = 0UL;

/*unsigned int WorkingTime[32]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
unsigned int TotalTime[32]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
unsigned int Percent[32]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};*/

/////////////////////////////////////////////////////////////////////
char *strlwr(char *pStr0)
{
  char chSb,*pStr=pStr0;

  chSb=*pStr;
  while(chSb)
  {
    if((chSb>='A') && (chSb<='Z')) *pStr='a'+(chSb-'A');
    pStr++;
    chSb=*pStr;
  }
  return pStr0;
}

unsigned int GetFreeTaskStackSize(void)
{
  unsigned int dwFreeStackSz=0xffffffff;

  volatile tskTCB *pxTCB=pxCurrentTCB;

  if(pxTCB) dwFreeStackSz=(unsigned int)(pxTCB->pxTopOfStack-pxTCB->pxStack);
  return(dwFreeStackSz);
}

void vTaskMonitoring(void)
{
/*
  unsigned int LastMks,j;
  volatile tskTCB *pxTCB;
  static unsigned int FirstMks=0;

  LastMks=*pTIMER0_COUNTER;
  if(FirstMks>LastMks) LastMks+=SCLK;
  pxTCB=pxCurrentTCB;
  if(pxTCB)
  {
    j=pxTCB->uxTCBNumber;
    if(j<32) WorkingTime[j]+=(LastMks-FirstMks);
  }
  FirstMks=*pTIMER0_COUNTER;
*/
}

void vTaskAnalizing(void)
{
/*  unsigned int WorkingTimeG,PercentTemp,j;

  WorkingTimeG=1;
  for(j=0;j<32;j++) WorkingTimeG+=WorkingTime[j];
  for(j=0;j<32;j++)
  {
    PercentTemp=WorkingTime[j]; // ���������� ������ �� ��������� ����� ��������
    WorkingTime[j]=0; // ������� ������ ������������� �������
    TotalTime[j]+=PercentTemp;  // ����� ����� ������ ������ � ���
    Percent[j]=PercentTemp*100/WorkingTimeG; // ������� �������� ����������
  }*/
}
 
/*-----------------------------------------------------------*/
void vPortEnterCritical(void)
{
  if(ulCriticalNesting==portNO_CRITICAL_NESTING ) portDISABLE_INTERRUPTS();
  ulCriticalNesting++;
}

void vPortExitCritical( void )
{
  if(ulCriticalNesting>portNO_CRITICAL_NESTING)
  {
    ulCriticalNesting--; // Decrement the nesting count as we are leaving a critical section
    if(ulCriticalNesting==portNO_CRITICAL_NESTING) portENABLE_INTERRUPTS(); // If the nesting level==0 then interrupts should be re-enabled
  }
}

void vPortDisableInterrupts( void )
{
	asm volatile ( "CLI R0;");
	asm volatile ( "BITCLR(R0, 10);" );
	asm volatile ( "STI R0;");
}

void vPortEnableInterrupts( void )
{
	asm volatile ( "CLI R0;");
	asm volatile ( "BITSET(R0, 10);" );
	asm volatile ( "STI R0;");
}

void vPortEnableSwitchTask(bool bEn)
{
  static int s_iDisableCount=0;
  unsigned int dwIRQ=cli();

  if(bEn)
  {
    if(s_iDisableCount>0) s_iDisableCount--;
    if(s_iDisableCount==0) g_bSwitchTask=true;
  }
  else
  { s_iDisableCount++; g_bSwitchTask=false; }
  sti(dwIRQ);
}

/*-----------------------------------------------------------
 * Implementation of functions defined in portable.h for the BlackFin port.
 *----------------------------------------------------------*/
void SaveRestoreContextContainer()
{
		asm volatile ( "_CpuSaveContext:");
		asm volatile ( ".global _CpuSaveContext;");
		asm volatile ( "	[--sp] = (r7:0);");		// Save all data and pointer registers
		asm volatile ( "	[--sp] = (p5:0);");
		asm volatile ( "	[--sp] = a0.W;");		// Save accumulators ( 40 bit )
		asm volatile ( "	[--sp] = a0.X;");
		asm volatile ( "	[--sp] = a1.W;");
		asm volatile ( "	[--sp] = a1.X;");
		asm volatile ( "	[--sp] = i0;");			// save loop control registers
		asm volatile ( "	[--sp] = i1;");
		asm volatile ( "	[--sp] = i2;");
		asm volatile ( "	[--sp] = i3;");
		asm volatile ( "	[--sp] = b0;");
		asm volatile ( "	[--sp] = b1;");
		asm volatile ( "	[--sp] = b2;");
		asm volatile ( "	[--sp] = b3;");
		asm volatile ( "	[--sp] = l0;");
		asm volatile ( "	[--sp] = l1;");
		asm volatile ( "	[--sp] = l2;");
		asm volatile ( "	[--sp] = l3;");
		asm volatile ( "	[--sp] = m0;");
		asm volatile ( "	[--sp] = m1;");
		asm volatile ( "	[--sp] = m2;");
		asm volatile ( "	[--sp] = m3;");
		asm volatile ( "	[--sp] = lc0;");		// save fast loop control registers
		asm volatile ( "	[--sp] = lc1;");
		asm volatile ( "	[--sp] = lt0;");
		asm volatile ( "	[--sp] = lt1;");
		asm volatile ( "	[--sp] = lb0;");
		asm volatile ( "	[--sp] = lb1;");
		asm volatile ( "	[--SP] = RETX;" );
		asm volatile ( "	[--SP] = RETN;" );
		asm volatile ( "	[--SP] = RETE;" );
		asm volatile ( "	[--SP] = SEQSTAT;" );
		asm volatile ( "	[--SP] = SYSCFG;" );
		asm volatile ( "	rts;");


		asm volatile ( "_CpuRestoreContext:");
		asm volatile ( ".global _CpuRestoreContext;");
		asm volatile ( "	SYSCFG = [SP++];" );
		asm volatile ( "	SEQSTAT = [SP++];" );
		asm volatile ( "	RETE = [SP++];" );
		asm volatile ( "	RETN = [SP++];" );
		asm volatile ( "	RETX = [SP++];" );
		asm volatile ( "	lb1 = [sp++];");
		asm volatile ( "	lb0 = [sp++];");
		asm volatile ( "	lt1 = [sp++];");
		asm volatile ( "	lt0 = [sp++];");
		asm volatile ( "	lc1 = [sp++];");
		asm volatile ( "	lc0 = [sp++];");
		asm volatile ( "	m3 = [sp++];");
		asm volatile ( "	m2 = [sp++];");
		asm volatile ( "	m1 = [sp++];");
		asm volatile ( "	m0 = [sp++];");
		asm volatile ( "	l3 = [sp++];");
		asm volatile ( "	l2 = [sp++];");
		asm volatile ( "	l1 = [sp++];");
		asm volatile ( "	l0 = [sp++];");
		asm volatile ( "	b3 = [sp++];");
		asm volatile ( "	b2 = [sp++];");
		asm volatile ( "	b1 = [sp++];");
		asm volatile ( "	b0 = [sp++];");
		asm volatile ( "	i3 = [sp++];");
		asm volatile ( "	i2 = [sp++];");
		asm volatile ( "	i1 = [sp++];");
		asm volatile ( "	i0 = [sp++];");
		asm volatile ( "	a1.X = [sp++];");
		asm volatile ( "	a1.W = [sp++];");
		asm volatile ( "	a0.X = [sp++];");
		asm volatile ( "	a0.W = [sp++];");
		asm volatile ( "	(p5:0) = [sp++];");
		asm volatile ( "	(r7:0) = [sp++];");
		asm volatile ( "	rts;");
}

/* 
 * See header file for description. 
 */
portSTACK_TYPE *pxPortInitialiseStackContainer( portSTACK_TYPE *pxTopOfStack, pdTASK_CODE pxCode, void *pvParameters )
{
//	static portSTACK_TYPE *LocpxTopOfStack;
	/* This requires an even address. */
//	LocpxTopOfStack = (portSTACK_TYPE *)(( unsigned portLONG ) pxTopOfStack & 0xfffffffC);

		asm volatile ( "_pxPortInitialiseStack:");
		asm volatile ( ".global _pxPortInitialiseStack;");

//		asm volatile ( "LINK 0x0c;");
		asm volatile ( "P0 = SP;");
		asm volatile ( "P1 = RETS;"); 		// return address
											
		asm volatile ( "SP = R0;"); // _pxTopOfStack
		asm volatile ( "R4.H = 0xaaaa;");	// Place a few bytes of known values on the bottom of the stack.
		asm volatile ( "R4.L = 0xaaaa;");	// Place a few bytes of known values on the bottom of the stack.
		asm volatile ( "[SP] = R4;");		// This is just useful for debugging.
		asm volatile ( "R4.H = 0xbbbb;");	// Place a few bytes of known values on the bottom of the stack.
		asm volatile ( "R4.L = 0xbbbb;");	// Place a few bytes of known values on the bottom of the stack.
		asm volatile ( "[--SP] = R4;");		// This is just useful for debugging.
		asm volatile ( "R4.H = 0xcccc;");	// Place a few bytes of known values on the bottom of the stack.
		asm volatile ( "R4.L = 0xcccc;");	// Place a few bytes of known values on the bottom of the stack.
		asm volatile ( "[--SP] = R4;");		// This is just useful for debugging.
		asm volatile ( "R4.H = 0xdddd;");	// Place a few bytes of known values on the bottom of the stack.
		asm volatile ( "R4.L = 0xdddd;");	// Place a few bytes of known values on the bottom of the stack.
		asm volatile ( "[--SP] = R4;");		// This is just useful for debugging.
		
		asm volatile ( "R4 = 0;");
		asm volatile ( "[--SP] = R4;");		// ASTAT
		asm volatile ( "[--SP] = R0;");  	// FP = _pxTopOfStack
		asm volatile ( "R0 = R2;"); 		// pvParameters
		asm volatile ( "[--SP] = RETS;" );
		asm volatile ( "[--SP] = R1;");  	// _pxCode

		asm volatile ( "CALL _CpuSaveContext;");
		asm volatile ( "[--SP] = R4;" );  // ulCriticalNesting as zero
		asm volatile ( "R0 = SP;");
		asm volatile ( "SP = P0;");
		asm volatile ( "JUMP (P1);");
	
	return pxTopOfStack;
}
/*-----------------------------------------------------------*/

portBASE_TYPE xPortStartScheduler( void )
{
  portInit_Timers(); // Setup the hardware to generate the tick
  *pEVT14=vPortYield; // ������ ������������ ���������� 14
  *pIMASK|=0x4000; // ���������� ���������� 14

		asm volatile ( ".extern _pxCurrentTCB;" );
		asm volatile ( ".extern _ulCriticalNesting;" );
		asm volatile ( "P1.H = _pxCurrentTCB;" );
		asm volatile ( "P1.L = _pxCurrentTCB;" );
		asm volatile ( "P1 = [P1];" );
		asm volatile ( "SP = [P1];" );
		asm volatile ( "R0 = [SP++];" );
		asm volatile ( "P1.H = _ulCriticalNesting;");
		asm volatile ( "P1.L = _ulCriticalNesting;");
		asm volatile ( "[P1] = R0;");

		asm volatile ( "CALL _CpuRestoreContext;");

		asm volatile ( "P0 = [SP++];" );
		asm volatile ( "RETS = [SP++];" );
		asm volatile ( "FP = [SP++];" );
		asm volatile ( "ASTAT = [SP++];" );
		asm volatile ( "JUMP (P0);");
	/* Restore the context of the first task that is going to run.  This
	mirrors the function epilogue code generated by the compiler when the
	"saveall" function attribute is used. */

	/* Should not get here. */
	return pdTRUE;
}
/*-----------------------------------------------------------*/

void vPortEndScheduler( void )
{
	/* It is unlikely that the h8 port will get stopped. */
}
/*-----------------------------------------------------------*/

/* Manual context switch.  This is a trap handler.  The "saveall" function
 * attribute is used so the context is saved by the compiler prologue.  All
 * we have to do is save the stack pointer.*/
//#pragma interrupt
//#pragma interrupt_reentrant
void vPortYieldContainer(void)
{
// extern void * pxCurrentTCB;
// SAVE CONTEXT
		asm volatile ( "_vPortYield:");
		asm volatile ( ".global _vPortYield;");
		asm volatile ( ".extern _pxCurrentTCB;");
		asm volatile ( ".extern _ulCriticalNesting;");
		asm volatile ( "[--SP] = ASTAT;");	// ASTAT
		asm volatile ( "[--SP] = FP;");  	// FP
		asm volatile ( "[--SP] = RETS;" );
		asm volatile ( "[--SP] = RETI;");  	// ����� ��������
		asm volatile ( "CALL _CpuSaveContext;");

		asm volatile ( "P1.H = _ulCriticalNesting;");
		asm volatile ( "P1.L = _ulCriticalNesting;");
		asm volatile ( "P1 = [P1];");
		asm volatile ( "[--SP] = P1;" );
		asm volatile ( "P1.H = _pxCurrentTCB;");
		asm volatile ( "P1.L = _pxCurrentTCB;");
		asm volatile ( "P1 = [P1];");
		asm volatile ( "	[P1] = SP;");

		asm volatile ( "	CLI R0;");
		asm volatile ( "	[--SP] = R0;" );
		asm volatile ( "	BITCLR(R0, 10);" );
		asm volatile ( "	STI R0;");

//		asm volatile ( ".extern _vTaskMonitoring;");
//		asm volatile ( "CALL _vTaskMonitoring;");

		asm volatile ( ".extern _vTaskSwitchContext;");
		asm volatile ( "CALL _vTaskSwitchContext;");

		asm volatile ( "R0 = [SP++];" );
		asm volatile ( "	STI R0;");

// RESTORE CONTEXT
		asm volatile ( "P1.H = _pxCurrentTCB;" );
		asm volatile ( "P1.L = _pxCurrentTCB;" );
		asm volatile ( "CC = P1 == 0;");
		asm volatile ( "IF !CC JUMP .DI;");
		asm volatile ( "IDLE;");
		
		
		asm volatile ( ".DI: ");
		
		asm volatile ( "P1 = [P1];" );
		asm volatile ( "SP = [P1];" );
		asm volatile ( "R0 = [SP++];" );
		asm volatile ( "P1.H = _ulCriticalNesting;");
		asm volatile ( "P1.L = _ulCriticalNesting;");
		asm volatile ( "[P1] = R0;");
		
		asm volatile ( "CC = R0 == 0;");
		asm volatile ( "IF !CC JUMP .DInterrupts;");
		asm volatile ( "CALL _vPortEnableInterrupts;");
		asm volatile ( "JUMP .EInterrupts;");
		asm volatile ( ".DInterrupts: ");
		asm volatile ( "CALL _vPortDisableInterrupts;");
		asm volatile ( ".EInterrupts: ");

		asm volatile ( "CALL _CpuRestoreContext;");
		asm volatile ( "RETI = [SP++];" );
		asm volatile ( "RETS = [SP++];" );
		asm volatile ( "FP = [SP++];" );
		asm volatile ( "ASTAT = [SP++];" );
		asm volatile ( "RTI;" );
}
/*-----------------------------------------------------------*/

/*  The interrupt handler installed for the RTOS tick depends on whether the 
 * preemptive or cooperative scheduler is being used. */
#if( configUSE_PREEMPTION == 1 )
	/* 
	 * The preemptive scheduler is used so the ISR calls vTaskSwitchContext().
	 * The function prologue saves the context so all we have to do is save
	 * the stack pointer.
	 */
	void vTickISR(int val)
	{
		vTaskIncrementTick();
       if(ulCriticalNesting == portNO_CRITICAL_NESTING) portYIELD();  // switch context after handling interrupt
	}

#else

	/*
	 * The cooperative scheduler is being used so all we have to do is 
	 * periodically increment the tick.  This can just be a normal ISR and
	 * the "saveall" attribute is not required.
	 */
	void vTickISR(int val)
	{
		// confirm interrupt handling
		*pTIMER_STATUS = 0x0001;
		vTaskIncrementTick();
	}

#endif
/*-----------------------------------------------------------*/

//--------------------------------------------------------------------------//
// Function:	Init_Timers													//
//																			//
// Parameters:	None														//
//																			//
// Return:		None														//
//																			//
// Description:	This function initialises Timer0 for PWM mode.				//
//				It is used as reference for the 'shift-clock'.				//
//--------------------------------------------------------------------------//
unsigned int GetSCLK(void);

void portInit_Timers(void)
{
  *pTIMER0_CONFIG=0x0019;
  *pTIMER0_PERIOD=GetSCLK();
  *pTIMER0_WIDTH=0x00000050;
  *pTIMER_ENABLE=0x0001;
}

