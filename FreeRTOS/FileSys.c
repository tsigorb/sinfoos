#include "..\Sinfo.h"
#include <string.h> 
#include <stdlib.h> 
#include <math.h>
#include "GlobalData.h"

/////////////////////////////////////////////////////////////////////
#define MAGICSTRLEN         16
#define d_FsSmaphoreTOut    1000

/////////////////////////////////////////////////////////////////////
void ToogleWD_Main(void);

void *pvPortMalloc(size_t xSize);
void vPortFree(void *pv);

unsigned short _xRdFlash(unsigned int AdrFl);
bool _xWrFlash(unsigned int AdrFl,unsigned short DanFl);
bool _xWrDeviceShort(unsigned int Addr, unsigned short bInp);
bool _xRdFlashP(const unsigned int InpAddr, unsigned short *lpOut, unsigned int *lpHowOut);
bool _xWrDeviceArray(unsigned int Addr0, unsigned short *lpInp, unsigned int HowInp);

//void SimplePiskPult(unsigned char PultNb);

/////////////////////////////////////////////////////////////////////
extern xSemaphoreHandle g_SPILock;

unsigned long AddrLastFile; // ����� ���������� ����� �� ����
unsigned short ErrLastFile=0; // ������ ����������� ���������� ����� �� ����
unsigned short ErrLastFile1=0; // ������ ����������� ���������� ����� �� ����
/*bool ProcessNorm = false;  // ������� �������� �����������
bool ProcessFindAddr = false;  // ������� �������� ������ ������ ���������� �����*/

static xSemaphoreHandle xFileSemaphore = NULL;
static unsigned char BufDataFlash[512];
static const char g_szMagicStr[] = "MaGiCdIgItPwW___";
static const char g_szMagicFreeStr[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

// ��������� �� ������ ���������� �����. ������ ���������� ��� ������������� �������� �������.
static FILESYS2MEM *g_FileDescriber=NULL;

////////////////////////////////////////////////////////////////////////////////////////////
unsigned long CalcRealFileSize(unsigned long dwFSz)
{
  if(dwFSz % FLASHGRANULATE) dwFSz=((dwFSz/FLASHGRANULATE)+1)*FLASHGRANULATE;
  return(dwFSz);
}

char *PrepareFName(char *lpszName)
{
  lpszName=strclr(lpszName);
  if(!lpszName || (!strlen(lpszName))) return NULL;
  if(strlen(lpszName)>=FileNameLEN) lpszName[FileNameLEN-1]=0;
  return(lpszName);
}

unsigned long GetLastGranulate(void)
{
  unsigned long FlashAddr=(FLASHMAXADDRESS-FLASHADDRESS)/FLASHGRANULATE;
  return(FlashAddr*FLASHGRANULATE);
}

void _xFindLastFile(void)
{
  unsigned int HowRd;
  unsigned long FlashAddr;
  static MYFILE stFile;

  FlashAddr=GetLastGranulate();
  while(FlashAddr)
  { // ����� ���������� �����
    if(xTaskGetTickCount()==0) ResetWatchDog(true); // ���� ���� ������������� � �� ��� �� ��������
    HowRd = sizeof(MYFILE) / 2;
    _xRdFlashP(FlashAddr+FLASHADDRESS,(unsigned short *)&stFile,&HowRd);
    if(memcmp(g_szMagicStr,stFile.szMagicStr,MAGICSTRINGLEN)==0)
    { // ��������� ��������� ����� �������
      if((stFile.FileFlag==FILE_WRITED) || (stFile.FileFlag==FILE_CLEARTOWRITE))
      { // ������ ���������� ����
        AddrLastFile=FlashAddr;
        _xWrFlash(AdrFlashLastFile,AddrLastFile & 0xffff); // ������ ������ ���������� ����� �������� �������
        _xWrFlash(AdrFlashLastFile+2,AddrLastFile >> 16);
        return;
      }
      else
      {
        if(FlashAddr>=FLASHGRANULATE) FlashAddr-=FLASHGRANULATE;
        else 
        { // ������� ��� �����
          ErrLastFile1++;
          FlashAddr=0;
        }
      }
    }
    else
    { // ��� ���������
      if(FlashAddr>=FLASHGRANULATE) FlashAddr-=FLASHGRANULATE;
      else
      { // ������ ����
        FlashAddr=0;
      }
    }
  }
  AddrLastFile=FlashAddr;
  _xWrFlash(AdrFlashLastFile,AddrLastFile & 0xffff); // ������ ������ ���������� ����� �������� �������
  _xWrFlash(AdrFlashLastFile+2,AddrLastFile >> 16);
}

void _xInitFileSystem(void)
{
  unsigned int DanFlL,DanFlH;

  memset(g_FileDescriber,0,MAX_OPENFILES*sizeof(FILESYS2MEM));
  DanFlH=_xRdFlash(AdrFlashLastFile+2);
  DanFlL=_xRdFlash(AdrFlashLastFile);
  AddrLastFile=(DanFlH << 16) | DanFlL; // ����� ���������� ����� �� ����
  if(AddrLastFile>=(FLASHMAXADDRESS-FLASHADDRESS)) _xFindLastFile();
}

bool InitFileSystem(void)
{
  vSemaphoreCreateBinary(xFileSemaphore);
  if(!xFileSemaphore) return(false);
  g_FileDescriber=(FILESYS2MEM *)pvPortMalloc(MAX_OPENFILES*sizeof(FILESYS2MEM));
  if(!g_FileDescriber)
  { xFileSemaphore=NULL; return(false); }
  xSemaphoreTake(xFileSemaphore);
  xSemaphoreTake(g_SPILock);
  _xInitFileSystem();
  xSemaphoreGive(g_SPILock);
  xSemaphoreGive(xFileSemaphore);
  return true;
}

void FormatFileSystem(void)
{
  int iHandle;

  xSemaphoreTake(xFileSemaphore);
  xSemaphoreTake(g_SPILock);
  AddrLastFile=FLASHMAXADDRESS-FLASHADDRESS; // ����� ���������� ����� �� ����
  _xWrFlash(AdrFlashLastFile,AddrLastFile & 0xffff); // ������ ������ ���������� ����� �������� �������
  _xWrFlash(AdrFlashLastFile+2,AddrLastFile >> 16);
  xSemaphoreGive(g_SPILock);
  WrDeviceErase(FLASHADDRESS,AddrLastFile); // ����� ������� ��������!!!
  xSemaphoreTake(g_SPILock);
  _xInitFileSystem();
  xSemaphoreGive(g_SPILock);
  xSemaphoreGive(xFileSemaphore);
}

// ������������� ��������� ������ �� �������� �������
// �������������� ��������� ������
// smByIndex - �� ����������� ������ � ������ ������
// smByName  - �� ����� �����
// smFreeSpace - ��������� ����� ��������� �������
// � ������ �������� ������ � �������� MYFILE *lpstFile ������������ ��������� �����.
// �������: - "-1" - � ������ ������� ��� ��������� ������ ������;
//          - ����� �������� ��� ������ ����� � ��������� ������
// ���������: int aMode - ����� ������ (������ ��������� ������� ��. ����)
//            void *Param - ��������� �� ��������, ����� �������� ������� �� ������ ������.
//            MYFILE *lpstFile - ��������� �� ��������� MYFILE, � ������� ������������ ������
/////////////////////////////////////////////////////////////////////////////////
#define smByIndex       1
#define smByName        2
#define smFreeSpace     3

typedef struct
{
  unsigned long dwAddr;
  int iSize;
} FREESPACETP;

unsigned long _xUniversalFileSearch(int aMode, void *Param,MYFILE *lpstFile)
{
  unsigned long FlashAddr;
  unsigned int HowRd,LenForCompare,LenFreeSpace,Index;
  bool bFlag;
  char *pszName0,*pszName1;
  FREESPACETP sFreeResult,sFreeCurrent;

  sFreeResult.dwAddr=0xffffffff;
  sFreeResult.iSize=0;
  sFreeCurrent=sFreeResult;
  if(aMode==smFreeSpace) 
  { // ������ �������� ���������� ������������
    LenFreeSpace=CalcRealFileSize((*(int *)Param)+sizeof(MYFILE));
  }
  if(aMode==smByName)
  {
    pszName0=PrepareFName((char *)Param);
    if(!pszName0) return (unsigned long)-1;
  }
  if(AddrLastFile==(FLASHMAXADDRESS-FLASHADDRESS))
  {
    if(aMode!=smFreeSpace) return (unsigned long)-1;
    return 0;
  }
  Index=0;
  bFlag=false;
  FlashAddr=0;
  while(FlashAddr<=AddrLastFile)
  { // ������ �� ���������� ����������� �����
    HowRd=sizeof(MYFILE)/2;
    _xRdFlashP(FlashAddr+FLASHADDRESS,(unsigned short *)lpstFile,&HowRd);
    if(memcmp(g_szMagicStr,lpstFile->szMagicStr,MAGICSTRINGLEN)==0)
	{ // ��������� ��������� ����� �������
	  lpstFile->szFileName[FileNameLEN-1]=0;
	  if((lpstFile->FileFlag==FILE_WRITED) || (lpstFile->FileFlag==FILE_CLEARTOWRITE))
	  {
		bFlag=false;  // ���������� ������� ������ ������ ��������� �������
		switch(aMode)
		{
	      case smByIndex:
		    if(Index==*((int *)Param))
		    {
		      lpstFile->AddrFile=FlashAddr; //����� �� ����
		      return Index;
		    }
		    Index++;
		    break;
		  case smByName:
		    pszName1=PrepareFName(lpstFile->szFileName);
		    if(pszName1)
		    {
		      LenForCompare=max(strlen(pszName0),strlen(pszName1));
			  if(memcmp(pszName0,strlwr(pszName1),LenForCompare)==0) return FlashAddr;
		    }
			break;
		}
	  }
	  else
      { // ���� FILE_DELETED
        if(!bFlag) 
        {
          sFreeCurrent.dwAddr=FlashAddr; 
          bFlag=true;
        }
      }
      if(lpstFile->FileLen>=0) FlashAddr+=CalcRealFileSize(lpstFile->FileLen+sizeof(MYFILE));
      else FlashAddr+=FLASHGRANULATE;
      if(bFlag && (aMode==smFreeSpace))
      {
        LenForCompare=FlashAddr-sFreeCurrent.dwAddr;
        if(LenForCompare==LenFreeSpace) return sFreeCurrent.dwAddr;
        if(LenForCompare>LenFreeSpace)
        { // ���� ���������� ���������� ����� ����� �������
          if(sFreeResult.dwAddr==0xffffffff)
          {
            sFreeResult.dwAddr=sFreeCurrent.dwAddr;
            sFreeResult.iSize=LenForCompare;
          }
          else
          {
            if(sFreeResult.iSize>LenForCompare)
            {
              sFreeResult.dwAddr=sFreeCurrent.dwAddr;
              sFreeResult.iSize=LenForCompare;
            }
          }
        }
      }
      memset(lpstFile,0x00,sizeof(MYFILE)); 
    }
    else
    { // �� ��������� - ����� ��������� ������� ����� �������
      if(aMode==smFreeSpace) 
      {
        if(!bFlag) 
        {
          sFreeCurrent.dwAddr=FlashAddr;
          bFlag=true;
        }
        FlashAddr+=FLASHGRANULATE;
        LenForCompare=FlashAddr-sFreeCurrent.dwAddr;
        if(LenForCompare==LenFreeSpace) return sFreeCurrent.dwAddr;
        if(LenForCompare>LenFreeSpace)
        { // ���� ���������� ���������� ����� ����� �������
          if(sFreeResult.dwAddr==0xffffffff)
          {
            sFreeResult.dwAddr=sFreeCurrent.dwAddr;
            sFreeResult.iSize=LenForCompare;
          }
          else
          {
            if(sFreeResult.iSize>LenForCompare)
            {
              sFreeResult.dwAddr=sFreeCurrent.dwAddr;
              sFreeResult.iSize=LenForCompare;
            }
          }
        }
      }
      else FlashAddr+=FLASHGRANULATE; // �� ���������, ����� �� smFreeSpace
    }
  }
  if(aMode==smFreeSpace)
  {
    if(sFreeResult.dwAddr!=0xffffffff) return(sFreeResult.dwAddr);
    if(FLASHMAXADDRESS-FLASHADDRESS-FlashAddr>=LenFreeSpace) return FlashAddr;
  }
  return (unsigned long)-1;
}

// ����� ������������ ����� ��� ������ �����
// �������: 0xffffffff - ���� ����� �����������, ����� ����� ������ ���������� �������
// ���������: int NeedSpace - ����������� ����� ��� ����� � ������.
unsigned long _xFindFreeArea(int NeedSpace)
{
  MYFILE stFile;
  return _xUniversalFileSearch(smFreeSpace, &NeedSpace, &stFile);
}

// ��������� ������ ������. ������ ������ �������������� ���������������� ������� 
// ��������� � ����������� �� ������� ������� Offset ������� � ����.
// � ������ �������� ������ � �������� MYFILE *lpstFile ������������ ��������� �����.
// �������: ������� ������, �������� ��� ������ ��� "-1" - � ������ ���� ������ ������ ��������.
// ���������: int Offset - ������, ���������� ��� ������ , �� ������ Flash ������.
//			MYFILE *lpstFile - ��������� �� ��������� MYFILE, � ������� ������������ ������
// Return Address In FlashMemory(������ ��������) OR 0xFFFFFFFF If Fail
unsigned long FindNextFile(int Offset, MYFILE *lpstFile)
{
  unsigned long dwRet=0xffffffff;

  xSemaphoreTake(xFileSemaphore);
  xSemaphoreTake(g_SPILock);
  dwRet=_xUniversalFileSearch(smByIndex, &Offset, lpstFile);
  xSemaphoreGive(g_SPILock);
  xSemaphoreGive(xFileSemaphore);
  return dwRet;
}

// �������: 0xffffffff - � ������ ���� ����� ��������.
//			� ������ �������� ������ � �������� MYFILE *lpstFile ������������ ��������� �����
// ���������: char *lpszFileName - ��� ����� (�� ����� 16 �������� � ����������� � ������)
//			MYFILE *lpstFile - ��������� �� ��������� MYFILE, � ������� ������������ ������
// Return Address In FlashMemory(������ ��������) OR 0xFFFFFFFF If Fail
unsigned long FindFileInternal(char *lpszFileName0, MYFILE *lpstFile)
{
  unsigned long dwRet=0xffffffff;
  static char FName[FileNameLEN];

  xSemaphoreTake(xFileSemaphore);
  xSemaphoreTake(g_SPILock);
  if(strlen(lpszFileName0)>=FileNameLEN) lpszFileName0[FileNameLEN-1]=0;
  strcpy(FName,lpszFileName0);
  dwRet=_xUniversalFileSearch(smByName,strlwr(FName),lpstFile);
  return dwRet;
}

unsigned long FindFile(char *lpszFileName,MYFILE *lpstFile)
{
  unsigned long dwRet=FindFileInternal(lpszFileName,lpstFile);
  xSemaphoreGive(g_SPILock);
  xSemaphoreGive(xFileSemaphore);
  return dwRet;
}

void FindLastFile(void)
{
  xSemaphoreTake(xFileSemaphore);
  xSemaphoreTake(g_SPILock);
  _xFindLastFile();
  xSemaphoreGive(g_SPILock);
  xSemaphoreGive(xFileSemaphore);
}

void _xDeleteFile(unsigned long FlashAddr)
{
  _xWrDeviceShort(FlashAddr+FLASHADDRESS,FILE_DELETED); // ������ ������
  if(FlashAddr==AddrLastFile)
  { // ���� ��� ��������� ���� - �������� AddrLastFile
    _xFindLastFile();
  }
}

// �������� �����
// ���������: char *lpszFileName - ��� ����� (�� 16 ��������, ������� ����� � ����������)
// �������: TRUE - ���� ���� ������
bool DeleteFile(char *lpszFileName)
{
  bool bRet=false;
  unsigned long FlashAddr;
  MYFILE stFile;

  FlashAddr=FindFileInternal(lpszFileName, &stFile);
  if(FlashAddr!=0xffffffff)
  { // ���� ����� ����
     bRet=true;
    _xDeleteFile(FlashAddr);
  }
  xSemaphoreGive(g_SPILock);
  xSemaphoreGive(xFileSemaphore);
  return bRet;
}

// �������: >= 0 ���� ���� ������, 0xffffffff - ���� ������
// ���������: 
// char *lpszName - ������ �� ����� 15 ��������
// unsigned int CreateMode - CREATE_ALWAYS - ��������� ������ 
//							 GENERIC_WRITE - ������� ���� ��� ������
//							 GENERIC_READ - ������� ���� ��� ������
// int WriteFileLen - ����� ������������� �����
HANDLE CreateFile(char *lpszName, unsigned int CreateMode, int WriteFileLen)
{
  int Index;
  unsigned long FlashAddr,RealFSzSrc,RealFSzDst;
  HANDLE ValRet=INVALID_HANDLE_VALUE;
  MYFILE stFile;
  int nHow;
 
  lpszName=PrepareFName(lpszName);
  if(!lpszName) return(INVALID_HANDLE_VALUE);
  memset(&stFile,0x00,sizeof(MYFILE)); 
  FlashAddr=FindFileInternal(lpszName,&stFile);
  switch(CreateMode)
  {
    case GENERIC_WRITE:
      if(FlashAddr!=0xffffffff) FlashAddr=0xffffffff; // ������ - ���� ��� �������
      else FlashAddr=_xFindFreeArea(WriteFileLen);
      break;
    case GENERIC_READ:
//      if(stFile.FileFlag!=FILE_WRITED) FlashAddr=0xffffffff; // ������ - ���� ��� �� ������� ���������
      break;
    case CREATE_ALWAYS:
      if(FlashAddr==0xffffffff) FlashAddr=_xFindFreeArea(WriteFileLen);
      else
      {
        RealFSzDst=CalcRealFileSize(WriteFileLen+sizeof(MYFILE));
        RealFSzSrc=CalcRealFileSize(stFile.FileLen+sizeof(MYFILE));
        if(RealFSzDst!=RealFSzSrc)
        {
          _xDeleteFile(FlashAddr);
          FlashAddr=_xFindFreeArea(WriteFileLen);
        }
      }
      CreateMode=GENERIC_WRITE;
      break;
    default:
      FlashAddr=0xffffffff; // ������ - ������������ �����
      break;
  }
  if(FlashAddr!=0xffffffff) 
  {
      Index=0;
      while(Index<MAX_OPENFILES)
      {
        if(g_FileDescriber[Index].FDFlags==FILE_FLAG_CLEAR)
        {
          ValRet=Index;
          g_FileDescriber[Index].FDLocation = FlashAddr;
          g_FileDescriber[Index].FDPosition = 0;
          g_FileDescriber[Index].dwOpenFlags = CreateMode;
          if(CreateMode & GENERIC_READ)
          {
            g_FileDescriber[Index].FDFileLen = stFile.FileLen;
            g_FileDescriber[Index].FDFlags = stFile.FileFlag;
          }
          if(CreateMode & GENERIC_WRITE)
          {
            g_FileDescriber[Index].FDFileLen = WriteFileLen;
            g_FileDescriber[Index].FDCRC = 0; // ����������� �����
            stFile.FileFlag = FILE_CLEARTOWRITE;
            stFile.FileLen = WriteFileLen;
            strcpy(stFile.szFileName,lpszName);
            stFile.FileTime = ReadClock();
            memcpy(stFile.szMagicStr, (unsigned char *)g_szMagicStr, MAGICSTRLEN);
            stFile.CRC = 0;
            nHow=sizeof(MYFILE)/2;
            _xWrDeviceArray(g_FileDescriber[Index].FDLocation+FLASHADDRESS,(unsigned short *)&stFile,nHow);
          }
          break;
        }
        else Index++;
      }
  }
  xSemaphoreGive(g_SPILock);
  xSemaphoreGive(xFileSemaphore);
  return ValRet;
}

// �������� �����
// �������: ������ TRUE
// ���������: HANDLE hFile - ��������� ����� ����� ���������� � ������� CreateFile
bool CloseHandle(HANDLE hFile)
{
  unsigned long FlashAddrCRC;

  xSemaphoreTake(xFileSemaphore);
  if((hFile>=0) && (hFile<MAX_OPENFILES))
  {
    xSemaphoreTake(g_SPILock);
//    vPortEnableSwitchTask(false);
	if(g_FileDescriber[hFile].dwOpenFlags & GENERIC_WRITE)
	{ // ��� ������ ��� ������
      FlashAddrCRC=g_FileDescriber[hFile].FDLocation+OFFSETTOFILECRC+FLASHADDRESS;
      _xWrDeviceShort((unsigned int)FlashAddrCRC,(unsigned short)g_FileDescriber[hFile].FDCRC);
	  if(g_FileDescriber[hFile].FDPosition==g_FileDescriber[hFile].FDFileLen)
	  { // ���� ��������� �������
	    _xWrDeviceShort((unsigned int)(g_FileDescriber[hFile].FDLocation+FLASHADDRESS),FILE_WRITED);
	  }
	  if((AddrLastFile==(FLASHMAXADDRESS-FLASHADDRESS)) || (g_FileDescriber[hFile].FDLocation>AddrLastFile))
	  {
	    AddrLastFile=g_FileDescriber[hFile].FDLocation;
		_xWrFlash(AdrFlashLastFile,AddrLastFile & 0xffff); // ������ ������ ���������� ����� �������� �������
		_xWrFlash(AdrFlashLastFile+2,AddrLastFile >> 16); // 
	  }
	}
    memset(&g_FileDescriber[hFile],0x00,sizeof(FILESYS2MEM)); 
//    vPortEnableSwitchTask(true);
    xSemaphoreGive(g_SPILock);
  }
  xSemaphoreGive(xFileSemaphore);
  return true;
}

int ReadFileLP(HANDLE hFile, unsigned long *lpFile)
{
  unsigned int FDFileLen=0xffffffff;

  xSemaphoreTake(xFileSemaphore);
  if((hFile>=0) && (hFile<MAX_OPENFILES))
  {
    if(lpFile) *lpFile=g_FileDescriber[hFile].FDLocation+OFFSETTOFILEDATA;
    FDFileLen=g_FileDescriber[hFile].FDFileLen;
  }
  xSemaphoreGive(xFileSemaphore);
  return FDFileLen;
}

unsigned int GetFileSize(HANDLE hFile)
{
  unsigned int FDFileLen=0xffffffff;

  xSemaphoreTake(xFileSemaphore);
  if((hFile>=0) && (hFile<MAX_OPENFILES)) FDFileLen=g_FileDescriber[hFile].FDFileLen;
  xSemaphoreGive(xFileSemaphore);
  return FDFileLen;
}

// ���������: HANDLE hFile - ��������� �����, ���������� � ������� CreateFile
//				void *lpDst - ��������� �� ����� - ��������
//				int HowDst - ���������� ����������� ����
// ���������: ��� ������� ������ ��������� ��������� ����� ������������ �� �������� ����������� ����
int _xReadFile(HANDLE hFile,void *lpDst,int HowDst)
{
  unsigned long FlashAddr0,FlashAddr;
  unsigned int HowRd,HowRdAll;
  unsigned char *pbtBuf;
  static unsigned short wRdBuf[1025];

  if(HowDst<0) HowDst=g_FileDescriber[hFile].FDFileLen-g_FileDescriber[hFile].FDPosition;
  else
  {
    HowRd=g_FileDescriber[hFile].FDFileLen-g_FileDescriber[hFile].FDPosition;
    if(HowDst>HowRd) HowDst=HowRd;
  }
  HowRdAll=0;
  while(HowDst>0)
  {
    HowRd=HowDst >> 1;
    if(HowRd>1024) HowRd=1024;
    else
    {
      if(HowDst & 0x1) HowRd++;
    }
    FlashAddr0=(g_FileDescriber[hFile].FDLocation+OFFSETTOFILEDATA+
                g_FileDescriber[hFile].FDPosition)+FLASHADDRESS;
    FlashAddr=FlashAddr0;
    pbtBuf=(unsigned char *)wRdBuf;
    if(FlashAddr0 & 0x01)
    {
      FlashAddr--;
      HowRd++;
      pbtBuf++;
    }
    if(_xRdFlashP(FlashAddr,wRdBuf,&HowRd)==false) return(-1);
    HowRd<<=1;
    if(FlashAddr0 & 0x01) HowRd--;
    if((HowRd<2048) && (HowDst & 0x01)) HowRd--; 
    memcpy((unsigned char *)lpDst+HowRdAll,pbtBuf,HowRd);
    HowRdAll+=HowRd;
    HowDst-=HowRd;
    g_FileDescriber[hFile].FDPosition+=HowRd;
  }
  return(HowRdAll);
}

int ReadFile(HANDLE hFile,void *lpDst,int HowDst)
{
  int HowRdAll=-1;
  xSemaphoreTake(xFileSemaphore);
  if((hFile>=0) && (hFile<MAX_OPENFILES))
  {
    xSemaphoreTake(g_SPILock);
//    vPortEnableSwitchTask(false);
    HowRdAll=_xReadFile(hFile,lpDst,HowDst);
//    vPortEnableSwitchTask(true);
    xSemaphoreGive(g_SPILock);
  }
  xSemaphoreGive(xFileSemaphore);
  return(HowRdAll);
}

int WriteFile(HANDLE hFile, void *lpSrc, int HowSrc)
{
  unsigned long FlashAddr;
  unsigned char *pbtBuf,btBuf[2];
  int HowWr, i;

  xSemaphoreTake(xFileSemaphore);
  if((hFile>=0) && (hFile<MAX_OPENFILES))
  {
    if(!HowSrc || (g_FileDescriber[hFile].FDFileLen-g_FileDescriber[hFile].FDPosition<HowSrc))
    {
      xSemaphoreGive(xFileSemaphore);
      return 0;
    }
    xSemaphoreTake(g_SPILock);
//    vPortEnableSwitchTask(false);
    pbtBuf=(unsigned char *)lpSrc;
    if(g_FileDescriber[hFile].FDPosition & 0x01)
    {
      g_FileDescriber[hFile].FDPosition--;
      if(_xReadFile(hFile,btBuf,2)!=2)
      {
//        vPortEnableSwitchTask(true);
        xSemaphoreGive(g_SPILock);
        xSemaphoreGive(xFileSemaphore);
        return(0);
      }
      g_FileDescriber[hFile].FDPosition-=2;
      btBuf[1]=*pbtBuf;
      FlashAddr=g_FileDescriber[hFile].FDLocation+OFFSETTOFILEDATA+g_FileDescriber[hFile].FDPosition;
      if(!_xWrDeviceArray((unsigned int)FlashAddr+FLASHADDRESS,(unsigned short *)btBuf,1))
      {
//        vPortEnableSwitchTask(true);
        xSemaphoreGive(g_SPILock);
        xSemaphoreGive(xFileSemaphore);
        return(0);
      }
      g_FileDescriber[hFile].FDPosition+=2;
      g_FileDescriber[hFile].FDCRC+=btBuf[1];
      HowSrc--;
      if(!HowSrc)
      {
//        vPortEnableSwitchTask(true);
        xSemaphoreGive(g_SPILock);
        xSemaphoreGive(xFileSemaphore);
        return(1);
      }
      pbtBuf++;
    }
    if(HowSrc & 0x01) HowWr=(HowSrc+1) >> 1;
    else HowWr=HowSrc >> 1;
    FlashAddr=g_FileDescriber[hFile].FDLocation+OFFSETTOFILEDATA+g_FileDescriber[hFile].FDPosition;
    if(_xWrDeviceArray((unsigned int)FlashAddr+FLASHADDRESS,(unsigned short *)pbtBuf,HowWr))
    {
      for(i=0;i<HowSrc;i++) g_FileDescriber[hFile].FDCRC+=*pbtBuf++;
      g_FileDescriber[hFile].FDPosition+=HowSrc;
    }
    else HowSrc=0;
//    vPortEnableSwitchTask(true);
    xSemaphoreGive(g_SPILock);
  }
  else HowSrc=0;
  xSemaphoreGive(xFileSemaphore);
  return HowSrc;
}

char RenameFile(char *lpszFileNameOld, char *lpszFileNameNew)
{
  char chRes;
  unsigned long FlashAddr;
  MYFILE stFile;

  lpszFileNameNew=PrepareFName(lpszFileNameNew);
  if(!lpszFileNameNew) return(0);
  FlashAddr=FindFileInternal(lpszFileNameOld,&stFile);
  if(FlashAddr!=0xffffffff)
  { // ���� ����� ����
    memset(stFile.szFileName,0,LenFileName);
    strncpy(stFile.szFileName,lpszFileNameNew,LenFileName);
    chRes=_xWrDeviceArray(FlashAddr+FLASHADDRESS,(unsigned short *)&stFile,sizeof(MYFILE) >> 1); // ������ �� ����
  }
  else chRes=2;
  xSemaphoreGive(g_SPILock);
  xSemaphoreGive(xFileSemaphore);
  return(chRes);
}

// �������� ��������� ������� ������
// �������: ��������� ������� ������, ��� -1 � ������ ��������
// ���������: int Key ����� ��������� ��������:
// 				#define FREESPACEFLASH		0			// ��� ��������� ������
// 				#define FREECONTINUOUSLY	1			// ������������ ����������� �������
unsigned long GetFreeSpace(int Key)
{
  unsigned int Index, nHow,iFSz;
  bool bContinued=true;
  unsigned long FlashAddr=0, StartAddr=0xfffff;
  unsigned long FreeSpaceTotal=0, FreeSpaceMax=0,FreeSpaceContinued=0;
  static MYFILE sFI;

  xSemaphoreTake(xFileSemaphore);
  if(AddrLastFile==FLASHMAXADDRESS-FLASHADDRESS)
  {
    xSemaphoreGive(xFileSemaphore);
    return(FLASHMAXADDRESS-FLASHADDRESS+1);
  }
  xSemaphoreTake(g_SPILock);
  Index=0;
  while(Index<=AddrLastFile/FLASHGRANULATE)
  {
    nHow = sizeof(MYFILE) / 2;
    FlashAddr=Index*FLASHGRANULATE;
    _xRdFlashP(FlashAddr+FLASHADDRESS,(unsigned short *)&sFI,&nHow);
    if(memcmp(g_szMagicStr,sFI.szMagicStr,MAGICSTRINGLEN)==0)
    { // ��������� ��������� ����� �������
      iFSz=sizeof(MYFILE)+sFI.FileLen;
      if((sFI.FileFlag==FILE_WRITED) || (sFI.FileFlag==FILE_CLEARTOWRITE))
      {
        if(bContinued)
        {
          bContinued=false;
          if(FreeSpaceContinued>FreeSpaceMax) FreeSpaceMax=FreeSpaceContinued;
          FreeSpaceContinued=0;
        }
      }
      else
      {
        if(!bContinued) bContinued=true;
        nHow=CalcRealFileSize(iFSz);
        FreeSpaceContinued+=nHow;
        FreeSpaceTotal+=nHow;
      }
      Index+=(iFSz / FLASHGRANULATE);
      if(iFSz % FLASHGRANULATE) Index++;
    }
    else
    {
      iFSz=0;
      if(!bContinued) bContinued=true;
      FreeSpaceContinued+=FLASHGRANULATE;
      FreeSpaceTotal+=FLASHGRANULATE;
      Index++;
    }
  }
  FreeSpaceContinued=(FLASHMAXADDRESS-FLASHADDRESS)-(AddrLastFile+iFSz);
  FreeSpaceTotal+=FreeSpaceContinued;
  if(FreeSpaceMax<FreeSpaceContinued) FreeSpaceMax=FreeSpaceContinued;
  xSemaphoreGive(g_SPILock);
  xSemaphoreGive(xFileSemaphore);
  if(Key==FREESPACEFLASH) return(FreeSpaceTotal);
  else return(FreeSpaceMax);
}

bool ReadCRC(char *lpszFileName, unsigned short *pCRC)
{
  unsigned int HowWr;
  HANDLE hFile;

  hFile=CreateFile(lpszFileName,GENERIC_READ,0);
  if(hFile>=0)
  {
    xSemaphoreTake(xFileSemaphore);
    xSemaphoreTake(g_SPILock);
    HowWr=1;
    _xRdFlashP(g_FileDescriber[hFile].FDLocation+OFFSETTOFILECRC+FLASHADDRESS,
               pCRC,&HowWr);
    xSemaphoreGive(g_SPILock);
    xSemaphoreGive(xFileSemaphore);
    CloseHandle(hFile);
    return true;
  }
  return false;
}

// ���������� ��������� �����
// �������: ����� ��������� ����� ��� INVALID_SET_FILE_POINTER, ���� ������
// ���������: HANDLE hFile - ��������� ����� ����� ���������� � ������� CreateFile
//			unsigned long DistanceToMove - �� ������� ����������� ���������
//			int dwMoveMethod - ����� ������������ ���������(�� ������� �������, �� ������, �� �����)
unsigned long SetFilePointer(HANDLE hFile,unsigned long DistanceToMove,int dwMoveMethod)
{
  unsigned long dwNewPos=(unsigned long)INVALID_SET_FILE_POINTER;

  xSemaphoreTake(xFileSemaphore);
  if((hFile>=0) && (hFile<MAX_OPENFILES))
  {
    if((g_FileDescriber[hFile].dwOpenFlags==GENERIC_WRITE) ||
       (g_FileDescriber[hFile].dwOpenFlags==GENERIC_READ))
    {
      switch(dwMoveMethod)
      {
        case FILE_CURRENT:
          if(g_FileDescriber[hFile].FDFileLen>=(g_FileDescriber[hFile].FDPosition+DistanceToMove))
          { g_FileDescriber[hFile].FDPosition+=DistanceToMove; dwNewPos=g_FileDescriber[hFile].FDPosition; }
          break;
        case FILE_BEGIN:
          if(g_FileDescriber[hFile].FDFileLen>=DistanceToMove)
          { g_FileDescriber[hFile].FDPosition=DistanceToMove; dwNewPos=g_FileDescriber[hFile].FDPosition; }
          break;
        case FILE_END:
          if(DistanceToMove<=g_FileDescriber[hFile].FDFileLen)
          {
            g_FileDescriber[hFile].FDPosition=g_FileDescriber[hFile].FDFileLen-DistanceToMove;
            dwNewPos=g_FileDescriber[hFile].FDPosition;
          }
          break;
      }
    }
  }
  xSemaphoreGive(xFileSemaphore);
  return dwNewPos;
}

// ������� ������� ������� � �����
// �������: ������� ��������� �����
// ���������: HANDLE hFile - ��������� ����� ����� ���������� � ������� CreateFile
unsigned long GetFilePointer( HANDLE hFile)
{
  unsigned long dwPos=(unsigned long)INVALID_SET_FILE_POINTER;

  xSemaphoreTake(xFileSemaphore);
  if((hFile>=00) && (hFile<MAX_OPENFILES))
  {
    if((g_FileDescriber[hFile].dwOpenFlags==GENERIC_WRITE) || (g_FileDescriber[hFile].dwOpenFlags==GENERIC_READ))
    { dwPos=g_FileDescriber[hFile].FDPosition; }
  }
  xSemaphoreGive(xFileSemaphore);
  return dwPos;
}

