#pragma once 

#ifndef FILE_SYSTEM_H_INCLUDED
#define FILE_SYSTEM_H_INCLUDED

#include <time.h>

#define _DEBUG_PWW

#define MAX_OPENFILES		 10			// ������������ ���������� �������� ������(�������� � g_FileDescriber)
#define INVALID_HANDLE_VALUE -1			// ������ �������� �����

#define FILE_INVALID	  	-1
#define FILENAME_INVALID  	-2
#define FILEWRITE_INVALID  	-3
#define FILEREAD_INVALID  	-4
#define READBYFF_INVALID  	-5
#define MEMWR_INVALID	  	-6

// ����� ��� �������� ����� (� ������� CreateFile)
//#define OPEN_EXISTING		0x00000000	// ������� ������������
#define CREATE_ALWAYS		0x00000002	// ������� ��� ������ ������
#define GENERIC_WRITE 		0x00000100	// ������� ��� ������
#define GENERIC_READ		0x00000200	// ������� ��� ������

#define SIGNATURE_WRITED	0x00008000	// 

// ������������ ��� ����������� ������ �� Flash
#define FILE_FLAG_CLEAR		0x00000
#define FILE_CLEARTOWRITE	0xffff		      // ����. ���� ����� ��� ������
#define FILE_WRITED			0xfffe		      // ����. ���� ������� �� Flash
#define FILE_DELETED		FILE_FLAG_CLEAR   // ����. ���� ������

//
#define FLASHGRANULATE      64          // ������ "��������" Flash ������
#define FLASHADDRESS        0x100000ul  // ��������� ����� �������� ������� � AT45DB161
#define FLASHMAXADDRESS     0x3e8000ul  // �������� ����� �������� ������� � AT45DB161 (2-� ����������!)
//
#define FREESPACEFLASH		0			// ��� ��������� ������
#define FREECONTINUOUSLY	1			// ������������ ����������� �������
//
#define MAGICSTRINGLEN      16			// 
#define FileNameLEN         16			// 
//
#define HANDLE	int

#define INVALID_SET_FILE_POINTER	-1		// ������ ��������� ��������� �����
#define FILE_CURRENT	0		// �� ������� �������
#define FILE_BEGIN 		1		// �� ������
#define FILE_END 		2		// �� �����

#pragma pack(2) 
typedef struct tag_MyFile 
{ // size MYFILE = 48 ����
	unsigned short FileFlag; // �����
	char szFileName[FileNameLEN]; // ��� �����
	time_t  FileTime; // ����� ��������
	int FileLen; // ����� �����
	char szMagicStr[MAGICSTRINGLEN];
	unsigned short CRC;		// ����������� �����
	unsigned long AddrFile; // ����� ����� �� ����
} MYFILE;
#pragma pack() 
// 
// ������ � �������������???????????????????????????
#define OFFSETTOFILELEN		(sizeof(short)+FileNameLEN+sizeof(unsigned long))
#define OFFSETTOMAGICSTR	(OFFSETTOFILELEN+sizeof(int))
#define OFFSETTOFILEDATA	(sizeof(MYFILE))
#define OFFSETTOFILECRC     (OFFSETTOFILEDATA-sizeof(unsigned long)-sizeof(short))

// ��������� ��������� �����
typedef struct
{
	int FDFileLen;				//	����� �����
	unsigned int FDFlags;		// ����� 
	unsigned long FDLocation;	// ������������ ����� �� Flash-������
	unsigned short FDCRC;		// ����������� �����
	unsigned int dwOpenFlags;	// ����� 
	int FDPosition;				// ������� ������� ��������� �����
} FILESYS2MEM;
//

bool InitFileSystem(void);
bool TerminateFileSystem(void);
void FormatFileSystem(void);

// �������: >= 0 ���� ���� ������, 0xffffffff - ���� ������
// ���������: 
// char *lpszName - ������ ������ �� ����� 16 ��������
// unsigned int CreateMode - CREATE_ALWAYS - ��������� ������ 
//							GENERIC_WRITE - ������� ���� ��� ������
//							GENERIC_READ - ������� ���� ��� ������
// int WriteFileLen - ����� ������������� �����
HANDLE CreateFile(char *lpszName, unsigned int CreateMode, int WriteFileLen);
bool CloseHandle(HANDLE hFile);
void FindLastFile(void);
bool DeleteFile(char *lpszFileName);
char RenameFile(char *lpszFileNameOld, char *lpszFileNameNew);
unsigned int GetFileSize(HANDLE hFile);
int WriteFile(HANDLE hFile, void *lpSrc, int HowSrc);
int ReadFile(HANDLE hFile, void *lpDst, int HowDst);
int ReadFileLP(HANDLE hFile, unsigned long *lpFile);
unsigned long FindNextFile(int Offset, MYFILE *lpstFile);
unsigned long FindFile(char *lpszFileName, MYFILE *lpstFile);

// �������� ��������� ������� ������
// �������: ��������� ������� ������, ��� -1 � ������ ��������
// ���������: int Key ����� ��������� ��������:
// 				#define FREESPACEFLASH		0			// ��� ��������� ������
// 				#define FREECONTINUOUSLY	1			// ������������ ����������� �������
unsigned long GetFreeSpace(int Key);
void FindAddrLastFile(void);
bool ReadCRC(char *lpszFileName, unsigned short *pCRC);
unsigned long SetFilePointer(HANDLE hFile,unsigned long DistanceToMove,int dwMoveMethod);
unsigned long GetFilePointer( HANDLE hFile);

#endif //FILE_SYSTEM_H_INCLUDED

