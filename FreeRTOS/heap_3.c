/*
	FreeRTOS V3.2.3 - Copyright (C) 2003-2005 Richard Barry.

	This file is part of the FreeRTOS distribution.

	FreeRTOS is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	FreeRTOS is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with FreeRTOS; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

	A special exception to the GPL can be applied should you wish to distribute
	a combined work that includes FreeRTOS, without being obliged to provide
	the source code for any proprietary components.  See the licensing section 
	of http://www.FreeRTOS.org for full details of how and when the exception
	can be applied.

	***************************************************************************
	See http://www.FreeRTOS.org for documentation, latest information, license 
	and contact details.  Please ensure to read the configuration and relevant 
	port sections of the online documentation.
	***************************************************************************
*/


/* Implementation of pvPortMalloc() and vPortFree() that relies on the
 * compilers own malloc() and free() implementations.
 *
 * This file can only be used if the linker is configured to to generate
 * a heap memory area.
 *
 * See heap_2.c and heap_1.c for alternative implementations, and the memory
 * management pages of http://www.FreeRTOS.org for more information. */

#include "..\FreeRTOS/FreeRTOS.h"
#include "..\FreeRTOS/queue.h"
#include "..\FreeRTOS/task.h"
#include "..\FreeRTOS/semphr.h"

// ###############################################################################
extern volatile tskTCB * volatile pxCurrentTCB;

// ###############################################################################
int g_iHeapIndex=-1;

// �������� ����
unsigned int MallocSize[32]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
unsigned int FreeSize[32]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};;

void *pvPortMalloc(size_t xWantedSize)
{
  void *pvReturn;

  vTaskSuspendAll();
  pvReturn=heap_malloc(g_iHeapIndex,xWantedSize);
  if(pvReturn)
  {
    volatile tskTCB *pxTCB=pxCurrentTCB;
    if(pxTCB)
    {
      int j=pxTCB->uxTCBNumber;
      if(j<32) MallocSize[j]+=xWantedSize;
    }
  }
  xTaskResumeAll();
  return pvReturn;
}

void vPortFree(void *pv)
{
  if(pv)
  {
    volatile tskTCB *pxTCB;

    vTaskSuspendAll();
    pxTCB=pxCurrentTCB;
    if(pxTCB)
    {
      int j=pxTCB->uxTCBNumber;
      if(j<32) FreeSize[j]+=*((int *)((int)pv-8));
    }
    heap_free(g_iHeapIndex,pv);
    xTaskResumeAll();
  }
}

// ###############################################################################
/*-----------------------------------------------------------*/
/*
void *pvPortMalloc( size_t xWantedSize )
{
void *pvReturn;

	vTaskSuspendAll();
	{
		pvReturn = malloc( xWantedSize );
	}
	xTaskResumeAll();

	return pvReturn;
}
//-----------------------------------------------------------

void vPortFree( void *pv )
{
	if( pv )
	{
		vTaskSuspendAll();
		{
			free( pv );
			pv = NULL;
		}
		xTaskResumeAll();
	}
}
*/


