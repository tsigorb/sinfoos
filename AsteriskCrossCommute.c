#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "EDSS/pri_config.h"

///////////////////////////////////////////////////////////////////////////////
void *pvPortMalloc(size_t xWantedSize);
void vPortFree(void *pv);

#define d_MaxCrossCommute       30
#define d_MaxElementDP          50

typedef struct
{
  unsigned short wCPSrs,wCPDst;
  unsigned char btE1Srs,btE1Dst;
} CrossCommuteTP;

typedef struct
{
  unsigned long dwNumbStart,dwNumbEnd;
  unsigned char btE1;
} DialPlanElementTP;

///////////////////////////////////////////////////////////////////////////////
CrossCommuteTP MassCrossCommute[d_MaxCrossCommute];

DialPlanElementTP DialPlan[d_MaxElementDP];

///////////////////////////////////////////////////////////////////////////////
void DialplanElementAdd(unsigned int dwNumbStart,unsigned int dwNumbEnd,unsigned char btE1)
{
  unsigned char btInd;

  for(btInd=0;btInd<d_MaxElementDP;btInd++)
  {
    if(!DialPlan[btInd].dwNumbStart)
    {
      DialPlan[btInd].dwNumbStart=dwNumbStart;
      DialPlan[btInd].dwNumbEnd=dwNumbEnd;
      DialPlan[btInd].btE1=btE1;
      return;
    }
  }
}

/*
������ ������ ���������:
5501 - 5506 : 1
*/
void DialplanDecodeStr(char *pStr0)
{
  char *pNumbStart,*pNumbEnd,*pNumbE1;
  unsigned int dwNumbStart,dwNumbEnd,dwE1;

  if(!pStr0 || !pStr0[0]) return;
  pNumbStart=pStr0;
  pNumbEnd=strchr(pStr0,'-');
  if(!pNumbEnd) return;
  pNumbEnd++;
  pNumbE1=strchr(pNumbEnd,':');
  if(!pNumbE1) return;
  pNumbE1++;
  pNumbStart=strclr(pNumbStart);
  pNumbEnd=strclr(pNumbEnd);
  pNumbE1=strclr(pNumbE1);
  if(pNumbStart && pNumbEnd && pNumbE1)
  {
    dwNumbStart=atoi(pNumbStart);
    dwNumbEnd=atoi(pNumbEnd);
    dwE1=atoi(pNumbE1);
    if(dwNumbStart && dwNumbEnd && dwE1)
    {
      if(dwE1<=NUM_TRUNKS) DialplanElementAdd(dwNumbStart,dwNumbEnd,(unsigned char)dwE1);
    }
  }
}

void DialplanDecode(char *pBuf)
{
  char *pStr;
  while(pBuf)
  {
    pStr=pBuf;
    pBuf=GetNextStr(pBuf);
    DialplanDecodeStr(pStr);
  }
}

void ReadDialplan(void)
{
  unsigned int dwFSize;
  char *pBuf;
  HANDLE hFile;
 
  memset(DialPlan,0,sizeof(DialPlan));
  hFile=CreateFile("Dialplan.txt",GENERIC_READ,0);
  if(hFile>=0)
  {
    dwFSize=GetFileSize(hFile);
    if(dwFSize)
    {
      pBuf=pvPortMalloc(dwFSize+1);
      if(pBuf)
      {
        if(ReadFile(hFile,pBuf,dwFSize))
        {
          pBuf[dwFSize]=0;
          DialplanDecode(pBuf);
        }
        vPortFree(pBuf);
      }
    }
    CloseHandle(hFile);
  }
}

unsigned char FindE1fromDialplan(char *NumbDst)
{
  unsigned long dwNumbDst;
  unsigned char btInd,btE1;

  dwNumbDst=atol(NumbDst);
  if(dwNumbDst)
  {
    for(btInd=0;btInd<d_MaxElementDP;btInd++)
    {
      if((dwNumbDst>=DialPlan[btInd].dwNumbStart) &&
         (dwNumbDst<=DialPlan[btInd].dwNumbEnd)) return(DialPlan[btInd].btE1);
    }
  }
  return(0xff);
}

///////////////////////////////////////////////////////////////////////////////
void InitCrossCommute(void)
{ memset(MassCrossCommute,0,sizeof(MassCrossCommute)); }

bool IsCrossCommuteAbnt(unsigned short wNChan)
{
  unsigned char i;

  for(i=0;i<d_MaxCrossCommute;i++)
  {
    if((MassCrossCommute[i].wCPSrs==wNChan) || (MassCrossCommute[i].wCPDst==wNChan)) return(true);
  }
  return(false);
}

void CrossCommuteOtboi(unsigned short wNChan)
{
  unsigned char i;
  unsigned short wCPSrs,wCPDst;

  for(i=0;i<d_MaxCrossCommute;i++)
  {
    wCPSrs=MassCrossCommute[i].wCPSrs;
    wCPDst=MassCrossCommute[i].wCPDst;
    if((wCPSrs==wNChan) || (wCPDst==wNChan))
    {
      if(wCPSrs) SendCmdOtboiISDN(PRI_CAUSE_CALL_REJECTED,wCPSrs,wCPDst);
      if(wCPDst) SendCmdOtboiISDN(PRI_CAUSE_CALL_REJECTED,wCPDst,wCPSrs);
      MassCrossCommute[i].wCPSrs=0;
      MassCrossCommute[i].wCPDst=0;
      return;
    }
  }
}

void CrossCommuteOtboiAll(void)
{
  unsigned char i;
  unsigned short wCPSrs,wCPDst;

  for(i=0;i<d_MaxCrossCommute;i++)
  {
    wCPSrs=MassCrossCommute[i].wCPSrs;
    wCPDst=MassCrossCommute[i].wCPDst;
    if(wCPSrs || wCPDst)
    {
      if(wCPSrs) SendCmdOtboiISDN(PRI_CAUSE_CALL_REJECTED,wCPSrs,wCPDst);
      if(wCPDst) SendCmdOtboiISDN(PRI_CAUSE_CALL_REJECTED,wCPDst,wCPSrs);
      MassCrossCommute[i].wCPSrs=0;
      MassCrossCommute[i].wCPDst=0;
      return;
    }
  }
}

bool CrossCallISDN(unsigned short wNChanSrs,unsigned char btE1Srs,
                   char *NumberSrs,char *NameSrs,char *NumberDst)
{
  unsigned short wNChanDst;
  unsigned char i,btE1Dst,btE1Cnt;

  for(i=0;i<d_MaxCrossCommute;i++)
  {
    if((MassCrossCommute[i].wCPSrs==0) && (MassCrossCommute[i].wCPDst==0))
    {
      wNChanDst=GetFreeConnectionPoint();
      if(wNChanDst==0xFF) return(false);
      btE1Dst=btE1Srs;
      btE1Cnt=0;
      do
      {
        btE1Dst=FindE1fromDialplan(NumberDst);
        if(btE1Dst==0xff)
        {
          btE1Dst=GetNextE1(btE1Dst);
          if(btE1Dst==0xff)
          {
            btE1Dst=GetFirstE1();
            if(btE1Dst==btE1Srs) return(false);
          }
          btE1Cnt++;
          if((btE1Dst==0xff) || (btE1Cnt==NUM_TRUNKS)) return(false);
        }
        if(_xOutgoingCallISDN(wNChanDst,wNChanSrs,btE1Dst,NumberDst,NumberSrs,NameSrs,0))
        {
          if(_xIncomingCallISDNFree(wNChanSrs,wNChanDst,0))
          {
            MassCrossCommute[i].wCPSrs=wNChanSrs;
            MassCrossCommute[i].btE1Srs=btE1Srs;
            MassCrossCommute[i].wCPDst=wNChanDst;
            MassCrossCommute[i].btE1Dst=btE1Dst;
            return(true);
          }
          else return(false);
        }
      }
      while(btE1Cnt);
    }
  }
  return(false);
}

void CrossCommuteAnswer(unsigned short wCPDst,unsigned short wCPSrs)
{
  unsigned char i;

  for(i=0;i<d_MaxCrossCommute;i++)
  {
    if((wCPSrs==MassCrossCommute[i].wCPSrs) && (wCPDst==MassCrossCommute[i].wCPDst))
    {
      if(_xIncomingCallISDNAnswer(wCPSrs,wCPDst,1)) return;
      break;
    }
  }
  CrossCommuteOtboi(wCPDst);
}

