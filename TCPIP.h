//#define	__SNMP__		// Function for TCP

#include "./TCPIP/socket.h"

// Constants used to configure the Tx and Rx buffer sizes within the WIZnet device
#define tcp8K_RX						( ( unsigned portCHAR ) 0x03 )
#define tcp8K_TX						( ( unsigned portCHAR ) 0x03 )
#define tcp2KAll_RX						( ( unsigned portCHAR ) 0x55 )
#define tcp2KAll_TX						( ( unsigned portCHAR ) 0x55 )

/* Various delays used in the driver. */
#define tcpRESET_DELAY					( ( portTickType ) 16 / portTICK_RATE_MS )
#define tcpINIT_DELAY					( ( portTickType ) 5  / portTICK_RATE_MS  )
#define tcpLONG_DELAY					( ( portTickType ) 5  / portTICK_RATE_MS  )
#define tcpSHORT_DELAY					( ( portTickType ) 1  / portTICK_RATE_MS )
#define tcpCONNECTION_WAIT_DELAY		( ( portTickType ) 100 / portTICK_RATE_MS )
#define tcpNO_DELAY						( ( portTickType ) 0 )

/* Misc constants. */
#define tcpNO_STATUS_BITS				( ( unsigned portCHAR ) 0x00 )
#define tcpISR_QUEUE_LENGTH				( ( unsigned portCHAR ) 10 )
#define tcpISR_QUEUE_ITEM_SIZE			( ( unsigned portCHAR ) 0 )
#define tcpBUFFER_LEN					( 4 * 1024 )
#define tcpMAX_REGISTER_LEN				( 4 )
#define tcpMAX_ATTEMPTS_TO_CHECK_BUFFER	( 6 )
#define tcpMAX_NON_LISTEN_STAUS_READS	( 5 )

/* Select parameter to use */
#define SEL_CONTROL		0	// Confirm socket status
#define SEL_SEND		1	// Confirm Tx free buffer size
#define SEL_RECV		2	// Confirm Rx data size

#ifdef DIGITAL_STUDIO_CONTROLLER
  #define EtherCatSocket ((SOCKET)0)
#else
  #define SNMP_SOCKET     ((SOCKET)0)
  #define SoundSocket     ((SOCKET)2)  // ����� ������ ��� �������� ����
  #define RecordSocket    ((SOCKET)3)  // ����� ������ ������� �����������
#endif
#define CommandSocket     ((SOCKET)1)  // ����� ������ ��� �������� ������
// !! ������������ ����� ������ ��� W3150==3, ��� W6100==7 !!

#define IP_NONE             0
#define IP_PULT             1
#define IP_DSPCOM           2
#define IP_RECORD           3
#define IP_LOGGER           4

#define IP_PULT_DEL_START   11
#define IP_PULT_DEL_END     12
#define IP_UNKNOWN          0xfe

#define dIpPultTOut         4000

#pragma pack(1)
typedef struct
{
// ######## ��������� ������ ������ ��������� � ������������ ������� ###############
// ######## ������ ��� ��� ������� � �������� ������ �� Flash ������ ###############
unsigned portCHAR ucDataSIPR[4]; // IP address
unsigned portCHAR ucDataMSR[4]; // Subnet mask
unsigned portCHAR ucDataGAR[4]; // Gateway address
unsigned portCHAR ucDataSHAR[6]; // MAC address - DO NOT USE THIS ON A PUBLIC NETWORK!
unsigned short wDummy; // �������� ���� ��� ������������ �� ������� 4 �����
unsigned short wPortSNMP;
unsigned short wPortSound;
unsigned short wPortControl;
unsigned short wPortHTTP;
unsigned portCHAR ucDataSNMPmIP[4]; // IP address SNMP manager ��������
unsigned portCHAR ucDataSNMPrIP[4]; // IP address SNMP manager ���������
unsigned portCHAR ucDataSoundIP[4]; // IP address ������� �����������
unsigned short wDataVLANID[6]; // VLANID ��� ������ 0-5 Marvell
unsigned portCHAR ucDataVLANPri[6]; // VLAN ��������� ��� ������ 0-5 Marvell
// #################################################################################
} IPPARAMS_TP;
#pragma pack()

extern IPPARAMS_TP g_IpParams;

/*void ultoa( unsigned portLONG ulVal, portCHAR *pcBuffer, portLONG lIgnore );
void vTCPHardReset( void );
portLONG lTCPSoftReset( void );*/

/********************************************************************************
*               Initialization function to appropriate channel
*
* Description : Initialize designated channel and wait until W3100 has done.
* Arguments   : s - channel number
*               protocol - designate protocol for channel
*                          SOCK_STREAM(0x01) -> TCP.
*                          SOCK_DGRAM(0x02)  -> UDP.
*                          SOCK_IPL_RAW(0x03) -> IP LAYER RAW.
*                          SOCK_MACL_RAW(0x04) -> MAC LAYER RAW.
*               port     - designate source port for appropriate channel
*               flag     - designate option to be used in appropriate.
*                          SOCKOPT_BROADCAST(0x80) -> Send/receive broadcast message in UDP
*                          SOCKOPT_NDTIMEOUT(0x40) -> Use register value which designated TIMEOUT value
*                          SOCKOPT_NDACK(0x20)     -> When not using no delayed ack
*                          SOCKOPT_SWS(0x10)       -> When not using silly window syndrome
* Returns     : When succeeded : Channel number, failed :1
* Note        : API Function
*********************************************************************************/
//portLONG lTCPCreateSocket( unsigned portCHAR s, unsigned portCHAR protocol, unsigned short port, unsigned portCHAR flag );

