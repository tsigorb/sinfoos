#pragma once 
#ifndef MIBDATA_H_INCLUDED
#define MIBDATA_H_INCLUDED

#include ".\SNMP\Asn1.h"

//

/*
      internet    OBJECT IDENTIFIER ::= { iso org(3) dod(6) 1 }

   That is, the Internet subtree of OBJECT IDENTIFIERs starts with the prefix:

      1.3.6.1.

   This memo, as a standard approved by the IAB, now specifies the policy under which this subtree of OBJECT IDENTIFIERs is administered.  Initially, four nodes are present:

      directory     OBJECT IDENTIFIER ::= { internet 1 }
      mgmt          OBJECT IDENTIFIER ::= { internet 2 }
      experimental  OBJECT IDENTIFIER ::= { internet 3 }
      private       OBJECT IDENTIFIER ::= { internet 4 }
*/
#define INTERNET	0x032b0601       02010516
//
#define DIRECTORY		0x042b060101
#define MGMT			0x042b060102   
#define EXPERIMENTAL	0x042b060103
#define PRIVATE			0x042b060104
/*
               mib        OBJECT IDENTIFIER ::= { mgmt 1 }

               system     OBJECT IDENTIFIER ::= { mib 1 }
               interfaces OBJECT IDENTIFIER ::= { mib 2 }
               at         OBJECT IDENTIFIER ::= { mib 3 }
               ip         OBJECT IDENTIFIER ::= { mib 4 }
               icmp       OBJECT IDENTIFIER ::= { mib 5 }
               tcp        OBJECT IDENTIFIER ::= { mib 6 }
               udp        OBJECT IDENTIFIER ::= { mib 7 }
               egp        OBJECT IDENTIFIER ::= { mib 8 }
*/
//
static unsigned char arraySYSTEM[]=		{0x06,0x2b,0x06,0x01,0x02,0x01,0x01};
/*static unsigned char arrayINTERFACES[]= {0x06,0x2b,0x06,0x01,0x02,0x01,0x02, 0x02, 0x01};
static unsigned char arrayAT[]=			{0x06,0x2b,0x06,0x01,0x02,0x01,0x03};
static unsigned char arrayIP[]=			{0x06,0x2b,0x06,0x01,0x02,0x01,0x04};
static unsigned char arrayICMP[]=		{0x06,0x2b,0x06,0x01,0x02,0x01,0x05};
static unsigned char arrayTCP[]=		{0x06,0x2b,0x06,0x01,0x02,0x01,0x06};
static unsigned char arrayUDP[]=		{0x06,0x2b,0x06,0x01,0x02,0x01,0x07};
static unsigned char arrayEGP[]=		{0x06,0x2b,0x06,0x01,0x02,0x01,0x08};
static unsigned char arrayCMOT[]=		{0x06,0x2b,0x06,0x01,0x02,0x01,0x09};
static unsigned char arrayTRANSMITTION[]={0x06,0x2b,0x06,0x01,0x02,0x01,0x0a};
static unsigned char arraySNMP[]=		{0x06,0x2b,0x06,0x01,0x02,0x01,0x0b};*/
//
static unsigned char arrayEnterpriseSINFO[]=	{0x08,0x2b,0x06,0x01,0x04,0x01,0xe0,0x39,0x01};
static unsigned char arrayEnterpriseSINFO_AGM[]={0x0a,0x2b,0x06,0x01,0x04,0x01,0xe0,0x39,0x01,0x01,0x01,0x01,0x01,0x01};

static unsigned char arrayExperimentSINFO[]=	{0x0a,0x2b,0x06,0x01,0x03,0x01,0xe0,0x39,0x01,0x01,0x01};
//
static unsigned char arraySINFOTrapAgm[]={0x0b,0x2b,0x06,0x01,0x04,0x01,0xe0,0x39,0x01,0x01,0x01,0x7f};
//
//
//
typedef struct tag_TabletValue
{
	unsigned short ValType;
//	union tag_Un 
//	{
//	void *lpValue;
	unsigned long datValue;
//	};
} TABLETVALUE;
//
//
//
typedef struct tag_OidBypass
{
	TABLETVALUE *lpTabValue;
	AsnObjectId ThisOid;
} OIDBYPASS;
//
// SNMP Group SYSTEM Variable
// Implementation of the System group is mandatory for all systems.
//


//
#define NOTE_ISNOT_USING	0x0000	// Note In Table Is Not Using
#define NOTE_CAPACITY		0xe000	// Note In Table == Number of Notes in Table
#define NOTE_INRAM			0x0100	// Atual Data Are In RAM
#define NOTE_INFLASH		0x0200 	// Atual Data Are In Flash ROM
#define NOTE_BYFUNC			0x0400 	// Atual Data Get By Function
#define NOTE_HASINDEX		0x0800	// Variable Are Indexed
#define NOTE_ISRDONLY		0x2000 	// Data Is ReadONLY
#define NOTE_ISNEXTTABLE	0x4000 	// Data Is a Next Table Address or End(if NULL)
#define NOTE_ISTABLE		0x8000 	// Has Not Data. Data Is a Pointer to Child Table


//
//
//
const char g_agm32ComunityRO[] = "PwsRO.txt";
const char g_agm32ComunityRW[] = "PwsRW.txt";
const char g_agm32ComunityWA[] = "PwsWA.txt";
//
const char g_SnmpTable[] = "SnmpTable.txt";

//
// SYSTEM TABLE
//
const AsnOctetString g_sysDescr[]={39, "����� ������ ������ �������� ����������"};	// sysDescr { system 1 } // ��������� ��������
									// Access: read-only.
const AsnObjectId g_sysObjectID[]={8, {0x2b,0x06,0x01,0x04,0x1,0x33,0xaa,0x77}};	// OBJECT IDENTIFIER sysObjectID { system 2 } 
									// Access: read-only.
									// ������������� ���������� � ��������� MIB
unsigned long g_sysUpTime=0x12345678;		// SNMPv1 & SNMPv2 // TimeTicks(32) sysUpTime { system 3 } 
									// Access: read-only.
									// ����� � ����� ����� ������ � ���� �������, ����� ����� �������, 
									// ���������� �� ���������� �������, ���� ��������������.
const char g_sysContact[] = "Sys1.txt";	// sysContact { system 4 } 
									// ACCESS  read-write
									// "The textual identification of the contact person
									// for this managed node, together with information
									// on how to contact this person."
const char g_sysName[] = "Sys2.txt";	// sysName { system 5 } // ������ ��� ������ ���� (FQDN) .
									// ACCESS  read-write
const char g_sysLocation[] = "Sys3.txt";	// sysLocation { system 6 } // ���������� ������������ ����.
									// ACCESS  read-write
const unsigned int g_sysServices=0x4444;	// sysServices { system 7 }
									// ACCESS  read-only
									// ��������, ����������� �� ��, ����� ������� ��������������� �����. 
									// ��� ����� ������� OSI ������, �������������� �����. 
									// ��������� �������� ������������ ������, � ����������� �� ����, ����� 
									// ������� ��������������: 0�01 (����������), 0�02 (���������), 
									// 0�04 (�������), 0�08 (�����-�����), 0�40 (����������).
									// A value which indicates the set of services that
									// this entity primarily offers.
									// The value is a sum.  This sum initially takes the
									// value zero, Then, for each layer, L, in the range
	                      			// 1 through 7, that this node performs transactions
                      				// for, 2 raised to (L - 1) is added to the sum.  For
                      				// example, a node which performs primarily routing
                      				// functions would have a value of 4 (2^(3-1)).  In
                      				// contrast, a node which is a host offering
                      				// application services would have a value of 72
                      				// (2^(4-1) + 2^(7-1)).  Note that in the context of
                      				// the Internet suite of protocols, values should be
                      				// calculated accordingly:
                           			// layer  functionality
                               		//		1  physical (e.g., repeaters)
                               		//		2  datalink/subnetwork (e.g., bridges)
                               		// 		3  internet (e.g., IP gateways)
                               		// 		4  end-to-end  (e.g., IP hosts)
                               		// 		7  applications (e.g., mail relays)
                      				// For systems including OSI protocols, layers 5 and
                      				// 6 may also be counted."
int g_Var1, g_Var2, g_Var3;
//
			const TABLETVALUE g_Table_2[3] = 
			{
				{NOTE_CAPACITY, (unsigned long )2},
				{ NOTE_INRAM | ASN_TIMETICKS, (unsigned long)&g_Var3 }, 
				{ NOTE_ISNEXTTABLE, 0}
			};
		const TABLETVALUE g_Table_1[4] = 
		{
			{NOTE_CAPACITY, (unsigned long )3},
			{ NOTE_ISTABLE, (unsigned long)&g_Table_2 },
			{ NOTE_INRAM | ASN_TIMETICKS, (unsigned long)&g_Var2 }, 
			{ NOTE_ISNEXTTABLE, 0}
		};
	const TABLETVALUE g_EnterpriseSINFO[4] = 
	{
		{NOTE_CAPACITY, (unsigned long )3},
		{ NOTE_ISTABLE, (unsigned long)&g_Table_1 },
		{ NOTE_INRAM | ASN_TIMETICKS, (unsigned long)&g_Var1 }, 
		{ NOTE_ISNEXTTABLE, 0}
	};
//
//
//	
	const TABLETVALUE g_SystemTable[9] =
	{
		{NOTE_CAPACITY, (unsigned long )8},
		{ NOTE_ISRDONLY | NOTE_INFLASH | ASN_OCTETSTRING, (unsigned long)&g_sysDescr },
		{ NOTE_ISRDONLY | NOTE_INRAM | ASN_OBJECTIDENTIFIER, (unsigned long)&g_sysObjectID },  
		{ NOTE_INRAM | ASN_TIMETICKS, (unsigned long)&g_sysUpTime }, 
		{ NOTE_INFLASH | ASN_OCTETSTRING, (unsigned long)&g_sysContact }, 	
		{ NOTE_INFLASH | ASN_OCTETSTRING, (unsigned long)&g_sysName }, 
		{ NOTE_INFLASH | ASN_OCTETSTRING, (unsigned long)&g_sysLocation }, 
		{ NOTE_ISRDONLY | ASN_INTEGER, (unsigned long)&g_sysServices},
		{ NOTE_ISNEXTTABLE, g_EnterpriseSINFO}
	};
//
//
//
const static AsnObjectId g_OidBypass[] = 
{
//static unsigned char arraySYSTEM[]=		{0x06,0x2b,0x06,0x01,0x02,0x01,0x01};
	{6, {0x2b,0x06,0x01,0x02,0x01,0x01} },
//static unsigned char arrayEnterpriseSINFO[]=	{0x08,0x2b,0x06,0x01,0x04,0x01,0xe0,0x39,0x01};
	{8, {0x2b,0x06,0x01,0x04,0x01,0xe0,0x39,0x01} }
};

//
#endif	// #ifndef MIBDATA_H_INCLUDED
