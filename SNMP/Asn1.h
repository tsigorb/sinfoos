#ifndef ASN_1_H_INCLUDED
#define ASN_1_H_INCLUDED
//
#define ASNSTRINGMAXLEN	128	// Printable String Max Length 

typedef struct {
    unsigned int   length;
    unsigned char stream[ASNSTRINGMAXLEN];
} AsnOctetString;

#define OBJECTIDMAXLEN	20	// Object ID Max Length 

typedef struct {
    unsigned int   idLength;
    unsigned char 		 ObjectId[OBJECTIDMAXLEN];
} AsnObjectId;

typedef struct {
    unsigned int   len;
    unsigned char adr[4];
} AsnAddressIp;

typedef long                    AsnInteger32;
typedef unsigned long           AsnUnsigned32;
typedef unsigned long long      AsnCounter64;
typedef AsnUnsigned32           AsnCounter32;
typedef AsnUnsigned32           AsnGauge32;
typedef AsnUnsigned32           AsnTimeticks;
typedef AsnOctetString          AsnBits;
typedef AsnOctetString          AsnSequence;
typedef AsnOctetString          AsnImplicitSequence;
typedef AsnOctetString          AsnIPAddress;
typedef AsnOctetString          AsnNetworkAddress;
typedef AsnOctetString          AsnDisplayString;
typedef AsnOctetString          AsnOpaque;

typedef struct {
    unsigned char asnType;
    union {
        AsnInteger32            number;     // ASN_INTEGER
                                            // ASN_INTEGER32
        AsnUnsigned32           unsigned32; // ASN_UNSIGNED32
        AsnCounter64            counter64;  // ASN_COUNTER64
        AsnOctetString          string;     // ASN_OCTETSTRING
        AsnBits                 bits;       // ASN_BITS
        AsnObjectId     		object;     // ASN_OBJECTIDENTIFIER
        AsnSequence             sequence;   // ASN_SEQUENCE
        AsnIPAddress            address;    // ASN_IPADDRESS
        AsnCounter32            counter;    // ASN_COUNTER32
        AsnGauge32              gauge;      // ASN_GAUGE32
        AsnTimeticks            ticks;      // ASN_TIMETICKS
        AsnOpaque               arbitrary;  // ASN_OPAQUE
    } asnValue;
} AsnAny;

typedef AsnObjectId     	AsnObjectName;
typedef AsnAny                  AsnObjectSyntax;


///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// ASN/BER Base Types                                                        //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#define ASN_UNIVERSAL                   0x00
#define ASN_APPLICATION                 0x40
#define ASN_CONTEXT                     0x80
#define ASN_PRIVATE                     0xC0

#define ASN_PRIMITIVE                   0x00
#define ASN_CONSTRUCTOR                 0x20
///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// SNMP Simple Syntax Values                                                 //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#define ASN_INTEGER                 (ASN_UNIVERSAL | ASN_PRIMITIVE | 0x02)
#define ASN_BITS                    (ASN_UNIVERSAL | ASN_PRIMITIVE | 0x03)
#define ASN_OCTETSTRING             (ASN_UNIVERSAL | ASN_PRIMITIVE | 0x04)
#define ASN_NULL                    (ASN_UNIVERSAL | ASN_PRIMITIVE | 0x05)
#define ASN_OBJECTIDENTIFIER        (ASN_UNIVERSAL | ASN_PRIMITIVE | 0x06)
#define ASN_INTEGER32               ASN_INTEGER
#define ASN_CHOICE					(ASN_CONSTRUCTOR | ASN_CONTEXT )
///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// SNMP Constructor Syntax Values                                            //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#define ASN_SEQUENCE                (ASN_UNIVERSAL | ASN_CONSTRUCTOR | 0x10)
#define ASN_SEQUENCEOF              ASN_SEQUENCE

#define ASN_IPADDRESS               (ASN_APPLICATION | ASN_PRIMITIVE | 0x00)
#define ASN_COUNTER32               (ASN_APPLICATION | ASN_PRIMITIVE | 0x01)
#define ASN_GAUGE32                 (ASN_APPLICATION | ASN_PRIMITIVE | 0x02)
#define ASN_TIMETICKS               (ASN_APPLICATION | ASN_PRIMITIVE | 0x03)
#define ASN_OPAQUE                  (ASN_APPLICATION | ASN_PRIMITIVE | 0x04)
#define ASN_COUNTER64               (ASN_APPLICATION | ASN_PRIMITIVE | 0x06)
#define ASN_UNSIGNED32              (ASN_APPLICATION | ASN_PRIMITIVE | 0x07)

//
#endif	//#ifndef ASN_1_H_INCLUDED
