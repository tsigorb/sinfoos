#include <time.h>

typedef struct
{
  time_t Timepoint; // ���� / �����
  unsigned char status; // ������ (�����������, ��������������, ������ � �.�.)
  unsigned char source; // ��� ��������� (��, ������)
  unsigned char category;
  unsigned short message; // ��� ������� (=255 - ���� ������)
} LOG_RECORD;

#define d_LOG_STRINFO_LEN   32

// �������� ��� ���� status
#define lt_Notification     0
#define lt_Warning          1
#define lt_Error            2
#define lt_CriticalError    3

// �������� ��� ���� source
#define ls_OS               0
#define ls_Task             1

// �������� ��� ���� category
#define lc_TaskManager      0
#define lc_CriticalError    1
#define lc_MemoryManager    2
#define lc_Debug            3
#define lc_WiznetReinit     4
#define lc_IpPultUnreg      5
#define lc_IpPultReg        6

void InitFRAM(void);

void ClearLogs(void);

unsigned int GetLogRecordsCount(void);

// ������ ������ � �������� ������� �� ������� �������
// �������: ������ TRUE
// ���������: unsigned int RecNumber - ���������� ����� ������ � �������.
//            LOG_RECORD *lpRecord - ����� ��������� ������.
bool PeekLogRecord(const unsigned int RecNumber, LOG_RECORD *lpRecord,char *strInfo);

// �������� ������ � log-���� ��������� ����������
// �������: ������ TRUE
// ���������: unsigned char typ - ��� (�����������, ��������������, ������ � �.�.)
//            unsigned char source - �������� (�������, ������ � �.�.)
//            unsigned char category - ���������
//            unsigned short code - ��� �������
bool AddToLog(unsigned char typ,unsigned char source,unsigned char category,unsigned short code);

bool AddToLogWithInfo(unsigned char status,unsigned char source,unsigned char category,char *strInfo);

