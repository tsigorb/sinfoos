#pragma once 
#ifndef SNMP_H_INCLUDED
#define SNMP_H_INCLUDED
//
#include "..\SNMP\Asn1.h"
#include <time.h>
//
#define _DEBUG_PWW
//
#define BYTE 	unsigned char

//
// Definition For SNMP FRAME Types
//
#define SNMP_V1_0	0
#define SNMP_V2_0	1
//
//#define SNMP_TRAP_PORT	162
//#define SNMP_PORT		161

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// PDU Type Values                                                           //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#define SNMP_PDU_GET                (ASN_CONTEXT | ASN_CONSTRUCTOR | 0x0)	// SNMPv1
#define SNMP_PDU_GETNEXT            (ASN_CONTEXT | ASN_CONSTRUCTOR | 0x1)	// SNMPv1
#define SNMP_PDU_RESPONSE           (ASN_CONTEXT | ASN_CONSTRUCTOR | 0x2)	// SNMPv1
#define SNMP_PDU_SET                (ASN_CONTEXT | ASN_CONSTRUCTOR | 0x3)	// SNMPv1
#define SNMP_PDU_V1TRAP             (ASN_CONTEXT | ASN_CONSTRUCTOR | 0x4)	// SNMPv1
#define SNMP_PDU_GETBULK            (ASN_CONTEXT | ASN_CONSTRUCTOR | 0x5)	// SNMPv1 & SNMPv2
#define SNMP_PDU_INFORM             (ASN_CONTEXT | ASN_CONSTRUCTOR | 0x6)	// SNMPv1 & SNMPv2
#define SNMP_PDU_TRAP               (ASN_CONTEXT | ASN_CONSTRUCTOR | 0x7)	// SNMPv1 & SNMPv2
///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// SNMP Error Codes                                                          //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#define SNMP_ERRORSTATUS_NOERROR                0
#define SNMP_ERRORSTATUS_TOOBIG                 1
#define SNMP_ERRORSTATUS_NOSUCHNAME             2
#define SNMP_ERRORSTATUS_BADVALUE               3
#define SNMP_ERRORSTATUS_READONLY               4
#define SNMP_ERRORSTATUS_GENERR                 5
#define SNMP_ERRORSTATUS_NOACCESS               6
#define SNMP_ERRORSTATUS_WRONGTYPE              7
#define SNMP_ERRORSTATUS_WRONGLENGTH            8
#define SNMP_ERRORSTATUS_WRONGENCODING          9
#define SNMP_ERRORSTATUS_WRONGVALUE             10
#define SNMP_ERRORSTATUS_NOCREATION             11
#define SNMP_ERRORSTATUS_INCONSISTENTVALUE      12
#define SNMP_ERRORSTATUS_RESOURCEUNAVAILABLE    13
#define SNMP_ERRORSTATUS_COMMITFAILED           14
#define SNMP_ERRORSTATUS_UNDOFAILED             15
#define SNMP_ERRORSTATUS_AUTHORIZATIONERROR     16
#define SNMP_ERRORSTATUS_NOTWRITABLE            17
#define SNMP_ERRORSTATUS_INCONSISTENTNAME       18
#define SNMP_ERRORSTATUS_ERRMalloc       		19

//
#define SNMP_MY_INTERNAL_ERROR			       	0xff


///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// SNMPv1 Trap Types                                                         //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#define SNMP_GENERICTRAP_COLDSTART              0
#define SNMP_GENERICTRAP_WARMSTART              1
#define SNMP_GENERICTRAP_LINKDOWN               2
#define SNMP_GENERICTRAP_LINKUP                 3
#define SNMP_GENERICTRAP_AUTHFAILURE            4
#define SNMP_GENERICTRAP_EGPNEIGHLOSS           5
#define SNMP_GENERICTRAP_ENTERSPECIFIC          6

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// SNMP Access Types                                                         //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#define SNMP_ACCESS_NONE                        0
#define SNMP_ACCESS_NOTIFY                      1
#define SNMP_ACCESS_READ_ONLY                   2
#define SNMP_ACCESS_READ_WRITE                  3
#define SNMP_ACCESS_READ_CREATE                 4

#define MAX_PSWLEN		16

#define GETCONSIST		64	// ������� ������ �����������������(������) ���� ��
							// (65)����� �� ������� - 4 �����(2 ���� �� �����)
							//						00 - �����������
							// 						01 - ����������������
							// 						10 - �������� ���������
							// ������������ ������� - ����� 1..12 ��� 1..12
							// 						����� 13..16 ��� 1..4
//
#define GETMKAPARAM			15	// ������ ���������� ����� MKA
#define COMAMKA_SETTYPE		20	// ��������� ���� ��������� �������: 1-���, 2-���, 4-��������, 
								// 8-����, 16-�� ������, 32-������� �����, 64-��������, 128-�����������, 
								// 256-��, 512-����������, 1024-���������� ���������
#define COMAMKA_SETMODE		21	// ��������� ������ ������ ���������:1-����������������,2-�������������,
								// 3-��, 4-���
#define COMAMKA_SETINPLEV	22	// ��������� ������������ �������� ������ ������. 0..28 ������������� -38..+4�� ����� 1.5��
#define COMAMKA_SETOUTLEV	23	// ��������� ������������ ��������� ������ ������. 0..28 ������������� -38..+4�� ����� 1.5��
#define COMAMKA_SETARULEV	24	// ��������� ����������� ������������ ������ ���. (-38�+4�� ����� 1.5 ��)
#define COMAMKA_SETECHOSUPPR	25	// ��������� ������ ������� ��������������. 0�100% (0-���������)
#define COMAMKA_SETTIMELEV	26	// ��������� ���������� ������� ������� ��������������. 0�32
#define COMAMKA_SETECHOON	33	// ��������� ������� �������� �������������� ��� ���������
#define COMAMKA_SETECHOOFF	34	// ���������� ������� �������� ��������������
#define COMAMKA_MKAARUONOFF	37	// ��������� ���
#define READVERSW			60	// ������ ���� � ������ ��
//
#define GETMKCPARAM			107	// ������� ������ ��������� ������ ���
//
// DATA DEFINITION  DATA DEFINITION  DATA DEFINITION  DATA DEFINITION  DATA DEFINITION 
#pragma pack(1)
typedef struct tag_MkaParam
{
	unsigned char InpLevel;
	unsigned char OutLevel;
	unsigned char NotUsung1;
	unsigned char NotUsung2;
	unsigned char EchoSuppres;	// Only BIT 0x02
	unsigned short EndingType;	// Only BIT 0x02
	unsigned char NotUsung3;
	unsigned char NotUsung4;
	unsigned char AruLevel;
	unsigned char NoiseReject;
	unsigned char NotUsung5;
	unsigned char EndingMode;
	unsigned char LevelTime;
} MKAPARAM;
//
// MKC
//
typedef struct tag_MkcParam
{
	unsigned char bNotUsing : 1;
	unsigned char KoeffErr : 2;	
	unsigned char ExternalTrain : 1;	// 
	unsigned char InternalTrain : 1;	// 
	unsigned char StateCrc : 1;
	unsigned char Pcm30 : 1;
	unsigned char FlowE1 : 1;
} MKC_C0_C3;
//
typedef struct tag_MkcParam1
{
	unsigned char Source2 : 3;	// 
	unsigned char Source1 : 3;
	unsigned char Leader : 1;
	unsigned char bNotUsing : 1;
} MKC_C4;
//
typedef struct tag_MkcParam2
{
	unsigned char DamageRemovedSide16 : 1;
	unsigned char ErrIAS : 1;	
	unsigned char ErrAIS_KI16 : 1;	// 
	unsigned char LossSynchrCRC : 1;	// 
	unsigned char DamageRemovedSide : 1;	// 
	unsigned char LossCycleSynchr : 1;
	unsigned char ErrAIS : 1;
	unsigned char LossInputSignal : 1;//
} MKC_C5_CB;
//
typedef struct tag_MkcParam3
{
	unsigned char bNotUsing : 4;
	unsigned char ErrSES : 1;	// 
	unsigned char ErrES : 1;
	unsigned char SingleErr : 1;
	unsigned char ExcessKoeffErr : 1;
} MKC_C6_CC;
//
typedef struct tag_MkcParam4
{
	unsigned char bNotUsing : 6;
	unsigned char Damage2 : 1;	
	unsigned char Damage1 : 1;	// 
} MKC_CD;
//
// Trap Exchange Struct
//
typedef struct tag_TrapParam5
{
	unsigned char bComa; // == 0x95
	unsigned char bAdr;
	unsigned char bPort;
	unsigned char Reason;
} SITALTRAP;
// set: Comand, codeResult, 
// get: Info
#pragma pack()

typedef struct CrashUK
{
	time_t d_t; // ���� / �����
	SITALTRAP cod; //��� ������
} sCrash;

typedef struct ListCrash
{
	int psCrash;
	sCrash 	Inf;
} sListCrash;

//char g_AccessLevel;	

// FUNC DEFINITION  FUNC DEFINITION  FUNC DEFINITION  FUNC DEFINITION  FUNC DEFINITION 
// FUNC DEFINITION  FUNC DEFINITION  FUNC DEFINITION  FUNC DEFINITION  FUNC DEFINITION 
 
bool SnmpParser(unsigned char * BufSnmp, unsigned int * EndBufSnmp, unsigned int *lpBegTx, unsigned int *lpEndTx, int *lpLen);
unsigned char GetSnmpByteFromIp(unsigned int Offset);
void PutSnmpByteToIp(unsigned char *lpInp, int nHow, unsigned int TxOffset, bool fMode);
int GetTypeLength( unsigned char * lpOffset, unsigned int * LengthOfLength );
void GetAsnValue(unsigned char asnType, unsigned char *lpOffset, int LenToParse, AsnAny *AnyValue);
bool CheckSnmpCommunity(AsnAny *AnyValue);
bool SetNewOidVariable(AsnObjectId *lpObjID, AsnAny *lpAnyValue);
bool Get_OidVariable(AsnObjectId *lpObjID, AsnAny *lpAnyValue);
int FindOid(AsnObjectId *lpObjID, int *lpUsedoid);
unsigned long GetTable(int Index, AsnObjectId *lpObjID, int *lpUsedOid);
bool WalkThroughTheTable(int *lpUsedOid, AsnObjectId *lpObjID, unsigned int *lpThisTable, unsigned short *lpValType, unsigned long *lpValValue);
bool GetIndex(unsigned long valValue, int *lpUsedOid, AsnObjectId *lpObjID);
int GetMaxEventIndex(void);
bool UploadValue(int UsedOid, AsnObjectId *lpObjID, AsnAny *lpAnyValue, unsigned short ValType, unsigned long ValValue);
bool SetupValue(int UsedOid, AsnObjectId *lpObjID, AsnAny *lpAnyValue, unsigned short ValType, unsigned long ValValue);
int WriteOutValue(AsnObjectId *lpObjID, AsnAny *lpAnyValue, unsigned char *lpOffset, bool Mode );
int WriteAsnLength(bool fMode, int CogingLen, unsigned char *lpOffset);
int StringToAsn(bool fMode, unsigned char *lpOffset, AsnAny *lpAnyValue );
int DigitToAsn(bool fMode, unsigned char *lpOffset, AsnAny *lpAnyValue );
bool CreateTrap(SITALTRAP *lpBuff, unsigned int *lpBegTx, unsigned int *lpEndTx);
int FormatTrapMsg(SITALTRAP *lpBuff, unsigned char *lpOutBuff);

bool SnmpTask(void);
void InsertCrash(SITALTRAP *codTrap);
void DeleteCrash(SITALTRAP *codTrap);

//
#endif 	//SNMP_H_INCLUDED
