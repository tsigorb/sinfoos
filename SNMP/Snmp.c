#include "..\Sinfo.h"
#include "..\SNMP\CommonSnmp.h"
#include "..\SNMP\MibData.h"
#include "..\SNMP\Snmp.h"
#include ".\GlobalData.h" 
#include ".\FunctionsPrototypes.h"

#define MAX_BUF_SIZE_SNMP   2048
#define TX_WRITE_OFFSET     64 // ��������, �� �������� ���������� ������������ SNMP - ������

/////////////////////////////////////////////////////////////////////
void *pvPortMalloc(size_t xWantedSize);
void vPortFree(void *pv);

/////////////////////////////////////////////////////////////////////
section("bigdata") static unsigned portCHAR tcpRecBufSnmp[MAX_BUF_SIZE_SNMP];
section("bigdata") static unsigned portCHAR tcpSendBufSnmp[MAX_BUF_SIZE_SNMP];
static sListCrash *endList=NULL; // ��������� �� ����� ������
static sListCrash *beginList=NULL;  // ��������� �� ������ ������

char g_AccessLevel;
unsigned int g_ErrStatus, g_ErrIndex, g_RequestID;
unsigned char g_PduType;

// �������
unsigned short CountSnmp=0;
short ii=0;
//extern unsigned short DataSave[];

/////////////////////////////////////////////////////////////////////
// ������������ ��������(������) SNMP �����
// ���������:	unsigned int BegInp - ��������� ����� ��������� ������
//				unsigned int EndOut - �������� ����� ��������� ������
//				unsigned int *lpBegTx - ����� ����������, � ������� ������������ ��������� ����� ������ ��� ��������
//				unsigned int *lpEndTx - ����� ����������, � ������� ������������ �������� ����� ������ ��� ��������
// �������: TRUE - ���� ������; FALSE - ���� �����
/////////////////////////////////////////////////////////////////////
bool SnmpParser(unsigned char * BufSnmp, unsigned int * EndBufSnmp, unsigned int *lpBegTx, unsigned int *lpEndTx, int *lpLen)
{
unsigned int RdPointer;
unsigned char *OutBuffPtr;
unsigned char asnType;
int LenToParse, FrameLen, VarPassNumber, OutDataLen;
AsnAny AnyValue;
AsnObjectId ObjID;
int Dovesok;
HANDLE hFile;
int Index, tt;

    //
	OutDataLen = 0; RdPointer = 0; 
	g_ErrStatus = 0; g_ErrIndex = 0;
	*EndBufSnmp = (int) BufSnmp;
	//
	// SNMP HEADER 
	//
	// 0x30,Length,...
	AnyValue.asnType = *(BufSnmp++); 
	if(AnyValue.asnType != (ASN_CONSTRUCTOR | ASN_SEQUENCE) )
	{
		return false;
	}
	LenToParse = GetTypeLength( BufSnmp, &RdPointer );
	FrameLen =LenToParse+ 2;
	*EndBufSnmp += FrameLen; // ����� ��������� ������
	(*(lpLen)) = (*(EndBufSnmp)) - (int) BufSnmp +1;
	BufSnmp += RdPointer;
	//
	// SNMP Version
	//
	AnyValue.asnType = *(BufSnmp++); 
	LenToParse = GetTypeLength( BufSnmp, &RdPointer );
	BufSnmp += RdPointer;
	GetAsnValue(AnyValue.asnType, BufSnmp, LenToParse, &AnyValue);
	BufSnmp += LenToParse;
	if(AnyValue.asnValue.number > SNMP_V1_0) // ������
	{ 
		// Warning!! Fill Error Value "Is Not Consistent Version"
		g_ErrStatus = SNMP_ERRORSTATUS_WRONGTYPE; g_ErrIndex = 0; 
		return false; 
	}
	//
	// SNMP Comunity
	//
	AnyValue.asnType = *(BufSnmp++);
	LenToParse = GetTypeLength( BufSnmp, &RdPointer );
	BufSnmp += RdPointer;
	GetAsnValue(AnyValue.asnType, BufSnmp, LenToParse, &AnyValue);
	BufSnmp += LenToParse;
	g_AccessLevel = -1;
	if(CheckSnmpCommunity(&AnyValue) != true)
	{
		g_ErrStatus = SNMP_ERRORSTATUS_NOACCESS; g_ErrIndex = 0; 
		//return false; 
	}
	//
	// SNMP Frame Type
	//
	(*(lpBegTx)) = (int) BufSnmp;
	g_PduType =  *(BufSnmp++);// & 0x0f; // ��� PDU
	g_PduType =  g_PduType & 0x0f;
	LenToParse = GetTypeLength( BufSnmp, &RdPointer );
	BufSnmp += RdPointer;
	if( (g_PduType != (SNMP_PDU_GET&0x07) ) && 
		(g_PduType != (SNMP_PDU_GETNEXT&0x07) ) &&
		(g_PduType != (SNMP_PDU_SET&0x07)) )
	{
		// Warning!! Fill Error Value "Is Not Consistent SNMP Frame Type"
		g_ErrStatus =1; g_ErrIndex = 0; 
		return false; 
	}
	//
	// SNMP Frame IDentifier
	//
	AnyValue.asnType = *(BufSnmp++);
	LenToParse = GetTypeLength( BufSnmp, &RdPointer );
	BufSnmp += RdPointer;
	GetAsnValue(AnyValue.asnType, BufSnmp, LenToParse, &AnyValue);
	BufSnmp += LenToParse;
	g_RequestID = (unsigned int)AnyValue.asnValue.number; // ����� �������
	// SNMP Error Status
	//
	AnyValue.asnType = *(BufSnmp++);
	LenToParse = GetTypeLength( BufSnmp, &RdPointer );
	BufSnmp += RdPointer;
	//
	(*(lpEndTx)) = (int) BufSnmp;
	if(g_ErrStatus != 0)
	{
		return false;
	}
	//
	GetAsnValue(AnyValue.asnType, BufSnmp, LenToParse, &AnyValue);
	BufSnmp += LenToParse;
	g_ErrStatus = 0;//(unsigned int)AnyValue.asnValue.number; ??????????????????????
	//
	// SNMP Error *Index
	//
	AnyValue.asnType = *(BufSnmp++);
	LenToParse = GetTypeLength( BufSnmp, &RdPointer );
	BufSnmp += RdPointer;
	GetAsnValue(AnyValue.asnType, BufSnmp, LenToParse, &AnyValue);
	BufSnmp += LenToParse;
	g_ErrIndex = 0;//(unsigned int)AnyValue.asnValue.number;
	//
	// Get SNMP Oid & Variable Common Struct Length. I'm Don't Use It.
	//
	/////////////////////////////////////////////////////////
	AnyValue.asnType = *(BufSnmp++);
	LenToParse = GetTypeLength( BufSnmp, &RdPointer );
	BufSnmp += RdPointer;
	VarPassNumber = OutDataLen = 0;
	//
	OutBuffPtr = &tcpSendBufSnmp[TX_WRITE_OFFSET];
	//
	while ((int)BufSnmp < *EndBufSnmp)
	{
	OutBuffPtr = &tcpSendBufSnmp[TX_WRITE_OFFSET] + OutDataLen;
		//
		// Get SNMP Oid & Variable One Struct Length. I'm Don't Use It.
		//
		AnyValue.asnType = *(BufSnmp++);
		LenToParse = GetTypeLength( BufSnmp, &RdPointer );
		BufSnmp += RdPointer;
		//
		// Get SNMP Oid.
		//
		AnyValue.asnType = *(BufSnmp++);
		LenToParse = GetTypeLength( BufSnmp, &RdPointer );
		BufSnmp += RdPointer;
		GetAsnValue(AnyValue.asnType, BufSnmp, LenToParse, &AnyValue);
		BufSnmp += LenToParse;
		ObjID.idLength = AnyValue.asnValue.object.idLength;
		memcpy(ObjID.ObjectId, AnyValue.asnValue.object.ObjectId, AnyValue.asnValue.object.idLength);
		//
		// Get SNMP Variable.
		//
		AnyValue.asnType = *(BufSnmp++);
		if(AnyValue.asnType != ASN_NULL) 
		{
			LenToParse = GetTypeLength( BufSnmp, &RdPointer );
			BufSnmp += RdPointer;
		}
		GetAsnValue(AnyValue.asnType, BufSnmp, LenToParse, &AnyValue); // 
			BufSnmp += RdPointer;
		if( g_PduType == (SNMP_PDU_SET & 0x07) )
		{
			if(g_AccessLevel < SNMP_ACCESS_READ_WRITE)
			{
				g_ErrStatus = SNMP_ERRORSTATUS_NOACCESS; g_ErrIndex = 0; 
				return false; 
			}
			if(SetNewOidVariable(&ObjID, &AnyValue) == false) // ��������� ������ �������� SNMP ����������
			{
				// g_ErrStatus = SNMP_ERRORSTATUS_NOSUCHNAME;
				g_ErrIndex = VarPassNumber; 
				//break; 
				return false; 
			}
		}
		if(Get_OidVariable(&ObjID, &AnyValue) == false) //������ �������� SNMP ����������
		{
			g_ErrStatus = SNMP_ERRORSTATUS_NOSUCHNAME; g_ErrIndex = VarPassNumber;
			return false; 
		}
		// Write Value To Output Buffer
		OutDataLen += WriteOutValue(&ObjID, &AnyValue, OutBuffPtr, true); // ������ � �������� ����� MIB ���������� � �� ��������
		//
		VarPassNumber++;
	}
	// Write DataLength & ASN_SEQUENCE
	Dovesok = WriteAsnLength(false, OutDataLen, 0); // ���������� ���� ����� (0� 1 �� 4)
	(*(lpEndTx)) = (int)&tcpSendBufSnmp[TX_WRITE_OFFSET] + OutDataLen;
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
OutBuffPtr = &tcpSendBufSnmp[TX_WRITE_OFFSET - Dovesok];	
	//
	WriteAsnLength(true, OutDataLen, OutBuffPtr);
	OutBuffPtr--;
	*(unsigned char *)OutBuffPtr = ASN_SEQUENCE; // 0x30;
	// Write Error Index
	AnyValue.asnType = ASN_INTEGER;
	AnyValue.asnValue.unsigned32 = g_ErrIndex; // ������ ������
	Dovesok = DigitToAsn(false, OutBuffPtr, &AnyValue );
	OutBuffPtr -= Dovesok;
	DigitToAsn(true, OutBuffPtr, &AnyValue );
	// Write Error Status
	AnyValue.asnType = ASN_INTEGER;
	AnyValue.asnValue.unsigned32 = g_ErrStatus; // ������ ������ 
	Dovesok = DigitToAsn(false, OutBuffPtr, &AnyValue );
	OutBuffPtr -= Dovesok;
	DigitToAsn(true, OutBuffPtr, &AnyValue );
	// BackWrite Frame ID
	AnyValue.asnType = ASN_INTEGER;
	AnyValue.asnValue.unsigned32 = g_RequestID; // ����� �������
	Dovesok = DigitToAsn(false, OutBuffPtr, &AnyValue );
	OutBuffPtr -= Dovesok;
	DigitToAsn(true, OutBuffPtr, &AnyValue );
	// Write Common Length & Frame Type
	Dovesok = WriteAsnLength(false, TX_WRITE_OFFSET+OutDataLen-(OutBuffPtr-tcpSendBufSnmp), 0);
	OutBuffPtr -= Dovesok;
	WriteAsnLength(true, TX_WRITE_OFFSET+OutDataLen-(OutBuffPtr-tcpSendBufSnmp)-Dovesok, OutBuffPtr);
	OutBuffPtr--;
	*(unsigned char *)OutBuffPtr =  SNMP_PDU_RESPONSE; // 0xa2
	// Write "Comunity"
	switch(g_AccessLevel)
	{
		case SNMP_ACCESS_READ_ONLY://             2
			hFile = CreateFile((char *)g_agm32ComunityRO, GENERIC_READ, 0);
		break;
		case SNMP_ACCESS_READ_WRITE://
			hFile = CreateFile((char *)g_agm32ComunityRW, GENERIC_READ, 0);
		break;
		case SNMP_ACCESS_READ_CREATE://                 4
			hFile = CreateFile((char *)g_agm32ComunityWA, GENERIC_READ, 0);
		break;
	}
	AnyValue.asnValue.string.length = ReadFile(hFile, AnyValue.asnValue.string.stream, -1);
	AnyValue.asnType = ASN_OCTETSTRING;
	CloseHandle( hFile );
	Dovesok = StringToAsn(false, OutBuffPtr, &AnyValue );
	OutBuffPtr -= Dovesok;
	Dovesok = StringToAsn(true, OutBuffPtr, &AnyValue );
	// Write Protocol ID
	OutBuffPtr -= 3;
	AnyValue.asnValue.string.stream[0] = 0x02;
	AnyValue.asnValue.string.stream[1] = 0x01;
	AnyValue.asnValue.string.stream[2] = 0x00;
	AnyValue.asnValue.string.length = 3;
	for( Index  = 0; Index < AnyValue.asnValue.string.length; Index++)
		*(unsigned char *)( OutBuffPtr++) = *(unsigned char *)(AnyValue.asnValue.string.stream+Index);
	OutBuffPtr -= 3;
	// Write Frame Length & ASN_SEQUENCE
	Dovesok = WriteAsnLength(false, TX_WRITE_OFFSET+OutDataLen-(OutBuffPtr-tcpSendBufSnmp), 0);
	OutBuffPtr -= Dovesok;
	WriteAsnLength(true, TX_WRITE_OFFSET+OutDataLen-(OutBuffPtr-tcpSendBufSnmp)-Dovesok, OutBuffPtr); // ����� Frame
	OutBuffPtr--;
	*(unsigned char *)OutBuffPtr =  ASN_SEQUENCE; // 0x30
	//
	(*(lpBegTx)) = (int)OutBuffPtr;
return true;	
}

//////////////////////////////////////////////////////////////
// ������� ������ ���� ���� �� ������ W3100A �� ���������� ��������
// ���������:	unsigned int Offset - ��������, �� �������� ���� ������ ����
// �������: ����������� ����
//////////////////////////////////////////////////////////////
unsigned char GetSnmpByteFromIp(unsigned int Offset)
{
unsigned int Addr;
unsigned char bData;
//
bData= 0;
/*
	//Addr = BASE_READ_ADDRESS + Offset;
	Addr = RBUFBASEADDRESS[SNMP_SOCKET] + Offset;
	IDM_AR0 = (unsigned char)(Addr >> 8);
	IDM_AR1 = (unsigned char)(Addr & 0xFF);
    //csync();
  	ssync();
	bData = IDM_DR;
*/
//	
//
return bData;	
}
//////////////////////////////////////////////////////////////
// ������� ���������� ����� ���������� � ����������� �������� � ������
// ���������: ��������� �� �������� �� �������� ���������� ���� �����
// �������: ����� ����������
//////////////////////////////////////////////////////////////
//int GetTypeLength( unsigned char * lpOffset, int LengthOfLength )
int GetTypeLength( unsigned char * lpBuf, unsigned int * Offset )
{
int RetVal, Index, LengthOfLength;
//
	RetVal = *Offset = 0;
	LengthOfLength = 1;
	if ( (*(lpBuf)) & 0x80)
	{
		(*Offset)++;
		LengthOfLength = (*(lpBuf++)) & 0x7f;
	}
	for(Index = 0; Index < LengthOfLength; Index++)
	{
		RetVal <<= 8;
		RetVal |= (*(lpBuf++));
		(*Offset)++;
	}
//
return RetVal;
}
//////////////////////////////////////////////////////////////
// ������� ������ � ����������� ASN.1 ��� � ������� �����
// ���������:	BYTE asnType - ��� ������
//				unsigned int *lpOffset - ��������� �� �������� �� �������� ���������� ������
//				int LenToParse - ����� ������
//				AsnAny *AnyValue - ��������� �� ���������, � ������� ������������ ������
// �������: ���
//////////////////////////////////////////////////////////////
void GetAsnValue(unsigned char asnType, unsigned char *lpOffset, int LenToParse, AsnAny *AnyValue)
{
int Index;
//
	switch(asnType)
	{
	case ASN_INTEGER:
		AnyValue->asnValue.number = 0;
		for(Index = 0; Index < LenToParse; Index++)
		{
			AnyValue->asnValue.number <<= 8;
			AnyValue->asnValue.number |= *(lpOffset++);
		}
	break;
	case ASN_IPADDRESS:
	case ASN_OCTETSTRING:
		memset(AnyValue->asnValue.string.stream, 0x00, ASNSTRINGMAXLEN );
		AnyValue->asnValue.string.length = LenToParse;
		for(Index = 0; Index < LenToParse; Index++)	AnyValue->asnValue.string.stream[Index] = *(lpOffset++);
//		for(Index = 0; Index < LenToParse; Index++)	AnyValue->asnValue.string.stream[Index] = GetSnmpByteFromIp( (*(lpOffset)) + Index);
//		(*(lpOffset)) += LenToParse;
	break;
	case ASN_OBJECTIDENTIFIER:
		memset(AnyValue->asnValue.object.ObjectId, 0x00, OBJECTIDMAXLEN );
		AnyValue->asnValue.object.idLength = LenToParse;
		for(Index = 0; Index < LenToParse; Index++)	AnyValue->asnValue.object.ObjectId[Index] = *(lpOffset++);
//		for(Index = 0; Index < LenToParse; Index++)	AnyValue->asnValue.object.ObjectId[Index] = GetSnmpByteFromIp( (*(lpOffset)) + Index);
//		(*(lpOffset)) += LenToParse;
	break;
	case ASN_NULL:
		AnyValue->asnValue.number = 0;
		LenToParse = 1;
		lpOffset++;
	break;
	default:
	break;
	}
//
return;
}
//////////////////////////////////////////////////////////////
// ������� ��������� ������
// ���������:	AsnAny *AnyValue - ��������� �� ���������, � ������� ���������� ������ � �������
// �������:  TRUE - ���� ������; FALSE - ���� �����
//////////////////////////////////////////////////////////////
bool CheckSnmpCommunity(AsnAny *AnyValue)
{
//
int Len;
unsigned long lpbPsw = 0;
HANDLE hFile;
//char szPsw[ASNSTRINGMAXLEN];
//
	hFile = CreateFile((char *)g_agm32ComunityRO, GENERIC_READ, 0);
	if(hFile < 0) return false;
	Len = ReadFileLP( hFile, (unsigned long *)&lpbPsw);
	CloseHandle( hFile );
	if( Len > 0 )
	{
		if( (WithFlashMemCmp((const unsigned short *)AnyValue->asnValue.string.stream, (unsigned short *)lpbPsw, -Len ) == true) &&
			(AnyValue->asnValue.string.length == Len) )
		{
			g_AccessLevel = SNMP_ACCESS_READ_ONLY;
			return true;
		}
	}
	//
	hFile = CreateFile((char *)g_agm32ComunityRW, GENERIC_READ, 0);
	Len = ReadFileLP( hFile, &lpbPsw);
	CloseHandle( hFile );
	if( Len > 0) 
	{
		if( (WithFlashMemCmp((unsigned short *)AnyValue->asnValue.string.stream, (unsigned short *)lpbPsw, Len ) == true) &&
			(AnyValue->asnValue.string.length == Len) )
		{
			g_AccessLevel = SNMP_ACCESS_READ_WRITE;
			return true;
		}
	}
/*
	//
	hFile = CreateFile((char *)g_agm32ComunityWA, GENERIC_READ, 0);
	Len = ReadFileLP( hFile, &lpbPsw);
	CloseHandle( hFile );
	if( Len > 0)
	{
		if( (WithFlashMemCmp((unsigned short *)AnyValue->asnValue.string.stream, (unsigned short *)lpbPsw, Len ) == true) &&
			(AnyValue->asnValue.string.length == Len) )
		{
			g_AccessLevel = SNMP_ACCESS_READ_CREATE;
			return true;
		}
	}
*/	
/*	if(strcmp((char *)AnyValue->asnValue.string.stream, "sital") == 0 )
	{
		g_AccessLevel = SNMP_ACCESS_READ_CREATE;
		return true;
	}*/
//
return false;
}
///////////////////////////////////////////////////////////////////////
// ������� ������������� ����� ������� SNMP ����������.
// ���������:	AsnObjectId *lpObjID - ��������� �� ������������� ����������
//				AsnAny *lpAnyValue - ��������� �� ����������
// �������:  TRUE - ���� ������; FALSE - ���� �����(��� ���� ��������������� �������� ������� ������ � SNMP �����)
///////////////////////////////////////////////////////////////////////
bool SetNewOidVariable(AsnObjectId *lpObjID, AsnAny *lpAnyValue)
{
int Index;
TABLETVALUE *lpTable;
unsigned short ValType;
unsigned long Value;
int UsedOid;
//
	Index = FindOid(lpObjID, &UsedOid);
	if(Index < 0) return false;
	lpTable = (TABLETVALUE *)GetTable(Index, lpObjID, &UsedOid); 
	if( WalkThroughTheTable(&UsedOid, lpObjID, (unsigned int *)lpTable, &ValType, &Value) == false)// ����� ������ ��������� � ������� � ��������� ������
	{
		if(g_ErrStatus == 0) g_ErrStatus = SNMP_ERRORSTATUS_NOSUCHNAME; 
		return false; 
	}
	//
	if(ValType & NOTE_ISRDONLY)
	{
		g_ErrStatus = SNMP_ERRORSTATUS_READONLY; return false; 
	}
	//
	lpAnyValue->asnType = ValType & 0x00ff;
	if(ValType & NOTE_INRAM)
	{
		if( (lpAnyValue->asnType == ASN_OCTETSTRING) | 
			(lpAnyValue->asnType == ASN_OBJECTIDENTIFIER) | 
			(lpAnyValue->asnType == ASN_IPADDRESS) )
		{
			((AsnOctetString *)Value)->length = lpAnyValue->asnValue.string.length;
			memcpy(((AsnOctetString *)Value)->stream, lpAnyValue->asnValue.string.stream, lpAnyValue->asnValue.string.length);
			return true;
		}
		else 
		{
			memcpy( (unsigned int *)Value, &lpAnyValue->asnValue.unsigned32, sizeof(unsigned int) );
			return true;
		}
	}
	else if(ValType & NOTE_INFLASH)
	{
		DeleteFile((char *)Value);
		Index = CreateFile((char *)Value, CREATE_ALWAYS, lpAnyValue->asnValue.string.length);
		if(Index >= 0)
		{
			WriteFile(Index, (unsigned char *)lpAnyValue->asnValue.string.stream, lpAnyValue->asnValue.string.length);
			CloseHandle( Index);
			return true;
		}
		else {	return false;	}
	}
	else if(ValType & NOTE_BYFUNC)
	{
		return SetupValue(UsedOid, lpObjID, lpAnyValue, ValType, Value);
	}
//
return true;	
}
///////////////////////////////////////////////////////////////////////
// ������� ������ ������� SNMP ���������� � ���������� �� � ������������ ���������.
// ���������:	AsnObjectId *lpObjID - ��������� �� ������������� ����������
//				AsnAny *lpAnyValue - ��������� �� ���������, � ������� ������������ ������
// �������:  TRUE - ���� ������; FALSE - ���� �����(��� ���� ��������������� �������� ������� ������ � SNMP �����)
///////////////////////////////////////////////////////////////////////
bool Get_OidVariable(AsnObjectId *lpObjID, AsnAny *lpAnyValue)
{
int Index;
TABLETVALUE *lpTable;
unsigned short ValType;
unsigned long Value;
int UsedOid;
//
	/*if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )
	{
		lpObjID->ObjectId[lpObjID->idLength-1]++;
	}*/
Label_01:		
	Index = FindOid(lpObjID, &UsedOid); // ����� ������� OID � ������� g_OidBypass ��� �������� OID 
	if(Index < 0)
	{
		g_ErrStatus = SNMP_ERRORSTATUS_NOSUCHNAME; 
		return false; 
	}
	lpTable = (TABLETVALUE *)GetTable(Index, lpObjID, &UsedOid); // ���������� ����� ������� ��� ������� GET. 
	if( WalkThroughTheTable(&UsedOid, lpObjID, (unsigned int *)lpTable, &ValType, &Value) == false)// ����� ������ ��������� � ������� � ��������� ������
	{
	/*if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )
		{
			goto Label_01;
		}*/
		if(g_ErrStatus == 0) 
		{
			g_ErrStatus = SNMP_ERRORSTATUS_NOSUCHNAME; 
		}
		else if(g_ErrStatus == SNMP_MY_INTERNAL_ERROR)
		{ 
			g_ErrStatus = 0;
			memcpy(lpObjID->ObjectId, g_OidBypass[SYSTABIND].ObjectId, 6);
			lpObjID->ObjectId[6] = 0x01;
			lpObjID->idLength = 7; goto Label_01;
		}
		return false; 
	}
	lpAnyValue->asnType = ValType & 0x00ff;
	if(ValType & NOTE_INRAM)
	{
		if( (lpAnyValue->asnType == ASN_OCTETSTRING) | 
			(lpAnyValue->asnType == ASN_OBJECTIDENTIFIER) |
			(lpAnyValue->asnType == ASN_IPADDRESS) )
		{
			lpAnyValue->asnValue.string.length = ((AsnOctetString *)Value)->length;
			memcpy(lpAnyValue->asnValue.string.stream, ((AsnOctetString *)Value)->stream, lpAnyValue->asnValue.string.length);
			return true;
		}
		else 
		{
			memcpy( &lpAnyValue->asnValue.unsigned32, (unsigned int *)Value, sizeof(unsigned int) );
			return true;
		}
	}
	else if(ValType & NOTE_INFLASH)
	{
		Index = CreateFile((char *)Value, GENERIC_READ, 0);
		if(Index >= 0)
		{
			lpAnyValue->asnValue.string.length = ReadFile(Index, lpAnyValue->asnValue.string.stream, -1);
			CloseHandle( Index );
			return true;
		}
		else 
		{	
			return false;	
		}
	}
	else if(ValType & NOTE_BYFUNC)
	{
		return UploadValue(UsedOid, lpObjID, lpAnyValue, ValType, Value);
	}
//
return true;	
}
///////////////////////////////////////////////////////////////////////
// ����� ������ ��������� � ������� � ��������� ������
// �������: true - � ������ ������; false - �������
// ���������: int *lpUsedOid - ��������� �� ������, �������� ���������� ������������� ���������������
//			AsnObjectId *lpObjID - ��������� OID
//			unsigned int *lpThisTable ����� �������, � ������� ������� �����
//			[out] unsigned short *lpValType - ����� ������ ��� ������ ���� ������������ ����������
//			[out] unsigned long *lpValValue - ����� ������ ��� ������ ������������ ����������
///////////////////////////////////////////////////////////////////////
bool WalkThroughTheTable(int *lpUsedOid, AsnObjectId *lpObjID, unsigned int *lpThisTable, unsigned short *lpValType, unsigned long *lpValValue)
{
#ifdef 	 _DEBUG_PWW	
unsigned short debOid;	
#endif
HANDLE hFile;
//
	// GetNext OID
	for( ; (*(lpUsedOid)) < lpObjID->idLength; (*(lpUsedOid))++ )
	{
		if( lpObjID->ObjectId[(*(lpUsedOid))] > (((TABLETVALUE *)lpThisTable)[0].datValue) ) // ����� ������� ?
		{
			if(g_PduType == (SNMP_PDU_GET & 0x07) )
			{
				return false;
			}
			/*else if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )
			{
				// �������� ������, � ��������� OID �������� �� 1
				lpObjID->idLength--;
				(*(lpUsedOid))--;
				lpObjID->ObjectId[(*(lpUsedOid))]++;
				return false;
			}*/
		}
		// ���� "������ � �������" -> ��������� ������ �� ������������
		else if( ((TABLETVALUE *)lpThisTable)[lpObjID->ObjectId[(*(lpUsedOid))]].ValType & NOTE_HASINDEX)
		{
			if(GetIndex( ((TABLETVALUE *)lpThisTable)[lpObjID->ObjectId[(*(lpUsedOid))]].datValue, 
						lpUsedOid, lpObjID) == false) 
			{
				return false;
			}
		}
		//
		if( ( ((TABLETVALUE *)lpThisTable)[lpObjID->ObjectId[(*(lpUsedOid))]].ValType == NOTE_ISNOT_USING) ||
			( ((TABLETVALUE *)lpThisTable)[lpObjID->ObjectId[(*(lpUsedOid))]].ValType == NOTE_CAPACITY) ||
			( ((TABLETVALUE *)lpThisTable)[lpObjID->ObjectId[(*(lpUsedOid))]].ValType == NOTE_ISNEXTTABLE) )
			{
				return false;
			}
		else if( ((TABLETVALUE *)lpThisTable)[lpObjID->ObjectId[(*(lpUsedOid))]].ValType & NOTE_ISTABLE)//	��� ������ - ���������	0x8000 	// Has Not Data. Data Is a Pointer to Child Table
		{
			lpThisTable = (unsigned int *)((TABLETVALUE *)lpThisTable)[lpObjID->ObjectId[(*(lpUsedOid))]].datValue; // ����� ������� ����������
			/*if( ((*(lpUsedOid)) >= (lpObjID->idLength-1)) && (g_PduType == (SNMP_PDU_GETNEXT & 0x07)) )
			{ // ���� ������ "GETNEXT" � "OID"-� ����������� - ��������
				lpObjID->idLength++;
				lpObjID->ObjectId[lpObjID->idLength-1] = 1;
			}*/
		}
		else// if( ((TABLETVALUE *)lpThisTable)[lpObjID->ObjectId[(*(lpUsedOid))]].ValType & NOTE_INRAM ) // ������ ��� GET �������
		{
			(*(lpValType)) = ((TABLETVALUE *)lpThisTable)[lpObjID->ObjectId[(*(lpUsedOid))]].ValType;
			(*(lpValValue)) = ((TABLETVALUE *)lpThisTable)[lpObjID->ObjectId[(*(lpUsedOid))]].datValue;
			(*(lpUsedOid))++;
			//
			return true;
		}
	}
//
//g_deb[ii++]= 0x2288;
return false;	
}
//////////////////////////////////////////////////////////////////////////////////////////////
// ���� ������ GET - ��������� ������� �� ������������. ���� ������ GET NEXT - ��������� �� ������������ � 
// �������� ������ �� ��������� �� ������� (���������� ��� � AsnObjectId *lpObjID)
// �������: true - ���� ������; false - ���� �����
// ���������: unsigned short numOfInd - ��������� ����������� �������� ��� ����������
//			unsigned long valValue - �������� datValue �� ������� TABLETVALUE;
//			int *lpUsedOid ��������� �� ���������� ������������� OID  � AsnObjectId *lpObjID
// 			AsnObjectId *lpObjID - ��������� �� ������������� OID
//////////////////////////////////////////////////////////////////////////////////////////////
bool GetIndex(unsigned long valValue, int *lpUsedOid, AsnObjectId *lpObjID)
{
unsigned short HowInd;
int Index;
//	
	HowInd = (unsigned short) (valValue >> 16) & 0x000f;
	// ���� ������ "GET" � ����� ������� < ������������
	if( ((lpObjID->idLength - (*(lpUsedOid)) ) < HowInd) && (g_PduType == (SNMP_PDU_GET & 0x07) ) ) 
	{
		return false;
	}
	else if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )
	{// ���� ������ GET_NEXT - ��������� ������� �������. ������� �� ������� �������.
		if( (lpObjID->idLength - ((*(lpUsedOid)) + 1) ) < HowInd)
		{
			lpObjID->idLength = ( (*(lpUsedOid)) + 1) + HowInd;
			for(Index = 0; Index < (int)HowInd; Index++) lpObjID->ObjectId[(*(lpUsedOid))+1+Index] = 0;
		}
	}
	switch(valValue & 0x0000ffff)
	{
		case UKCSCARDNUMBER://	ukcsCardNumber
		case UKCSCARDTYPE://	ukcsCardType
			if( lpObjID->ObjectId[(*(lpUsedOid))+1] >= MAXCARDNUMBER)
			{
				if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )
				{
					g_ErrStatus = SNMP_MY_INTERNAL_ERROR; 
					return false;
				}
			}
			else if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )	lpObjID->ObjectId[(*(lpUsedOid))+1]++;
			return true;
		//
/*		case UKCSNUMBER: 
		case UKCSNUMBERSERIAL: 
			if( lpObjID->ObjectId[(*(lpUsedOid))+1] > MAXSINDEXNUMBER)
			{
				if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )
				{
					g_ErrStatus = SNMP_MY_INTERNAL_ERROR; 
					return false;
				}
			}
			else if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )	lpObjID->ObjectId[(*(lpUsedOid))+1]++;
			return true;*/
		// ��� ������ 
		case MKCCARDNUM:// ����� ������			
		case MKCFLOWINDEX: // ����� ������
		case MKCSTATE:
		case MKCFLOWE1:				
		case MKCPCM30:				
		case MKCEXTERNALTRAIN:		
		case MKCINTERNALTRAIN:	
		case MKCKOEFFERR:				
		case MKCEXCESSKOEFFERR:		
		case MKCSTATECRC:				
		case MKCLOSSINPUTSIGNAL:		
		case MKCLOSSCYCLESYNCHR:		
		case MKCSINGLEERR:			
		case MKCLOSSSYNCHRCRC:	
		case MKCERRES:			
		case MKCERRSES:			
		case MKCERRAIS:			
		case MKCERRAIS_KI16:	
		case MKCERRIAS:			
		case MKCDAMAGEREMOVEDSIDE:
		case MKCDAMAGEREMOVEDSIDE16:
			if( (lpObjID->ObjectId[(*(lpUsedOid))+1] >= MAXMKCCARDNUM ) && (lpObjID->ObjectId[(*(lpUsedOid))+2] >= MAXMKCPORTNUM ) )
			{
				if( ( (g_PduType == (SNMP_PDU_GET & 0x07) ) || (g_PduType == (SNMP_PDU_SET & 0x07) ) ) && 
					( (lpObjID->ObjectId[(*(lpUsedOid))+1] <= MAXMKCCARDNUM ) && (lpObjID->ObjectId[(*(lpUsedOid))+2] == MAXMKCPORTNUM ) ) )
					{
						return true;
					}
				if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )
				{
					// �������� ������ �� 2, � ��������� OID �������� �� 1
					lpObjID->idLength -= 2;
					lpObjID->ObjectId[(*(lpUsedOid))]++;
				}
				g_ErrStatus = SNMP_MY_INTERNAL_ERROR; 
				return false;

			}
			else if( ( lpObjID->ObjectId[(*(lpUsedOid))+2] >= MAXMKCPORTNUM ) || (lpObjID->ObjectId[(*(lpUsedOid))+1] == 0) ) 
			{ 
				if( ((g_PduType == (SNMP_PDU_GET & 0x07) ) || ((g_PduType == (SNMP_PDU_SET & 0x07) )) )&& 
				(lpObjID->ObjectId[(*(lpUsedOid))+2] == MAXMKCPORTNUM ) ) 
				{
					return true;
				}
				if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )
				{
					lpObjID->ObjectId[(*(lpUsedOid))+1]++;
					lpObjID->ObjectId[(*(lpUsedOid))+2] = 1;
					return true;
				}
				else 
				{
					g_ErrStatus = SNMP_MY_INTERNAL_ERROR; 
					return false;
				}
			}
			else if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )	
			{
				lpObjID->ObjectId[(*(lpUsedOid))+2]++;
			}
			return true;
		// ��� ������ 		
		case MKCCARDNUM1:// ����� ������ [13..16]		
		case MKCSTATE1:
		case MKCLEADER:// �������/�������		
		case MKCSOURCE1:// �������� ������������� � 1-� �����������		
		case MKCSOURCE2:// �������� ������������� � 2-� �����������		
		case MKCDAMAGE1:// ������ ������������� 1-�� ������		
		case MKCDAMAGE2:// ������ ������������� 2-�� ������		
			if( lpObjID->ObjectId[(*(lpUsedOid))+1] >= MAXMKCCARDNUM)
			{
				if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )
				{
					g_ErrStatus = SNMP_MY_INTERNAL_ERROR; 
					return false;
				}
			}
			else if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )	lpObjID->ObjectId[(*(lpUsedOid))+1]++;
			return true;
		//
		case MKACARDNUM:// 		INTEGER,
		case MKAPORTIND:// 		INTEGER, 
		case MKAVERS:// 				OCTET STRING,
		case MKAPORTONOF:// 		INTEGER, 
		case MKAPORTTYPE:// 			INTEGER, 
		case MKAPORTMODE:// 			INTEGER, 
		case MKAPORTINPLEV:// 		INTEGER, 
		case MKAPORTOUTLEV:// 		INTEGER, 
		case MKAPORTARULEV:// 		INTEGER, 
		case MKAPORTNOISELEV:// 	INTEGER, 
		case MKAPORTTIMELEV:// 	INTEGER, 
		case MKAPORTECHOSUPP:// 	INTEGER, 
//		case MKAPORTOVERVOLT:// 		INTEGER, 
		case MKAPORTALARM:// 		INTEGER
		case MKAARUONOFF:
			if( (lpObjID->ObjectId[(*(lpUsedOid))+1] >= MAXMKACARDNUM ) && (lpObjID->ObjectId[(*(lpUsedOid))+2] >= MAXMKAPORTNUM ) )
			{
				if( ( (g_PduType == (SNMP_PDU_GET & 0x07) ) || (g_PduType == (SNMP_PDU_SET & 0x07) ) ) && 
					( (lpObjID->ObjectId[(*(lpUsedOid))+1] <= MAXMKACARDNUM ) && (lpObjID->ObjectId[(*(lpUsedOid))+2] == MAXMKAPORTNUM ) ) )
					{
						return true;
					}
				if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )
				{
					// �������� ������ �� 2, � ��������� OID �������� �� 1
					lpObjID->idLength -= 2;
					lpObjID->ObjectId[(*(lpUsedOid))]++;
				}
				g_ErrStatus = SNMP_MY_INTERNAL_ERROR; 
				return false;

			}
			else if( ( lpObjID->ObjectId[(*(lpUsedOid))+2] >= MAXMKAPORTNUM ) || (lpObjID->ObjectId[(*(lpUsedOid))+1] == 0) ) 
			{ 
				if( ((g_PduType == (SNMP_PDU_GET & 0x07) ) || ((g_PduType == (SNMP_PDU_SET & 0x07) )) )&& 
				(lpObjID->ObjectId[(*(lpUsedOid))+2] == MAXMKAPORTNUM ) ) 
				{
					return true;
				}
				if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )
				{
					lpObjID->ObjectId[(*(lpUsedOid))+1]++;
					lpObjID->ObjectId[(*(lpUsedOid))+2] = 1;
					return true;
				}
				else 
				{
					g_ErrStatus = SNMP_MY_INTERNAL_ERROR; 
					return false;
				}
			}
			else if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )	
			{
				lpObjID->ObjectId[(*(lpUsedOid))+2]++;
			}
			return true;
		// PM128  		
		case PMSERIALNUM:// ����� ������ [1..12]		
		case PMNUMBMANUF:// 
		case PMSTATE:// 		
			if( lpObjID->ObjectId[(*(lpUsedOid))+1] >= NumberKeyboard)
			{
				if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )
				{
					g_ErrStatus = SNMP_MY_INTERNAL_ERROR; 
					return false;
				}
			}
			else if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )	lpObjID->ObjectId[(*(lpUsedOid))+1]++;
			return true;
	}
//
return false;	
}
//////////////////////////////////////////////////////////////////////////////////////////
// ���������� ���������� ������� � LOG �����.
//////////////////////////////////////////////////////////////////////////////////////////
int GetMaxEventIndex(void)
{
int How=1;	
//	
//
return How;	
}
///////////////////////////////////////////////////////////////////////
// ���������� ����� ������� ��� ������� GET. ��� ������� GETNEXT ��������� ������� �� ��������� 
// ������� � ���������� OID, ���� OID ���������, �� ��������� � ������� ������ ������� ������� OID-����������
// �������: ����� ������� ��� 0 - � ������ ������.
// ���������: int Index - ������ ������� � ������� g_OidBypass ���������� ��� ������ FindOid(...)
///////////////////////////////////////////////////////////////////////
unsigned long GetTable(int Index, AsnObjectId *lpObjID, int *lpUsedOid)
{
	switch(Index)
	{
	case UKCSINFO: 
		return (unsigned long)g_ukcsInformation;
	case UKCSTABIND: 
		return (unsigned long)g_ukcsConfigTable;
	case MUTABIND: 
		return (unsigned long)g_muConfig;
	case MKCTABIND: 
		return (unsigned long)g_mkcTable;
	case MKATABIND: 
		return (unsigned long)g_mkaTable;
	case PM128TABIND: 
		return (unsigned long)g_pm128Table;
	case OBJECTSTABIND: //return (unsigned long)0;//sitallObjects;
	case ENTERPRISESTABIND: //return (unsigned long)0;//SitallEnterprises;
	case PRODUCTINFOTABIND: 
		return (unsigned long)g_ProductInfo;
	case SYSTABIND: 
		return (unsigned long)g_SystemTable;
	}
//	
return 0;	
}
///////////////////////////////////////////////////////////////////////
// ����� ������� OID � ������� g_OidBypass ��� �������� OID 
// �������: �������� ������ ��� -1 � ������ �������
// ���������: AsnObjectId *lpObjID - ������� OID
//			int *lpUsedoid - ������������ ���������� ������������� ���������������
///////////////////////////////////////////////////////////////////////
int FindOid(AsnObjectId *lpObjID, int *lpUsedoid)
{
int How = sizeof(g_OidBypass)/sizeof(AsnObjectId);//OIDBYPASS);
int Index;
int In;

//
	for(Index = 0; Index < How; Index++)
	{
    	if(memcmp(g_OidBypass[Index].ObjectId, lpObjID->ObjectId, g_OidBypass[Index].idLength) != 0) continue;
    	else
		{
			(*(lpUsedoid)) = g_OidBypass[Index].idLength;
			return Index;
		}
	}
//
(*(lpUsedoid)) = 0;
return -1;	
}
///////////////////////////////////////////////////////////////////////
// ������� ������������ ASN.1 �����������
// ���������:	AsnObjectId *lpObjID - ��������� �� ���������, � ������� ���������� ������������� ����������
//				AsnAny *lpAnyValue - ��������� �� ���������, � ������� ���������� ������
//int Offset - �������� � �������� ������, ���� ������������ ���������
//				bool Mode - ���� TRUE - ������ ������������ � �������� �����; ���� FALSE - ������ �� ������������ � �������� ����� 
// �������: ����� ������������� ������
///////////////////////////////////////////////////////////////////////
int WriteOutValue(AsnObjectId *lpObjID, AsnAny *lpAnyValue, unsigned char *lpOffset, bool Mode )
{
int nHow, Dovesok;
unsigned char bData;
int Index;
//
nHow=0;
	
	// Calc Common Length
	// Next-> Length Of Length Of Value
	switch(lpAnyValue->asnType)
	{
		case ASN_UNSIGNED32:
		case ASN_COUNTER64:
		case ASN_COUNTER32:
		case ASN_GAUGE32:
		case ASN_TIMETICKS:
		case ASN_INTEGER32:
			nHow = DigitToAsn(false, lpOffset, lpAnyValue );
		break;
		case ASN_OCTETSTRING:
		case ASN_BITS:
		case ASN_OBJECTIDENTIFIER:
		case ASN_IPADDRESS:
			nHow = StringToAsn(false, lpOffset, lpAnyValue ); //
		break;
		case ASN_NULL:
			nHow = 2;	
		break;
	}
	// Length Of ID
	nHow += lpObjID->idLength;
	// Length Of Length Of ID
	nHow += 1;
	// Type Of Value
	nHow += 1;
	// ASN_SEQUENCE
	//nHow += 1;
	// Coding & Write
//------------ ������ � �������� ����� MIB ���������� --------
	*(lpOffset++) = ASN_SEQUENCE; // 0x30;
	Dovesok = WriteAsnLength(true, nHow, lpOffset);
	lpOffset += Dovesok; nHow ++; nHow += Dovesok;
	*(unsigned char *) (lpOffset++) =  ASN_OBJECTIDENTIFIER;
	*(unsigned char *) (lpOffset++) = (char)lpObjID->idLength;
	for( Index  = 0; Index < lpObjID->idLength; Index++)
		*(unsigned char *)( lpOffset++) = *(unsigned char *)(lpObjID->ObjectId+Index);
//----------------------------------------------------------------------
//------------ ������ � �������� ����� �������� MIB ���������� --------
	switch(lpAnyValue->asnType)
	{
		case ASN_UNSIGNED32:
		case ASN_COUNTER64:
		case ASN_COUNTER32:
		case ASN_GAUGE32:
		case ASN_TIMETICKS:
		case ASN_INTEGER32:
			lpOffset += DigitToAsn(true, lpOffset, lpAnyValue );
		break;
		case ASN_OCTETSTRING:
		case ASN_BITS:
		case ASN_OBJECTIDENTIFIER:
		case ASN_SEQUENCE:
		case ASN_IPADDRESS:
		case ASN_OPAQUE:
			lpOffset += StringToAsn(true, lpOffset, lpAnyValue );
		break;
		case ASN_NULL:
			*(unsigned char *) (lpOffset++) = ASN_NULL;
			*(unsigned char *) (lpOffset++) = 0;
		break;
	}
//
return nHow;	
}
/////////////////////////////////////////////////////////////////////////
// ������� ������������ ASN.1 ����������� ��������� ����� 
// ���������:	bool Mode - ���� TRUE - ������ ������������ � �������� �����; ���� FALSE - ������ �� ������������ � �������� ����� 
//int Offset - �������� � �������� ������, ���� ������������ ���������
//����� 			unsigned char *Offset - ����� ��������� ������, ���� ������������ ���������
//				AsnAny *lpAnyValue - ��������� �� ���������, � ������� ���������� ������
// �������: ����� ������������� ������
///////////////////////////////////////////////////////////////////////
int DigitToAsn(bool fMode, unsigned char *Offset, AsnAny *lpAnyValue )
{
int nRet;
unsigned char *lpCodLen;
int Index;
//
nRet= 0;

	if(fMode == true) 
	{
		*(unsigned char *)Offset = lpAnyValue->asnType;
	}
	lpCodLen = (unsigned char *)&lpAnyValue->asnValue.unsigned32;
	for(nRet = 0, Index = (sizeof(int)-1); Index >= 0; Index--)
	{
		if( ( (*(lpCodLen+Index)) != 0) || (nRet > 0) )
		{
			if(fMode == true)
			{
				*(unsigned char *)(Offset+2+nRet) = (*(lpCodLen+Index)); 
			}
			nRet++;
		}
	}
	if(nRet == 0) 
	{
		if(fMode == true) *(unsigned char *)(Offset+2) = 0;
		nRet++;	
	}
	if(fMode == true) 
	{
		*(unsigned char *)(Offset+1) = (char)nRet;
	}
	nRet += 2;
//
return nRet;	
}
/////////////////////////////////////////////////////////////////////////
// ������� ������������ ASN.1 ����������� ������, �������������, IP ������
// ���������:	bool Mode - ���� TRUE - ������ ������������ � �������� �����; ���� FALSE - ������ �� ������������ � �������� ����� 
//int Offset - �������� � �������� ������, ���� ������������ ���������
//				AsnAny *lpAnyValue - ��������� �� ���������, � ������� ���������� ������
// �������: ����� ������������� ������
///////////////////////////////////////////////////////////////////////
int StringToAsn(bool fMode, unsigned char *lpOffset, AsnAny *lpAnyValue )
{
int nRet;
unsigned char *lpCodLen;
int Index;
//
	nRet = 0;

	if(fMode == true) 
	{
		*(unsigned char *)lpOffset = (char)lpAnyValue->asnType;
	}
	if(lpAnyValue->asnValue.string.length < 128)
	{
		nRet++;
		if(fMode == true) 
		{
			*(unsigned char *)( lpOffset+nRet) = (char)lpAnyValue->asnValue.string.length;
		}
		nRet++;
		if(fMode == true)
		{ 
			for( Index  = 0; Index < lpAnyValue->asnValue.string.length; Index++)
				*(unsigned char *)( lpOffset+nRet+Index) = *(unsigned char *)(lpAnyValue->asnValue.string.stream+Index);
		}
		nRet += lpAnyValue->asnValue.string.length;
	}
	else
	{
		lpCodLen = (unsigned char *)&lpAnyValue->asnValue.string.length;
		for( Index  = (sizeof(int) - 1); Index >= 0; Index--)
		{
			if( ( (*(lpCodLen+Index)) != 0) || (nRet > 0) )
			{
				if(fMode == true) 
				{
					*(unsigned char *)( lpOffset+2+Index) = (*(lpCodLen+Index));
				}
				nRet++;
			}
		}
		if(fMode == true) 
		{
			*(unsigned char *)( lpOffset+1) = 0x80 | nRet;
		}
		nRet += 2;
		nRet += lpAnyValue->asnValue.string.length;
	}
//
return nRet;	
}
///////////////////////////////////////////////////////////////////////
// ������� ���������� ����� � �������� �����
// ���������:	bool Mode - ���� TRUE - ������ ������������ � �������� �����; ���� FALSE - ������ �� ������������ � �������� ����� 
//				int CogingLen - ����� ������
//				int Offset - �������� � �������� ������, ���� ������������ ���������
// �������: ����� ������������� ������
///////////////////////////////////////////////////////////////////////
int WriteAsnLength(bool fMode, int CogingLen, unsigned char *lpOffset)
{
unsigned char *lpCodLen;
int nRet;
int Index;	
//
nRet= 0;

	if(CogingLen < 128)
	{
		//if(fMode == true) g_lpTxBuff[Offset++] = (char)CogingLen;
//		if(fMode == true){ IndirectWriteByte( Offset++, CogingLen); }
		if(fMode == true) *(unsigned char *) (lpOffset++) = CogingLen; //
		nRet = 1;
	}
	else
	{
		lpCodLen = (unsigned char *)&CogingLen;
		for(nRet = 0, Index = (sizeof(int)-1); Index >= 0; Index--)
		{
			if(  ( (*(lpCodLen+Index)) != 0) || (nRet > 0) )
			{
				//if(fMode == true) g_lpTxBuff[Offset+1+Index] = (*(lpCodLen+Index));
				//if(fMode == true){ IndirectWriteByte( Offset+1+Index, (*(lpCodLen+Index))); }
//				if(fMode == true){ IndirectWriteByte( Offset+1+nRet, (*(lpCodLen+Index))); }
				if(fMode == true) *(unsigned char *)( lpOffset+1+nRet) = (*(lpCodLen+Index)); 
				nRet++;
			}
		}
		//if(fMode == true) g_lpTxBuff[Offset] = 0x80 | (char)nRet;
//		if(fMode == true) { IndirectWriteByte( Offset, 0x80 | (char)nRet); }
		if(fMode == true) *(unsigned char *) lpOffset = 0x80 | (char)nRet; 
		nRet++;
	}
//
return nRet;	
}
///////////////////////////////////////////////////////////////////////
// ������� ����������� �������������� ��������� ����� ���������� � ������� � ���������� �� � 
// ��������� AsnAny.
// ���������:	int UsedOid - ����� ������������� OID
//			AsnObjectId *lpObjID - ��������� � ������������� OID
//			AsnAny *lpAnyValue - ��������� �� ���������� � ������� ������������ ������������� ���������
//			unsigned short ValType - ��� �������������� ���������. ��� ����� � ������ ������� ����� ���� 
// 						NOTE_BYFUNC(Atual Data Get By Function) && 
//					(NOTE_HASINDEX(Variable Are Indexed) || (NOTE_ISRDONLY(Data Is ReadONLY) )
//			unsigned long ValValue - ������������� ��������. ������� 2 ����� ���������� ������������� 
//								�������������� ���������. ������� - ��������� ��������� ����������� 
//								��������.
// �������: TRUE - ���� �����, ����� - FALSE.
///////////////////////////////////////////////////////////////////////
bool UploadValue(int UsedOid, AsnObjectId *lpObjID, AsnAny *lpAnyValue, unsigned short ValType, unsigned long ValValue)
{
static sCommand stComa; // ��������� ��� ���������� � ������� ��������� xQueueCommand
static sInfoKadr stReply; // ��������� ��� ������ �� ������� ��������
MKAPARAM *lpMka;
unsigned char bModType;
unsigned char fFromQueue;
unsigned char Coma[4];
unsigned int Mask=1;
unsigned short i, MaskShift;
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
unsigned int tt;
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//	
	// ������������ ������� � ���������� � ������� �� �������� � RS232 
 fFromQueue = false; 
 stComa.Source = UserSNMP; // �������� 
	stComa.A1 = 0;
	switch(ValValue&0x0000ffff)
	{
	// System Brunch
	case SYSUPTIME: 
		lpAnyValue->asnValue.unsigned32 = (unsigned int)xTaskGetTickCount()/1000;	
		if(lpAnyValue->asnValue.unsigned32 == 0xffffffff) lpAnyValue->asnValue.unsigned32 = 0;
		lpAnyValue->asnType = ASN_INTEGER; return true;
	// Product Info Branch
	case SITALLALARMSTATE: 
	case SITALLADMINSTATE:
		lpAnyValue->asnValue.unsigned32 = 0x00;	lpAnyValue->asnType = ASN_INTEGER; return true;
	case SITALLADRESSIP: 
		memcpy(lpAnyValue->asnValue.address.stream, g_IpParams.ucDataSIPR, 4);
		lpAnyValue->asnValue.address.length = 4; lpAnyValue->asnType = ASN_IPADDRESS;
	return true;
	// UK_CS block. 		
	// MKC Module	
	case MKCCARDNUM1:	
	case MKCCARDNUM: lpAnyValue->asnValue.unsigned32 = lpObjID->ObjectId[UsedOid]+12;
		lpAnyValue->asnType = ASN_INTEGER; return true;
	case UKCSCARDNUMBER:
	case MKACARDNUM: lpAnyValue->asnValue.unsigned32 = lpObjID->ObjectId[UsedOid];
		lpAnyValue->asnType = ASN_INTEGER; return true;
	case MKCFLOWINDEX:
	case PMSERIALNUM:
	case MKAPORTIND:lpAnyValue->asnValue.unsigned32 = lpObjID->ObjectId[UsedOid+1];
		lpAnyValue->asnType = ASN_INTEGER; return true;
	case MKAPORTONOF: lpAnyValue->asnValue.unsigned32 = 0x01;
		lpAnyValue->asnType = ASN_INTEGER; return true;
	case MKAPORTALARM:  lpAnyValue->asnValue.unsigned32 = 0;
		lpAnyValue->asnType = ASN_INTEGER; return true;
	case PMNUMBMANUF:
#ifndef SINFO_ENERGO
	    lpAnyValue->asnValue.unsigned32 = MassM1[lpObjID->ObjectId[UsedOid]-1];
#else
        lpAnyValue->asnValue.unsigned32 = lpObjID->ObjectId[UsedOid];
#endif
		lpAnyValue->asnType = ASN_INTEGER;
		return true;
	case PMSTATE: 
#ifndef SINFO_ENERGO
		if (!(MassM1[lpObjID->ObjectId[UsedOid]-1])) lpAnyValue->asnValue.unsigned32 = 0;
		else 
		{
			if (IsKbdReady(lpObjID->ObjectId[UsedOid]-1)) lpAnyValue->asnValue.unsigned32 = 1;
			else lpAnyValue->asnValue.unsigned32 = 2;
		}
#else
        lpAnyValue->asnValue.unsigned32 = 0;
#endif
		lpAnyValue->asnType = ASN_INTEGER;
		return true;
	//
	default:
		if (( ((ValValue&0x0000ffff) / 100) == 7) || ( ((ValValue&0x0000ffff) / 100) == 6) ) // ��� ��� ���
		{
			stComa.L = 2;
/*			stComa.BufCommand = pvPortMalloc( 2 ); // �������� �������
			if (stComa.BufCommand)*/
			{
				stComa.BufCommand[0] = GETCONSIST;
				stComa.BufCommand[1] = 0;
				if (  xQueueSend( xQueueCommand, &stComa, 1,0) == errQUEUE_FULL) // ���������� � ������� �������
				{   // ������� �����������
					g_ErrStatus = SNMP_ERRORSTATUS_GENERR;
					return false;
				}
			
				if( xQueueReceive( xQueueReply[stComa.Source], &stReply, 110 ) ) // 
				{
					Mask = (stReply.ByteInfoKadr[4] | (stReply.ByteInfoKadr[3]<<8) | 
						(stReply.ByteInfoKadr[2]<<16) | (stReply.ByteInfoKadr[1]<<24)); 
					MaskShift = lpObjID->ObjectId[UsedOid]-1;
					if ( ((ValValue&0x0000ffff) / 100) == 6) 
					{
						MaskShift = lpObjID->ObjectId[UsedOid]+11;	// ���
					}
					Mask >>=  MaskShift * 2; 
					Mask &= 0x00000003;
					if( (Mask != 0x00000002) )
					{ 
						// ������ �� � ������� ���������
 						lpAnyValue->asnType = ASN_INTEGER;
						switch(ValValue&0x0000ffff)
						{
							case MKAVERS: 
							case MKCSTATE:
							case MKCSTATE1:
							if (Mask == 0x00000001)
							{
								strcpy((char*)lpAnyValue->asnValue.string.stream, "Loader"); 
								lpAnyValue->asnValue.string.length = strlen("Loader"); 
								lpAnyValue->asnType = ASN_OCTETSTRING; return true;
							}
							else
							{
								strcpy((char*)lpAnyValue->asnValue.string.stream, "Absent"); 
								lpAnyValue->asnValue.string.length = strlen("Absent"); 
								lpAnyValue->asnType = ASN_OCTETSTRING; return true;
							}
							case MKAPORTTYPE: 
							case MKAPORTMODE:  
							case MKAPORTINPLEV:  
							case MKAPORTOUTLEV:  
							case MKAPORTARULEV:  
							case MKAPORTNOISELEV:  
							case MKAPORTTIMELEV:  
							case MKAPORTECHOSUPP:  
							case MKAARUONOFF:  
							case MKCCARDNUM:			
							case MKCFLOWINDEX:		
							case MKCFLOWE1:				
							case MKCPCM30:			
							case MKCEXTERNALTRAIN:
							case MKCINTERNALTRAIN:	
							case MKCKOEFFERR:		
							case MKCEXCESSKOEFFERR:
							case MKCSTATECRC:		
							case MKCLOSSINPUTSIGNAL:
							case MKCLOSSCYCLESYNCHR:	
							case MKCSINGLEERR:
							case MKCLOSSSYNCHRCRC:
							case MKCERRES:	
							case MKCERRSES:			
							case MKCERRAIS:			
							case MKCERRAIS_KI16:
							case MKCERRIAS:	
							case MKCDAMAGEREMOVEDSIDE:
							case MKCDAMAGEREMOVEDSIDE16:
							case MKCLEADER:// �������/�������		
							case MKCSOURCE1:// �������� ������������� � 1-� �����������		
							case MKCSOURCE2:// �������� ������������� � 2-� �����������		
							case MKCDAMAGE1:// ������ ������������� 1-�� ������		
							case MKCDAMAGE2:// ������ ������������� 2-�� ������			
							lpAnyValue->asnValue.unsigned32 = 0;
/*g_deb[ii++]= 0x1111;
g_deb[ii++]= MaskShift;
g_deb[ii++]= ValValue;*/
							return true;
						}
					}
				}
			}
			if ( ((ValValue&0x0000ffff) / 100) == 7)
			{ 
				bModType = MODULE_MKA;  fFromQueue = true; 
				Coma[0] = GETMKAPARAM; 
				Coma[1] = 0; 
				Coma[2] = (lpObjID->ObjectId[UsedOid+1]-1)  + ((lpObjID->ObjectId[UsedOid]-1)*4); 
			}
			else //if( ((ValValue&0x0000ffff) / 100) == 6)
			{
				bModType = MODULE_MKC; fFromQueue = true; 
				Coma[0] = 10; 
				Coma[1] = GETMKCPARAM; //107
				Coma[2] = lpObjID->ObjectId[UsedOid]+12; // � ������ ���
			}
			break;
		}
//		else if( ((ValValue&0x0000ffff) / 100) == 6)
//		{
//			bModType = MODULE_MKC; fFromQueue = true; Coma[0] = GETMKCPARAM; Coma[1] = lpObjID->ObjectId[UsedOid]; break;
//		}
		else if( ((ValValue&0x0000ffff) / 100) == 4) // ������ ������������ �� ��
		{
			bModType = BLOCK_UKCS; fFromQueue = true; Coma[0] = GETCONSIST; break;
		}
		else if( ((ValValue&0x0000ffff) / 100) == 2) // �������� ����� �� ��
		{
			if ( (ValValue & 0x0000ffff) == UKCSNUMBERSERIAL)
			{
				strcpy((char*)lpAnyValue->asnValue.string.stream, "SN 241"); 
				lpAnyValue->asnValue.string.length = strlen("SN 241"); 
				lpAnyValue->asnType = ASN_OCTETSTRING; return true;
			}
			else return false;		
		}
		else if(((ValValue&0x0000ffff) / 100) == 0)
		{
			if ( (ValValue & 0x0000ffff) == MUSWVERSION) // ������ �� ��
			{
				stComa.L = 1;
/*				stComa.BufCommand = pvPortMalloc( stComa.Length ); // �������� �������
				if (stComa.BufCommand)*/
				{
					stComa.BufCommand[0] = READVERSW;
					if (  xQueueSend( xQueueCommand, &stComa, 1,0 ) == errQUEUE_FULL) // ���������� � ������� ������
					{   // ������� �����������
						g_ErrStatus = SNMP_ERRORSTATUS_GENERR;
						return false;
					}
				
					if( xQueueReceive( xQueueReply[stComa.Source], &stReply, 110 ) ) // 
					{
						for (i=0; i<24; i++) 
						{
							lpAnyValue->asnValue.string.stream[i] = stReply.ByteInfoKadr[i+1];
						}
						for (i=(2*24); i<(3*24); i++) 
						{
							lpAnyValue->asnValue.string.stream[i-24] = stReply.ByteInfoKadr[i+1];
						}
						lpAnyValue->asnValue.string.length = 48; 
						lpAnyValue->asnType = ASN_OCTETSTRING; 
						return true;
					}
				}
			}
			else
			if ( (ValValue & 0x0000ffff) == MUTIMEPROCESSINTERRUPT) // ����� ��������� ����������
			{
				stComa.L = 2;
/*				stComa.BufCommand = pvPortMalloc( stComa.Length ); // �������� �������
				if (stComa.BufCommand)*/
				{
					stComa.BufCommand[0] = 10;
					stComa.BufCommand[1] = 100;
					if (  xQueueSend( xQueueCommand, &stComa, 1,0 ) == errQUEUE_FULL) // ���������� � ������� ������
					{   // ������� �����������
						g_ErrStatus = SNMP_ERRORSTATUS_GENERR;
						return false;
					}
					if( xQueueReceive( xQueueReply[stComa.Source], &stReply, 110 ) ) // 
					{
						lpAnyValue->asnType = ASN_INTEGER; 
						lpAnyValue->asnValue.unsigned32 = ( (stReply.ByteInfoKadr[2]*256) + stReply.ByteInfoKadr[3] )*20/1250;						
						return true;
					}
				}
			}
		}
		else return false;		
	}
	if( fFromQueue == true )
	{
		stComa.L = 3;
/*		stComa.BufCommand = pvPortMalloc( 3 ); // �������� �������
		if (stComa.BufCommand)*/
		{
			stComa.BufCommand[0] = Coma[0];
			stComa.BufCommand[1] = Coma[1];
			stComa.BufCommand[2] = Coma[2];			
			if (  xQueueSend( xQueueCommand, &stComa, 1,0 ) == errQUEUE_FULL) // ���������� � ������� �������
			{   // ������� �����������
				g_ErrStatus = SNMP_ERRORSTATUS_GENERR;
				return false;
			}			
			if( xQueueReceive( xQueueReply[stComa.Source], &stReply, 110 ) ) // 
			{
				switch(bModType)
				{
					case MODULE_MKA: 
						if (stReply.L != 2) // �� ������
						{	// ������� ����������� 
							lpAnyValue->asnType = ASN_INTEGER;
							switch(ValValue&0x0000ffff)
							{
							case MKAVERS: 
								strcpy((char*)lpAnyValue->asnValue.string.stream, "Ver.1.0"); 
								lpAnyValue->asnValue.string.length = strlen("Ver.1.0"); 
								lpAnyValue->asnType = ASN_OCTETSTRING; break;
							case MKAPORTTYPE: 
								lpAnyValue->asnValue.unsigned32 = ((MKAPARAM *)&stReply.ByteInfoKadr[3])->EndingType;								
								break;
							case MKAPORTMODE:  
								lpAnyValue->asnValue.unsigned32 = ((MKAPARAM *)&stReply.ByteInfoKadr[3])->EndingMode;
								break;
							case MKAPORTINPLEV:  
								lpAnyValue->asnValue.unsigned32 = ((MKAPARAM *)&stReply.ByteInfoKadr[3])->InpLevel;
								break;
							case MKAPORTOUTLEV:  
								lpAnyValue->asnValue.unsigned32 = ((MKAPARAM *)&stReply.ByteInfoKadr[3])->OutLevel;
								break;
							case MKAPORTARULEV:  
								lpAnyValue->asnValue.unsigned32 = ((MKAPARAM *)&stReply.ByteInfoKadr[3])->AruLevel;
								break;
							case MKAPORTNOISELEV:  
								lpAnyValue->asnValue.unsigned32 = ((MKAPARAM *)&stReply.ByteInfoKadr[3])->NoiseReject;
								break;
							case MKAPORTTIMELEV:  
								lpAnyValue->asnValue.unsigned32 = ((MKAPARAM *)&stReply.ByteInfoKadr[3])->LevelTime;
								break;
							case MKAPORTECHOSUPP:  
								lpAnyValue->asnValue.unsigned32 = (((MKAPARAM *)&stReply.ByteInfoKadr[3])->EchoSuppres >> 1) & 0x01;
								break;
							case MKAARUONOFF:  
								lpAnyValue->asnValue.unsigned32 = (((MKAPARAM *)&stReply.ByteInfoKadr[3])->EchoSuppres >> 5) & 0x01;
								break;
							default: 
								return false;
							}
						}
						else 
						{
						if	((ValValue&0x0000ffff) == MKAVERS)
							{
								strcpy((char*)lpAnyValue->asnValue.string.stream, "Absent"); 
								lpAnyValue->asnValue.string.length = strlen("Absent"); 
							}
							return false;
						}
						break;
						case MODULE_MKC:
							if (stReply.ByteInfoKadr[2] == 1) // �� ������
							{	// ������� ����������� 
								switch(ValValue&0x0000ffff)
								{
									// ��� ������ 		
									case MKCSTATE: // ��������� ���
									case MKCSTATE1: 
										strcpy((char*)lpAnyValue->asnValue.string.stream, "Working"); 
										lpAnyValue->asnValue.string.length = strlen("Working"); 
									lpAnyValue->asnType = ASN_OCTETSTRING; break;
									case  MKCFLOWE1: // ����� �������/��������	
										lpAnyValue->asnValue.unsigned32 = ((MKC_C0_C3 *)&stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+2])->FlowE1; 
										break;										
									case  MKCPCM30:		// ��� ������		
										lpAnyValue->asnValue.unsigned32 = ((MKC_C0_C3 *)&stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+2])->Pcm30; 
										break;										
									case  MKCEXTERNALTRAIN:		// ������� �����
										lpAnyValue->asnValue.unsigned32 = ((MKC_C0_C3 *)&stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+2])->ExternalTrain; 
										break;										
									case  MKCINTERNALTRAIN:	// ���������� �����		
										lpAnyValue->asnValue.unsigned32 = ((MKC_C0_C3 *)&stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+2])->InternalTrain; 
										break;										
									case  MKCKOEFFERR:		// �����. ������		
										lpAnyValue->asnValue.unsigned32 = ((MKC_C0_C3 *)&stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+2])->KoeffErr; 
										break;										
									case  MKCEXCESSKOEFFERR:		
										lpAnyValue->asnValue.unsigned32 = ((MKC_C6_CC *)&stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+11])->ExcessKoeffErr; 
										break;										
									case  MKCSTATECRC:	//��������� CRC				
										lpAnyValue->asnValue.unsigned32 = ((MKC_C0_C3 *)&stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+2])->StateCrc; 
										break;										
									case  MKCLOSSINPUTSIGNAL:	// 	������ ��������� ������
										lpAnyValue->asnValue.unsigned32 = ((MKC_C5_CB *)&stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+7])->LossInputSignal; 
										break;										
									case  MKCLOSSCYCLESYNCHR: //������ �������� �������������		
										lpAnyValue->asnValue.unsigned32 = ((MKC_C5_CB *)&stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+7])->LossCycleSynchr; 
										break;										
									case  MKCSINGLEERR:		// ������� ��������� ������	
										lpAnyValue->asnValue.unsigned32 = ((MKC_C6_CC *)&stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+11])->SingleErr; 
										break;										
									case  MKCLOSSSYNCHRCRC:		// ������ ������������� �R�
										lpAnyValue->asnValue.unsigned32 = ((MKC_C5_CB *)&stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+7])->LossSynchrCRC; 
										break;										
									case  MKCERRES:	//			
										lpAnyValue->asnValue.unsigned32 = ((MKC_C6_CC *)&stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+11])->ErrES; 
										break;										
									case  MKCERRSES:				
										lpAnyValue->asnValue.unsigned32 = ((MKC_C6_CC *)&stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+11])->ErrSES; 
										break;										
									case  MKCERRAIS:				
										lpAnyValue->asnValue.unsigned32 = ((MKC_C5_CB *)&stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+7])->ErrAIS; 
										break;										
									case  MKCERRAIS_KI16:			
										lpAnyValue->asnValue.unsigned32 = ((MKC_C5_CB *)&stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+7])->ErrAIS_KI16; 
										break;										
									case  MKCERRIAS:				
										lpAnyValue->asnValue.unsigned32 = ((MKC_C5_CB *)&stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+7])->ErrIAS; 
										break;										
									case  MKCDAMAGEREMOVEDSIDE:	
										lpAnyValue->asnValue.unsigned32 = ((MKC_C5_CB *)&stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+7])->DamageRemovedSide; 
										break;										
									case  MKCDAMAGEREMOVEDSIDE16:	
										lpAnyValue->asnValue.unsigned32 = ((MKC_C5_CB *)&stReply.ByteInfoKadr[7])->DamageRemovedSide16; 
										break;	
									// ��� ������ 		
									case MKCLEADER:// �������/�������		
										lpAnyValue->asnValue.unsigned32 = ((MKC_C4 *)&stReply.ByteInfoKadr[7])->Leader; 
										break;	
									case MKCSOURCE1:// �������� ������������� � 1-� �����������		
										lpAnyValue->asnValue.unsigned32 = ((MKC_C4 *)&stReply.ByteInfoKadr[7])->Source1; 
										break;	
									case MKCSOURCE2:// �������� ������������� � 2-� �����������		
										lpAnyValue->asnValue.unsigned32 = ((MKC_C4 *)&stReply.ByteInfoKadr[7])->Source2; 
										break;	
									case MKCDAMAGE1:// ������ ������������� 1-�� ������		
										lpAnyValue->asnValue.unsigned32 = ((MKC_CD *)&stReply.ByteInfoKadr[16])->Damage1; 
										break;	
									case MKCDAMAGE2:// ������ ������������� 2-�� ������			
										lpAnyValue->asnValue.unsigned32 = ((MKC_CD *)&stReply.ByteInfoKadr[16])->Damage2; 
										break;										
									default: 
										return false;
								}
							}
							else 
							{
								if	(((ValValue&0x0000ffff) == MKCSTATE) || ((ValValue&0x0000ffff) == MKCSTATE1) )
								{
									strcpy((char*)lpAnyValue->asnValue.string.stream, "Error"); 
									lpAnyValue->asnValue.string.length = strlen("Error"); 
									return true;
								}
								return false;
							}
/*g_deb[ii++]= 0x2222;
g_deb[ii++]= MaskShift;
g_deb[ii++]= ValValue;*/
						break;
//						case MODULE_MU:
//						break;
						case BLOCK_UKCS:
							lpAnyValue->asnType = ASN_INTEGER;
							switch(ValValue&0x0000ffff)
							{
							case UKCSCARDTYPE: 
								if( (lpObjID->ObjectId[UsedOid] >= MKA_POS_BEG) && (lpObjID->ObjectId[UsedOid] <= MKA_POS_END) )
								{ 
									Mask = (0 | stReply.ByteInfoKadr[4] | (stReply.ByteInfoKadr[3]<<8) | 
											(stReply.ByteInfoKadr[2]<<16) | (stReply.ByteInfoKadr[1]<<24) );

									Mask >>= ((lpObjID->ObjectId[UsedOid]-MKA_POS_BEG) * 2);
									Mask &= 3;
									if( (Mask == 0)||(Mask > 2) ) lpAnyValue->asnValue.unsigned32 = 0;
									else lpAnyValue->asnValue.unsigned32 = MODULE_MKA;
									break; 
								}
								else if( (lpObjID->ObjectId[UsedOid] >= MKC_POS_BEG) && (lpObjID->ObjectId[UsedOid] <= MKC_POS_END) )
								{ 
									Mask = (0 | stReply.ByteInfoKadr[4] | (stReply.ByteInfoKadr[3]<<8) | 
											(stReply.ByteInfoKadr[2]<<16) | (stReply.ByteInfoKadr[1]<<24) );
									Mask >>= ((lpObjID->ObjectId[UsedOid]-MKA_POS_BEG) * 2);
									Mask &= 3;
									if( (Mask == 0)||(Mask > 2) ) lpAnyValue->asnValue.unsigned32 = 0;
									else lpAnyValue->asnValue.unsigned32 = MODULE_MKC;  
									break; 
								}
								else if(lpObjID->ObjectId[UsedOid] == MU_POS_END)
								{
									lpAnyValue->asnValue.unsigned32 = MODULE_MU;  
									break; 
								}
							default: 
								return false;
							}
//						break;
					}
				return true;
			}
			else 
			{
				return false;
			}
		}
	}
//
return false;	
}
///////////////////////////////////////////////////////////////////////
// ������� ��������� �� ������������ � ������������� �������������� ��������� 
// ���������:	int UsedOid - ����� ������������� OID
//			AsnObjectId *lpObjID - ��������� � ������������� OID
//			AsnAny *lpAnyValue - ��������� �� ��������� � ������� ������������ ������������� ���������
//			unsigned short ValType - ��� �������������� ���������. ��� ����� � ������ ������� ����� ���� 
// 						NOTE_BYFUNC(Atual Data Get By Function) && 
//					(NOTE_HASINDEX(Variable Are Indexed) || (NOTE_ISRDONLY(Data Is ReadONLY) )
//			unsigned long ValValue - ������������� ��������. ������� 2 ����� ���������� ������������� 
//								�������������� ���������. ������� - ��������� ��������� ����������� 
//								��������.
// �������: TRUE - ���� �����, ����� - FALSE(��� ���� ��������������� ��� ������� ������ � SNMP ����).
///////////////////////////////////////////////////////////////////////
bool SetupValue(int UsedOid, AsnObjectId *lpObjID, AsnAny *lpAnyValue, unsigned short ValType, unsigned long ValValue)
{
static sCommand stComa; // ��������� ��� ���������� � ������� ��������� xQueueCommand
static sInfoKadr stReply; // ��������� ��� ������ �� ������� ��������
MKAPARAM *lpMka;
unsigned char bModType;
unsigned char fToQueue;
unsigned char Coma, ParamMKC;
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
short i;
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// ������������ ������� � ���������� � ������� �� ��������
 stComa.Source = UserSNMP; // �������� 
 stComa.A1 = 0;
 stComa.L = 6;
/* stComa.BufCommand = pvPortMalloc(6); // �������� �������
 if (stComa.BufCommand == 0) { g_ErrStatus = SNMP_ERRORSTATUS_GENERR; return false; }*/
 if ( ((ValValue&0x0000ffff) / 100) == 7) 
 {
	switch(ValValue&0x0000ffff)
	{
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!		
	case MKAPORTONOF:	 return false;
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!		
	case MKAPORTTYPE: 
		stComa.BufCommand[0] = COMAMKA_SETTYPE;
		stComa.BufCommand[1] = 0; 
		stComa.BufCommand[2] = (unsigned char)(lpObjID->ObjectId[UsedOid+1]-1)  + ((lpObjID->ObjectId[UsedOid]-1)*4); 
		stComa.BufCommand[3] = (unsigned char)lpAnyValue->asnValue.number; 
		stComa.BufCommand[4] = 0; 
		stComa.BufCommand[5] = 0; 				
	break;	
	case MKAPORTMODE:// ��������� ������ ������ ���������:1-����������������,2-�������������,3-��, 4-���
	
		if( (lpAnyValue->asnValue.number > 0) && (lpAnyValue->asnValue.number <= 4) )
		{
			stComa.BufCommand[0] = COMAMKA_SETMODE;
			stComa.BufCommand[1] = 0; 
			stComa.BufCommand[2] = (unsigned char)(lpObjID->ObjectId[UsedOid+1]-1)  + ((lpObjID->ObjectId[UsedOid]-1)*4); 
			stComa.BufCommand[3] = (unsigned char)lpAnyValue->asnValue.number; 
		}
		else { g_ErrStatus = SNMP_ERRORSTATUS_BADVALUE; goto BadResult; }
	break;
	case MKAPORTINPLEV:
		if( (lpAnyValue->asnValue.number >= 0) && (lpAnyValue->asnValue.number <= 28) )
		{
			stComa.BufCommand[0] = COMAMKA_SETINPLEV;
			stComa.BufCommand[1] = 0; 
		stComa.BufCommand[2] = (unsigned char)(lpObjID->ObjectId[UsedOid+1]-1)  + ((lpObjID->ObjectId[UsedOid]-1)*4); 
			stComa.BufCommand[3] = (unsigned char)lpAnyValue->asnValue.number; 
		}
		else { g_ErrStatus = SNMP_ERRORSTATUS_BADVALUE; goto BadResult; }
	break;
	case MKAPORTOUTLEV:
		if( (lpAnyValue->asnValue.number >= 1) && (lpAnyValue->asnValue.number <= 28) )
		{
			stComa.BufCommand[0] = COMAMKA_SETOUTLEV;
			stComa.BufCommand[1] = 0; 
			stComa.BufCommand[2] = (unsigned char)(lpObjID->ObjectId[UsedOid+1]-1)  + ((lpObjID->ObjectId[UsedOid]-1)*4); 
			stComa.BufCommand[3] = (unsigned char)lpAnyValue->asnValue.number; 
		}
		else { g_ErrStatus = SNMP_ERRORSTATUS_BADVALUE; goto BadResult; }
	break;
	case MKAPORTARULEV:
		if( (lpAnyValue->asnValue.number >= 0) && (lpAnyValue->asnValue.number <= 28) )
		{
			stComa.BufCommand[0] = COMAMKA_SETARULEV;
			stComa.BufCommand[1] = 0; 
			stComa.BufCommand[2] = (unsigned char)(lpObjID->ObjectId[UsedOid+1]-1)  + ((lpObjID->ObjectId[UsedOid]-1)*4); 
			stComa.BufCommand[3] = (unsigned char)lpAnyValue->asnValue.number; 
		}
		else { g_ErrStatus = SNMP_ERRORSTATUS_BADVALUE; goto BadResult; }
	break;
	case MKAPORTNOISELEV:
		if( (lpAnyValue->asnValue.number >= 0) && (lpAnyValue->asnValue.number <= 100) )
		{
			stComa.BufCommand[0] = COMAMKA_SETECHOSUPPR;
			stComa.BufCommand[1] = 0; 
		stComa.BufCommand[2] = (unsigned char)(lpObjID->ObjectId[UsedOid+1]-1)  + ((lpObjID->ObjectId[UsedOid]-1)*4); 
			stComa.BufCommand[3] = (unsigned char)lpAnyValue->asnValue.number; 
		}
		else { g_ErrStatus = SNMP_ERRORSTATUS_BADVALUE; goto BadResult; }
	break;
	case MKAPORTTIMELEV:
		if( (lpAnyValue->asnValue.number >= 0) && (lpAnyValue->asnValue.number <= 32) )
		{
			stComa.BufCommand[0] = COMAMKA_SETTIMELEV;
			stComa.BufCommand[1] = 0; 
		stComa.BufCommand[2] = (unsigned char)(lpObjID->ObjectId[UsedOid+1]-1)  + ((lpObjID->ObjectId[UsedOid]-1)*4); 
			stComa.BufCommand[3] = (unsigned char)lpAnyValue->asnValue.number; 
		}
		else { g_ErrStatus = SNMP_ERRORSTATUS_BADVALUE; goto BadResult; }
	break;
	case MKAPORTECHOSUPP:
		if(lpAnyValue->asnValue.number & 0x01) stComa.BufCommand[0] = COMAMKA_SETECHOON;
		else  stComa.BufCommand[0] = COMAMKA_SETECHOOFF;
		stComa.BufCommand[1] = 0; 
		stComa.BufCommand[2] = (unsigned char)(lpObjID->ObjectId[UsedOid+1]-1)  + ((lpObjID->ObjectId[UsedOid]-1)*4); 
		break;
	case MKAARUONOFF:
		if( (lpAnyValue->asnValue.number >= 0) && (lpAnyValue->asnValue.number <= 1) )
		{
			stComa.BufCommand[0] = COMAMKA_MKAARUONOFF;
			stComa.BufCommand[1] = 0; 
			stComa.BufCommand[2] = (unsigned char)(lpObjID->ObjectId[UsedOid+1]-1)  + ((lpObjID->ObjectId[UsedOid]-1)*4); 
			stComa.BufCommand[3] = (unsigned char)lpAnyValue->asnValue.number; 
		}
		else { g_ErrStatus = SNMP_ERRORSTATUS_BADVALUE; goto BadResult; }
		break;
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!		
//	case MKAPORTOVERVOLT: 	return false;
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!		
	default: return false;
	}
	// Send Command
	if (  xQueueSend( xQueueCommand, &stComa, 5,0 ) == errQUEUE_FULL) // ���������� � ������� ������ 
	{   // ������� �����������
BadResult:			
		return false;
	}
	// Wait Reply
	if( xQueueReceive( xQueueReply[stComa.Source], &stReply, 100 ) ) // 
	{
		if(stReply.ByteInfoKadr[1] != CodeStatusTrue)
		{
			g_ErrStatus = SNMP_ERRORSTATUS_GENERR;
			return false;
		}
	}
//
 return true;	
 }
 else  if ( ((ValValue&0x0000ffff) / 100) == 6) // ��� 
 {
		// ��� ������ 		
//		bModType = MODULE_MKC; fFromQueue = true; 
		stComa.BufCommand[0] = 10; 
		stComa.BufCommand[1] = GETMKCPARAM; //107
		stComa.BufCommand[2] = lpObjID->ObjectId[UsedOid]+12; // � ������ ���
		if (  xQueueSend( xQueueCommand, &stComa, 5,0 ) == errQUEUE_FULL) // ���������� � ������� ������ 
		{   // ������� �����������
			return false;
		}
		// Wait Reply
		if( xQueueReceive( xQueueReply[stComa.Source], &stReply, 100 ) ) // 
		{
			if (stReply.ByteInfoKadr[2] != 1) //  ������
			{
				g_ErrStatus = SNMP_ERRORSTATUS_GENERR;
				return false;
			}
		}
/*	stComa.BufCommand = pvPortMalloc(6); // �������� �������
	if (!stComa.BufCommand) { g_ErrStatus = SNMP_ERRORSTATUS_GENERR; return false; }*/
	stComa.BufCommand[0] = 10;
	stComa.BufCommand[1] = 102; 
	stComa.BufCommand[2] = lpObjID->ObjectId[UsedOid]+12; // � ������ ��� 	
	switch(ValValue&0x0000ffff)
	{
// !!!!!!!!!!!!   C0 .. C3!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!		
		case  MKCFLOWE1:	// ����� �������/��������
			stComa.BufCommand[3] = 0xC0+lpObjID->ObjectId[UsedOid+1]-1; // ����� ��������
			stComa.BufCommand[4] = stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+2] & (~0x80);
			stComa.BufCommand[4] |= (unsigned char)lpAnyValue->asnValue.number << 7; 
			break;										
		case  MKCPCM30:		// ��� ������		
			stComa.BufCommand[3] = 0xC0+lpObjID->ObjectId[UsedOid+1]-1; // ����� ��������
			stComa.BufCommand[4] = stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+2] & (~0x40);
			stComa.BufCommand[4] |= (unsigned char)lpAnyValue->asnValue.number << 6; 
			break;										
		case  MKCSTATECRC:	//��������� CRC			
			stComa.BufCommand[3] = 0xC0+lpObjID->ObjectId[UsedOid+1]-1; // ����� ��������
			stComa.BufCommand[4] = stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+2] & (~0x20);
			stComa.BufCommand[4] |= (unsigned char)lpAnyValue->asnValue.number << 5; 
			break;										
		case  MKCINTERNALTRAIN:	// ���������� �����	
			stComa.BufCommand[3] = 0xC0+lpObjID->ObjectId[UsedOid+1]-1; // ����� ��������
			stComa.BufCommand[4] = stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+2] & (~0x10);
			stComa.BufCommand[4] |= (unsigned char)lpAnyValue->asnValue.number << 4; 
			break;										
		case  MKCEXTERNALTRAIN:		// ������� �����
			stComa.BufCommand[3] = 0xC0+lpObjID->ObjectId[UsedOid+1]-1; // ����� ��������
			stComa.BufCommand[4] = stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+2] & (~0x8);
			stComa.BufCommand[4] |= (unsigned char)lpAnyValue->asnValue.number << 3; 
			break;										
		case  MKCKOEFFERR:	//	����� ������������ ������
			stComa.BufCommand[3] = 0xC0+lpObjID->ObjectId[UsedOid+1]-1; // ����� ��������
			stComa.BufCommand[4] = stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]+2] & (~0x6);
			stComa.BufCommand[4] |= (unsigned char)lpAnyValue->asnValue.number << 1; 
			break;	
// !!!!!!!!!!!!   C4  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!		
		case MKCLEADER:// �������/�������		
			stComa.BufCommand[3] = 0xC4; // ����� ��������
			stComa.BufCommand[4] = stReply.ByteInfoKadr[7] & (~0x40);
			stComa.BufCommand[4] |= (unsigned char)lpAnyValue->asnValue.number << 6; 
			break;	
		case MKCSOURCE1:// �������� ������������� � 1-� �����������		
			stComa.BufCommand[3] = 0xC4; // ����� ��������
			stComa.BufCommand[4] = stReply.ByteInfoKadr[7] & (~0x38);
			stComa.BufCommand[4] |= (unsigned char)lpAnyValue->asnValue.number << 3; 
			break;	
		case MKCSOURCE2:// �������� ������������� � 2-� �����������		
			stComa.BufCommand[3] = 0xC4; // ����� ��������
			stComa.BufCommand[4] = stReply.ByteInfoKadr[7] & (~0x7);
			stComa.BufCommand[4] |= (unsigned char)lpAnyValue->asnValue.number; 
			break;	
		default: return false;
	}
	if (  xQueueSend( xQueueCommand, &stComa, 5,0 ) == errQUEUE_FULL) // ���������� � ������� ������ 
	{   // ������� �����������
		return false;
	}
	// Wait Reply
	if( xQueueReceive( xQueueReply[stComa.Source], &stReply, 100 ) ) // 
	{
		if (stReply.ByteInfoKadr[2] != 1) //  ������
		{
			g_ErrStatus = SNMP_ERRORSTATUS_GENERR;
			return false;
		}
	}
	return true;	
 }
 return false;	
}
//////////////////////////////////////////////////////////////////////////////////////////
// ������� ��������� ����� � �������� � �������������� TRAP ���������.
// ���������: SITALTRAP *lpBuff - ����������� �����
//				unsigned int *lpBegTx - ����� ����������, � ������� ������������ ����� ������ UDP ���������
//				unsigned int *lpEndTx - ����� ����������, � ������� ������������ ����� ����� UDP ���������
// �������: TRUE - ���� ������; FALSE -���� �����
//////////////////////////////////////////////////////////////////////////////////////////
bool CreateTrap(SITALTRAP *lpBuff, unsigned int *lpBegTx, unsigned int *lpEndTx)
{
int DataLen, TmpInt, TrapType;
unsigned char *OutBuffPtr;
//unsigned int OutBuffPtr;
AsnAny AnyValue;
AsnObjectId ObjID;
unsigned int Dovesok, tt;
HANDLE hFile;
//
   csync();
	OutBuffPtr = &tcpSendBufSnmp[TX_WRITE_OFFSET];
	(*(lpEndTx)) = *(unsigned char *)OutBuffPtr;
	// Write Address
	memcpy(ObjID.ObjectId, &g_OidBypass[UKCSTABIND].ObjectId, g_OidBypass[UKCSTABIND].idLength);
	ObjID.idLength = g_OidBypass[UKCSTABIND].idLength;
	ObjID.ObjectId[ObjID.idLength++] = 125; // sitallOkTrap 	::= { sitall_UKCS 125 }
	
	AnyValue.asnType = ASN_OCTETSTRING;
	TrapType = FormatTrapMsg(lpBuff, AnyValue.asnValue.string.stream);

	AnyValue.asnValue.string.length = strlen((char *)AnyValue.asnValue.string.stream);

	DataLen = WriteOutValue( &ObjID, &AnyValue, OutBuffPtr, true );
	OutBuffPtr += DataLen;
	// Write Port
	//
	(*(lpEndTx)) = (int)OutBuffPtr;
	OutBuffPtr = &tcpSendBufSnmp[TX_WRITE_OFFSET];
	// Write TrapData Object ID
	DataLen = (int)(*(lpEndTx))-(int)&tcpSendBufSnmp[TX_WRITE_OFFSET];
	Dovesok = WriteAsnLength(false, DataLen, 0);
	OutBuffPtr -= Dovesok;
	WriteAsnLength(true, DataLen, OutBuffPtr);
	OutBuffPtr--;

	*(unsigned char *)(OutBuffPtr--) =  ASN_SEQUENCE; // 0x30
	// Write Time Stamp
	AnyValue.asnType = ASN_INTEGER;
	AnyValue.asnValue.unsigned32 = (unsigned int)xTaskGetTickCount()/1000;
	Dovesok = DigitToAsn(false, OutBuffPtr, &AnyValue );
	OutBuffPtr -= (Dovesok-1);
	DigitToAsn(true, OutBuffPtr, &AnyValue );
	OutBuffPtr--;
	// Write specific TRAP: "enterpriseCpecific Trap"(6)
	*(unsigned char *)(OutBuffPtr--) =  TrapType; // {1,2,3}
	*(unsigned char *)(OutBuffPtr--) =  1; 
	*(unsigned char *)(OutBuffPtr--) =  ASN_INTEGER;
	// Write TRAP-Type
	*(unsigned char *)(OutBuffPtr--) =  6; 
	*(unsigned char *)(OutBuffPtr--) =  1; 
	*(unsigned char *)(OutBuffPtr) =  ASN_INTEGER;
	// Write Agent's Address
	AnyValue.asnType = ASN_IPADDRESS;
	AnyValue.asnValue.string.length = 4;
	memcpy(AnyValue.asnValue.string.stream, g_IpParams.ucDataSIPR, 4);
	Dovesok = StringToAsn(false, OutBuffPtr, &AnyValue );
	OutBuffPtr -= Dovesok;
	Dovesok = StringToAsn(true, OutBuffPtr, &AnyValue );
	// Write Enterprise's Object Identifier {0x08,0x2b,0x06,0x01,0x04,0x01,0xe0,0x39,0x01};
	AnyValue.asnType = ASN_OBJECTIDENTIFIER;
	AnyValue.asnValue.string.length = g_OidBypass[ENTERPRISESTABIND].idLength;
	memcpy(AnyValue.asnValue.string.stream, g_OidBypass[ENTERPRISESTABIND].ObjectId, g_OidBypass[ENTERPRISESTABIND].idLength);
	Dovesok = StringToAsn(false, OutBuffPtr, &AnyValue );
	OutBuffPtr -= Dovesok;
	Dovesok = StringToAsn(true, OutBuffPtr, &AnyValue );
	// Write PDU's Frame Length
	DataLen = (*(lpEndTx))-(int)OutBuffPtr;
	Dovesok = WriteAsnLength(false, DataLen, 0);
	OutBuffPtr -= Dovesok;
	WriteAsnLength(true, DataLen, OutBuffPtr);
	OutBuffPtr--;
	// PDU Type
	*(unsigned char *)(OutBuffPtr) =  SNMP_PDU_V1TRAP;
	// Write Community
	hFile = CreateFile((char *)g_agm32ComunityRO, GENERIC_READ, 0);
	AnyValue.asnValue.string.length = ReadFile(hFile, AnyValue.asnValue.string.stream, -1);
	AnyValue.asnType = ASN_OCTETSTRING;
	CloseHandle( hFile );
	Dovesok = StringToAsn(false, OutBuffPtr, &AnyValue );
	OutBuffPtr -= Dovesok;
	Dovesok = StringToAsn(true, OutBuffPtr, &AnyValue );
	OutBuffPtr--;
	// Write SNMP Version
	*(unsigned char *)(OutBuffPtr--) =  SNMP_V1_0; 
	*(unsigned char *)(OutBuffPtr--) =  1; 
	*(unsigned char *)(OutBuffPtr) =  ASN_INTEGER;
	// Write Frame Length & ASN_SEQUENCE
	DataLen = (*(lpEndTx))-(int)OutBuffPtr;
	Dovesok = WriteAsnLength(false, DataLen, 0);
	OutBuffPtr -= Dovesok;
	WriteAsnLength(true, DataLen, OutBuffPtr);
	OutBuffPtr--;
	*(unsigned char *)(OutBuffPtr) =  ASN_SEQUENCE; // 0x30
	(*(lpBegTx)) = (int)OutBuffPtr;
//
return true;
}
//////////////////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////////////////
unsigned char mass1[8];
unsigned portCHAR IPadrSnmp[4]; //IP ����� �� 4-� ����
unsigned short portSnmp; // ����

bool SnmpTask(void)
{
unsigned char mass[8];
int HowRecv, i, SnmpLen, Index;
unsigned int EndInp;
int TrapType;
unsigned char stream[ASNSTRINGMAXLEN];
unsigned int BegTx, EndTx;
//unsigned portCHAR IPadrSnmp[4]; //IP ����� �� 4-� ����
//unsigned short portSnmp; // ����
//SITALTRAP Buff;
static sInfoKadr stReply;
bool tr;
bool Go = false; // �������� ������
//
//    csync();

	// Wait TRAP From "IRON"
	if( xQueueReceive( xQueueReply[UserSNMP], &stReply, 0 ) )
	{		
//----  ��������� / ���������� �������� ������������ ----
		TrapType = FormatTrapMsg((SITALTRAP *)&stReply.ByteInfoKadr[0], stream);
		if (TrapType == 3) // ������ 
		{
			// �������� ������ �� ������ � ������ � �������� ������������, ���� �� ��������
			InsertCrash((SITALTRAP *)&stReply.ByteInfoKadr[0]);
		}
		else if (TrapType == 1) // ������ �������
		{
			// ������� ������ �� ������ � ��������� ������������, ���� ������ �� ��������
			DeleteCrash((SITALTRAP *)&stReply.ByteInfoKadr[0]);		
		}
//-------------------------------------------------------
#ifdef	__SNMP__
!!! ���� ���������� ������ � ����� ��� SNMP !!!
		BegTx = EndTx = 0;
 		if (Go)
			if( CreateTrap( (SITALTRAP *)&stReply.ByteInfoKadr[0], &BegTx, &EndTx) == true )
			{
				//
				// SEND
				portSnmp = g_IpParams.wPortSNMP + 1; // 162
				sendto(SNMP_SOCKET, (unsigned char*)BegTx, (unsigned char*)EndTx-(unsigned char*)BegTx, (unsigned char*)g_IpParams.ucDataSNMPmIP, portSnmp);
				BegTx = EndTx = 0;
			}
		//
#endif
	}

#ifdef	__SNMP__
	//
	HowRecv= select(SNMP_SOCKET, SEL_RECV); // ���� ������ � ���������
	if (HowRecv > 0)
	{
		Go = true;
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//CountSnmp++;
//g_deb[ii++]= CountSnmp;
//LedOn(ledUpr); // �������� ���������
//vTaskDelay(100);
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		HowRecv = recvfrom(SNMP_SOCKET, tcpRecBufSnmp, HowRecv, IPadrSnmp, (uint16 *)&portSnmp);
		SnmpLen = 0;
		BegTx = EndTx = 0;
		if( SnmpParser(tcpRecBufSnmp, &EndInp, &BegTx, &EndTx, &SnmpLen) == true)
		{
			sendto(SNMP_SOCKET, (unsigned char*)BegTx, (unsigned char*)EndTx-(unsigned char*)BegTx, IPadrSnmp, portSnmp);
		}
		else //if( (BegTx + EndTx) != 0) // ���� ��������� SNMP- ����� ��������� � �������.
		{
			for(i=0; i<SnmpLen; i++)
			{
				mass[0] = tcpRecBufSnmp[i];
				if(((int)BegTx - (int)tcpRecBufSnmp) == i) tcpSendBufSnmp[i] = SNMP_PDU_RESPONSE;
				else if(((int)EndTx - (int)tcpRecBufSnmp) == i) tcpSendBufSnmp[i] = (unsigned char)g_ErrStatus;
				else tcpSendBufSnmp[i] = mass[0];
			}
			sendto(SNMP_SOCKET, tcpSendBufSnmp, SnmpLen, IPadrSnmp, portSnmp);
		}
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//LedOff(ledUpr); // ����� ���������
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	}
#endif
//
return true;	
}


extern short CountErrMalloc1; // ������� ������ ��� ��������� ������ heap1
//////////////////////////////////////////////////////////////////////////////////////////
//	 	�������� ������ �� ������ � ������ � �������� ������������, ���� �� ��������
//////////////////////////////////////////////////////////////////////////////////////////
void InsertCrash(SITALTRAP *codTrap)
{
  sListCrash *newSt; // ��������� �� ������ ������

	newSt=pvPortMalloc(sizeof(sListCrash));
	if(newSt)
	{
		newSt->psCrash = 0;
		newSt->Inf.d_t = ReadClock();
		newSt->Inf.cod.bComa = 0x95;
		newSt->Inf.cod.bAdr = codTrap->bAdr;
		newSt->Inf.cod.bPort = codTrap->bPort;
		newSt->Inf.cod.Reason = codTrap->Reason;
		if(!endList) // ������ ������
		{
			beginList = newSt;
			*pFIO_FLAG_C = 0x8; // �������� ������������
		}
		else endList->psCrash = (int)newSt;
		endList = newSt;
	}
}
//////////////////////////////////////////////////////////////////////////////////////////
// 		������� ������ �� ������ � ��������� ������������, ���� ������ �� ��������
//////////////////////////////////////////////////////////////////////////////////////////
void DeleteCrash(SITALTRAP *codTrap)
{
sListCrash *preSt; // ��������� �� ���������� ��������� ������
sListCrash *currSt; // ��������� �� ������� ��������� ������
bool find;
static short CountDC = 0;

	find = true;
	preSt = NULL;
	currSt = beginList;
	while (find) // ����� ��������� ������
	{
		if ( ( currSt->Inf.cod.bAdr == codTrap->bAdr) && (currSt->Inf.cod.bPort == codTrap->bPort) &&
			  ( (currSt->Inf.cod.Reason == codTrap->Reason - 1) || 
				( (currSt->Inf.cod.Reason > 80) && (currSt->Inf.cod.Reason < 100) ) ||
				( (codTrap->bPort == 0xFF) && (!currSt->Inf.cod.Reason))
			  ))
		{
			find = false; // ����� ��������
			if (preSt) 
			{ // ���� ���������� ���������
				preSt->psCrash = currSt->psCrash;
				if (currSt == endList) endList = preSt;
			}
			else // ������ ������
			{
				if (currSt == endList) 
				{
					// ��� ������ ������
					endList=NULL; // ��������� �� ������ ������
					beginList=NULL;  // ��������� �� ����� ������
					// ���������� ������������
					*pFIO_FLAG_S = 0x8;
				}
				else beginList = (sListCrash *)currSt->psCrash;
			}
			vPortFree(currSt); // ������������ ������
		}
		else
		{
			if (currSt == endList) // ����� ������ - ������ �� �������
			{
				find = false; // ����� ��������
//return 0;	
			}
			else
			{
				preSt = currSt;
				currSt = (sListCrash *)(currSt->psCrash);
			}
		}
	}
//return 1;	
}

const  char CrMU[] = "�� ";
const  char CrTimeInterr[] = "����� ��������� ���������� > 80%";
const  char CrMKA[] = "��� ";
const  char CrNoReply[] = "�� ��������";
const  char CrMKC[] = "��� ";
const  char CrCrSin[] = "������ ������������� 1-�� ������"; 
const  char CrE1[] = "����� ";
const  char CrInSign[] = "������ �������� �������";
const  char CrCyclSin[] = "������ �������� �������������";
const  char CrPult[] = "����� ";

//////////////////////////////////////////////////////////////////////////////////////////
// 		����� ������ � ������ �� ������
//		������� - ���������� ����
//////////////////////////////////////////////////////////////////////////////////////////
unsigned short ReadCrash(unsigned char NumbSt, unsigned char *CrashString)
{
sListCrash *currSt; // ��������� �� ������� ��������� ������
unsigned char count = 0;
unsigned short lIndex, i;
struct tm *ttt;
time_t t1;
static unsigned char str1[50], str2[50];
// 
	lIndex = 0;
	CrashString[0] = 0;	// ������ ������
	if (!(endList)) return  (lIndex); // ������ ����
	currSt = beginList; // ������ ������
	while (currSt != NULL)
	{
		// ����� ������ ������
		count++; // ����� ������
		if (count == NumbSt)
		{
		  LockPrintf();
			if (!(currSt->Inf.cod.bAdr)) // �� 
			{
				strcpy((char *)str1, (char *)CrMU);
				strcpy((char *)str2,(char *)CrTimeInterr);
			}
			else if (currSt->Inf.cod.bAdr == 0xFF) // �����  
			{
				strcpy((char *)str1, (char *)CrPult);
				sprintf((char *)str1+6, "%3d", currSt->Inf.cod.bPort);
				strcpy((char *)str2,(char *)CrNoReply);
			}
			else if (currSt->Inf.cod.bAdr < 13) // ��� 
			{
				strcpy((char *)str1, (char *)CrMKA);
				sprintf((char *)(str1+4),"%2d",currSt->Inf.cod.bAdr);
				strcpy((char *)str2,(char *)CrNoReply);
	
			}
			else if ((currSt->Inf.cod.bAdr <= 16) && !(currSt->Inf.cod.bPort)) // ���
			{
				strcpy((char *)str1, (char *)CrMKC);
				sprintf((char *)(str1+4),"%2d",currSt->Inf.cod.bAdr-12); // ����� ���
				strcpy((char *)str2,(char *)CrCrSin); // ������ ������������� 1-�� ������
			}
			else if ((currSt->Inf.cod.bAdr <= 16) && (currSt->Inf.cod.bPort == 0xFF)) // ���
			{
				strcpy((char *)str1, (char *)CrMKC);
				sprintf((char *)(str1+4),"%2d",currSt->Inf.cod.bAdr-12); // ����� ���
				strcpy((char *)str2,(char *)CrNoReply);
			}
			else if ((currSt->Inf.cod.bAdr <= 16) && (currSt->Inf.cod.Reason == 0x0B)) // E1
			{
				strcpy((char *)str1, (char *)CrE1); // "�����  "
				sprintf((char *)(str1+6),"%2d",currSt->Inf.cod.bPort); // ����� ������
				strcat((char *)str1, (char *)CrMKC); // "��� "
				lIndex = strlen((char *) str1 );
				sprintf((char *)(str1+lIndex),"%2d",currSt->Inf.cod.bAdr-12); // ����� ���
				strcpy((char *)str2,(char *)CrCyclSin); // "������ �������� �������������"
				
			}
			else if ((currSt->Inf.cod.bAdr <= 16) && (currSt->Inf.cod.Reason == 0x0F)) // E1
			{
				strcpy((char *)str1, (char *)CrE1); // "�����  "
				sprintf((char *)(str1+6),"%2d",currSt->Inf.cod.bPort); // ����� ������
				strcat((char *)str1, " "); // " "
				strcat((char *)str1, (char *)CrMKC); // "��� "
				lIndex = strlen( (char *)str1 );
				sprintf((char *)(str1+lIndex),"%2d",currSt->Inf.cod.bAdr-12); // ����� ���
				strcpy((char *)str2,(char *)CrInSign); // ������ �������� �������
			}		
			CrashString[0] = NumbSt;
			lIndex = 1;
			t1=currSt->Inf.d_t ;
		 	ttt=gmtime(&t1);
			sprintf((char *)CrashString+lIndex,"%2.2d:%2.2d:%2.2d %2.2d.%2.2d.%4.4d",
			        ttt->tm_hour, ttt->tm_min, ttt->tm_sec, ttt->tm_mday, ttt->tm_mon+1, ttt->tm_year+1900);
			UnlockPrintf();
			lIndex+=20;
			i=strlen((char *)str1);
			if(i+lIndex<250)
			{ 
			   memcpy(CrashString+lIndex,str1,i);
			   lIndex+=i;
			   CrashString[lIndex++]=0;
			   i=strlen((char *)str2);
			   if(i+lIndex<250)
			   {
			     memcpy(CrashString+lIndex,str2,i);
			     lIndex+=i;
			     CrashString[lIndex++]=0;
			   }
			}
			return lIndex;
		}
		else currSt = (sListCrash *)currSt->psCrash;
	}
 return (lIndex);
}
//////////////////////////////////////////////////////////////////////////////////////////
// ������� ��������� ����� � �������� � ����������� TRAP ��������� ��� ������.
// ���������: SITALTRAP *lpBuff - ����������� �����
//				unsigned char *lpOutBuff - �������� �����
// �������: 1 - ������� ���������; 2 - ������������� ������; 3 - ����������� ������
//////////////////////////////////////////////////////////////////////////////////////////
int FormatTrapMsg(SITALTRAP *lpBuff, unsigned char *lpOutBuff)
{
//
    LockPrintf();
	sprintf((char *)lpOutBuff, "Mod:%u, Port:%u, Code:%u", lpBuff->bAdr, lpBuff->bPort, lpBuff->Reason);
	UnlockPrintf();
	if ( (lpBuff->bPort == 0xff) && ((lpBuff->Reason == 0x02) )) 
	{
/*StepSNMP[ii++] = lpBuff->bAdr;			
StepSNMP[ii++] = lpBuff->bPort;			
StepSNMP[ii++] = lpBuff->Reason;			
StepSNMP[ii++] = 1;	*/	
		
		return 1;	// ��� ������
	}
	if (lpBuff->bAdr == 0xff) // ����� 
	{
		 if (lpBuff->Reason == 0x00) return 2; 
	}	
	if (!(lpBuff->bAdr) && ( (lpBuff->Reason < 80) && (lpBuff->Reason > 60) )) return 2; // �� - ����� ��������� ���������� 
	if ( ((lpBuff->bPort == 0xff) && (lpBuff->Reason == 0x01)) || (lpBuff->Reason >= 80)) 
	{
/*StepSNMP[ii++] = lpBuff->bAdr;			
StepSNMP[ii++] = lpBuff->bPort;			
StepSNMP[ii++] = lpBuff->Reason;			
StepSNMP[ii++] = 3;	*/	
		return 3; // ������
	}	
	if( (lpBuff->bAdr >= 13) && (lpBuff->bAdr <= 16)) // ���
	{
	 	if(lpBuff->bPort == 0x00)
	 	{
	 		if (lpBuff->Reason == 0x0F) return 3;
	 		else if( (lpBuff->Reason == 0x09) || (lpBuff->Reason == 0x0b) || (lpBuff->Reason == 0x0f) || (lpBuff->Reason == 0x0d)) return 2;
	 	}
	 	else if( (lpBuff->bPort >= 1) && (lpBuff->bPort <= 4) ) 
	 	{
	 		if ((lpBuff->Reason == 0x0B) || (lpBuff->Reason == 0x0F)) return 3; // ����������� ������
	 		else if (lpBuff->Reason % 2 ) return 2;	// ��������� �������� - ������������� ������
	 	}	
	}
	if ( (lpBuff->bPort == 0xff) && (lpBuff->Reason % 2 )) return 2;	// ������������� ������
//
return 1;	
}


