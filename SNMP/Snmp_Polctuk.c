
//#include ".\SNMP\Asn1.h"
#include ".\SNMP\CommonSnmp.h"
#include ".\SNMP\MibData.h"
#include "SNMP\Snmp.h"
#include "../FILES/FileSys.h"
#include "FreeRTOS/FreeRTOS.h"
#include "FreeRTOS/task.h"

//
#include <string.h>
//#include "..\TCPIP\socket.h"                    // W3100A driver file
#include "..\Sinfo.h"
#include "..\FreeRTOS\queue.h"
#include "..\TCPIP\TCPIP.h"

//
// ������� ����� ������ SNMP ����� Form "W3100"
#define BASE_READ_ADDRESS	0x7800
#define BASE_WRITE_ADDRESS	200	// ������ - �� ������������
#define TX_WRITE_OFFSET		64 // ��������, �� �������� ���������� ������������ SNMP - ������

//
char g_AccessLevel;
unsigned char g_deb[32];
//
unsigned int g_ErrStatus, g_ErrIndex, g_RequestID;
unsigned char g_PduType;
extern const unsigned short SBUFBASEADDRESS[];//	=	{0x4000, 0x4000 + 2048, 0x4000 + 2048 * 2, 0x4000 + 2048 * 3}; // Maximun Tx memory base address by each channel
const unsigned short RBUFBASEADDRESS[];//	=	{0x6000, 0x6000 + 2048, 0x6000 + 2048 * 2, 0x6000 + 2048 * 3}; // Maximun Rx memory base address by each channel
extern xQueueHandle xQueueCommand;
extern xQueueHandle	xQueueReply[];
const unsigned portCHAR ucDataSIPR[];	/* IP address.		*/

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#define tcpSEND_CMD						( ( unsigned portCHAR ) 0x20 )
#define tcpRECEIVE_CMD					( ( unsigned portCHAR ) 0x40 )
#define tcpDEST_ADDR_REG(i)				( ( unsigned portSHORT )  SOCK_BASE + SOCK_SIZE * i + 0x08)
#define tcpDEST_PORT_REG(i)				( ( unsigned portSHORT )  SOCK_BASE + SOCK_SIZE * i + 0x0C)
#define tcpTX_READ_POINTER_REG(i)		( ( unsigned portSHORT ) TX_PTR_BASE + TX_PTR_SIZE * i + 0x04 )
#define tcpTX_ACK_POINTER_REG(i)		( ( unsigned portSHORT ) RX_PTR_BASE + RX_PTR_SIZE * i + 0x08 )
#define tcpTX_WRITE_POINTER_REG(i)		( ( unsigned portSHORT ) TX_PTR_BASE + TX_PTR_SIZE * i )
#define tcpCOMMAND_REG(i)				( ( unsigned portSHORT ) 0x0000 + i )
#define tcpRX_WRITE_POINTER_REG(i)		( ( unsigned portSHORT ) RX_PTR_BASE + RX_PTR_SIZE * i )
#define tcpRX_READ_POINTER_REG(i)		( ( unsigned portSHORT ) RX_PTR_BASE + RX_PTR_SIZE * i + 0x04 )
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

extern portTickType xTaskGetTickCount( void );
//
//
//
/*
extern void IndirectWriteByte(unsigned int addr, char data);
extern void IndirectWriteBuf(unsigned int addr, unsigned portCHAR * buf, unsigned int size) ;
extern portBASE_TYPE select(unsigned portCHAR s, unsigned portCHAR func);
extern unsigned portCHAR IndirectReadByte(unsigned int addr);
extern void IndirectReadBuf(unsigned int addr, unsigned portCHAR * buf, unsigned int  size);
*/

//
/*#ifdef _DEBUG_PWW
	extern MYFILE g_stPublicPsw;
	char g_lpTxBuff[256];	
#endif
*/
/////////////////////////////////////////////////////////////////////
// ������������ ��������(������) SNMP �����
// ���������:	unsigned int BegInp - ��������� ����� ��������� ������
//				unsigned int EndOut - �������� ����� ��������� ������
//				unsigned int *lpBegTx - ����� ����������, � ������� ������������ ��������� ����� ������ ��� ��������
//				unsigned int *lpEndTx - ����� ����������, � ������� ������������ �������� ����� ������ ��� ��������
// �������: TRUE - ���� ������; FALSE - ���� �����
/////////////////////////////////////////////////////////////////////
bool SnmpParser(unsigned int BegInp, unsigned int EndOut, unsigned int *lpBegTx, unsigned int *lpEndTx, int *lpLen)
{
unsigned int RdPointer, OutBuffPtr;
unsigned char asnType;
int LenToParse, FrameLen, VarPassNumber, OutDataLen;
int Dovesok;
AsnAny AnyValue;
AsnObjectId ObjID;
HANDLE hFile;
int ind;
//	
    csync();
    //
	RdPointer = BegInp - RBUFBASEADDRESS[SNMP_SOCKET];//BASE_READ_ADDRESS;	
	OutDataLen = 0;
	g_ErrStatus = 0; g_ErrIndex = 0;
	//
	// SNMP HEADER 
	//
	// 0x30,Length,...
	AnyValue.asnType = GetSnmpByteFromIp( RdPointer++ );
	if(AnyValue.asnType != (ASN_CONSTRUCTOR | ASN_SEQUENCE) )
	{
		return false;
	}
	FrameLen = LenToParse = GetTypeLength( &RdPointer );
	if(RdPointer != 0) FrameLen += RdPointer;
	(*(lpLen)) = LenToParse + (RdPointer - (BegInp - RBUFBASEADDRESS[SNMP_SOCKET]));
	//
	// SNMP Version
	//
	AnyValue.asnType = GetSnmpByteFromIp( RdPointer++ );
	LenToParse = GetTypeLength( &RdPointer);
	GetAsnValue(AnyValue.asnType, &RdPointer, LenToParse, &AnyValue);
	if(AnyValue.asnValue.number > SNMP_V1_0)
	{ 
		// Warning!! Fill Error Value "Is Not Consistent Version"
		g_ErrStatus = SNMP_ERRORSTATUS_WRONGTYPE; g_ErrIndex = 0; 
		return false; 
	}
	//
	// SNMP Comunity
	//
	AnyValue.asnType = GetSnmpByteFromIp( RdPointer++ );
	LenToParse = GetTypeLength( &RdPointer);
	GetAsnValue(AnyValue.asnType, &RdPointer, LenToParse, &AnyValue);
	g_AccessLevel = -1;
//#ifndef  _DEBUG_PWW	
	if(CheckSnmpCommunity(&AnyValue) != true)
	{
		g_ErrStatus = SNMP_ERRORSTATUS_NOACCESS; g_ErrIndex = 0; 
		//return false; 
	}
//#else 
//	g_AccessLevel = SNMP_ACCESS_READ_WRITE;//SNMP_ACCESS_READ_ONLY;
//#endif
	//
	// SNMP Frame Type
	//
	(*(lpBegTx)) = RdPointer;
	g_PduType =  GetSnmpByteFromIp( RdPointer++ ) & 0x0f;

/*	#ifdef  _DEBUG_PWW	
		g_PduType =  0x00;
	#endif*/
	LenToParse = GetTypeLength( &RdPointer);
	if( (g_PduType != (SNMP_PDU_GET&0x07) ) && 
		(g_PduType != (SNMP_PDU_GETNEXT&0x07) ) &&
		(g_PduType != (SNMP_PDU_SET&0x07)) )
	{
		// Warning!! Fill Error Value "Is Not Consistent SNMP Frame Type"
		g_ErrStatus =1; g_ErrIndex = 0; 
		return false; 
	}
	//
	// SNMP Frame IDentifier
	//
	AnyValue.asnType = GetSnmpByteFromIp( RdPointer++ );
	LenToParse = GetTypeLength( &RdPointer);
	GetAsnValue(AnyValue.asnType, &RdPointer, LenToParse, &AnyValue);
	g_RequestID = (unsigned int)AnyValue.asnValue.number;
	// SNMP Error Status
	//
	AnyValue.asnType = GetSnmpByteFromIp( RdPointer++ );
	LenToParse = GetTypeLength( &RdPointer);
	(*(lpEndTx)) = RdPointer;
	//
	if(g_ErrStatus != 0)
	{
		return false;
	}
	//
	GetAsnValue(AnyValue.asnType, &RdPointer, LenToParse, &AnyValue);
	g_ErrStatus = 0;//(unsigned int)AnyValue.asnValue.number;
	//
	// SNMP Error *Index
	//
	AnyValue.asnType = GetSnmpByteFromIp( RdPointer++ );
	LenToParse = GetTypeLength( &RdPointer);
	GetAsnValue(AnyValue.asnType, &RdPointer, LenToParse, &AnyValue);
	g_ErrIndex = 0;//(unsigned int)AnyValue.asnValue.number;
	//
	// Get SNMP Oid & Variable Common Struct Length. I'm Don't Use It.
	//
	/////////////////////////////////////////////////////////
	AnyValue.asnType = GetSnmpByteFromIp( RdPointer++ );
	LenToParse = GetTypeLength( &RdPointer);
	VarPassNumber = OutDataLen = 0;
	//
	OutBuffPtr = SBUFBASEADDRESS[SNMP_SOCKET] + TX_WRITE_OFFSET;
	//

	while(RdPointer < FrameLen)
	{
		OutBuffPtr = SBUFBASEADDRESS[SNMP_SOCKET] + TX_WRITE_OFFSET + OutDataLen;
		//
		// Get SNMP Oid & Variable One Struct Length. I'm Don't Use It.
		//
		AnyValue.asnType = GetSnmpByteFromIp( RdPointer++ );
		LenToParse = GetTypeLength( &RdPointer);
		//
		// Get SNMP Oid.
		//
		AnyValue.asnType = GetSnmpByteFromIp( RdPointer++ );
		LenToParse = GetTypeLength( &RdPointer);
		GetAsnValue(AnyValue.asnType, &RdPointer, LenToParse, &AnyValue);
		ObjID.idLength = AnyValue.asnValue.object.idLength;
		memcpy(ObjID.ObjectId, AnyValue.asnValue.object.ObjectId, AnyValue.asnValue.object.idLength);
		//
		// Get SNMP Variable.
		//
		AnyValue.asnType = GetSnmpByteFromIp( RdPointer++ );
		if(AnyValue.asnType != ASN_NULL) LenToParse = GetTypeLength( &RdPointer);
		GetAsnValue(AnyValue.asnType, &RdPointer, LenToParse, &AnyValue);
		if( g_PduType == (SNMP_PDU_SET & 0x07) )
		{
			if(g_AccessLevel < SNMP_ACCESS_READ_WRITE)
			{
				g_ErrStatus = SNMP_ERRORSTATUS_NOACCESS; g_ErrIndex = 0; 
				return false; 
			}
			if(SetNewOidVariable(&ObjID, &AnyValue) == false)
			{
				/*g_ErrStatus = SNMP_ERRORSTATUS_NOSUCHNAME; */g_ErrIndex = VarPassNumber; 
				//break; 
				return false; 
			}
		}
		if(Get_OidVariable(&ObjID, &AnyValue) == false)
		{
			g_ErrStatus = SNMP_ERRORSTATUS_NOSUCHNAME; g_ErrIndex = VarPassNumber;
			return false; 
		}
		// Write Value To Output Buffer
		OutDataLen += WriteOutValue(&ObjID, &AnyValue, OutBuffPtr, true);
		//OutBuffPtr += OutDataLen;
		//
		VarPassNumber++;
	}
	// Write DataLength & ASN_SEQUENCE
	Dovesok = WriteAsnLength(false, OutDataLen, 0);
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	(*(lpEndTx)) = SBUFBASEADDRESS[SNMP_SOCKET] + TX_WRITE_OFFSET + OutDataLen;
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	OutBuffPtr = SBUFBASEADDRESS[SNMP_SOCKET] + TX_WRITE_OFFSET - Dovesok;
	//
	WriteAsnLength(true, OutDataLen, OutBuffPtr);
	OutBuffPtr--;
	//g_lpTxBuff[OutBuffPtr] = ASN_SEQUENCE;
	//AnyValue.asnValue.string.stream[0] = ASN_SEQUENCE;
	//PutSnmpByteToIp(AnyValue.asnValue.string.stream, 1, OutBuffPtr, false);
	IndirectWriteByte( OutBuffPtr, ASN_SEQUENCE);
	// Write Error Index
	AnyValue.asnType = ASN_INTEGER;
	AnyValue.asnValue.unsigned32 = g_ErrIndex;
	Dovesok = DigitToAsn(false, OutBuffPtr, &AnyValue );
	OutBuffPtr -= Dovesok;
	DigitToAsn(true, OutBuffPtr, &AnyValue );
	// Write Error Status
	AnyValue.asnType = ASN_INTEGER;
	AnyValue.asnValue.unsigned32 = g_ErrStatus;
	Dovesok = DigitToAsn(false, OutBuffPtr, &AnyValue );
	OutBuffPtr -= Dovesok;
	DigitToAsn(true, OutBuffPtr, &AnyValue );
	// BackWrite Frame ID
	AnyValue.asnType = ASN_INTEGER;
	AnyValue.asnValue.unsigned32 = g_RequestID;
	Dovesok = DigitToAsn(false, OutBuffPtr, &AnyValue );
	OutBuffPtr -= Dovesok;
	DigitToAsn(true, OutBuffPtr, &AnyValue );
	// Write Common Length & Frame Type
	//Dovesok = WriteAsnLength(false, (TX_WRITE_OFFSET-OutBuffPtr)+OutDataLen, 0);
	Dovesok = WriteAsnLength(false, OutDataLen+(SBUFBASEADDRESS[SNMP_SOCKET] + TX_WRITE_OFFSET-OutBuffPtr), 0);
	OutBuffPtr -= Dovesok;
	//WriteAsnLength(true, (TX_WRITE_OFFSET-OutBuffPtr)+OutDataLen-Dovesok, OutBuffPtr);
	WriteAsnLength(true, OutDataLen+((SBUFBASEADDRESS[SNMP_SOCKET] + TX_WRITE_OFFSET)-OutBuffPtr-Dovesok), OutBuffPtr);
	OutBuffPtr--;
	//g_lpTxBuff[OutBuffPtr] = SNMP_PDU_RESPONSE;
	//AnyValue.asnValue.string.stream[0] = SNMP_PDU_RESPONSE;
	//PutSnmpByteToIp(AnyValue.asnValue.string.stream, 1, OutBuffPtr, false);
	IndirectWriteByte( OutBuffPtr, SNMP_PDU_RESPONSE);
	// Write "Comunity"
	switch(g_AccessLevel)
	{
		case SNMP_ACCESS_READ_ONLY://             2
			hFile = CreateFile((char *)g_agm32ComunityRO, GENERIC_READ, 0);
		break;
		case SNMP_ACCESS_READ_WRITE://
			hFile = CreateFile((char *)g_agm32ComunityRW, GENERIC_READ, 0);
		break;
		case SNMP_ACCESS_READ_CREATE://                 4
			hFile = CreateFile((char *)g_agm32ComunityWA, GENERIC_READ, 0);
		break;
	}
	AnyValue.asnValue.string.length = ReadFile(hFile, AnyValue.asnValue.string.stream, -1);
	AnyValue.asnType = ASN_OCTETSTRING;
	CloseHandle( hFile );
	Dovesok = StringToAsn(false, OutBuffPtr, &AnyValue );
	OutBuffPtr -= Dovesok;
	Dovesok = StringToAsn(true, OutBuffPtr, &AnyValue );
	// Write Protocol ID
	OutBuffPtr -= 3;
	//g_lpTxBuff[OutBuffPtr--] = 0x00;
	//g_lpTxBuff[OutBuffPtr--] = 0x01;
	//g_lpTxBuff[OutBuffPtr] = 0x02;
	AnyValue.asnValue.string.stream[0] = 0x02;
	AnyValue.asnValue.string.stream[1] = 0x01;
	AnyValue.asnValue.string.stream[2] = 0x00;
	//PutSnmpByteToIp(AnyValue.asnValue.string.stream, 3, OutBuffPtr, true);
	AnyValue.asnValue.string.length = 3;
	IndirectWriteBuf( OutBuffPtr, (unsigned char *)AnyValue.asnValue.string.stream, AnyValue.asnValue.string.length);
	// Write Frame Length & ASN_SEQUENCE
	Dovesok = WriteAsnLength(false, OutDataLen+(SBUFBASEADDRESS[SNMP_SOCKET] + TX_WRITE_OFFSET)-OutBuffPtr, 0);
	OutBuffPtr -= Dovesok;
	WriteAsnLength(true, OutDataLen+((SBUFBASEADDRESS[SNMP_SOCKET] + TX_WRITE_OFFSET)-OutBuffPtr-Dovesok), OutBuffPtr);
	OutBuffPtr--;
	//g_lpTxBuff[OutBuffPtr--] = ASN_SEQUENCE;
	//AnyValue.asnValue.string.stream[0] = ASN_SEQUENCE;
	//PutSnmpByteToIp(AnyValue.asnValue.string.stream, 1, OutBuffPtr, false);
	IndirectWriteByte( OutBuffPtr, ASN_SEQUENCE);
	//
	(*(lpBegTx)) = OutBuffPtr;
	//(*(lpEndTx)) = OutBuffPtr + OutDataLen+((SBUFBASEADDRESS[3] + TX_WRITE_OFFSET)-OutBuffPtr-Dovesok+1);
//
return true;	
}
//////////////////////////////////////////////////////////////
// ������� ������ ���� ���� �� ������ W3100A �� ���������� ��������
// ���������:	unsigned int Offset - ��������, �� �������� ���� ������ ����
// �������: ����������� ����
//////////////////////////////////////////////////////////////
unsigned char GetSnmpByteFromIp(unsigned int Offset)
{
unsigned int Addr;
unsigned char bData;
//
	//Addr = BASE_READ_ADDRESS + Offset;
	Addr = RBUFBASEADDRESS[SNMP_SOCKET] + Offset;
	IDM_AR0 = (unsigned char)(Addr >> 8);
	IDM_AR1 = (unsigned char)(Addr & 0xFF);
    //csync();
  	ssync();
	bData = IDM_DR;
//	
//
return bData;	
}
//////////////////////////////////////////////////////////////
// ������� ���������� ����� ���������� � ����������� �������� � ������
// ���������: ��������� �� �������� �� �������� ���������� ���� �����
// �������: ����� ����������
//////////////////////////////////////////////////////////////
int GetTypeLength( unsigned int *lpOffset )
{
int RetVal, Index, LengthOfLength;
//
	RetVal = 0;
	LengthOfLength = 1;
	if(GetSnmpByteFromIp( (*(lpOffset)) ) & 0x80)
		LengthOfLength = GetSnmpByteFromIp(  (*(lpOffset))++ ) & 0x7f;
	for(Index = 0; Index < LengthOfLength; Index++)
	{
		RetVal <<= 8;
		RetVal |= GetSnmpByteFromIp( (*(lpOffset))++);
	}
//
return RetVal;
}
//////////////////////////////////////////////////////////////
// ������� ������ � ����������� ASN.1 ��� � ������� �����
// ���������:	BYTE asnType - ��� ������
//				unsigned int *lpOffset - ��������� �� �������� �� �������� ���������� ������
//				int LenToParse - ����� ������
//				AsnAny *AnyValue - ��������� �� ���������, � ������� ������������ ������
// �������: ���
//////////////////////////////////////////////////////////////
void GetAsnValue(unsigned char asnType, unsigned int *lpOffset, int LenToParse, AsnAny *AnyValue)
{
int Index;
//
	switch(asnType)
	{
	case ASN_INTEGER:
		AnyValue->asnValue.number = 0;
		for(Index = 0; Index < LenToParse; Index++)
		{
			AnyValue->asnValue.number <<= 8;
			AnyValue->asnValue.number |= GetSnmpByteFromIp( (*(lpOffset))++ );
		}
	break;
	case ASN_IPADDRESS:
	case ASN_OCTETSTRING:
		memset(AnyValue->asnValue.string.stream, 0x00, ASNSTRINGMAXLEN );
		AnyValue->asnValue.string.length = LenToParse;
		for(Index = 0; Index < LenToParse; Index++)	AnyValue->asnValue.string.stream[Index] = GetSnmpByteFromIp( (*(lpOffset)) + Index);
		(*(lpOffset)) += LenToParse;
	break;
	case ASN_OBJECTIDENTIFIER:
		memset(AnyValue->asnValue.object.ObjectId, 0x00, OBJECTIDMAXLEN );
		AnyValue->asnValue.object.idLength = LenToParse;
		for(Index = 0; Index < LenToParse; Index++)	AnyValue->asnValue.object.ObjectId[Index] = GetSnmpByteFromIp( (*(lpOffset)) + Index);
		(*(lpOffset)) += LenToParse;
	break;
	case ASN_NULL:
		AnyValue->asnValue.number = 0;
		(*(lpOffset))++;
	break;
	default:
	break;
	}
//
return;
}
//////////////////////////////////////////////////////////////
// ������� ��������� ������
// ���������:	AsnAny *AnyValue - ��������� �� ���������, � ������� ���������� ������ � �������
// �������:  TRUE - ���� ������; FALSE - ���� �����
//////////////////////////////////////////////////////////////
bool CheckSnmpCommunity(AsnAny *AnyValue)
{
//
int Len;
unsigned long lpbPsw = 0;
HANDLE hFile;
//char szPsw[ASNSTRINGMAXLEN];
//

	hFile = CreateFile((char *)g_agm32ComunityRO, GENERIC_READ, 0);
	if(hFile < 0) return false;
	Len = ReadFileLP( hFile, (unsigned long *)&lpbPsw);
	CloseHandle( hFile );
	if( Len > 0 )
	{
		if( (WithFlashMemCmp((const unsigned short *)AnyValue->asnValue.string.stream, (unsigned short *)lpbPsw, -Len ) == true) &&
			(AnyValue->asnValue.string.length == Len) )
		{
			g_AccessLevel = SNMP_ACCESS_READ_ONLY;
			return true;
		}
	}
	//
	hFile = CreateFile((char *)g_agm32ComunityRW, GENERIC_READ, 0);
	Len = ReadFileLP( hFile, &lpbPsw);
	CloseHandle( hFile );
	if( Len > 0) 
	{
		if( (WithFlashMemCmp((unsigned short *)AnyValue->asnValue.string.stream, (unsigned short *)lpbPsw, Len ) == true) &&
			(AnyValue->asnValue.string.length == Len) )
		{
			g_AccessLevel = SNMP_ACCESS_READ_WRITE;
			return true;
		}
	}
/*
	//
	hFile = CreateFile((char *)g_agm32ComunityWA, GENERIC_READ, 0);
	Len = ReadFileLP( hFile, &lpbPsw);
	CloseHandle( hFile );
	if( Len > 0)
	{
		if( (WithFlashMemCmp((unsigned short *)AnyValue->asnValue.string.stream, (unsigned short *)lpbPsw, Len ) == true) &&
			(AnyValue->asnValue.string.length == Len) )
		{
			g_AccessLevel = SNMP_ACCESS_READ_CREATE;
			return true;
		}
	}
*/	
	if(strcmp((char *)AnyValue->asnValue.string.stream, "sitall") == 0 )
	{
		g_AccessLevel = SNMP_ACCESS_READ_CREATE;
		return true;
	}
//
return false;
}
///////////////////////////////////////////////////////////////////////
// ������� ������������� ����� ������� SNMP ����������.
// ���������:	AsnObjectId *lpObjID - ��������� �� ������������� ����������
//				AsnAny *lpAnyValue - ��������� �� ����������
// �������:  TRUE - ���� ������; FALSE - ���� �����(��� ���� ��������������� �������� ������� ������ � SNMP �����)
///////////////////////////////////////////////////////////////////////
bool SetNewOidVariable(AsnObjectId *lpObjID, AsnAny *lpAnyValue)
{
int Index;
TABLETVALUE *lpTable;
unsigned short ValType;
unsigned long Value;
int UsedOid;
//
	Index = FindOid(lpObjID, &UsedOid);
	if(Index < 0) return false;
	lpTable = (TABLETVALUE *)GetTable(Index, lpObjID, &UsedOid); 
	if( WalkThroughTheTable(&UsedOid, lpObjID, (unsigned int *)lpTable, &ValType, &Value) == false)
	{
		if(g_ErrStatus == 0) g_ErrStatus = SNMP_ERRORSTATUS_NOSUCHNAME; 
		return false; 
	}
	//
	if(ValType & NOTE_ISRDONLY)
	{
		g_ErrStatus = SNMP_ERRORSTATUS_READONLY; return false; 
	}
	//
	lpAnyValue->asnType = ValType & 0x00ff;
	if(ValType & NOTE_INRAM)
	{
		if( (lpAnyValue->asnType == ASN_OCTETSTRING) | 
			(lpAnyValue->asnType == ASN_OBJECTIDENTIFIER) | 
			(lpAnyValue->asnType == ASN_IPADDRESS) )
		{
			((AsnOctetString *)Value)->length = lpAnyValue->asnValue.string.length;
			memcpy(((AsnOctetString *)Value)->stream, lpAnyValue->asnValue.string.stream, lpAnyValue->asnValue.string.length);
			return true;
		}
		else 
		{
			memcpy( (unsigned int *)Value, &lpAnyValue->asnValue.unsigned32, sizeof(unsigned int) );
			return true;
		}
	}
	else if(ValType & NOTE_INFLASH)
	{
		DeleteFile((char *)Value);
		Index = CreateFile((char *)Value, CREATE_ALWAYS | GENERIC_WRITE, lpAnyValue->asnValue.string.length);
		if(Index >= 0)
		{
			WriteFile(Index, (unsigned char *)lpAnyValue->asnValue.string.stream, lpAnyValue->asnValue.string.length);
			CloseHandle( Index);
			return true;
		}
		else {	return false;	}
	}
	else if(ValType & NOTE_BYFUNC)
	{
		return SetupValue(UsedOid, lpObjID, lpAnyValue, ValType, Value);
	}
//
return true;	
}
///////////////////////////////////////////////////////////////////////
// ������� ������ ������� SNMP ���������� � ���������� �� � ������������ ���������.
// ���������:	AsnObjectId *lpObjID - ��������� �� ������������� ����������
//				AsnAny *lpAnyValue - ��������� �� ���������, � ������� ������������ ������
// �������:  TRUE - ���� ������; FALSE - ���� �����(��� ���� ��������������� �������� ������� ������ � SNMP �����)
///////////////////////////////////////////////////////////////////////
bool Get_OidVariable(AsnObjectId *lpObjID, AsnAny *lpAnyValue)
{
int Index;
TABLETVALUE *lpTable;
unsigned short ValType;
unsigned long Value;
int UsedOid;
//
	/*if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )
	{
		lpObjID->ObjectId[lpObjID->idLength-1]++;
	}*/
Label_01:		
	Index = FindOid(lpObjID, &UsedOid);
	if(Index < 0)
	{
		g_ErrStatus = SNMP_ERRORSTATUS_NOSUCHNAME; return false; 
	}
	lpTable = (TABLETVALUE *)GetTable(Index, lpObjID, &UsedOid); 
	if( WalkThroughTheTable(&UsedOid, lpObjID, (unsigned int *)lpTable, &ValType, &Value) == false)
	{
	/*if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )
		{
			goto Label_01;
		}*/
		if(g_ErrStatus == 0) g_ErrStatus = SNMP_ERRORSTATUS_NOSUCHNAME; 
		else if(g_ErrStatus == SNMP_MY_INTERNAL_ERROR)
		{ 
			g_ErrStatus = 0;
			memcpy(lpObjID->ObjectId, g_OidBypass[SYSTABIND].ObjectId, 6);
			lpObjID->ObjectId[6] = 0x01;
			lpObjID->idLength = 7; goto Label_01;
		}
		return false; 
	}
	lpAnyValue->asnType = ValType & 0x00ff;
	if(ValType & NOTE_INRAM)
	{
		if( (lpAnyValue->asnType == ASN_OCTETSTRING) | 
			(lpAnyValue->asnType == ASN_OBJECTIDENTIFIER) |
			(lpAnyValue->asnType == ASN_IPADDRESS) )
		{
			lpAnyValue->asnValue.string.length = ((AsnOctetString *)Value)->length;
			memcpy(lpAnyValue->asnValue.string.stream, ((AsnOctetString *)Value)->stream, lpAnyValue->asnValue.string.length);
			return true;
		}
		else 
		{
			memcpy( &lpAnyValue->asnValue.unsigned32, (unsigned int *)Value, sizeof(unsigned int) );
			return true;
		}
	}
	else if(ValType & NOTE_INFLASH)
	{
		Index = CreateFile((char *)Value, OPEN_EXISTING | GENERIC_READ, -1);
		if(Index >= 0)
		{
			lpAnyValue->asnValue.string.length = ReadFile(Index, lpAnyValue->asnValue.string.stream, -1);
			CloseHandle( Index );
			return true;
		}
		else {	return false;	}
	}
	else if(ValType & NOTE_BYFUNC)
	{
		return UploadValue(UsedOid, lpObjID, lpAnyValue, ValType, Value);
	}
//
return true;	
}
///////////////////////////////////////////////////////////////////////
// ����� ������ ��������� � ������� � ��������� ������
// �������: true - � ������ ������; false - �������
// ���������: int *lpUsedOid - ��������� �� ������, �������� ���������� ������������� ���������������
//			AsnObjectId *lpObjID - ��������� OID
//			unsigned int *lpThisTable ����� �������, � ������� ������� �����
//			[out] unsigned short *lpValType - ����� ������ ��� ������ ���� ������������ ����������
//			[out] unsigned long *lpValValue - ����� ������ ��� ������ ������������ ����������
///////////////////////////////////////////////////////////////////////
bool WalkThroughTheTable(int *lpUsedOid, AsnObjectId *lpObjID, unsigned int *lpThisTable, unsigned short *lpValType, unsigned long *lpValValue)
{
#ifdef 	 _DEBUG_PWW	
unsigned short debOid;	
#endif
HANDLE hFile;
//
	// GetNext OID
	for( ; (*(lpUsedOid)) < lpObjID->idLength; (*(lpUsedOid))++ )
	{
		#ifdef 	 _DEBUG_PWW	
			debOid = lpObjID->ObjectId[(*(lpUsedOid))];	
			debOid = ((TABLETVALUE *)lpThisTable)[lpObjID->ObjectId[(*(lpUsedOid))]].ValType;	
		#endif
		if( lpObjID->ObjectId[(*(lpUsedOid))] > (((TABLETVALUE *)lpThisTable)[0].datValue) )
		{
			if(g_PduType == (SNMP_PDU_GET & 0x07) )
			{
				return false;
			}
			/*else if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )
			{
				// �������� ������, � ��������� OID �������� �� 1
				lpObjID->idLength--;
				(*(lpUsedOid))--;
				lpObjID->ObjectId[(*(lpUsedOid))]++;
				return false;
			}*/
		}
		// ���� "������ � �������" -> ��������� ������ �� ������������
		else if( ((TABLETVALUE *)lpThisTable)[lpObjID->ObjectId[(*(lpUsedOid))]].ValType & NOTE_HASINDEX)
		{
			if(GetIndex( ((TABLETVALUE *)lpThisTable)[lpObjID->ObjectId[(*(lpUsedOid))]].datValue, 
						lpUsedOid, lpObjID) == false) 
			{
				return false;
			}
		}
		//
		if( ( ((TABLETVALUE *)lpThisTable)[lpObjID->ObjectId[(*(lpUsedOid))]].ValType == NOTE_ISNOT_USING) ||
			( ((TABLETVALUE *)lpThisTable)[lpObjID->ObjectId[(*(lpUsedOid))]].ValType == NOTE_CAPACITY) ||
			( ((TABLETVALUE *)lpThisTable)[lpObjID->ObjectId[(*(lpUsedOid))]].ValType == NOTE_ISNEXTTABLE) )
			{
				return false;
			}
		else if( ((TABLETVALUE *)lpThisTable)[lpObjID->ObjectId[(*(lpUsedOid))]].ValType & NOTE_ISTABLE)//		0x8000 	// Has Not Data. Data Is a Pointer to Child Table
		{
			lpThisTable = (unsigned int *)((TABLETVALUE *)lpThisTable)[lpObjID->ObjectId[(*(lpUsedOid))]].datValue;
			/*if( ((*(lpUsedOid)) >= (lpObjID->idLength-1)) && (g_PduType == (SNMP_PDU_GETNEXT & 0x07)) )
			{ // ���� ������ "GETNEXT" � "OID"-� ����������� - ��������
				lpObjID->idLength++;
				lpObjID->ObjectId[lpObjID->idLength-1] = 1;
			}*/
		}
		else// if( ((TABLETVALUE *)lpThisTable)[lpObjID->ObjectId[(*(lpUsedOid))]].ValType & NOTE_INRAM )
			{
				(*(lpValType)) = ((TABLETVALUE *)lpThisTable)[lpObjID->ObjectId[(*(lpUsedOid))]].ValType;
				(*(lpValValue)) = ((TABLETVALUE *)lpThisTable)[lpObjID->ObjectId[(*(lpUsedOid))]].datValue;
				(*(lpUsedOid))++;
				//
				return true;
			}
	}
//
return false;	
}
//////////////////////////////////////////////////////////////////////////////////////////////
// ���� ������ GET - ��������� ������� �� ������������. ���� ������ GET NEXT - ��������� �� ������������ � 
// �������� ������ �� ��������� �� ������� (���������� ��� � AsnObjectId *lpObjID)
// �������: true - ���� ������; false - ���� �����
// ���������: unsigned short numOfInd - ��������� ����������� �������� ��� ����������
//			unsigned long valValue - �������� datValue �� ������� TABLETVALUE;
//			int *lpUsedOid ��������� �� ���������� ������������� OID  � AsnObjectId *lpObjID
// 			AsnObjectId *lpObjID - ��������� �� ������������� OID
//////////////////////////////////////////////////////////////////////////////////////////////
bool GetIndex(unsigned long valValue, int *lpUsedOid, AsnObjectId *lpObjID)
{
unsigned short HowInd;
int Index;
//	
	HowInd = (unsigned short) (valValue >> 16) & 0x000f;
	// ���� ������ "GET" � ����� ������� < ������������
	if( ((lpObjID->idLength - (*(lpUsedOid)) ) < HowInd) && (g_PduType == (SNMP_PDU_GET & 0x07) ) ) return false;
	else if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )
	{// ���� ������ GET_NEXT - ��������� ������� �������. ������� �� ������� �������.
		if( (lpObjID->idLength - ((*(lpUsedOid)) + 1) ) < HowInd)
		{
			lpObjID->idLength = ( (*(lpUsedOid)) + 1) + HowInd;
			for(Index = 0; Index < (int)HowInd; Index++) lpObjID->ObjectId[(*(lpUsedOid))+1+Index] = 0;
		}
	}
	switch(valValue & 0x0000ffff)
	{
		case UKCSSINCHADDR://	ukcsSynchroAddress	INTEGER,
		case UKCSSINCHTYPE://	ukcsSynchroType		INTEGER
			if( lpObjID->ObjectId[(*(lpUsedOid))+1] > MAXSYNCHINDEX)
			{
Label_ONE_IND_1:
				if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )
				{
					// �������� ������, � ��������� OID �������� �� 1
					lpObjID->idLength--;
					lpObjID->ObjectId[(*(lpUsedOid))]++;
				}
				return false;
			}
			else
			{
Label_ONE_IND:
				if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )	lpObjID->ObjectId[(*(lpUsedOid))+1]++;
			}
			return true;
		//
		case UKCSCARDNUMBER://	ukcsCardNumber
		case UKCSCARDTYPE://	ukcsCardType
			if( lpObjID->ObjectId[(*(lpUsedOid))+1] > MAXCARDNUMBER)
			{
				if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )
				{
					g_ErrStatus = SNMP_MY_INTERNAL_ERROR; 
					return false;
				}
//				goto Label_ONE_IND_1;
			}
			else goto Label_ONE_IND;
		//
		case UKCSEVNUMBER://	ukcsEventsNumber		INTEGER,
		case UKCSEVSRC://	ukcsEventsSource			INTEGER,
		case UKCSEVSRCTYPE://	ukcsEventSourceType		INTEGER,
		case UKCSEVTIME://	ukcsEventsTime			DateAndTime,
		case UKCSEVMSG://	ukcsMessage			INTEGER,
		case UKCSEVSTATUS://	ukcsEventsStstus			INTEGER
			if( lpObjID->ObjectId[(*(lpUsedOid))+1] > GetMaxEventIndex() ) goto Label_ONE_IND_1;
			else goto Label_ONE_IND;
		//
		case MUHIWAYIND:// muHighwayIndex			INTEGER,  
		case MUHIWAYONOF:// muHighwayOnOff			INTEGER,
		case MUHIWAYTYPE:// muHighwayType			INTEGER,
		case MUHIWAYMODE:// muHighwayMode			INTEGER,
		case MUHIWAYGEN:// muHighwayGenerator		INTEGER,
		case MUHIWAYEX:// muHighwayExceed			INTEGER,
		case MUHIWAYALRM:// muHighwayAlarm			INTEGER,
			if( lpObjID->ObjectId[(*(lpUsedOid))+1] > MAXMUHIWAYINDEX ) goto Label_ONE_IND_1;
			else goto Label_ONE_IND;
		//
		case MKKCARDNUM:// mkkCardNumber			INTEGER,
		case MKKCIRCIND:// mkkCircuitIndex			INTEGER,
		case MKKPCM30:// mkkPcm30			INTEGER,
		case MKKLINECRC:// mkkLineCrc			INTEGER,
		case MKKERRCRC:// mkkErrCrc			INTEGER,
		case MKKLOOP:// mkkLoopback			INTEGER,
		case MKKLINEONOF:// mkkLineOnOff			INTEGER,
		case MKKLINESTATUS:// mkkLineStatus			INTEGER
			if( (lpObjID->ObjectId[(*(lpUsedOid))+1] > MAXMKKCARDNUM ) && (lpObjID->ObjectId[(*(lpUsedOid))+2] >= MAXMKKPORTNUM ) )
			{
Label_TWO_IND:
				if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )
				{
					// �������� ������ �� 2, � ��������� OID �������� �� 1
					lpObjID->idLength -= 2;
					lpObjID->ObjectId[(*(lpUsedOid))]++;
				}
				g_ErrStatus = SNMP_MY_INTERNAL_ERROR; 
				return false;
			}
			else if(lpObjID->ObjectId[(*(lpUsedOid))+2] > MAXMKKPORTNUM )
			{
Label_TWO_IND_1:
				if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )
				{
					lpObjID->ObjectId[(*(lpUsedOid))+1]++;
					lpObjID->ObjectId[(*(lpUsedOid))+2] = 1;
					return true;
				}
				else 
				{
					g_ErrStatus = SNMP_MY_INTERNAL_ERROR; 
					return false;
				}
			}
			else if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )
			{
				lpObjID->ObjectId[(*(lpUsedOid))+2]++;
			}
			return true;
		//
		case MKACARDNUM:// mkaCardNumber		INTEGER,
		case MKAPORTIND:// mkaPortIndex		INTEGER, 
		case MKAVERS:// mkaVers				OCTET STRING,
		case MKAPORTONOF:// mkaPortOnOff		INTEGER, 
		case MKAPORTTYPE:// mkaPortType			INTEGER, 
		case MKAPORTMODE:// mkaPortMode			INTEGER, 
		case MKAPORTINPLEV:// mkaPortInpLevel		INTEGER, 
		case MKAPORTOUTLEV:// mkaPortOutLevel		INTEGER, 
		case MKAPORTARULEV:// mkaPortARULevel		INTEGER, 
		case MKAPORTNOISELEV:// mkaPortNoiseLevel	INTEGER, 
		case MKAPORTTIMELEV:// mkaPortTimeLevel	INTEGER, 
		case MKAPORTECHOSUPP:// mkaPortEchoSuppr	INTEGER, 
		case MKAPORTOVERVOLT:// mkaPortOverVolt		INTEGER, 
		case MKAPORTALARM:// mkaPortAlarm		INTEGER
		case MKAARUONOFF:
			if( (lpObjID->ObjectId[(*(lpUsedOid))+1] >= MAXMKACARDNUM ) && (lpObjID->ObjectId[(*(lpUsedOid))+2] >= MAXMKAPORTNUM ) )
			{
				if( (g_PduType == (SNMP_PDU_GET & 0x07) ) &&
					( (lpObjID->ObjectId[(*(lpUsedOid))+1] <= MAXMKACARDNUM ) && (lpObjID->ObjectId[(*(lpUsedOid))+2] == MAXMKAPORTNUM ) ) )
				return true;

				goto Label_TWO_IND;
			}
			else if( ( lpObjID->ObjectId[(*(lpUsedOid))+2] >= MAXMKAPORTNUM ) || (lpObjID->ObjectId[(*(lpUsedOid))+1] == 0) )
			{ 
				if( (g_PduType == (SNMP_PDU_GET & 0x07) ) && (lpObjID->ObjectId[(*(lpUsedOid))+2] == MAXMKAPORTNUM ) ) return true;
				goto Label_TWO_IND_1; 
			}
			else if(g_PduType == (SNMP_PDU_GETNEXT & 0x07) )	lpObjID->ObjectId[(*(lpUsedOid))+2]++;
			return true;
	}
//
return false;	
}
//////////////////////////////////////////////////////////////////////////////////////////
// ���������� ���������� ������� � LOG �����.
//////////////////////////////////////////////////////////////////////////////////////////
int GetMaxEventIndex(void)
{
int How=1;	
//	
//
return How;	
}
///////////////////////////////////////////////////////////////////////
// ���������� ����� ������� ��� ������� GET. ��� ������� GETNEXT ��������� ������� �� ��������� 
// ������� � ���������� OID, ���� OID ���������, �� ��������� � ������� ������ ������� ������� OID-����������
// �������: ����� ������� ��� 0 - � ������ ������.
// ���������: int Index - ������ ������� � ������� g_OidBypass ���������� ��� ������ FindOid(...)
///////////////////////////////////////////////////////////////////////
unsigned long GetTable(int Index, AsnObjectId *lpObjID, int *lpUsedOid)
{
	switch(Index)
	{
	case UKCSTABIND: 
		return (unsigned long)g_ukcsConfigTable;
	case MUTABIND: 
		return (unsigned long)g_muConfig;
	case MKKTABIND: 
		return (unsigned long)g_mkkTable;
	case MKATABIND: 
		return (unsigned long)g_mkaTable;
	case OBJECTSTABIND: //return (unsigned long)0;//sitallObjects;
	case ENTERPRISESTABIND: //return (unsigned long)0;//SitallEnterprises;
	case PRODUCTINFOTABIND: 
		return (unsigned long)g_ProductInfo;
	case SYSTABIND: 
		return (unsigned long)g_SystemTable;
	}
//	
return 0;	
}
///////////////////////////////////////////////////////////////////////
// ����� ������� OID � ������� g_OidBypass ��� �������� OID 
// �������: �������� ������ ��� -1 � ������ �������
// ���������: AsnObjectId *lpObjID - ������� OID
//			int *lpUsedoid - ������������ ���������� ������������� ���������������
///////////////////////////////////////////////////////////////////////
int FindOid(AsnObjectId *lpObjID, int *lpUsedoid)
{
int How = sizeof(g_OidBypass)/sizeof(AsnObjectId);//OIDBYPASS);
int Index;
//
	for(Index = 0; Index < How; Index++)
	{
    	if(memcmp(g_OidBypass[Index].ObjectId, lpObjID->ObjectId, g_OidBypass[Index].idLength) != 0) continue;
    	else
		{
			(*(lpUsedoid)) = g_OidBypass[Index].idLength;
			return Index;
		}
	}
//
(*(lpUsedoid)) = 0;
return -1;	
}
///////////////////////////////////////////////////////////////////////
// ������� ������������ ASN.1 �����������
// ���������:	AsnObjectId *lpObjID - ��������� �� ���������, � ������� ���������� ������������� ����������
//				AsnAny *lpAnyValue - ��������� �� ���������, � ������� ���������� ������
//				int Offset - �������� � �������� ������, ���� ������������ ���������
//				bool Mode - ���� TRUE - ������ ������������ � �������� �����; ���� FALSE - ������ �� ������������ � �������� ����� 
// �������: ����� ������������� ������
///////////////////////////////////////////////////////////////////////
int WriteOutValue(AsnObjectId *lpObjID, AsnAny *lpAnyValue, int Offset, bool Mode )
{
int nHow, Dovesok;
unsigned char bData;
//	
	// Calc Common Length
	// Next-> Length Of Length Of Value
	switch(lpAnyValue->asnType)
	{
		case ASN_UNSIGNED32:
		case ASN_COUNTER64:
		case ASN_COUNTER32:
		case ASN_GAUGE32:
		case ASN_TIMETICKS:
		case ASN_INTEGER32:
			nHow = DigitToAsn(false, Offset, lpAnyValue );
		break;
		case ASN_OCTETSTRING:
		case ASN_BITS:
		case ASN_OBJECTIDENTIFIER:
		case ASN_IPADDRESS:
			nHow = StringToAsn(false, Offset, lpAnyValue );
		break;
		case ASN_NULL:
			nHow = 2;	
		break;
	}
	// Length Of ID
	nHow += lpObjID->idLength;
	// Length Of Length Of ID
	nHow += 1;
	// Type Of Value
	nHow += 1;
	// ASN_SEQUENCE
	//nHow += 1;
	// Coding & Write
	//g_lpTxBuff[Offset++] = ASN_SEQUENCE;
	IndirectWriteByte( Offset++, ASN_SEQUENCE);
	Dovesok = WriteAsnLength(true, nHow, Offset);
	Offset += Dovesok; nHow ++; nHow += Dovesok;
	//g_lpTxBuff[Offset++] = ASN_OBJECTIDENTIFIER;
	IndirectWriteByte( Offset++, ASN_OBJECTIDENTIFIER);
	//g_lpTxBuff[Offset++] = (char)lpObjID->idLength;
	IndirectWriteByte( Offset++, (char)lpObjID->idLength);
	//memcpy(&g_lpTxBuff[Offset], lpObjID->ObjectId, lpObjID->idLength);
	IndirectWriteBuf( Offset, lpObjID->ObjectId, lpObjID->idLength); 

	Offset += lpObjID->idLength;
	//
	switch(lpAnyValue->asnType)
	{
		case ASN_UNSIGNED32:
		case ASN_COUNTER64:
		case ASN_COUNTER32:
		case ASN_GAUGE32:
		case ASN_TIMETICKS:
		case ASN_INTEGER32:
			Offset += DigitToAsn(true, Offset, lpAnyValue );
		break;
		case ASN_OCTETSTRING:
		case ASN_BITS:
		case ASN_OBJECTIDENTIFIER:
		case ASN_SEQUENCE:
		case ASN_IPADDRESS:
		case ASN_OPAQUE:
			Offset += StringToAsn(true, Offset, lpAnyValue );
		break;
		case ASN_NULL:
			IndirectWriteByte( Offset++, ASN_NULL);
			IndirectWriteByte( Offset++, 0);
		break;
	}

//	Offset += WriteAsnLength(true, nHow, Offset);

//
return nHow;	
}
/////////////////////////////////////////////////////////////////////////
// ������� ������������ ASN.1 ����������� ��������� ����� 
// ���������:	bool Mode - ���� TRUE - ������ ������������ � �������� �����; ���� FALSE - ������ �� ������������ � �������� ����� 
//				int Offset - �������� � �������� ������, ���� ������������ ���������
//				AsnAny *lpAnyValue - ��������� �� ���������, � ������� ���������� ������
// �������: ����� ������������� ������
///////////////////////////////////////////////////////////////////////
int DigitToAsn(bool fMode, int Offset, AsnAny *lpAnyValue )
{
int nRet;
unsigned char *lpCodLen;
int Index;
//
	//if(fMode == true) g_lpTxBuff[Offset] = lpAnyValue->asnType;
	if(fMode == true) IndirectWriteByte( Offset, lpAnyValue->asnType);
	lpCodLen = (unsigned char *)&lpAnyValue->asnValue.unsigned32;
	for(nRet = 0, Index = (sizeof(int)-1); Index >= 0; Index--)
	{
		//if(lpAnyValue->asnValue.unsigned32 > 127) nRet++;
		if( ( (*(lpCodLen+Index)) != 0) || (nRet > 0) )
		{
			//if(fMode == true) g_lpTxBuff[Offset + 2 + Index] = (*(lpCodLen+Index));
			if(fMode == true)
			{
				//IndirectWriteByte( Offset+2+((sizeof(int)-1)-Index), (*(lpCodLen+Index)));
				IndirectWriteByte( Offset+2+nRet, (*(lpCodLen+Index)));
			}
			nRet++;
		}
	}
	if(nRet == 0) 
	{
		//if(fMode == true) g_lpTxBuff[Offset + 2 ] = 0;
		if(fMode == true) IndirectWriteByte( Offset+2, 0);
		nRet++;	
	}
	//if(fMode == true) g_lpTxBuff[Offset+1] = (char)nRet;
	if(fMode == true) IndirectWriteByte( Offset+1, (char)nRet);
	nRet += 2;
//
return nRet;	
}
/////////////////////////////////////////////////////////////////////////
// ������� ������������ ASN.1 ����������� ������, �������������, IP ������
// ���������:	bool Mode - ���� TRUE - ������ ������������ � �������� �����; ���� FALSE - ������ �� ������������ � �������� ����� 
//				int Offset - �������� � �������� ������, ���� ������������ ���������
//				AsnAny *lpAnyValue - ��������� �� ���������, � ������� ���������� ������
// �������: ����� ������������� ������
///////////////////////////////////////////////////////////////////////
int StringToAsn(bool fMode, int Offset, AsnAny *lpAnyValue )
{
int nRet;
unsigned char *lpCodLen;
int Index;
//
	nRet = 0;
	//if(fMode == true) g_lpTxBuff[Offset] = lpAnyValue->asnType;
	if(fMode == true) IndirectWriteByte( Offset, (char)lpAnyValue->asnType);
	if(lpAnyValue->asnValue.string.length < 128)
	{
		nRet++;
		//if(fMode == true) g_lpTxBuff[Offset+nRet] = lpAnyValue->asnValue.string.length;
		if(fMode == true) IndirectWriteByte( Offset+nRet, (char)lpAnyValue->asnValue.string.length);
		nRet++;
		//if(fMode == true) memcpy( &g_lpTxBuff[Offset+nRet], (unsigned char *)lpAnyValue->asnValue.string.stream, lpAnyValue->asnValue.string.length);
		if(fMode == true) IndirectWriteBuf( Offset+nRet, (unsigned char *)lpAnyValue->asnValue.string.stream, lpAnyValue->asnValue.string.length);
		nRet += lpAnyValue->asnValue.string.length;
	}
	else
	{
		lpCodLen = (unsigned char *)&lpAnyValue->asnValue.string.length;
		for( Index  = (sizeof(int) - 1); Index >= 0; Index--)
		{
			if( ( (*(lpCodLen+Index)) != 0) || (nRet > 0) )
			{
				//if(fMode == true) g_lpTxBuff[Offset + 2 + Index] = (*(lpCodLen+Index));
				if(fMode == true) IndirectWriteByte( Offset+2+Index, (*(lpCodLen+Index)));
				nRet++;
			}
		}
		//if(fMode == true) g_lpTxBuff[Offset + 1] = 0x80 | nRet;
		if(fMode == true) IndirectWriteByte( Offset+1, 0x80 | nRet);
		nRet += 2;
		//if(fMode == true) memcpy( &g_lpTxBuff[Offset+nRet], (unsigned char *)lpAnyValue->asnValue.string.stream, lpAnyValue->asnValue.string.length);
		if(fMode == true) IndirectWriteBuf( Offset+nRet, (unsigned char *)lpAnyValue->asnValue.string.stream, lpAnyValue->asnValue.string.length);
		nRet += lpAnyValue->asnValue.string.length;
	}
//
return nRet;	
}
///////////////////////////////////////////////////////////////////////
// ������� ���������� ����� � �������� �����
// ���������:	bool Mode - ���� TRUE - ������ ������������ � �������� �����; ���� FALSE - ������ �� ������������ � �������� ����� 
//				int CogingLen - ����� ������
//				int Offset - �������� � �������� ������, ���� ������������ ���������
// �������: ����� ������������� ������
///////////////////////////////////////////////////////////////////////
int WriteAsnLength(bool fMode, int CogingLen, int Offset)
{
unsigned char *lpCodLen;
int nRet;
int Index;	
//
	if(CogingLen < 128)
	{
		//if(fMode == true) g_lpTxBuff[Offset++] = (char)CogingLen;
		if(fMode == true){ IndirectWriteByte( Offset++, CogingLen); }
		nRet = 1;
	}
	else
	{
		lpCodLen = (unsigned char *)&CogingLen;
		for(nRet = 0, Index = (sizeof(int)-1); Index >= 0; Index--)
		{
			if(  ( (*(lpCodLen+Index)) != 0) || (nRet > 0) )
			{
				//if(fMode == true) g_lpTxBuff[Offset+1+Index] = (*(lpCodLen+Index));
				//if(fMode == true){ IndirectWriteByte( Offset+1+Index, (*(lpCodLen+Index))); }
				if(fMode == true){ IndirectWriteByte( Offset+1+nRet, (*(lpCodLen+Index))); }
				nRet++;
			}
		}
		//if(fMode == true) g_lpTxBuff[Offset] = 0x80 | (char)nRet;
		if(fMode == true) { IndirectWriteByte( Offset, 0x80 | (char)nRet); }
		nRet++;
	}
//
return nRet;	
}
///////////////////////////////////////////////////////////////////////
// ������� ����������� �������������� ��������� ����� ���������� � ������� � ���������� �� � 
// ��������� AsnAny.
// ���������:	int UsedOid - ����� ������������� OID
//			AsnObjectId *lpObjID - ��������� � ������������� OID
//			AsnAny *lpAnyValue - ��������� �� ���������� � ������� ������������ ������������� ���������
//			unsigned short ValType - ��� �������������� ���������. ��� ����� � ������ ������� ����� ���� 
// 						NOTE_BYFUNC(Atual Data Get By Function) && 
//					(NOTE_HASINDEX(Variable Are Indexed) || (NOTE_ISRDONLY(Data Is ReadONLY) )
//			unsigned long ValValue - ������������� ��������. ������� 2 ����� ���������� ������������� 
//								�������������� ���������. ������� - ��������� ��������� ����������� 
//								��������.
// �������: TRUE - ���� �����, ����� - FALSE.
///////////////////////////////////////////////////////////////////////
bool UploadValue(int UsedOid, AsnObjectId *lpObjID, AsnAny *lpAnyValue, unsigned short ValType, unsigned long ValValue)
{
static sCommand stComa; // ��������� ��� ���������� � ������� ��������� xQueueCommand
static sInfoKadr stReply; // ��������� ��� ������ �� ������� ��������
MKAPARAM *lpMka;
unsigned char bModType;
unsigned char fFromQueue;
unsigned char Coma[4];
unsigned int Mask=1;
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
unsigned char cDeb[14];
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//	
	// ������������ ������� � ���������� � ������� �� �������� � RS232 
	stComa.Source = SOURCE_SNMP; // �������� 
	stComa.A1 = 0;
	switch(ValValue&0x0000ffff)
	{
	// System Brunch
	case SYSUPTIME: 
		lpAnyValue->asnValue.unsigned32 = (unsigned int)xTaskGetTickCount()/10;	
		if(lpAnyValue->asnValue.unsigned32 == 0xffffffff) lpAnyValue->asnValue.unsigned32 = 0;
		lpAnyValue->asnType = ASN_INTEGER; return true;
	// Product Info Branch
	case SITALLALARMSTATE: 
	case SITALLADMINSTATE:
		lpAnyValue->asnValue.unsigned32 = 0x00;	lpAnyValue->asnType = ASN_INTEGER; return true;
	case SITALLADRESSIP: 
		memcpy(lpAnyValue->asnValue.address.stream, ucDataSIPR, 4);
		lpAnyValue->asnValue.address.length = 4; lpAnyValue->asnType = ASN_IPADDRESS;
	return true;
	// UK_CS block. 		
	// MKA Module		
	case UKCSCARDNUMBER:
	case MKKCARDNUM:
	case MKACARDNUM: lpAnyValue->asnValue.unsigned32 = lpObjID->ObjectId[UsedOid];
		lpAnyValue->asnType = ASN_INTEGER; return true;
	case MKKCIRCIND:
	case MKAPORTIND:lpAnyValue->asnValue.unsigned32 = lpObjID->ObjectId[UsedOid+1];
		lpAnyValue->asnType = ASN_INTEGER; return true;
/*	case MKAVERS: strcpy((char*)lpAnyValue->asnValue.string.stream, "Ver.1.0"); 
		lpAnyValue->asnValue.string.length = strlen("Ver.1.0"); 
		lpAnyValue->asnType = ASN_OCTETSTRING; return true;*/
	case MKAPORTONOF: lpAnyValue->asnValue.unsigned32 = 0x01;
		lpAnyValue->asnType = ASN_INTEGER; return true;
/*	case MKAPORTTYPE:
	case MKAPORTMODE:
	case MKAPORTINPLEV:
	case MKAPORTOUTLEV:
	case MKAPORTARULEV:
	case MKAPORTNOISELEV:
	case MKAPORTTIMELEV:
	case MKAPORTECHOSUPP:
	case MKAARUONOFF:
		bModType = MODULE_MKA; fFromQueue = true; 
		Coma = GETMKAPARAM; break;*/
	case MKAPORTOVERVOLT: lpAnyValue->asnValue.unsigned32 = 0x00;
		lpAnyValue->asnType = ASN_INTEGER; return true;
	case MKAPORTALARM:  lpAnyValue->asnValue.unsigned32 = 0;
		lpAnyValue->asnType = ASN_INTEGER; return true;
	//
	default:
		if( ((ValValue&0x0000ffff) / 100) == 7)
		{
/*			stComa.Length = 2;
			stComa.BufCommand = pvPortMalloc( 2 ); // �������� �������
			if (stComa.BufCommand)
			{
				stComa.BufCommand[0] = GETCONSIST;
				stComa.BufCommand[1] = 0;
				if (  xQueueSend( xQueueCommand, &stComa, 1 ) == errQUEUE_FULL) // ���������� � ������� �������
				{   // ������� �����������
					vPortFree(stComa.BufCommand);	// �������� ������������� �������
					g_ErrStatus = SNMP_ERRORSTATUS_GENERR;
					return false;
				}
			
				if( xQueueReceive( xQueueReply[stComa.Source], &stReply, 110 ) ) // 
				{
///////////////////////////////////////////////////////////////				
g_deb[0] = 0xff;
///////////////////////////////////////////////////////////////				
					Mask = 0x00000003 << ((lpObjID->ObjectId[UsedOid]-4) * 2);
					Mask &= (0 | stReply.ByteInfoKadr[1] | (stReply.ByteInfoKadr[2]<<8) | 
							(stReply.ByteInfoKadr[3]<<16) | (stReply.ByteInfoKadr[4]<<24) );
					vPortFree(stReply.ByteInfoKadr);	// �������� ������������� �������
					if( (Mask == 0) && (lpObjID->ObjectId[UsedOid] >= MKK_POS_BEG) && (lpObjID->ObjectId[UsedOid] <= MKK_POS_END) )
					{ 
						lpAnyValue->asnType = ASN_INTEGER;
						switch(ValValue&0x0000ffff)
						{
						case MKAVERS: 
							strcpy((char*)lpAnyValue->asnValue.string.stream, "Absent"); 
							lpAnyValue->asnValue.string.length = strlen("Absent"); 
							lpAnyValue->asnType = ASN_OCTETSTRING; return true;
						case MKAPORTTYPE: 
						case MKAPORTMODE:  
						case MKAPORTINPLEV:  
						case MKAPORTOUTLEV:  
						case MKAPORTARULEV:  
						case MKAPORTNOISELEV:  
						case MKAPORTTIMELEV:  
						case MKAPORTECHOSUPP:  
						case MKAARUONOFF:  
							lpAnyValue->asnValue.unsigned32 = 0;
							return true;
						}
					}
					
					//
				}
			}
			else
			{
				vPortFree(stComa.BufCommand);
			}
*/			
			bModType = MODULE_MKA; fFromQueue = true; 
			Coma[0] = GETMKAPARAM; Coma[1] = (lpObjID->ObjectId[UsedOid+1]-1)  + ((lpObjID->ObjectId[UsedOid]-1)*4); 
			break;
		}
		else if( ((ValValue&0x0000ffff) / 100) == 6)
		{
			bModType = MODULE_MKK; fFromQueue = true; Coma[0] = GETMKKPARAM; Coma[1] = lpObjID->ObjectId[UsedOid]; break;
		}
		else if( ((ValValue&0x0000ffff) / 100) == 4)
		{
			bModType = BLOCK_UKCS; fFromQueue = true; Coma[0] = GETCONSIST; break;
		}
	/*	else if(((ValValue&0x0000ffff) / 100) == 2)
		{
					{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )UKCSSINCHADDR | ONE_IND},	//	ukcsSynchroAddress	INTEGER,
					{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )UKCSSINCHTYPE | ONE_IND},	//	ukcsSynchroType		INTEGER
		}*/
		else return false;		
	}
	if( fFromQueue == true )
	{
		stComa.Length = 2;
		stComa.BufCommand = pvPortMalloc( 2 ); // �������� �������
		if (stComa.BufCommand)
		{
			stComa.BufCommand[0] = Coma[0];
			stComa.BufCommand[1] = Coma[1];
			if (  xQueueSend( xQueueCommand, &stComa, 1 ) == errQUEUE_FULL) // ���������� � ������� �������
			{   // ������� �����������
				vPortFree(stComa.BufCommand);	// �������� ������������� �������
				g_ErrStatus = SNMP_ERRORSTATUS_GENERR;
				return false;
			}
			
			if( xQueueReceive( xQueueReply[stComa.Source], &stReply, 110 ) ) // 
			{
				//if (stReply.ByteInfoKadr[1] == CodeStatusTrue)
				//{	// ������� ����������� 
					switch(bModType)
					{
						case MODULE_MKA: 
							lpAnyValue->asnType = ASN_INTEGER;
							switch(ValValue&0x0000ffff)
							{
							case MKAVERS: 
								if(stReply.ByteInfoKadr[0] == GETMKAPARAM)
								{
									strcpy((char*)lpAnyValue->asnValue.string.stream, "Absent"); 
									lpAnyValue->asnValue.string.length = strlen("Absent"); 
									//sprintf((char*)lpAnyValue->asnValue.string.stream, "Buff(inHex):%x, %x, %x, %x, %x, %x", stReply.ByteInfoKadr[0], stReply.ByteInfoKadr[1], stReply.ByteInfoKadr[2], stReply.ByteInfoKadr[3], stReply.ByteInfoKadr[4], stReply.ByteInfoKadr[5] );
									//lpAnyValue->asnValue.string.length = strlen((char*)lpAnyValue->asnValue.string.stream); 
								}
								else
								{
									strcpy((char*)lpAnyValue->asnValue.string.stream, "Ver.1.0"); 
									lpAnyValue->asnValue.string.length = strlen("Ver.1.0"); 
									//sprintf((char*)lpAnyValue->asnValue.string.stream, "O.K.Buff(inHex):%x, %x, %x, %x, %x, %x", stReply.ByteInfoKadr[0], stReply.ByteInfoKadr[1], stReply.ByteInfoKadr[2], stReply.ByteInfoKadr[3], stReply.ByteInfoKadr[4], stReply.ByteInfoKadr[5] );
									//lpAnyValue->asnValue.string.length = strlen((char*)lpAnyValue->asnValue.string.stream); 
								}
								lpAnyValue->asnType = ASN_OCTETSTRING; Mask=0; break;
							case MKAPORTTYPE: 
								/*if (stReply.ByteInfoKadr[1] == CodeStatusTrue) lpAnyValue->asnValue.unsigned32 = 0;
								else */lpAnyValue->asnValue.unsigned32 = ((MKAPARAM *)&stReply.ByteInfoKadr[2])->EndingType;
								Mask=0; break;
							case MKAPORTMODE:  
								/*if (stReply.ByteInfoKadr[1] == CodeStatusTrue) lpAnyValue->asnValue.unsigned32 = 0;
								else */lpAnyValue->asnValue.unsigned32 = ((MKAPARAM *)&stReply.ByteInfoKadr[2])->EndingMode;
								Mask=0; break;
							case MKAPORTINPLEV:  
								/*if (stReply.ByteInfoKadr[1] == CodeStatusTrue) lpAnyValue->asnValue.unsigned32 = 0;
								else*/ lpAnyValue->asnValue.unsigned32 = ((MKAPARAM *)&stReply.ByteInfoKadr[2])->InpLevel;
								Mask=0; break;
							case MKAPORTOUTLEV:  
								/*if (stReply.ByteInfoKadr[1] == CodeStatusTrue) lpAnyValue->asnValue.unsigned32 = 0;
								else */lpAnyValue->asnValue.unsigned32 = ((MKAPARAM *)&stReply.ByteInfoKadr[2])->OutLevel;
								Mask=0; break;
							case MKAPORTARULEV:  
								/*if (stReply.ByteInfoKadr[1] == CodeStatusTrue) lpAnyValue->asnValue.unsigned32 = 0;
								else */lpAnyValue->asnValue.unsigned32 = ((MKAPARAM *)&stReply.ByteInfoKadr[2])->AruLevel;
								Mask=0; break;
							case MKAPORTNOISELEV:  
								/*if (stReply.ByteInfoKadr[1] == CodeStatusTrue) lpAnyValue->asnValue.unsigned32 = 0;
								else */lpAnyValue->asnValue.unsigned32 = ((MKAPARAM *)&stReply.ByteInfoKadr[2])->NoiseReject;
								Mask=0; break;
							case MKAPORTTIMELEV:  
								/*if (stReply.ByteInfoKadr[1] == CodeStatusTrue) lpAnyValue->asnValue.unsigned32 = 0;
								else */lpAnyValue->asnValue.unsigned32 = ((MKAPARAM *)&stReply.ByteInfoKadr[2])->LevelTime;
								Mask=0; break;
							case MKAPORTECHOSUPP:  
								/*if (stReply.ByteInfoKadr[1] == CodeStatusTrue) lpAnyValue->asnValue.unsigned32 = 0;
								else */lpAnyValue->asnValue.unsigned32 = (((MKAPARAM *)&stReply.ByteInfoKadr[2])->EchoSuppres >> 1) & 0x01;
								Mask=0; break;
							case MKAARUONOFF:  
								/*if (stReply.ByteInfoKadr[1] == CodeStatusTrue) lpAnyValue->asnValue.unsigned32 = 0;
								else */lpAnyValue->asnValue.unsigned32 = (((MKAPARAM *)&stReply.ByteInfoKadr[2])->EchoSuppres >> 5) & 0x01;
								Mask=0; break;
							}
						case MODULE_MKK:
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//							lpMkk = ((MKAPARAM *)&stReply.ByteInfoKadr[lpObjID->ObjectId[UsedOid+1]])->Tribype;
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
							// MKK Module
							switch(ValValue&0x0000ffff)
							{
							case MKKPCM30: lpAnyValue->asnValue.unsigned32 = ((MKKPARAM *)cDeb[lpObjID->ObjectId[UsedOid+1]])->Tribype; Mask=0; break;
							case MKKLINECRC: lpAnyValue->asnValue.unsigned32 = ((MKKPARAM *)cDeb[lpObjID->ObjectId[UsedOid+1]])->CrcOnOff; Mask=0; break;
							case MKKERRCRC: lpAnyValue->asnValue.unsigned32 = ((MKKPARAM *)cDeb[lpObjID->ObjectId[UsedOid+1]])->ErrThreshold; Mask=0; break;
							case MKKLOOP: lpAnyValue->asnValue.unsigned32 = ((MKKPARAM *)cDeb[lpObjID->ObjectId[UsedOid+1]])->Loop; Mask=0; break;
							case MKKLINEONOF: lpAnyValue->asnValue.unsigned32 = ((MKKPARAM *)cDeb[lpObjID->ObjectId[UsedOid+1]])->Using; Mask=0; break;
							case MKKLINESTATUS: lpAnyValue->asnValue.unsigned32 = 0 | cDeb[ lpObjID->ObjectId[UsedOid+1]*2+3 ] |
									(cDeb[ lpObjID->ObjectId[UsedOid+1]*2+4 ] << 8) | (cDeb[ 0x0d] << 16); Mask=0; break;
							}
						break;
						case MODULE_MU:
						break;
						case BLOCK_UKCS:
							lpAnyValue->asnType = ASN_INTEGER;
							switch(ValValue&0x0000ffff)
							{
							case UKCSCARDTYPE: 
								if( (lpObjID->ObjectId[UsedOid] >= MKA_POS_BEG) && (lpObjID->ObjectId[UsedOid] <= MKA_POS_END) )
								{ 
/*									Mask = 0x00000003 << (22-((lpObjID->ObjectId[UsedOid]-3) * 2));
									Mask &= (0 | (stReply.ByteInfoKadr[1]<<24) | (stReply.ByteInfoKadr[2]<<16) | 
											(stReply.ByteInfoKadr[3]<<8) | stReply.ByteInfoKadr[4] );*/
									Mask = (0 | stReply.ByteInfoKadr[4] | (stReply.ByteInfoKadr[3]<<8) | 
											(stReply.ByteInfoKadr[2]<<16) | (stReply.ByteInfoKadr[1]<<24) );

									Mask >>= (22-((lpObjID->ObjectId[UsedOid]-3) * 2));
									Mask &= 3;
									if( (Mask == 0)||(Mask > 2) ) lpAnyValue->asnValue.unsigned32 = 0;
									else lpAnyValue->asnValue.unsigned32 = MODULE_MKA;
									Mask=0; break; 
								}
								else if( (lpObjID->ObjectId[UsedOid] >= MKK_POS_BEG) && (lpObjID->ObjectId[UsedOid] <= MKK_POS_END) )
								{ 
/*									Mask = 0x00000003 << ((lpObjID->ObjectId[UsedOid]-4) * 2);
									Mask &= (0 | stReply.ByteInfoKadr[4] | (stReply.ByteInfoKadr[3]<<8) | 
											(stReply.ByteInfoKadr[2]<<16) | (stReply.ByteInfoKadr[1]<<24) );
*/											
									//Mask = 0x00000003 << ((lpObjID->ObjectId[UsedOid]-4) * 2);
									Mask = (0 | stReply.ByteInfoKadr[4] | (stReply.ByteInfoKadr[3]<<8) | 
											(stReply.ByteInfoKadr[2]<<16) | (stReply.ByteInfoKadr[1]<<24) );
									Mask >>= ((lpObjID->ObjectId[UsedOid]-4) * 2);
									Mask &= 3;
									if( (Mask == 0)||(Mask > 2) ) lpAnyValue->asnValue.unsigned32 = 0;
									else lpAnyValue->asnValue.unsigned32 = MODULE_MKK;  
									Mask=0; break; 
								}
								else if(lpObjID->ObjectId[UsedOid] == MPU_POS_END)
								{
									lpAnyValue->asnValue.unsigned32 = MODULE_MPU;  
									Mask=0; break; 
								}
								else if(lpObjID->ObjectId[UsedOid] == MPL_POS_END)
								{
									lpAnyValue->asnValue.unsigned32 = 0;//MODULE_MPL;  
									Mask=0; break; 
								}
								else if(lpObjID->ObjectId[UsedOid] == MU_POS_END)
								{
									lpAnyValue->asnValue.unsigned32 = MODULE_MU;  
									Mask=0; break; 
								}
							}
						break;
					}
				//}
				//else return false;
				vPortFree(stReply.ByteInfoKadr);	// �������� ������������� �������
				if(Mask == 0) return true;
			}
			else return false;
		}
		else
		{
			vPortFree(stComa.BufCommand);
		}
	}
//
return false;	
}
///////////////////////////////////////////////////////////////////////
// ������� ��������� �� ������������ � ������������� �������������� ��������� 
// ���������:	int UsedOid - ����� ������������� OID
//			AsnObjectId *lpObjID - ��������� � ������������� OID
//			AsnAny *lpAnyValue - ��������� �� ���������� � ������� ������������ ������������� ���������
//			unsigned short ValType - ��� �������������� ���������. ��� ����� � ������ ������� ����� ���� 
// 						NOTE_BYFUNC(Atual Data Get By Function) && 
//					(NOTE_HASINDEX(Variable Are Indexed) || (NOTE_ISRDONLY(Data Is ReadONLY) )
//			unsigned long ValValue - ������������� ��������. ������� 2 ����� ���������� ������������� 
//								�������������� ���������. ������� - ��������� ��������� ����������� 
//								��������.
// �������: TRUE - ���� �����, ����� - FALSE(��� ���� ��������������� ��� ������� ������ � SNMP ����).
///////////////////////////////////////////////////////////////////////
bool SetupValue(int UsedOid, AsnObjectId *lpObjID, AsnAny *lpAnyValue, unsigned short ValType, unsigned long ValValue)
{
sCommand stComa; // ��������� ��� ���������� � ������� ��������� xQueueCommand
sInfoKadr stReply; // ��������� ��� ������ �� ������� ��������
MKAPARAM *lpMka;
unsigned char bModType;
unsigned char fToQueue;
unsigned char Coma;
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
unsigned char cDeb[14];
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//	
	// ������������ ������� � ���������� � ������� �� �������� � RS232 
	stComa.Source = SOURCE_SNMP; // �������� 
	stComa.A1 = 0;
	stComa.Length = 4;
	stComa.BufCommand = pvPortMalloc( 4 ); // �������� �������
	if (stComa.BufCommand == 0) { g_ErrStatus = SNMP_ERRORSTATUS_GENERR; return false; }
	switch(ValValue&0x0000ffff)
	{
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!		
	case MKAPORTONOF:	 return false;
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!		
	case MKAPORTTYPE: 
/*		switch(lpAnyValue->asnValue.number)
		{
			case 1://-���, 
			case 2://-���, 
			case 4://-��������, 
			case 8://-����, 
			case 16://-�� ������, 
			case 32://-������� �����, 
			case 64://-��������, 
			case 128://-�����������, 
			case 256://-��, 
			case 512://-����������, 
			case 1024://-���������� ���������*/
				stComa.BufCommand[0] = COMAMKA_SETTYPE;
				stComa.BufCommand[1] = (unsigned char)lpObjID->ObjectId[UsedOid] * lpObjID->ObjectId[UsedOid+1]-1; 
				stComa.BufCommand[2] = (unsigned char)lpAnyValue->asnValue.number; 
/*			break;
			default: g_ErrStatus = SNMP_ERRORSTATUS_BADVALUE; goto BadResult;
		}*/
	break;	
	case MKAPORTMODE:// ��������� ������ ������ ���������:1-����������������,2-�������������,3-��, 4-���
		if( (lpAnyValue->asnValue.number > 0) && (lpAnyValue->asnValue.number <= 4) )
		{
			stComa.BufCommand[0] = COMAMKA_SETMODE;
			stComa.BufCommand[1] = (unsigned char)lpObjID->ObjectId[UsedOid] * lpObjID->ObjectId[UsedOid+1]-1; 
			stComa.BufCommand[2] = (unsigned char)lpAnyValue->asnValue.number; 
		}
		else { g_ErrStatus = SNMP_ERRORSTATUS_BADVALUE; goto BadResult; }
	break;
	case MKAPORTINPLEV:
		if( (lpAnyValue->asnValue.number >= 0) && (lpAnyValue->asnValue.number <= 28) )
		{
			stComa.BufCommand[0] = COMAMKA_SETINPLEV;
			stComa.BufCommand[1] = (unsigned char)lpObjID->ObjectId[UsedOid] * lpObjID->ObjectId[UsedOid+1]-1; 
			stComa.BufCommand[2] = (unsigned char)lpAnyValue->asnValue.number; 
		}
		else { g_ErrStatus = SNMP_ERRORSTATUS_BADVALUE; goto BadResult; }
	break;
	case MKAPORTOUTLEV:
		if( (lpAnyValue->asnValue.number >= 1) && (lpAnyValue->asnValue.number <= 28) )
		{
			stComa.BufCommand[0] = COMAMKA_SETOUTLEV;
			stComa.BufCommand[1] = (unsigned char)lpObjID->ObjectId[UsedOid] * lpObjID->ObjectId[UsedOid+1]-1; 
			stComa.BufCommand[2] = (unsigned char)lpAnyValue->asnValue.number; 
		}
		else { g_ErrStatus = SNMP_ERRORSTATUS_BADVALUE; goto BadResult; }
	break;
	case MKAPORTARULEV:
		if( (lpAnyValue->asnValue.number >= 0) && (lpAnyValue->asnValue.number <= 28) )
		{
			stComa.BufCommand[0] = COMAMKA_SETARULEV;
			stComa.BufCommand[1] = (unsigned char)lpObjID->ObjectId[UsedOid] * lpObjID->ObjectId[UsedOid+1]-1; 
			stComa.BufCommand[2] = (unsigned char)lpAnyValue->asnValue.number; 
		}
		else { g_ErrStatus = SNMP_ERRORSTATUS_BADVALUE; goto BadResult; }
	break;
	case MKAPORTNOISELEV:
		if( (lpAnyValue->asnValue.number >= 0) && (lpAnyValue->asnValue.number <= 100) )
		{
			stComa.BufCommand[0] = COMAMKA_SETECHOSUPPR;
			stComa.BufCommand[1] = (unsigned char)lpObjID->ObjectId[UsedOid] * lpObjID->ObjectId[UsedOid+1]-1; 
			stComa.BufCommand[2] = (unsigned char)lpAnyValue->asnValue.number; 
		}
		else { g_ErrStatus = SNMP_ERRORSTATUS_BADVALUE; goto BadResult; }
	break;
	case MKAPORTTIMELEV:
		if( (lpAnyValue->asnValue.number >= 0) && (lpAnyValue->asnValue.number <= 32) )
		{
			stComa.BufCommand[0] = COMAMKA_SETTIMELEV;
			stComa.BufCommand[1] = (unsigned char)lpObjID->ObjectId[UsedOid] * lpObjID->ObjectId[UsedOid+1]-1; 
			stComa.BufCommand[2] = (unsigned char)lpAnyValue->asnValue.number; 
		}
		else { g_ErrStatus = SNMP_ERRORSTATUS_BADVALUE; goto BadResult; }
	break;
	case MKAPORTECHOSUPP:
		if(lpAnyValue->asnValue.number & 0x01) stComa.BufCommand[0] = COMAMKA_SETECHOON;
		else  stComa.BufCommand[0] = COMAMKA_SETECHOOFF;
		stComa.BufCommand[1] = (unsigned char)lpObjID->ObjectId[UsedOid] * lpObjID->ObjectId[UsedOid+1]-1; 
		break;
	case MKAARUONOFF:
		if( (lpAnyValue->asnValue.number >= 0) && (lpAnyValue->asnValue.number <= 1) )
		{
			stComa.BufCommand[0] = COMAMKA_MKAARUONOFF;
			stComa.BufCommand[1] = (unsigned char)lpObjID->ObjectId[UsedOid] * lpObjID->ObjectId[UsedOid+1]-1; 
			stComa.BufCommand[2] = (unsigned char)lpAnyValue->asnValue.number; 
		}
		else { g_ErrStatus = SNMP_ERRORSTATUS_BADVALUE; goto BadResult; }
		break;
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!		
	case MKAPORTOVERVOLT: 	return false;
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!		
	default: return false;		
	}
	// Send Command
	if (  xQueueSend( xQueueCommand, &stComa, 1 ) == errQUEUE_FULL) // ���������� � ������� �������
	{   // ������� �����������
BadResult:			
		vPortFree(stComa.BufCommand);	// �������� ������������� �������
		return false;
	}
	// Wait Reply
	if( xQueueReceive( xQueueReply[stComa.Source], &stReply, 1000 ) ) // 
	{
		if(stReply.ByteInfoKadr[1] != CodeStatusTrue)
		{
			g_ErrStatus = SNMP_ERRORSTATUS_GENERR;
		}
		vPortFree(stReply.ByteInfoKadr);	// �������� ������������� �������
	}
//
return true;	
}
//////////////////////////////////////////////////////////////////////////////////////////
// ������� ��������� ����� � �������� � �������������� TRAP ���������.
// ���������: SITALTRAP *lpBuff - ����������� �����
//				unsigned int *lpBegTx - ����� ����������, � ������� ������������ ����� ������ UDP ���������
//				unsigned int *lpEndTx - ����� ����������, � ������� ������������ ����� ����� UDP ���������
// �������: TRUE - ���� ������; FALSE -���� �����
//////////////////////////////////////////////////////////////////////////////////////////
bool CreateTrap(SITALTRAP *lpBuff, unsigned int *lpBegTx, unsigned int *lpEndTx)
{
int DataLen, TmpInt, TrapType;
unsigned int OutBuffPtr;
AsnAny AnyValue;
AsnObjectId ObjID;
unsigned int Dovesok;
HANDLE hFile;
//
    csync();
	OutBuffPtr = SBUFBASEADDRESS[SNMP_SOCKET] + TX_WRITE_OFFSET;
	(*(lpEndTx)) = OutBuffPtr;
	// Write Address
	memcpy(ObjID.ObjectId, &g_OidBypass[UKCSTABIND].ObjectId, g_OidBypass[UKCSTABIND].idLength);
	ObjID.idLength = g_OidBypass[UKCSTABIND].idLength;
	ObjID.ObjectId[ObjID.idLength++] = 125;
	
	//AnyValue.asnType = ASN_INTEGER;
	//AnyValue.asnValue.unsigned32 = lpBuff->bAdr;
	AnyValue.asnType = ASN_OCTETSTRING;
	//sprintf(AnyValue.asnValue.string.stream, "Adr:%u", lpBuff->bAdr);
	//sprintf(AnyValue.asnValue.string.stream, "%02X %02X, %02X, %02X", lpBuff->bComa, lpBuff->bAdr, lpBuff->bPort, lpBuff->Reason);
	TrapType = FormatTrapMsg(lpBuff, AnyValue.asnValue.string.stream);

	AnyValue.asnValue.string.length = strlen(AnyValue.asnValue.string.stream);

	DataLen = WriteOutValue( &ObjID, &AnyValue, OutBuffPtr, true );
	OutBuffPtr += DataLen;
	// Write Port
/*	AnyValue.asnType = ASN_INTEGER;
	AnyValue.asnValue.unsigned32 = lpBuff->bPort;
	memcpy(ObjID.ObjectId, &g_OidBypass[UKCSTABIND].ObjectId, g_OidBypass[UKCSTABIND].idLength);
	ObjID.idLength = g_OidBypass[UKCSTABIND].idLength;
	ObjID.ObjectId[ObjID.idLength++] = 126;
	DataLen = WriteOutValue( &ObjID, &AnyValue, OutBuffPtr, true );
	OutBuffPtr += DataLen;
	// Write Reason
	AnyValue.asnType = ASN_INTEGER;
	AnyValue.asnValue.unsigned32 = lpBuff->Reason;
	memcpy(ObjID.ObjectId, &g_OidBypass[UKCSTABIND].ObjectId, g_OidBypass[UKCSTABIND].idLength);
	ObjID.idLength = g_OidBypass[UKCSTABIND].idLength;
	ObjID.ObjectId[ObjID.idLength++] = 127;
	DataLen = WriteOutValue( &ObjID, &AnyValue, OutBuffPtr, true );
	OutBuffPtr += DataLen;
*/	//
	(*(lpEndTx)) = OutBuffPtr;
	OutBuffPtr = SBUFBASEADDRESS[SNMP_SOCKET] + TX_WRITE_OFFSET;
	// Write TrapData Object ID
	DataLen = (*(lpEndTx))-OutBuffPtr;
	Dovesok = WriteAsnLength(false, DataLen, 0);
	OutBuffPtr -= Dovesok;
	WriteAsnLength(true, DataLen, OutBuffPtr);
	OutBuffPtr--;
	IndirectWriteByte( OutBuffPtr--, ASN_SEQUENCE);
	// Write Time Stamp
	AnyValue.asnType = ASN_INTEGER;
	AnyValue.asnValue.unsigned32 = (unsigned int)0xaa;//xTaskGetTickCount()/1000;
	Dovesok = DigitToAsn(false, OutBuffPtr, &AnyValue );
	OutBuffPtr -= (Dovesok-1);
	DigitToAsn(true, OutBuffPtr, &AnyValue );
	OutBuffPtr--;
		//IndirectWriteByte( OutBuffPtr--, 0x0);
		//IndirectWriteByte( OutBuffPtr--, 1);
		//IndirectWriteByte( OutBuffPtr--, ASN_INTEGER);
	// Write specific TRAP: "enterpriseCpecific Trap"(6)
	IndirectWriteByte( OutBuffPtr--, TrapType);//0x01);
	IndirectWriteByte( OutBuffPtr--, 1);
	IndirectWriteByte( OutBuffPtr--, ASN_INTEGER);
	// Write TRAP-Type
	IndirectWriteByte( OutBuffPtr--, 0x06);
	IndirectWriteByte( OutBuffPtr--, 1);
	IndirectWriteByte( OutBuffPtr, ASN_INTEGER);
	// Write Agent's Address
	AnyValue.asnType = ASN_IPADDRESS;
	AnyValue.asnValue.string.length = 4;
	memcpy(AnyValue.asnValue.string.stream, ucDataSIPR, 4);
	Dovesok = StringToAsn(false, OutBuffPtr, &AnyValue );
	OutBuffPtr -= Dovesok;
	Dovesok = StringToAsn(true, OutBuffPtr, &AnyValue );
	// Write Enterprise's Object Identifier {0x08,0x2b,0x06,0x01,0x04,0x01,0xe0,0x39,0x01};
	AnyValue.asnType = ASN_OBJECTIDENTIFIER;
	AnyValue.asnValue.string.length = g_OidBypass[ENTERPRISESTABIND].idLength;
	memcpy(AnyValue.asnValue.string.stream, g_OidBypass[ENTERPRISESTABIND].ObjectId, g_OidBypass[ENTERPRISESTABIND].idLength);
	Dovesok = StringToAsn(false, OutBuffPtr, &AnyValue );
	OutBuffPtr -= Dovesok;
	Dovesok = StringToAsn(true, OutBuffPtr, &AnyValue );
	// Write PDU's Frame Length
	DataLen = (*(lpEndTx))-OutBuffPtr;
	Dovesok = WriteAsnLength(false, DataLen, 0);
	OutBuffPtr -= Dovesok;
	WriteAsnLength(true, DataLen, OutBuffPtr);
	OutBuffPtr--;
	// PDU Type
	IndirectWriteByte( OutBuffPtr, SNMP_PDU_V1TRAP);
	// Write Community
	hFile = CreateFile((char *)g_agm32ComunityRO, GENERIC_READ, 0);
	AnyValue.asnValue.string.length = ReadFile(hFile, AnyValue.asnValue.string.stream, -1);
	AnyValue.asnType = ASN_OCTETSTRING;
	CloseHandle( hFile );
	Dovesok = StringToAsn(false, OutBuffPtr, &AnyValue );
	OutBuffPtr -= Dovesok;
	Dovesok = StringToAsn(true, OutBuffPtr, &AnyValue );
	OutBuffPtr--;
	// Write SNMP Version
	IndirectWriteByte( OutBuffPtr--, SNMP_V1_0);
	IndirectWriteByte( OutBuffPtr--, 1);
	IndirectWriteByte( OutBuffPtr, ASN_INTEGER);
	// Write Frame Length & ASN_SEQUENCE
	DataLen = (*(lpEndTx))-OutBuffPtr;
	Dovesok = WriteAsnLength(false, DataLen, 0);
	OutBuffPtr -= Dovesok;
	WriteAsnLength(true, DataLen, OutBuffPtr);
	OutBuffPtr--;
	IndirectWriteByte( OutBuffPtr, ASN_SEQUENCE);
	//
	(*(lpBegTx)) = OutBuffPtr;
//
return true;
}
//////////////////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////////////////
unsigned char mass1[8];
bool SnmpTask(void)
{
unsigned char mass[8];
int HowRecv, i, SnmpLen;
unsigned int BegInp, EndInp;
unsigned int BegTx, EndTx;
SITALTRAP Buff;
sInfoKadr stReply;
//

//    csync();
	// Wait TRAP From "IRON"
	if( xQueueReceive( xQueueReply[SOURCE_SNMP], &stReply, 100 ) )
	{
		BegTx = EndTx = 0;
		//if( CreateTrap( (SITALTRAP *)&stReply.ByteInfoKadr[1], &BegTx, &EndTx) == true )
		if( CreateTrap( (SITALTRAP *)&stReply.ByteInfoKadr[0], &BegTx, &EndTx) == true )
		{
			//AsnAddressIp  g_MainMngIp[4] = {4, {192, 168, 1, 21} };
			//AsnAddressIp  g_MainStbyIp[4] = {4, {192, 168, 1, 21} };
			// Setup Target Address & Source Port
	   		IndirectWriteByte(tcpDEST_PORT_REG(SNMP_SOCKET), 0x00);
	   		IndirectWriteByte(tcpDEST_PORT_REG(SNMP_SOCKET) + 1, (char)SNMP_TRAP_PORT );
	   		if( (g_MainMngIp.len == 4) && (g_MainMngIp.adr[0]+g_MainMngIp.adr[1]+g_MainMngIp.adr[2]+g_MainMngIp.adr[3]) )
	   		{ 
		   		for(HowRecv = 0; HowRecv < 4; HowRecv++)
		   		{
			   		IndirectWriteByte(tcpDEST_ADDR_REG(SNMP_SOCKET)+HowRecv, g_MainMngIp.adr[HowRecv]);
		   		}
		   		//
			    mass[0]= (unsigned char)(BegTx>>24); mass[1]= (unsigned char)(BegTx>>16);
			    mass[2]= (unsigned char)(BegTx>>8); mass[3]= (unsigned char)BegTx;
				IndirectWriteBuf(tcpTX_READ_POINTER_REG(SNMP_SOCKET),	mass, 4);
				vTaskDelay( 1 );
				IndirectWriteBuf(tcpTX_ACK_POINTER_REG(SNMP_SOCKET),	mass, 4);
				vTaskDelay( 1 );
			    mass[0]= (unsigned char)(EndTx>>24); mass[1]= (unsigned char)(EndTx>>16);
			    mass[2]= (unsigned char)(EndTx>>8); mass[3]= (unsigned char)EndTx;
				IndirectWriteBuf(tcpTX_WRITE_POINTER_REG(SNMP_SOCKET), mass, 4);
				vTaskDelay( 1 );
		   		//
				IndirectWriteByte(tcpCOMMAND_REG(SNMP_SOCKET), tcpSEND_CMD);
				//
				vTaskDelay(100);
		   		IndirectWriteByte(tcpDEST_PORT_REG(SNMP_SOCKET), 0x00);
		   		IndirectWriteByte(tcpDEST_PORT_REG(SNMP_SOCKET) + 1, (char)SNMP_PORT );
				IndirectWriteByte(tcpCOMMAND_REG(SNMP_SOCKET), tcpRECEIVE_CMD);
				//
				// SEND
				BegTx = EndTx = 0;
	   		}
		}

		//
		vPortFree(stReply.ByteInfoKadr);	// �������� ������������� �������
		return true;	
	}

	//
	HowRecv= select(SNMP_SOCKET, SEL_RECV);
	if (HowRecv > 0)
	{
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*pFIO_FLAG_T = 0x0800; // ������ PF11 - ������� ���������
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		mass[0] = IndirectReadByte(tcpRX_READ_SHADOW_REG(SNMP_SOCKET) );//0x1ea);
		vTaskDelay(1);
		IndirectReadBuf(tcpRX_READ_POINTER_REG(SNMP_SOCKET), (unsigned char *)&mass[0], 4);
		BegInp = 0 | mass[3] | (mass[2] << 8) | (mass[1] << 16) | (mass[0] << 24);
		vTaskDelay(1);
		mass[1] = IndirectReadByte(tcpRX_WRITE_SHADOW_REG(SNMP_SOCKET) );//0x1e9);
		vTaskDelay(1);
		IndirectReadBuf(tcpRX_WRITE_POINTER_REG(SNMP_SOCKET), (unsigned char *)&mass[0], 4);
		EndInp = 0 | mass[3] | (mass[2] << 8) | (mass[1] << 16) | (mass[0] << 24);
		//
LabelLoop:
		IndirectReadBuf(BegInp, (unsigned char *)&HowRecv, 2);
		//
		BegInp += 8;
		//
		// ���� ��� �� SNMP ���� -> continue
		// ��������� SNMP - ����
		SnmpLen = 0;
		BegTx = EndTx = 0;
		//if(	CreateTrap(&Buff, &BegTx, &EndTx) == true)
		if( SnmpParser(BegInp, EndInp, &BegTx, &EndTx, &SnmpLen) == true)
		{
		    mass[0]= (unsigned char)(BegTx>>24); mass[1]= (unsigned char)(BegTx>>16);
		    mass[2]= (unsigned char)(BegTx>>8); mass[3]= (unsigned char)BegTx;
			IndirectWriteBuf(tcpTX_READ_POINTER_REG(SNMP_SOCKET), mass, 4);
			//vTaskDelay( 1 );
			IndirectWriteBuf(tcpTX_ACK_POINTER_REG(SNMP_SOCKET), mass, 4);
			//vTaskDelay( 1 );
		    mass[0]= (unsigned char)(EndTx>>24); mass[1]= (unsigned char)(EndTx>>16);
		    mass[2]= (unsigned char)(EndTx>>8); mass[3]= (unsigned char)EndTx;
			IndirectWriteBuf(tcpTX_WRITE_POINTER_REG(SNMP_SOCKET), mass, 4);
			//vTaskDelay( 1 );
			//IndirectWriteByte(tcpCOMMAND_REG(SNMP_SOCKET), tcpSEND_CMD | tcpRECEIVE_CMD);							 		// SEND
		}
		else if( (BegTx + EndTx) != 0) // ���� ��������� SNMP- ����� ��������� � �������.
		{
			mass[0] = (unsigned char)(SBUFBASEADDRESS[SNMP_SOCKET] >>24);
			mass[1] = (unsigned char)(SBUFBASEADDRESS[SNMP_SOCKET] >>16);
			mass[2] = (unsigned char)(SBUFBASEADDRESS[SNMP_SOCKET] >>8);
			mass[3] = (unsigned char)SBUFBASEADDRESS[SNMP_SOCKET];
			IndirectWriteBuf(tcpTX_READ_POINTER_REG(SNMP_SOCKET),	mass, 4);
//			vTaskDelay( 1 );
			IndirectWriteBuf(tcpTX_ACK_POINTER_REG(SNMP_SOCKET),	mass, 4);
//			vTaskDelay( 1 );
			//
			//for(i=0; i<(EndInp-BegInp); i++)
			for(i=0; i<SnmpLen; i++)
			{
				mass[0] = IndirectReadByte(BegInp+i);
				if((BegTx-8) == i) IndirectWriteByte(SBUFBASEADDRESS[SNMP_SOCKET]+i, SNMP_PDU_RESPONSE);
				else if((EndTx-8) == i) IndirectWriteByte(SBUFBASEADDRESS[SNMP_SOCKET]+i, (unsigned char)g_ErrStatus);
				else IndirectWriteByte(SBUFBASEADDRESS[SNMP_SOCKET]+i, mass[0]);
			}
		    mass[0]= (unsigned char)((SBUFBASEADDRESS[SNMP_SOCKET]+i)>>24);
		    mass[1]= (unsigned char)((SBUFBASEADDRESS[SNMP_SOCKET]+i)>>16);
		    mass[2]= (unsigned char)((SBUFBASEADDRESS[SNMP_SOCKET]+i)>>8); 
		    mass[3]= (unsigned char)(SBUFBASEADDRESS[SNMP_SOCKET]+i);
			IndirectWriteBuf(tcpTX_WRITE_POINTER_REG(SNMP_SOCKET), mass, 4);
//			vTaskDelay( 1 );
			//IndirectWriteByte(tcpCOMMAND_REG(SNMP_SOCKET), tcpSEND_CMD | tcpRECEIVE_CMD);							 		// SEND
		}
		// Renew DST Address & Port For Tx
		IndirectReadBuf(BegInp-6, (unsigned char *)&mass1[0], 4);
		IndirectWriteBuf(tcpDEST_ADDR_REG(SNMP_SOCKET), mass1, 4);	// Designate destination IP address
		IndirectReadBuf(BegInp-2, (unsigned char *)&mass[0], 2);
   		IndirectWriteByte(tcpDEST_PORT_REG(SNMP_SOCKET), mass[0]);
   		IndirectWriteByte(tcpDEST_PORT_REG(SNMP_SOCKET) + 1, mass[1]);
  		//
 		IndirectWriteByte(tcpCOMMAND_REG(SNMP_SOCKET), tcpSEND_CMD | tcpRECEIVE_CMD);							 		// SEND
		//
		mass[1] = IndirectReadByte(tcpRX_WRITE_SHADOW_REG(SNMP_SOCKET) );//0x1e9);
//		vTaskDelay(1);
		IndirectReadBuf(tcpRX_WRITE_POINTER_REG(SNMP_SOCKET), (unsigned char *)&mass[0], 4);
		EndInp = 0 | mass[3] | (mass[2] << 8) | (mass[1] << 16) | (mass[0] << 24);
		//
		//if( (BegInp+HowRecv) < EndInp) { BegInp += HowRecv; g_deb[0]++; goto LabelLoop; }
		if( (BegInp+SnmpLen) < EndInp) 
		{ 
			BegInp += SnmpLen; goto LabelLoop; 
		}
		else 
		{
			mass[0] = (unsigned char)(RBUFBASEADDRESS[SNMP_SOCKET]>>24); 
			mass[1] = (unsigned char)(RBUFBASEADDRESS[SNMP_SOCKET]>>16);
			mass[2] = (unsigned char)(RBUFBASEADDRESS[SNMP_SOCKET]>>8); 
			mass[3] = (unsigned char)(RBUFBASEADDRESS[SNMP_SOCKET]);
			IndirectWriteBuf(tcpRX_READ_POINTER_REG(SNMP_SOCKET), mass, 4);
//			vTaskDelay( 1 );
			IndirectWriteBuf(tcpRX_WRITE_POINTER_REG(SNMP_SOCKET), mass, 4);
//			vTaskDelay( 1 );
		}
	}
//
return true;	
}
//////////////////////////////////////////////////////////////////////////////////////////
// ������� ��������� ����� � �������� � ����������� TRAP ��������� ��� ������.
// ���������: SITALTRAP *lpBuff - ����������� �����
//				unsigned char *lpOutBuff - �������� �����
// �������: TRUE - ���� ������; FALSE -���� �����
//////////////////////////////////////////////////////////////////////////////////////////
int FormatTrapMsg(SITALTRAP *lpBuff, unsigned char *lpOutBuff)
{
//
	sprintf(lpOutBuff, "Mod:%u, Port:%u, Code:%u", lpBuff->bAdr, lpBuff->bPort, lpBuff->Reason);
	if( (lpBuff->bPort == 0xff) && (lpBuff->Reason == 0x01)) return 3;
	else if( (lpBuff->bAdr >= 13) && (lpBuff->bAdr <= 16))
	{
	 	if(lpBuff->bPort >= 0x00)
	 	{
	 		if( (lpBuff->Reason == 0x07) || (lpBuff->Reason == 0x0d) ) return 3;
	 		else if( (lpBuff->Reason == 0x09) || (lpBuff->Reason == 0x0b) || (lpBuff->Reason == 0x0f) ) return 2;
	 	}
	 	else if( (lpBuff->bPort >= 1) && (lpBuff->bPort <= 4) )
	 	{
 		 	if( (lpBuff->Reason == 0x09) || (lpBuff->Reason == 0x0b) || (lpBuff->Reason == 0x0f) ) return 3;
 		 	else if( (lpBuff->Reason == 0x01) || (lpBuff->Reason == 0x02) || (lpBuff->Reason == 0x05) ||
 		 			(lpBuff->Reason == 0x07) || (lpBuff->Reason == 0x0d) || (lpBuff->Reason == 0x29) ||
 		 			(lpBuff->Reason == 0x2b) || (lpBuff->Reason == 0x2d) || (lpBuff->Reason == 0x2f)) return 2;
	 	}
	}
//
return 1;	
}

