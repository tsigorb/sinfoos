#include <cdefBF533.h>
#include <ccblkfn.h>
#include <string.h>

#include "jornal.h"
#include "..\RTC.h"
/* RTOS Scheduler include files. */
#include "../FreeRTOS/FreeRTOS.h"
#include "../FreeRTOS/queue.h"
#include "../FreeRTOS/task.h"
#include "..\FreeRTOS/semphr.h"

extern unsigned short DataSave1[];

// ������ FRAM 128 k���� !
///////////////////////////////////////////////////////////////////////////////
#define d_LogRecordSz2FRAM      (sizeof(LOG_RECORD)*2)
#define d_LOG_STRINFO_LEN2FRAM  (d_LOG_STRINFO_LEN*2)
#define JornalMaxRecord         1000
#define JornalSize              (JornalMaxRecord*d_LogRecordSz2FRAM)
#define FirstAddress            0x20000100
#define LastAddress             (FirstAddress+JornalSize)
#define StrInfoAddress          (((LastAddress / 64)+1)*64)
#define FirstRecordAddress      0x20000000
#define LastRecordAddress       (FirstRecordAddress+sizeof(unsigned int)*2)

#define d_FRAMSemaphoreTOut     300

///////////////////////////////////////////////////////////////////////////////
static xSemaphoreHandle xFRAMSemaphore = NULL;

///////////////////////////////////////////////////////////////////////////////
void Wait300nS(void)
{
  int i,j;
  for(i=0;i<100;i++)
  {
    j=*pFIO_FLAG_D;
    ssync();
  }
}

// ������ HowOut ���� �� FRAM �� ���������� ������
// �������: TRUE ���� ���������� �������������� (����� LVL - ������� �������)
// ���������: unsigned int InpAddr - ����� ������.
//			unsigned short *lpOut - ����� ��������� ������.
// 			unsigned int HowOut ���������� ���� ��� ������
static bool ReadFRAM(const unsigned int InpAddr, unsigned char *lpOut, unsigned int HowOut)
{
  unsigned int j,Index;
  unsigned short *pwAddr=(unsigned short *)InpAddr;

  if(*pFIO_FLAG_D & PF15)
  { // ���� ���������� ��������������
    for(Index=0;Index<HowOut;Index++)
    {
      j=cli();
      lpOut[Index]=*pwAddr++;
      ssync();
      sti(j);
      Wait300nS();
    }
    return true;
  }
  return false;
}

// ������ ������� ������ �� Flash.
// �������: TRUE ���� ���������� �������������� (����� LVL - ������� �������)
// ���������: unsigned int Addr - ����� ������
//			unsigned char *lpInp - ��������� �� ������ ������� ������
//			int HowInp - ���������� ������������ ����
static bool WriteFRAM(unsigned int Addr, unsigned char *lpInp, unsigned int HowInp)
{
  unsigned int j,Index;
  unsigned short *pwAddr=(unsigned short *)Addr;

  if(*pFIO_FLAG_D & PF15)
  { // ���� ���������� ��������������
    for(Index=0;Index<HowInp;Index++)
    {
      j=cli();
      *pwAddr++=lpInp[Index];
      ssync();
      sti(j);
      Wait300nS();
    }
    return true;
  }
  return false;
}

// ������ ������ ������ ������ ������� �������
// �������: ����� ������
static unsigned int GetFirstRecordAddress(void)
{
  unsigned int adr;

  ReadFRAM(FirstRecordAddress,(unsigned char *)(&adr), sizeof(unsigned int));
  return adr;
}

// ������ ������ ��������� ������ ������� �������
// �������: ����� ������
static unsigned int GetLastRecordAddress(void)
{
  unsigned int adr;

  ReadFRAM(LastRecordAddress,(unsigned char *)(&adr),sizeof(unsigned int));
  return(adr);
}

// ����������� ������ ��������� ������ ������� �������
// �������: -
// ���������: unsigned int newAddress - ����� ����� ������
static void SetLastRecordAddress(unsigned int newAddress)
{ WriteFRAM(LastRecordAddress, (unsigned char *)(&newAddress), sizeof(int)); }

// ����������� ������ ������ ������ ������� �������
// �������: -
// ���������: unsigned int newAddress - ����� ����� ������
static void SetFirstRecordAddress(unsigned int newAddress)
{ WriteFRAM(FirstRecordAddress, (unsigned char *)(&newAddress), sizeof(int)); }

// ��������� ��������� FRAM
// �������: -
// ���������: -
void InitFRAM(void)
{
  (*pFIO_DIR) &= ~PF15;
  Wait300nS();
  (*pFIO_INEN) |= PF15;
  Wait300nS();
  if(*pFIO_FLAG_D & PF15)
  { // ���� ���������� ��������������
    vSemaphoreCreateBinary(xFRAMSemaphore);
    if(!xFRAMSemaphore) return;
    if((GetFirstRecordAddress()<FirstAddress) ||
       (GetFirstRecordAddress()>=JornalSize+FirstAddress) ||
       (GetLastRecordAddress()<FirstAddress) ||
       (GetLastRecordAddress()>=JornalSize+FirstAddress))
    {
      SetFirstRecordAddress(FirstAddress);
      SetLastRecordAddress(FirstAddress);
    }
  }
}

void ClearLogs(void)
{
  if(!xFRAMSemaphore) return;
  xSemaphoreTake(xFRAMSemaphore);
  SetFirstRecordAddress(FirstAddress);
  SetLastRecordAddress(FirstAddress);
  xSemaphoreGive(xFRAMSemaphore);
}

// ���������� ���������� ������� ������� �������
// �������: ���������� �������
unsigned int _xGetLogRecordsCount(void)
{
  unsigned int fa,la;
  fa=GetFirstRecordAddress();
  la=GetLastRecordAddress();
  if(la<fa) la+=LastAddress-FirstAddress;
  fa=(la-fa)/d_LogRecordSz2FRAM;
  return fa;
}

unsigned int GetLogRecordsCount(void)
{
  unsigned int cnt;

  if(!xFRAMSemaphore) return(0);
  xSemaphoreTake(xFRAMSemaphore);
  cnt=_xGetLogRecordsCount();
  xSemaphoreGive(xFRAMSemaphore);
  return cnt;
}

// ������ ������ � �������� ������� �� ������� �������
// ���������: unsigned int RecNumber - ���������� ����� ������ � �������.
//            LOG_RECORD *lpRecord - ����� ��������� ������.
bool PeekLogRecord(unsigned int RecNumber, LOG_RECORD *lpRecord,char *strInfo)
{
  bool bRes;
  unsigned int fa;

  memset(lpRecord,0,sizeof(LOG_RECORD));
  if(!xFRAMSemaphore) return(false);
  xSemaphoreTake(xFRAMSemaphore);
  if(_xGetLogRecordsCount()<RecNumber) bRes=true;
  else
  {
    fa=GetFirstRecordAddress()-FirstAddress;
    fa=(fa+(RecNumber*d_LogRecordSz2FRAM)) % JornalSize;
    bRes=ReadFRAM(fa+FirstAddress,(unsigned char *)lpRecord,sizeof(LOG_RECORD));
    if(bRes && strInfo && (lpRecord->message==0xffff))
    {
      fa/=d_LogRecordSz2FRAM;
      fa*=d_LOG_STRINFO_LEN2FRAM;
      bRes=ReadFRAM(fa+StrInfoAddress,(unsigned char *)strInfo,d_LOG_STRINFO_LEN);
    }
  }
  xSemaphoreGive(xFRAMSemaphore);
  return(bRes);
}

// ������� ������ � ������ �������
// �������: ������ TRUE
// ���������: LOG_RECORD *lpRecord - ����� ��������� ������.
static bool WriteLogRecord(LOG_RECORD *lpRecord)
{
  unsigned int fa, la;
  bool bOk;
 
  if(!xFRAMSemaphore) return(false);
  xSemaphoreTake(xFRAMSemaphore);
  fa = GetFirstRecordAddress();
  la = GetLastRecordAddress();
  bOk=WriteFRAM(la,(unsigned char *)lpRecord,sizeof(LOG_RECORD));
  la+=d_LogRecordSz2FRAM;
  if(la>=LastAddress) la=FirstAddress;
  SetLastRecordAddress(la);
  if(fa==la)
  {
    fa+=d_LogRecordSz2FRAM;
    if(fa>=LastAddress) fa=FirstAddress;
    SetFirstRecordAddress(fa);
  }
  xSemaphoreGive(xFRAMSemaphore);
  return bOk;
}

// ������� ������ � ������ �������
// �������: ������ TRUE
// ���������: LOG_RECORD *lpRecord - ����� ��������� ������.
static bool WriteLogRecordWithInfo(LOG_RECORD *lpRecord,char *strInfo)
{
  unsigned int fa,la,sa;
  bool bOk;
 
  if(!xFRAMSemaphore) return(false);
  xSemaphoreTake(xFRAMSemaphore);
  la = GetLastRecordAddress();
  bOk=WriteFRAM(la,(unsigned char *)lpRecord,sizeof(LOG_RECORD));
  if(bOk)
  {
    sa=(la-FirstAddress)/d_LogRecordSz2FRAM;
    sa*=d_LOG_STRINFO_LEN2FRAM;
    bOk=WriteFRAM(sa+StrInfoAddress,(unsigned char *)strInfo,strlen(strInfo)+1);
    if(bOk)
    {
      la+=d_LogRecordSz2FRAM;
      if(la>=LastAddress) la=FirstAddress;
      SetLastRecordAddress(la);
      fa = GetFirstRecordAddress();
      if(fa==la)
      {
        fa+=d_LogRecordSz2FRAM;
        if(fa>=LastAddress) fa=FirstAddress;
        SetFirstRecordAddress(fa);
      }
    }
  }
  xSemaphoreGive(xFRAMSemaphore);
  return bOk;
}

// �������� ������ � log-���� ��������� ����������
// �������: ������ TRUE
// ���������: unsigned char typ - ��� (�����������, ��������������, ������ � �.�.)
// unsigned char source - �������� (�������, ������ � �.�.)
// unsigned char category - ��������� (������� �����������)
// unsigned short code - ��� �������
bool AddToLog(unsigned char status,unsigned char source,unsigned char category,unsigned short message)
{
  LOG_RECORD nr;

  nr.status=status;
  nr.source=source;
  nr.category=category;
  nr.message=message;
  nr.Timepoint=ReadClock();
  return WriteLogRecord(&nr);
}

bool AddToLogWithInfo(unsigned char status,unsigned char source,unsigned char category,char *strInfo)
{
  LOG_RECORD nr;

  if(!strInfo) return(false);
  if(strlen(strInfo)>=d_LOG_STRINFO_LEN) strInfo[d_LOG_STRINFO_LEN-1]=0;
  nr.status=status;
  nr.source=source;
  nr.category=category;
  nr.message=0xffff;
  nr.Timepoint=ReadClock();
  return(WriteLogRecordWithInfo(&nr,strInfo));
}

