#pragma once 
#ifndef MIBDATA_H_INCLUDED
#define MIBDATA_H_INCLUDED

#include "..\SNMP\Asn1.h"

//
// Module Idenitfiers
//
#define MODULE_MU	0x01
#define MODULE_MKC	0x02
#define MODULE_MKA	0x03
//#define MODULE_MU	0x04
#define BLOCK_UKCS	0x7f
// 
#define MU_POS_END	1
#define MKA_POS_BEG	2
#define MKA_POS_END	13
#define MKC_POS_BEG	14
#define MKC_POS_END	17
//
// String For Trap Messages
//
/*
������	����� ������	����� �����	���	���������

�������	1�12	0xFF	0x01	��� ������ ������ ���
�������	13�16	0xFF	0x01	��� ������ ������ ���
�������	13�16	0		0x07	���� ������ � ������� ����������
�������	13�16	0		0x0D	������ 2-�� ���������� �������������
�������	13�16	1�4		0x09	������ ������������� CRC
�������	13�16	1�4		0x0B	������ �������� �������������
�������	13�16	1�4		0x0F	������ �������� �������

������	13�16	0		0x09	������ �� ��������
������	13�16	0		0x0B	������������� �� ����������� ����������
������	13�16	0		0x0F	������ 1-�� ���������� �������������
������	13�16	1�4		0x01	��������� �� ������ � ��������� ������� ��-16
������	13�16	1�4		0x03	������ ������������� �������������
������	13�16	1�4		0x05	����� ������� ��������� ���������� ��������� ���� � ��-16
������	13�16	1�4		0x07	��������� �� ������ � ��������� �������
������	13�16	1�4		0x0D	����� ������� ��������� ���������� ��������� ����
������	13�16	1�4		0x29	���� ������� � ���. > 10-3
������	13�16	1�4		0x2B	���� ������� � ���. < 10-3
������	13�16	1�4		0x2D	���� ��������� ������
������	13�16	1�4		0x2F	���������� ��������� ������ ������

�������	1..12	0xFF	0x02	����� ������ ��� ������������
�������	13�16	0xFF	0x02	���� ����� ������ ���
�������	13�16	0		0x08	��� ���� ������ � ������� ����������
�������	13�16	0		0x0A	������ ��������
�������	13�16	0		0x0C	������������� �� ��������� ����������
�������	13�16	0		0x0E	��� ������ 2-�� ���������� �������������
�������	13�16	0		0x10	��� ������ 1-�� ���������� �������������
�������	13�16	1�4		0x02	��� ������ � ��������� ������� ��-16
�������	13�16	1�4		0x04	������������� ������������� �������������
�������	13�16	1�4		0x06	��� ������� ��������� ���������� ��������� ���� � ��-16
�������	13�16	1�4		0x08	��� ������ � ��������� �������
�������	13�16	1�4		0x0A	������������� CRC �������������
�������	13�16	1�4		0x0C	�������� ������������� �������������
�������	13�16	1�4		0x0E	��� ������� ��������� ���������� ��������� ����
�������	13�16	1�4		0x10	�������������� �������� �������
		13�16	1�4		0x21	-
		13�16	1�4		0x22	-
		13�16	1�4		0x23	-
		13�16	1�4		0x24	-
		13�16	1�4		0x25	-
		13�16	1�4		0x26	-
		13�16	1�4		0x27	-
		13�16	1�4		0x28	-
�������	13�16	1�4		0x2A	��� ������ � ���. > 10-3
�������	13�16	1�4		0x2C	��� ������ � ���. < 10-3
�������	13�16	1�4		0x2E	��� ��������� ������
�������	13�16	1�4		0x30	��� ���������� ��������� ������ ������
		13�16	C0�CD	XX	��������� ��������� ������ ���
*/
/*
      internet    OBJECT IDENTIFIER ::= { iso org(3) dod(6) 1 }

   That is, the Internet subtree of OBJECT IDENTIFIERs starts with the prefix:

      1.3.6.1.

   This memo, as a standard approved by the IAB, now specifies the policy under which this subtree of OBJECT IDENTIFIERs is administered.  Initially, four nodes are present:

      directory     OBJECT IDENTIFIER ::= { internet 1 }
      mgmt          OBJECT IDENTIFIER ::= { internet 2 }
      experimental  OBJECT IDENTIFIER ::= { internet 3 }
      private       OBJECT IDENTIFIER ::= { internet 4 }
*/
#define INTERNET	0x032b0601       02010516
//
#define DIRECTORY		0x042b060101
#define MGMT			0x042b060102   
#define EXPERIMENTAL	0x042b060103
#define PRIVATE			0x042b060104
/*
               mib        OBJECT IDENTIFIER ::= { mgmt 1 }

               system     OBJECT IDENTIFIER ::= { mib 1 }
               interfaces OBJECT IDENTIFIER ::= { mib 2 }
               at         OBJECT IDENTIFIER ::= { mib 3 }
               ip         OBJECT IDENTIFIER ::= { mib 4 }
               icmp       OBJECT IDENTIFIER ::= { mib 5 }
               tcp        OBJECT IDENTIFIER ::= { mib 6 }
               udp        OBJECT IDENTIFIER ::= { mib 7 }
               egp        OBJECT IDENTIFIER ::= { mib 8 }
*/
//
static unsigned char arraySYSTEM[]=		{0x06,0x2b,0x06,0x01,0x02,0x01,0x01};
//
static unsigned char arrayEnterpriseSINFO[]=	{0x08,0x2b,0x06,0x01,0x04,0x01,0xe0,0x39,0x01};
static unsigned char arrayEnterpriseSINFO_AGM[]={0x0a,0x2b,0x06,0x01,0x04,0x01,0xe0,0x39,0x01,0x01,0x01,0x01,0x01,0x01};

static unsigned char arrayExperimentSINFO[]=	{0x0a,0x2b,0x06,0x01,0x03,0x01,0xe0,0x39,0x01,0x01,0x01};
//
static unsigned char arraySINFOTrapAgm[]={0x0b,0x2b,0x06,0x01,0x04,0x01,0xe0,0x39,0x01,0x01,0x01,0x7f};
//
//
//
typedef struct tag_TabletValue
{
	unsigned short ValType;
//	union tag_Un 
//	{
//	void *lpValue;
	unsigned long datValue;
//	};
} TABLETVALUE;
//
//
//
typedef struct tag_OidBypass
{
	TABLETVALUE *lpTabValue;
	AsnObjectId ThisOid;
} OIDBYPASS;
//
// SNMP Group SYSTEM Variable
// Implementation of the System group is mandatory for all systems.
//

//
#define NOTE_ISNOT_USING	0x00000000	// Note In Table Is Not Using
#define NOTE_CAPACITY		0x00008000	// Note In Table == Number of Notes in Table
#define NOTE_INRAM			0x00000100	// Atual Data Are In RAM
#define NOTE_INFLASH		0x00000200 	// Atual Data Are In Flash ROM
#define NOTE_BYFUNC			0x00000400 	// Atual Data Get By Function
#define NOTE_HASINDEX		0x00000800	// Variable Are Indexed
#define NOTE_ISRDONLY		0x00001000 	// Data Is ReadONLY
#define NOTE_ISNEXTTABLE	0x00002000 	// Data Is a Next Table Address or End(if NULL)
#define NOTE_ISTABLE		0x00004000 	// Has Not Data. Data Is a Pointer to Child Table
//
#define ONE_IND				0x00010000
#define TWO_IND				0x00020000
#define THREE_IND			0x00030000
#define FOUR_IND			0x00040000

//
//
//
const char g_agm32ComunityRO[] = "PwsRO.txt";
const char g_agm32ComunityRW[] = "PwsRW.txt";
const char g_agm32ComunityWA[] = "PwsWA.txt";
//

//
// SYSTEM TABLE
//
//#define Sital 1 // � �������� ������������ 
#ifdef 	 Sital	
const AsnOctetString g_sysDescr[]={39, "Multiplex from SITAL 'UK_CS' 1234567890"};	// sysDescr { system 1 } // ��������� ��������
#else
const AsnOctetString g_sysDescr[]={26, "Multiplex 'KDS' 1234567890"};	// sysDescr { system 1 } // ��������� ��������
#endif

									// Access: read-only.
//const AsnObjectId g_sysObjectID[]={8, {0x2b,0x06,0x01,0x04,0x1,0x33,0xaa,0x77}};	// OBJECT IDENTIFIER sysObjectID { system 2 } 
const AsnObjectId g_sysObjectID[]={10, {0x2b,0x06,0x01,0x04,0x01,0x83,0xa8, 0x31, 2, 1} };
									// Access: read-only.
									// ������������� ���������� � ��������� MIB
unsigned long g_sysUpTime=0x12345678;		// SNMPv1 & SNMPv2 // TimeTicks(32) sysUpTime { system 3 } 
									// Access: read-only.
									// ����� � ����� ����� ������ � ���� �������, ����� ����� �������, 
									// ���������� �� ���������� �������, ���� ��������������.
#ifdef 	 Sital	
const char g_sysContact[] = "Sys1.txt";	// sysContact { system 4 } 
									// ACCESS  read-write
									// "The textual identification of the contact person
									// for this managed node, together with information
									// on how to contact this person."
const char g_sysName[] = "Sys2.txt";	// sysName { system 5 } // ������ ��� ������ ���� (FQDN) .
									// ACCESS  read-write
const char g_sysLocation[] = "Sys3.txt";	// sysLocation { system 6 } // ���������� ������������ ����.
									// ACCESS  read-write
#else
const char g_sysContact[] = "SysKDS1.txt";	
const char g_sysName[] = "SysKDS2.txt";	
const char g_sysLocation[] = "SysKDS3.txt";	
#endif
const unsigned int g_sysServices=0x4444;	// sysServices { system 7 }
									// ACCESS  read-only
									// ��������, ����������� �� ��, ����� ������� ��������������� �����. 
									// ��� ����� ������� OSI ������, �������������� �����. 
									// ��������� �������� ������������ ������, � ����������� �� ����, ����� 
									// ������� ��������������: 0�01 (����������), 0�02 (���������), 
									// 0�04 (�������), 0�08 (�����-�����), 0�40 (����������).
									// A value which indicates the set of services that
									// this entity primarily offers.
									// The value is a sum.  This sum initially takes the
									// value zero, Then, for each layer, L, in the range
	                      			// 1 through 7, that this node performs transactions
                      				// for, 2 raised to (L - 1) is added to the sum.  For
                      				// example, a node which performs primarily routing
                      				// functions would have a value of 4 (2^(3-1)).  In
                      				// contrast, a node which is a host offering
                      				// application services would have a value of 72
                      				// (2^(4-1) + 2^(7-1)).  Note that in the context of
                      				// the Internet suite of protocols, values should be
                      				// calculated accordingly:
                           			// layer  functionality
                               		//		1  physical (e.g., repeaters)
                               		//		2  datalink/subnetwork (e.g., bridges)
                               		// 		3  internet (e.g., IP gateways)
                               		// 		4  end-to-end  (e.g., IP hosts)
                               		// 		7  applications (e.g., mail relays)
                      				// For systems including OSI protocols, layers 5 and
                      				// 6 may also be counted."
//
AsnAddressIp  g_MainMngIp = {4, {192, 168, 1, 10} };
AsnAddressIp  g_MainStbyIp = {4, {192, 168, 1, 10} };
                      				
/*	const TABLETVALUE g_EnterpriseSINFO[4] = 
	{
		{NOTE_CAPACITY, (unsigned long )3},
		{ NOTE_ISTABLE, (unsigned long)&g_Table_1 },
		{ NOTE_INRAM | ASN_TIMETICKS, (unsigned long)&g_Var1 }, 
		{ NOTE_ISNEXTTABLE, 0}
	};*/

//
//   PM128 Config Liaf
//	
#define 	PMSERIALNUM	301
#define 	PMNUMBMANUF	302
#define 	PMSTATE		303
//
// ������������ ���������� ������� 
#define NumberKeyboard		12
			const TABLETVALUE g_pm128Conf[4] = 
			{
				{NOTE_CAPACITY, (unsigned long )3},
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )PMSERIALNUM | ONE_IND},	//�����  [1..12]			
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )PMNUMBMANUF | ONE_IND},	//	��������� �����
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )PMSTATE | ONE_IND},	//��������� ������	
			};
		const TABLETVALUE g_pm128EntryTab[2] = 
		{
			{NOTE_CAPACITY, (unsigned long )1},
			{NOTE_ISTABLE, (unsigned long )g_pm128Conf },
		};
	const TABLETVALUE g_pm128Table[2] = 
	{
		{NOTE_CAPACITY, (unsigned long )1},
		{NOTE_ISTABLE, (unsigned long )g_pm128EntryTab },
	};
//
//   MKA Config Liaf
//	
#define MKADEF				700
#define MKACARDNUM			MKADEF+1
#define MKAPORTIND			MKADEF+2
#define MKAVERS				MKADEF+3
#define MKAPORTONOF			MKADEF+4
#define MKAPORTTYPE			MKADEF+5
#define MKAPORTMODE			MKADEF+6
#define MKAPORTINPLEV		MKADEF+7
#define MKAPORTOUTLEV		MKADEF+8
#define MKAPORTARULEV		MKADEF+9
#define MKAPORTNOISELEV		MKADEF+10
#define MKAPORTTIMELEV		MKADEF+11
#define MKAPORTECHOSUPP		MKADEF+12
//#define MKAPORTOVERVOLT		MKADEF+13
#define MKAARUONOFF			MKADEF+13
#define MKAPORTALARM		MKADEF+14
//
// ������������ ���������� ���� � �����
#define MAXMKACARDNUM		12
// ������������ ���������� ������ �� �����
#define MAXMKAPORTNUM		4
//
			const TABLETVALUE g_mkaConf[15] = 
			{
				{NOTE_CAPACITY, (unsigned long )14},
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKACARDNUM | TWO_IND },	// mkaCardNumber		INTEGER,
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKAPORTIND | TWO_IND },	// mkaPortIndex		INTEGER, 
				{NOTE_ISRDONLY | ASN_OCTETSTRING | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKAVERS | TWO_IND },	// mkaVers				OCTET STRING,
				{ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKAPORTONOF | TWO_IND },	// mkaPortOnOff		INTEGER, 
				{ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKAPORTTYPE | TWO_IND },	// mkaPortType			INTEGER, 
				{ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKAPORTMODE | TWO_IND },	// mkaPortMode			INTEGER, 
				{ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKAPORTINPLEV | TWO_IND },	// mkaPortInpLevel		INTEGER, 
				{ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKAPORTOUTLEV | TWO_IND },	// mkaPortOutLevel		INTEGER, 
				{ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKAPORTARULEV | TWO_IND },	// mkaPortARULevel		INTEGER, 
				{ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKAPORTNOISELEV | TWO_IND },	// mkaPortNoiseLevel	INTEGER, 
				{ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKAPORTTIMELEV | TWO_IND },	// mkaPortTimeLevel	INTEGER, 
				{ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKAPORTECHOSUPP | TWO_IND },	// mkaPortEchoSuppr	INTEGER, 
				{ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKAARUONOFF | TWO_IND },	// mkaAruOnOff		INTEGER, 
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKAPORTALARM | TWO_IND }	// mkaPortAlarm		INTEGER
			};
		const TABLETVALUE g_mkaEntryTab[2] = 
		{
			{NOTE_CAPACITY, (unsigned long )1},
			{NOTE_ISTABLE, (unsigned long )g_mkaConf },
		};
	const TABLETVALUE g_mkaTable[2] = 
	{
		{NOTE_CAPACITY, (unsigned long )1},
		{NOTE_ISTABLE, (unsigned long )g_mkaEntryTab },
	};
//
// The MKC config Liaf
//
#define MKCCARDNUM				601
#define MKCFLOWINDEX			602
#define MKCFLOWE1				603
#define MKCPCM30				604
#define MKCEXTERNALTRAIN		605
#define MKCINTERNALTRAIN		606
#define MKCKOEFFERR				607
#define MKCEXCESSKOEFFERR		608
#define MKCSTATECRC				609
#define MKCLOSSINPUTSIGNAL		610
#define MKCLOSSCYCLESYNCHR		611
#define MKCSINGLEERR			612
#define MKCLOSSSYNCHRCRC		613
#define MKCERRES				614
#define MKCERRSES				615
#define MKCERRAIS				616
#define MKCERRAIS_KI16			617
#define MKCERRIAS				618
#define MKCDAMAGEREMOVEDSIDE	619
#define MKCDAMAGEREMOVEDSIDE16	620
#define MKCSTATE				627

#define MKCCARDNUM1	626
#define MKCSTATE1	628
#define MKCLEADER	621
#define MKCSOURCE1	622
#define MKCSOURCE2	623
#define MKCDAMAGE1	624
#define MKCDAMAGE2	625


//
// ������������ ���������� ������� ���
#define MAXMKCCARDNUM			4
// ������������ ���������� ������ �� �����
#define MAXMKCPORTNUM			4
			const TABLETVALUE g_mkcFlowEntry[22] = 
			{
				{NOTE_CAPACITY, (unsigned long )21},
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCCARDNUM | TWO_IND },	// ����� ���
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCFLOWINDEX | TWO_IND },	// ����� ������
				{NOTE_ISRDONLY | ASN_OCTETSTRING | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCSTATE | TWO_IND },	// ��������� ������
				{ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCFLOWE1 | TWO_IND },	// 	����� �1 ���/����
				{ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCPCM30 | TWO_IND },	// ��� ������
				{ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCEXTERNALTRAIN | TWO_IND },	// 	������� �����
				{ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCINTERNALTRAIN | TWO_IND },	// 	���������� �����
				{ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCKOEFFERR | TWO_IND },	// ����� ������������ ������
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCEXCESSKOEFFERR | TWO_IND },	// ���������� ������ ������
				{ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCSTATECRC | TWO_IND },	// ��������� CRC ���/����
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCLOSSINPUTSIGNAL | TWO_IND },	// ������ �������� �������
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCLOSSCYCLESYNCHR | TWO_IND },	// ������ �������� �������������
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCSINGLEERR | TWO_IND },	// ������� ��������� ������
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCLOSSSYNCHRCRC | TWO_IND },	// ������ ������������� CRC				
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCERRES | TWO_IND },	// MKCLineOnOff			INTEGER,
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCERRSES | TWO_IND },	// MKCLineOnOff			INTEGER,
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCERRAIS | TWO_IND },	// MKCLineOnOff			INTEGER,
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCERRAIS_KI16 | TWO_IND },	// MKCLineOnOff			INTEGER,
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCERRIAS | TWO_IND },	// MKCLineOnOff			INTEGER,
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCDAMAGEREMOVEDSIDE | TWO_IND },	// MKCLineOnOff			INTEGER,
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCDAMAGEREMOVEDSIDE16 | TWO_IND }	// MKCLineOnOff			INTEGER,
			};
		const TABLETVALUE g_mkcFlow[2] = 
		{
			{NOTE_CAPACITY, (unsigned long )1},
			{NOTE_ISTABLE, (unsigned long )g_mkcFlowEntry }
		};
		
			const TABLETVALUE g_mkcSynchrEntry[8] = 
			{
				{NOTE_CAPACITY, (unsigned long )7},
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCCARDNUM1 | ONE_IND},	//����� ������ [13..16]			
				{NOTE_ISRDONLY | ASN_OCTETSTRING | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCSTATE1 | ONE_IND },	// ��������� ������
				{ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCLEADER | ONE_IND},	//	�������/�������	
				{ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCSOURCE1 | ONE_IND},	// �������� ������������� � 1-� �����������		
				{ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )MKCSOURCE2 | ONE_IND},	// �������� ������������� � 2-� �����������		
				{NOTE_ISRDONLY | ASN_INTEGER  | NOTE_HASINDEX, (unsigned long )MKCDAMAGE1 | ONE_IND},	// ������ ������������� 1-�� ������	
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_HASINDEX, (unsigned long )MKCDAMAGE2 | ONE_IND}	//	������ ������������� 2-�� ������	
			};				
				
		const TABLETVALUE g_mkcSynchr[2] = 
		{
			{NOTE_CAPACITY, (unsigned long )1},
			{NOTE_ISTABLE, (unsigned long )g_mkcSynchrEntry }
		};
		
	const TABLETVALUE g_mkcTable[3] = 
	{
		{NOTE_CAPACITY, (unsigned long )2},
		{NOTE_ISTABLE, (unsigned long )g_mkcFlow },
		{NOTE_ISTABLE, (unsigned long )g_mkcSynchr }
	};

//
// The MU config Liaf
//
#define MUHIWAYIND				501
#define MUHIWAYONOF				502
#define MUHIWAYTYPE				503
#define MUHIWAYMODE				504
#define MUHIWAYGEN				505
#define MUHIWAYEX				506
#define MUHIWAYALRM				507
//
// ������������ ������(���������� �������)
//

#define MUSWVERSION 			1		
#define MUTIMEPROCESSINTERRUPT 	2	

	const TABLETVALUE g_muConfig[3] = 
	{
		{NOTE_CAPACITY, (unsigned long )2},
		{NOTE_ISRDONLY | ASN_OCTETSTRING | NOTE_BYFUNC, (unsigned long )MUSWVERSION },	// muSWVersion OBJECT-TYPE	SYNTAX  DisplayString
		{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC, (unsigned long )MUTIMEPROCESSINTERRUPT },	// muTimeProcesInterrup SYNTAX  INTEGER
	};
//
// Brunch UK_CS Data
//
#define UKCSCARDNUMBER				407
#define UKCSCARDTYPE				408
//
// ������������ ���������� ����, ������������� � �����
//
#define MAXCARDNUMBER				17	
//	
			const TABLETVALUE g_ukcsConfig[3] = 
			{
				{NOTE_CAPACITY, (unsigned long )2},
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )UKCSCARDNUMBER | ONE_IND},	//	ukcsCardNumber
				{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC | NOTE_HASINDEX, (unsigned long )UKCSCARDTYPE | ONE_IND},	//	ukcsCardType
			};
		const TABLETVALUE g_ukcsConfigEntry[2] = 
		{
			{NOTE_CAPACITY, (unsigned long )1},
			{NOTE_ISTABLE, (unsigned long )g_ukcsConfig},
		};
	const TABLETVALUE g_ukcsConfigTable[2] = 
	{
		{NOTE_CAPACITY, (unsigned long )1},
		{NOTE_ISTABLE, (unsigned long )g_ukcsConfigEntry },
	};
//

#define UKCSNUMBERSERIAL	201		

	const TABLETVALUE g_ukcsInformation[2] = 
	{
		{NOTE_CAPACITY, (unsigned long )1},
		{NOTE_ISRDONLY | ASN_OCTETSTRING | NOTE_BYFUNC, (unsigned long )UKCSNUMBERSERIAL },	//  SYNTAX  DisplayString
	};
//
 


#define SITALLALARMSTATE			101
#define SITALLADMINSTATE			102
#define SITALLADRESSIP				103
#define SITALLMAINMNGR				104
#define SITALLSTBYMNGR				105
#define SITALLCOMMUNRO				106
#define SITALLCOMMUNRW				107
//#define SITALLCOMMUNWA				108
	const TABLETVALUE g_ProductInfo[8] =
	{
		{NOTE_CAPACITY, (unsigned long )7},
		{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC, (unsigned long )SITALLALARMSTATE },	// sitallAlarmState
		{NOTE_ISRDONLY | ASN_INTEGER | NOTE_BYFUNC, (unsigned long )SITALLADMINSTATE },	// sitallAdminState
		{NOTE_ISRDONLY | ASN_IPADDRESS | NOTE_BYFUNC, (unsigned long )SITALLADRESSIP },	// sitallAddressIp
		{ASN_IPADDRESS | NOTE_INRAM, (unsigned long )&g_MainMngIp},//SITALLMAINMNGR },	// sitallMainManager
		{ASN_IPADDRESS | NOTE_INRAM, (unsigned long )&g_MainStbyIp},//SITALLSTBYMNGR },	// sitallStbyManager
		{NOTE_INFLASH | ASN_OCTETSTRING, (unsigned long )&g_agm32ComunityRO},//SITALLCOMMUNRO },	// sitallComunityRO
		{NOTE_INFLASH | ASN_OCTETSTRING, (unsigned long )&g_agm32ComunityRW}, //SITALLCOMMUNRW },	// sitallComunityRW
		//{NOTE_INFLASH | ASN_OCTETSTRING, (unsigned long )&g_agm32ComunityWA},//SITALLCOMMUNWA },	// sitallComunityWA
	};
//const char g_agm32ComunityRO[] = "PwsRO.txt";
//const char g_agm32ComunityRW[] = "PwsRW.txt";
//const char g_agm32ComunityWA[] = "PwsWA.txt";
//
//
//	
#define SYSUPTIME			9901
	const TABLETVALUE g_SystemTable[8] =
	{
		{NOTE_CAPACITY, (unsigned long )7},
		{ NOTE_ISRDONLY | NOTE_INRAM | ASN_OCTETSTRING, (unsigned long)&g_sysDescr },
		{ NOTE_ISRDONLY | NOTE_INRAM | ASN_OBJECTIDENTIFIER, (unsigned long)&g_sysObjectID },  
		//{ NOTE_INRAM | ASN_INTEGER, (unsigned long)&g_sysUpTime }, 
		{ NOTE_ISRDONLY | NOTE_BYFUNC | ASN_INTEGER, (unsigned long)SYSUPTIME }, 
		{ NOTE_INFLASH | ASN_OCTETSTRING, (unsigned long)&g_sysContact }, 	
		{ NOTE_INFLASH | ASN_OCTETSTRING, (unsigned long)&g_sysName }, 
		{ NOTE_INFLASH | ASN_OCTETSTRING, (unsigned long)&g_sysLocation }, 
		{ NOTE_ISRDONLY | ASN_INTEGER, (unsigned long)&g_sysServices},
	};
//
//
//
#define SYSTABIND				9
#define ENTERPRISESTABIND		8
#define OBJECTSTABIND			7
#define PRODUCTINFOTABIND		6
#define PM128TABIND				5
#define MKATABIND				4
#define MKCTABIND				3
#define MUTABIND				2
#define UKCSTABIND				1
#define UKCSINFO				0
const static AsnObjectId g_OidBypass[10] = 
{
// sitall_UKCS_Information
	{10, {0x2b,0x06,0x01,0x04,0x01,0x83,0xa8, 0x31, 2, 1} },
// sitall_UKCS
	{10, {0x2b,0x06,0x01,0x04,0x01,0x83,0xa8, 0x31, 2, 2} },
// sitall_MU
	{10, {0x2b,0x06,0x01,0x04,0x01,0x83,0xa8, 0x31, 2, 3} },
// sitall_MKC
	{10, {0x2b,0x06,0x01,0x04,0x01,0x83,0xa8, 0x31, 2, 4} },
// sitall_MKA
	{10, {0x2b,0x06,0x01,0x04,0x01,0x83,0xa8, 0x31, 2, 5} },
// sitall_PM128
	{10, {0x2b,0x06,0x01,0x04,0x01,0x83,0xa8, 0x31, 2, 6} },
// sitallProductInfo
	{9, {0x2b,0x06,0x01,0x04,0x01,0x83,0xa8, 0x31, 1} },
// sitallObjects
	{9, {0x2b,0x06,0x01,0x04,0x01,0x83,0xa8, 0x31, 2} },
// SitallEnterprises
	{8, {0x2b,0x06,0x01,0x04,0x01,0x83,0xa8, 0x31} },
//static unsigned char arraySYSTEM[]=		{0x06,0x2b,0x06,0x01,0x02,0x01,0x01};
	{6, {0x2b,0x06,0x01,0x02,0x01,0x01} }
};

//
#endif	// #ifndef MIBDATA_H_INCLUDED
