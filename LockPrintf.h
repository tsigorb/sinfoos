#ifndef __LOCKPRINTF__
#define __LOCKPRINTF__

#include "..\FreeRTOS/FreeRTOS.h"
#include "..\FreeRTOS/queue.h"
#include "..\FreeRTOS/task.h"

inline void LockPrintf(void)
{
 vPortEnableSwitchTask(false);
}

inline void UnlockPrintf(void)
{
  vPortEnableSwitchTask(true);
}
#endif

