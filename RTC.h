#include <time.h>

// ��������� ��������� ����� ��������� �������
void InitRTC(void);

// ��������� ����� ��������� �������
void SetClock(time_t tt);

// ������ ����� ��������� �������
time_t ReadClock(void);

void StoreDtTm2Buf(unsigned char *pbtBuf,struct tm *ptime);

// ������ ������ strDtTm ������ ���� >21 �����
// ���� strDtTm==NULL, �� ������������ ���������� ����� �������
char *ConvertCurrentDTTM2Str(char *strDtTm);

// ������ ������ strDtTm ������ ���� >10 ����
// ���� strTm==NULL, �� ������������ ���������� ����� �������
char *ConvertCurrentTM2Str(char *strTm);

