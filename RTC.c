#include <cdefBF533.h>
#include <time.h>
#include <stdio.h> 
#include "../FreeRTOS/FreeRTOS.h"
#include "../FreeRTOS/task.h"

#define CLEAR_ALL_IRQS 0x807F   // ����� ���� RTC ����������

///////////////////////////////////////////
unsigned char IsCurrentTickCountLT(unsigned int dwTick0);

///////////////////////////////////////////
extern time_t Time_Sys; 
extern unsigned long TimeSet;

void InitRTC(void)
{
#ifndef DIGITAL_STUDIO_CONTROLLER
  portTickType xTimeToWake;

  vPortEnableSwitchTask(false);
  *pRTC_PREN=1; // ���������� ������������ 1 Hz RTC
  ssync();
  xTimeToWake = xTaskGetTickCount()+5;
  while(!(*pRTC_ISTAT & 0x8000) && IsCurrentTickCountLT(xTimeToWake)) ssync();
  *pRTC_ISTAT=CLEAR_ALL_IRQS; // ����� ������
  vPortEnableSwitchTask(true);
#endif
}
 
void SetClock(time_t tt)
{
  vPortEnableSwitchTask(false);
  Time_Sys=tt; TimeSet=0; // ���������� �������
#ifndef DIGITAL_STUDIO_CONTROLLER
  unsigned int dwTOut,dw86400,dw3600;

  dw86400=tt % 86400;
  dw3600=dw86400 % 3600;
  tt=(dw3600%60) | ((dw3600/60) << 6) | ((dw86400/3600) << 12) | ((tt/86400) << 17);
  *pRTC_STAT=tt;
  ssync();
  dwTOut=xTaskGetTickCount()+5; 
  while(!(*pRTC_ISTAT & 0x8000) && IsCurrentTickCountLT(dwTOut)) ssync();
  *pRTC_ISTAT = CLEAR_ALL_IRQS; // ����� ������
  ssync();
#endif
  vPortEnableSwitchTask(true);
}

time_t ReadClock(void)
{
  time_t tt;

#ifdef DIGITAL_STUDIO_CONTROLLER
  tt=Time_Sys;
#else
  vPortEnableSwitchTask(false);
  tt=*pRTC_STAT; // ������ ������� �� ��������
  ssync();
  vPortEnableSwitchTask(true);
  tt=(tt & 0x3f)+((tt & 0xfc0) >> 6)*60 + ((tt & 0x1f000) >> 12)*3600 + ((tt & 0xfffe0000) >> 17)*86400;
#endif
  return tt; // ����� � ��������
}

void StoreDtTm2Buf(unsigned char *pbtBuf,struct tm *ptime)
{
  pbtBuf[0]=(unsigned char)ptime->tm_year-100;
  pbtBuf[1]=(unsigned char)ptime->tm_mon+1;
  pbtBuf[2]=(unsigned char)ptime->tm_mday;
  pbtBuf[3]=(unsigned char)ptime->tm_hour;
  pbtBuf[4]=(unsigned char)ptime->tm_min;
  pbtBuf[5]=(unsigned char)ptime->tm_sec;
  pbtBuf[6]=(unsigned char)ptime->tm_wday;
}

char *ConvertCurrentDTTM2Str(char *strDtTm)
{
  time_t tt;
  struct tm *ptime;
  static char Buf[80];

  if(!strDtTm) strDtTm=Buf;
  tt=ReadClock();
  vPortEnableSwitchTask(false);
  ptime=gmtime(&tt);
  sprintf(strDtTm,"%04d-%02d-%02d %02d:%02d:%02d",
          ptime->tm_year+1900,ptime->tm_mon+1,ptime->tm_mday,ptime->tm_hour,ptime->tm_min,ptime->tm_sec);
  vPortEnableSwitchTask(true);
  return(strDtTm);
}

char *ConvertCurrentTM2Str(char *strTm)
{
  time_t tt;
  struct tm *ptime;
  static char Buf[20];

  if(!strTm) strTm=Buf;
  tt=ReadClock();
  vPortEnableSwitchTask(false);
  ptime=gmtime(&tt);
  sprintf(strTm,"%02d:%02d:%02d",
          ptime->tm_hour,ptime->tm_min,ptime->tm_sec);
  vPortEnableSwitchTask(true);
  return(strTm);
}

