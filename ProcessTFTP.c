#ifdef DIGITAL_STUDIO_CONTROLLER
  #include ".\DigitalStudioController\Sinfo.h"
#else
  #include "Sinfo.h"
#endif
#include "GlobalData.h"

/////////////////////////////////////////////////////////////////////
#define TFTPPackSize    128
#define d_TOutTFTP      2000

// ���� ������ TFTP:
#define d_TFTP_FileNotFound     1   // ���� �� ������
#define d_TFTP_CreateFileErr    2   // ������ �������� �����
#define d_TFTP_ReadFileErr      3   // ���� ��� ������ ����������
#define d_TFTP_WriteFileErr     4   // ���� ��� ������ ����������
#define d_TFTP_Busy             5   // TFTP �����
#define d_TFTP_CmdErr           6   // ������������� ������� TFTP
#define d_TFTP_TOut             7   // ����-��� TFTP

/////////////////////////////////////////////////////////////////////
typedef struct TFTPData
{ // ��������� �������� ������ ������ ��� ������ �������
  unsigned short Mode;    // ������� ����� ������
  unsigned short BlockNum;// ����� ����������� (���������� �����)
  unsigned int Timer;     // ����� ���������� ��������� ��������
  unsigned short wPort;
  unsigned int dwAddr;
  char FileName[FileNameLEN];
} sTFTPData;

/////////////////////////////////////////////////////////////////////
void vAnswerTFTP(sInfoKadr *psInfo,int aSize,char *TFTPanswer)
{
  psInfo->L=aSize;
  memcpy(psInfo->ByteInfoKadr,TFTPanswer,aSize);
  xQueueSend(xQueueReply[psInfo->Source],psInfo,0,0);
}

void TFTP_SendError(sInfoKadr *psInfo,unsigned char btErrCode,sTFTPData *psTFTP)
{
  static sInfoKadr sInfo;
  if(!psInfo)
  {
    if(psTFTP->dwAddr && psTFTP->wPort)
    {
      psInfo=&sInfo;
      psInfo->Source=UserUDP;
      psInfo->A1=psTFTP->dwAddr;
      psInfo->PORT1=psTFTP->wPort;
    }
    else return;
  }
  psInfo->L=3;
  psInfo->ByteInfoKadr[0]=13;
  psInfo->ByteInfoKadr[1]=5;
  psInfo->ByteInfoKadr[2]=btErrCode;
  xQueueSend(xQueueReply[psInfo->Source],psInfo,0,0);
SendStr2IpLogger("$ERROR$TFTP_SendError: Error code=%d, sTFTP Mode=%d, Addr=0x%08x, Port=%d",
                 btErrCode,psTFTP->Mode,psInfo->A1,psInfo->PORT1);
}

void TFTPListFiles(sInfoKadr *psInfo,char *TFTPanswer,sTFTPData *psTFTP)
{
  unsigned int lIndex;
  struct tm *ttt;
  time_t t1;
  MYFILE stFile1;

  psTFTP->Mode=10;
  if(FindNextFile(psTFTP->BlockNum,&stFile1)!=(unsigned long)-1)
  {
    TFTPanswer[1]=11;
    TFTPanswer[2]=psTFTP->BlockNum / 256;
    TFTPanswer[3]=psTFTP->BlockNum % 256;
    memcpy(TFTPanswer+4,stFile1.szFileName,strlen(stFile1.szFileName));
    lIndex=3+strlen(stFile1.szFileName)+1;
    TFTPanswer[lIndex]=32;
    lIndex++;
    LockPrintf();
    sprintf(TFTPanswer+lIndex,"%6d",stFile1.FileLen);
    TFTPanswer[lIndex-1]=0;
    lIndex+=7;
    t1=stFile1.FileTime;
    ttt=gmtime(&t1);
    sprintf(TFTPanswer+lIndex,"%2.2d:%2.2d %2.2d.%2.2d.%4.4d",
            ttt->tm_hour,ttt->tm_min,ttt->tm_mday,ttt->tm_mon+1,ttt->tm_year+1900);
    lIndex +=17;
    TFTPanswer[lIndex-1]=0;
    sprintf(TFTPanswer+lIndex,"%6x",stFile1.AddrFile);
    UnlockPrintf();
    lIndex+=6;
    vAnswerTFTP(psInfo,lIndex,TFTPanswer);
    psTFTP->Timer=xTaskGetTickCount()+d_TOutTFTP;
  }
  else
  { // ������ ������ ����
    TFTPanswer[1]=11;
    TFTPanswer[2]=psTFTP->BlockNum / 256;
    TFTPanswer[3]=psTFTP->BlockNum % 256;
    TFTPanswer[4]=0;
    TFTPanswer[5]=0;
    vAnswerTFTP(psInfo,6, TFTPanswer);
    psTFTP->Mode=0;
  }
}

// ��������� ������ ������ ������� ������� �������
void vProcessJornal(sInfoKadr *psInfo,int aSize, unsigned char *BufCommand)
{
#ifndef DIGITAL_STUDIO_CONTROLLER
  int i,j,cnt,iSz;
  char *pStr;
  static char answer[6+sizeof(LOG_RECORD)+d_LOG_STRINFO_LEN];
  LOG_RECORD *psLogRec;

  switch(BufCommand[0]) 
  {
    case 1: // ������ ������ �������
      j=BufCommand[4];
      j=(j << 8) | BufCommand[3];
      j=(j << 8) | BufCommand[2];
      j=(j << 8) | BufCommand[1];
      if(!j) j=GetLogRecordsCount();
      if(j)
      {
        j--;
        psLogRec=(LOG_RECORD *)(answer+6);
        pStr=(char *)(psLogRec+1);
        for(i=j,cnt=0;(i>=0) && (cnt<50);i--)
        {
          cnt++;
          answer[0]= 71;
          answer[1]= 1;
          answer[2]= i;
          answer[3]= i >> 8;
          answer[4]= i >> 16;
          answer[5]= i >> 24;
          PeekLogRecord(i,psLogRec,pStr);
          iSz=6+sizeof(LOG_RECORD);
          if(psLogRec->message==0xffff) iSz+=strlen(pStr)+1;
          vAnswerTFTP(psInfo,iSz,answer);
        }
      }
      else
      {
          answer[0]= 71;
          answer[1]= 1;
          answer[2]= 0;
          answer[3]= 0;
          answer[4]= 0;
          answer[5]= 0;
          memset(psLogRec,0,sizeof(LOG_RECORD));
          vAnswerTFTP(psInfo,6+sizeof(LOG_RECORD),answer);
      }
      break;
    case 3: // ������� �������
      ClearLogs();
      break;
    case 4: // ������ ���������� ������� � �������
      i=GetLogRecordsCount();
      answer[0]= 71;
      answer[1]= 4;
      answer[2]= i;
      answer[3]= i >> 8;
      answer[4]= i >> 16;
      answer[5]= i >> 24;
      vAnswerTFTP(psInfo,6,answer);
      break;
  }
#endif
}

// ����� ������� � �����������
void vProcessTFTP(sInfoKadr *psInfo,unsigned char *BufCommand,int aSize)
{
  int i;
  unsigned int dwSize,dwPos;
  unsigned short wInfo;
  static unsigned int dwTOut=0;
  static char FileNameOld[FileNameLEN],FileNameNew[FileNameLEN];
  static char TFTPanswer[TFTPPackSize+12];
  static sTFTPData sTFTP={0,};
  static HANDLE s_hFile=INVALID_HANDLE_VALUE;
  
  if(!psInfo)
  { // ������ ������������� ������� �������� ������ � ����
    if(sTFTP.Mode)
    {
      if(IsCurrentTickCountGT(sTFTP.Timer))
      {
        TFTP_SendError(psInfo,d_TFTP_TOut,&sTFTP);
        memset(&sTFTP,0x00,sizeof(sTFTPData));
        if(s_hFile!=INVALID_HANDLE_VALUE)
        { CloseHandle(s_hFile); s_hFile=INVALID_HANDLE_VALUE; }
      }
    }
    return;
  }
  if(sTFTP.Mode)
  {
    if((sTFTP.dwAddr!=psInfo->A1) || (sTFTP.wPort!=psInfo->PORT1))
    {
      TFTP_SendError(psInfo,d_TFTP_Busy,&sTFTP);
      return;
    }
  }
  TFTPanswer[0]=13;
  switch(BufCommand[0])
  {
    case 1: // ������ ������ �����
      if(sTFTP.Mode && (sTFTP.Mode!=1))
      { TFTP_SendError(psInfo,d_TFTP_Busy,&sTFTP); return; }
      else CloseHandle(s_hFile);
      memset(&sTFTP, 0x00, sizeof(sTFTPData)); 
      strncpy(sTFTP.FileName,(char *)BufCommand+1,FileNameLEN);
      sTFTP.FileName[FileNameLEN-1]=0;
      s_hFile=CreateFile(sTFTP.FileName, GENERIC_READ, 0);
      if(s_hFile!=INVALID_HANDLE_VALUE)
      {
        TFTPanswer[1]=3;
        TFTPanswer[2]=sTFTP.BlockNum >> 8;
        TFTPanswer[3]=sTFTP.BlockNum & 0xff;
        i=ReadFile(s_hFile,TFTPanswer+4, TFTPPackSize);
        if(i>=TFTPPackSize)
        {
          sTFTP.Mode=1;
          sTFTP.dwAddr=psInfo->A1;
          sTFTP.wPort=psInfo->PORT1;
        }
        else 
        { CloseHandle(s_hFile); s_hFile=INVALID_HANDLE_VALUE; }
        sTFTP.Timer=xTaskGetTickCount()+d_TOutTFTP;
        vAnswerTFTP(psInfo,4+i,TFTPanswer);
      }
      else TFTP_SendError(psInfo,d_TFTP_FileNotFound,&sTFTP);
      break;
    case 2: // ������ ������ �����
      if(sTFTP.Mode && (sTFTP.Mode!=2))
      { TFTP_SendError(psInfo,d_TFTP_Busy,&sTFTP); return; }
      else CloseHandle(s_hFile);
      memset(&sTFTP,0x00,sizeof(sTFTPData)); 
      strncpy(sTFTP.FileName,(char *)BufCommand+1,FileNameLEN);
      sTFTP.FileName[FileNameLEN-1]=0;
      i=strlen(sTFTP.FileName)+2;
      dwSize=BufCommand[i];
      dwSize=(dwSize << 8) | BufCommand[i+1];
      dwSize=(dwSize << 8) | BufCommand[i+2];
      dwSize=(dwSize << 8) | BufCommand[i+3];
      s_hFile=CreateFile(sTFTP.FileName,CREATE_ALWAYS,dwSize);
      if(s_hFile>=0)
      {
        TFTPanswer[1]=4;
        TFTPanswer[2]=sTFTP.BlockNum >> 8;
        TFTPanswer[3]=sTFTP.BlockNum & 0xff;
        vAnswerTFTP(psInfo,4,TFTPanswer);
        sTFTP.Mode=2;
        sTFTP.Timer=xTaskGetTickCount()+d_TOutTFTP;
        sTFTP.dwAddr=psInfo->A1;
        sTFTP.wPort=psInfo->PORT1;
      }
      else TFTP_SendError(psInfo,d_TFTP_CreateFileErr,&sTFTP);
      break;
    case 3: // ����������� ������ �����
      sTFTP.BlockNum=BufCommand[1];
      sTFTP.BlockNum=(sTFTP.BlockNum << 8) + BufCommand[2];
      if((sTFTP.Mode!=2) || (!sTFTP.BlockNum)) // DspCom >=6
      {
        TFTP_SendError(psInfo,d_TFTP_CmdErr,&sTFTP);
        return;
      }
      aSize-=4; 
      dwPos=sTFTP.BlockNum-1;     // DspCom >=6
      dwPos*=TFTPPackSize;
      if(SetFilePointer(s_hFile,dwPos,FILE_BEGIN)!=dwPos) i=-1;
      else
      {
        i=WriteFile(s_hFile,BufCommand+3,aSize);
        if(i || (aSize==0))
        {
          if(aSize<TFTPPackSize)
          {
            sTFTP.Mode=0;
            CloseHandle(s_hFile);
            s_hFile=INVALID_HANDLE_VALUE;
          }
          TFTPanswer[1]=4;
          TFTPanswer[2]=sTFTP.BlockNum >> 8;
          TFTPanswer[3]=sTFTP.BlockNum & 0xff;
          vAnswerTFTP(psInfo,4,TFTPanswer);
          sTFTP.Timer=xTaskGetTickCount()+d_TOutTFTP;
        }
        else i=-1;
      }
      if(i<0)
      {
        CloseHandle(s_hFile);
        s_hFile=INVALID_HANDLE_VALUE;
        TFTP_SendError(psInfo,d_TFTP_WriteFileErr,&sTFTP);
        sTFTP.Mode=0;
      }
      break;
    case 4: // ������������� ������
      if(sTFTP.Mode==1)
      { // ����������� ������ �����
        sTFTP.BlockNum=BufCommand[1];
        sTFTP.BlockNum=(sTFTP.BlockNum << 8) + BufCommand[2];
        dwPos=sTFTP.BlockNum;
        dwPos*=TFTPPackSize;
        if(SetFilePointer(s_hFile,dwPos,FILE_BEGIN)!=dwPos) i=-1;
        else
        {
          i=ReadFile(s_hFile,TFTPanswer+4,TFTPPackSize);
          if(i>=0)
          {
            TFTPanswer[1]=3;
            TFTPanswer[2]=sTFTP.BlockNum >> 8;
            TFTPanswer[3]=sTFTP.BlockNum & 0xff;
            if(i<TFTPPackSize)
            {
              sTFTP.Mode=0;
              CloseHandle(s_hFile);
              s_hFile=INVALID_HANDLE_VALUE;
            }
            vAnswerTFTP(psInfo,4+i,TFTPanswer);
            sTFTP.Timer=xTaskGetTickCount()+d_TOutTFTP;
          }
          else i=-1;
        }
        if(i<0)
        { // ������ ������ �����
          TFTP_SendError(psInfo,d_TFTP_ReadFileErr,&sTFTP);
          sTFTP.Mode=0;
          CloseHandle(s_hFile);
          s_hFile=INVALID_HANDLE_VALUE;
        }
      }
      else
      {
        if(sTFTP.Mode==10)
        { // ����������� �������� ������ ������
          sTFTP.BlockNum=(BufCommand[1]*256)+BufCommand[2];       // DspCom >=6
          TFTPListFiles(psInfo,TFTPanswer,&sTFTP);
        }
        else
        {
SendStr2IpLogger("$ERROR$vProcessTFTP: Error Cmd=4, sTFTP.Mode=%d",sTFTP.Mode);
          TFTP_SendError(psInfo,d_TFTP_CmdErr,&sTFTP);
        }
      }
      break;
    case 10: // ������ ������ ������
      if(sTFTP.Mode && (sTFTP.Mode!=10))
      { TFTP_SendError(psInfo,d_TFTP_Busy,&sTFTP); return; }
      sTFTP.dwAddr=psInfo->A1;
      sTFTP.wPort=psInfo->PORT1;
      sTFTP.BlockNum=0;
      TFTPListFiles(psInfo,TFTPanswer,&sTFTP);
      break;
    case 12: // �������� ����� (������ �������)
        TFTPanswer[1]=4;
        TFTPanswer[2]=0;
        TFTPanswer[3]=0;
        vAnswerTFTP(psInfo,4, TFTPanswer);
        break;
    case 112: // �������� ����� (����� �������)
      if(sTFTP.Mode)
      { TFTP_SendError(psInfo,d_TFTP_Busy,&sTFTP); return; }
      strncpy(sTFTP.FileName,(char *)BufCommand+1,FileNameLEN-1);
      sTFTP.FileName[FileNameLEN-1]=0;
      if(DeleteFile(sTFTP.FileName))
      {
        TFTPanswer[1]=4;
        TFTPanswer[2]=0;
        TFTPanswer[3]=0;
        vAnswerTFTP(psInfo,4,TFTPanswer);
      }
      else TFTP_SendError(psInfo,d_TFTP_FileNotFound,&sTFTP);
      break;
    case 13: // �������������� �������� �������
      if(sTFTP.Mode)
      { TFTP_SendError(psInfo,d_TFTP_Busy,&sTFTP); return; }
      TFTPanswer[1]=4;
      TFTPanswer[2]=0;
      TFTPanswer[3]=0;
      vAnswerTFTP(psInfo,4,TFTPanswer);
      vTaskDelay(200); 
      FormatFileSystem();
      vAnswerTFTP(psInfo,4,TFTPanswer); 
      break;
    case 15: // �������������� �����
      if(sTFTP.Mode)
      { TFTP_SendError(psInfo,d_TFTP_Busy,&sTFTP); return; }
      memset(FileNameOld,0,FileNameLEN);
      memset(FileNameNew,0,FileNameLEN);
      strncpy(FileNameOld,(char *)BufCommand+1,FileNameLEN);
      i=aSize-2-strlen(FileNameOld); // ������� �������� � �������
      if(i>FileNameLEN) i=FileNameLEN;
      strncpy(FileNameNew,(char *)BufCommand+2+strlen(FileNameOld),i);
      FileNameNew[FileNameLEN-1]=0;
      if(FileNameNew[0]!=0) i=RenameFile(FileNameOld,FileNameNew);
      else i=0;
      if(i==1)
      { // �������������� ���������
        TFTPanswer[1]=4;
        TFTPanswer[2]=0;
        TFTPanswer[3]=0;
        vAnswerTFTP(psInfo,4, TFTPanswer);
      }
      else
      {
        if(i) TFTP_SendError(psInfo,d_TFTP_FileNotFound,&sTFTP);
        else TFTP_SendError(psInfo,d_TFTP_WriteFileErr,&sTFTP);
      }
      break;
    case 16: // ��������� ������� ������
      if(sTFTP.Mode)
      { TFTP_SendError(psInfo,d_TFTP_Busy,&sTFTP); return; }
      dwSize=GetFreeSpace(BufCommand[1]);
      TFTPanswer[1]=16;
      TFTPanswer[2]=BufCommand[1];
      TFTPanswer[3]=(unsigned char)dwSize; // ������� ������
      TFTPanswer[4]=(unsigned char)(dwSize >> 8);
      TFTPanswer[5]=(unsigned char)(dwSize >> 16);
      TFTPanswer[6]=(unsigned char)(dwSize >> 24);
      vAnswerTFTP(psInfo,7, TFTPanswer);
      break;
    case 17: // ������ ����������� �����
      if(sTFTP.Mode)
      { TFTP_SendError(psInfo,d_TFTP_Busy,&sTFTP); return; }
      wInfo=0;
      if(ReadCRC((char *)BufCommand+1,&wInfo))
      { // ���� ������
        TFTPanswer[1]=17;
        TFTPanswer[2]=wInfo; // ����������� ����� (������� �����)
        vAnswerTFTP(psInfo,3,TFTPanswer);
      }
      else TFTP_SendError(psInfo,d_TFTP_FileNotFound,&sTFTP);
      break;
    case 18: // �������������� ������ ���������� ����� � �������� �������
      if(sTFTP.Mode)
      { TFTP_SendError(psInfo,d_TFTP_Busy,&sTFTP); return; }
      FindLastFile();
      TFTPanswer[1]=4;
      TFTPanswer[2]=0;
      TFTPanswer[3]=0;
      vAnswerTFTP(psInfo,4,TFTPanswer);
      break;
    case 19: // ���������� ����� �����
      if(sTFTP.Mode)
      { TFTP_SendError(psInfo,d_TFTP_Busy,&sTFTP); return; }
      memset(&sTFTP, 0x00, sizeof(sTFTPData)); 
      strncpy(sTFTP.FileName,(char *)BufCommand+1,FileNameLEN);
      sTFTP.FileName[FileNameLEN-1]=0;
      s_hFile=CreateFile(sTFTP.FileName, GENERIC_READ, 0);
      if(s_hFile==INVALID_HANDLE_VALUE)
      { TFTP_SendError(psInfo,d_TFTP_FileNotFound,&sTFTP); return; }
      dwSize=GetFileSize(s_hFile);
      CloseHandle(s_hFile);
      TFTPanswer[1]=19;
      TFTPanswer[2]=dwSize >> 24;
      TFTPanswer[3]=dwSize >> 16;
      TFTPanswer[4]=dwSize >> 8;
      TFTPanswer[5]=dwSize & 0xff;
      vAnswerTFTP(psInfo,6,TFTPanswer);
      break;
    default:
SendStr2IpLogger("$ERROR$vProcessTFTP: Error unknown cmd=%d",BufCommand[0]);
      TFTP_SendError(psInfo,d_TFTP_CmdErr,&sTFTP);
      break;
  }
}

void vTFTPTask(void *pvParameters)
{
  static sCommand sCmd;
  static sInfoKadr sInfo;

  while(1)
  {
    if(xQueueReceive(xQueueTFTP,&sCmd,0)) 
    {
      sInfo.Source=sCmd.Source;
      sInfo.A1=sCmd.A1;
      sInfo.PORT1=sCmd.PORT1;
      vProcessTFTP(&sInfo,sCmd.BufCommand+1,sCmd.L);
    }
    vProcessTFTP(NULL,0,0); // ������ ������������� ������� �������� ������ � ���� �� TFTP
    SetWDState(d_WDSTATE_TFTP);
    vTaskDelay(10);
  }
}

