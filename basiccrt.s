/////////////////////////////////////////////////////////////////
// !!��� ��������� ���� �������������, ������� �� ������������ //
// ������ �-��� main() � ������ �����������, �.�. ��������� �� //
// ����������� ����������� �����������, ������� ��������� ADSP //
// � ����� ����������� ����� �������� ��������� ��!!           //
/////////////////////////////////////////////////////////////////

// blackfin-edinburgh-core
#include <Def_LPBlackfin.h>
#include <sys/platform.h>

/////////////////////////////////////////////////////////////////
// standard
#define IVBh                (EVT0 >> 16)
#define IVBl                (EVT0 & 0xFFFF)
#define UNASSIGNED_VAL      0x8181
#define INTERRUPT_BITS      0x400 // just IVG15
#define SYSCFG_VALUE        0x30

	.section/DOUBLEANY program;
	.align 2;

start:
	// Initialise the Event Vector table.
	P0.H = IVBh;
	P0.L = IVBl;

	// Install __unknown_exception_occurred in EVT so that 
	// there is defined behaviour.
	P0 += 2*4;     // Skip Emulation and Reset
	P1 = 13;
	R1.L = __unknown_exception_occurred;
	R1.H = __unknown_exception_occurred;
	LSETUP (.ivt,.ivt) LC0 = P1;
.ivt:	[P0++] = R1;

	// Initialise the stack.
	// Note: this points just past the end of the section.
	// First write should be with [--SP].
	SP.L=ldf_stack_end;
	SP.H=ldf_stack_end;
	usp = sp;

	// We're still in supervisor mode at the moment, so the FP
	// needs to point to the supervisor stack.
	FP = SP;

	// And make space for incoming "parameters" for functions
	// we call from here:
	SP += -12;

	R0 = INTERRUPT_BITS;
	R0 <<= 5;  // Bits 0-4 not settable.
	CALL.X __install_default_handlers;

	R1 = SYSCFG;
	R4 = R0;		// Save modified list for later
	BITSET(R1,1);
	SYSCFG = R1;	// Enable the cycle counter

	R0.L = UNASSIGNED_VAL;
	R0.H = UNASSIGNED_VAL;
	// Push a RETS and Old FP onto the stack, for sanity.
	[--SP]=R0;
	[--SP]=R0;
	// Make sure the FP is sensible.
	FP = SP;
	// And leave space for incoming "parameters"
	SP += -12;

	// At long last, call the application program.
	CALL.X _main;

.idle:
    JUMP .idle;

/////////////////////////////////////////////////////////////////
/*
toogle_wd_always:
    P0.L=FIO_DIR & 0xffff;
    P0.H=FIO_DIR >> 16;
    R0.L=W[P0];
    R1=PF0;
    R0=R0 | R1;
    W[P0]=R0.L;
    ssync;
    P0.L=FIO_FLAG_S & 0xffff;
    P0.H=FIO_FLAG_S >> 16;
    P1.L=FIO_FLAG_C & 0xffff;
    P1.H=FIO_FLAG_C >> 16;
    R0.L=PF0;
  while1:
      P2=4;
      LSETUP (.wds_s,.wds_e) LC0 = P2;
        .wds_s: W[P0] = R1;
        .wds_e: ssync;
      P2=0xff;
      LSETUP (.wdc_s,.wdc_e) LC0 = P2;
        .wdc_s: W[P1] = R1;
        .wdc_e: ssync;
    JUMP while1;
*/

/////////////////////////////////////////////////////////////////
// standard
.start.end:        // To keep the linker happy.

.global start;
.type start,STT_FUNC;
.global .start.end;
.type .start.end,STT_FUNC;
.extern _main;
.type _main,STT_FUNC;
.extern ldf_stack_end;
.extern __unknown_exception_occurred;
.type __unknown_exception_occurred,STT_FUNC;
.extern __install_default_handlers;
.type __install_default_handlers,STT_FUNC;

