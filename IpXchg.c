#include "..\Sinfo.h"
#include "FunctionsPrototypes.h"
#include "ViewCommandFunc.h"
#include "GlobalData.h"

#ifdef __TCP__
#include "html_pages.h"
#endif

#ifdef __SNMP__
#include "..\SNMP\Snmp.h"
#endif

#ifdef __ISDN__
  #include "..\EDSS\pri_debug.h"
  #include "..\EDSS\pri_channel.h"
  #ifdef __ISDN_DEBUG__
    volatile int is_tcpip_started=0;
  #endif
#endif

/////////////////////////////////////////////////////////////////////
#define dIpTOutFirst        4000ul
#define dIpTOutNext         5000ul

/////////////////////////////////////////////////////////////////////
void LedWorkIndicator(void);
unsigned short RdFlash(unsigned short AdrFl);
bool WrFlash(unsigned short DanFl,unsigned short AdrFl);
void CheckIpSendTime(void);

bool GetSoundPort4IpPult(unsigned int dwIp,unsigned short *pwPort);

bool IsW6100(void);

#ifdef SINFO_ENERGO
void RecieveAudioPacket(sInfoKadr *psInfo,unsigned char *pbtAudio);
#endif

/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
IPPARAMS_TP g_IpParams=
{
{ 192, 168, 1, 32 }, // IP address
{ 255, 255, 255, 0 }, // Subnet mask
{ 0, 0, 0, 0 }, // Gateway address
{ 0, 8, 0xDC, 0, 0, 0 }, // MAC address - DO NOT USE THIS ON A PUBLIC NETWORK!
0xFFFF, // �������� ���� ��� ������������ �� ������� 4 �����
161, // wPortSNMP
5002, // wPortSound
4000, // wPortControl
80, // wPortHTTP
{ 0, 0, 0, 0 }, // IP address SNMP manager ��������
{ 0, 0, 0, 0 }, // IP address SNMP manager ���������
{ 0, 0, 0, 0 }, // IP address ������� �����������
{ 0, 0, 5, 0, 0, 0 }, // VLANID ��� ������ 0-5 Marvell
{ 0, 0, 0, 0, 0, 0 } // VLAN ��������� ��� ������ 0-5 Marvell
};

static unsigned char TcpUdpBuf[MAX_BUF_SIZE];

volatile static unsigned int s_dwIpTOut=0;
volatile static unsigned short s_wIpReinitSeq=0,s_wIpReinit=0;
volatile static bool s_bNeedSendAboutReinit=false;

volatile static unsigned char s_btCnt4Cmd=0,
                              s_btCnt4Rec=0,
                              s_btCnt4Snd=0;

int g_iPultPingPong[NumberKeyboard];

/////////////////////////////////////////////////////////////////////
portLONG WiznetSoftReset(void)
{
  g_IpParams.ucDataSHAR[5]=g_IpParams.ucDataSIPR[3];  // !!! ��������� ���� ���_������==���������� ����� IP-������ !!!
  initWiznet((uint8 *)g_IpParams.ucDataSHAR,(uint8 *)g_IpParams.ucDataGAR,(uint8 *)g_IpParams.ucDataMSR,(uint8 *)g_IpParams.ucDataSIPR);
  return (portLONG)pdPASS;
}

#ifdef __SNMP__
void CreateSNMPFiles(void)
{
  HANDLE hFile;
  MYFILE stFile1;

  if(FindFile("PwsRO.txt",&stFile1)== (unsigned)-1)
  {
    hFile = CreateFile("PwsRO.txt",CREATE_ALWAYS,6);
    if(hFile >= 0)
    {
      WriteFile(hFile,"public", 6);
      CloseHandle(hFile);
    }
  }
  if(FindFile("PwsRW.txt", &stFile1)== (unsigned)-1)
  {
    hFile = CreateFile("PwsRW.txt", CREATE_ALWAYS, 7);
    if(hFile >= 0)
    {
      WriteFile(hFile,"private",7);
      CloseHandle(hFile);
    }
  }
}
#endif

void WriteBuf2Flash(unsigned short Addr2Flash,unsigned short *Buf,unsigned char Sz)
{
  unsigned char i;
  for(i=0;i<Sz;i+=2) WrFlash(*Buf++,Addr2Flash+i); 
}
  
void ReadFlash2Buf(unsigned short *Buf,unsigned short Addr2Flash,unsigned char Sz)
{
  unsigned char i;
  for(i=0;i<Sz;i+=2) *Buf++=RdFlash(Addr2Flash+i);
}

//-----------------------------------------------------------
unsigned short GetWiznetReinitCounter(void)
{ return(s_wIpReinit); }

void SendIpReinitCount2IpPult(unsigned char SN,sInfoKadr *psInfo)
{ // ���������� ���������� ����������������� Wiznet
  psInfo->L=4;
  psInfo->ByteInfoKadr[0]=112;
  psInfo->ByteInfoKadr[1]=SN;
  psInfo->ByteInfoKadr[2]=s_wIpReinit >> 8;
  psInfo->ByteInfoKadr[3]=s_wIpReinit;
  xQueueSend(xQueueReply[UserUDP],psInfo,0,0);
  s_bNeedSendAboutReinit=false;
}

void InitIP(void)
{ // ������������� ������
  unsigned int csum;

  LedToggle(ledUpr);
  // ��������� IP-������ �� Flash-������ ��� Default
  csum=RdFlash(TCPIPStack);
  if(csum!=0xFFFF)
  {
    if(*pFIO_FLAG_D & PF13) ReadFlash2Buf((unsigned short *)&g_IpParams,TCPIPStack,sizeof(g_IpParams));
  }
  else WriteBuf2Flash(TCPIPStack,(unsigned short *)&g_IpParams,sizeof(g_IpParams)); // ������ �������� �� ��������� �� ����

#ifdef __VLAN__
  ResetVLAN();
  if(*pFIO_FLAG_D & PF13)
  { // ���� �� default IP �����
    CheckVLAN((GT_U16 *)ucDataVLANID);
    SetVLANID((GT_U16 *)ucDataVLANID,(unsigned char *)ucDataVLANPri);
  }
#else
  if(RdFlash(MarvellInfo) != 0xFFFF) WrFlash(0xFFFF, MarvellInfo);
#endif

  WiznetSoftReset();

  socketUDP(CommandSocket,g_IpParams.wPortControl,0);
  socketUDP(SoundSocket,g_IpParams.wPortSound,0);
#ifdef SINFO_ENERGO
  socketUDP(RecordSocket,g_IpParams.wPortSound,0);
#endif
#ifdef __SNMP__
  CreateSNMPFiles();
  socketUDP(SNMP_SOCKET,g_IpParams.wPortSNMP,0);
#endif

  memset(g_iPultPingPong,0,sizeof(g_iPultPingPong));
  InitIRP();
  s_dwIpTOut=xTaskGetTickCount()+dIpTOutNext;
  if(s_wIpReinitSeq<4)
  {
    s_wIpReinitSeq++;
    s_wIpReinit++;
  }
  s_bNeedSendAboutReinit=true;
}

/////////////////////////////////////////////////////////////////////
void ReOpenSocket(unsigned char btSocket,unsigned short wPort,bool bSend)
{
  close(btSocket);
  vTaskDelay(5);
  switch(btSocket)
  {
    case CommandSocket:
      s_btCnt4Cmd=0;
      break;
    case RecordSocket:
      s_btCnt4Rec=0;
      break;
    case SoundSocket:
      s_btCnt4Snd=0;
      break;
  }
  socketUDP(btSocket,wPort,0);
  if(bSend) SendStr2IpLogger("$ERROR$Reopen send socket for port=%d",wPort);
}

#ifdef SINFO_ENERGO
void SendSound2AllRecordSystems(unsigned char btSN,unsigned char btIdDSRS,unsigned char btSrc,xListRTP *psSndIp)
{
  unsigned char btInd;
  static section ("bigdata") sSoundIp sSnd;

  sSnd.L=sizeof(xListRTP)+1;
  psSndIp->ProFile=btIdDSRS;
  psSndIp->ProFile=(psSndIp->ProFile << 8) | (btSN<<1) | btSrc; // btSrc==0 - from Pult, ==1 - UK
  sSnd.ByteInfo[0]=0;     // Type of packet (0-sound packet, 1-command packet)
  memcpy(sSnd.ByteInfo+1,psSndIp,sizeof(xListRTP));
  btInd=0;
  while(GetIpRecordAddr(btInd,&sSnd.A1,&sSnd.PORT1))
  {
    xQueueSend(xQueueSound4Rec,&sSnd,0,0);
    btInd++;
  }
}

void SendCmd2AllRecordSystems(unsigned short wLn,unsigned char *ByteInfo)
{
  unsigned char btInd;
  static section ("bigdata") sSoundIp sSnd;
  if(wLn)
  {
    sSnd.L=wLn+1;
    sSnd.ByteInfo[0]=1;     // Type of packet (0-sound packet, 1-command packet)
    memcpy(sSnd.ByteInfo+1,ByteInfo,wLn);
    btInd=0;
    while(GetIpRecordAddr(btInd,&sSnd.A1,&sSnd.PORT1))
    {
      xQueueSend(xQueueSound4Rec,&sSnd,0,0);
      btInd++;
    }
  }
}

void SendIpPacket4Record(void)
{
  unsigned short TcpUdpSize;
  static section ("bigdata") sSoundIp sSnd;

  if(!isSocketSendEnd(RecordSocket))
  {
    if(isSocketTOut(RecordSocket)) ReOpenSocket(RecordSocket,g_IpParams.wPortSound,true);
    return;
  }
  if(getsizeUDP(RecordSocket,true)<sizeof(sSoundIp))
  {
    if(s_btCnt4Rec<10) s_btCnt4Rec++;
    else ReOpenSocket(RecordSocket,g_IpParams.wPortSound,true);
    return;
  }
  s_btCnt4Rec=0;
  if(xQueueReceive(xQueueSound4Rec,&sSnd,0)==pdPASS)
  {
    if(sSnd.A1 && sSnd.PORT1 && sSnd.L)
    {
      TcpUdpSize=sSnd.L;
      if(TcpUdpSize>MAX_BUF_SIZE) TcpUdpSize=MAX_BUF_SIZE;
      sendto(RecordSocket,sSnd.ByteInfo,TcpUdpSize,(unsigned char *)(&sSnd.A1),sSnd.PORT1);
    }
  }
}
#endif

void SendIpCmdPacket(void)
{
  unsigned short i,TcpUdpSize;
  unsigned int csum;
  static section ("bigdata") sInfoKadr sInfo;

  if(!isSocketSendEnd(CommandSocket))
  {
    if(isSocketTOut(CommandSocket)) ReOpenSocket(CommandSocket,g_IpParams.wPortControl,true);
    return;
  }
  if(getsizeUDP(CommandSocket,true)<sizeof(sInfoKadr))
  {
    if(s_btCnt4Cmd<10) s_btCnt4Cmd++;
    else ReOpenSocket(CommandSocket,g_IpParams.wPortControl,true);
    return;
  }
  s_btCnt4Cmd=0;
  if(xQueueReceive(xQueueReply[UserUDP],&sInfo,0)==pdPASS)
  {
    if(sInfo.L && sInfo.A1 && sInfo.PORT1)
    {
      TcpUdpSize=sInfo.L;
      if(TcpUdpSize>MAX_BUF_SIZE-2) TcpUdpSize=MAX_BUF_SIZE-2;
      TcpUdpBuf[0]=0x16;
      for(i=1;i<=TcpUdpSize;i++) TcpUdpBuf[i]=sInfo.ByteInfoKadr[i-1];
      for(i=0,csum=0;i<=TcpUdpSize;i++) csum+=TcpUdpBuf[i];
      TcpUdpBuf[i]=csum & 0xFF;
      TcpUdpSize+=2;
      sendto(CommandSocket,TcpUdpBuf,TcpUdpSize,(unsigned char *)(&sInfo.A1),sInfo.PORT1);
/*{
  if(0x0a01a8c0==sInfo.A1)
  {
SendStr2IpLogger("$DEBUG$SendIpPacket: A=%08x, p=%d, size=%d",
                 sInfo.A1,sInfo.PORT1,TcpUdpSize);
  }
}*/

if((sInfo.ByteInfoKadr[0]==11) && (sInfo.ByteInfoKadr[1]==0xb4))
{
  unsigned char PultNb=sInfo.ByteInfoKadr[50];

  g_iPultPingPong[PultNb]--;
  if(g_iPultPingPong[PultNb]<-1)
  SendStr2IpLogger("$LIFE_INDIC$UK life indicator send for IP-pult SN=%d",sInfo.ByteInfoKadr[51]);
}
#ifdef SINFO_ENERGO
      if((TcpUdpBuf[1]==0xFC) || (TcpUdpBuf[1]==0xFD))
      { SendCmd2AllRecordSystems(TcpUdpSize,TcpUdpBuf); }
#endif
CheckIpSendTime();
    }
  }
}

void SendIpSoundPacket(void)
{
#ifdef SINFO_ENERGO
  unsigned char btSN,btId;
#endif
  unsigned short TcpUdpSize,wPortSnd;
  static section ("bigdata") sSoundIp sSnd;

  if(!isSocketSendEnd(SoundSocket))
  {
    if(isSocketTOut(SoundSocket)) ReOpenSocket(SoundSocket,g_IpParams.wPortSound,true);
    return;
  }
  if(getsizeUDP(SoundSocket,true)<sizeof(sSoundIp))
  {
    if(s_btCnt4Snd<10) s_btCnt4Snd++;
    else ReOpenSocket(SoundSocket,g_IpParams.wPortSound,true);
    return;
  }
  s_btCnt4Snd=0;
  if(xQueueReceive(xQueueSoundIpOut,&sSnd,0)==pdPASS)
  {
    if(sSnd.L && sSnd.A1 && sSnd.PORT1)
    {
//DataSave1[5]++;
      TcpUdpSize=sSnd.L;
      if(TcpUdpSize>MAX_BUF_SIZE) TcpUdpSize=MAX_BUF_SIZE;
#ifdef SINFO_ENERGO
      btId=GetCodeFromSndCP(sSnd.ByteInfo[13]);
      sSnd.ByteInfo[13]=btId;
      btSN=GetSNFromCP(btId);
#endif
      sendto(SoundSocket,sSnd.ByteInfo,TcpUdpSize,(unsigned char *)(&sSnd.A1),sSnd.PORT1);
#ifdef SINFO_ENERGO
      SendSound2AllRecordSystems(btSN,btId,1,(xListRTP *)sSnd.ByteInfo);
#endif
    }
  }
}

void SendIpLoggerPacket(void)
{
  unsigned char btInd;
  unsigned short i,TcpUdpSize,PORT1=0;
  unsigned int A1=0,csum;
  static section ("bigdata") sInfoKadr sInfo;

  if(!isSocketSendEnd(CommandSocket))
  {
    if(isSocketTOut(CommandSocket)) ReOpenSocket(CommandSocket,g_IpParams.wPortControl,true);
    return;
  }
  if(getsizeUDP(CommandSocket,true)<sizeof(sInfoKadr))
  {
    if(s_btCnt4Cmd<10) s_btCnt4Cmd++;
    else ReOpenSocket(CommandSocket,g_IpParams.wPortControl,true);
    return;
  }
  s_btCnt4Cmd=0;
  btInd=FindIpReg(IP_LOGGER,&A1,&PORT1,NULL,NULL);
  if((btInd!=0xff) && A1 && PORT1)
  {
    if(xQueueReceive(xQueueLogger,&sInfo,0)==pdPASS)
    {
      TcpUdpSize=strlen((char *)sInfo.ByteInfoKadr);
      if(TcpUdpSize>MAX_BUF_SIZE-4) TcpUdpSize=MAX_BUF_SIZE-4;
      sInfo.ByteInfoKadr[TcpUdpSize]=0;
      TcpUdpBuf[0]=SynchroByte;
      TcpUdpBuf[1]=123;  // ��� ������� IP-������
      memcpy(TcpUdpBuf+2,sInfo.ByteInfoKadr,TcpUdpSize+1);
      TcpUdpSize+=4;
      for(i=0,csum=0;i<TcpUdpSize-1;i++) csum+=TcpUdpBuf[i];
      TcpUdpBuf[i]=csum & 0xFF;
      sendto(CommandSocket,TcpUdpBuf,TcpUdpSize,(unsigned char *)&A1,PORT1);
    }
  }
}

void SendReinitCount2Ip(void)
{
  unsigned char PultNb,SN,btInd;
  static section ("bigdata") sInfoKadr sInfo;

  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
#ifdef SINFO_ENERGO
    SN=GetPultSNFromIndex(PultNb);
#else
    SN=MassM1[PultNb];
#endif
    if(SN>16)
    {
      if(GetAddressIpPult(SN,&sInfo.A1,&sInfo.PORT1)) SendIpReinitCount2IpPult(SN,&sInfo);
    }
  }
  if(GetControlIpAddr(&sInfo.A1,&sInfo.PORT1)) SendIpReinitCount2IpPult(0,&sInfo);
  SendStr2IpLogger("$INFO$Wiznet reinit counter=%d",s_wIpReinit);
  s_bNeedSendAboutReinit=false;
}

void ReciveIpPacket(void)
{
  unsigned short TcpUdpSize;

  if(TcpUdpSize=getsizeUDP(CommandSocket,false))
  {
    unsigned char btCmd;
    unsigned short i,wChan;
    unsigned int csum;
    static sCommand sCmd;
    static sInfoKadr sInfoS1;

    if(TcpUdpSize>MAX_BUF_SIZE) TcpUdpSize=MAX_BUF_SIZE;
    TcpUdpSize=recvfrom(CommandSocket,TcpUdpBuf,TcpUdpSize,(unsigned char *)&sCmd.A1,&sCmd.PORT1);
/*  if(0x0401a8c0!=sCmd.A1)
  {
SendStr2IpLogger("$DEBUG$ReciveIpPacket: A=%08x, p=%d, size=%d, [0]=%02x, [1]=%02x",
                 sCmd.A1,sCmd.PORT1,TcpUdpSize,TcpUdpBuf[0],TcpUdpBuf[1]);
  }*/
    if(TcpUdpSize<3) return; // ����������� ������ ����: ����������,��� �������,CRC
    if(TcpUdpSize-2>d_MaxCmdBufSz) return;
    if(TcpUdpBuf[0]!=0x16) return;
    // ������� ����������� �����
    for(i=0,csum=0;i<TcpUdpSize-1;i++) csum+=TcpUdpBuf[i];
    // �������� ����������� �����
    if((csum & 0xFF)!=TcpUdpBuf[TcpUdpSize-1]) return;
    sInfoS1.Source=UserUDP;
    sInfoS1.A1=sCmd.A1;
    sInfoS1.PORT1=sCmd.PORT1;
    if((TcpUdpBuf[1]>=0xea) && (TcpUdpBuf[1]<=0xed))
    {
      // sInfoS1.ByteInfoKadr[0..2] ��� ������� IPPM �� ������������! 
      sInfoS1.ByteInfoKadr[3]=CallIPPM;  // ������� IPPM
      sInfoS1.L=TcpUdpSize; // ��������� - ������ ��������� ������
      memcpy(sInfoS1.ByteInfoKadr+4,TcpUdpBuf,TcpUdpSize); // ���� �� �������
      xQueueSend(xQueueVxCall,&sInfoS1,0,0);
      return;
    }
    s_dwIpTOut=xTaskGetTickCount()+dIpTOutFirst;
    s_wIpReinitSeq=0;
    if(s_bNeedSendAboutReinit) SendReinitCount2Ip();
    btCmd=TcpUdpBuf[1];
    switch(btCmd)
    {
      case 0xff:
        IRP_Check(IP_DSPCOM,sCmd.A1);
        break;
      case 10:
        // ������ ����� ����������� DspComm - ��� ���� F12 (cmd100) ��� ���� ��������� ��������� (cmd154)
        if((TcpUdpBuf[2]==100)/* || (TcpUdpBuf[2]==154)*/) IRP_Registration(IP_DSPCOM,sCmd.A1,sCmd.PORT1,0);
        // ����� ����� ����������� DspComm 
        if(TcpUdpBuf[2]==198) IRP_Registration(IP_DSPCOM,sCmd.A1,sCmd.PORT1,TcpUdpBuf+3);
        if(TcpUdpBuf[2]==199) IRP_Unregistration(IP_DSPCOM,sCmd.A1);
        break;
/*case 38:
{ // ������� ���/���� ������ ������ �� ����
SendStr2IpLogger("$OUTCTRL$IpEnDisableOut: wChan=%d, cmd=%d",TcpUdpBuf[2]*256+TcpUdpBuf[3]+1,TcpUdpBuf[4]);
  break;
}*/
      case 254:
        TcpUdpBuf[1]=btCmd=10; // ��������� ������� �� ������������� ������, ��������������� � ������� 10
                               // ���������� ��� ��������� ����������� �� ������������� DspCom
        switch(TcpUdpBuf[2])
        {
           case 198:
             IRP_Registration(IP_LOGGER,sCmd.A1,sCmd.PORT1,TcpUdpBuf+3);
             break;
           case 199:
             IRP_Unregistration(IP_LOGGER,sCmd.A1);
             break;
           case 255:
             IRP_Check(IP_LOGGER,sCmd.A1);
             break;
#ifdef SINFO_ENERGO
           case 162:
           case 163:
             wChan=GetOperatorChannelFromCode(TcpUdpBuf[4])-1;
             TcpUdpBuf[3]=wChan >> 8;
             TcpUdpBuf[4]=wChan & 0xff;
SendStr2IpLogger("$DEBUG$IP_Cmd=%d, TypeChan=%d, CP=%d",TcpUdpBuf[2],TcpUdpBuf[3],TcpUdpBuf[4]);
             break;
           case 200:
             IRP_Registration(IP_RECORD,sCmd.A1,sCmd.PORT1,TcpUdpBuf+3);
             break;
           case 201:
             IRP_Unregistration(IP_RECORD,sCmd.A1);
             break;
           case 202:
             IRP_Check(IP_RECORD,sCmd.A1);
             break;
#else
           case 162:
           case 163:
SendStr2IpLogger("$DEBUG$IP_Cmd=%d, TypeChan=%d, CP=%d",TcpUdpBuf[2],TcpUdpBuf[3],TcpUdpBuf[4]);
                 TcpUdpBuf[4]-=101;
             break;
#endif
           default:
             break;
        }
        break;
      default: break;  
    }
    if((btCmd==252) || (btCmd==253))
    { // ������ �����(252) ��� �������(253) �� ������-������
#ifdef SINFO_ENERGO
      sInfoS1.L=(TcpUdpSize-2)+4;
      if((btCmd==253) && (TcpUdpBuf[6]==10))
      { // ������� �������� ����� ��������� ��� ���������� (������������ ����� ��� ���������)
        memcpy(sInfoS1.ByteInfoKadr+4,TcpUdpBuf+1,8); // �������� ������ ��������� ����� ���������
        RecieveAudioPacket(&sInfoS1,TcpUdpBuf+9);
      }
      else
      {
        // sInfoS1.ByteInfoKadr[0..2] ��� ������� ������������� ������ �� ������������! 
        sInfoS1.ByteInfoKadr[3]=34;  // ������� ������������� ������
        memcpy(sInfoS1.ByteInfoKadr+4,TcpUdpBuf+1,sInfoS1.L);
        xQueueSend(xQueueVxCall,&sInfoS1,0,0);
//      if(btCmd==253) SendCmd2AllRecordSystems(sInfoS1.L-2,TcpUdpBuf);
      }
#endif
    }
    else
    {
      sCmd.L=TcpUdpSize-2; // ����� ������ �������
      sCmd.Source=UserUDP;
      memcpy(sCmd.BufCommand,TcpUdpBuf+1,sCmd.L);
      if(btCmd==12) xQueueSend(xQueueTFTP,&sCmd,0,0); // ������� ������ � �������
      else
      {
        if(!IRP_Cmd(sCmd.BufCommand,sCmd.L))
        {
          if(btCmd==10)
          {
            if(sCmd.BufCommand[1]==179) SendArray2PC(&sInfoS1,DataSave1,20);
            else
            {
              if(xQueueSend(xQueueSportX,&sCmd,0,0)==errQUEUE_FULL) CountErrQueueReply++;
            }
          }
          else xQueueSendCmd(&sCmd,0);
        }
      }
    }
  }

  TcpUdpSize=getsizeUDP(SoundSocket,false);
  if(TcpUdpSize)
  {
    static section ("bigdata") xListRTP sSndIp;
    unsigned short SoundInPort;
    unsigned int dwIP;

    TcpUdpSize=recvfrom(SoundSocket,(uint8 *)&sSndIp,sizeof(xListRTP),(uint8 *)&dwIP,&SoundInPort);
    if(TcpUdpSize==sizeof(xListRTP))
    { // ��������� ���� .ProFile � ������������ � ��������, �������� � ���� � SportX
      if(sSndIp.Octet12==0x0890)
      {
#ifdef SINFO_ENERGO
        unsigned char btSN,btId;
//      unsigned short ProFile=sSndIp.ProFile;
        btId=sSndIp.ProFile >> 8;
        sSndIp.ProFile=((GetOperatorChannelFromCode(btId)-1) << 8) | 1;
//SendStr2IpLogger("$AUDIO_IN$Recieve audio .ProFile_0=%04x, .ProFile_1=%04x",ProFile,sSndIp.ProFile);
#else
//SendStr2IpLogger("$AUDIO_IN$Recieve audio sSndIp.ProFile=%04x",sSndIp.ProFile);
        sSndIp.ProFile-=0x6500;
#endif
        xQueueSend(xQueueSoundIpIn,&sSndIp,0,0);
#ifdef SINFO_ENERGO
        btSN=GetSNFromCP(btId);
        SendSound2AllRecordSystems(btSN,btId,0,&sSndIp);
#endif
        s_dwIpTOut=xTaskGetTickCount()+dIpTOutFirst;
        s_wIpReinitSeq=0;
      }
      else
      {
#ifndef SINFO_ENERGO
        if(sSndIp.Octet12==0x0891)
        { // ����� �� IPPM
//          unsigned short ProFile=sSndIp.ProFile;
          sSndIp.ProFile=GetCP_IPPM2MU(sSndIp.ProFile & 0xff);
          if(sSndIp.ProFile)
          {
            sSndIp.ProFile=((sSndIp.ProFile-1) << 8) | 1;
//SendStr2IpLogger("$AUDIO_IN$Recieve audio .ProFile_0=%04x, .ProFile_1=%04x",ProFile,sSndIp.ProFile);
            xQueueSend(xQueueSoundIpIn,&sSndIp,0,0);
            s_dwIpTOut=xTaskGetTickCount()+dIpTOutFirst;
            s_wIpReinitSeq=0;
          }
        }
#endif
      }
    }
/*    else
    {
      uint8 *pbtBufCmd=(uint8 *)&sSndIp;
      if((pbtBufCmd[0]==10) && (pbtBufCmd[1]==180))
      {
        IpPultLife(pbtBufCmd+2,1);
        return(true);
      }
    }*/
  }
}

void vIPTask(void *pvParameters)
{
  InitIP();
  s_wIpReinit=0;
  s_wIpReinitSeq=0;
  while(1)
  {
    LedWorkIndicator();

    if(!(*pFIO_FLAG_D & PF6) || IsNeedIpReinit() || IsCurrentTickCountGT(s_dwIpTOut))
    { // ��� ������� ������ "����." ������������� Wiznet
      LedOn(ledRabota);
      InitIP();
      if(s_wIpReinitSeq<4) AddToLog(lt_Error,ls_Task,lc_WiznetReinit,s_wIpReinit+100); // +100 - ��� DspCom
      vTaskDelay(500);
    }
#ifdef __VLAN__
    if(Marvell) CheckVLAN((GT_U16 *)ucDataVLANID);
#endif
//TestSendSoundPacket(); //!!!
    //if(g_bInitMulticastSocket) CreateMulticastSocketW6100();
    IRPTOut();
    //SendTaskState2Ip();
    ReciveIpPacket();
    SendIpCmdPacket();
    SendIpSoundPacket();
    //if(IsW6100()) SendIpSoundMulticastPacketW6100();
    SendIpLoggerPacket();
#ifdef SINFO_ENERGO
    SendIpPacket4Record();
#endif
#ifdef __SNMP__
!!!    SnmpTask();
#endif
    SetWDState(d_WDSTATE_TcpIp);
    vTaskDelay(2);
  }
}

/////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdarg.h>
extern int vsprintf(char *buf, const char *fmt, va_list ap);
void SendStr2IpLogger(const char *fmt,...)
{
  char *pStr;
  static section ("bigdata") sInfoKadr sInfoE;
  va_list ap;

  LockPrintf();
  if(xIsQueueFull(xQueueLogger)) xQueueReceive(xQueueLogger,&sInfoE,0);
  pStr=(char *)sInfoE.ByteInfoKadr;
  va_start(ap,fmt);
  vsnprintf(pStr,250,fmt,ap);
  va_end(ap);
  sprintf(pStr+strlen(pStr)," (%s)",ConvertCurrentTM2Str(NULL));
  xQueueSend(xQueueLogger,&sInfoE,0,0);
  UnlockPrintf();
}

