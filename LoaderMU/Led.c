#include "Sinfo.h"

// ##### ������ �� ������������ (D0..D3) � SPI CS ��� W6100 (D5) � SC16IS740IPW (D4) #####
volatile static unsigned char LEDdata;

// ������������� ����������� - ��� ��������
void InitLed()
{
  LEDdata=0xf0;
  *pLED=LEDdata;
  ssync();
  *pFIO_FLAG_C = PF12; // ���������� ������ 74HC253
}

// ��������� ���������� � �������� �������
void LedOn(unsigned char LedNum)
{
  LEDdata |= ((1<<(LedNum-1)) & 0x0f);  // ����������� ������, ����������� ������ �����������
  *pLED=LEDdata;
  ssync();
}

// ���������� ���������� � �������� �������
void LedOff(unsigned char LedNum)
{
  LEDdata &= ~((1<<(LedNum-1)) & 0x0f);  // ����������� ������, ����������� ������ �����������
  *pLED=LEDdata;
  ssync();
}

// ������������ ���������� � �������� �������
void LedToggle(unsigned char LedNum)
{
  LEDdata ^= ((1<<(LedNum-1)) & 0x0f);  // ����������� ������, ����������� ������ �����������
  *pLED=LEDdata;
  ssync();
}

// ��������� ���������� � �������� �������
bool LedStatus(unsigned char LedNum)
{
  return((LEDdata & (1<<(LedNum-1))) & 0x0f);  // ������ ������, ����������� ������ �����������
}

void IpCS_Low(void)
{
  LEDdata &= 0xdf;
  *pLED=LEDdata;
  ssync();
}

void IpCS_Hi(void)
{
  LEDdata |= 0x20;
  *pLED=LEDdata;
  ssync();
}

