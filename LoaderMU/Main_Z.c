/*
#include "Sinfo.h"

void ToogleWD_Main(void)
{ // !! ������� ��� ������� !!
  unsigned i,j;

  *pFIO_INEN|= PF13|PF7|PF6;       // ���������� ����� PF13 ��� Default �������� IP ������, ������ PF7, PF6
  *pFIO_DIR |= PF11|PF10|PF5|PF0;  // ��������� �� ����� PF11, PF10, PF5, PF0
                                   // (PF0 - ����� WD, PF5 - ���������)
  ssync();
  *pFIO_FLAG_C = PF0; // ����� WD
  *pFIO_FLAG_S = PF11|PF10|PF5;
  ssync();
  while(1)
  {
    for(i=0;i<0x4;i++)
    {
      *pFIO_FLAG_S=PF0;
      ssync();
    }
    *pFIO_FLAG_C=PF0;
    ssync();
    for(i=0;i<0xff;i++)
    {
      *pFIO_FLAG_C=PF0;
      ssync();
    }
  }
}*/

void main(void)
{
//ToogleWD_Main();
  asm volatile (".extern ldf_stack_end;");
  asm volatile (".extern _LoaderMainCode;");
  
  asm volatile ("CLI R0;");
  asm volatile ("SP.L=ldf_stack_end;");
  asm volatile ("SP.H=ldf_stack_end;");
  asm volatile ("P0.L=_LoaderMainCode;");
  asm volatile ("P0.H=_LoaderMainCode;");
  asm volatile ("JUMP (P0);");
}

