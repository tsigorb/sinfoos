
/*
** basiccrt.s generated on Dec 03, 2014 at 16:58:33.
**
** Copyright (C) 2000-2004 Analog Devices Inc., All Rights Reserved.
** This contains Analog Devices Background IP and Development IP as
** defined in the ADI/Intel Collaboration Agreement.
**
** This file is generated automatically based upon the options selected
** in the Startup Code Wizard. Changes to the startup configuration
** should be made by changing the appropriate options rather than
** editing this file. Additional user code to be executed before calling
** main can be inserted between the labels .start_of_user_code and
** .end_of_user_code. This code is preserved if the CRT is re-generated.
**
** Configuration:-
**     processor:                    ADSP-BF533
**     cplb_init:                    false
**     mem_init:                     true
**     device_init:                  false
**     init_regs:                    false
**     zero_return_regs:             false
**     profiling:                    false
**     set_clock_and_power:          false
**
*/

/////////////////////////////////////////////////////////////////
// blackfin-edinburgh-core
#include <Def_LPBlackfin.h>
#include <sys/platform.h>

/////////////////////////////////////////////////////////////////
// standard
#define IVBh                (EVT0 >> 16)
#define IVBl                (EVT0 & 0xFFFF)
#define UNASSIGNED_VAL      0x8181
#define INTERRUPT_BITS      0x400 // just IVG15
#define SYSCFG_VALUE        0x30

	.section/DOUBLEANY program;
	.align 2;

start:

/////////////////////////////////////////////////////////////////
// blackfin-edinburgh-core
#if ((defined(__SILICON_REVISION__) && \
     (__SILICON_REVISION__ == 0xffff || __SILICON_REVISION__ <= 0x2)) || \
     !defined(__SILICON_REVISION__))
	// Avoid BF532 Anomaly #22
	R1 = SYSCFG_VALUE;
	SYSCFG = R1;
#endif

/////////////////////////////////////////////////////////////////
// standard
#if (defined(__ADSPBLACKFIN__) && defined (__SILICON_REVISION__) && \
       (defined(__ADSPBF531__) || defined(__ADSPBF532__) || \
        defined(__ADSPBF533__) || defined(__ADSPBF538__) || \
        defined(__ADSPBF539__)))
	// Avoid Anomaly 05-00-0229: DMA5_CONFIG and SPI_CTL not cleared on reset.
	R1 = 0x400;
#if defined(__ADSPBF538__) || defined(__ADSPBF539__)
	P0.L = SPI0_CTL & 0xFFFF;
	P0.H = SPI0_CTL >> 16;
	W[P0] = R1.L;
#else
	P0.L = SPI_CTL & 0xFFFF;
	P0.H = SPI_CTL >> 16;
	W[P0] = R1.L;
#endif
	P0.L = DMA5_CONFIG & 0xFFFF;
	P0.H = DMA5_CONFIG >> 16;
	R1 = 0;
	W[P0] = R1.L;
#endif
	// Zap loop counters to zero, to make sure that
	// hw loops are disabled - it could be really baffling
	// if the counters and bottom regs are set, and we happen
	// to run into them.
	R7 = 0;
	LC0 = R7;
	LC1 = R7;

	// Clear the DAG Length regs too, so that it's safe to
	// use I-regs without them wrapping around.
	L0 = R7;
	L1 = R7;
	L2 = R7;
	L3 = R7;

	// Zero ITEST_COMMAND and DTEST_COMMAND
	// (in case they have crud in them and
	// does a write somewhere when we enable cache)
	I0.L = (ITEST_COMMAND & 0xFFFF);
	I0.H = (ITEST_COMMAND >> 16);
	I1.L = (DTEST_COMMAND & 0xFFFF);
	I1.H = (DTEST_COMMAND >> 16);
	[I0] = R7;
	[I1] = R7;
	CSYNC;

	// Initialise the Event Vector table.
	P0.H = IVBh;
	P0.L = IVBl;

	// Install __unknown_exception_occurred in EVT so that 
	// there is defined behaviour.
	P0 += 2*4;     // Skip Emulation and Reset
	P1 = 13;
	R1.L = __unknown_exception_occurred;
	R1.H = __unknown_exception_occurred;
	LSETUP (.ivt,.ivt) LC0 = P1;
.ivt:	[P0++] = R1;

	// Set IVG15's handler to be the start of the mode-change
	// code. Then, before we return from the Reset back to user
	// mode, we'll raise IVG15. This will mean we stay in supervisor
	// mode, and continue from the mode-change point, but at a
	// much lower priority.
	P1.H = supervisor_mode;
	P1.L = supervisor_mode;
	[P0] = P1;

/////////////////////////////////////////////////////////////////
// standard
	// Initialise the stack.
	// Note: this points just past the end of the section.
	// First write should be with [--SP].
	SP.L=ldf_stack_end;
	SP.H=ldf_stack_end;
	usp = sp;

	// We're still in supervisor mode at the moment, so the FP
	// needs to point to the supervisor stack.
	FP = SP;

	// And make space for incoming "parameters" for functions
	// we call from here:
	SP += -12;

	R0 = INTERRUPT_BITS;
	R0 <<= 5;  // Bits 0-4 not settable.
	CALL.X __install_default_handlers;

	R1 = SYSCFG;
	R4 = R0;		// Save modified list for later
	BITSET(R1,1);
	SYSCFG = R1;	// Enable the cycle counter

/////////////////////////////////////////////////////////////////
// blackfin-edinburgh-core
#if ((defined(__SILICON_REVISION__) && \
     (__SILICON_REVISION__ == 0xffff || __SILICON_REVISION__ <= 0x2)) || \
     !defined(__SILICON_REVISION__))
	// Set the port preferences of DAG0 and DAG1 to be
	// different; this gives better performance when
	// performing daul-dag operations on SDRAM.
	P0.L = DMEM_CONTROL & 0xFFFF;
	P0.H = DMEM_CONTROL >> 16;
	R0 = [P0];
	BITSET(R0, 12);
	BITCLR(R0, 13);
	[P0] = R0;
	CSYNC;
#endif

/////////////////////////////////////////////////////////////////
// !!!�������� ��� �� �����!!!
// runtime-mem-init
	// Reinitialise data areas in RAM from ROM, if MemInit's
	// been used.
//	CALL.X _mi_initialize;
//.extern _mi_initialize;
//.type _mi_initialize, STT_FUNC;

/////////////////////////////////////////////////////////////////
// standard
	//  Enable interrupts
	STI R4;    // Using the mask from default handlers
	RAISE 15;

	// Move the processor into user mode.
	P0.L=still_interrupt_in_ipend;
	P0.H=still_interrupt_in_ipend;
	RETI=P0;
	NOP;       // Purely to prevent a stall warning

	// execute RTI until we've `finished` servicing all
	// interrupts of priority higher than IVG15. Normally one
	// would expect to only have the reset interrupt in IPEND
	// being serviced, but occasionally when debugging this may
	// not be the case - if restart is hit when servicing an
	// interrupt.
	//
	// When we clear all bits from IPEND, we'll enter user mode,
	// then we'll automatically jump to supervisor_mode to start
	// servicing IVG15 (which we will 'service' for the whole
	// program, so that the program is in supervisor mode.
	// Need to do this to 'finish' servicing the reset interupt.
	RTI;
still_interrupt_in_ipend:
    JUMP still_interrupt_in_ipend;

supervisor_mode:
	[--SP] = RETI;     // re-enables the interrupt system

	R0.L = UNASSIGNED_VAL;
	R0.H = UNASSIGNED_VAL;

	// Push a RETS and Old FP onto the stack, for sanity.
	[--SP]=R0;
	[--SP]=R0;
	// Make sure the FP is sensible.
	FP = SP;
	// And leave space for incoming "parameters"
	SP += -12;

/////////////////////////////////////////////////////////////////
// standard
	// At long last, call the application program.
	CALL.X _main;

/////////////////////////////////////////////////////////////////
// call-exit
//	CALL.X _exit;  // passing in main's return value
//.extern _exit;
//.type _exit,STT_FUNC;

/////////////////////////////////////////////////////////////////
/*
toogle_wd_always:
    P0.L=FIO_DIR & 0xffff;
    P0.H=FIO_DIR >> 16;
    R0.L=W[P0];
    R1=PF0;
    R0=R0 | R1;
    W[P0]=R0.L;
    ssync;
    P0.L=FIO_FLAG_S & 0xffff;
    P0.H=FIO_FLAG_S >> 16;
    P1.L=FIO_FLAG_C & 0xffff;
    P1.H=FIO_FLAG_C >> 16;
    R0.L=PF0;
  while1:
      P2=4;
      LSETUP (.wds_s,.wds_e) LC0 = P2;
        .wds_s: W[P0] = R1;
        .wds_e: ssync;
      P2=0xff;
      LSETUP (.wdc_s,.wdc_e) LC0 = P2;
        .wdc_s: W[P1] = R1;
        .wdc_e: ssync;
    JUMP while1;
*/

/////////////////////////////////////////////////////////////////
// standard
.start.end:        // To keep the linker happy.

.global start;
.type start,STT_FUNC;
.global .start.end;
.type .start.end,STT_FUNC;
.extern _main;
.type _main,STT_FUNC;
.extern ldf_stack_end;
.extern __unknown_exception_occurred;
.type __unknown_exception_occurred,STT_FUNC;
.extern __install_default_handlers;
.type __install_default_handlers,STT_FUNC;

