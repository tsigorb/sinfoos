#include <cdefBF533.h>
#include <sys\exception.h>
#include <ccblkfn.h>
#include <sysreg.h>
#include "Led.h"

// SPI transfer mode
#define TIMOD_01        0x0001

#define CoeffMult       30 //����������� ��������� ����������

#define CCLK            491520
#define SCLK            49152

#define IVG_PLL_WAKEUP	P0_IVG
#define IVG_DMA_ERROR	P1_IVG
#define IVG_PPI_ERROR	P2_IVG
#define IVG_SPT0_ERROR	P3_IVG
#define IVG_SPI_ERROR	P4_IVG
#define IVG_SPT1_ERROR	P5_IVG
#define IVG_UART_ERROR	P6_IVG
#define IVG_RTC			P7_IVG
#define IVG_PPI			P8_IVG
#define IVG_SPT0_RX		P9_IVG
#define IVG_SPT0_TX		P10_IVG
#define IVG_SPT1_RX		P11_IVG
#define IVG_SPT1_TX		P12_IVG
#define IVG_SPI			P13_IVG
#define IVG_UART_RX		P14_IVG
#define IVG_UART_TX		P15_IVG
#define IVG_TIMER0		P16_IVG
#define IVG_TIMER1		P17_IVG
#define IVG_TIMER2		P18_IVG
#define IVG_PFA			P19_IVG
#define IVG_PFB			P20_IVG
#define IVG_MEMDMA0		P21_IVG
#define IVG_MEMDMA1		P22_IVG
#define IVG_SWDT		P23_IVG

//����� ��� SIC_IMASK 
#define IRQ_PLL_WAKEUP	0x00000001
#define IRQ_DMA_ERROR	0x00000002
#define IRQ_PPI_ERROR	0x00000004
#define IRQ_SPT0_ERROR	0x00000008
#define IRQ_SPT1_ERROR	0x00000010
#define IRQ_SPI_ERROR	0x00000020
#define IRQ_UART_ERROR	0x00000040
#define IRQ_RTC			0x00000080
#define IRQ_PPI			0x00000100
#define IRQ_SPT0_RX		0x00000200
#define IRQ_SPT0_TX		0x00000400
#define IRQ_SPT1_RX		0x00000800
#define IRQ_SPT1_TX		0x00001000
#define IRQ_SPI			0x00002000
#define IRQ_UART_RX		0x00004000
#define IRQ_UART_TX		0x00008000
#define IRQ_TIMER0		0x00010000
#define IRQ_TIMER1		0x00020000
#define IRQ_TIMER2		0x00040000
#define IRQ_PFA			0x00080000
#define IRQ_PFB			0x00100000
#define IRQ_MEMDMA0		0x00200000
#define IRQ_MEMDMA1		0x00400000
#define IRQ_SWDT		0x00800000

// DMA flow mode=1 - ����� ���������������
#define FLOW_1          0x1000

#define pFlash         (volatile unsigned char *)0x20000000
#define pIP            (volatile unsigned char *)0x20100000

#define pRHR	       (volatile unsigned char *)0x202f0000
#define pTHR	       (volatile unsigned char *)0x202f0000
#define pIER	       (volatile unsigned char *)0x202f0002
#define pFCR	       (volatile unsigned char *)0x202f0004
#define pISR	       (volatile unsigned char *)0x202f0004
#define pLCR	       (volatile unsigned char *)0x202f0006
#define pMCR	       (volatile unsigned char *)0x202f0008
#define pLSR	       (volatile unsigned char *)0x202f000a
#define pMSR	       (volatile unsigned char *)0x202f000c
#define pSPR	       (volatile unsigned char *)0x202f000e
#define pDLL	       (volatile unsigned char *)0x202f0000
#define pDLM	       (volatile unsigned char *)0x202f0002
#define pLED	       (volatile unsigned char *)0x202f0010

#define pRHR1	       (volatile unsigned char *)0x203f0000
#define pTHR1	       (volatile unsigned char *)0x203f0000
#define pIER1	       (volatile unsigned char *)0x203f0002
#define pFCR1	       (volatile unsigned char *)0x203f0004
#define pISR1	       (volatile unsigned char *)0x203f0004
#define pLCR1	       (volatile unsigned char *)0x203f0006
#define pMCR1	       (volatile unsigned char *)0x203f0008
#define pLSR1	       (volatile unsigned char *)0x203f000a
#define pMSR1	       (volatile unsigned char *)0x203f000c
#define pSPR1	       (volatile unsigned char *)0x203f000e
#define pDLL1	       (volatile unsigned char *)0x203f0000
#define pDLM1	       (volatile unsigned char *)0x203f0002

#define CodeStatusTrue      0
#define CodeStatusFalse     255 

// ���� ������ � ������ ����� ����������� � ��
#define ErrCS 				3 // ������ ����������� ����� �� ������ RS232 
#define ErrCodeQueueSend 	4 // ������� xQueueCommand �����������
#define ErrNoSinByte 		5 // �� ���� ����������� � �������
#define ErrProtokolUart 	6 // ������ ��������� Uart
#define ErrTimeOut 			7 // ����������� ����� �������� ����� ������� �� RS232
#define ErrNoCommand 		8 // �� ���������� �������� �������
#define ErrQueueUart 		251 // ������� xCharsForTx �� �����, � ����� ���������� ����� ������� 
#define ErrInReply 			11 // ������ � ������ ��
#define ErrNoReplyUart 		252 // �� ������ ����� �� ��
#define NoCommand 			0xB0 //��� ����� �������

// ----- ������ �������� �� ���� -----
#define OffsetFl 			0
#define AdrConditionKey 	OffsetFl+0x60 //��������� ������ 1 ����� �� 1 �����
#define AdrKIn	 			OffsetFl+0xC0 // ������� �������� 
#define	AdrKOut	 			OffsetFl+0x120 // �������� ��������
#define	AdrAnalPorog 		OffsetFl+0x180 // ����������� ������� ������� �������
#define AdrMinARU			OffsetFl+0x1E0 //������������ �������� ���
#define AdrNoise4dB			OffsetFl+0x240 //����� ��������������
#define AdrKoefNoise 		OffsetFl+0x2A0 //���������� ������� ������� ��������������
#define	AdrCoefCorrF 		OffsetFl+0x300 //  ��� �������� �� ������� 3000 ��
#define AdrReturnCoef 		OffsetFl+0x360  //������� ��������
#define AdrARU				OffsetFl+0x3C0  //��� ��������/���������
// ������� ��������� ������������� 128 ���� 
#define AdrFiltrEcho        OffsetFl+0x420 
// ����� ��� �������� ���-�������������  
#define AdrBufEcho          OffsetFl+0x480  //� 0�480 �� 0�1080 (�� 0�100 �� 1 �����, LengthEcho �� 1 �����)
#define AdrTypeQuit         OffsetFl+0x1020
#define AdrTypeCall         OffsetFl+0x1080  //��� ������ (48 �������*4 �����= 192 (�0))
// ������ �������� ��: 1 - � ����������, 0 - � FLASH 
#define AdrZagrBK           OffsetFl+0x1300 //0x1300-1301
#define AdrCoeffLow         OffsetFl+0x1900 //����������� ��������� ���������
#define TCPIPStack          OffsetFl+0x2000 //��������� ����� ���������� TCPIP 0x2000-0x20FF
#define MarvellInfo         OffsetFl+0x203A // ������ � ������� ���������� Marvell 0x203A-0x203B

#define LengthEcho          0x40 //���-�� ���� �� FLASH ��� 1-�� ������
#define NEcho	            25 //���������� ���������������� (�� 2 ����� �� 1 �����������)

#define d_SyncByte          0x16
#define d_BufMaxSz          1500
#define d_XchgMaxSz         260

//--------- ��������� ��������������� ����� ��� ������
typedef struct InfoKadr 
{ 	unsigned char Source; //�������� �������
	unsigned char L; // �����
	unsigned int A1; // ����� ����������
	unsigned short PORT1; // ���� ����������
	unsigned char ByteInfoKadr[d_XchgMaxSz]; //��������� �� ����� �����
} sInfoKadr;

//--------------- ��������� �������
typedef struct ACommand 
{ 	unsigned char Source; //�������� �������
	unsigned char Length; // ����� ������ �������
	unsigned int A1; // ����� ����������
	unsigned short PORT1; // ���� ����������
	unsigned char BufCommand[d_XchgMaxSz]; //��������� �� ����� �������
} sCommand;

/////////////////////////////////////////////////////////////////////
#define SEL_CONTROL     0   // Confirm socket status
#define SEL_SEND        1   // Confirm Tx free buffer size
#define SEL_RECV        2   // Confirm Rx data size

#define d_CommandSocket 1   // ����� ������ ��� �������� ������
// !! ������������ ����� ������ - 3 !!

#pragma pack(1)
typedef struct
{ // ��������� ������ ������ ��������� � ������������ �������, �.�. ��� ������� � �������� ������ �� Flash ������
unsigned char ucDataSIPR[4]; // IP address
unsigned char ucDataMSR[4]; // Subnet mask
unsigned char ucDataGAR[4]; // Gateway address
unsigned char ucDataSHAR[6]; // MAC address - DO NOT USE THIS ON A PUBLIC NETWORK!
unsigned short wDummy; // �������� ���� ��� ������������ �� ������� 4 �����
unsigned short wPortSNMP;
unsigned short wPortSound;
unsigned short wPortControl;
unsigned short wPortHTTP;
unsigned char ucDataSNMPmIP[4]; // IP address SNMP manager ��������
unsigned char ucDataSNMPrIP[4]; // IP address SNMP manager ���������
unsigned char ucDataSoundIP[4]; // IP address ������� �����������
unsigned short wDataVLANID[6]; // VLANID ��� ������ 0-5 Marvell
unsigned char ucDataVLANPri[6]; // VLAN ��������� ��� ������ 0-5 Marvell
// #################################################################################
} IPPARAMS_TP;
#pragma pack()

#define dAddrLdr2Flash          0x10000
#define dAddrLdr2SDRAM          0xA00000

typedef void (*FuncPtr)(void);

void InitSPI(void);
unsigned short RdFlash(unsigned short AdrFl);
bool WrFlash(unsigned short DanFl,unsigned short AdrFl);
bool RdFlashP(const unsigned short *lpInpAddr,unsigned short *lpOut,unsigned int *lpHowOut);
bool WrDeviceArray(unsigned int Addr0, unsigned short *lpInp, int HowInp);

void memcpy(void *pAdrDst0,const void *pAdrSrs0,unsigned int CountDan);
unsigned int GetPgmAddress(void);
bool IsProgramInFlash(void);
bool CheckLdr(bool bFlash);
bool ZagrLdr(bool bFlash);

void ResetWatchDog(void);

