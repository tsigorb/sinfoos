#include "Sinfo.h"
#include "../MD5/md5.h" 

/////////////////////////////////////////////////////////////////////
unsigned short Buf4Ldr[0x8000];

/////////////////////////////////////////////////////////////////////
unsigned int GetPgmAddress(void)
{
  unsigned int WordContrl;
  unsigned short *AdrF;

  AdrF=(unsigned short *)dAddrLdr2Flash; // ����� ����
  WordContrl=2;
  RdFlashP(AdrF, Buf4Ldr, &WordContrl);
  WordContrl=Buf4Ldr[1];
  WordContrl=(WordContrl << 16) | Buf4Ldr[0];
  return WordContrl;
}

bool IsProgramInFlash(void)
{
  unsigned int WordContrl=GetPgmAddress();
  return !((WordContrl==0xffffffff) || (WordContrl==0xfffffffe)); // ���� ������ �� ����
}

void memcpy(void *pAdrDst0,const void *pAdrSrs0,unsigned int CountDan)
{
  unsigned char *pAdrDst,*pAdrSrs;
  unsigned int i;

  pAdrDst=(unsigned char *)pAdrDst0;
  pAdrSrs=(unsigned char *)pAdrSrs0;
  for(i=0;i<CountDan;i++)
  {
    pAdrDst[i]=pAdrSrs[i];
    if(!(i & 0xfff)) ResetWatchDog();
  }
}

void MemSet(unsigned char *pAdrDst,unsigned char btCode,unsigned int CountDan)
{
  unsigned int i;

  for(i=0;i<CountDan;i++)
  {
    pAdrDst[i]=btCode;
    if(!(i & 0xfff)) ResetWatchDog();
  }
}

bool CheckLdr(bool bFlash)
{
  unsigned int i,CountDan,dwPgmSz;
  unsigned char *pAdr2SDRAM,*pbtMD5;
  md5_byte_t digest[16];
  md5_state_t state_md5;

  pAdr2SDRAM=(unsigned char *)(dAddrLdr2SDRAM+8); // ��������� ����� ������ �������� � SDRAM
  CountDan=2; // ������� � ������
  if(bFlash)
  {
    // ������ ������ ��������
    RdFlashP((unsigned short *)(dAddrLdr2Flash+4),(unsigned short *)&dwPgmSz,&CountDan);
    // ������ �������� � ����� � SDRAM
    for(i=0x000;i<dwPgmSz;i+=0x100)
    {
      CountDan=0x80; // ������� � ������
      RdFlashP((unsigned short *)(dAddrLdr2Flash+8+i),(unsigned short *)(pAdr2SDRAM+i),&CountDan);
      if((i & 0xfff)==0)
      {
        LedToggle(ledPult);
        ResetWatchDog();
      }
    }
  }
  else dwPgmSz=*((unsigned int *)(dAddrLdr2SDRAM+4));
  dwPgmSz&=0xffffff00;
  // ������� MD5 ��������
  md5_init(&state_md5);
  md5_append(&state_md5,(const md5_byte_t *)pAdr2SDRAM,dwPgmSz);
  md5_finish(&state_md5,digest);

  // ��������� ����� MD5
  pbtMD5=(unsigned char *)Buf4Ldr;
  CountDan=8;
  if(bFlash) RdFlashP((unsigned short *)(dAddrLdr2Flash+8+dwPgmSz+0x100),Buf4Ldr,&CountDan);
  else memcpy(pbtMD5,(unsigned char *)(dAddrLdr2SDRAM+8+dwPgmSz+0x100),CountDan << 1);
  
  // ���������� MD5
  for(i=0;i<16;i++)
  {
    if(digest[i]!=pbtMD5[i]) return(false);
  }
  return(true);
}

bool ZagrLdr(bool bFlash)
{
  unsigned int CountDan,AdrDst;
  short i,Flags;
  unsigned short *AdrF;
  bool bDMA;

  if(bFlash) AdrF=(unsigned short *)(dAddrLdr2Flash+8); // ����� ����
  else AdrF=(unsigned short *)(dAddrLdr2SDRAM+8);
  do
  {
    bDMA=true;
    LedToggle(ledPult);
    // -------- ������ header -----------------
    CountDan = 5; // ������� � ������
    if(bFlash) RdFlashP(AdrF, Buf4Ldr, &CountDan); 
    else memcpy(Buf4Ldr,AdrF,CountDan << 1);
    AdrF += CountDan;
    AdrDst=Buf4Ldr[1];
    AdrDst=(AdrDst << 16) | Buf4Ldr[0];
    CountDan=Buf4Ldr[3];
    CountDan=(CountDan<<16) | Buf4Ldr[2];
    Flags=Buf4Ldr[4]; // ����
    if(!(Flags & 1))
    { // ������ ������ � ������
      if(CountDan & 0x1) CountDan++;
      CountDan>>=1; // ���������� ����
      if(!CountDan) return(false);
      if(CountDan>0x8000) return(false);  // !������ ����� �� ����� ���� > 64 �����!
      if(bFlash) RdFlashP(AdrF, Buf4Ldr, &CountDan);
      else memcpy(Buf4Ldr,AdrF,CountDan << 1);
      AdrF+=CountDan;
      if(AdrDst & 0x1)
      {
        bDMA=false;
        memcpy((unsigned char *)AdrDst,Buf4Ldr,CountDan << 1);
      }
      else
      {
        *pMDMA_S0_X_COUNT = CountDan;// ���������� ����
        *pMDMA_D0_X_COUNT = CountDan;
        *pMDMA_S0_X_MODIFY = 2;
      }
    }
    else
    { // ������ � ������ �����
      if((AdrDst & 0x1) || (CountDan & 0x1) ||
         ((AdrDst>=0xffb00000) && (AdrDst<=0xffb00fff))) // scratch-memory area
      {
        bDMA=false;
        MemSet((unsigned char *)AdrDst,0x00,CountDan); // ����� CountDan � ������!!
      }
      else
      {
        CountDan>>=1; // ���������� ����
        Buf4Ldr[0] = 0;
        *pMDMA_S0_X_MODIFY = 0;
        *pMDMA_S0_X_COUNT = CountDan;
        *pMDMA_D0_X_COUNT = CountDan;
      }
    }
    if(bDMA)
    {
      *pMDMA_S0_START_ADDR = Buf4Ldr;
      *pMDMA_D0_START_ADDR = (void *)AdrDst;
      *pMDMA_D0_X_MODIFY = 2;
      *pMDMA_S0_CONFIG = DMAEN |WDSIZE_16 | FLOW_1; //0x1001
      ssync();
      *pMDMA_D0_CONFIG = DMAEN |WDSIZE_16 | WNR  | FLOW_1 | DI_EN; //0x1083
      ssync();
      while(!(*pMDMA_D0_IRQ_STATUS & 1)) ResetWatchDog();
      ssync();
      *pMDMA_S0_CONFIG = 0; // ������ DMA
      *pMDMA_D0_CONFIG = 0; 
      *pMDMA_D0_IRQ_STATUS = 1;// ����� ����������
      ssync();
    }
    ResetWatchDog();
  }
  while(!(Flags & 0x8000));
  return(true);
}

