#include "Sinfo.h"
#include "..\tcpip\socket.h"

#define d_LedTOut       0x3fff

/////////////////////////////////////////////////////////////////////
static char FwVersionInfo[4][24]=
{
__DATE__, // build date
__TIME__, // build time
"07.01.00", // version number
"Loader MU" // application name
};

/////////////////////////////////////////////////////////////////////
extern short Err_Flash;

/////////////////////////////////////////////////////////////////////
IPPARAMS_TP g_IpParams=
{
{ 192, 168, 1, 32 }, // IP address
{ 255, 255, 255, 0 }, // Subnet mask
{ 0, 0, 0, 0 }, // Gateway address
{ 0, 8, 0xDC, 0, 0, 0 }, // MAC address - DO NOT USE THIS ON A PUBLIC NETWORK!
0xFFFF, // �������� ���� ��� ������������ �� ������� 4 �����
161, // wPortSNMP
5002, // wPortSound
4000, // wPortControl
80, // wPortHTTP
{ 0, 0, 0, 0 }, // IP address SNMP manager ��������
{ 0, 0, 0, 0 }, // IP address SNMP manager ���������
{ 0, 0, 0, 0 }, // IP address ������� �����������
{ 0, 0, 5, 0, 0, 0 }, // VLANID ��� ������ 0-5 Marvell
{ 0, 0, 0, 0, 0, 0 } // VLAN ��������� ��� ������ 0-5 Marvell
};

static unsigned char s_btUdpBuf[d_BufMaxSz];

/////////////////////////////////////////////////////////////////////
void ToogleWD(void)
{ // !! ������� ��� ������� !!
  unsigned i,j;

  while(1)
  {
    for(i=0;i<0x4;i++)
    {
      *pFIO_FLAG_S=PF0;
      ssync();
    }
    *pFIO_FLAG_C=PF0;
    ssync();
    for(i=0;i<0xff;i++)
    {
      *pFIO_FLAG_C=PF0;
      ssync();
    }
  }
}

//################ �-��� ��������� �������� ������� � �������� #################
unsigned int g_dwSystemTick=0,g_dwInternalTakt=0;

unsigned int xTaskGetTickCount(void)
{
  g_dwSystemTick=0; 
  g_dwInternalTakt=0;
  return(0);
}

unsigned char IsCurrentTickCountGT(unsigned int dwTick0)
{
  if(g_dwInternalTakt<50) g_dwInternalTakt++;
  else
  {
    g_dwInternalTakt=0;
    g_dwSystemTick++;
    if(g_dwSystemTick>dwTick0) return(1);
  }
  return(0);
}

unsigned char IsCurrentTickCountLT(unsigned int dwTick0)
{
  if(g_dwInternalTakt<50) g_dwInternalTakt++;
  else
  {
    g_dwInternalTakt=0;
    g_dwSystemTick++;
    if(g_dwSystemTick>dwTick0) return(0);
  }
  return(1);
}

void vTaskDelay(unsigned int dwDelay)
{
  unsigned int dwTm=xTaskGetTickCount()+dwDelay;
  while(IsCurrentTickCountLT(dwTm));
}

////////////////////////////////////////////////////////////////////////////////////////
void Init_PLL(void)
{ 
#define MSEL		60 // ��������� CLKIN
#define CSEL		0  // �������� VCO ��� CCLK = 491.52 ��� 
#define SSEL		10 // �������� VCO ��� SCLK = 49.152 ��� (20,356 ��)
#define BYPASS_ 	0
#define Tout		0
#define TIn			0
#define PDWN_		0
#define STOPCK		0
#define PLL_OFF_	0
#define DF			1  // ������������ CLKIN (VCO = 16.384 * 60 / 2 = 491.52 ��� )

  *pPLL_CTL = (MSEL <<9)|(BYPASS_ <<8)|(Tout <<7)|(TIn <<6)|(PDWN_ <<5)|(STOPCK <<3)|(PLL_OFF_ <<1)| DF;
  idle(); // put in idle
  *pPLL_DIV = (CSEL <<4) | SSEL;
  ssync();
}

void Init_EBIU(void)
{
    // 1 ���� CLOOUT =~20 nc
// ���� 0 Flash Am29LV
#define BOWAT		4 // ������������ WE
#define BORAT		4 // ������������ RE
#define BOHAT		1 // ����� �� ������ FJT
#define BOSAT		1 // ����� �� ��������� WE ��� RE
#define BORDYP		1 // ���������� RDY
#define BORDY		0 // ���������� RDY
// ���� 1 Ethernet 
#define B1WAT		4 // ������������ WE
#define B1RAT		4 // ������������ RE
#define B1HAT		1 // ����� �� ������ FJT
#define B1SAT		1 // ����� �� ��������� WE ��� RE
#define B1RDYP		1 // ���������� RDY
#define B1RDY		0 // ���������� RDY
// ���� 2 RS232
#define B2WAT		4 // ������������ WE
#define B2RAT		4 // ������������ RE
#define B2HAT		1 // ����� �� ������ FJT
#define B2SAT		1 // ����� �� ��������� WE ��� RE
#define B2RDYP		1 // ���������� RDY
#define B2RDY		0 // ���������� RDY
// ���� 3 RS485
#define B3WAT		4 // ������������ WE
#define B3RAT		4 // ������������ RE
#define B3HAT		1 // ����� �� ������ FJT
#define B3SAT		1 // ����� �� ��������� WE ��� RE
#define B3RDYP		1 // ���������� RDY
#define B3RDY		0 // ���������� RDY

#define AMBEN		4 // ���������� ������
#define AMCKEN_		1 // ���������� CLKOUT

  *pEBIU_AMBCTL0=(B1WAT<<28)|(B1RAT<<24)|(B1HAT<<22)|(B1SAT<<20)|(B1RDYP<<18)|(B1RDY<<17)|
                 (BOWAT<<12)|(BORAT<<8)|(BOHAT<<6)|(BOSAT<<4)|(BORDYP<<2)| BORDY;
  *pEBIU_AMBCTL1=(B3WAT<<28)|(B3RAT<<24)|(B3HAT<<22)|(B3SAT<<20)|(B3RDYP<<18)|(B3RDY<<17)|
                 (B2WAT<<12)|(B2RAT<<8)|(B2HAT<<6)|(B2SAT<<4)|(B2RDYP<<2)| B2RDY; 
  *pEBIU_AMGCTL=0x00f0 | (AMBEN<<1) | AMCKEN_;
}

bool GetMACAddrFromIp(unsigned int dwAddr,unsigned char *MACAddr)
{ return(false); }

void LedPulse()
{
  static unsigned int dwLedTOut=d_LedTOut;
  
  if(dwLedTOut) dwLedTOut--;
  else
  {
    dwLedTOut=d_LedTOut;
    LedToggle(ledRabota);
    LedToggle(ledUpr);
  }
}

void ResetWatchDog(void)
{
  unsigned i;

  for(i=0;i<0x4;i++)
  {
    *pFIO_FLAG_S=PF0;
    ssync();
  }
  *pFIO_FLAG_C=PF0;
  ssync();
  LedPulse();
}

void StartProgram()
{
  long Addr=0xFFA00000;
  (*(FuncPtr)Addr)();
}

void WriteBuf2Flash(unsigned short Addr2Flash,unsigned short *Buf,unsigned char Sz)
{
  unsigned char i;
  for(i=0;i<Sz;i+=2) WrFlash(*Buf++,Addr2Flash+i); 
}
  
void ReadFlash2Buf(unsigned short *Buf,unsigned short Addr2Flash,unsigned char Sz)
{
  unsigned char i;
  for(i=0;i<Sz;i+=2) *Buf++=RdFlash(Addr2Flash+i);
}

void InitIP(void)
{
  unsigned int csum;

  // ��������� IP-������ �� Flash-������ ��� Default
  if(*pFIO_FLAG_D & 0x40)
  {
    csum=RdFlash(TCPIPStack);
    if(csum!=0xFFFF)
    { // ����� �� Flash � ��������� ������ ������������� Default
      if(*pFIO_FLAG_D & PF13) ReadFlash2Buf((unsigned short *)&g_IpParams,TCPIPStack,sizeof(g_IpParams));
      else WriteBuf2Flash(TCPIPStack,(unsigned short *)&g_IpParams,sizeof(g_IpParams)); // ������ �������� �� ��������� �� ����
    }
    else WriteBuf2Flash(TCPIPStack,(unsigned short *)&g_IpParams,sizeof(g_IpParams)); // ������ �������� �� ��������� �� ����
  }
  initWiznet((uint8 *)g_IpParams.ucDataSHAR,(uint8 *)g_IpParams.ucDataGAR,(uint8 *)g_IpParams.ucDataMSR,(uint8 *)g_IpParams.ucDataSIPR);
  socketUDP(d_CommandSocket,g_IpParams.wPortControl,0);
}

bool RcvUDPCmd(sCommand *psCmd)
{
  unsigned char IPadr[4],*pBt;
  unsigned short i,port,UdpSize;
  unsigned int csum;
  
  UdpSize=getsizeUDP(d_CommandSocket,false);
  if(UdpSize>0)
  {
    if(UdpSize>d_BufMaxSz) UdpSize=d_BufMaxSz;
    UdpSize=recvfrom(d_CommandSocket,s_btUdpBuf,UdpSize,IPadr,&port);
    if(UdpSize<3) return(false); // ����������� ������ ����: ����������,��� �������,CRC
    // �������� ������� �����������
    if(s_btUdpBuf[0]!=d_SyncByte) return(false);
    // ������� ����������� �����
    for(i=0,csum=0;i<UdpSize-1;i++) csum+=s_btUdpBuf[i];
    // �������� ����������� �����
    if((csum & 0xFF)!=s_btUdpBuf[UdpSize-1]) return(false);
    psCmd->Length=UdpSize-2; // ����� ������ �������
    if(psCmd->Length>d_XchgMaxSz) return(false);
    pBt=(unsigned char *)&psCmd->A1;
    for(i=0;i<4;i++) *pBt++=IPadr[i];
    psCmd->PORT1=port;
    for(i=0;i<psCmd->Length;i++) psCmd->BufCommand[i]=s_btUdpBuf[i+1];
    return(true);
  }
  return false;
}

void SetReply73Hdr(sInfoKadr *psInfoS1,sCommand *psCmd,unsigned short MaxByte)
{
  char i;

  psCmd->BufCommand[6]=MaxByte >> 8;
  psCmd->BufCommand[7]=MaxByte & 0xff;
  psInfoS1->L=8+MaxByte;
  psInfoS1->ByteInfoKadr[0]=73;
  for(i=1;i<8;i++) psInfoS1->ByteInfoKadr[i]=psCmd->BufCommand[i]; 
}

unsigned int GetCmdAddrAndSize(unsigned short *pCount,sCommand *psCmd)
{
  unsigned short Count;
  unsigned int Addr;

  Addr=psCmd->BufCommand[2];
  Addr=(Addr << 8) | psCmd->BufCommand[3];
  Addr=(Addr << 8) | psCmd->BufCommand[4];
  Addr=(Addr << 8) | psCmd->BufCommand[5];

  Count=psCmd->BufCommand[6];
  Count=(Count << 8) | psCmd->BufCommand[7];
  if(Count>sizeof(d_BufMaxSz)-8) Count=(unsigned short)(sizeof(d_BufMaxSz)-8);
  *pCount=Count;
  return(Addr);
}

bool ProcessUDPCmd(sCommand *psCmd,sInfoKadr *psReply)
{
  unsigned int i,DanMU;
  unsigned short CountByte,Adr2SPI,*Adr2Par;
  unsigned char *Adr2MU;

  psReply->A1 = psCmd->A1;
  psReply->PORT1 = psCmd->PORT1;
  switch(psCmd->BufCommand[0])
  { // ��� �������
    case 60:
    { // ������� ������ ������ �� � ������ ������ ��
      psReply->L=1+4*24;
      psReply->ByteInfoKadr[0]=60; // ������� 60
      Adr2MU=(unsigned char *)FwVersionInfo;
      for(i=0;i<(4*24);i++) psReply->ByteInfoKadr[i+1]=*Adr2MU++;
      break;
    }
    case 72:
    { // ������� ������ ������ ��
      switch(psCmd->BufCommand[1])
      {
        case 0: // ������ ������
          Adr2MU=(unsigned char *)GetCmdAddrAndSize(&CountByte,psCmd);
          SetReply73Hdr(psReply,psCmd,CountByte);
          for(i=0;i<CountByte;i++) psReply->ByteInfoKadr[8+i]=Adr2MU[i];
          break;
        case 1: // ������ ��������
          psCmd->BufCommand[5]&=0xFE;
          Adr2MU=(unsigned char *)GetCmdAddrAndSize(&CountByte,psCmd);
          CountByte&=0xfffc;
          SetReply73Hdr(psReply,psCmd,CountByte);
          for(i=0;i<CountByte;i+=4)
          {
            *pMDMA_S0_START_ADDR =(void *)Adr2MU;
            *pMDMA_D0_START_ADDR = &DanMU;
            *pMDMA_S0_X_COUNT = 1;
            *pMDMA_D0_X_COUNT = 1;
            *pMDMA_S0_X_MODIFY = 0;
            *pMDMA_D0_X_MODIFY = 0;
            *pMDMA_S0_CONFIG = DMAEN |WDSIZE_32 | FLOW_1; //0x1001
            ssync();
            *pMDMA_D0_CONFIG = DMAEN |WDSIZE_32 | WNR  | FLOW_1 | DI_EN; //0x1083
            ssync();
            while(!(*pMDMA_D0_IRQ_STATUS & 1));
            ssync();
            *pMDMA_S0_CONFIG = 0; // ������ DMA
            *pMDMA_D0_CONFIG = 0; 
            *pMDMA_D0_IRQ_STATUS = 1;// ����� ����������
            ssync();
            psReply->ByteInfoKadr[8+i] = DanMU & 0x000000FF;
            psReply->ByteInfoKadr[8+i+1] = ((DanMU & 0x0000FF00) >> 8);
            psReply->ByteInfoKadr[8+i+2] = ((DanMU & 0x00FF0000) >> 16);
            psReply->ByteInfoKadr[8+i+3] = ((DanMU & 0xFF000000) >> 24);
            Adr2MU+=4;
          }
          break;
        case 2: // ��� ������ - ���� SPI 
          Adr2SPI=(unsigned short)GetCmdAddrAndSize(&CountByte,psCmd);
          SetReply73Hdr(psReply,psCmd,CountByte);
          for(i=0;i<CountByte;i+=2)
          {
            DanMU = RdFlash(Adr2SPI);
            if(Err_Flash) return(false); //  ������ ������ ����
            psReply->ByteInfoKadr[8+i] = DanMU & 0x00FF;
            psReply->ByteInfoKadr[9+i] = DanMU >> 8;
            Adr2SPI+=2;
          }
          break;
        case 3: // ��� ������ - ���� ������������
          psCmd->BufCommand[5]&=0xFE;
          Adr2Par=(unsigned short *)GetCmdAddrAndSize(&CountByte,psCmd);
          CountByte&=0xfffe;
          SetReply73Hdr(psReply,psCmd,CountByte);
          i=CountByte >> 1;
          RdFlashP(Adr2Par,(unsigned short *)(psReply->ByteInfoKadr+8),&i);
          break;
        default: return(false);
      }
      break;
    }
    case 74:
    { // ������� ������ ������ � ������ ��
      psCmd->BufCommand[5]&=0xFE;
      Adr2MU=(unsigned char *)GetCmdAddrAndSize(&CountByte,psCmd);
      switch(psCmd->BufCommand[1]) // ��� ������
      {
        case 0: // ������ ������
          for(i=0;i<CountByte;i++) Adr2MU[i]=psCmd->BufCommand[8+i];
          break;
        case 1: // ������ ��������
          for(i=0;i<CountByte;i++)
          {
            *pMDMA_S0_START_ADDR = psCmd->BufCommand+8+i;
            *pMDMA_D0_START_ADDR = (void *)Adr2MU;
            *pMDMA_S0_X_COUNT = 1;
            *pMDMA_D0_X_COUNT = 1;
            *pMDMA_S0_X_MODIFY = 0;
            *pMDMA_D0_X_MODIFY = 0;
            *pMDMA_S0_CONFIG = DMAEN |WDSIZE_8 | FLOW_1; //0x1001
            ssync();
            *pMDMA_D0_CONFIG = DMAEN |WDSIZE_8 | WNR  | FLOW_1 | DI_EN; //0x1083
            ssync();
            while(!(*pMDMA_D0_IRQ_STATUS & 1));
            ssync();
            *pMDMA_S0_CONFIG = 0; // ������ DMA
            *pMDMA_D0_CONFIG = 0; 
            *pMDMA_D0_IRQ_STATUS = 1;// ����� ����������
            ssync();
            Adr2MU++;
          }
          break;
        case 2: // ��� ������ - ���� SPI
          DanMU=(unsigned int)Adr2MU;
          Adr2SPI=DanMU & 0xffff;
          for(i=0;i<CountByte;i+=2)
          {
            DanMU=psCmd->BufCommand[9+i];
            DanMU=(DanMU << 8) | psCmd->BufCommand[8+i];
            if(!WrFlash(DanMU,Adr2SPI)) return(false);
            Adr2SPI+=2;
          }
          break;
        case 4: // ��� ������ - ����� ��������
          if((unsigned int)Adr2MU>=0x10000)
          {
            Adr2MU=Adr2MU-0x10000+dAddrLdr2SDRAM+8;
            memcpy(Adr2MU,psCmd->BufCommand+8,CountByte);
          }
          break;
        default: return(false);
      }
      psReply->L=7;
      psReply->ByteInfoKadr[0]=75;
      for(i=1;i<6;i++) psReply->ByteInfoKadr[i]=psCmd->BufCommand[i];
      psReply->ByteInfoKadr[6]=CodeStatusTrue;
      break;
    }
    case 85: // 
    {
      unsigned int WordContrl=GetPgmAddress();
      unsigned short Addr2Flash[2]={0xfffe,0xffff};

      if(WordContrl!=0xfffffffe)
      { // ���������� � �������� ���������� ������ �������� -2
        // ��� ������� ��������� �������� ����� � ��� ����� �����
        WrDeviceArray(dAddrLdr2Flash,Addr2Flash,2);
      }
      break;
    }
    case 86: // ������� �������  ��
    {
      if(psCmd->Length<20) StartProgram();
      else
      {
        unsigned int dwSz;

        dwSz=psCmd->BufCommand[1];
        dwSz=(dwSz << 8) | psCmd->BufCommand[2];
        dwSz=(dwSz << 8) | psCmd->BufCommand[3];
        dwSz=(dwSz << 8) | psCmd->BufCommand[4];
        *((unsigned int *)(dAddrLdr2SDRAM+4))=dwSz;
        dwSz&=0xffffff00;
        memcpy((unsigned char *)(dAddrLdr2SDRAM+8+dwSz+0x100),psCmd->BufCommand+5,16);
        if(CheckLdr(false) && ZagrLdr(false)) StartProgram();
      }
      
    }
    default: return(false);
  }
  return(true); // ���� ����� ��� �������� �� ����
}

bool SendUDPReply(sInfoKadr *psInfoE)
{
  unsigned short i,port,UdpSize;
  unsigned int csum;

  UdpSize=psInfoE->L;
  if(UdpSize>d_BufMaxSz-2) UdpSize=d_BufMaxSz-2;
  s_btUdpBuf[0]=d_SyncByte;
  for(i=1;i<=UdpSize;i++) s_btUdpBuf[i]= psInfoE->ByteInfoKadr[i-1];
  for(i=0,csum=0;i<=UdpSize;i++) csum+=s_btUdpBuf[i];
  s_btUdpBuf[i]=csum & 0xFF;
  sendto(d_CommandSocket,s_btUdpBuf,UdpSize+2,(unsigned char *)&psInfoE->A1,psInfoE->PORT1);
  return true;
}

void LoaderMainCode(void)
{
  *pFIO_INEN|= PF13|PF7|PF6; // ���������� ������ PF13, PF7, PF6
  *pFIO_DIR |= PF12|PF5|PF0; // ��������� �� ����� PF12,PF5, PF0
                             // (PF0 - ����� WD, PF5 - ���������, PF12 - ���������� ������ 74HC253
                             // ���� �� ������������: PF11 - IP CS, PF10 - RS-485 CS)
  ssync();
  
  *pFIO_FLAG_S = PF12 | PF5;
  *pFIO_FLAG_C = PF0; // ����� WD
  ssync();

  Init_PLL();
  Init_EBIU();
  InitLed(); // ���������� ����� InitSPI
  InitSPI();
  InitIP();
  if((*pFIO_FLAG_D & 0x40) && IsProgramInFlash())
  {
    if(CheckLdr(true) && ZagrLdr(true)) StartProgram();
  }
  {
    static sCommand sCmd;
    static sInfoKadr sReply;

    while(1)
    {
      if(RcvUDPCmd(&sCmd))
      {
        LedToggle(ledRabota);
        if(ProcessUDPCmd(&sCmd,&sReply)) SendUDPReply(&sReply);
      }
      ResetWatchDog();
    }
  }
}

