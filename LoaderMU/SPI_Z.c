#include <cdefBF532.h>
#include <ccblkfn.h>

#define SPI_Err     (MODF | TXE | RBSY | TXCOL)
#define Fl_PF4      0xEF10
#define Fl_PF2      0xFB04

//--------------------------------------------------------------------------//
void ResetWatchDog(void);

//--------------------------------------------------------------------------//
short Err_Flash=0;
unsigned short DataSave[2];

//--------------------------------------------------------------------------//
void CS2Down(void)
{
  char i;

  for(i=0;i<5;i++)
  { *pSPI_FLG=Fl_PF2; ssync(); }
}

void CS2Up(void)
{
  char i;

  for(i=0;i<5;i++)
  { *pSPI_FLG=0xFF00; ssync(); }
}

void CS4Down(void)
{
  char i;

  for(i=0;i<5;i++)
  { *pSPI_FLG=Fl_PF4; ssync(); }
}

void CS4Up(void)
{
  char i;

  for(i=0;i<5;i++)
  { *pSPI_FLG=0xFF00; ssync(); }
}

void SPI_SpeedHi(void)
{ *pSPI_BAUD = 2; }

void SPI_SpeedNormal(void)
{ *pSPI_BAUD = 5; }

void InitSPI(void)
{
  *pFIO_DIR|=PF2 | PF4;
  *pFIO_FLAG_S=PF2 | PF4; // 
  *pSPI_BAUD=5; // ��������� ������� ������ SPI SCK = SCLK/(2*SPIBAUD0) = 49152/10 ~ 4915 kHz
  *pSPI_CTL=0x5C05; // ��������� ������������ � ��������� SPI
  ssync();
}

unsigned short SPIO_IO(unsigned short Instr)
{
  unsigned short DanFlash,wCycle=0xfff;

  *pSPI_TDBR=Instr;
  ssync();
  while(wCycle)
  {
    if(*pSPI_STAT & RXS)
    {
      DanFlash=*pSPI_RDBR;
      Err_Flash=0;
      ssync();
      return(DanFlash); // ���������� SPI ����
    }
    ssync();
    wCycle--;
  }
  *pSPI_STAT=SPI_Err; // ����� ������
  Err_Flash=1; // ���������� SPI ���
  return 0x0f; 
}

bool SPIO_Wr(unsigned short Instr)
{
  SPIO_IO(Instr);
  return true;
}

bool SPIO_WrRd(unsigned short Instr,unsigned short *DanFlash)
{
  *DanFlash=SPIO_IO(Instr);
  return true;
}

bool AT25_ReadyWait(void)
{
  unsigned short wCycle=0xfff,DanFlash;

  while(wCycle)
  {
    CS4Down(); // ��������� PF4
    SPIO_IO(5);
    if(Err_Flash)
    { CS4Up(); return false; }
    DanFlash=SPIO_IO(0); // �������� �������� � ������ �������� ������� ����
    CS4Up(); // ���������� PF4
    if(Err_Flash) return false;
    if(!(DanFlash & 1)) return(true);
    wCycle--;
  }
  return false;
}

unsigned short RdFlash(unsigned short AdrFl)
{
  short i;
  unsigned short DanFlash=0;

  Err_Flash=0;
  if(AT25_ReadyWait())
  {
    CS4Down(); // ��������� PF4 � �������� ~80 ��
    SPIO_IO(3); // �������� ������� "������ ������ �� ������"
    if(!Err_Flash)
    { 
      *pSPI_CTL|=0x100;  // ����� ����� 16 ���
      ssync();
      SPIO_IO(AdrFl); // �������� ������
      if(!Err_Flash) DanFlash=SPIO_IO(0); // �������� �������� � ������ ������ 
    }
    CS4Up(); // ���������� PF4 � �������� ~80 ��
    *pSPI_CTL&=~0x100; // ����� ����� 8 ���
    ssync();
  }
  return DanFlash; // ��������� ������
}

bool AT25_WriteEnable(void)
{
  if(AT25_ReadyWait())
  {
    CS4Down(); // ��������� PF4
    SPIO_IO(6);
    CS4Up(); // ���������� PF4
  }
  return(!Err_Flash);
}

bool WrFlash(unsigned short DanFl,unsigned short AdrFl)
{
  bool ValRet=false;

  if(AT25_WriteEnable())
  { // ������ �� ���� ���������
    CS4Down(); // ��������� PF4 � �������� ~80 ��
    SPIO_IO(2); // �������� ������� "������ ������ � ������"
    if(!Err_Flash)
    { 
      *pSPI_CTL |= 0x100; // ����� ����� 16 ���
      SPIO_IO(AdrFl); // �������� ������
      if(!Err_Flash)
      { 
        SPIO_IO(DanFl); // ������ ������ 
        if(!Err_Flash) 
        {
          CS4Up(); // ���������� PF4
          *pSPI_CTL &= ~0x100; // ����� ����� 8 ���
          ssync();
          if(AT25_ReadyWait()) ValRet=true; // ������ ��������
        }
      }
    }
    CS4Up(); // ���������� PF4
    *pSPI_CTL&=~0x100; // ����� ����� 8 ���
    ssync();
  }
  return ValRet;
}

//////////////////////////////////////////////////////////////////////////
// ������ � Flash ������� AT45DB161
unsigned char GetStatus()
{
  unsigned short DanFlash=0;

  CS2Down(); // ��������� PF2
  SPIO_IO(0x57); // ������� ������ �������� ���������
  if(!Err_Flash) DanFlash=SPIO_IO(0); // �������� �������� � ������ �������� ������� ����
  CS2Up(); // ���������� PF2
  return DanFlash;
}

bool ResetFlash(void)
{
  return true;
}

// �������� ��������� Flash ������ ��� ���������� ������� ������\��������
// �������: TRUE - ���� �������� ��������� �������; FALSE - ���� �������� ��������� � �������
// ���������: unsigned short *lpAddr - ����� ������, ����������� �� ������� 2 ����
//            unsigned short Value - ������������ ������
bool CheckState(unsigned short *lpAddr, unsigned short Value)
{
  unsigned short wCycle=0xfff;

  while(wCycle--)
  {
    if(((GetStatus() & 0x80)!=0) && (!Err_Flash)) return true;
  }
  return false;
}

// ������ �������� Flash ������ (528 ��� 512 ����)
// �������: ������ � ������
unsigned short PageSize(void)
{ return 512; }

// ����������� ����� ������ � ������ Flash (����� ��������, ����� ����� � ��������)
// �������: ����� � ������� Flash
// ���������: unsigned int Addr - ����� ������
unsigned int AdrToPage(unsigned int Addr)
{
// if(PageSize()==512) 
 return(((Addr << 1) & 0xFFFC00) + (Addr & 0x1ff));
// else return(((Addr/528) << 10)+(Addr%528));
}

// ������� �������� ������� Flash ������.
// �������: TRUE - ���� �����, FALSE -���� ������
// ���������: unsigned int Addr - ����� ������, ����������� �� ������� �������� 528 (512) ����
//            int How - ����� ��������� ������ � ������ ������������� �� ������� 528 (512) ����
bool WrDeviceErase(unsigned int Addr,unsigned int How)
{
  unsigned int j,PageNum;

  for(j=Addr;j<(Addr+How);j+=PageSize())
  {
    PageNum=AdrToPage(j);
    CS2Down(); // ��������� PF2
    SPIO_IO(0x81); // ������� �������� ��������
    SPIO_IO(PageNum>>16);
    SPIO_IO(PageNum>>8);
    SPIO_IO(PageNum);
    CS2Up(); // ���������� PF2
    if(!CheckState(0,0)) return(false);
    ResetWatchDog();
  }
  return(true);
}

// ������ *lpHowOut ���� �� Flash �� ���������� ������
// �������: ������ TRUE
// ���������: unsigned short *lpInpAddr - ����� ������. ������ ���� �������� �� ������� 2 ������
//            unsigned short *lpOut - ����� ��������� ������(�� ����� 2 ����)
//            int *lpHowOut ���������� ���� ��� ������
/////////////////////////////////////////////////////////////////////////////////
bool RdFlashP(const unsigned short *lpInpAddr, unsigned short *lpOut,unsigned  int *lpHowOut)
{
  unsigned int Index,Data,DanFlash;
  unsigned int Address;
  unsigned int PageNum;
  unsigned int LastPage;
  unsigned int FirstPage;
  unsigned int j, k;

  *pSPI_BAUD=2; // ��������� �������� ������ �� ����� ������
  Address=(unsigned int)lpInpAddr;
  Index=0;
  LastPage=(Address + *(lpHowOut)*2) >> 9;
  FirstPage=Address >> 9;
  for(j=FirstPage;j<=LastPage;j++)
  { // �������� �� ���������� �������
    PageNum=AdrToPage(j*PageSize())+((j==FirstPage)?(Address & 0x1ff):0);
    CS2Down(); // ��������� PF2
    SPIO_IO(0x68); // ������� Continuous Array Read
    SPIO_IO(PageNum>>16);
    SPIO_IO(PageNum>>8);
    SPIO_IO(PageNum);
    for(k=0;k<4;k++) SPIO_IO(0);
    for(k=((j==FirstPage)?(Address & 0x1ff):0);
        k<((j==LastPage)?((Address+*(lpHowOut)*2)-LastPage*PageSize()):PageSize());
        k+=2)
    { // �������� ������ ��������
      Data=SPIO_IO(0);
      DanFlash=SPIO_IO(0);
      ssync();
      Data= Data | (DanFlash<<8);
      lpOut[Index++]=Data;
    }
    ResetWatchDog();
    CS2Up(); // ���������� PF2
  }
  *lpHowOut=Index;
  *pSPI_BAUD = 5; // ��������� �������� ������ ����� ������
  return true;
}

//////////////////////////////////////////////////////////////////////////
// ������ ������� ������ �� Flash.
// �������: TRUE - ���� �����, FALSE -���� ������
// ���������: unsigned int Addr - ����� ����-������, ����������� �� ������� 2 ����
//			unsigned short *lpInp - ��������� �� ������ ������� ������
//			int HowInp - ���������� ������������ ����
//////////////////////////////////////////////////////////////////////////
bool WrDeviceArray(unsigned int Addr0, unsigned short *lpInp, int HowInp)
{
  unsigned int Data;
  unsigned int Address,PageNum;
  unsigned int i,j,j_Start,j_End,
                 k,k_Start,k_End;

  if(Addr0 & 0x01) return(false);
  Address = Addr0 & 0x1FFFFF; // �������� ������� ����� ������, ������ ������ ����������
  *pSPI_BAUD = 2; 
  j_Start=Address >> 9;
  j_End=(Address+HowInp*2) >> 9;
  for(j=j_Start;j<=j_End;j++)
  { // �������� �� ���������� �������
    k_Start=(j==j_Start) ? (Address  & 0x1ff):0;
    k_End=(j==j_End) ? ((Address+HowInp*2)-j_End*PageSize()) : PageSize();
    if(((j==j_Start) && (k_Start!=0)) ||
       ((j==j_End) && (k_End!=PageSize())))
    { // ��������� ������ � ��������� �������� ���� � �����
      CS2Down(); // ��������� �������
      SPIO_IO(0x53); // ������� Memory Page to Buffer 1 Transfer
      SPIO_IO((j<<10)>>16);
      SPIO_IO((j<<10)>>8);
      SPIO_IO(0);
      CS2Up(); // ���������� �������
      if(!CheckState(0,0)) break;
    }
	for(k=k_Start;k<k_End;k+=2)
	{ // �������� ������ ��������
		CS2Down(); // ��������� �������
		SPIO_IO(0x84); // ������� Write to Buffer 1
		SPIO_IO(0);
		SPIO_IO(k>>8);
		SPIO_IO(k);
		Data=*lpInp++;
		SPIO_IO(Data);
		SPIO_IO(Data >> 8);
		CS2Up(); // ���������� �������
		for (i = 0; i < 25; i++); // �������� ~80 ��
	}
	CS2Down(); // ��������� �������
	SPIO_IO(0x83); // ������� Buffer1 To Memory Page without build-in erase
	SPIO_IO((j<<10)>>16);
	SPIO_IO((j<<10)>>8);
	SPIO_IO(0);
	CS2Up(); // ���������� �������
	if(!CheckState(0,0)) break;
    ResetWatchDog();
    Addr0+=PageSize();
  }
  *pSPI_BAUD = 5; 
  if(j>j_End) return true;
  return(false);
}

/////////////////////////////////////////////////////////////////////
#include "..\FreeRTOS/FreeRTOS.h"
#include "..\FreeRTOS/queue.h"
#include "..\FreeRTOS/semphr.h"

xSemaphoreHandle g_SPILock=NULL;

signed portBASE_TYPE xSemaphoreTake(xSemaphoreHandle sLock)
{ return(0); }

signed portBASE_TYPE xQueueSend(xQueueHandle xQueue, const void * pvItemToQueue, portTickType xTicksToWait)
{ return(0); }

signed portBASE_TYPE xQueueReceive(xQueueHandle xQueue,void *pcBuffer,portTickType xTicksToWait)
{ return(0); }

void vPortEnableSwitchTask(bool bEn)
{ }

