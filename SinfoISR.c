#include "..\Sinfo.h"
#include "GlobalData.h"

/////////////////////////////////////////////////////////////////////
void vTickISR(void);

/////////////////////////////////////////////////////////////////////
// ���
static volatile unsigned char s_btUartCmd=0,s_btUARTState=0;

/////////////////////////////////////////////////////////////////////
void SendErrFromUart(unsigned char cRxByte,unsigned char *pInBufUart,unsigned char ErrCode)
{
  pInBufUart[0]=ErrCode;
  pInBufUart[1]=3;
  pInBufUart[2]=cRxByte;
  CountErrNoReplyUart++;
  s_btUARTState=0;
  xQueueSendFromISR(xRxedChars,pInBufUart,pdFALSE,0);
}

// ��������� ���������� UART �� ������
void vCOM_1_Rx_ISR(void)
{
  static unsigned char cRxByte;
  static unsigned char LenGet=0,Num=0;
  static unsigned char pInBufUart[d_UARTRcvBufSz]; // ����� ��� ������ ������ �� ������� �� ���
  cRxByte=*pUART_RBR;
  if(*pUART_LSR);
  ssync();
  switch(s_btUARTState)
  {
    case 0:
      pInBufUart[0]=s_btUartCmd;
      if(cRxByte==s_btUartCmd) s_btUARTState=1; // ����� �� ������� ������
      else SendErrFromUart(cRxByte,pInBufUart,ErrAnswBadCmd);
      break;
    case 1:
/* ��� ���������� ���������� ����� ��� �� ����!!
      if((*pDSPID & 0xff)<5)
      { // ������� �����
        s_btUARTState=2;
        break;
      }*/
    case 2:
      LenGet=cRxByte;
      if(LenGet<3) SendErrFromUart(cRxByte,pInBufUart,ErrAnswBadLen);
      else
      {
        pInBufUart[1]=LenGet;
        Num=2;
        s_btUARTState=3;
      }
      break;
    case 3:
      pInBufUart[Num++]=cRxByte;
      if(Num==LenGet)
      {
        s_btUARTState=0;
        xQueueSendFromISR(xRxedChars,pInBufUart,0,0);
      }
      break;
    default:
      SendErrFromUart(cRxByte,pInBufUart,ErrInReply);
      break;
  }
}

// ��������� ���������� UART �� ��������
void vCOM_1_Tx_ISR(void)
{
// ������� ��� ��� ����������� ��������� ����������  if(*pUART_IIR); // ����� ����� ���������� �� ��������
  *pUART_IER&=~ETBEI; // ���������� ���������� �� ��������
  ssync();
}

void StopWaitReplyUART(void)
{
  static unsigned int dwIRQ;
  dwIRQ=cli();
  s_btUARTState=0;
  sti(dwIRQ);
}

static inline unsigned short GetUartLSR(void)
{
  static unsigned short wLSR;
  
  wLSR=*pUART_LSR;
  ssync();
  return(wLSR);
}

#define d_UartDelay     2500ul
void UartDelay(void)
{ // �������� ����� �������, ����� ��� ������� �� ��������� (1 ���� ������� ==20 ����������)
  unsigned int dwStart,dwStop;
  bool bGE;

  do
  { // ����, ���� ������ �������� ����� 0
    dwStart=(*pTIMER0_COUNTER);
    ssync();
    dwStop=dwStart+d_UartDelay;
  }
  while(dwStop<dwStart);
  dwStop=dwStart+d_UartDelay;
  if(dwStart>=d_UartDelay) bGE=true;
  else bGE=false;
  do
  {
    dwStart=(*pTIMER0_COUNTER);
    ssync();
    if(dwStop<dwStart) break;
    if((dwStart<d_UartDelay) && bGE) break;
  }
  while(1);
}

bool xUARTPutCmd(unsigned portCHAR *cOutChar)
{
  static unsigned int xLastWakeTime,i,iSz;

//SendStr2IpLogger("$DEBUG$xUARTPutCmd: start...");
  *pUART_IER=0; // ���������� ���������� �� �������,������ � ��������
  ssync();
  xLastWakeTime=xTaskGetTickCount()+10;
  while(!(GetUartLSR() & TEMT))
  { // ���� ������������ ����������� UART
    if(IsCurrentTickCountGT(xLastWakeTime)) return(false);
  }
  s_btUARTState=0;
  s_btUartCmd=cOutChar[0];
  *pUART_LCR|=EPS; // Even parity ��� �������
  ssync();
  *pUART_THR=s_btUartCmd;
  ssync();
  xLastWakeTime=xTaskGetTickCount()+10;
  while(!(GetUartLSR() & TEMT))
  {
    if(IsCurrentTickCountGT(xLastWakeTime)) return(false);
  }
  *pUART_LCR&=~EPS; // Odd parity ��� ��������� ����
  ssync();
  UartDelay();
  iSz=cOutChar[1];
/*{
  static char str[80];
  LockPrintf();
  sprintf(str,"$$DEBUG$xUARTPutCmd: send packet start (iSz=%d)",iSz);
  UnlockPrintf();
  SendStr2IpLogger(str);
}*/
  for(i=1;i<iSz;i++)
  {
    *pUART_THR=cOutChar[i];
    ssync();
    if(i<iSz-1)
    {
      xLastWakeTime=xTaskGetTickCount()+10;
      while(!(GetUartLSR() & TEMT))
      {
        if(IsCurrentTickCountGT(xLastWakeTime)) return(false);
      }
      UartDelay();
    }
  }
  *pUART_IER|=ERBFI; // ���������� ���������� �� ������
//SendStr2IpLogger("$DEBUG$xUARTPutCmd: finish!");
  return true;
}

/////////////////////////////////////////////////////////////////////
// ��������� ���������� �� Timer0, Uart, RS-485
unsigned char getRs485_Data(void);
unsigned char getRs485_ISR(void);
bool IsST16C1550(void);
void RcvByte_Rs485(void);

void ISR_SinfoIVG10(void)
{
  static unsigned int i;
  static int iPolar=1;

#ifndef SINFO_ENERGO
  // ���������� �� RS-485 /////////////////////////////////////////////
  i=*pSIC_ISR;
  ssync();
  if(i & IRQ_PFA)
  { // ���� ����� �� RS-485
    *pFIO_POLAR^=PF1; // �������� ���������� �� ������ 0-1
    ssync();
    if(iPolar || IsST16C1550())
    { // ��� SC16IS740 ���������� �� ������ 0 ������������, �� ������ 1 - ����������, � ��� ST16C1550 - ������������ ���!!!
      i=getRs485_ISR();
      if(i==0x4)
      { // ����� ����� �� RS-485 (���� ����������� � ����� �������� ����� - ����������� �����!)
        RcvByte_Rs485();
      }
    }
    iPolar^=1;
  }
#endif

  // ���������� �� Uart /////////////////////////////////////////////
  i=*pUART_IIR; //����������� ��������� ����������
  ssync();
  i&=0x7;
  switch(i)
  {
    case 0x06:
      i=*pUART_LSR;
      break;
    case 0x04:
      vCOM_1_Rx_ISR();
      break;
    case 0x02:
      vCOM_1_Tx_ISR();
      break;
  }

  // ���������� �� ������� //////////////////////////////////////////
  i=*pTIMER_STATUS;
  ssync();
  if(i & 0x0001)
  {
    vTickISR();
    *pTIMER_STATUS = 0x0001; // confirm interrupt handling
    *pFIO_FLAG_T=PF0; // toogle watchdog
    ssync();
  }
}

// ��������� ���������� IVG10 (Flag A, Timer0,UART)
void ISR_InterrIVG10(void);
void ISR_InterrIVG10Container(void)
{
  asm volatile ( ".global _ISR_InterrIVG10;");
  asm volatile ( ".extern _ISR_SinfoIVG10;");
  asm volatile ( "_ISR_InterrIVG10:");

  asm volatile ( "[--SP] = ASTAT;");
  asm volatile ( "[--SP] = FP;");
  asm volatile ( "[--SP] = RETS;" );
  asm volatile ( "CALL _CpuSaveContext;");

  asm volatile ( "CALL _ISR_SinfoIVG10;");

  asm volatile ( "CALL _CpuRestoreContext;");
  asm volatile ( "RETS = [SP++];" );
  asm volatile ( "FP = [SP++];" );
  asm volatile ( "ASTAT = [SP++];" );
  asm volatile ( "RTI;" );
}

// ��������� ���������� IVG8 (SPORT)
void ISR_InterrIVG8(void);
void ISR_InterrContainerIVG8(void)
{
  asm volatile ( ".global _ISR_InterrIVG8;");
  asm volatile ( ".extern _Sport_Int;");

  asm volatile ( "_ISR_InterrIVG8:");
  asm volatile ( "[--SP] = ASTAT;");
  asm volatile ( "[--SP] = FP;");
  asm volatile ( "[--SP] = RETS;" );
  asm volatile ( "CALL _CpuSaveContext;");

  asm volatile ( "CALL _Sport_Int;");

  asm volatile ( "CALL _CpuRestoreContext;");
  asm volatile ( "RETS = [SP++];" );
  asm volatile ( "FP = [SP++];" );
  asm volatile ( "ASTAT = [SP++];" );
  asm volatile ( "RTI;" );
}

// ��������� �������������� �������� ////////////////////////////////
void ISR_Exclude_Sinfo(void)
{
  unsigned int mySEQSTAT;

  mySEQSTAT=sysreg_read(reg_SEQSTAT) & 0x3f;
  if(mySEQSTAT==0x25) return;  // ��������� ����������, �� ����������
  sysreg_read(reg_RETX);
  AddToLog(lt_Error,ls_OS,lc_CriticalError,mySEQSTAT);  // ������ � ��������� ������
}

void ISR_Exclude(void);
void ISR_Exclude_Container(void)
{
  asm volatile ( ".extern _CpuRestoreContext;");
  asm volatile ( ".extern _CpuSaveContext;");
  asm volatile ( "_ISR_Exclude:");
  asm volatile ( ".global _ISR_Exclude;");
  asm volatile ( ".global _CpuSaveContext;");
  asm volatile ( ".global _CpuRestoreContext;");

  asm volatile ( "[--SP] = ASTAT;");
  asm volatile ( "[--SP] = FP;");
  asm volatile ( "[--SP] = RETS;" );
  asm volatile ( "CALL _CpuSaveContext;");
  asm volatile ( "[--SP] = RETX;");

  asm volatile ( "CALL _ISR_Exclude_Sinfo;");

  asm volatile ( "RETX = [SP++];" );
  asm volatile ( "CALL _CpuRestoreContext;");
  asm volatile ( "RETS = [SP++];" );
  asm volatile ( "FP = [SP++];" );
  asm volatile ( "ASTAT = [SP++];" );

  asm volatile ( "[--SP] = RETS;" );
  asm volatile ( "RETX = [SP++];" );
  asm volatile ( "CLI R0;" );
  asm volatile ( "idle;" );
  asm volatile ( "RTX;" );
}

