#include "..\Sinfo.h"
#include "GlobalData.h"

/////////////////////////////////////////////////////////////////////
extern char FwVersionInfo[4][24];
extern unsigned int TestBoard;
extern short Err_Flash;
extern unsigned int ldf_firmware_start,ldf_firmware_size;

/////////////////////////////////////////////////////////////////////
bool vCmdReadSoftVersionMU(unsigned char *CommandPort0,sInfoKadr *psInfoS1)
{
  unsigned char i;

  psInfoS1->L=121;
  psInfoS1->ByteInfoKadr[0]=60;
  memcpy(psInfoS1->ByteInfoKadr+1,(unsigned char *)FwVersionInfo,4*24);
#ifdef DIGITAL_STUDIO_CONTROLLER
  memset(psInfoS1->ByteInfoKadr+5+(4*24),0,19);
#else
  strcpy((char *)psInfoS1->ByteInfoKadr+1+(4*24),"��� ");
  for(i=0;i<MAX_MKA_BOARD;i++) 
  {
    if(((TestBoard >> (i<<1)) & 0x3)==2)
    {
      CommandPort0[0] = 53; // ��� ������ � ������
      CommandPort0[1] = 4; // ����� �������
      CommandPort0[2] = i<<2;
      CommandPort0[3] = 0;
      CommandPort0[4] = 0;
      return true; // ���� ������ ���
    }
  }
  psInfoS1->ByteInfoKadr[4*24+5]='-';
  memset(psInfoS1->ByteInfoKadr+6+(4*24),0,18);
#endif
  xQueueSend(xQueueReply[psInfoS1->Source],psInfoS1,0,0);
  return false; // ��� ������� ���
}

void SetReply73Hdr(sInfoKadr *psInfoS1,sCommand *psViewCommand,unsigned short MaxByte)
{
  psViewCommand->BufCommand[6]=MaxByte >> 8;
  psViewCommand->BufCommand[7]=MaxByte & 0xff;
  psInfoS1->L=8+MaxByte;
  psInfoS1->ByteInfoKadr[0]=73;
  memcpy(psInfoS1->ByteInfoKadr+1,psViewCommand->BufCommand+1,7);
}

unsigned short GetCmd72AddrAndSize(unsigned int *pAddr,sCommand *psViewCommand)
{
  unsigned short Count;
  unsigned int Addr;

  Addr=psViewCommand->BufCommand[2];
  Addr=(Addr << 8) | psViewCommand->BufCommand[3];
  Addr=(Addr << 8) | psViewCommand->BufCommand[4];
  Addr=(Addr << 8) | psViewCommand->BufCommand[5];

  Count=psViewCommand->BufCommand[6];
  Count=(Count << 8) | psViewCommand->BufCommand[7];
  *pAddr=Addr;
  return(Count);
}

void vCmdReadDataFromMU(sCommand *psViewCommand,sInfoKadr *psInfoS1)
{
  unsigned short i,Count;
  unsigned int Addr,DanMU;

  switch(psViewCommand->BufCommand[1])
  {
    case 0: // ������ ������
      Count=GetCmd72AddrAndSize(&Addr,psViewCommand);
      if(Count>240) Count=240;
      SetReply73Hdr(psInfoS1,psViewCommand,Count);
      memcpy(psInfoS1->ByteInfoKadr+8,(void *)Addr,Count);
      break;
    case 1: // ������ ��������
      psViewCommand->BufCommand[5]&=0xFE;
      Count=GetCmd72AddrAndSize(&Addr,psViewCommand);
      Count=Count & 0xfffc;
      if(Count>240) Count=240; // ���. 60 ���� ������ ��������
      SetReply73Hdr(psInfoS1,psViewCommand,Count);
      for(i=0;i<Count;i+=4)
      {
        *pMDMA_S0_START_ADDR=(void *)Addr;
        *pMDMA_D0_START_ADDR=&DanMU;
        *pMDMA_S0_X_COUNT = 1;
        *pMDMA_D0_X_COUNT = 1;
        *pMDMA_S0_X_MODIFY = 0;
        *pMDMA_D0_X_MODIFY = 0;
        *pMDMA_S0_CONFIG = DMAEN |WDSIZE_32 | FLOW_1; //0x1001
        ssync();
        *pMDMA_D0_CONFIG = DMAEN |WDSIZE_32 | WNR  | FLOW_1 | DI_EN; //0x1083
        ssync();
        while(!(*pMDMA_D0_IRQ_STATUS & 1));
        ssync();
        *pMDMA_S0_CONFIG = 0; // ������ DMA
        *pMDMA_D0_CONFIG = 0; 
        *pMDMA_D0_IRQ_STATUS = 1;// ����� ����������
        psInfoS1->ByteInfoKadr[8+i] = DanMU & 0xFF;
        psInfoS1->ByteInfoKadr[9+i] = (DanMU >> 8);
        psInfoS1->ByteInfoKadr[10+i] = (DanMU >> 16);
        psInfoS1->ByteInfoKadr[11+i] = (DanMU >>  24);
        ssync();
        Addr+=4;
      }
      break;
    case 2: // ��� ������ - ���� SPI AT25256
      Addr=psViewCommand->BufCommand[4];
      Addr=(Addr << 8) | psViewCommand->BufCommand[5];
      Count=psViewCommand->BufCommand[6];
      Count=(Count << 8) | psViewCommand->BufCommand[7];
      Count=Count & 0xfffe;
      if(Count>240) Count=240; // ���. 120 ���� ������ ���� SPI
      SetReply73Hdr(psInfoS1,psViewCommand,Count);
      memset(psInfoS1->ByteInfoKadr+8,0xa5,Count);
      for(i=0;i<Count;i+=2)
      {
        DanMU=RdFlash(Addr);
        if(Err_Flash) break;
        psInfoS1->ByteInfoKadr[8+i]=DanMU & 0x00FF;
        psInfoS1->ByteInfoKadr[9+i]=DanMU >> 8;
        Addr+=2;
      }
      break;
    case 3: // ��� ������ - ���� ������������ AT45DB161
      psViewCommand->BufCommand[5]&=0xFE;
      Count=GetCmd72AddrAndSize(&Addr,psViewCommand);
      Count=Count & 0xfffe;
      if(Count>240) Count=240;
      SetReply73Hdr(psInfoS1,psViewCommand,Count);
      memset(psInfoS1->ByteInfoKadr+8,0xa5,Count);
      DanMU=Count >> 1;
      RdFlashP((unsigned short *)Addr,(unsigned short *)(psInfoS1->ByteInfoKadr+8),&DanMU);
      break;
    default:
      return;
  }
  xQueueSend(xQueueReply[psInfoS1->Source],psInfoS1,0,0);
}

#define dAddrPgm2Flash          0x10000

unsigned char WritePgmBuf2Flash(unsigned int dwPgmSize0,unsigned char *pbtMD5)
{
  unsigned short DanP;
  unsigned int i,dwPgmSize,Addr,Count;
  unsigned char *pbtFirmware;
  md5_byte_t digest[16];
  md5_state_t state_md5;
  static unsigned char checkdata[0x100];

  dwPgmSize=dwPgmSize0;
  if(!dwPgmSize) return(CodeStatusFalse);
  pbtFirmware=(unsigned char *)&ldf_firmware_start;

  // �������� ���� ����� �����������
  md5_init(&state_md5);
  md5_append(&state_md5,(const md5_byte_t *)pbtFirmware,dwPgmSize & 0xFFFFFF00);
  md5_finish(&state_md5,digest);
  if(memcmp(digest,pbtMD5,16)) return(CodeStatusFalse);

  WrDeviceArray(dAddrPgm2Flash+4,(unsigned short *)&dwPgmSize,2);
  // ���������� ������ Firmware �� ����
  for(i=0;i<dwPgmSize;i+=0x100)
  {
    WrDeviceArray(dAddrPgm2Flash+8+i,(unsigned short *)(pbtFirmware+i),0x80);
//    if((i & 0xfff)==0) ResetWatchDog(true);
  }

  // ������� ���� ���������� ������
  dwPgmSize&=0xffffff00;
  md5_init(&state_md5);
  for(i=0;i<dwPgmSize;i+=0x100)
  {
    Count=0x80;
    RdFlashP((unsigned short *)(dAddrPgm2Flash+8+i),(unsigned short *)checkdata,&Count);
    md5_append(&state_md5,(const md5_byte_t *)checkdata, 0x100);
//    if((i & 0xfff)==0) ResetWatchDog(true);
  }
  md5_finish(&state_md5,digest);
  if(memcmp(digest,pbtMD5,16))
  {
    dwPgmSize=0xFFFFFFFF;
    WrDeviceArray(dAddrPgm2Flash,(unsigned short *)&dwPgmSize,2);
    return(CodeStatusFalse);
  }
  Addr=dAddrPgm2Flash+8+dwPgmSize+0x100;
  WrDeviceArray(Addr,(unsigned short *)pbtMD5,0x8); // ������ ����������� ����� �� ����
  i=0;
  WrDeviceArray(dAddrPgm2Flash,(unsigned short *)&i,2);
  return(CodeStatusTrue);
}

void vCmdWriteData2MU(sCommand *psViewCommand,sInfoKadr *psInfoS1)
{
  unsigned short DanP;
  unsigned int i,Count,Addr;
  unsigned char btErr,*pbtFirmware;
  bool bRes;

  btErr=CodeStatusTrue;
  psViewCommand->BufCommand[5]&=0xFE;
  Count=GetCmd72AddrAndSize(&Addr,psViewCommand);
  switch(psViewCommand->BufCommand[1])
  {

    case 0: // ������ ������
      memcpy((void *)Addr,psViewCommand->BufCommand+8,Count);
      break;
    case 1: // ������ �������� 
      for(i=0;i<Count;i++)
      {
        *pMDMA_S0_START_ADDR =psViewCommand->BufCommand+8+i;
        *pMDMA_D0_START_ADDR =(void *)Addr;
        *pMDMA_S0_X_COUNT = 1;
        *pMDMA_D0_X_COUNT = 1;
        *pMDMA_S0_X_MODIFY = 0;
        *pMDMA_D0_X_MODIFY = 0;
        *pMDMA_S0_CONFIG = DMAEN | WDSIZE_8 | FLOW_1; //0x1001
        ssync();
        *pMDMA_D0_CONFIG = DMAEN | WDSIZE_8 | WNR  | FLOW_1 | DI_EN; //0x1083
        ssync();
        while(!(*pMDMA_D0_IRQ_STATUS & 1));
        ssync();
        *pMDMA_S0_CONFIG = 0; // ������ DMA
        *pMDMA_D0_CONFIG = 0; 
        *pMDMA_D0_IRQ_STATUS = 1; // ����� ����������
        ssync();
        Addr+=1;
      }
      break;
    case 2: // ��� ������ - ���� SPI AT25256
//      Addr&=0xfffe;
      for(i=0;i<Count;i+=2)
      {
        DanP=psViewCommand->BufCommand[9+i];
        DanP=(DanP << 8) | psViewCommand->BufCommand[8+i];
        bRes=WrFlash(DanP,Addr);
        if(!bRes) break;
        Addr+=2;
      }
      break;
    case 3: // ��� ������ - ���� ������������ AT45DB161
      if(!WrDeviceArray(Addr,(unsigned short *)(psViewCommand->BufCommand+8),Count >> 1)) 
      { btErr=CodeStatusFalse; }
      break;
    case 4: // ��� ������ - ����� ��������
      if(Addr>=0x10000)
      {
        Addr-=0x10000;
        pbtFirmware=(unsigned char *)&ldf_firmware_start;
        memcpy(pbtFirmware+Addr,psViewCommand->BufCommand+8,Count);
      }
      break;
    case 5: // ���������� ������ �� ������ � ������� ������
      EnableWrite2ProgrammArea(true);
      btErr=WritePgmBuf2Flash(Addr,psViewCommand->BufCommand+8); // � ���� ������� Addr �������� ������ �������� � ������!
      EnableWrite2ProgrammArea(false);
#ifndef DIGITAL_STUDIO_CONTROLLER
  #ifndef SINFO_ENERGO
      for(i=0;i<NumberKeyboard;i++) NeedShowCurrentKbdState(i);
  #endif
#endif
      break;
    default:
      btErr=CodeStatusFalse;
      break;
  }
  psInfoS1->L=7;
  psInfoS1->ByteInfoKadr[0]=75;
  memcpy(psInfoS1->ByteInfoKadr+1,psViewCommand->BufCommand+1,5);
  psInfoS1->ByteInfoKadr[6]=btErr;
  xQueueSend(xQueueReply[psInfoS1->Source],psInfoS1,0,0);
}

