//--------------------------------------------------------------------------//
// Header files																//
//--------------------------------------------------------------------------//
#include <cdefBF533.h>
#include <ccblkfn.h>
#include <sysreg.h>
#include <sys\exception.h>
#include <signal.h>
#include <time.h>
#include <stdio.h> 
#include <string.h> 
#include <heapinfo.h>

#include "..\FreeRTOS/FreeRTOS.h"
#include "..\FreeRTOS/queue.h"
#include "..\FreeRTOS/task.h"
#include "..\FreeRTOS/semphr.h"
#include "..\FreeRTOS/FileSys.h"

#include "..\LockPrintf.h"

#define CCLK                491520
#define SCLK                 98304  // CCLK/5

#define TIMOD_01            0x0001 // SPI transfer mode

#define CoeffMult           30 //����������� ��������� ����������

#define IVG_PLL_WAKEUP	P0_IVG
#define IVG_DMA_ERROR	P1_IVG
#define IVG_PPI_ERROR	P2_IVG
#define IVG_SPT0_ERROR	P3_IVG
#define IVG_SPI_ERROR	P4_IVG
#define IVG_SPT1_ERROR	P5_IVG
#define IVG_UART_ERROR	P6_IVG
#define IVG_RTC			P7_IVG
#define IVG_PPI			P8_IVG
#define IVG_SPT0_RX		P9_IVG
#define IVG_SPT0_TX		P10_IVG
#define IVG_SPT1_RX		P11_IVG
#define IVG_SPT1_TX		P12_IVG
#define IVG_SPI			P13_IVG
#define IVG_UART_RX		P14_IVG
#define IVG_UART_TX		P15_IVG
#define IVG_TIMER0		P16_IVG
#define IVG_TIMER1		P17_IVG
#define IVG_TIMER2		P18_IVG
#define IVG_PFA			P19_IVG
#define IVG_PFB			P20_IVG
#define IVG_MEMDMA0		P21_IVG
#define IVG_MEMDMA1		P22_IVG
#define IVG_SWDT		P23_IVG

//����� ��� SIC_IMASK 
#define IRQ_PLL_WAKEUP	0x00000001
#define IRQ_DMA_ERROR	0x00000002
#define IRQ_PPI_ERROR	0x00000004
#define IRQ_SPT0_ERROR	0x00000008
#define IRQ_SPT1_ERROR	0x00000010
#define IRQ_SPI_ERROR	0x00000020
#define IRQ_UART_ERROR	0x00000040
#define IRQ_RTC			0x00000080
#define IRQ_PPI			0x00000100
#define IRQ_SPT0_RX		0x00000200
#define IRQ_SPT0_TX		0x00000400
#define IRQ_SPT1_RX		0x00000800
#define IRQ_SPT1_TX		0x00001000
#define IRQ_SPI			0x00002000
#define IRQ_UART_RX		0x00004000
#define IRQ_UART_TX		0x00008000
#define IRQ_TIMER0		0x00010000
#define IRQ_TIMER1		0x00020000
#define IRQ_TIMER2		0x00040000
#define IRQ_PFA			0x00080000
#define IRQ_PFB			0x00100000
#define IRQ_MEMDMA0		0x00200000
#define IRQ_MEMDMA1		0x00400000
#define IRQ_SWDT		0x00800000

// SPORT0 word length
#define SLEN_16         0x000f
#define SLEN_8          0x0007

// DMA flow mode=1 - ����� ���������������
#define FLOW_1          0x1000

#define lengthTX0       48
#define lengthRX0       60
#define lengthTX1       64
#define lengthRX1       64

#define pFlash          (volatile unsigned char *)0x20000000
#define pIP             (volatile unsigned char *)0x20100000

#define THR             0x20 // ��� ���������� TX

#define pRHR	(volatile unsigned char *)0x202f0000
#define pTHR	(volatile unsigned char *)0x202f0000
#define pIER	(volatile unsigned char *)0x202f0002
#define pFCR	(volatile unsigned char *)0x202f0004
#define pISR	(volatile unsigned char *)0x202f0004
#define pLCR	(volatile unsigned char *)0x202f0006
#define pMCR	(volatile unsigned char *)0x202f0008
#define pLSR	(volatile unsigned char *)0x202f000a
#define pMSR	(volatile unsigned char *)0x202f000c
#define pSPR	(volatile unsigned char *)0x202f000e
#define pDLL	(volatile unsigned char *)0x202f0000
#define pDLM	(volatile unsigned char *)0x202f0002
#define pLED	(volatile unsigned char *)0x202f0010

#define pRHR1	(volatile unsigned char *)0x203f0000
#define pTHR1	(volatile unsigned char *)0x203f0000
#define pIER1	(volatile unsigned char *)0x203f0002
#define pFCR1	(volatile unsigned char *)0x203f0004
#define pISR1	(volatile unsigned char *)0x203f0004
#define pLCR1	(volatile unsigned char *)0x203f0006
#define pMCR1	(volatile unsigned char *)0x203f0008
#define pLSR1	(volatile unsigned char *)0x203f000a
#define pMSR1	(volatile unsigned char *)0x203f000c
#define pSPR1	(volatile unsigned char *)0x203f000e
#define pDLL1	(volatile unsigned char *)0x203f0000
#define pDLM1	(volatile unsigned char *)0x203f0002

#define p4r57600_ST16C1550 4
#define p4r38400_ST16C1550 6
#define p4r38400_SC16IS740 3

#define CodeStatusTrue  0
#define CodeStatusFalse 255

#define MAX_BUF_SIZE    1518    // ������ ������ ��� UDP - ������ ������!
#define SynchroByte     0x16

#define MAX(a,b)        ((a)>(b)?(a):(b))

#define d_NameLen       20

#define LenSound        240 // ����� ������ ������ �������� ����

#define LenFileName     16 // ������������ ����� ����� �����

typedef struct
{
    unsigned short Octet12;				// ��������� ������ RTP:
/* 	10..............					- V ������ ��������� = 2
	..0.............					- P ������� �������������� ������� - ������������
	...1............					- X ������� ���������� ���������
	....0000........					- �� ����� �������� ��������������� ����������
	........0.......					- M ������ 
	.........0001000					- PT ����������� ������� ���� ������ (8 - �-�����, 0 - u-�����)
*/ 
	unsigned short Number;				// ���������� �����
	unsigned int   Timer;				// ��������� �����
	unsigned int   SSRC;				// ������������� ��������� ������������� (SSRC)
	unsigned short ProFile;				// �������� ������� (� ������ ������ - ����� ������)
	unsigned short Len;					// == 0 ����� �������������� �������� (�� 32 ����)
	unsigned char  BufSpeak[LenSound];  // ����� ������� ������
} xListRTP;

#define d_MaxCmdBufSz               280
#define d_MaxReplyBufSz             280
#define d_MaxSoundIpPcktSz          MAX(sizeof(xListRTP),8+128*2)

//--------- ��������� ��������������� ����� ��� ������
typedef struct
{
  unsigned char Source; //�������� �������
  unsigned short L; // �����
  unsigned int A1; // ����� ����������
  unsigned short PORT1; // ���� ����������
  unsigned char ByteInfoKadr[d_MaxReplyBufSz]; //����� �����
} sInfoKadr;

//--------------- ��������� �������
typedef struct
{
  unsigned char Source; //�������� �������
  unsigned short L; // ����� ������ �������
  unsigned int A1; // ����� ����������
  unsigned short PORT1; // ���� ����������
  unsigned char BufCommand[d_MaxCmdBufSz]; //����� �������
} sCommand;

typedef struct
{
  unsigned short L; // �����
  unsigned int A1; // ����� ����������
  unsigned short PORT1; // ���� ����������
  unsigned char ByteInfo[d_MaxSoundIpPcktSz];
} sSoundIp;

#ifdef DIGITAL_STUDIO_CONTROLLER
#define d_MAXDSPCOM         5
#define d_MAXLOGGER         2
#define IRP_MAX             (d_MAXDSPCOM+d_MAXLOGGER)

#define STACK_ViewCommand   (configMINIMAL_STACK_SIZE+672)
#define STACK_TCPIP         (configMINIMAL_STACK_SIZE+896)
#define STACK_TFTP          (configMINIMAL_STACK_SIZE+512)
#define STACK_SportX        (configMINIMAL_STACK_SIZE+546)
#define STACK_SportXCmd     (configMINIMAL_STACK_SIZE+512)
#define STACK_ETHERCAT      (configMINIMAL_STACK_SIZE+4000)

/*#define STACK_ALL           (STACK_ViewCommand+STACK_TCPIP++STACK_TFTP+STACK_SportX+STACK_SportXCmd+STACK_ETHERCAT)
#if (STACK_ALL > 0x8000)
  #error "ERROR: stack for create tasks too large!!!"
#endif*/

// �������������� ����� ��� �������� � �����������
#define d_MaxTask               16

extern xTaskHandle              thTaskHandles[d_MaxTask];

#define thSportX                thTaskHandles[0]
#define thSportXCmd             thTaskHandles[1]
#define thViewCmd               thTaskHandles[2]
#define thTcpIp                 thTaskHandles[3]
#define thTFTP                  thTaskHandles[4]
#define thEthCat                thTaskHandles[5]

#define d_WDSTATE_NU1           0x0001
#define d_WDSTATE_NU2           0x0002
#define d_WDSTATE_SportX        0x0004
#define d_WDSTATE_SportXCmd     0x0008
#define d_WDSTATE_ViewCmd       0x0010
#define d_WDSTATE_NU3           0x0020
#define d_WDSTATE_TcpIp         0x0040
#define d_WDSTATE_NU4           0x0080
#define d_WDSTATE_TFTP          0x0100

#define d_WDSTATE_TASKALL (d_WDSTATE_SportX | d_WDSTATE_SportXCmd | \
                           d_WDSTATE_ViewCmd | d_WDSTATE_TcpIp | \
                           d_WDSTATE_TFTP)

#pragma pack(1)
typedef struct
{
  int L;
  unsigned char Buf[MAX_BUF_SIZE];
} ECAT_XCHG_TP;
#pragma pack()

#else

#define d_MAXDSPCOM         5
#define d_MAXRECORD         2
#define d_MAXLOGGER         2
#define IRP_MAX             (NumberKeyboard+d_MAXDSPCOM+d_MAXRECORD+d_MAXLOGGER)

#ifdef SINFO_ENERGO
#define AUDIO_PACKET_SIZE   512
#endif

#define STACK_ViewCommand   (configMINIMAL_STACK_SIZE+672)
#define STACK_TCPIP         (configMINIMAL_STACK_SIZE+768)
#define STACK_RS485         (configMINIMAL_STACK_SIZE+768)
#define STACK_TaskCall      (configMINIMAL_STACK_SIZE+896)
#define STACK_TFTP          (configMINIMAL_STACK_SIZE+512)
#define STACK_CheckMKATask  (configMINIMAL_STACK_SIZE+512)
#define STACK_SportX        (configMINIMAL_STACK_SIZE+546)
#define STACK_WaitSportX    (configMINIMAL_STACK_SIZE+512)

/*#define STACK_ALL           (STACK_ViewCommand+STACK_TCPIP+STACK_RS485+STACK_TaskCall+STACK_TFTP+STACK_CheckMKATask+STACK_SportX+STACK_WaitSportX)
#if (STACK_ALL > 0x8000)
  #error "ERROR: stack for create tasks too large!!!"
#endif*/

#define MAX_MKA_BOARD       12 // ������������ ���������� ���� ���
#define NumbChanMKA         4   // ���������� ������� �� ���
#define MAX_MKA_CHANNELS    (MAX_MKA_BOARD*NumbChanMKA)

#define MAX_DIGIT_TLFN      18  // ������������ ���-�� ���� � ������ (������ ������ ��-�� ����� "MTel1.dts")

#define d_UARTRcvBufSz      260

typedef struct
{
  char Name[33]; // ��� ���������
  unsigned short wConnectionPoint;
  unsigned char btNumb;
  unsigned char btStatus;
} sAbntSelectorInfo;

typedef struct
{
  unsigned short MKA_UART[MAX_MKA_BOARD];
} ERROR_COUNTERS_TP;

// �������������� ����� ��� �������� � �����������
#define d_MaxTask               16

extern xTaskHandle              thTaskHandles[d_MaxTask];

#define thRS485                 thTaskHandles[0]
#define thISDN                  thTaskHandles[1]
#define thSportX                thTaskHandles[2]
#define thSportXCmd             thTaskHandles[3]
#define thViewCmd               thTaskHandles[4]
#define thTaskCall              thTaskHandles[5]
#define thTcpIp                 thTaskHandles[6]
#define thCheckTask             thTaskHandles[7]
#define thTFTP                  thTaskHandles[8]
#define thCmdFromYuri           thTaskHandles[9]

/////////////////////////////////////////////////////////////////////
#define d_WDSTATE_Rs485         0x0001
#define d_WDSTATE_ISDN          0x0002
#define d_WDSTATE_SportX        0x0004
#define d_WDSTATE_SportXCmd     0x0008
#define d_WDSTATE_ViewCmd       0x0010
#define d_WDSTATE_TaskCall      0x0020
#define d_WDSTATE_TcpIp         0x0040
#define d_WDSTATE_ChecTask      0x0080
#define d_WDSTATE_TFTP          0x0100

#define d_WDSTATE_TASKALL (d_WDSTATE_Rs485 | \
                           d_WDSTATE_ISDN | d_WDSTATE_SportX | d_WDSTATE_SportXCmd | \
                           d_WDSTATE_ViewCmd | d_WDSTATE_TcpIp | d_WDSTATE_TaskCall | \
                           d_WDSTATE_ChecTask | d_WDSTATE_TFTP)
#endif

inline unsigned char WDSTATE2NUMB(unsigned int dwState)
{
  unsigned char btNumb=0;

  for(btNumb=0;btNumb<d_MaxTask;btNumb++)
  {
    if(dwState & 0x01) return(btNumb);
    dwState>>=1;
  }
  return(d_MaxTask);
}

#include "..\RTC.h"
#include "..\TCPIP.h"
#include "..\SPI.h"
#include "..\SNMP/Jornal.h"
#include "..\MD5/md5.h" 

// ���� ������ � ������ ����� ����������� � ��
#define ErrCS 					3 // ������ ����������� ����� �� ������ RS232 
#define ErrCodeQueueSend 		4 // ������� xQueueCommand �����������
#define ErrNoSinByte 			5 // �� ���� ����������� � �������
#define ErrProtokolUart 		6 // ������ ��������� Uart
#define ErrTimeOut 				7 // ����������� ����� �������� ����� ������� �� RS232
#define ErrNoCommand 			8 // �� ���������� �������� �������
#define ErrQueueUart 			251 // ������� xCharsForTx �� �����, � ����� ���������� ����� ������� 
#define ErrInReply 				11 // ������ � ������ ���
#define ErrAnswBadCmd           250 // ����� �� �� �� �������
#define ErrAnswBadLen           251 // ������������ ����� ������
#define ErrNoReplyUart 			252 // �� ������ ����� �� ���
#define NoCommand 				0xB0 //��� ����� �������

#ifdef DIGITAL_STUDIO_CONTROLLER
////////////////////////////////////////////////////////////////////////////////////////////
// !!!! ������ ������������ ������ ������� xQueueReply ������ ������ �� � ���� ������!!!! //
//                             (�������� ������� �� SPORTX � DSPCOM)                      //
#define QueueReplyCnt			8                                                         //
#define UserNU1                 0                                                         //
#define UserNU2 				1                                                         //
#define UserNU3 				2                                                         //
#define UserSportX				3                                                         //
#define UserNU4     			4                                                         //
#define UserNU5                 5                                                         //
#define UserUDP					6                                                         //
#define UserViewCmd				7                                                         //
////////////////////////////////////////////////////////////////////////////////////////////
#else
#include "..\Led.h"
#include "..\EDSS/libpri.h"
#include "..\SportX/SportXC.h"
#include "..\SportX/SportX.h"
#include "..\SportX/SportXFn.h"
#include "RS485/keyboard.h"

//#define __ADACE__           // ������� ���������� ������� �����

#define Tranzit             1 // ������� �������� ������� �� ��

//���������� ���������
#define CorrectKey          5
#define CorrectKIn          28
#define CorrectKOut         28
#define CorrectNoise        100
#define CorrectKoefNoise    32
#define CorrectNoiseUp      0x03
#define CorrectCorrF        9
#define NumbVar             8  // ���������� ��������� ����������
#define NEcho               49 //60 //���������� ���������������� (�� 2 ����� �� 1 �����������)

#define BaudRate 70// 70000 �������� �������� ������ �� UART
#define DEL_UART SCLK/(BaudRate*16)

////////////////////////////////////////////////////////////////////////////////////////////
// !!!! ������ ������������ ������ ������� xQueueReply ������ ������ �� � ���� ������!!!! //
//                             (�������� ������� �� SPORTX � DSPCOM)                      //
#define QueueReplyCnt			8                                                         //
#define UserNU1                 0                                                         //
#define UserSNMP				1                                                         //
#define UserRS485  				2                                                         //
#define UserSportX				3                                                         //
#define UserLoaderMKA			4                                                         //
#define UserNU2                 5                                                         //
#define UserUDP					6                                                         //
#define UserViewCmd				7                                                         //
////////////////////////////////////////////////////////////////////////////////////////////
#endif

// ----- ������ �������� �� ���� -----
#define OffsetFl         0

// ��� �������� �������
#define AdrFlashLastFile OffsetFl+0x0 // ����� ���������� ����� �� ����

#define AdrStartSave2XML OffsetFl+0x60 // ---------- ����������� � XML-����� ������� ����-������ (������) ---------

#ifdef DIGITAL_STUDIO_CONTROLLER
// ---------- ����������� � XML-����� ������� ����-������ (������ ��� �������� ��!!!) ---------
// ������� ������������
#define AdrErrTime		OffsetFl+0x398C // ������ ����� � ������� 2-� �����
#define AdrSysTime		OffsetFl+0x398E // ����� ��������� �������� ����� (4 �����)

#define AdrEndSave2XML  OffsetFl+0x3AC� // ---------- ����������� � XML-����� ������� ����-������ (�����+1) ---------
#else
// ---------- ����������� � XML-����� ������� ����-������ (������) ---------
#define AdrConditionKey OffsetFl+0x60 //��������� ������ 1 ����� �� 1 �����
#define AdrKIn	 		OffsetFl+0xC0 // ������� �������� 
#define	AdrKOut	 		OffsetFl+0x120 // �������� ��������
//#define	AdrAnalPorog 		OffsetFl+0x180 // ����������� ������� ������� �������
#define AdrMinARU		OffsetFl+0x1E0 //������������ �������� ���
#define AdrNoise4dB		OffsetFl+0x240 //����� ��������������
#define AdrKoefNoise 	OffsetFl+0x2A0 //���������� ������� ����� ������� ��������������
#define AdrCoefCorrF 	OffsetFl+0x300 //  ��� �������� �� ������� 3000 ��
#define AdrNoiseUp	 	OffsetFl+0x360 //���������� ������� ���������� ������� ��������������
#define AdrARU			OffsetFl+0x3C0 //��� ��������/���������
// ������� ������������� 128 ���� (0 - ����, 1-9 �������)
#define AdrFiltrEcho 	OffsetFl+0x420 
#define LengthEcho 		0x80 //���-�� ���� �� FLASH ��� 1-�� ������
// � 0x480 �� 0x1080 �������� 
#define AdrTypeQuit 	OffsetFl+0x1080 //(0x10e0) // ��������� ������ 600 ��
// ������ �������� ��: 1 - � ����������, 0 - � FLASH 
#define AdrZagrBK 		OffsetFl+0x1300 //0x1300-1301
#define AdrFr25 		OffsetFl+0x1302 //0x1302-1303 ������� ������ ��������� 25/50 ��
// � 0x1304 �� 0x1400 �������� 
#define AdrTypeCall 	OffsetFl+0x1400 // ��� ������ (112 �������*4 �����= 448 (0x1�0))
#define	AdrAnalPorog 	OffsetFl+0x15C0 // ����������� ������� ������� ������� (112 �������*2 �����= 224 (0xE0))
#define AdrReturnCoef 	OffsetFl+0x16A0 // ������� �������� (112 �������*2 �����= 224 (0xE0))
// 0x1780
#define TCPIPStack 		OffsetFl+0x2000 // ��������� ����� ���������� TCPIP 0x2000-0x20FF
#define MarvellInfo 	OffsetFl+0x203A // ������ � ������� ���������� Marvell 0x203A-0x203B
// ��� ������� MK�
#define AdrParamMKC		OffsetFl+0x2104 // ��������� ������� ��� (4 ������ * 8 ���� * 4 ��������)
#define AdrParamUKC		OffsetFl+0x2184 // 14 ����
						// ��������� ��-� (���������� ������ (2�.), ������������ ���������, ������������ SNMP, N ����������)

// ����� ��� �������� ���-�������������  
#define AdrBufEcho 		OffsetFl+0x2192 // �� 0�200 �� 1 ����� (LengthEcho =0x80 �� 1 �����) * 12 ���� = 0x1800

// ������� ������������
#define AdrErrTime		OffsetFl+0x398C // ������ ����� � ������� 2-� �����
#define AdrSysTime		OffsetFl+0x398E // ����� ��������� �������� ����� (4 �����)

#define AdrFTikker		OffsetFl+0x3992 // ������� �������
#define AdrMLayer		OffsetFl+0x3994 // ������ ��������� ���������� ��� 12-�� ������� (24 �����)
#define AdrSpeakInSel	OffsetFl+0x39AC // ������� ������� ������� ��� �������� (2 �����)
#define AdrBlockPult	OffsetFl+0x39AE // ���������� �������� ���������� ������(12 *2)
#define AdrPeriodTikker	OffsetFl+0x39C6 // ������ �������

#define AdrMinARUTP		OffsetFl+0x39C8 //������������ �������� ��� (64*2)
#define AdrARUTP		OffsetFl+0x3A4B //��� ��������/��������� (64*2)
#endif

#define AdrEndSave2XML  OffsetFl+0x3AC� // ---------- ����������� � XML-����� ������� ����-������ (�����+1) ---------

#define AdrLic			OffsetFl+0x3ACC // 4 ����� ��������� ������ ��

#define FreeAdr			OffsetF1+0x3AD0 // ������ ��������� �����

/////////////////////////////////////////////////////////////////////
unsigned int GET_DW(unsigned char *pbtBuf);
void SET_DW(unsigned char *pbtBuf,unsigned int dwData);
unsigned short GET_W(unsigned char *pbtBuf);

bool IsDigit(char chSb);
unsigned char *SaveInt2Buf(unsigned char *pbtBuf,int iData);
unsigned char *RestoreIntFromBuf(unsigned char *pbtBuf,int *piData);

void SendTaskState2Ip(void);
void SetTaskVar(unsigned int dwTask,unsigned char btOff,unsigned char btVar);
void SetWDState(unsigned int dwStateTask);

// ����� ����������� �������
void ResetWatchDog(bool bNow);
// ���������� ������ ����������� ������� ��� ����������� �������
void BlockWatchDog(void);

char *strclr(char *str);

unsigned char IsTickCountGT(unsigned int dwTick1,unsigned int dwTick0);
unsigned char IsCurrentTickCountGT(unsigned int dwTick0);
unsigned char IsTickCountLT(unsigned int dwTick1,unsigned int dwTick0);
unsigned char IsCurrentTickCountLT(unsigned int dwTick0);

signed portBASE_TYPE xQueueSendCmd(sCommand *psCmd,portTickType xTicksToWait);

void SendDiagnostics2DspCom(unsigned char *pData,unsigned char Len);
void SendStr2IpLogger(const char *fmt,...);

/////////////////////////////////////////////////////////////////////
void Init_EBIU(void);
void InitSPI(void);
void Init_Sport0(void);
void Init_Sport1(void);
void Init_DMA(void);
void Init_Timer0(void);
void Init_IP(void);
void Init_Interrupts(void);
void Enable_DMA_Sport(void);
void Init_Flags(void);

void Sport0_RX_TX(int val);
void Sport1_RX_TX(int val);
void Timer0(int val);

#ifndef DIGITAL_STUDIO_CONTROLLER
void InitUART(void);
#endif

