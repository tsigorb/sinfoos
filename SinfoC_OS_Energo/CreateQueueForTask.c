#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"

//*******************************************************************
xQueueHandle xQueueCommand; // �� ��������� ������
xQueueHandle xQueueTFTP;
xQueueHandle xQueueReply[QueueReplyCnt]; // ������� ��  �������
xQueueHandle xRxedChars; // UART Rx 
xQueueHandle xQueueSportX;
xQueueHandle xKeyProcessing; // ������� �������� ������� ������ �� ���������
xQueueHandle xPort485RX;
xQueueHandle xKeyIndic; // ������� �������� ��� ��������� �� ����������
xQueueHandle xQueueVxCall; // ������� �������� �������
xQueueHandle xQueueISDNCall; // ������� ��� ������� ���������� ���� ISDN
xQueueHandle xQueueSoundIpIn; // ������� ��� ������ �������� ������� ����� IP
xQueueHandle xQueueSoundIpOut; // ������� ��� �������� �������� ������� ����� IP
xQueueHandle xQueueAnswYuri;
xQueueHandle xQueueLogger;
xQueueHandle xQueueSound4Rec;

/////////////////////////////////////////////////////////////////////
// �������� �������� ��� �����
void CreateQueueForTask(void)
{
  short i;
  unsigned portBASE_TYPE MasQueueReplySz[QueueReplyCnt]=
{
  1,                 // UserNU1
  5,                 // UserSNMP
  25*NumberKeyboard, // UserRS485
  200,               // UserSportX
  5,                 // UserLoaderMKA
  1,                 // UserNU2
  100*NumberKeyboard, // UserUDP
  5*NumberKeyboard   // UserViewCmd
};

  xQueueCommand=xQueueCreate(500,sizeof(sCommand));
  if(!xQueueCommand) BlockWatchDog();
  xQueueTFTP=xQueueCreate(10,sizeof(sCommand));
  if(!xQueueTFTP) BlockWatchDog();
  for(i=0;i<QueueReplyCnt;i++)
  {
    xQueueReply[i]=xQueueCreate(MasQueueReplySz[i],sizeof(sInfoKadr));
    if(!xQueueReply[i]) BlockWatchDog();
  }
  xQueueSportX=xQueueCreate(200,sizeof(sCommand));
  if(!xQueueSportX) BlockWatchDog();
  xRxedChars=xQueueCreate(1,d_UARTRcvBufSz);
  if(!xRxedChars) BlockWatchDog();
  // �������� ������� �������� �������
  xQueueVxCall=xQueueCreate(NumberKeyboard*10,sizeof(sInfoKadr));
  if(!xQueueVxCall) BlockWatchDog();
  // �������� ������� �������� �������� �������
  xQueueSoundIpIn=xQueueCreate(NumberKeyboard*10,sizeof(xListRTP));
  if(!xQueueSoundIpIn) BlockWatchDog();
  xQueueSoundIpOut=xQueueCreate(NumberKeyboard*10,sizeof(sSoundIp));
  if(!xQueueSoundIpOut) BlockWatchDog();
  xQueueAnswYuri=xQueueCreate(1,sizeof(sCommand));
  if(!xQueueAnswYuri) BlockWatchDog();
  xQueueLogger=xQueueCreate(50,sizeof(sInfoKadr));
  if(!xQueueLogger) BlockWatchDog();
  xQueueSound4Rec=xQueueCreate(NumberKeyboard*10,sizeof(sSoundIp));
  if(!xQueueSound4Rec) BlockWatchDog();
}

