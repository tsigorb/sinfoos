#include "..\Sinfo.h"
#include "FunctionsPrototypes.h"
#include "GlobalData.h"

#ifdef __SNMP__
#include "SNMP\Snmp.h"
#endif

/////////////////////////////////////////////////////////////////////
unsigned char CheckRegistration(unsigned char btSN,unsigned char btIpPultCnt);
void DeleteAudioMessages4Pult(unsigned char SN);
void ReOpenSocket(unsigned char btSocket,unsigned short wPort,bool bSend);

/////////////////////////////////////////////////////////////////////
#define dIpPultCmdTOut    3000

typedef struct
{
  unsigned long int dwIp;
  unsigned short wPort;
  unsigned char btMAC[6];
  unsigned int dwTOut,dwTOutCmd;
  unsigned char btType,btSN;
  unsigned char btDS,btRS;
  unsigned short wCP4DS,wCP4RS;
  unsigned char btName[d_NameLen+1],btNumbUniq[MAX_DIGIT_TLFN+1],btNumbSerial[MAX_DIGIT_TLFN+1];
} IRP_TP;

/////////////////////////////////////////////////////////////////////
extern int g_iPultPingPong[NumberKeyboard];

static IRP_TP s_MassIRP[IRP_MAX]; // ������ ����������� � 0 �� NumberKeyboard-1, ����� - ��� ���������!!
volatile static bool s_bNeedIpReinit=false;
static unsigned int s_dwUKTOut=0;

static char s_strInfo[128];

////////////////////////////////////////////////////////////////////
void InitIRP(void)
{
  memset(s_MassIRP,0,sizeof(s_MassIRP));
  s_dwUKTOut=xTaskGetTickCount()+1000;
}

bool IsNeedIpReinit(void)
{
  bool bNeed=s_bNeedIpReinit;
  s_bNeedIpReinit=false;
  return(bNeed);
}

void SetNeedIpReinit(void)
{ s_bNeedIpReinit=true; }

unsigned short GetOperatorChannel(unsigned char SN,unsigned char btDS_RS)
{
  unsigned char btInd;

  for(btInd=0;btInd<NumberKeyboard;btInd++)
  {
    if((s_MassIRP[btInd].btType==IP_PULT) && (s_MassIRP[btInd].btSN==SN))
    {
      if(btDS_RS) return(s_MassIRP[btInd].wCP4RS);
      else return(s_MassIRP[btInd].wCP4DS);
    }
  }
  return(0);
}

unsigned short GetOperatorChannelFromCode(unsigned char btCode)
{
  unsigned char btInd;

  if(!btCode) return(0);
  for(btInd=0;btInd<NumberKeyboard;btInd++)
  {
    if(s_MassIRP[btInd].btType==IP_PULT)
    {
      if(s_MassIRP[btInd].btDS==btCode) return(s_MassIRP[btInd].wCP4DS);
      if(s_MassIRP[btInd].btRS==btCode) return(s_MassIRP[btInd].wCP4RS);
    }
  }
  return(0);
}

unsigned char GetCodeFromOperatorChannel(unsigned short wOp)
{
  unsigned char btInd;

  if(!wOp) return(0);
  for(btInd=0;btInd<NumberKeyboard;btInd++)
  {
    if(s_MassIRP[btInd].btType==IP_PULT)
    {
      if(s_MassIRP[btInd].wCP4DS==wOp) return(s_MassIRP[btInd].btDS);
      if(s_MassIRP[btInd].wCP4RS==wOp) return(s_MassIRP[btInd].btRS);
    }
  }
  return(0);
}

unsigned char GetCodeFromSndCP(unsigned char btCP)
{
  unsigned char btInd;

  if(btCP<100) return(0);
  btCP-=100;
  for(btInd=0;btInd<NumberKeyboard;btInd++)
  {
    if(s_MassIRP[btInd].btType==IP_PULT)
    {
      if((s_MassIRP[btInd].wCP4DS & 0xff)==btCP) return(s_MassIRP[btInd].btDS);
      if((s_MassIRP[btInd].wCP4RS & 0xff)==btCP) return(s_MassIRP[btInd].btRS);
    }
  }
  return(0);
}

unsigned char GetSNFromCP(unsigned short wChan)
{
  unsigned char btInd;

  wChan&=0xff;
  for(btInd=0;btInd<NumberKeyboard;btInd++)
  {
    if(s_MassIRP[btInd].btType==IP_PULT)
    {
      if((wChan==s_MassIRP[btInd].btDS) ||
         (wChan==s_MassIRP[btInd].btRS)) return(s_MassIRP[btInd].btSN);
    }
  }
  return(0);
}

bool GetPultIsdnInfoFromSN(unsigned char SN,unsigned char *pbtNumb,unsigned char *pbtName)
{
  unsigned char btInd;

  for(btInd=0;btInd<NumberKeyboard;btInd++)
  {
    if((s_MassIRP[btInd].btType==IP_PULT) && (s_MassIRP[btInd].btSN=SN))
    {
      memcpy(pbtName,s_MassIRP[btInd].btName,d_NameLen);
      pbtName[d_NameLen]=0;
      memcpy(pbtNumb,s_MassIRP[btInd].btNumbUniq,MAX_DIGIT_TLFN);
      pbtNumb[MAX_DIGIT_TLFN]=0;
      return(true);
    }
  }
  return(false);
}

unsigned char GetPultSNFromIndex(unsigned char btInd)
{
  if(btInd<NumberKeyboard)
  {
    if((s_MassIRP[btInd].btType==IP_PULT)/* ||
       (s_MassIRP[btInd].btType==IP_PULT_DEL_START)*/) return(s_MassIRP[btInd].btSN);
  }
  return(0);
}

unsigned char FindPultByNumb(char *strNumb,unsigned char *pbtInd)
{
  unsigned char btInd;
  
  if(strNumb && strNumb[0])
  {
    for(btInd=0;btInd<NumberKeyboard;btInd++)
    {
      if(s_MassIRP[btInd].btType==IP_PULT)
      {
        if(!strcmp((char *)s_MassIRP[btInd].btNumbUniq,strNumb))
        {
          if(pbtInd) *pbtInd=btInd+1;
          return(s_MassIRP[btInd].btSN);
        }
      }
    }
    if(pbtInd)
    {
      btInd=*pbtInd;
      for(;btInd<NumberKeyboard;btInd++)
      {
        if(s_MassIRP[btInd].btType==IP_PULT)
        {
          if(!strcmp((char *)s_MassIRP[btInd].btNumbSerial,strNumb))
          { *pbtInd=btInd+1; return(s_MassIRP[btInd].btSN); }
        }
      }
    }
  }
  else
  {
    if(pbtInd)
    { // ���� ��������� ������������������ �����, ������� � ��������� �������
      btInd=*pbtInd;
      for(;btInd<NumberKeyboard;btInd++)
      {
        if(s_MassIRP[btInd].btType==IP_PULT)
        {
          *pbtInd=btInd+1;
          return(s_MassIRP[btInd].btSN);
        }
      }
    }
  }
  return(0);
}

void _xResetIpPult(unsigned char btInd)
{
  unsigned char btSN=s_MassIRP[btInd].btSN;
  static section ("bigdata") sCommand sCmd;

  // ���������� ���������� � ������� ��� ��������� ������
  // !!!
  DeleteAudioMessages4Pult(s_MassIRP[btInd].btSN);

  sCmd.Source=UserUDP;
  sCmd.A1=0;
  // ������ �� �������� ���� ���������� ������
  sCmd.Length=2;
  sCmd.BufCommand[0]=113;
  sCmd.BufCommand[1]=btSN;
  xQueueSendCmd(&sCmd,10);
  // ������� ������ ����� ������
  sCmd.Length=3;
  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[1]=184;
  sCmd.BufCommand[2]=btSN;
  xQueueSendCmd(&sCmd,10);
  vTaskDelay(20);
  vPortEnableSwitchTask(false);
  s_MassIRP[btInd].btType=IP_PULT_DEL_END;
  vPortEnableSwitchTask(true);
}

void ResetIPPults(void)
{
  unsigned char btInd;

  for(btInd=0;btInd<NumberKeyboard;btInd++)
  {
    if(s_MassIRP[btInd].btType==IP_PULT)
    {
      AddToLog(lt_Error,ls_Task,lc_IpPultUnreg,s_MassIRP[btInd].btSN);
      _xResetIpPult(btInd);
    }
  }
}

// �������� ����� ����������� ��� ������ IP
void DelConnectionPoint4IP(unsigned short wCP)
{
  static sCommand sCmd;
  sCmd.Source=UserUDP; // �������� ������� RS485
  sCmd.Length=4; // ���������� ���� � �������
  sCmd.A1=0;
  sCmd.BufCommand[0]=10; // �������
  sCmd.BufCommand[1]=142; // ����������
  sCmd.BufCommand[2]=wCP >> 8;
  sCmd.BufCommand[3]=wCP & 0xff;
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  vTaskDelay(20);
}

// ����� ��������� ����� ����������� ��� ������ IP
unsigned short GetFreeConnectionPoint4IP(void)
{
  unsigned short wCP=0;
  static sCommand sCmd;
  static sInfoKadr sReply;

  sCmd.Source=UserUDP; // �������� ������� RS485
  sCmd.Length=2; // ���������� ���� � �������
  sCmd.A1=0;
  sCmd.BufCommand[0]=10; // �������
  sCmd.BufCommand[1]=141; // ����������
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(xQueueReceive(xQueueReply[UserUDP],&sReply,250))
    {
      if((sReply.ByteInfoKadr[0]==11) && (sReply.ByteInfoKadr[1]==141))
      {
        wCP=sReply.ByteInfoKadr[2];
        wCP=(wCP << 8) | sReply.ByteInfoKadr[3];
        if(wCP==0xffff) wCP=0;
      }
    }
  }
  return(wCP);
}

void CheckIpSendTime(void)
{
  unsigned char btInd;
  static unsigned short s_wErrCnt=0;
  static unsigned int s_dwTm=0;

  for(btInd=0;btInd<IRP_MAX;btInd++)
  {
    if(s_MassIRP[btInd].btType==IP_PULT)
    {
      if(s_dwTm && IsCurrentTickCountGT(s_dwTm))
      {
        s_wErrCnt++;
        LockPrintf();
        sprintf(s_strInfo,"CheckIpSendTimeErr=%d,SN=%d",s_wErrCnt,s_MassIRP[btInd].btSN);
        UnlockPrintf();
        AddToLogWithInfo(lt_Error,ls_Task,lc_Debug,s_strInfo);
      }
      s_dwTm=xTaskGetTickCount()+1500;
      return;
    }
  }
  s_dwTm=xTaskGetTickCount()+1500;
}

void PultEndWorkOK(unsigned char SN)
{
  unsigned char btInd;

  for(btInd=0;btInd<NumberKeyboard;btInd++)
  {
    if(s_MassIRP[btInd].btType==IP_PULT)
    {
      if(s_MassIRP[btInd].btSN==SN)
      {
        vPortEnableSwitchTask(false);
        s_MassIRP[btInd].btType=IP_PULT_DEL_START;
        s_MassIRP[btInd].dwTOut=0;
        vPortEnableSwitchTask(true);
        return;
      }
    }
  }
}

// ���� � ������� 3 ��� �� �������� �� Ip-������ ��������� �����, �� ������������� ��������� �����
void CheckRcvIpPultAlive(unsigned char btInd)
{
  if(s_MassIRP[btInd].dwTOutCmd)
  {
    if(IsCurrentTickCountGT(s_MassIRP[btInd].dwTOutCmd))
    {
      ReOpenSocket(CommandSocket,g_IpParams.wPortControl,true);
      s_MassIRP[btInd].dwTOutCmd=xTaskGetTickCount()+dIpPultCmdTOut;
    }
  }
}

void IRPTOut(void)
{
  unsigned char btInd,PultNb;
  static section ("bigdata") sInfoKadr sInfo;
  static section ("bigdata") sSoundIp sSnd;

  for(btInd=0;btInd<IRP_MAX;btInd++)
  {
    if(s_MassIRP[btInd].btType==IP_PULT) CheckRcvIpPultAlive(btInd);
    if(s_MassIRP[btInd].dwTOut)
    {
      if(IsCurrentTickCountGT(s_MassIRP[btInd].dwTOut))
      {
        if(s_MassIRP[btInd].btType==IP_PULT)
        {
          s_MassIRP[btInd].btType=IP_PULT_DEL_START;
          LockPrintf();
          sprintf((char *)sInfo.ByteInfoKadr,
                  "%s : IP-pult SN=%d life indicator timeout (DS=%x, RS=%x)!",
                  ConvertCurrentDTTM2Str(NULL),s_MassIRP[btInd].btSN,
                  s_MassIRP[btInd].wCP4DS,s_MassIRP[btInd].wCP4RS);
          UnlockPrintf();
          xQueueSend(xQueueLogger,&sInfo,0);
          s_MassIRP[btInd].dwTOut=xTaskGetTickCount()+3600000ul;
        }
        else
        {
          AddToLog(lt_Error,ls_Task,lc_IpPultUnreg,s_MassIRP[btInd].btSN);
          if(s_MassIRP[btInd].btType==IP_PULT_DEL_START)
          {
            LockPrintf();
            sprintf((char *)sInfo.ByteInfoKadr,
                    "%s : IP-pult SN=%d (DS=%x, RS=%x) deleted from IRP table!",
                    ConvertCurrentDTTM2Str(NULL),s_MassIRP[btInd].btSN,
                    s_MassIRP[btInd].wCP4DS,s_MassIRP[btInd].wCP4RS);
            UnlockPrintf();
            xQueueSend(xQueueLogger,&sInfo,0);
            _xResetIpPult(btInd);
            s_MassIRP[btInd].dwTOut=0;
          }
          else memset(s_MassIRP+btInd,0,sizeof(IRP_TP));
        }
      }
    }
    else
    {
      if(s_MassIRP[btInd].btType==IP_PULT_DEL_START)
      {
        AddToLog(lt_Error,ls_Task,lc_IpPultUnreg,s_MassIRP[btInd].btSN);
        LockPrintf();
        sprintf((char *)sInfo.ByteInfoKadr,
                "%s : IP-pult SN=%d (DS=%x, RS=%x) end work!",
                ConvertCurrentDTTM2Str(NULL),s_MassIRP[btInd].btSN,
                s_MassIRP[btInd].wCP4DS,s_MassIRP[btInd].wCP4RS);
        UnlockPrintf();
        xQueueSend(xQueueLogger,&sInfo,0);
        _xResetIpPult(btInd);
      }
    }
  }
  if(IsCurrentTickCountGT(s_dwUKTOut))
  { // ��������� ����������� ��������� ����� 1 ��� � ������� ���� ������������������ �����������
    for(btInd=0;btInd<IRP_MAX;btInd++)
    {
      switch(s_MassIRP[btInd].btType)
      {
        case IP_LOGGER:
          sInfo.L=2;
          sInfo.ByteInfoKadr[0]=254;
          sInfo.ByteInfoKadr[1]=0xff;
          sInfo.A1=s_MassIRP[btInd].dwIp;
          sInfo.PORT1=s_MassIRP[btInd].wPort;
          xQueueSend(xQueueReply[UserUDP],&sInfo,0);
          break;
        case IP_RECORD:
          sSnd.L=4;
          sSnd.ByteInfo[0]=1;     // Type of packet (0-sound packet, 1-command packet)
          sSnd.ByteInfo[1]=0x16;
          sSnd.ByteInfo[2]=0xFE;
          sSnd.ByteInfo[3]=0xFF;
          sSnd.A1=s_MassIRP[btInd].dwIp;
          sSnd.PORT1=s_MassIRP[btInd].wPort;
          xQueueSend(xQueueSound4Rec,&sSnd,0);
          break;
        case IP_PULT:
          // "UK alive for pult xx, L1 chan xx, L2 chan xx"
          sInfo.A1=s_MassIRP[btInd].dwIp;
          sInfo.PORT1=s_MassIRP[btInd].wPort;
          sInfo.L=48;
          sInfo.ByteInfoKadr[0]=11;
          sInfo.ByteInfoKadr[1]=0xb4;
          LockPrintf();
          sprintf((char *)sInfo.ByteInfoKadr+2,
                  "UK alive for pult %02d, L1 chan %02d, L2 chan %02d",
                  (int)s_MassIRP[btInd].btSN,(int)s_MassIRP[btInd].btDS,(int)s_MassIRP[btInd].btRS);
          UnlockPrintf();
          sInfo.ByteInfoKadr[50]=btInd; // ����� 50 � 51 � ���� �� ����������, � ������������ ��� ���������� ����!!
          sInfo.ByteInfoKadr[51]=s_MassIRP[btInd].btSN;
          xQueueSend(xQueueReply[UserUDP],&sInfo,0);
          break;
        case IP_PULT_DEL_END: // ��������� �������� ������
          DelPultRepeatCmd(s_MassIRP[btInd].btSN);
          if(s_MassIRP[btInd].wCP4DS >> 8) DelConnectionPoint4IP(s_MassIRP[btInd].wCP4DS);
          if(s_MassIRP[btInd].wCP4RS >> 8) DelConnectionPoint4IP(s_MassIRP[btInd].wCP4RS);
{
LockPrintf();
sprintf(s_strInfo,"IP_PULT_DEL_END SN=%d(%x,%x)",
        s_MassIRP[btInd].btSN,
        s_MassIRP[btInd].wCP4DS,
        s_MassIRP[btInd].wCP4RS);
UnlockPrintf();
AddToLogWithInfo(lt_Error,ls_Task,lc_Debug,s_strInfo);

SendStr2IpLogger("$DEBUG$IpPultDelEnd: SN=%d, wCP4DS=%x, wCP4RS=%x",
        s_MassIRP[btInd].btSN,
        s_MassIRP[btInd].wCP4DS,
        s_MassIRP[btInd].wCP4RS);
}
          memset(s_MassIRP+btInd,0,sizeof(IRP_TP));
          break;
        default:
          break;
      }
    }
    s_dwUKTOut=xTaskGetTickCount()+1000;
  }
}

unsigned char FindIpReg(unsigned char btType,
                        unsigned int *pA1,unsigned short *pPORT1,
                        unsigned char *pbtCnt,unsigned char *pbtIndFree)
{
  unsigned char btInd,btIndMax;
  unsigned short PORT1=0;
  unsigned int A1=0;

  if(pbtCnt) *pbtCnt=0;
  if(pbtIndFree) *pbtIndFree=0xff;
  if(pA1) A1=*pA1;
  if(pPORT1) PORT1=*pPORT1;
  if(btType==IP_PULT)
  { btInd=0; btIndMax=NumberKeyboard; }
  else
  { btInd=NumberKeyboard; btIndMax=IRP_MAX; }
  for(;btInd<btIndMax;btInd++)
  {
    if(pbtIndFree && (*pbtIndFree==0xff) && (s_MassIRP[btInd].btType==IP_NONE)) *pbtIndFree=btInd;
    if(s_MassIRP[btInd].btType==btType)
    {
      if(pbtCnt) (*pbtCnt)++;
      if(!A1)
      {
        if(pA1) *pA1=s_MassIRP[btInd].dwIp;
        if(pPORT1) *pPORT1=s_MassIRP[btInd].wPort;
        return(btInd);
      }
      if(s_MassIRP[btInd].dwIp==A1)
      {
        if(PORT1)
        {
          if(s_MassIRP[btInd].wPort==PORT1) return(btInd);
          return(0xff);
        }
        else return(btInd);
      }
    }
  }
  return(0xff);
}

void IRP_Registration(unsigned char btType,unsigned int A1,unsigned short PORT1,unsigned char *pbtMAC)
{
  unsigned char btInd,btCnt,btIndFree,btMaxId,btSN;

  switch(btType)
  {
    case IP_DSPCOM:
      btMaxId=d_MAXDSPCOM;
      btSN=0xff;
      break;
    case IP_RECORD:
      btMaxId=d_MAXRECORD;
      btSN=0xfd;
      break;
    case IP_LOGGER:
      btMaxId=d_MAXLOGGER;
      btSN=0xfc;
      break;
    default: return;
  }
  btInd=FindIpReg(btType,&A1,NULL,&btCnt,&btIndFree);
  if(btInd==0xff)
  {
    if((btCnt>=btMaxId) || (btIndFree==0xff)) return;
    s_MassIRP[btIndFree].btType=btType;
    s_MassIRP[btIndFree].btSN=btSN;
    s_MassIRP[btIndFree].wPort=PORT1;
    s_MassIRP[btIndFree].dwIp=A1;
    s_MassIRP[btIndFree].dwTOut=xTaskGetTickCount()+dIpPultTOut;
    if(pbtMAC) memcpy(s_MassIRP[btIndFree].btMAC,pbtMAC,6);
    else memset(s_MassIRP[btIndFree].btMAC,0,6);
    AddToLog(lt_Notification,ls_Task,lc_IpPultReg,btSN);
    {
      static section("bigdata") sInfoKadr sInfo;

      if(btType==IP_LOGGER)
      {
        while(xQueueReceive(xQueueLogger,&sInfo,0)==pdPASS);
        strcpy((char *)sInfo.ByteInfoKadr,"\r\nIP-LOGGER REGISTRATION!!!");
      }
      if(btType==IP_RECORD)
      {
        static section ("bigdata") sSoundIp sSnd;
        strcpy((char *)sInfo.ByteInfoKadr,"\r\nSitalRecorder REGISTRATION!!!");
        sSnd.L=strlen((char *)sInfo.ByteInfoKadr)+4;
        sSnd.ByteInfo[0]=1;     // Type of packet (0-sound packet, 1-command packet)
        sSnd.ByteInfo[1]=0x16;
        sSnd.ByteInfo[2]=123;   // ������������ ����������� ����������� (��� CRC)
        memcpy(sSnd.ByteInfo+3,sInfo.ByteInfoKadr,sSnd.L-3);
        sSnd.A1=A1;
        sSnd.PORT1=PORT1;
        xQueueSend(xQueueSound4Rec,&sSnd,0);
      }
      xQueueSend(xQueueLogger,&sInfo,0);
    }
  }
  else
  {
    s_MassIRP[btInd].wPort=PORT1;
    s_MassIRP[btInd].dwTOut=xTaskGetTickCount()+dIpPultTOut;
    if(pbtMAC &&
       (pbtMAC[0] | pbtMAC[1] | pbtMAC[2] | pbtMAC[3] | pbtMAC[4] | pbtMAC[5])) memcpy(s_MassIRP[btInd].btMAC,pbtMAC,6);
  }
}

void IRP_Unregistration(unsigned char btType,unsigned int A1)
{
  unsigned char btInd;

  btInd=FindIpReg(btType,&A1,NULL,NULL,NULL);
  if(btInd!=0xff) memset(s_MassIRP+btInd,0,sizeof(IRP_TP));
}

void IRP_Check(unsigned char btType,unsigned int A1)
{
  unsigned char btInd;

  btInd=FindIpReg(btType,&A1,NULL,NULL,NULL);
  if(btInd!=0xff) s_MassIRP[btInd].dwTOut=xTaskGetTickCount()+dIpPultTOut;
}

void SendDiagnostics2DspCom(unsigned char *pData,unsigned char Len)
{
  unsigned char btInd;
  static section("bigdata") sInfoKadr sInfo;

  if(!Len) return;
  vPortEnableSwitchTask(false);
  sInfo.L=Len;
  memcpy(sInfo.ByteInfoKadr,pData,Len);
  for(btInd=NumberKeyboard;btInd<IRP_MAX;btInd++)
  {
    if(s_MassIRP[btInd].btType==IP_DSPCOM)
    {
      sInfo.A1=s_MassIRP[btInd].dwIp;
      sInfo.PORT1=s_MassIRP[btInd].wPort;
      if(xQueueSend(xQueueReply[UserUDP],&sInfo,0)==errQUEUE_FULL)
      { ErrQueueAns++; break; }
    }
  }
  vPortEnableSwitchTask(true);
}

/*
0 - OK
1 - ���������� �������� ������ ����� �������
2 - ��� ����� �������� ��� ������� ��
3 - ��������� ����� ������
4 - ������� ����������� ����� ������������������� ������ � ����������� ����������� (��,��,����,IP-�����)
5 - �� � �� ==0
6 - ���������� �������� �� ��� �� � ��
7 - ������� ���������������� �����, ����������� �� ������ ��������
8 - ��������� ����. ����� ������������ ������������������ �������
*/
bool IpPultRegistration(unsigned char *pbtBufCmd)
{
  unsigned char btRes,btInd,btSN,btDS,btRS,
                btIpPultCnt,btIndFree;
  unsigned int dwIp;
  unsigned short wPort;
  static section ("bigdata") sInfoKadr sIK;

  btRes=0;
  btIpPultCnt=0;
  btIndFree=0xff;
  
  btSN=pbtBufCmd[0];
  wPort=pbtBufCmd[1]; wPort=(wPort<<8) | pbtBufCmd[2];
  dwIp=pbtBufCmd[6];
  dwIp=(dwIp<<8) | pbtBufCmd[5]; dwIp=(dwIp<<8) | pbtBufCmd[4]; dwIp=(dwIp<<8) | pbtBufCmd[3];
  btDS=pbtBufCmd[13]; btRS=pbtBufCmd[14];

  for(btInd=0;btInd<NumberKeyboard;btInd++)
  {
    if((s_MassIRP[btInd].btType==IP_PULT) ||
       (s_MassIRP[btInd].btType==IP_PULT_DEL_START))
    {
      btIpPultCnt++;
      if(s_MassIRP[btInd].btSN==btSN)
      {
        if((s_MassIRP[btInd].btDS!=btDS) || (s_MassIRP[btInd].btRS!=btRS) ||
           (s_MassIRP[btInd].wPort!=wPort) ||
           (s_MassIRP[btInd].dwIp!=dwIp))
        {
/*SendStr2IpLogger("$DEBUG$IpPultReg not EQ:\nSN=%d, DS=%d, RS=%d, port=%d, dwIp=%x\nSN=%d, DS=%d, RS=%d, port=%d, dwIp=%x",
                 btSN,btDS,btRS,wPort,dwIp,
                 s_MassIRP[btInd].btDS,s_MassIRP[btInd].btRS,s_MassIRP[btInd].wPort,s_MassIRP[btInd].dwIp);*/
          btRes=4;
        }
        break;
      }
      else
      {
        if((btDS && ((s_MassIRP[btInd].btDS==btDS) || (s_MassIRP[btInd].btRS==btDS))) ||
           (btRS && ((s_MassIRP[btInd].btDS==btRS) || (s_MassIRP[btInd].btRS==btRS))))
        {
          btRes=4;
          break;
        }
      }
    }
    else
    {
      if((s_MassIRP[btInd].btType!=IP_NONE) && (s_MassIRP[btInd].btSN==btSN))
      { // ������� ���������������� �����, ����������� �� ������ ��������
        btRes=7;
        break;
      }
    }
    if((btIndFree==0xff) && (s_MassIRP[btInd].btType==IP_NONE)) btIndFree=btInd;
  }
/*SendStr2IpLogger("$DEBUG$IpPultReg need: SN=%d (DS=%d, RS=%d), btRes=%d, btInd=%d, btIndFree=%d",
                 btSN,btDS,btRS,btRes,btInd,btIndFree);*/
  if(btRes==0)
  {
    if(btInd<NumberKeyboard) btIndFree=btInd;
    else
    {
      if(btIndFree==0xff) btRes=8;
      else
      {
        if(btDS || btRS) btRes=CheckRegistration(btSN,btIpPultCnt);
        else btRes=5;
        if(btRes==0)
        {
          // ����� ���� �������� ����� ����������� ��� ������� ��/�� ������, � ����� �������� ������ �� ������� �� ����������!
          if(btDS) s_MassIRP[btIndFree].wCP4DS=GetFreeConnectionPoint4IP();
          if(btRS) s_MassIRP[btIndFree].wCP4RS=GetFreeConnectionPoint4IP();
          if(s_MassIRP[btIndFree].wCP4DS || s_MassIRP[btIndFree].wCP4RS)
          { // ��������� ����� � �������
            vPortEnableSwitchTask(false);
            s_MassIRP[btIndFree].dwIp=dwIp;
            s_MassIRP[btIndFree].wPort=wPort;
            s_MassIRP[btIndFree].btSN=btSN;
            s_MassIRP[btIndFree].btDS=btDS;
            s_MassIRP[btIndFree].btRS=btRS;
            memcpy(s_MassIRP[btIndFree].btMAC,pbtBufCmd+7,6);
            pbtBufCmd+=15;
            memcpy(s_MassIRP[btIndFree].btNumbUniq,pbtBufCmd,MAX_DIGIT_TLFN+1);
            pbtBufCmd+=MAX_DIGIT_TLFN+1;
            memcpy(s_MassIRP[btIndFree].btNumbSerial,pbtBufCmd,MAX_DIGIT_TLFN+1);
            pbtBufCmd+=MAX_DIGIT_TLFN+1;
            memcpy(s_MassIRP[btIndFree].btName,pbtBufCmd,MAX_DIGIT_TLFN+1);
            vPortEnableSwitchTask(true);
          }
          else btRes=6;
        }
      }
    }
  }
  sIK.Source=UserUDP;
  sIK.A1=dwIp;
  sIK.PORT1=wPort;
  sIK.L=4;
  sIK.ByteInfoKadr[0]=11;
  sIK.ByteInfoKadr[1]=177;
  if(btRes)
  { // ������ �����������
    sIK.ByteInfoKadr[2]=0xff;
    sIK.ByteInfoKadr[3]=btRes;
  }
  else
  { // ����������� �������
    sIK.ByteInfoKadr[2]=1;
    sIK.ByteInfoKadr[3]=0;
    vPortEnableSwitchTask(false);
    s_MassIRP[btIndFree].btType=IP_PULT;
    s_MassIRP[btIndFree].dwTOut=xTaskGetTickCount()+dIpPultTOut;
    s_MassIRP[btIndFree].dwTOutCmd=xTaskGetTickCount()+dIpPultCmdTOut;
    g_iPultPingPong[btIndFree]=0;
    vPortEnableSwitchTask(true);
    SendStr2IpLogger("$DEBUG$IpPultReg: SN=%d, DS=%d CP4DS=%x, RS=%d wCP4RS=%x",
                     btSN,
                     btDS,s_MassIRP[btIndFree].wCP4DS,
                     btRS,s_MassIRP[btIndFree].wCP4RS);
    AddToLog(lt_Notification,ls_Task,lc_IpPultReg,btSN);
  }
  xQueueSend(xQueueReply[UserUDP],&sIK,0);
  if(btRes) return(false); // ������ ����������� -> ������������� �������� ���� �� ����
  if(btInd==NumberKeyboard) return(true); // ����������� ������ ������� -> ����������� �������� ����
  return(false); // ����������� ��� ���� -> ������������� �������� ���� �� ����
}

void _xIpPultLife(unsigned char btSN)
{
  unsigned char btInd;
  if(!btSN) return;
  for(btInd=0;btInd<NumberKeyboard;btInd++)
  {
    if((s_MassIRP[btInd].btType==IP_PULT) && (s_MassIRP[btInd].btSN==btSN))
    {
      if(g_iPultPingPong[btInd]<-1) g_iPultPingPong[btInd]=0;
      g_iPultPingPong[btInd]++;
      if(g_iPultPingPong[btInd]>1)
      {
        g_iPultPingPong[btInd]=1;
        SendStr2IpLogger("$LIFE_INDIC$Pult SN=%d alive",btSN);
      }
      s_MassIRP[btInd].dwTOut=xTaskGetTickCount()+dIpPultTOut;
      s_MassIRP[btInd].dwTOutCmd=xTaskGetTickCount()+dIpPultCmdTOut;
      RestartOpovProcessTOut(s_MassIRP[btInd].btSN);
      return;
    }
  }
}

void IpPultLife(unsigned char *pbtBufCmd)
{ // "Pult XX alive"
  unsigned char btSN;

  if((!memcmp(pbtBufCmd,"Pult ",5)) &&
     (!memcmp(pbtBufCmd+7," alive",6)))
  {
    if(IsDigit(pbtBufCmd[5]) && IsDigit(pbtBufCmd[6]))
    {
      btSN=(pbtBufCmd[5]-'0')*10+(pbtBufCmd[6]-'0');
      _xIpPultLife(btSN);
    }
  }
}

bool IRP_Cmd(unsigned char *pbtBufCmd,unsigned char btLn)
{
  if(pbtBufCmd[0]==10)
  {
    switch(pbtBufCmd[1])
    {
      case 177:
      {
        if(btLn>=74)    // 17+(MAX_DIGIT_TLFN+1)*3)
        {
          if(IpPultRegistration(pbtBufCmd+2)) return(false); // ����������� ������� ���� ��� ��� ����� �� IP-������
        }
        return(true);
      }
      case 180:
      {
        if(btLn==15) IpPultLife(pbtBufCmd+2);
        return(true);
      }
    }
  }
  else
  {
    if(pbtBufCmd[0]==254) return(true);
  }
  return(false);
}

bool GetAddressIpPult(unsigned char SN,unsigned int *pdwIP,unsigned short *pwPort)
{
  unsigned char btInd;

  if(SN<16) return(false);
  vPortEnableSwitchTask(false);
  for(btInd=0;btInd<NumberKeyboard;btInd++)
  {
    if((s_MassIRP[btInd].btType==IP_PULT) && (s_MassIRP[btInd].btSN==SN))
    {
      if(pdwIP) *pdwIP=s_MassIRP[btInd].dwIp;
      if(pwPort) *pwPort=s_MassIRP[btInd].wPort;
      vPortEnableSwitchTask(true);
      return(true);
    }
  }
  vPortEnableSwitchTask(true);
  return(false);
}

bool GetIpRecordAddr(unsigned char btIndRec,unsigned int *pdwIP,unsigned short *pwPort)
{ // ��� ������� ���������� ������ � IP-������
  unsigned char btInd;

  for(btInd=NumberKeyboard;btInd<IRP_MAX;btInd++)
  {
    if(s_MassIRP[btInd].btType==IP_RECORD)
    {
      if(btIndRec) btIndRec--;
      else
      {
        if(pdwIP) *pdwIP=s_MassIRP[btInd].dwIp;
        if(pwPort) *pwPort=s_MassIRP[btInd].wPort;
        return(true);
      }
    }
  }
  return(false);
}

bool GetControlIpAddr(unsigned int *pdwIpAddr, unsigned short *pdwIpPort)
{ // ��� ������� ���������� ������ � IP-������
  unsigned char btInd;
  unsigned int A1;

  memcpy(&A1,g_IpParams.ucDataSNMPrIP,4);
  if(A1)
  {
    btInd=FindIpReg(IP_DSPCOM,&A1,NULL,NULL,NULL);
    if(btInd!=0xff)
    {
      *pdwIpAddr=A1;
      *pdwIpPort=s_MassIRP[btInd].wPort;
      return(true);
    }
  }
  return(false);
}

bool GetMACAddrFromIp(unsigned int dwIP,unsigned char *MACAddr)
{ // ���������� ������������ ����� �� �����, �.�. ��� �-��� �������� ������ � ���� ������
  unsigned char btInd,*pbtMAC;

  if(!dwIP) return(false);
  for(btInd=0;btInd<IRP_MAX;btInd++)
  {
    if((s_MassIRP[btInd].btType!=IP_NONE) && (s_MassIRP[btInd].dwIp==dwIP))
    {
      pbtMAC=s_MassIRP[btInd].btMAC;
      if(pbtMAC[0] || pbtMAC[1] || pbtMAC[2] || pbtMAC[3] || pbtMAC[4] || pbtMAC[5])
      { memcpy(MACAddr,pbtMAC,6); return(true); }
      return(false);
    }
  }
  return(false);
}

/*void TestSendSoundPacket(void)
{
  unsigned char btInd;
  static unsigned int s_dwTOut=0;
  static section ("bigdata") sSoundIp sSnd;

  if(!s_dwTOut) s_dwTOut=xTaskGetTickCount()+30;
  if(IsCurrentTickCountLT(s_dwTOut)) return;
  s_dwTOut=xTaskGetTickCount()+30;
  memset(&sSnd,0,sizeof(sSoundIp));
  sSnd.L=sizeof(xListRTP);
  for(btInd=0;btInd<NumberKeyboard;btInd++)
  {
    if(s_MassIRP[btInd].btType==IP_PULT)
    {
      sSnd.A1=s_MassIRP[btInd].dwIp;
      sSnd.PORT1=g_IpParams.wPortSound;
      xQueueSend(xQueueSoundIpOut,&sSnd,0);
    }
  }
}
*/

