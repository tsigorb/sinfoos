#include "..\Sinfo.h"
#include "ViewCommandFunc.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"

/////////////////////////////////////////////////////////////////////
void vCmdPultState(sCommand *psViewCommand,sInfoKadr *psInfoS1)
{
  int iRes;
  unsigned char i,SN;
  static sAbntSelectorInfo sAbntInfo;

  switch(psViewCommand->BufCommand[1])
  {
    case 0: // ������ ��������� �������
      psInfoS1->L=6*NumberKeyboard+2;
      psInfoS1->ByteInfoKadr[0] = 99; // ����� 
      psInfoS1->ByteInfoKadr[1] = 0; // ����������
      for(i=0;i<NumberKeyboard;i++) 
      {
        SN=GetPultSNFromIndex(i);
        psInfoS1->ByteInfoKadr[i*6+2]=SN;
        psInfoS1->ByteInfoKadr[i*6+3]=(SN ? 1 : 0);
        psInfoS1->ByteInfoKadr[i*6+4]=0x0f; 
        psInfoS1->ByteInfoKadr[i*6+5]=0; // ������� ���������
        psInfoS1->ByteInfoKadr[i*6+6]=0; // ��������� ������������ ��������� (� ������ ������ �� ������ 0)
        psInfoS1->ByteInfoKadr[i*6+7]=0; // ��������� ����������� ��������� (� ������ ������ �� ������ 0)
      }
      break;
    case 1: // ����������/������������� ������
      psInfoS1->L = 3;
      psInfoS1->ByteInfoKadr[0] = 99; // ����� 
      psInfoS1->ByteInfoKadr[1] = 1; // ����������
      psInfoS1->ByteInfoKadr[2] = CodeStatusTrue;
      break;
    case 2: // ������������ ��������� ������
      psInfoS1->L = 3;
      psInfoS1->ByteInfoKadr[0] = 99; // ����� 
      psInfoS1->ByteInfoKadr[1] = 2; // ����������
      psInfoS1->ByteInfoKadr[2] = CodeStatusTrue;
      break;
    case 3: // ������ ������ ��������� ������
      return;
    case 4: // ������ ������ ������� ����
      psInfoS1->L=2;// +CountCallDTMF*5;
      psInfoS1->ByteInfoKadr[0] = 99; // ����� 
      psInfoS1->ByteInfoKadr[1] = 4; // ����������
/*      CountAb = 0;
      for(i=0;i<LengthMassDTMF;i++)
      {
        if(MassCallDTMF[i].NChan1)
        { // ���� �����  !!! ��� ����� ��-������� !!!
          psInfoS1->ByteInfoKadr[CountAb*5+2] = MassCallDTMF[i].NChan1 >> 8;  // ����� ������ ��-�� 
          psInfoS1->ByteInfoKadr[CountAb*5+3] = MassCallDTMF[i].NChan1;  // ����� ������
          psInfoS1->ByteInfoKadr[CountAb*5+4] = MassCallDTMF[i].NumbAbon; // ����� ��������
          psInfoS1->ByteInfoKadr[CountAb*5+5] = MassCallDTMF[i].NChan2 >> 8;  // ����� ������ ��-��
          psInfoS1->ByteInfoKadr[CountAb*5+6] = MassCallDTMF[i].NChan2;  // ����� ������ ��-��
          CountAb++;
        }
      }*/
      break;
    case 5: // ������ ���������� �����������
      return;
    case 6: // ������ �������� ������������ ���������
      return;
    case 7: // ������ ������ ���������� ������������ ���������
      return;
    case 8: // ������ ����� ����������� ���������� ������������ ���������
      return;
    default:
      return;
  }
  if(xQueueSend(xQueueReply[psInfoS1->Source],psInfoS1,10)==errQUEUE_FULL) CountErrQueueReply++;
}

