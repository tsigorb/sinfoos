/////////////////////////////////////////////////////////////////////
bool CommandStand(sInfoKadr *psInfoS1,unsigned char codeCommand,unsigned char Result);
void SendArray2PC(sInfoKadr *psInfoS1,unsigned short *DataBuf,unsigned char btCnt);
bool vCmdInputResistance(unsigned char *CommandPort0,sCommand *psViewCommand,sInfoKadr *psInfoS1,unsigned char codeCommand);
bool vCmdAGCOnOff(unsigned char *CommandPort0,sCommand *psViewCommand,sInfoKadr *psInfoS1,unsigned char codeCommand);
bool vCmdEchoOnOff(unsigned char *CommandPort0,sCommand *psViewCommand,sInfoKadr *psInfoS1,unsigned char codeCommand);
bool vCmdSetAnalPorog(unsigned char *CommandPort0,sCommand *psViewCommand,sInfoKadr *psInfoS1,unsigned char codeCommand);
bool vCmdSetReturnCoef(unsigned char *CommandPort0,sCommand *psViewCommand,sInfoKadr *psInfoS1,unsigned char codeCommand);
bool vCmdSetParametrs(unsigned char *CommandPort0,sCommand *psViewCommand,sInfoKadr *psInfoS1,unsigned char codeCommand);
bool vCmdSetChanelType(unsigned char *CommandPort0,sCommand *psViewCommand,sInfoKadr *psInfoS1,unsigned char codeCommand);
bool vCmdOutInductor(unsigned char *CommandPort0,sInfoKadr *psInfoS1,unsigned char codeCommand,unsigned char NChan);
void SendCommand2Uart(unsigned char *CommandPort0,sInfoKadr *psInfoS1,unsigned char *pxContinOutCommand,unsigned char NChan);
void vProcessJornal(sInfoKadr *psInfoS1,int aSize, unsigned char *BufCommand);
void vCmdCP2SelectorInfo(unsigned char SN,sInfoKadr *psInfoS);
//void vCmdCP2ConferenceInfo(unsigned char SN,unsigned char btConf,sInfoKadr *psInfoS);
void vCmdPultState(sCommand *psViewCommand,sInfoKadr *psInfoS1);
void vCmdReadCrash(sCommand *psViewCommand,sInfoKadr *psInfoS1);
void vCmdReadError(sCommand *psViewCommand,sInfoKadr *psInfoS1);
bool vCmdReadSoftVersionMU(unsigned char *CommandPort0,sInfoKadr *psInfoS1);
void vCmdReadDataFromMU(sCommand *sViewCommand,sInfoKadr *psInfoS1);
void vCmdWriteData2MU(sCommand *psViewCommand,sInfoKadr *psInfoS1);

