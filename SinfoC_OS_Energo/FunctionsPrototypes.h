//*****************************************************
bool IsNeedDestroyAllConnections2Pult(unsigned char SN);

void WriteAddrMU(unsigned char year, unsigned char mon, unsigned char day);
void SoundKeep(unsigned short NChan); // ��������� ������� �������� �� ���������
void StopKeep(unsigned short NChan); // ���������� ������� �������� �� ���������
bool ListenDispatcher(unsigned short ChanTechnic,unsigned short ChanDS,unsigned short ChanRS,unsigned char Incl);
bool IsCommute(unsigned short chan1,unsigned short chan2);

void ResetIPPults(void);
bool IsLevelPresent(unsigned short NChan);

void CreateErrInfo(unsigned char CodeCommand, unsigned char CodeErr, sInfoKadr *pxInfoS);
bool xUARTPutCmd(unsigned portCHAR *cOutChar);

void InitIRP(void);
bool IsNeedIpReinit(void);
unsigned short GetOperatorChannel(unsigned char SN,unsigned char btDS_RS);
unsigned short GetOperatorChannelFromCode(unsigned char btCode);
unsigned char GetCodeFromSndCP(unsigned char btCP);
unsigned char GetSNFromCP(unsigned short wChan);
unsigned char FindPultByNumb(char *strNumb,unsigned char *pbtInd);
unsigned char GetPultSNFromIndex(unsigned char btInd);
void SendReinitCount2Ip(void);
unsigned char FindIpReg(unsigned char btType,
                        unsigned int *pA1,unsigned short *pPORT1,
                        unsigned char *pbtCnt,unsigned char *pbtIndFree);
void PultEndWorkOK(unsigned char SN);
void IRPTOut(void);
void IRP_Registration(unsigned char btType,unsigned int A1,unsigned short PORT1,unsigned char *pbtMAC);
void IRP_Unregistration(unsigned char btType,unsigned int A1);
void IRP_Check(unsigned char btType,unsigned int A1);
void _xIpPultLife(unsigned char btSN);
bool IRP_Cmd(unsigned char *pbtBufCmd,unsigned char btLn);
bool GetAddressIpPult(unsigned char SN,unsigned int *pdwIP,unsigned short *pwPort);
bool GetIpRecordAddr(unsigned char btIndRec,unsigned int *pdwIP,unsigned short *pwPort);
bool GetControlIpAddr(unsigned int *pdwIpAddr, unsigned short *pdwIpPort);

void OutSNMP(unsigned char NMod, unsigned char Port, unsigned char Cod);

unsigned short RdFlash(unsigned short AdrFl);
bool WrFlash(unsigned short DanFl,unsigned short AdrFl);

void SetNeedIpReinit(void);
void SendIpReinitCount2IpPult(unsigned char SN,sInfoKadr *psInfo);

signed portBASE_TYPE xQueueSendCmd(sCommand *psCmd,portTickType xTicksToWait);

bool IsSelector2Pult(unsigned char SN);

char *GetNextStr(char *pBuf);

