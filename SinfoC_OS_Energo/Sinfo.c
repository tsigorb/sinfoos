#include "..\Sinfo.h"
#include "FunctionsPrototypes.h"
#include "GlobalData.h"

#define ssLoader      0
#define ssApplication 1

#define NumTekVer "Ver 01.01.19 "   // ������� ������ ��

#ifdef __ADACE__
    #define ADACE "����� "
#else
    #define ADACE ""
#endif

#define TekVer NumTekVer, __DATE__ " " __TIME__,"EDSS ","Energo " ADACE   // ������ ��

char FwVersionInfo[4][24]={TekVer}; // version number

///////////////////////////////////////////////////////////////////////////////
volatile bool g_bPrinfBusy=false;

///////////////////////////////////////////////////////////////////////////////
char *GetNextStr(char *pBuf)
{
  char chSimb;

  do
  {
    chSimb=*pBuf;
    if(!chSimb) return(NULL);
    if(chSimb==0x0a)
    {
      *pBuf=0;
      return(pBuf+1);
    }
    pBuf++;
  }
  while(chSimb);
  return(NULL);
}

unsigned int GET_DW(unsigned char *pbtBuf)
{
  unsigned int dwCode;
  dwCode=pbtBuf[3];
  dwCode=(dwCode<<8) | pbtBuf[2];
  dwCode=(dwCode<<8) | pbtBuf[1];
  dwCode=(dwCode<<8) | pbtBuf[0];
  return(dwCode);
}

void SET_DW(unsigned char *pbtBuf,unsigned int dwData)
{
  pbtBuf[3]=dwData >> 24;
  pbtBuf[2]=dwData >> 16;
  pbtBuf[1]=dwData >> 8;
  pbtBuf[0]=dwData;
}

unsigned short GET_W(unsigned char *pbtBuf)
{
  unsigned short wCode;
  wCode=pbtBuf[1];
  wCode=(wCode<<8) | pbtBuf[0];
  return(wCode);
}

///////////////////////////////////////////////////////////////////////////////
unsigned char *SaveInt2Buf(unsigned char *pbtBuf,int iData)
{
  pbtBuf[0]=iData >> 24;
  pbtBuf[1]=iData >> 16;
  pbtBuf[2]=iData >> 8;
  pbtBuf[3]=iData;
  return(pbtBuf+4);
}

unsigned char *RestoreIntFromBuf(unsigned char *pbtBuf,int *piData)
{
  int iData;
  iData=pbtBuf[0];
  iData=(iData << 8) | pbtBuf[1];
  iData=(iData << 8) | pbtBuf[2];
  iData=(iData << 8) | pbtBuf[3];
  *piData=iData;
  return(pbtBuf+4);
}

//################ �-��� ��������� �������� ������� � �������� #################
unsigned char IsCurrentTickCountGT(unsigned int dwTick0)
{
  unsigned char btState;
  unsigned int dwTick1=xTaskGetTickCount();

  btState=(unsigned char)((dwTick0 >> 30) | ((dwTick1 >> 29) & 0x02));
  switch(btState)
  {
    case 0x00:
    case 0x03:
      if(dwTick1>dwTick0) return(1);
      return(0);
    case 0x01:
      return(1);
    case 0x02:
      if(dwTick1-dwTick0>0x7fffffff) return(0);
      return(1);
  }
  return(0);
}

unsigned char IsCurrentTickCountLT(unsigned int dwTick0)
{
  unsigned char btState;
  unsigned int dwTick1=xTaskGetTickCount();

  btState=(unsigned char)((dwTick0 >> 30) | ((dwTick1 >> 29) & 0x02));
  switch(btState)
  {
    case 0x00:
    case 0x03:
      if(dwTick0>dwTick1) return(1);
      return(0);
    case 0x01:
      return(0);
    case 0x02:
      if(dwTick1-dwTick0>0x7fffffff) return(1);
      return(0);
  }
  return(0);
}

// ##### ������ �� ������������ #####
volatile static unsigned char LEDdata;

// ������������� ����������� - ��� ��������
void LedInit(void)
{
	LEDdata= 0;  // ��������� ������, ����������� ������ �����������
	*pLED= LEDdata;
}
// ��������� ���������� � �������� �������
void LedOn(unsigned char LedNum)
{
	LEDdata |= (1<<(LedNum-1));  // ����������� ������, ����������� ������ �����������
	*pLED= LEDdata;
}
// ���������� ���������� � �������� �������
void LedOff(unsigned char LedNum)
{
	LEDdata &= ~(1<<(LedNum-1));  // ����������� ������, ����������� ������ �����������
	*pLED= LEDdata;
}
// ������������ ���������� � �������� �������
void LedToggle(unsigned char LedNum)
{
	LEDdata ^= (1<<(LedNum-1));  // ����������� ������, ����������� ������ �����������
	*pLED= LEDdata;
}
// ��������� ���������� � �������� �������
bool LedStatus(unsigned char LedNum)
{
  return (LEDdata & (1<<(LedNum-1)));  // ������ ������, ����������� ������ �����������
}

void LedWorkIndicator(void)
{
  static portTickType xTimeToWake=0;

  if(IsCurrentTickCountLT(xTimeToWake)) return;
  if(!LedStatus(ledRabota))
  { // �������� ���������
    xTimeToWake=xTaskGetTickCount()+100;
    LedOn(ledRabota);
  }
  else
  { // ����� ���������
    xTimeToWake=xTaskGetTickCount()+1500;
    LedOff(ledRabota);
  }
}

static bool s_bST16C1550=false;

bool IsST16C1550(void)
{ return s_bST16C1550; }

void InitRS485(void)
{
  unsigned char btLSR;

  btLSR=*pLSR1;
  ssync();
  if(btLSR==0x60)
  {
    s_bST16C1550=true;
SendStr2IpLogger("$KBD$InitRS485: Old MU (ST16C1550 detected)!");
  }
  else
  {
    s_bST16C1550=false;
  }
}

void IpCS_Low(void)
{
  unsigned int dwIRQ=cli();
  LEDdata&=0xdf;
  *pLED=LEDdata;
  ssync();
  sti(dwIRQ);
}

void IpCS_Hi(void)
{
  unsigned int dwIRQ=cli();
  LEDdata|=0x20;
  *pLED=LEDdata;
  ssync();
  sti(dwIRQ);
}

//###################### ������ � ���������� ����� � ���������� �������� ##########################
#pragma pack(1)
typedef struct
{
  unsigned int dwCycle;
//  unsigned int dwFreeStackSize;
//  unsigned char btMasTaskVar[4];
} TASK_STATE_TP;
#pragma pack()

unsigned int GetFreeTaskStackSize(void);

/////////////////////////////////////////////////////////////////////
static TASK_STATE_TP s_MassTaskState[d_MaxTask];

volatile static unsigned int s_dwWDTaskState=0;

/////////////////////////////////////////////////////////////////////
signed portBASE_TYPE xQueueSendCmd(sCommand *psCmd,portTickType xTicksToWait)
{
  signed portBASE_TYPE Result;
  static sInfoKadr sReply;

  if(psCmd->Source==UserRS485)
  {  
    while(xQueueReceive(xQueueReply[UserRS485],&sReply,1));
  }
  if(psCmd->BufCommand[0]==10) Result=xQueueSend(xQueueSportX,psCmd,xTicksToWait);
  else Result=xQueueSend(xQueueCommand,psCmd,xTicksToWait);
  vTaskDelay(1);
  return(Result);
}

void SetTaskVar(unsigned int dwTask,unsigned char btOff,unsigned char btVar)
{
  unsigned char btPos=WDSTATE2NUMB(dwTask);

  if((btPos<d_MaxTask) && (btOff<4))
  {
//    s_MassTaskState[btPos].btMasTaskVar[btOff]=btVar;
  }
}

void SetWDState(unsigned int dwTask)
{
  unsigned char btPos=WDSTATE2NUMB(dwTask);
  if(btPos<d_MaxTask)
  {
    vPortEnableSwitchTask(false);
    s_dwWDTaskState|=dwTask;
    s_MassTaskState[btPos].dwCycle++;
//    s_MassTaskState[btPos].dwFreeStackSize=GetFreeTaskStackSize();
    vPortEnableSwitchTask(true);
  }
}

/*void SendTaskState2Ip(void)
{
  unsigned char btPos,btOff;
  static unsigned int s_dwTOut=0;
  static sInfoKadr sInfo;

  if(IsCurrentTickCountLT(s_dwTOut)) return;
  if(GetControlIpAddr(&sInfo.A1,&sInfo.PORT1))
  {
    vPortEnableSwitchTask(false);
    sInfo.Source=UserUDP;
    memset(sInfo.ByteInfoKadr,0,sizeof(sInfo.ByteInfoKadr));
    sInfo.ByteInfoKadr[0]=114;
    btOff=1;
    for(btPos=0;btPos<d_MaxTask;btPos++)
    {
      if(d_WDSTATE_TASKALL & (0x01ul << btPos))
      {
        memcpy(sInfo.ByteInfoKadr+btOff,s_MassTaskState+btPos,sizeof(TASK_STATE_TP));
      }
      btOff+=sizeof(TASK_STATE_TP);
    }
    sInfo.L=btOff;
//    xQueueSend(xQueueReply[sInfo.Source],&sInfo,10);
    vPortEnableSwitchTask(true);
  }
  s_dwTOut=xTaskGetTickCount()+1000;
}*/

// ����� ����������� �������
void ResetWatchDog(bool bNow)
{
  static portTickType xTimeToWatchDog=0;
  
  if(bNow || IsCurrentTickCountGT(xTimeToWatchDog))
  {
    unsigned int i;

    vPortEnableSwitchTask(false);
    for(i=0;i<0x4;i++)
    {
      *pFIO_FLAG_S=PF0;
      ssync();
    }
    *pFIO_FLAG_C=PF0;
    ssync();
    s_dwWDTaskState=0;
    vPortEnableSwitchTask(true);
    xTimeToWatchDog=xTaskGetTickCount()+100; // �������� 100 ��
  }
}

// ���������� ������ ����������� ������� ��� ����������� �������
void BlockWatchDog(void)
{
  cli();
  while(1);
}

