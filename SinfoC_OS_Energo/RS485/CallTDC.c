#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"

/////////////////////////////////////////////////////////////////////
static unsigned char s_MasFreqTDC[90]=
{
#include "..\DAT\NumberTDC.dat"
};

/////////////////////////////////////////////////////////////////////
void ControlTDCTOut(CONNECTION_STATE_TP *psConn)
{
  switch(GetConnectionStateFromPtr(psConn))
  {
    case CallTest:
      // ����������� ������� �� �������
      SetConnectionStateByPtr(psConn,CallTestError,0);
      psConn->dwTimerEnd=d_IndicTestCallResult+xTaskGetTickCount();
      return;
    case IncomingCall:
    case Call:
    case CallTestError:
    case CallTestUp:
    case CallTestDown:
    default:
      OtboiConnection(psConn);
      return;
  }
}

bool _xOutgoingCallTDC(unsigned short wNChan,unsigned char AbntNumb,bool bTest)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485;
  sCmd.Length=5;
  sCmd.A1=0;
  sCmd.BufCommand[0]=1;
  sCmd.BufCommand[1]=wNChan >> 8;
  sCmd.BufCommand[2]=wNChan;
  if(bTest) sCmd.BufCommand[3]=TestTDC;
  else sCmd.BufCommand[3]=CallTDC;
  sCmd.BufCommand[4]=AbntNumb-1;
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else 
  {
    if(WaitReply(1,0)) return(true);
  }
  return(false);
}

void IncomingCallFromAbonentMKA(unsigned short wNChan,unsigned char TypeAbon,unsigned char AbntNumb)
{
  CONNECTION_STATE_TP *psConn;

  psConn=GetConnectionStatePtrByFind(0,wNChan,0xffff,TypeAbon,AbntNumb,NULL);
  if(!psConn)
  { // ������� ����� ���������� � ���������� "�������� �����"
    psConn=GetNewConnection();
    if(!psConn)
    {
      OtboiConnectionMKA(0,wNChan,AbntNumb,TypeAbon);
      return;
    }
    psConn->SN=0xff;
    psConn->wConnectionPoint=wNChan;
    psConn->wOperatorChannel=0;
    psConn->btType=TypeAbon;
    psConn->NumbAbon=AbntNumb;
    psConn->dwTimerEnd=MassTimer[TIMER_INCOMINGCALL]+xTaskGetTickCount();
    SetConnectionStateByPtr(psConn,IncomingCall,0);
    if(!IsConnectionBinding(psConn)) OtboiConnection(psConn);
    return;
  }
  else
  {
    if(TypeAbon==TDC)
    {
      switch(GetConnectionStateFromPtr(psConn))
      {
        case Call:
          /*if(IsGroupAutoAnswer4Pult(psConn)) AddAbnt2FreeHandConf(PultNb,psAbnt);
          else*/
          Commute(true,psConn->wConnectionPoint,psConn->wOperatorChannel);
          SetConnectionStateByPtr(psConn,Talk,0);
          psConn->dwTimerEnd=0;
          return;
        case CallTest:
          Commute(false,psConn->wOperatorChannel,psConn->wConnectionPoint);
          SetConnectionStateByPtr(psConn,CallTestUp,0);
          psConn->dwTimerEnd=d_IndicTestCallResult+xTaskGetTickCount();
          return;
        default:
          return;
      }
    }
  }
}

void IncomingCall_TDC(unsigned short wNChan,unsigned char AbntNumb,unsigned char FreqInd)
{ // ����� ��������� �� ��� ������, � ������������� ������ �����!
  unsigned char i,TrubaUp,TrubaDown;

  i=(AbntNumb-1) << 1;
  TrubaUp=s_MasFreqTDC[i]; // ����� ������� �������� ������
  TrubaDown=s_MasFreqTDC[i+1]; // ����� ������� ���������� ������
  if(FreqInd==TrubaUp) IncomingCallFromAbonentMKA(wNChan,TDC,AbntNumb);
  else
  {
    if(FreqInd==TrubaDown) OtboiConnectionMKA(0,wNChan,AbntNumb,TDC);
  }
}

