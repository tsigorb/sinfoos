#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"

/////////////////////////////////////////////////////////////////////
void ControlPultsTOut(void)
{
/*  unsigned char PultNb,SN;
  
  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    SN=GetPultSNFromIndex(PultNb);
    if(SN)
    {
      PultIpCmdTOut(SN);
      PultCallTOut(SN);
      CallLevelTOut(SN);
      HoldTOut(SN);
    }
  }*/
}

void ControlConnectionsOut(void)
{
  unsigned char btInd;
  CONNECTION_STATE_TP *psConn;

  btInd=0;
  do
  {
    psConn=GetConnectionStatePtrFromIndex(btInd);
    if(!psConn) return;
    btInd++;
    if(psConn->dwId && psConn->wConnectionPoint && psConn->dwTimerEnd)
    {
      if(IsCurrentTickCountGT(psConn->dwTimerEnd))
      {
        psConn->dwTimerEnd=0;
        switch(psConn->btType)
        {
          case TDC:
            ControlTDCTOut(psConn);
            break;
          case ISDN:
            if(!psConn->wOperatorChannel && (GetConnectionStateFromPtr(psConn)==Call))
            { SetConnectionStateByPtr(psConn,ConnectionTOut,0); }
          case ATC:
          default:
            OtboiConnection(psConn);
            break;
        }
      }
    }
  }
  while(1);
}

void DetectAllTimers(void)
{
  static unsigned int s_dwTOut=0;

  if(IsCurrentTickCountLT(s_dwTOut)) return;
  ControlConnectionsOut();
  ControlATCLevelTOut();
  OpovProcessTOut();
  s_dwTOut=100+xTaskGetTickCount();
}

