#define LengthRX485         90
#define LengthTX485         90
#define NumberKeyboard      12 // ������������ ���������� �������
#define MaxNumberMKA        12 // ������������ ���������� ���� ���
#define MAX_MKA_CHANNEL     (MaxNumberMKA*4)
#define NumberKonf          6 // ����. ���������� ����������� �� ������
#define MAX_CONF_ABONENT    64
#define NumberSelector      16 // ����. ���������� ����������
#define MaxAbonentSelector  90 // ������������ ���������� ��������� ���������
#define d_NameLen           20
#define MAX_DIGIT_TLFN      18 // ������������ ���-�� ���� � ������

#define NumberTimer                 13  // ����� ��������
#define TIMER_OUTGOINGCALL          0   // ������ ���������� ������
#define TIMER_INCOMINGCALL          1   // ������ ��������� ������
#define TIMER_SPEAKLEVEL            2   // ������ ��������� �� ������
#define TIMER_SPEAKSIGNALS          3   // ������ ��������� �� ��������
#define TIMER_TESTCALL              4   // ������ ��������� ������
#define TIMER_HOLD                  5   // ������ �� ���������
#define TIMER_INDUCTOR              6   // ������ ������������ ������
#define TIMER_OUTGOINGNUMBER        7   // ������ ������������
#define TIMER_ATCDIRECT             8   // ������ ������� �������� ���
#define TIMER_FARABONENT            9   // ������ ��������� ���������� ��������
#define TIMER_GENERALCALL           10  // ������ ������������ ������
#define TIMER_GROUPCALL             11  // ������ ���������� ������
#define TIMER_SELECTOR              12  // ������ ���������

#define d_TIMER_BUSY                3000

// ��������� ����������
#define Passive                     0
#define Call                        1  // ��������� �����
#define Talk                        2  // �������� 
#define Hold                        3  // ���������
#define Busy                        4  // ����� 
#define IncomingCall                5  // �������� �����
#define CallTest                    6  // ��������� �������� ����� ��� ��������� TDC
#define PultListen                  7  // ��������� ������
#define SelectorListen              8  // ������� ��������� "���������"
#define SelectorMain                9  // ������� ��������� "�������"
#define CallTestUp                  10
#define CallTestDown                11
#define CallTestError               12
#define SelectorDisableOut          13 
#define CallError                   14
#define CallWaitAnswer              15 // ����� ������� �����
#define ConnectionTOut              16

#define dIncomingTalkFlag           0x80

#define d_IncomingTalkState         0x80000000ul
#define d_AutomaticCallAbonent      0x40000000ul

// ������ ���
#define UP                          1
#define DOWN                        2

// --------- ��� ������ -----------
#define TDC  			1 
#define RSDT			2
#define ATC				3 
#define Selector		4
#define Level	 		5 
#define GroupTDC		6
#define NoCall			7
#define ImitatorTDC		8
#define PultCall        9
#define Commutator		10
#define CB				11
#define GroupRSDT		12
#define IndCall			13
#define ImitatorRSDT	14
#define Circul2_7		15
#define Circul2_8		16
#define Circul2_11		17
#define Circul2_12		18
#define PPSR			19
#define GenCallTDC		20
#define RD3				21
#define TDCIcx			22
#define Spec1			23
#define Spec2			24
#define Spec3			25
#define Spec4			26
#define Spec5			27
#define Spec6			28
#define Spec7			29
#define Spec8			30
#define ISDN			31

#define d_GroupISDN                 63
#define d_GroupCallPult             64

/////////////////////////////////////////////////////////////////////
#define MaxConnectionActive         240

typedef struct
{
  unsigned short wConnectionPoint,wOperatorChannel;
  unsigned char SN,btType,btHold,btE1,btOutgoingCnt;
  unsigned char NumbAbon;
  unsigned char btLvlTOut;
  unsigned char btTypeNabor;
  unsigned char MasBind[NumberKeyboard];
  char NumbSrc[MAX_DIGIT_TLFN+1],NameSrc[d_NameLen+1],NumbDst[MAX_DIGIT_TLFN+1];
  unsigned int dwState,dwId;
  unsigned int dwTimerEnd,dwTimerAnaliz;
  unsigned char btAId,btACycle,btAnswTm,btConnTm;
  unsigned char bAutomaticCall,btOpovRec,btOpovState;
} CONNECTION_STATE_TP;

#pragma pack(1)
typedef struct
{
  unsigned char btState,SN,CallType;
  unsigned short w_Chan_OpId;
  char NumbSrc[MAX_DIGIT_TLFN+1],NameSrc[d_NameLen+1],NumbDst[MAX_DIGIT_TLFN+1];
} CONNECTION_INFO4IP_TP;
#pragma pack()

/////////////////////////////////////////////////////////////////////
#define d_IndicTestCallResult   3000

#define d_Pult2MUIpTOut         200
#define d_MU2PultMaxRepeat      3

struct _LIST_MU2PULT_TP;

struct _LIST_MU2PULT_TP
{
  unsigned short wId;
  unsigned char SN,repeat_cnt;
  unsigned long int dwTOut;
  CONNECTION_STATE_TP *psConn;
  void (*pFunc_PultAnswer2MU)(struct _LIST_MU2PULT_TP *pItem,sInfoKadr *psInfo);
  void (*pFunc_PultNoAnswer2MU)(struct _LIST_MU2PULT_TP *pItem);
  struct _LIST_MU2PULT_TP *prev,*next;
};

typedef struct _LIST_MU2PULT_TP LIST_MU2PULT_TP;

typedef struct
{
  unsigned char SN;
  unsigned short wABlockCnt,wABlockCur;
  unsigned char *pbtAudio;
} AUDIO_MESSAGE_TP;

typedef struct _AUDIO_MESSAGE_LIST_TP
{
  unsigned char btCycle;
  unsigned int dwId;
  unsigned short wNChan;
  unsigned int dwACnt,dwACur;
  unsigned char *pbtAudio;
  struct _AUDIO_MESSAGE_LIST_TP *psNext;
} AUDIO_MESSAGE_LIST_TP;

// ������ ��� ���������� ������ 24[2-3]/X
#define d_OperatorChannelNotExist       1
#define d_ConnectionNotExist            2
#define d_IncomingCallAnswErr           3
#define d_NotFreeConnection             4
#define d_NotFreeConnectionPoint        5
#define d_OutgoingCallErr               6
#define d_CallTypeUnknown               7
#define d_MKAChannelBusy                8
#define d_UnknownCmd                    9
#define d_PultNumbErr                   10
#define d_PultNotRegister               11
#define d_IncomingCallNotExist          12
#define d_HoldErr                       13
#define d_NoOpovMess                    14
#define d_CommonErr                     255

/////////////////////////////////////////////////////////////////////
// Conference_x
#define d_NoConference               0
#define d_CreateConference           1
#define d_ConferenceWithOperator     2
#define d_ConferenceWithoutOperator  3

#define d_FreeHandConferenceNumb    15
#define d_MaxConn2FreeHandConf      64

typedef struct
{
  unsigned char btExistConf;
  unsigned char MassConnInd[d_MaxConn2FreeHandConf];
} FreeHandConfTP,*PFreeHandConfTP;

/////////////////////////////////////////////////////////////////////
// ..\IP_IRP.c
bool GetPultIsdnInfoFromSN(unsigned char SN,unsigned char *pbtNumb,unsigned char *pbtName);

// vPult2MUCmdProcess.c
void IpPultEnergo_RcvCmd(sInfoKadr *psInfo);

// ListMU2Pult.c
void IpPultEnergo_RcvAnsw(sInfoKadr *psInfo);

// FunctionsRS485.c
bool IsDigit(char chSb);
unsigned char ConvertStrNumb2Byte(unsigned char *btMasNumb,unsigned char btLn);
unsigned char kb_calculate_crc_8(unsigned char *addr, char count);
unsigned short PhantomCP4PultCall(unsigned char SN_Dst,unsigned char SN_Src);
void OtboiConnectionMKA(unsigned char SN,unsigned short wNChan,unsigned char AbntNumb,unsigned char btType);
void StopOpov(CONNECTION_STATE_TP *psConn,unsigned char btPultOk);
void OtboiConnection(CONNECTION_STATE_TP *psConn);
bool WaitReply(unsigned char Command, unsigned char Subcommand);
void Commute(bool Code,unsigned short NChan1, unsigned short NChan2);
void DelConnectionPoint(unsigned short wCP);
unsigned short GetFreeConnectionPoint(void);
bool TrubaUP_Down(unsigned char UP_Down, unsigned short NChan);
unsigned int GetTypeCall4ChanelMKA(unsigned short NChan);
unsigned char CmdEnDisOut(unsigned short wCP,unsigned char btEnOut);
bool HoldOnOff(CONNECTION_STATE_TP *psConn,unsigned char btMode);
char *strclr(char *str);

// ConnectionState.c
void InitConnectionStateArray(void);
CONNECTION_STATE_TP *GetConnectionStatePtrByFind(unsigned char SN,
                                     unsigned short wConnectionPoint,unsigned short wOperatorChanel,
                                     unsigned char TypeAbon,unsigned char NumbAbon,unsigned char *pbtTlfn);
CONNECTION_STATE_TP *GetNewConnection(void);
void SetConnectionBinding(CONNECTION_STATE_TP *psConn,unsigned char SN);
void ClrConnectionBinding(CONNECTION_STATE_TP *psConn,unsigned char SN);
bool IsConnectionBinding(CONNECTION_STATE_TP *psConn);
void ClearAllConnectionBinding(CONNECTION_STATE_TP *psConn);
CONNECTION_STATE_TP *FindConnectionById(unsigned int dwId);
CONNECTION_STATE_TP *GetConnectionStatePtrFromIndex(unsigned char btInd);
unsigned char GetConnectionStateFromPtr(CONNECTION_STATE_TP *psConn);
bool IsAutomaticCallConnection(CONNECTION_STATE_TP *psConn);
void SetAutomaticCallConnection(CONNECTION_STATE_TP *psConn);
bool IsFreeMKAChannel(unsigned short wChan);
bool IsOtherConnectionUsedMKALine(CONNECTION_STATE_TP *psConn0);
bool SetConnectionStateByPtr(CONNECTION_STATE_TP *psConn,unsigned char btState,unsigned char btExt);
void DelConnectionByPtr(CONNECTION_STATE_TP *psConn);
void ControlConnectionTOut(void);
void DeleteAllConnections2Sinfo(void);
void StopOpovAbonents(unsigned char SN,unsigned char btPultOk);
void vCmdReadIpReinitCount(sCommand *psCmd,sInfoKadr *psInfo);
void PultNeedActiveConnections(unsigned char SN);

// DetectAllTimers.c
void DetectAllTimers(void);

// CallATC.c
bool _xOutgoingCallDTMF(unsigned short NChan,unsigned char *btBufNumb,unsigned char btLn);
void ControlATCLevelTOut(void);
void SignalATC_Ready(unsigned short wNChan);
void SignalATC_Busy(unsigned short wNChan);
void IncomingCallATC(unsigned short wNChan);

// CallTDC.c
void ControlTDCTOut(CONNECTION_STATE_TP *psConn);
bool _xOutgoingCallTDC(unsigned short wNChan,unsigned char AbntNumb,bool bTest);
void IncomingCall_TDC(unsigned short wNChan,unsigned char AbntNumb,unsigned char FreqInd);

// CallEDSS.c
void _xAbntRecord(CONNECTION_STATE_TP *psConn,unsigned char btOn);
bool SendCmdOtboiISDN(unsigned char Reason,unsigned short wNChan,unsigned short wChanOper);
bool _xIncomingCallISDNAnswer(unsigned short wNChan,unsigned short wOper,unsigned char btCommute);
bool _xOutgoingCallISDN(unsigned short NChan,unsigned short wOper,unsigned char btE1,
                        char *BufNumbDest,char *BufNumbSrs,char *BufNameSrs,unsigned char SN);
bool _xIncomingCallISDNFree(unsigned short wNChan,unsigned short wChanOper,unsigned char btKPV);
bool OutgoingCallISDN(CONNECTION_STATE_TP *psConn);
void IncomingCallISDN(unsigned short wNChan,sInfoKadr *psVxCall);
void AnswerOutgoingCallISDN(unsigned short wNChan,unsigned short wOper);
void FreeAbntOutgoingCallISDN(unsigned short wNChan);
void BusyISDN(unsigned short wNChan,bool bBusy);
void OtboiISDN(unsigned short wNChan);

// PultEvent4Call.c
void FreeConnection4PultEvent(LIST_MU2PULT_TP *pItem);
void Passive_PultNoAnswer2MU(LIST_MU2PULT_TP *pItem);
void Passive_PultAnswer2MU(LIST_MU2PULT_TP *pItem,sInfoKadr *psInfo);
void IncomingCall_PultNoAnswer2MU(LIST_MU2PULT_TP *pItem);
void IncomingCall_PultAnswer2MU(LIST_MU2PULT_TP *pItem,sInfoKadr *psInfo);
void Busy_PultNoAnswer2MU(LIST_MU2PULT_TP *pItem);
void Busy_PultAnswer2MU(LIST_MU2PULT_TP *pItem,sInfoKadr *psInfo);
void Talk_PultNoAnswer2MU(LIST_MU2PULT_TP *pItem);
void Talk_PultAnswer2MU(LIST_MU2PULT_TP *pItem,sInfoKadr *psInfo);
void CallTest_PultAnswer2MU(LIST_MU2PULT_TP *pItem,sInfoKadr *psInfo);

// AsteriskCrossCommute.c
void InitCrossCommute(void);
bool IsCrossCommuteAbnt(unsigned short wNChan);
void CrossCommuteOtboi(unsigned short wNChan);
void CrossCommuteOtboiAll(void);
bool CrossCallISDN(unsigned short wNChanSrs,unsigned char btE1Srs,
                       char *NumberSrs,char *NameSrs,char *NumberDst);
void CrossCommuteAnswer(unsigned short wCPDst,unsigned short wCPSrs);

// ListMU2Pult.c
LIST_MU2PULT_TP *ListMU2Pult_AddItem(LIST_MU2PULT_TP *pItem0);

// PultRepeatCmd.c
void InitPultRepeatCmd(void);
void DelPultRepeatCmd(unsigned char SN);
void SendAnsw4PultCmd(sInfoKadr *psInfo);
bool PultRepeatCmd(unsigned char SN,unsigned short wCmd);

// RS485InitFunction.c
void SetNeedInitPults(void);
bool IsNeedInitPults(void);
void InitPults(bool bWait);

// ListMU2Pult.c
void ListMU2Pult_Process(void);

// AudioMessage.c
void InitAudioMessages(void);
unsigned char CreateAudioMessage(unsigned char SN,unsigned int dwCnt);
void DeleteAudioMessage(unsigned char SN);
unsigned char _xRecieveAudioPacket(unsigned char SN,unsigned int dwPckt,unsigned char *pbtAudio);
unsigned int GetAudioTime(unsigned char SN);
bool StartAudioMessage(CONNECTION_STATE_TP *psConn);
unsigned char StopAudioMessage(unsigned int dwId);

// Selector.c
bool CreateSelector(unsigned char SN,unsigned char btCnt,unsigned char btOpID,unsigned char btSelNb);
void DestroySelector(SN);
bool AddAbnt2Selector(unsigned char SN,unsigned int dwId,unsigned char btMode);
void ReplaceCP2Selector(unsigned char SN,unsigned short wCP_Old,unsigned short wCP_New);
bool SetAbnt2SelectorState(unsigned char SN,unsigned int dwId,unsigned char btState);
bool DelAbntFromSelector(unsigned char SN,unsigned int dwId);

// OpovProcess.c
bool ExistOpovProcess(unsigned char SN,unsigned int dwId);
unsigned int StartOpovProcess(unsigned char SN,unsigned char btTOut);
void StopOpovProcess(unsigned char SN);
void OpovProcessTOut(void);
void RestartOpovProcessTOut(unsigned char SN);

