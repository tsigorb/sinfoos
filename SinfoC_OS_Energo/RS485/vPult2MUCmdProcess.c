#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"

///////////////////////////////////////////////////////////////////////////////
unsigned short FindFreeMKALine2Group(unsigned char *btLineMask)
{
  unsigned short wChan,wH,wL;

  for(wChan=0;wChan<MAX_MKA_CHANNEL;wChan++)
  {
    wH=wChan>>3; wL=wChan & 0x7;
    if(btLineMask[wH] & (1 << wL))
    {
      if(IsFreeMKAChannel(wChan+1)) return(wChan+1);
    }
  }
  return(0);
}

/*void AnswerIncomingCall_PultAnswer2MU(LIST_MU2PULT_TP *pItem,sInfoKadr *psInfo)
{
}

void AnswerIncomingCall_PultNoAnswer2MU(LIST_MU2PULT_TP *pItem)
{
}*/

bool AnswerIncomingCall(CONNECTION_STATE_TP *psConn,unsigned short wOperator)
{
  CONNECTION_STATE_TP *psConnSrc;

  switch(psConn->btType)
  {
    case ISDN:
      return _xIncomingCallISDNAnswer(psConn->wConnectionPoint,wOperator,1);
    case ATC:
      TrubaUP_Down(UP,psConn->wConnectionPoint);
    case TDC:
    case Selector:
      Commute(true,psConn->wConnectionPoint,wOperator);
      Commute(true,wOperator,psConn->wConnectionPoint);
      return true;
    case PultCall:
       psConnSrc=GetConnectionStatePtrByFind(0,PhantomCP4PultCall(psConn->SN,psConn->NumbAbon),
                                             0xffff,PultCall,0xff,NULL);
       if(psConnSrc)
       {
         SetConnectionStateByPtr(psConnSrc,Talk,0);
         Commute(true,psConn->wConnectionPoint,wOperator);
         Commute(true,wOperator,psConn->wConnectionPoint);
         return true;
       }
  }
  return false;
}

void SetAnswerHead2Pult(sInfoKadr *psInfoCmd,sInfoKadr *psInfoAnsw)
{
  psInfoAnsw->A1=psInfoCmd->A1;
  psInfoAnsw->PORT1=psInfoCmd->PORT1;
  psInfoAnsw->Source=psInfoCmd->Source;
  psInfoAnsw->ByteInfoKadr[0]=0xFC;
  psInfoAnsw->ByteInfoKadr[1]=0;
  psInfoAnsw->ByteInfoKadr[2]=psInfoCmd->ByteInfoKadr[5];   // PultSN
  psInfoAnsw->ByteInfoKadr[3]=psInfoCmd->ByteInfoKadr[7];   //-+ - wCmdId
  psInfoAnsw->ByteInfoKadr[4]=psInfoCmd->ByteInfoKadr[8];   //-+
  psInfoAnsw->ByteInfoKadr[5]=psInfoCmd->ByteInfoKadr[9];   // Cmd code
  psInfoAnsw->ByteInfoKadr[6]=0;          // ��� ��������: ���� ��
  psInfoAnsw->L=7;
}

bool PultCallAbntISDN(CONNECTION_STATE_TP *psConn,sInfoKadr *psInfo,sInfoKadr *psInfoResult)
{
  unsigned char btLn,btOff;

  psConn->wConnectionPoint=GetFreeConnectionPoint();
  if(psConn->wConnectionPoint==0xff)
  {
    DelConnectionByPtr(psConn);
    psInfoResult->ByteInfoKadr[6]=d_NotFreeConnectionPoint;
    return(false);
  }
  psConn->SN=psInfo->ByteInfoKadr[5];
  psConn->btE1=psInfo->ByteInfoKadr[11];
  GetPultIsdnInfoFromSN(psConn->SN,(unsigned char *)psConn->NumbSrc,(unsigned char *)psConn->NameSrc);
  btLn=strlen((char *)(psInfo->ByteInfoKadr+20));
  btOff=20+btLn+1;
  if(btLn>MAX_DIGIT_TLFN) btLn=MAX_DIGIT_TLFN;
  memcpy(psConn->NumbDst,psInfo->ByteInfoKadr+20,btLn);
  psConn->NumbDst[btLn]=0;
  btLn=strlen((char *)(psInfo->ByteInfoKadr+btOff));
  if(btLn>MAX_DIGIT_TLFN) btLn=MAX_DIGIT_TLFN;
  memcpy(psConn->NumbSrc,psInfo->ByteInfoKadr+btOff,btLn);
  psConn->NumbSrc[btLn]=0;
  if(!OutgoingCallISDN(psConn))
  {
    DelConnectionPoint(psConn->wConnectionPoint);
    DelConnectionByPtr(psConn);
    psInfoResult->ByteInfoKadr[6]=d_OutgoingCallErr;
    return(false);
  }
  psConn->dwTimerEnd=MassTimer[TIMER_OUTGOINGCALL]+xTaskGetTickCount();
  psInfoResult->ByteInfoKadr[11]=1; // ������� ������������ ���������� �� ��������
  return(true);
}

bool PultCallAbntTDC(CONNECTION_STATE_TP *psConn,sInfoKadr *psInfo,sInfoKadr *psInfoResult)
{
  bool bTest;

  psConn->wConnectionPoint=psInfo->ByteInfoKadr[11];
/*  if(IsOtherConnectionUsedMKALine(psConn))
  {
    Commute(true,psConn->wOperatorChannel,psConn->wConnectionPoint);
    Commute(true,psConn->wConnectionPoint,psConn->wOperatorChannel);
    SetConnectionStateByPtr(psConn,Talk,0);
    psConn->dwTimerEnd=0;
    psInfoResult->ByteInfoKadr[11]=0; // ������� ������������ ���������� ��������
  }
  else*/
  {
    /* ������ ������ ���: 0�[�] - ������� ����� �������� � ������� �[�]
                          *x[x] - �������� ����� �������� � ������� �[�] */
    if(psInfo->ByteInfoKadr[20]=='*')
    {
      bTest=true;
      SetConnectionStateByPtr(psConn,CallTest,0);
    }
    else bTest=false;
    psConn->NumbAbon=ConvertStrNumb2Byte(psInfo->ByteInfoKadr+21,psInfo->L-21);
    if(!_xOutgoingCallTDC(psConn->wConnectionPoint,psConn->NumbAbon,bTest))
    {
      DelConnectionByPtr(psConn);
      psInfoResult->ByteInfoKadr[6]=d_OutgoingCallErr;
      return(false);
    }
    Commute(true,psConn->wOperatorChannel,psConn->wConnectionPoint);
    psConn->dwTimerEnd=MassTimer[TIMER_OUTGOINGCALL]+xTaskGetTickCount();
    psInfoResult->ByteInfoKadr[11]=1; // ������� ������������ ���������� �� ��������
  }
  return(true);
}

bool PultCallAbntATC(CONNECTION_STATE_TP *psConn,sInfoKadr *psInfo,sInfoKadr *psInfoResult)
{
  unsigned char btLn;

  btLn=psInfo->ByteInfoKadr[11];
  if(btLn)
  {
    if(!IsFreeMKAChannel(btLn))
    {
      DelConnectionByPtr(psConn);
      psInfoResult->ByteInfoKadr[6]=d_MKAChannelBusy;
      return(false);
    }
  }
  else
  {
    btLn=FindFreeMKALine2Group(psInfo->ByteInfoKadr+12);
    if(!btLn)
    { // ��� ��������� ����� ���
      DelConnectionByPtr(psConn);
      psInfoResult->ByteInfoKadr[6]=d_MKAChannelBusy;
      return(false);
    }
  }
  psConn->wConnectionPoint=btLn;
  btLn=psInfo->L-20;
  if(btLn>MAX_DIGIT_TLFN) btLn=MAX_DIGIT_TLFN;
  memcpy(psConn->NumbDst,psInfo->ByteInfoKadr+20,btLn);
  psConn->NumbDst[btLn]=0;
  if(!TrubaUP_Down(UP,psConn->wConnectionPoint))
  {
    DelConnectionByPtr(psConn);
    psInfoResult->ByteInfoKadr[6]=d_OutgoingCallErr;
    return(false);
  }
  Commute(true,psConn->wOperatorChannel,psConn->wConnectionPoint);
  psConn->dwTimerEnd=MassTimer[TIMER_OUTGOINGCALL]+xTaskGetTickCount();
  psInfoResult->ByteInfoKadr[11]=1; // ������� ������������ ���������� �� ��������
  return(true);
}

void PultCall_PultNoAnswer2MU(LIST_MU2PULT_TP *pItem)
{
  CONNECTION_STATE_TP *psConnSrc,*psConn=pItem->psConn;

  psConnSrc=GetConnectionStatePtrByFind(0,PhantomCP4PultCall(psConn->SN,psConn->NumbAbon),
                                        0xffff,PultCall,0xff,NULL);
  if(psConnSrc)
  {
    LIST_MU2PULT_TP *psCmdMU=ListMU2Pult_AddItem(NULL);
    if(psCmdMU)
    {
      SetConnectionStateByPtr(psConnSrc,Busy,0);
      SetConnectionBinding(psConn,psConnSrc->SN);
      psCmdMU->SN=psConnSrc->SN;
      psCmdMU->pFunc_PultAnswer2MU=NULL;
      psCmdMU->pFunc_PultNoAnswer2MU=NULL;
      psCmdMU->psConn=psConnSrc;
    }
    else DelConnectionByPtr(psConnSrc);
  }
  DelConnectionByPtr(psConn);
}

void PultCall_PultAnswer2MU(LIST_MU2PULT_TP *pItem,sInfoKadr *psInfo)
{
  if(psInfo->ByteInfoKadr[10])
  { // ����� �� ������������ ���� �����
    PultCall_PultNoAnswer2MU(pItem);
  }
}

bool PultCallAbntPult(CONNECTION_STATE_TP *psConnSrc,sInfoKadr *psInfo,sInfoKadr *psInfoResult)
{
  unsigned char SN_Dst,btOff;
  unsigned short wCP4DS,wCP4RS;
  bool bTest;
  CONNECTION_STATE_TP *psConnDst;
  LIST_MU2PULT_TP *psCmdMU;

  if(psInfo->ByteInfoKadr[20]=='*')
  { bTest=true; btOff=21; }
  else
  { bTest=false; btOff=20; }
  SN_Dst=ConvertStrNumb2Byte(psInfo->ByteInfoKadr+btOff,psInfo->L-btOff);
  if(!SN_Dst)
  {
    DelConnectionByPtr(psConnSrc);
    psInfoResult->ByteInfoKadr[6]=d_PultNumbErr;
    SendAnsw4PultCmd(psInfoResult);
    return(false);
  }
  psConnSrc->wConnectionPoint=PhantomCP4PultCall(SN_Dst,psConnSrc->SN);
  psConnSrc->NumbAbon=SN_Dst;
  if(GetAddressIpPult(SN_Dst,NULL,NULL))
  {
    if(bTest)
    {
      wCP4DS=GetOperatorChannel(SN_Dst,0);
      wCP4RS=GetOperatorChannel(SN_Dst,1);
      if(ListenDispatcher(psConnSrc->wOperatorChannel,wCP4DS,wCP4RS,1))
      {
        SetConnectionStateByPtr(psConnSrc,PultListen,0);
        return(true);
      }
      DelConnectionByPtr(psConnSrc);
      psInfoResult->ByteInfoKadr[6]=d_OutgoingCallErr;
      return(false);
    }
    else 
    {
      psConnDst=GetNewConnection();
      if(!psConnDst)
      {
        DelConnectionByPtr(psConnSrc);
        psInfoResult->ByteInfoKadr[6]=d_NotFreeConnection;
        return(false);
      }
      psConnDst->SN=SN_Dst;
      psConnDst->NumbAbon=psConnSrc->SN;
      psConnDst->btType=PultCall;
      psConnDst->wConnectionPoint=psConnSrc->wOperatorChannel;
      psConnDst->dwTimerEnd=MassTimer[TIMER_INCOMINGCALL]+xTaskGetTickCount();
      psConnSrc->dwTimerEnd=MassTimer[TIMER_OUTGOINGCALL]+xTaskGetTickCount();
      SetConnectionStateByPtr(psConnDst,IncomingCall,0);
      psInfoResult->ByteInfoKadr[11]=1; // ������� ������������ ���������� �� ��������
      return(true);
    }
  }
  DelConnectionByPtr(psConnSrc);
  psInfoResult->ByteInfoKadr[6]=d_PultNotRegister;
  return(false);
}

void PultSendDTMFSeq(sInfoKadr *psInfo,sInfoKadr *psInfoAnsw)
{
  unsigned int dwIdCall;
  CONNECTION_STATE_TP *psConn;

  dwIdCall=psInfo->ByteInfoKadr[13]; dwIdCall<<=8;
  dwIdCall|=psInfo->ByteInfoKadr[12]; dwIdCall<<=8;
  dwIdCall|=psInfo->ByteInfoKadr[11]; dwIdCall<<=8;
  dwIdCall|=psInfo->ByteInfoKadr[10];
  psConn=FindConnectionById(dwIdCall);
  if(psConn)
  {
    if(!_xOutgoingCallDTMF(psConn->wConnectionPoint,
                           psInfo->ByteInfoKadr+14,
                           psInfo->L-13))
    { psInfoAnsw->ByteInfoKadr[6]=d_OutgoingCallErr; }
  }
  else psInfoAnsw->ByteInfoKadr[6]=d_ConnectionNotExist;
}

void PultCallAbnt(sInfoKadr *psInfo,sInfoKadr *psInfoAnsw)
{
  unsigned char SN,btTypeCall,btLn,btChan,btState;
  unsigned short wOperator;
  CONNECTION_STATE_TP *psConn=NULL;
  
  SN=psInfo->ByteInfoKadr[5];
  wOperator=GetOperatorChannelFromCode(psInfo->ByteInfoKadr[19]);
  if(wOperator) psConn=GetNewConnection();
  if(!psConn || !wOperator)
  {
    if(!wOperator) psInfoAnsw->ByteInfoKadr[6]=d_OperatorChannelNotExist;
    if(!psConn) psInfoAnsw->ByteInfoKadr[6]=d_NotFreeConnection;
    return;
  }
  psConn->SN=SN;
  psConn->wOperatorChannel=wOperator;
  if(psInfo->ByteInfoKadr[18] & 0x04) psConn->bAutomaticCall=1;
  btTypeCall=psInfo->ByteInfoKadr[10];
  psConn->btType=btTypeCall;
  btState=Call;
  switch(btTypeCall)
  {
    case ISDN:
      if(!PultCallAbntISDN(psConn,psInfo,psInfoAnsw)) return;
      break;
    case TDC:
      if(!PultCallAbntTDC(psConn,psInfo,psInfoAnsw)) return;
      break;
    case Selector:
      psConn->wConnectionPoint=psInfo->ByteInfoKadr[11];
      Commute(true,psConn->wOperatorChannel,psConn->wConnectionPoint);
      Commute(true,psConn->wConnectionPoint,psConn->wOperatorChannel);
      btState=Talk;
      psConn->dwTimerEnd=0;
      psInfoAnsw->ByteInfoKadr[11]=0; // ������� ������������ ���������� ��������
      break;
    case ATC:
      psConn->btTypeNabor=psInfo->ByteInfoKadr[18] & 0x03;
      if(!PultCallAbntATC(psConn,psInfo,psInfoAnsw)) return;
      break;
    case PultCall:
      if(!PultCallAbntPult(psConn,psInfo,psInfoAnsw)) return;
      break;
    default:
      DelConnectionByPtr(psConn);
      psInfoAnsw->ByteInfoKadr[6]=d_CallTypeUnknown;
      return;
  }
  psInfoAnsw->ByteInfoKadr[7]=psConn->dwId & 0xff;
  psInfoAnsw->ByteInfoKadr[8]=(psConn->dwId>>8) & 0xff;
  psInfoAnsw->ByteInfoKadr[9]=(psConn->dwId>>16) & 0xff;
  psInfoAnsw->ByteInfoKadr[10]=(psConn->dwId>>24) & 0xff;
  psInfoAnsw->L=12;
  if(GetConnectionStateFromPtr(psConn)==Passive) SetConnectionStateByPtr(psConn,btState,0);
}

void PultWantRecieveIncomingCall(sInfoKadr *psInfo,sInfoKadr *psInfoAnsw)
{
  unsigned char SN;
  unsigned short wOperator;
  unsigned int dwIdCall;
  CONNECTION_STATE_TP *psConn;

  SN=psInfo->ByteInfoKadr[5];
  wOperator=GetOperatorChannelFromCode(psInfo->ByteInfoKadr[10]);
  dwIdCall=psInfo->ByteInfoKadr[14]; dwIdCall<<=8;
  dwIdCall|=psInfo->ByteInfoKadr[13]; dwIdCall<<=8;
  dwIdCall|=psInfo->ByteInfoKadr[12]; dwIdCall<<=8;
  dwIdCall|=psInfo->ByteInfoKadr[11];
SendStr2IpLogger("$DEBUG$PultWantRecieveIncomingCall: SN=%d, OperatorCode=%d, wOp=0x%X, dwIdCall=%d",
                 SN,psInfo->ByteInfoKadr[10],wOperator,dwIdCall);
  if(wOperator)
  {
    psConn=FindConnectionById(dwIdCall);
    if(psConn)
    {
      if(GetConnectionStateFromPtr(psConn)==IncomingCall)
      {
        if(AnswerIncomingCall(psConn,wOperator))
        {
          psInfoAnsw->L=8;
          psInfoAnsw->ByteInfoKadr[6]=0;   // ��!
          psInfoAnsw->ByteInfoKadr[7]=SN;
          psConn->SN=SN;
          psConn->wOperatorChannel=wOperator;
          psConn->dwTimerEnd=0;
          SetConnectionStateByPtr(psConn,Talk,0);
        }
        else
        {
          psInfoAnsw->ByteInfoKadr[6]=d_IncomingCallAnswErr;
          ClrConnectionBinding(psConn,SN);
          if(!IsConnectionBinding(psConn)) DelConnectionByPtr(psConn);
        }
      }
      else
      {
        psInfoAnsw->L=8;
        psInfoAnsw->ByteInfoKadr[6]=0;   // ��!
        psInfoAnsw->ByteInfoKadr[7]=psConn->SN;
        if(psConn->SN!=SN) ClrConnectionBinding(psConn,SN);
      }
    }
    else psInfoAnsw->ByteInfoKadr[6]=d_IncomingCallNotExist;
  }
  else psInfoAnsw->ByteInfoKadr[6]=d_OperatorChannelNotExist;
}

void PultCmdsConnection(sInfoKadr *psInfo,sInfoKadr *psInfoAnsw)
{
  unsigned char SN;
  unsigned short wOperator;
  unsigned int dwIdCall;
  CONNECTION_STATE_TP *psConn;

  SN=psInfo->ByteInfoKadr[5];
  dwIdCall=psInfo->ByteInfoKadr[13]; dwIdCall<<=8;
  dwIdCall|=psInfo->ByteInfoKadr[12]; dwIdCall<<=8;
  dwIdCall|=psInfo->ByteInfoKadr[11]; dwIdCall<<=8;
  dwIdCall|=psInfo->ByteInfoKadr[10];
  psConn=FindConnectionById(dwIdCall);
  if(psConn)
  {
    switch(psInfo->ByteInfoKadr[9])
    {
      case 6: // �����
        OtboiConnection(psConn);
        break;
      case 8: // ���/���� ��������� ��� �������������� ������� �������� ��� � ��������� "Talk"
        if(HoldOnOff(psConn,psInfo->ByteInfoKadr[14])) psInfoAnsw->ByteInfoKadr[6]=0;
        else psInfoAnsw->ByteInfoKadr[6]=d_HoldErr;
        break;
      default:
        psInfoAnsw->ByteInfoKadr[6]=d_UnknownCmd;
        break;
    }
  }
  else psInfoAnsw->ByteInfoKadr[6]=d_ConnectionNotExist;
  memcpy(psInfoAnsw->ByteInfoKadr+7,psInfo->ByteInfoKadr+10,4);
  psInfoAnsw->L=11;
}

bool OpovAbntISDN(CONNECTION_STATE_TP *psConn,sInfoKadr *psInfo,sInfoKadr *psInfoAnsw)
{
  unsigned char btLn,btOff;

  psConn->SN=psInfo->ByteInfoKadr[5];
  if(!GetAudioTime(psConn->SN))
  {
    DelConnectionByPtr(psConn);
    psInfoAnsw->ByteInfoKadr[6]=d_NoOpovMess;
    return(false);
  }
  psConn->wConnectionPoint=GetFreeConnectionPoint();
  if(psConn->wConnectionPoint==0xff)
  {
    DelConnectionByPtr(psConn);
    psInfoAnsw->ByteInfoKadr[6]=d_NotFreeConnectionPoint;
    return(false);
  }
  psConn->btAId=psInfo->ByteInfoKadr[10];
  psConn->btOpovRec=psInfo->ByteInfoKadr[11];
  psConn->btACycle=psInfo->ByteInfoKadr[12];
  psConn->btE1=psInfo->ByteInfoKadr[14];
  psConn->btAnswTm=psInfo->ByteInfoKadr[16];
  psConn->btConnTm=psInfo->ByteInfoKadr[17];
  GetPultIsdnInfoFromSN(psConn->SN,(unsigned char *)psConn->NumbSrc,(unsigned char *)psConn->NameSrc);
  btLn=strlen((char *)(psInfo->ByteInfoKadr+18));
  btOff=18+btLn+1;
  if(btLn>MAX_DIGIT_TLFN) btLn=MAX_DIGIT_TLFN;
  memcpy(psConn->NumbDst,psInfo->ByteInfoKadr+18,btLn);
  psConn->NumbDst[btLn]=0;
  btLn=strlen((char *)(psInfo->ByteInfoKadr+btOff));
  if(btLn>MAX_DIGIT_TLFN) btLn=MAX_DIGIT_TLFN;
  memcpy(psConn->NumbSrc,psInfo->ByteInfoKadr+btOff,btLn);
  psConn->NumbSrc[btLn]=0;
SendStr2IpLogger("$DEBUG$OpovAbntISDN: SN=%d, CallId=%d, NumbDst=%s, NumbSrc=%s, NameSrc=%s",
                 psConn->SN,psConn->dwId,psConn->NumbDst,psConn->NumbSrc,psConn->NameSrc);
  if(!OutgoingCallISDN(psConn))
  {
    DelConnectionPoint(psConn->wConnectionPoint);
    DelConnectionByPtr(psConn);
    psInfoAnsw->ByteInfoKadr[6]=d_OutgoingCallErr;
    return(false);
  }
  psConn->dwTimerEnd=(unsigned int)psConn->btConnTm*1000+xTaskGetTickCount();
  psInfoAnsw->ByteInfoKadr[11]=1; // ������� ������������ ���������� �� ��������
  return(true);
}

void OpovAbnt(sInfoKadr *psInfo,sInfoKadr *psInfoAnsw)
{
  unsigned char SN,btTypeCall,btLn,btCycle,btState;
  unsigned int dwATm;
  CONNECTION_STATE_TP *psConn;
  
  SN=psInfo->ByteInfoKadr[5];
  dwATm=GetAudioTime(SN);
  if(!dwATm)
  { // ��� ���������
    psInfoAnsw->ByteInfoKadr[6]=d_NoOpovMess;
    return;
  }
  psConn=GetNewConnection();
  if(!psConn)
  {
    if(!psConn) psInfoAnsw->ByteInfoKadr[6]=d_NotFreeConnection;
    return;
  }
  psConn->SN=SN;
  psConn->wOperatorChannel=0;
  psConn->btOpovRec=psInfo->ByteInfoKadr[11];
  psConn->btACycle=psInfo->ByteInfoKadr[12];
  btTypeCall=psInfo->ByteInfoKadr[13];
  psConn->btType=btTypeCall;
  switch(btTypeCall)
  {
    case ISDN:
      btState=Call;
      if(!OpovAbntISDN(psConn,psInfo,psInfoAnsw)) return;
      break;
    case Selector:
      btState=Talk;
      psConn->dwTimerEnd=dwATm*psConn->btACycle+xTaskGetTickCount()+100;
      psConn->wConnectionPoint=(((unsigned short)psInfo->ByteInfoKadr[15]) << 8) | psInfo->ByteInfoKadr[14];
      if(StartAudioMessage(psConn)) psInfoAnsw->ByteInfoKadr[11]=0; // ���������� �����������
      else
      {
        DelConnectionByPtr(psConn);
        psInfoAnsw->ByteInfoKadr[6]=d_NoOpovMess;
        return;
      }
      break;
    default:
      DelConnectionByPtr(psConn);
      psInfoAnsw->ByteInfoKadr[6]=d_CallTypeUnknown;
      return;
  }
  psInfoAnsw->ByteInfoKadr[7]=psConn->dwId & 0xff;
  psInfoAnsw->ByteInfoKadr[8]=(psConn->dwId>>8) & 0xff;
  psInfoAnsw->ByteInfoKadr[9]=(psConn->dwId>>16) & 0xff;
  psInfoAnsw->ByteInfoKadr[10]=(psConn->dwId>>24) & 0xff;
  psInfoAnsw->L=12;
  if(GetConnectionStateFromPtr(psConn)==Passive) SetConnectionStateByPtr(psConn,btState,0);
}

void Cmd4OpovProcess(unsigned char SN,unsigned int dwId,unsigned char btTOut,sInfoKadr *psInfoAnsw)
{
SendStr2IpLogger("$DEBUG$Cmd4OpovProcess source: SN=%d, dwId=%d, TOut=%d",
                 SN,dwId,btTOut);
  if(dwId)
  { // ������ �� ������� ���������� � �������� ID
    if(!ExistOpovProcess(SN,dwId)) dwId=0;
  }
  else
  { // ������ �� ������ ������ ����������
    dwId=StartOpovProcess(SN,btTOut);
  }
SendStr2IpLogger("$DEBUG$Cmd4OpovProcess result: SN=%d, dwId=%d, TOut=%d",
                 SN,dwId,btTOut);
  psInfoAnsw->ByteInfoKadr[6]=dwId & 0xff;
  psInfoAnsw->ByteInfoKadr[7]=(dwId>>8) & 0xff;
  psInfoAnsw->ByteInfoKadr[8]=(dwId>>16) & 0xff;
  psInfoAnsw->ByteInfoKadr[9]=(dwId>>24) & 0xff;
  psInfoAnsw->L=10;
}

void IpPultEnergo_RcvCmd(sInfoKadr *psInfo)
{
  unsigned char SN,btRes;
  unsigned short wCmd,wSz;
  static sInfoKadr sInfo;

  SN=psInfo->ByteInfoKadr[5];
  if(!SN || psInfo->ByteInfoKadr[6]) return; // ������� �� ������������� ��� ��!
  wCmd=psInfo->ByteInfoKadr[8];
  wCmd=(wCmd<<8) | psInfo->ByteInfoKadr[7];
  if(PultRepeatCmd(SN,wCmd)) return;
  SetAnswerHead2Pult(psInfo,&sInfo);
  switch(psInfo->ByteInfoKadr[9])
  {
    case 2: // ��������� ����� �������� (� ������� ��� ��� � ����������� �� ���� ������)
      PultCallAbnt(psInfo,&sInfo);
      break;
    case 4: // ������� ������ ������ DTMF � ��� ������������� ����������
      PultSendDTMFSeq(psInfo,&sInfo);
      break;
    case 5: // ����� ����� ������� �������� �����
      PultWantRecieveIncomingCall(psInfo,&sInfo);
      break;
    case 6: // ����� �������� ������� �����
    case 8: // ����������/������ ���������
      PultCmdsConnection(psInfo,&sInfo);
      break;
    case 9: // ������������� ������ �����-��������� � ������ ��
      wSz=psInfo->ByteInfoKadr[11];
      wSz=(wSz<<8) | psInfo->ByteInfoKadr[10];
      btRes=CreateAudioMessage(SN,wSz);
      if(btRes==0xff) sInfo.ByteInfoKadr[6]=1;
      else
      {
        sInfo.L=8;
        sInfo.ByteInfoKadr[7]=btRes;
      }
      break;
    case 10: // ����� ������ �����-���������
/* !! �������������� ��������������� � IpXchg.c !!
      wSz=psInfo->ByteInfoKadr[11];
      wSz=(wSz<<8) | psInfo->ByteInfoKadr[10];
      sInfo.ByteInfoKadr[6]=RecieveAudioPacket(SN,wSz,psInfo->ByteInfoKadr+12);
*/
sInfo.ByteInfoKadr[6]=d_CommonErr; // 
      break;
    case 11: // ���������� ���������� � ��������� � �������� ��� �����-���������
      OpovAbnt(psInfo,&sInfo);
      break;
    case 13: // ��������� ����������
      StopOpovAbonents(SN,true);
      DeleteAudioMessage(SN);
      DestroySelector(SN);
      StopOpovProcess(SN);
      break;
    case 14:
      if(!CreateSelector(SN,psInfo->ByteInfoKadr[10],psInfo->ByteInfoKadr[11],psInfo->ByteInfoKadr[12]))
      { sInfo.ByteInfoKadr[6]=d_CommonErr; }
      break;
    case 15:
      if(!AddAbnt2Selector(SN,GET_DW(psInfo->ByteInfoKadr+11),psInfo->ByteInfoKadr[10]))
      { sInfo.ByteInfoKadr[6]=d_CommonErr; }
      break;
    case 16:
      DestroySelector(SN);
      break;
    case 17: // ���������� ��������� �������� ���������
      if(!SetAbnt2SelectorState(SN,GET_DW(psInfo->ByteInfoKadr+11),psInfo->ByteInfoKadr[10]))
      { sInfo.ByteInfoKadr[6]=d_CommonErr; }
      break;
    case 18: // ������� �������� �� ���������
      if(!DelAbntFromSelector(SN,GET_DW(psInfo->ByteInfoKadr+10)))
      { sInfo.ByteInfoKadr[6]=d_CommonErr; }
      break;
    case 19: // ������ � ������� ��������� �������� ����������
      Cmd4OpovProcess(SN,GET_DW(psInfo->ByteInfoKadr+10),psInfo->ByteInfoKadr[14],&sInfo);
      break;
    case 20: // ����� ��������� �������� ������
      PultEndWorkOK(SN);
      break;
    case 21: // ������ �������� ���������� ������
      PultNeedActiveConnections(SN);
      break;
    case 3: // ��������� ���������
    default:
      sInfo.ByteInfoKadr[6]=d_UnknownCmd;
      break;
  }
  SendAnsw4PultCmd(&sInfo);
}

// ������� �������� ������ ����� ��������� ��� ����������
void RecieveAudioPacket(sInfoKadr *psInfo,unsigned char *pbtAudio)
{
  unsigned char SN,btRes;
  unsigned short wCmd,wPckt;
  static sInfoKadr sInfo;

  SN=psInfo->ByteInfoKadr[5];
  if(!SN || psInfo->ByteInfoKadr[6]) return; // ������� �� ������������� ��� ��!
  vPortEnableSwitchTask(false);
  wCmd=psInfo->ByteInfoKadr[8];
  wCmd=(wCmd<<8) | psInfo->ByteInfoKadr[7];
  if(!PultRepeatCmd(SN,wCmd))
  {
    SetAnswerHead2Pult(psInfo,&sInfo);
    wPckt=psInfo->ByteInfoKadr[11];
    wPckt=(wPckt<<8) | psInfo->ByteInfoKadr[10];
/* SendStr2IpLogger("$DEBUG$RecieveAudioPacket: wCmd=%d, wPckt=%d",
                  wCmd,wPckt);*/
    sInfo.ByteInfoKadr[6]=_xRecieveAudioPacket(SN,wPckt,pbtAudio);
    SendAnsw4PultCmd(&sInfo);
  }
  vPortEnableSwitchTask(true);
}

