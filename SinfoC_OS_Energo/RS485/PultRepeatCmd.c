#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"

//*****************************************************************************
typedef struct
{
  unsigned char SN;
  unsigned short wCmd;
  bool bSendAnsw;
  sInfoKadr sInfo;
} LAST_PULT_CMD_TP;

//*****************************************************************************
static LAST_PULT_CMD_TP MasLastPultCmd[NumberKeyboard];

//*****************************************************************************
void InitPultRepeatCmd(void)
{ memset(MasLastPultCmd,0,sizeof(MasLastPultCmd)); }

void DelPultRepeatCmd(unsigned char SN)
{
  unsigned char btInd;

  for(btInd=0;btInd<NumberKeyboard;btInd++)
  {
    if(MasLastPultCmd[btInd].SN==SN)
    {
      vPortEnableSwitchTask(false);
      MasLastPultCmd[btInd].SN=0;
      MasLastPultCmd[btInd].bSendAnsw=false;
      vPortEnableSwitchTask(true);
      return;
    }
  }
}

void SendAnsw4PultCmd(sInfoKadr *psInfo)
{
  unsigned char btInd,SN;
  unsigned short wCmd;

  SN=psInfo->ByteInfoKadr[2];
  wCmd=psInfo->ByteInfoKadr[4];
  wCmd=(wCmd<<8) | psInfo->ByteInfoKadr[3];
if(psInfo->Source>=QueueReplyCnt)
{
  SendStr2IpLogger("$DEBUG$SendAnsw4PultCmd error: SN=%d, Source=%d",SN,psInfo->Source);
  return;
}
  LockPrintf();
  sprintf((char*)(psInfo->ByteInfoKadr+psInfo->L),"<Answ Cmd%02d>",psInfo->ByteInfoKadr[5]);
  UnlockPrintf();
  psInfo->L+=12;
  for(btInd=0;btInd<NumberKeyboard;btInd++)
  {
    if(MasLastPultCmd[btInd].SN==SN)
    {
//SendStr2IpLogger("$DEBUG$SendAnsw4PultCmd: SN=%d, wCmd=%d",SN,wCmd);
      MasLastPultCmd[btInd].wCmd=wCmd;
      MasLastPultCmd[btInd].bSendAnsw=true;
      memcpy(&MasLastPultCmd[btInd].sInfo,psInfo,sizeof(sInfoKadr));
      break;
    }
  }
/*{
static int s_iCycle=0;
s_iCycle++; s_iCycle&=0x1;
if(s_iCycle) return;
}*/
  if(xQueueSend(xQueueReply[psInfo->Source],psInfo,0)==errQUEUE_FULL) CountErrQueueReply++;
}

bool PultRepeatCmd(unsigned char SN,unsigned short wCmd)
{
  unsigned char btInd,btIndFree=0xff;
  LAST_PULT_CMD_TP *psLastCmd=MasLastPultCmd;

  for(btInd=0;btInd<NumberKeyboard;btInd++,psLastCmd++)
  {
    if(!psLastCmd->SN) btIndFree=btInd;
    if(psLastCmd->SN==SN)
    {
      if(wCmd==psLastCmd->wCmd)
      {
        if(psLastCmd->bSendAnsw)
        {
          if(psLastCmd->sInfo.Source>=QueueReplyCnt)
          {
SendStr2IpLogger("$DEBUG$PultRepeatCmd error: SN=%d, wCmd=%d, Source=%d",SN,wCmd,psLastCmd->sInfo.Source);
            return(true);
          }
SendStr2IpLogger("$DEBUG$PultRepeatCmd repeat answer: SN=%d, wCmd=%d",SN,wCmd);
          if(xQueueSend(xQueueReply[psLastCmd->sInfo.Source],&(psLastCmd->sInfo),0)==errQUEUE_FULL) CountErrQueueReply++;
          return(true);
        }
      }
      else
      {
        psLastCmd->wCmd=wCmd;
        psLastCmd->bSendAnsw=false;
      }
      return(false);
    }
  }
  if(btIndFree<NumberKeyboard)
  {
    MasLastPultCmd[btIndFree].SN=SN;
    MasLastPultCmd[btIndFree].wCmd=wCmd;
    MasLastPultCmd[btIndFree].bSendAnsw=false;
SendStr2IpLogger("$DEBUG$PultRepeatCmd new cmd: SN=%d, wCmd=%d",SN,wCmd);
  }
  return(false);
}

