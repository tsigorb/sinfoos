#include "..\Sinfo.h"
#include "..\EDSS\pri_config.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"

/////////////////////////////////////////////////////////////////////
static char NumberSrc[21],NumberDst[21],NameSrc[21];

/////////////////////////////////////////////////////////////////////
void _xAbntRecord(CONNECTION_STATE_TP *psConn,unsigned char btOn)
{ // ���/���� ������ ���������� ��������
  unsigned int dwIP;
  static sCommand sCmd;

  if(GetAddressIpPult(psConn->SN,&dwIP,NULL))
  {
    sCmd.Length=12;
    sCmd.Source=UserRS485;
    sCmd.A1=0;
    sCmd.BufCommand[0]=10;
    sCmd.BufCommand[1]=222;
    sCmd.BufCommand[2]=btOn;
    sCmd.BufCommand[3]=psConn->wConnectionPoint & 0xff;
    SET_DW(sCmd.BufCommand+4,dwIP);
    SET_DW(sCmd.BufCommand+8,psConn->dwId);
    xQueueSendCmd(&sCmd,100);
//SendStr2IpLogger("$DEBUG$_xAbntRecord: ConnId=%d, wCP=%d, btOn=%d",psConn->dwId,psConn->wConnectionPoint & 0xff,btOn);
  }
}

bool SendCmdOtboiISDN(unsigned char Reason,unsigned short wNChan,unsigned short wChanOper)
{
  static sCommand sCmd;
  
  sCmd.Length=7;
  sCmd.Source=UserRS485;// �������� ������� RS485
  sCmd.A1=0;
  sCmd.BufCommand[0]=10; // �������
  sCmd.BufCommand[1]=205; // ���������� "�����"
  sCmd.BufCommand[2]=wNChan >> 8;
  sCmd.BufCommand[3]=wNChan;
  sCmd.BufCommand[4]=wChanOper >> 8; // ����� ���������
  sCmd.BufCommand[5]=wChanOper; // ����� ���������
  sCmd.BufCommand[6]=Reason;
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  return(false);
}

bool _xIncomingCallISDNAnswer(unsigned short wNChan,unsigned short wOper,unsigned char btCommute)
{
  static sCommand sCmd;
  
  sCmd.Source=UserRS485;// �������� ������� RS485
  sCmd.Length=7; // ���������� ���� � �������
  sCmd.A1=0;
  sCmd.BufCommand[0]=10; // �������
  sCmd.BufCommand[1]=203; // ���������� ���������� ������� �������� � ������ ������
  sCmd.BufCommand[2]=wNChan >> 8;
  sCmd.BufCommand[3]=wNChan;
  sCmd.BufCommand[4]=wOper >> 8;
  sCmd.BufCommand[5]=wOper;
  sCmd.BufCommand[6]=btCommute;
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(WaitReply(11,203)) return(true);
    else CountErrEDSS++;
  }
  return(false);
}

bool _xOutgoingCallISDN(unsigned short NChan,unsigned short wOper,unsigned char btE1,
                        char *BufNumbDest,char *BufNumbSrs,char *BufNameSrs,unsigned char SN)
{
  bool bRes=false;
  static sCommand sCmd;

  sCmd.Source=UserRS485; // �������� ������� RS485
  sCmd.Length=21; // ���������� ���� � �������
  sCmd.A1=0;
  sCmd.BufCommand[0]=10; // �������
  sCmd.BufCommand[1]=201; // ����������
  sCmd.BufCommand[2]=NChan >> 8; // N �����������
  sCmd.BufCommand[3]=NChan; // N �����������
  sCmd.BufCommand[4]=wOper >> 8; //����� ������ ���������
  sCmd.BufCommand[5]=wOper; // ����� ������ ���������
  sCmd.BufCommand[6]=btE1;
  sCmd.BufCommand[7]=((unsigned int)BufNumbDest) >> 24; // ��������� �� ��������� �����
  sCmd.BufCommand[8]=((unsigned int)BufNumbDest) >> 16;
  sCmd.BufCommand[9]=((unsigned int)BufNumbDest) >> 8;
  sCmd.BufCommand[10]=(unsigned int)BufNumbDest;
  sCmd.BufCommand[11]=1; // ��� �����
  sCmd.BufCommand[12]=((unsigned int)BufNumbSrs) >> 24; // ��������� �� ����� �����������
  sCmd.BufCommand[13]=((unsigned int)BufNumbSrs) >> 16;
  sCmd.BufCommand[14]=((unsigned int)BufNumbSrs) >> 8;
  sCmd.BufCommand[15]=(unsigned int)BufNumbSrs;
  sCmd.BufCommand[16]=((unsigned int)BufNameSrs) >> 24; // ��������� �� ��� �����������
  sCmd.BufCommand[17]=((unsigned int)BufNameSrs) >> 16;
  sCmd.BufCommand[18]=((unsigned int)BufNameSrs) >> 8;
  sCmd.BufCommand[19]=(unsigned int)BufNameSrs;
  sCmd.BufCommand[20]=SN;
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else 
  {
    if(WaitReply(11,201)) bRes=true;
    else CountErrEDSS++;
  }
//SendStr2IpLogger("$OutgoingISDN$_xOutgoingCallISDN: NChan=%d, btE1=%d,bRes=%d",NChan,btE1,bRes);
  return bRes;
}

bool _xIncomingCallISDNFree(unsigned short wNChan,unsigned short wChanOper,unsigned char btKPV)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485;    // �������� ������� RS485
  sCmd.Length=7;            // ���������� ���� � �������
  sCmd.A1=0;
  sCmd.BufCommand[0]=10;    // �������
  sCmd.BufCommand[1]=202;   // ���������� - ���������� ������� ��������
  sCmd.BufCommand[2]=wNChan >> 8; // N �����������
  sCmd.BufCommand[3]=wNChan;      // N �����������
  sCmd.BufCommand[4]=wChanOper >> 8; // ����� ���������
  sCmd.BufCommand[5]=wChanOper;      // ����� ���������
  if(btKPV) sCmd.BufCommand[6]=1;
  else sCmd.BufCommand[6]=0;
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(WaitReply(11,202)) return(true);
    CountErrEDSS++;
  }
  return(false);
}

bool OutgoingCallISDN(CONNECTION_STATE_TP *psConn)
{
  unsigned char btE1Cnt;

  if(psConn->btE1)
  {
    return _xOutgoingCallISDN(psConn->wConnectionPoint,psConn->wOperatorChannel,
                              psConn->btE1,psConn->NumbDst,psConn->NumbSrc,psConn->NameSrc,
                              psConn->bAutomaticCall);
  }
  btE1Cnt=0;
  psConn->btE1=GetFirstE1();
  do
  { // ���� �� ���� ��������� ������� E1
    if(psConn->btE1==0xff)
    {
      if(btE1Cnt>=NUM_TRUNKS) return(false);
    }
    else
    {
      if(_xOutgoingCallISDN(psConn->wConnectionPoint,psConn->wOperatorChannel,
                            psConn->btE1,psConn->NumbDst,psConn->NumbSrc,psConn->NameSrc,
                            psConn->bAutomaticCall)) break;
    }
    psConn->btE1=GetNextE1(psConn->btE1);
    btE1Cnt++;
  }
  while(1);
  return(true);
}

/////////////////////////////////////////////////////////////////////
void GetNumberFromAddr(char *pchBuf,unsigned char *pParam)
{
  int i;
  unsigned int dwNumb;

  dwNumb=pParam[0];
  dwNumb=(dwNumb<<8) | pParam[1];
  dwNumb=(dwNumb<<8) | pParam[2];
  dwNumb=(dwNumb<<8) | pParam[3];
  LockPrintf();
  sprintf(pchBuf,"%d",dwNumb);
  UnlockPrintf();
}

void GetNameFromAddr(char *pchBuf,unsigned char *pParam,char iMaxLen)
{
  int i;
  unsigned int dwAddr;

  dwAddr=pParam[0];
  dwAddr=(dwAddr<<8) | pParam[1];
  dwAddr=(dwAddr<<8) | pParam[2];
  dwAddr=(dwAddr<<8) | pParam[3];
  i=0;
  pParam=(unsigned char *)dwAddr;
  if(pParam)
  {
    for(;(i<iMaxLen) && pParam[i];i++) pchBuf[i]=pParam[i];
  }
  pchBuf[i]=0;
}

void IncomingCallISDN(unsigned short wNChan,sInfoKadr *psVxCall)
{
  unsigned char btInd,SN;
  CONNECTION_STATE_TP *psConn;
  
  GetNumberFromAddr(NumberSrc,&psVxCall->ByteInfoKadr[5]);  // ����� �����������
  GetNameFromAddr(NameSrc,&psVxCall->ByteInfoKadr[9],18);   // ��� �����������
  GetNumberFromAddr(NumberDst,&psVxCall->ByteInfoKadr[24]); // ����� �����������
  SN=FindPultByNumb(NumberSrc,NULL);
  if(SN) 
  { // ����� ���������� ������ ������� ��� ����� �����!
    SendCmdOtboiISDN(PRI_CAUSE_CALL_REJECTED,wNChan,0);
    return;
  }
  btInd=0;
  SN=FindPultByNumb(NumberDst,&btInd);
/*SendStr2IpLogger("$DEBUG$IncomingCallISDN: NumberSrc=%s, NumberDst=%s, SN=%d",
                 NumberSrc,NumberDst,SN);*/
  if(SN==0) 
  {
    if(!CrossCallISDN(wNChan,psVxCall->ByteInfoKadr[18],NumberSrc,NameSrc,NumberDst))
    { SendCmdOtboiISDN(PRI_CAUSE_CALL_REJECTED,wNChan,0); }
    return;
  }
  psConn=GetNewConnection();
  if(!psConn)
  {
    SendCmdOtboiISDN(PRI_CAUSE_CALL_REJECTED,wNChan,0);
    return;
  }
  psConn->SN=0xff;
  psConn->wConnectionPoint=wNChan;
  psConn->wOperatorChannel=0x100;   // ��������� �������������� ����� ��������� ����� �������� �� ������������ ��������!
  psConn->btType=ISDN;
  memcpy(psConn->NumbSrc,NumberSrc,MAX_DIGIT_TLFN);
  memcpy(psConn->NameSrc,NameSrc,d_NameLen);
  memcpy(psConn->NumbDst,NumberDst,MAX_DIGIT_TLFN);
  psConn->dwTimerEnd=MassTimer[TIMER_INCOMINGCALL]+xTaskGetTickCount();
  SetConnectionStateByPtr(psConn,IncomingCall,0);
  if(IsConnectionBinding(psConn)) _xIncomingCallISDNFree(wNChan,0,0);
  else OtboiConnection(psConn);
}

void AnswerOutgoingCallISDN(unsigned short wNChan,unsigned short wOper)
{
  CONNECTION_STATE_TP *psConn;
  
  if(IsCrossCommuteAbnt(wNChan))
  {
    CrossCommuteAnswer(wNChan,wOper);
    return;
  }
  psConn=GetConnectionStatePtrByFind(0,wNChan,wOper,ISDN,0xff,NULL);
  if(psConn)
  {
    SetConnectionStateByPtr(psConn,Talk,0);
    if(!wOper)
    { // ������� ����������� - ��������� �����-��������� � ������������� ����� �����
      StartAudioMessage(psConn);
      if(psConn->btOpovRec)
      {
        _xAbntRecord(psConn,1);
        psConn->btOpovRec++;
      }
      psConn->dwTimerEnd=GetAudioTime(psConn->SN)*psConn->btACycle+xTaskGetTickCount()+100;
    }
  }
  else SendCmdOtboiISDN(89,wNChan,0);
}

bool RepeatOugoingCall(CONNECTION_STATE_TP *psConn)
{
  unsigned char btE1;
  unsigned short NChan;

  if((GetConnectionStateFromPtr(psConn)==Call) && (psConn->btOutgoingCnt<NUM_TRUNKS))
  {
    btE1=GetNextE1(psConn->btE1);
    if(btE1!=0xff)
    {
      NChan=GetFreeConnectionPoint();
      if(NChan!=0xff)
      {
        psConn->btE1=btE1;
        _xOutgoingCallISDN(NChan,psConn->wOperatorChannel,btE1,
                           psConn->NumbDst,psConn->NumbSrc,psConn->NameSrc,
                           psConn->bAutomaticCall);
        ReplaceCP2Selector(psConn->SN,psConn->wConnectionPoint,NChan);
        psConn->wConnectionPoint=NChan;
        if(psConn->wOperatorChannel) psConn->dwTimerEnd=MassTimer[TIMER_OUTGOINGCALL]+xTaskGetTickCount();
        else psConn->dwTimerEnd=(unsigned int)psConn->btConnTm*1000+xTaskGetTickCount();
        psConn->btOutgoingCnt++;
        return(true);
      }
    }
  }
  return(false);
}

void FreeAbntOutgoingCallISDN(unsigned short wNChan)
{
  unsigned char btInd;
  CONNECTION_STATE_TP *psConn;

  btInd=0;
  do
  {
    psConn=GetConnectionStatePtrFromIndex(btInd);
    if(!psConn) return;
    btInd++;
    if(psConn->wConnectionPoint==wNChan)
    {
      psConn->btOutgoingCnt=0xff; // ��������� ����� �������� �� ��������� ����� ���!
      if(!psConn->wOperatorChannel)
      { // ���������� �������� - ������������� ����� �� ������ ������
        psConn->dwTimerEnd=(unsigned int)psConn->btAnswTm*1000+xTaskGetTickCount();
        SetConnectionStateByPtr(psConn,CallWaitAnswer,0);
      }
      return;
    }
  }
  while(1);
}

void BusyISDN(unsigned short wNChan,bool bBusy)
{
  CONNECTION_STATE_TP *psConn;
  
  if(IsCrossCommuteAbnt(wNChan))
  { CrossCommuteOtboi(wNChan); return; }
  psConn=GetConnectionStatePtrByFind(0,wNChan,0xffff,ISDN,0xff,NULL);
  if(psConn)
  {
    if(RepeatOugoingCall(psConn))
    {
      SendCmdOtboiISDN(PRI_CAUSE_CALL_REJECTED,wNChan,0);
      return;
    }
    if(DelAbntFromSelector(psConn->SN,psConn->dwId))
    {
      SetConnectionStateByPtr(psConn,Busy,0);
      psConn->dwTimerEnd=400+xTaskGetTickCount(); // �������� ����� ������ �������� ��� �������� �������� ������
    }
    else
    {
      switch(psConn->wOperatorChannel)
      {
        case 0:
          StopOpov(psConn,true);
          break;
        case 0xffff:
          OtboiConnection(psConn);
          break;
        default:
          if(bBusy)
          {
            SetConnectionStateByPtr(psConn,Busy,0);
            psConn->dwTimerEnd=d_TIMER_BUSY+xTaskGetTickCount();
          }
          else OtboiConnection(psConn);
          break;
      }
    }
  }
  else SendCmdOtboiISDN(PRI_CAUSE_CALL_REJECTED,wNChan,0);
}

void OtboiISDN(unsigned short wNChan)
{ BusyISDN(wNChan,false); }

