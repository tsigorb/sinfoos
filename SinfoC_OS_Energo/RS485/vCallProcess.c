#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"

///////////////////////////////////////
void ISDN_Event(unsigned short wNChan,sInfoKadr *psVxCallT)
{ // ������� �� PRI
  unsigned short wChanOper;
  unsigned int dwCause;

  switch(psVxCallT->ByteInfoKadr[4])
  { // ����������
    case 201 :  // ����� �� ������� 201
      /*if(sVxCallT.ByteInfoKadr[26]!=1)
      { // ���� ������ -> ��������� ������� ��������
        wChanOper = ((unsigned short)(sVxCallT.ByteInfoKadr[5]))<<8 | sVxCallT.ByteInfoKadr[6];
      }*/
      break;
    case 207:  // ������� �������� ����� c ISDN
      IncomingCallISDN(wNChan,psVxCallT);
      break;
    case 208:  // �������� �������������� ����� c ISDN
      break;
    case 209:  // ������� Alert c ISDN
      FreeAbntOutgoingCallISDN(wNChan);
      break;
    case 210:  // ������� "� ��������" ...
/*      dwCause=*((unsigned int *)(psVxCallT->ByteInfoKadr+18));
SendStr2IpLogger("$ISDN$Progess ISDN: wNChan=%d, dwCause=%d",wNChan,dwCause);
*/
//      if(dwCause!=0) BusyISDN(wNChan,true);
      break;
    case 211:  // ������� Speaker c ISDN
      //����� ��� �� ����� ��� ���������...
      //wChanOper = ((unsigned short)(sVxCallT.ByteInfoKadr[5]))<<8 | sVxCallT.ByteInfoKadr[6];
      break;
    case 212:  // ������� ���� ������
      wChanOper=((unsigned short)(psVxCallT->ByteInfoKadr[5]))<<8 | psVxCallT->ByteInfoKadr[6];
      AnswerOutgoingCallISDN(wNChan,wChanOper);
      break; 
    case 213:  // ������� ����� c ISDN (������� ������� ������)
    case 214:  // ������� ����� c ISDN (����� ��� ����������)
      OtboiISDN(wNChan);
      break;
    default: break;
  }
}

void IPPult_Event(sInfoKadr *psInfo)
{
  switch(psInfo->ByteInfoKadr[4])
  {
    case 1: // ��������� ������������� ������
      break;
    case 252: // ����� �� ������ �� ������� ��
      IpPultEnergo_RcvAnsw(psInfo);
      break;
    case 253: // ������� �� ������ ��� ��
      IpPultEnergo_RcvCmd(psInfo);
      break;
    default: break;
  }
}

void EndSound2ConnectionPoint(unsigned short wNChan)
{
}

void ProcessQueueVxCall(void)
{
  unsigned short wNChan;
  static sInfoKadr sVxCallT; // ��������� ������ � �������� ������

  if(xQueueReceive(xQueueVxCall,&sVxCallT,0))
  { // ���� �������� ������ � �������
    wNChan=((unsigned short)sVxCallT.ByteInfoKadr[1] << 8) | (sVxCallT.ByteInfoKadr[2]+1);
    switch(sVxCallT.ByteInfoKadr[3])
    {
        case 1: // ������ "����� �������"
          SignalATC_Ready(wNChan);
          break;
        case 2: // �������� ����� ���
          IncomingCall_TDC(wNChan,sVxCallT.ByteInfoKadr[4],sVxCallT.ByteInfoKadr[5]);
          break;
        case 3: // ����� ����
//          IncomingCall_DTMF(wNChan,sVxCallT.ByteInfoKadr[4]);
          break;
        case 4: // �������� ����� �� ������
//          IncomingCall_Level(wNChan);
          break;
        case 5: // ������ ��� "������"
          SignalATC_Busy(wNChan);
          break;
        case 6: // �������� ����� ���
          IncomingCallATC(wNChan);
          break;
        case 7: // �������� ����� "��������"
          //IncomingCallSelectorCtrl(wNChan);
          break;
        case 9: // ����� ��������� ���
//          Call_ImitatorTDC(NumbAb_NChanV,sVxCallT.ByteInfoKadr[4],&DanSwetKonf);
          break;
        case 11: // ��� - ������ �������/��������
//          Call_CB(sVxCallT.ByteInfoKadr[4]);
          break;
        case 12: // ����� ��������� ����
//          Call_ImitatorRSDT(NumbAb_NChanV,sVxCallT.ByteInfoKadr[4],&DanSwetKonf);
          break;
        case 15: // ����� �� �������� ����
//          if(sVxCallT.ByteInfoKadr[4]==10) IncomingCall_PPSR_RSDT(wNChan,PPSR); // ==10 - ����������� ��������
          break;
        case 16 :
        case 17 :
        case 18 :
        case 19 :
        case 20 :
        case 21 :
        case 22 :
        case 23 : // �������� ����� ���� ���������
//          InCall_Special(sVxCallT.ByteInfoKadr[4],sVxCallT.ByteInfoKadr[3]+7,NumbAb_NChanV);
          break;
        case EndSound:
          EndSound2ConnectionPoint(wNChan);
          break;
        case ISDN:
          ISDN_Event(wNChan,&sVxCallT);
          break;
        case 34: // ������� ������������� ������
          IPPult_Event(&sVxCallT);
          break;
        default: break;
    }
  }
}

void DestroyAllConnections2Pults(void)
{
  unsigned char PultNb,SN,btInd;
  CONNECTION_STATE_TP *psConn;

  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    SN=GetPultSNFromIndex(PultNb);
    if(SN)
    {
      if(IsNeedDestroyAllConnections2Pult(SN))
      {
        btInd=0;
        do
        {
          psConn=GetConnectionStatePtrFromIndex(btInd);
          if(psConn && psConn->dwId && psConn->wConnectionPoint && (psConn->SN==SN))
          {
            if(ExistOpovProcess(psConn->SN,0) || (IsSelector2Pult(psConn->SN))) break;
            OtboiConnection(psConn);
          }
          btInd++;
        }
        while(psConn);
      }
    }
  }
}

void vCallProcess(void *pvParameters) 
{
  InitConnectionStateArray();
  InitCrossCommute();
//  InitFreeHandConf();
  InitAudioMessages();
  InitPults(true); // ������������� ���� �������
  while(1)
  {
    if(IsNeedInitPults()) InitPults(false);
    DestroyAllConnections2Pults();
    ListMU2Pult_Process();
    ProcessQueueVxCall();
    DetectAllTimers();
    SetWDState(d_WDSTATE_TaskCall);
    vTaskDelay(1);
  }
}

