#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"

/////////////////////////////////////////////////////////////////////
typedef struct
{
  unsigned char SN,btTOut0;
  unsigned int dwId,dwTOut;
} OPOVPROCESS_TP;

/////////////////////////////////////////////////////////////////////
static OPOVPROCESS_TP sOpov={0,0,0,0};

/////////////////////////////////////////////////////////////////////
bool ExistOpovProcess(unsigned char SN,unsigned int dwId)
{
  if(sOpov.dwId)
  {
    if(!SN) return true;
    if(SN==sOpov.SN)
    {
      if(!dwId) return true;
      if(dwId==sOpov.dwId) return true;
    }
  }
  return false;
}

unsigned int StartOpovProcess(unsigned char SN,unsigned char btTOut)
{
  vPortEnableSwitchTask(false);
  if(sOpov.dwId)
  {
    if(SN!=sOpov.SN)
    {
      vPortEnableSwitchTask(true);
      return 0;
    }
  }
  sOpov.SN=SN;
  sOpov.btTOut0=btTOut;
  sOpov.dwId=xTaskGetTickCount();
  sOpov.dwTOut=sOpov.dwId+btTOut*60000ul;
  vPortEnableSwitchTask(true);
SendStr2IpLogger("$DEBUG$StartOpovProcess: SN=%d, Id=%d, TOut=%d",SN,sOpov.dwId,btTOut);
  return sOpov.dwId;
}

void StopOpovProcess(unsigned char SN)
{
  if(SN!=sOpov.SN) return;
SendStr2IpLogger("$DEBUG$StopOpovProcess: SN=%d, Id=%d",SN,sOpov.dwId);
  vPortEnableSwitchTask(false);
  sOpov.SN=0;
  sOpov.btTOut0=0;
  sOpov.dwId=0;
  sOpov.dwTOut=0;
  vPortEnableSwitchTask(true);
}

void OpovProcessTOut(void)
{
  vPortEnableSwitchTask(false);
  if(sOpov.dwId && IsCurrentTickCountGT(sOpov.dwTOut))
  { // ��������� ����� �������� �������������� ���������� � �������
SendStr2IpLogger("$DEBUG$OpovProcessTOut: SN=%d, Id=%d",sOpov.SN,sOpov.dwId);
    StopOpovAbonents(sOpov.SN,false);
    DeleteAudioMessage(sOpov.SN);
    DestroySelector(sOpov.SN);
    sOpov.SN=0;
    sOpov.btTOut0=0;
    sOpov.dwId=0;
    sOpov.dwTOut=0;
  }
  vPortEnableSwitchTask(true);
}

void RestartOpovProcessTOut(unsigned char SN)
{
  vPortEnableSwitchTask(false);
  if(SN==sOpov.SN)
  {
    sOpov.dwTOut=xTaskGetTickCount()+sOpov.btTOut0*60000ul;
  }
  vPortEnableSwitchTask(true);
}

