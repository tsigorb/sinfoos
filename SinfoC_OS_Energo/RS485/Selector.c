#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"

bool CreateSelector(unsigned char SN,unsigned char btCnt,unsigned char btOpID,unsigned char btSelNb)
{
  unsigned char i,j;
  static sCommand sCmd;
  unsigned short wOperator=GetOperatorChannelFromCode(btOpID);

  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.Length=7+btCnt*2; // ���������� ���� � �������
  sCmd.BufCommand[0]=10; // �������
  sCmd.BufCommand[1]=155; // ���������� ���������� ������������ � �������� 
  sCmd.BufCommand[2]=SN;
  sCmd.BufCommand[3]=btSelNb;
  sCmd.BufCommand[4]=btCnt;
  sCmd.BufCommand[5]=wOperator >> 8; 
  sCmd.BufCommand[6]=wOperator;
  for(i=0,j=7;i<btCnt;i++,j+=2)
  {
    sCmd.BufCommand[j]=0x80; // � ����������� �����
    sCmd.BufCommand[j+1]=0;
  }
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else return WaitReply(11,155);
  return(false);
}

void DestroySelector(SN)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.Length=3;
  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[1]=156;
  sCmd.BufCommand[2]=SN;
  xQueueSendCmd(&sCmd,100);
}

void _xDelCPFromSelector(unsigned char SN,unsigned short wCP)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.BufCommand[0]=10; // �������
  sCmd.BufCommand[1]=159; // �������� ���������� ������������ ��������� � ������������ � �������� 
  sCmd.BufCommand[2]=SN;
  sCmd.BufCommand[3]=1;
  sCmd.BufCommand[4]=wCP >> 8;
  sCmd.BufCommand[5]=wCP;
  sCmd.Length=6; // ���������� ���� � �������
  xQueueSendCmd(&sCmd,100);
}

bool _xAddCP2Selector(unsigned char SN,unsigned short wCP)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.BufCommand[0]=10; // �������
  sCmd.BufCommand[1]=160; // ���������� ��������� �� ����� ��������
  sCmd.BufCommand[2]=SN;
  sCmd.BufCommand[3]=1;
  sCmd.BufCommand[4]=wCP >> 8;
  sCmd.BufCommand[5]=wCP;
  sCmd.Length=6; // ���������� ���� � �������
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(WaitReply(11,160)) return(true);
    else CountErrCommute++;
  }
  return(false);
}

bool AddAbnt2Selector(unsigned char SN,unsigned int dwId,unsigned char btMode)
{
  unsigned short wCP;
  bool bRet=false;
  CONNECTION_STATE_TP *psConn;

  psConn=FindConnectionById(dwId);
SendStr2IpLogger("$DEBUG$AddAbnt2Selector: SN=%d, dwId=%d, psConn=%p",SN,dwId,psConn);
  if(psConn)
  {
    wCP=psConn->wConnectionPoint;
    if(!btMode) wCP|=0x8000;
    if(_xAddCP2Selector(SN,wCP)) bRet=true;
    wCP&=0x7fff;
    if(ExistOpovProcess(psConn->SN,0))
    {
      Commute(false,wCP,psConn->wOperatorChannel);
      psConn->wOperatorChannel=0xffff;
    }
    else
    {
      if(bRet) Commute(false,wCP,psConn->wOperatorChannel);
    }
  }
  return(bRet);
}

void ReplaceCP2Selector(unsigned char SN,unsigned short wCP_Old,unsigned short wCP_New)
{
  _xDelCPFromSelector(SN,wCP_Old);
  _xAddCP2Selector(SN,wCP_New | 0x8000);
}

bool _xDelLeaderSelector(unsigned char SN,sCommand *psCmd)
{ // ������� �������� � ����� ��������
  psCmd->Length=7;
  psCmd->BufCommand[1]=176;
  psCmd->BufCommand[3]=0; // �� ������������
  psCmd->BufCommand[4]=0; // �� ������������
  psCmd->BufCommand[5]=0xff;
  psCmd->BufCommand[6]=0;
  xQueueSendCmd(psCmd,30);
  return(WaitReply(psCmd->BufCommand[0],psCmd->BufCommand[1]));
}

bool SetAbnt2SelectorState(unsigned char SN,unsigned int dwId,unsigned char btState)
{
  unsigned char btCmd;
  unsigned short wCP;
  CONNECTION_STATE_TP *psConn;
  static sCommand sCmd;

  psConn=FindConnectionById(dwId);
  if(!psConn) return(false);
  wCP=psConn->wConnectionPoint;
  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[2]=SN;
SendStr2IpLogger("$DEBUG$SetAbnt2SelectorState: SN=%d, wCP=%d, btState=%d",SN,wCP,btState);
  switch(btState)
  {
    case 0: // ���������
      if(IsLeaderSelector(SN,wCP))
      {
        if(!_xDelLeaderSelector(SN,&sCmd)) return(false);
      }
      btCmd=157; // ���������� ����� ��������
      break;
    case 1: // ��������
      if(IsLeaderSelector(SN,wCP))
      { // ������� �������� � ����� ��������
        return(_xDelLeaderSelector(SN,&sCmd));
      }
      else btCmd=158; // ������������� ����� ��������
      break;
    case 2: // �������
      if(IsLeaderSelector(SN,wCP)) return(true);
      if(!_xDelLeaderSelector(SN,&sCmd)) return(false);
      btCmd=176;
      break;
    case 3: // ���������� ������
    case 4: // ��������� ������
      return(CmdEnDisOut(wCP,(btState==4)));
    default: return(false);        
  }
  sCmd.BufCommand[1]=btCmd;
  if((btCmd==157) || (btCmd==158))
  {
    sCmd.Length=6;
    sCmd.BufCommand[3]=1;
    sCmd.BufCommand[4]=wCP >> 8;
    sCmd.BufCommand[5]=wCP;
  }
  else
  {
    sCmd.Length=7;
    sCmd.BufCommand[3]=0; // �� ������������
    sCmd.BufCommand[4]=0; // �� ������������
    sCmd.BufCommand[5]=wCP >> 8;
    sCmd.BufCommand[6]=wCP;
  }
  if(xQueueSendCmd(&sCmd,30)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else 
  {
    if(WaitReply(11,sCmd.BufCommand[1])) return(true);
  }
  return(false);
}

bool DelAbntFromSelector(unsigned char SN,unsigned int dwId)
{
  CONNECTION_STATE_TP *psConn;
  static sCommand sCmd;

  psConn=FindConnectionById(dwId);
  if(!psConn) return(false);
//SendStr2IpLogger("$DEBUG$DelAbntFromSelector: SN=%d, dwId=%d, wCP=%x",SN,dwId,psConn->wConnectionPoint);
  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[1]=159;
  sCmd.BufCommand[2]=SN;
  sCmd.BufCommand[3]=1;
  sCmd.BufCommand[4]=psConn->wConnectionPoint >> 8;
  sCmd.BufCommand[5]=psConn->wConnectionPoint;
  sCmd.Length=6;
  xQueueSendCmd(&sCmd,30);
  if(WaitReply(11,sCmd.BufCommand[1])) return(true);
  return(false);
}

