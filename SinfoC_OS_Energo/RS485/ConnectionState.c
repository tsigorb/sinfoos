#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"

//////////////////////////////////////////////////////////////
// ���������� true � IP-����� � ����� ����� �������� ��� ��������� �����������,
// ���� ������ ���������������� � ����-�� ���� �� ������� ������ ������ ������������
// ����� - false
bool GetAddrInfo4Record(unsigned char SN,unsigned char RecordNumb,
                        unsigned int *pdwIpAddr,unsigned short *pwPort);
                        
// ��������� ����� pBuf ����� ��� ������������� �������� � ��������� �������
// � ���������� ������ ������ �������������
unsigned char GetRecordTimeStamp(unsigned char *pBuf);

//////////////////////////////////////////////////////////////
static CONNECTION_STATE_TP MassConnState[MaxConnectionActive];

static unsigned int s_dwConnId=0;

//////////////////////////////////////////////////////////////
void InitConnectionStateArray(void)
{
  memset(MassConnState,0,sizeof(MassConnState));
}

unsigned char FindFirstConnectionIndex(unsigned char SN,
                                       unsigned short wConnectionPoint,unsigned short wOperatorChanel,
                                       unsigned char TypeAbon,unsigned char NumbAbon,unsigned char *pbtTlfn)
{
  unsigned char btIndex;
  CONNECTION_STATE_TP *psConn;
  
  if((SN==0) && (wConnectionPoint==0xffff) && (wOperatorChanel==0xffff) &&
     (NumbAbon!=0xff) && !pbtTlfn) return(0xff);
  psConn=MassConnState;
  for(btIndex=0;btIndex<MaxConnectionActive;btIndex++,psConn++)
  {
    if(!psConn->dwId) continue;
    if(SN)
    {
      if(SN!=psConn->SN) continue;
    }
    if(wConnectionPoint!=0xffff)
    {
      if(wConnectionPoint!=psConn->wConnectionPoint) continue;
    }
    if(wOperatorChanel!=0xffff)
    {
      if(wOperatorChanel!=psConn->wOperatorChannel) continue;
    }
    if(TypeAbon!=psConn->btType) continue;
    if(NumbAbon!=0xff)
    {
      if(NumbAbon!=psConn->NumbAbon) continue;
    }
    if(pbtTlfn)
    {
//      if(strcmp((char *)pbtTlfn,(char *)psConn->btTlfn)) continue;
    }
    return(btIndex);
  }
  return(0xff);
}

unsigned char FindNextConnectionIndex(unsigned char btIndex,
                                   unsigned char SN,
                                   unsigned short wConnectionPoint,
                                   unsigned short wOperatorChanel,
                                   unsigned char TypeAbon,
                                   unsigned char NumbAbon,
                                   unsigned char *pbtTlfn)
{
  CONNECTION_STATE_TP *psConn;

  if((SN==0) && (wConnectionPoint==0xffff) && (wOperatorChanel==0xffff) &&
     (NumbAbon==0xff) && !pbtTlfn) return(0xff);
  btIndex++;
  psConn=MassConnState+btIndex;
  for(;btIndex<MaxConnectionActive;btIndex++,psConn++)
  {
    if(!psConn->dwId) continue;
    if(SN)
    {
      if(SN!=psConn->SN) continue;
    }
    if(wConnectionPoint!=0xffff)
    {
      if(wConnectionPoint!=psConn->wConnectionPoint) continue;
    }
    if(wOperatorChanel!=0xffff)
    {
      if(wOperatorChanel!=psConn->wOperatorChannel) continue;
    }
    if(TypeAbon!=psConn->btType) continue;
    if(NumbAbon!=0xff)
    {
      if(NumbAbon!=psConn->NumbAbon) continue;
    }
    if(pbtTlfn)
    {
//      if(strcmp((char *)pbtTlfn,(char *)psConn->btTlfn)) continue;
    }
    return(btIndex);
  }
  return(0xff);
}

CONNECTION_STATE_TP *GetConnectionStatePtrByFind(unsigned char SN,
                                     unsigned short wConnectionPoint,unsigned short wOperatorChanel,
                                     unsigned char TypeAbon,unsigned char NumbAbon,unsigned char *pbtTlfn)
{
  unsigned char btIndex;
  
  btIndex=FindFirstConnectionIndex(SN,wConnectionPoint,wOperatorChanel,TypeAbon,NumbAbon,pbtTlfn);
  if(btIndex>=MaxConnectionActive) return(NULL);
  return(MassConnState+btIndex);
}

unsigned char _GetFreeConnectionStateIndex(void)
{
  unsigned char btIndex;
  CONNECTION_STATE_TP *psConn=MassConnState;

  for(btIndex=0;btIndex<MaxConnectionActive;btIndex++,psConn++)
  {
    if(!psConn->dwId)
    { psConn->dwState=0; return(btIndex); }
  }
  return(0xff);
}

CONNECTION_STATE_TP *GetNewConnection(void)
{
  unsigned char btInd=_GetFreeConnectionStateIndex();
  if(btInd!=0xff)
  {
    s_dwConnId++;
    if(!s_dwConnId) s_dwConnId++;
    memset(MassConnState+btInd,0x0,sizeof(CONNECTION_STATE_TP));
    MassConnState[btInd].dwId=s_dwConnId;
    return(MassConnState+btInd);
  }
  return(NULL);
}

void SetConnectionBinding(CONNECTION_STATE_TP *psConn,unsigned char SN)
{
  unsigned char btInd;

  for(btInd=0;btInd<NumberKeyboard;btInd++)
  {
    if(!psConn->MasBind[btInd])
    { psConn->MasBind[btInd]=SN; return; }
  }
}

void ClrConnectionBinding(CONNECTION_STATE_TP *psConn,unsigned char SN)
{
  unsigned char btInd;

  for(btInd=0;btInd<NumberKeyboard;btInd++)
  {
    if(psConn->MasBind[btInd]==SN)
    { psConn->MasBind[btInd]=0; return; }
  }
}

bool IsConnectionBinding(CONNECTION_STATE_TP *psConn)
{
  unsigned char btInd;

  for(btInd=0;btInd<NumberKeyboard;btInd++)
  {
    if(psConn->MasBind[btInd]) return(true);
  }
  return(false);
}

void ClearAllConnectionBinding(CONNECTION_STATE_TP *psConn)
{
  unsigned char btInd;

  for(btInd=0;btInd<NumberKeyboard;btInd++) psConn->MasBind[btInd]=0;
}

CONNECTION_STATE_TP *FindConnectionById(unsigned int dwId)
{
  unsigned char btIndex;
  CONNECTION_STATE_TP *psConn=MassConnState;

  if(!dwId) return(NULL);
  for(btIndex=0;btIndex<MaxConnectionActive;btIndex++,psConn++)
  {
    if(psConn->dwId==dwId) return(psConn);
  }
  return(NULL);
}

CONNECTION_STATE_TP *GetConnectionStatePtrFromIndex(unsigned char btInd)
{
  if(btInd>=MaxConnectionActive) return(NULL);
  return(MassConnState+btInd);
}

unsigned char GetConnectionStateIndexFromPtr(CONNECTION_STATE_TP *psConn0)
{
  unsigned char btInd;
  CONNECTION_STATE_TP *psConn=MassConnState;

  for(btInd=0;btInd<MaxConnectionActive;btInd++,psConn++)
  {
    if(psConn0==psConn) return(btInd);
  }
  return(0xff);
}

unsigned char GetConnectionStateFromPtr(CONNECTION_STATE_TP *psConn)
{
  return(psConn->dwState & 0xff);
}

bool IsAutomaticCallConnection(CONNECTION_STATE_TP *psConn)
{ return((psConn->dwState & d_AutomaticCallAbonent)!=0); }

void SetAutomaticCallConnection(CONNECTION_STATE_TP *psConn)
{ psConn->dwState|=d_AutomaticCallAbonent; }

bool IsFreeMKAChannel(unsigned short wChan)
{
  unsigned char btIndex;
  CONNECTION_STATE_TP *psConn=MassConnState;

  if(!wChan) return(false);
  for(btIndex=0;btIndex<MaxConnectionActive;btIndex++,psConn++)
  {
    if(psConn->dwId && (psConn->wConnectionPoint==wChan)) return(false);
  }
  return(true);
}

bool IsOtherConnectionUsedMKALine(CONNECTION_STATE_TP *psConn0)
{
  unsigned char btIndex;
  CONNECTION_STATE_TP *psConn=MassConnState;

  if(!psConn->dwId || !psConn0->wConnectionPoint) return(false);
  for(btIndex=0;btIndex<MaxConnectionActive;btIndex++,psConn++)
  {
    if(psConn!=psConn0)
    {
      if(psConn->dwId && (psConn->wConnectionPoint==psConn0->wConnectionPoint)) return(true);
    }
  }
  return(false);
}

void Connection_PultNoAnswer2MU(LIST_MU2PULT_TP *pItem)
{
  switch(GetConnectionStateFromPtr(pItem->psConn))
  {
    case Passive:
      Passive_PultNoAnswer2MU(pItem);
      break;
    case IncomingCall:
      IncomingCall_PultNoAnswer2MU(pItem);
      break;
    case Busy:
      Busy_PultNoAnswer2MU(pItem);
      break;
    case Talk:
      Talk_PultNoAnswer2MU(pItem);
      break;
    default:
      break;
  }
}

void Connection_PultAnswer2MU(LIST_MU2PULT_TP *pItem,sInfoKadr *psInfo)
{
  switch(GetConnectionStateFromPtr(pItem->psConn))
  {
    case Passive:
      Passive_PultAnswer2MU(pItem,psInfo);
      break;
    case IncomingCall:
      IncomingCall_PultAnswer2MU(pItem,psInfo);
      break;
    case Busy:
      Busy_PultAnswer2MU(pItem,psInfo);
      break;
    case Talk:
      Talk_PultAnswer2MU(pItem,psInfo);
      break;
    case CallTest:
//      CallTest_PultAnswer2MU(pItem,psInfo);
      break;
    default:
      break;
  }
}

bool PutConnState2AllPult(CONNECTION_STATE_TP *psConn)
{
  unsigned char btInd,SN;
  LIST_MU2PULT_TP *psCmdMU;
  
  ClearAllConnectionBinding(psConn);
  btInd=0;
  do
  {
    SN=FindPultByNumb(NULL,&btInd);
    if(SN)
    { // ���������� �� ������������������ ����� ���� � ��������� ����������
/*SendStr2IpLogger("$DEBUG$PutConnState2AllPult: SN=%d, btInd=%d",
                 SN,btInd);*/
      if(psConn->wOperatorChannel && (psConn->wOperatorChannel!=0xffff))
      {
        psCmdMU=ListMU2Pult_AddItem(NULL);
        if(!psCmdMU) break;
        SetConnectionBinding(psConn,SN);
        psCmdMU->SN=SN;
        psCmdMU->pFunc_PultAnswer2MU=Connection_PultAnswer2MU;
        psCmdMU->pFunc_PultNoAnswer2MU=Connection_PultNoAnswer2MU;
        psCmdMU->psConn=psConn;
      }
      else
      {
        if(psConn->SN==SN)
        { // � ������ ���������� ��������� ���������� ���������� ������ �� �����, ����������� �����������
          if(GetConnectionStateFromPtr(psConn)==ConnectionTOut) return(true);
          psCmdMU=ListMU2Pult_AddItem(NULL);
          if(!psCmdMU) return(false);
          SetConnectionBinding(psConn,SN);
          psCmdMU->SN=SN;
          psCmdMU->pFunc_PultAnswer2MU=Connection_PultAnswer2MU;
          psCmdMU->pFunc_PultNoAnswer2MU=Connection_PultNoAnswer2MU;
          psCmdMU->psConn=psConn;
          return(true);
        }
      }
    }
  }
  while(SN);
  if(IsConnectionBinding(psConn)) return(true);
  DelConnectionByPtr(psConn);
  return(false);
}

void SetConnectionMKALine2TalkIfNeed(CONNECTION_STATE_TP *psConn0)
{
  unsigned char btIndex,btState;
  CONNECTION_STATE_TP *psConn=MassConnState;

  for(btIndex=0;btIndex<MaxConnectionActive;btIndex++,psConn++)
  {
    if(psConn==psConn0) continue;
    if((psConn->wConnectionPoint==psConn0->wConnectionPoint) && (psConn->SN==psConn0->SN))
    {
      btState=GetConnectionStateFromPtr(psConn);
      if((btState==Hold) || (btState==IncomingCall))
      {
        psConn->dwState=(psConn->dwState & 0xffffff00) | Talk;
        if(!PutConnState2AllPult(psConn)) psConn->dwId=0;
      }
    }
  }
}

bool SetConnectionStateByPtr(CONNECTION_STATE_TP *psConn,unsigned char btState,unsigned char btExt)
{
/*SendStr2IpLogger("$DEBUG$SetConnectionStateByPtr: dwId=%d, State0=%d, State=%d",
                 psConn->dwId,GetConnectionStateFromPtr(psConn),btState);*/
  if(GetConnectionStateFromPtr(psConn)==btState) return(true);
  if(psConn->dwState & d_IncomingTalkState) btExt|=dIncomingTalkFlag;
  switch(btState)
  {
    case IncomingCall:
      btExt=dIncomingTalkFlag;
      break;
    case Passive:
      if(!psConn->wOperatorChannel || (psConn->wOperatorChannel==0xffff))
      { // ������� ���������� -> ��� ��������� ���������� �� �����
        psConn->dwId=0;
        return true;
      }
      break;
    case Hold:
      switch(psConn->btType)
      {
        case GroupTDC:
        case TDC:
          if(IsOtherConnectionUsedMKALine(psConn)) return(false);
          break;
      }
      break;
    case Talk:
      switch(psConn->btType)
      {
        case TDC:
          SetConnectionMKALine2TalkIfNeed(psConn);
          break;
      }
      break;
    case SelectorDisableOut:
//!!!      psConn->btLedState=dLedStateDisableOut;
      break;
  }
  psConn->dwState=btState;
  if(btExt & dIncomingTalkFlag) psConn->dwState|=d_IncomingTalkState;
  PutConnState2AllPult(psConn);
  return(true);
}

void DelConnectionByPtr(CONNECTION_STATE_TP *psConn)
{
  if(!psConn) return;
  psConn->dwId=0;
}

void DeleteAllConnections2Sinfo(void)
{
  unsigned char btIndex;
  CONNECTION_STATE_TP *psConn=MassConnState;

  for(btIndex=0;btIndex<MaxConnectionActive;btIndex++,psConn++)
  {
    if(psConn->dwId) OtboiConnection(psConn);
  }
}

void StopOpovAbonents(unsigned char SN,unsigned char btPultOk)
{
  unsigned char btIndex;
  CONNECTION_STATE_TP *psConn=MassConnState;

SendStr2IpLogger("$DEBUG$StopOpovAbonents: SN=%d",SN);
  for(btIndex=0;btIndex<MaxConnectionActive;btIndex++,psConn++)
  {
    if(psConn->dwId && (psConn->SN==SN))
    {
      if(!psConn->wOperatorChannel) StopOpov(psConn,btPultOk);
      else
        if(psConn->wOperatorChannel==0xffff) OtboiConnection(psConn);
    }
  }
}

void vCmdReadIpReinitCount(sCommand *psCmd,sInfoKadr *psInfo)
{
  unsigned char SN;

  SN=psCmd->BufCommand[1];
  if(GetAddressIpPult(SN,NULL,NULL))
  {
    vPortEnableSwitchTask(false);
/*    btLayer=GetCurrentLayer(PultNb);
    SendLayerAndLock2IpPult(PultNb,psInfo);
    SendAllKey2IpPult(SN,PultNb,btLayer,psInfo);
    SendAllActiveUnnameAbnt_ATC_ISDN(SN,PultNb,btLayer,psInfo);
    SendAllAbonentSelector2IpPult(PultNb);*/
    SendIpReinitCount2IpPult(SN,psInfo);
    vPortEnableSwitchTask(true);
  }
}

void PultNeedActiveConnections(unsigned char SN)
{
  unsigned char btInd;
  CONNECTION_STATE_TP *psConn=MassConnState;
  LIST_MU2PULT_TP *psCmdMU;

  for(btInd=0;btInd<MaxConnectionActive;btInd++,psConn++)
  {
    if(psConn->dwId && (psConn->SN==SN))
    {
SendStr2IpLogger("$DEBUG$PultNeedActiveConnections: SN=%d, dwId=%d",
                 SN,psConn->dwId);
      psCmdMU=ListMU2Pult_AddItem(NULL);
      if(!psCmdMU) return;
      psCmdMU->SN=SN;
      psCmdMU->psConn=psConn;
    }
  }
  psCmdMU=ListMU2Pult_AddItem(NULL);
  if(!psCmdMU) return;
  psCmdMU->SN=SN;
}

