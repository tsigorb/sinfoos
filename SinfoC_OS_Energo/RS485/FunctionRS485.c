#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"

/////////////////////////////////////////////////////////////////////
bool IsDigit(char chSb)
{
  if((chSb>='0') && (chSb<='9')) return(true);
  return(false);
}

unsigned char ConvertStrNumb2Byte(unsigned char *btMasNumb,unsigned char btLn)
{
  if(btLn==1)
  {
    if(IsDigit((char)btMasNumb[0])) return(btMasNumb[0]-'0');
  }
  else
  {
    if(btLn>=2)
    {
      if(IsDigit((char)btMasNumb[0]) && IsDigit((char)btMasNumb[1])) return((btMasNumb[0]-'0')*10+(btMasNumb[1]-'0'));
    }
  }
  return(0);
}

char *strclr(char *str)
{
  int i,j;
  unsigned char *str1,*str2;

  if(!str || !(*str)) return(NULL);
  i=1; str1=(unsigned char *)str;
  for(j=0;j<2;j++)
  {
    while((*str1) && (*str1<=' ')) str1+=i;
    if(!(*str1)) return(NULL);
    if(!j)
    {
      str2=str1;
      str1=(unsigned char *)(str+strlen((char *)str)-1);
      i=-1;
    }
  }
  str1++; *str1=0;
  return((char *)str2);
}

unsigned char kb_calculate_crc_8(unsigned char *addr, char count)
{
  unsigned char i,j,crc=0xFF;

  while(count--)
  {
    j=*addr++;
    for(i=0;i<8;i++)
    {
      if((crc ^ j) & 1)
      {
        crc >>= 1;
        crc ^= 0x8c;
      } 
      else crc >>= 1;
      j >>= 1;
    }
  }
  return crc;
}

/////////////////////////////////////////////////////////////////////
// ��������� ��������� ����� ����������� (�� ��� ����� ������ ����������)
unsigned short PhantomCP4PultCall(unsigned char SN_Dst,unsigned char SN_Src)
{ return(0x4000 | ((unsigned short)SN_Dst << 8) | SN_Src); }

void OtboiConnectionCommonPart(CONNECTION_STATE_TP *psConn,
                               unsigned short *pwConnectionPoint,unsigned short *pwOperatorChannel)
{
  if(pwConnectionPoint) *pwConnectionPoint=psConn->wConnectionPoint;
  if(pwOperatorChannel) *pwOperatorChannel=psConn->wOperatorChannel;
//DelAbntFromFreeHandConference(PultNb,psAbnt);
  psConn->wConnectionPoint=0;
}

void OtboiConnectionPultCall(CONNECTION_STATE_TP *psConn0)
{
  CONNECTION_STATE_TP *psConn1;

  if(psConn0->wConnectionPoint & 0x4000)
  { // ����� ���������� �� ������� ���������
    psConn1=GetConnectionStatePtrByFind(psConn0->NumbAbon,psConn0->wOperatorChannel,0xffff,PultCall,0xff,NULL);
    if(psConn1) Commute(false,psConn1->wConnectionPoint,psConn1->wOperatorChannel);
  }
  else
  { // ����� ���������� �� ������� ���������
    Commute(false,psConn0->wConnectionPoint,psConn0->wOperatorChannel);
    psConn1=GetConnectionStatePtrByFind(0,PhantomCP4PultCall(psConn0->SN,psConn0->NumbAbon),
                                        0xffff,PultCall,0xff,NULL);
  }
  if(psConn1)
  {
    SetConnectionStateByPtr(psConn1,Busy,0);
    psConn1->dwTimerEnd=d_TIMER_BUSY+xTaskGetTickCount();
  }
}

/*unsigned char CountMKALineUser(unsigned short NChan,unsigned short wOper)
{
  unsigned char btInd,btCnt;
  ABONENT_STATE_TP *psAbnt;

  btCnt=0;
  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return(btCnt);
    if((psAbnt->wConnectionPoint==NChan) && (psAbnt->wOperatorChanel==wOper)) btCnt++;
    btInd++;
  }
  while(1);
}*/

void OtboiConnectionMKA(unsigned char SN,unsigned short wNChan,unsigned char AbntNumb,unsigned char btType)
{
  unsigned char btInd,btState;
  unsigned short wOp;
  bool bInAutoConference;
  CONNECTION_STATE_TP *psConn;

  btInd=0;
  do
  {
    psConn=GetConnectionStatePtrFromIndex(btInd);
    if(!psConn) return;
    btInd++;
    if(!psConn->dwId) continue;
    if(SN && (SN!=psConn->SN)) continue;
    if(AbntNumb!=0xff)
    {
      if(psConn->NumbAbon!=AbntNumb) continue;
    }
    if((psConn->wConnectionPoint==wNChan) && (psConn->btType==btType))
    {
      if((btType==TDC) && (GetConnectionStateFromPtr(psConn)==CallTest))
      {
        SetConnectionStateByPtr(psConn,CallTestDown,0);
        psConn->dwTimerEnd=d_IndicTestCallResult+xTaskGetTickCount();
        return;
      }
      Commute(false,psConn->wConnectionPoint,psConn->wOperatorChannel);
      if(GetConnectionStateFromPtr(psConn)!=Passive) SetConnectionStateByPtr(psConn,Passive,0);
      if(SN) return;
    }
  }
  while(1);
}

void _xOtboiConnection(CONNECTION_STATE_TP *psConn)
{
  unsigned short wCP4DS,wCP4RS;
  unsigned char btState=GetConnectionStateFromPtr(psConn),
                TypeAbon=psConn->btType;

  if(psConn->wOperatorChannel && (psConn->wOperatorChannel!=0xffff))
  {
    if((TypeAbon==ISDN) || (TypeAbon==Selector) || (TypeAbon==ATC))
    {
      if(GetConnectionStateFromPtr(psConn)==Hold) StopKeep(psConn->wConnectionPoint);
    }
    if((TypeAbon==Selector) || (TypeAbon==ATC) || (TypeAbon==PultCall))
    {
      Commute(false,psConn->wConnectionPoint,psConn->wOperatorChannel);
    }
  }
/*  if((TypeAbon==ISDN) || (TypeAbon==Selector) || (TypeAbon==ATC))
  {
    DelAbonentFromSelector(PultNb,Key,btLayer);
    DelAbonentFromConference(PultNb,Key,btLayer);
  }*/
  SetConnectionStateByPtr(psConn,Passive,0);
SendStr2IpLogger("$DEBUG$_xOtboiConnection: SN=%d, wCP=%d, TypeAbon=%d",
                 psConn->SN,psConn->wConnectionPoint,TypeAbon);
  switch(TypeAbon)
  {
    case ISDN:
      SendCmdOtboiISDN(PRI_CAUSE_CALL_REJECTED,psConn->wConnectionPoint,0);
      break;
    case ATC:
      TrubaUP_Down(DOWN,psConn->wConnectionPoint);
      break;
    case PultCall:
      if(btState==PultListen)
      {
        wCP4DS=GetOperatorChannel(psConn->NumbAbon,0);
        wCP4RS=GetOperatorChannel(psConn->NumbAbon,1);
        ListenDispatcher(psConn->wOperatorChannel,wCP4DS,wCP4RS,0);
      }
      else OtboiConnectionPultCall(psConn);
      break;
    case Selector:
      break;
    case TDC:
      OtboiConnectionMKA(psConn->SN,psConn->wConnectionPoint,psConn->NumbAbon,TypeAbon);
      break;
    default:
      break;
  }
  OtboiConnectionCommonPart(psConn,NULL,NULL);
}

#define d_OpovResult_Busy     4  // ������� �����
#define d_OpovResult_Err      14 // ������ ������������ ���������� � ���������
#define d_OpovResult_Ok       22 // ���������� ������ ������� 
#define d_OpovResult_NoMess   16 // ������� ������� ������ �� ��������� ����� ������������ �����
#define d_OpovResult_NoAnsw   17 // ������� �� ��������� ������
#define d_OpovResult_ConnTOut 18 // ����-��� ����������

void _xOpov_PultNoAnsw2MU(LIST_MU2PULT_TP *pItem)
{
/* SendStr2IpLogger("$DEBUG$_xOpov_PultNoAnsw2MU: SN=%d, wCmd=%d, ConnId=%d",
                  pItem->SN,pItem->wId,pItem->psConn->dwId);*/
  _xOtboiConnection(pItem->psConn);
}

void _xOpov_PultAnsw2MU(LIST_MU2PULT_TP *pItem,sInfoKadr *psInfo)
{
/* SendStr2IpLogger("$DEBUG$_xOpov_PultAnsw2MU: SN=%d, wCmd=%d, ConnId=%d",
          pItem->SN,pItem->wId,pItem->psConn->dwId);*/
  _xOtboiConnection(pItem->psConn);
}

void StopOpov(CONNECTION_STATE_TP *psConn,unsigned char btPultOk)
{
  unsigned char btCycle=0,btState;
  LIST_MU2PULT_TP *psCmdMU;

  btState=GetConnectionStateFromPtr(psConn);
  switch(btState)
  {
    case Talk:
      if(psConn->btOpovRec==2) _xAbntRecord(psConn,0); // ��������� ������ ���������� ��������
      btCycle=StopAudioMessage(psConn->dwId);
      if(psConn->btACycle>btCycle) psConn->btOpovState=d_OpovResult_Ok;
      else psConn->btOpovState=d_OpovResult_NoMess;
      break;
    case Call:
      psConn->btOpovState=d_OpovResult_Err;
      break;
    case CallWaitAnswer:
      psConn->btOpovState=d_OpovResult_NoAnsw;
      break;
    case ConnectionTOut:
      psConn->btOpovState=d_OpovResult_ConnTOut;
      break;
    case Busy:
    default:
      psConn->btOpovState=d_OpovResult_Busy;
      break;
  }
  if(btPultOk)
  {
    ClearAllConnectionBinding(psConn);
    psCmdMU=ListMU2Pult_AddItem(NULL);
    if(!psCmdMU) return;
    SetConnectionBinding(psConn,psConn->SN);
/*SendStr2IpLogger("$DEBUG$StopOpov: wCmd=%d, wConnId=%d,btState=%d, OpovState=%d, btCycle=%d",
                 psCmdMU->wId,psConn->dwId,btState,psConn->btOpovState,btCycle);*/
    psCmdMU->SN=psConn->SN;
    psCmdMU->pFunc_PultAnswer2MU=_xOpov_PultAnsw2MU;
    psCmdMU->pFunc_PultNoAnswer2MU=_xOpov_PultNoAnsw2MU;
    psCmdMU->psConn=psConn;
    psConn->dwTimerEnd=0;
  }
  else _xOtboiConnection(psConn);
}

void OtboiConnection(CONNECTION_STATE_TP *psConn)
{
  if(!psConn->wOperatorChannel && ((psConn->btType==ISDN) || (psConn->btType==Selector)))
  { // ������� ����������
    StopOpov(psConn,true);
  }
  else _xOtboiConnection(psConn);
}

/////////////////////////////////////////////////////////////////////
// �������� ������ �� ������� Command
bool WaitReply(unsigned char Command, unsigned char Subcommand)
{
  int iRepeat;
  bool Result;
  static sInfoKadr sReply;

  iRepeat=0;
  Result=false;
  do
  {
    if(xQueueReceive(xQueueReply[UserRS485],&sReply,250))
    { // ������ �� ������� �������
      if(sReply.ByteInfoKadr[0]==Command)
      {
        if(Command==11)
        {
          if(Subcommand==sReply.ByteInfoKadr[1]) 
          {
            switch(Subcommand)
            {
              case 123: // ������ �������� ����������
                if(!sReply.ByteInfoKadr[2]) return(true);
                CountErrCommute++;
                return(false);
              case 122: // ��������� ������� ����������
                if(sReply.ByteInfoKadr[2]==NumbTabl) return(true);
                CountErrCommute++;
                return(false);
              default:
                if(sReply.ByteInfoKadr[2]==1) return(true);
                CountErrCommute++;
                return(false);
            }
          }
          else
          { // ����� �� ������ �������
            CountErrNoCommand++;
/*DataSave1[7]=CountErrNoCommand;
DataSave1[8]=sReply.ByteInfoKadr[1];
DataSave1[9]=Subcommand;*/
          }
        }
        else
        {
          if(sReply.ByteInfoKadr[1]==CodeStatusTrue) return(true);
          CountErrCommute++;
          return(false);
        }
      }
      else
      { // ����� �� ������ �������
        CountErrNoCommand++;
/*DataSave1[7]=CountErrNoCommand;
DataSave1[8]=sReply.ByteInfoKadr[0];
DataSave1[9]=Command;*/
      }
    }
    else
    { // ��� ������� � �������
      iRepeat++;
      if(iRepeat>=2)
      {
        CountNoReply++;
//DataSave1[4]=CountNoReply;
//DataSave1[5]=Command;
//if(Command==11) DataSave1[6]=Subcommand;
        return(false);
      }
    }
  }
  while(1);
}

// ��������� ���������� ������� NChan1 < NChan2
void Commute(bool Code,unsigned short NChan1, unsigned short NChan2)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485; // �������� ������� RS485
  sCmd.Length=6; // ���������� ���� � �������
  sCmd.A1=0;
  sCmd.BufCommand[0]=10; // �������
  if(Code) sCmd.BufCommand[1]=129; // ���������� ��������� ����������
  else sCmd.BufCommand[1]=130; // ���������� �������������
  sCmd.BufCommand[2]=NChan1 >> 8; // ��� ������
  sCmd.BufCommand[3]=NChan1; // N ������
  sCmd.BufCommand[4]=NChan2 >> 8; // ��� ������
  sCmd.BufCommand[5]=NChan2; // �����
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
}

void DelConnectionPoint(unsigned short wCP)
{
  if((wCP >> 8)==0x1)
  {
    static sCommand sCmd;
    sCmd.Source=UserRS485; // �������� ������� RS485
    sCmd.Length=4; // ���������� ���� � �������
    sCmd.A1=0;
    sCmd.BufCommand[0]=10; // �������
    sCmd.BufCommand[1]=142; // ����������
    sCmd.BufCommand[2]=wCP >> 8;
    sCmd.BufCommand[3]=wCP & 0xff;
    if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  }
}

// ����� ��������� ����� �����������
unsigned short GetFreeConnectionPoint(void)
{
  unsigned short wCP=0;
  static sCommand sCmd;
  static sInfoKadr sReply;

  sCmd.Source=UserRS485; // �������� ������� RS485
  sCmd.Length=2; // ���������� ���� � �������
  sCmd.A1=0;
  sCmd.BufCommand[0]=10; // �������
  sCmd.BufCommand[1]=141; // ����������
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(xQueueReceive(xQueueReply[UserRS485],&sReply,250))
    {
      if((sReply.ByteInfoKadr[0]==11) && (sReply.ByteInfoKadr[1]==141))
      {
        wCP=sReply.ByteInfoKadr[2];
        wCP=(wCP <<8) | sReply.ByteInfoKadr[3];
        if(wCP==0xffff) wCP=0;
      }
    }
  }
  return(wCP);
}

// �����/�������� ������ �� ������ NChan
bool TrubaUP_Down(unsigned char UP_Down, unsigned short NChan)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485;
  sCmd.Length=4;
  sCmd.A1=0;
  sCmd.BufCommand[0]=31;
  sCmd.BufCommand[1]=0;
  sCmd.BufCommand[2]=NChan-1;
  sCmd.BufCommand[3]=UP_Down;
  if(xQueueSendCmd(&sCmd,30)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else 
  {
    if(WaitReply(31,0)) return(true);
  }
  return(false);
}

unsigned int GetTypeCall4ChanelMKA(unsigned short NChan)
{
  unsigned int dwHi;

  NChan--; NChan<<=2;
  dwHi=RdFlash(AdrTypeCall+2+NChan);
  return((dwHi << 16) | RdFlash(AdrTypeCall+NChan));
}

unsigned char CmdEnDisOut(unsigned short wCP,unsigned char btEnOut)
{
  static sCommand sCmd;
  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.Length=4; // ���������� ���� � �������
  sCmd.BufCommand[0]=38; // ������� 38 - ���/���� ������ (������ ���������� ������� � ����� ����������� � 0!!!)
  sCmd.BufCommand[1]=wCP >> 8;
  sCmd.BufCommand[2]=wCP-1;
  if(btEnOut) sCmd.BufCommand[3]=1;
  else sCmd.BufCommand[3]=0;
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL)
  { CountErrSendQueueCommand++; return(0); }
  return(1);
}

bool HoldOnOff(CONNECTION_STATE_TP *psConn,unsigned char btMode)
{
  switch(btMode)
  {
    case 0:
      Commute(false,psConn->wOperatorChannel,psConn->wConnectionPoint);
      if((psConn->btType==ISDN) || (psConn->btType==ATC) || (psConn->btType==Selector)) SoundKeep(psConn->wConnectionPoint);
      return(SetConnectionStateByPtr(psConn,Hold,0));
    case 1:
      if((psConn->btType==ISDN) || (psConn->btType==ATC) || (psConn->btType==Selector)) StopKeep(psConn->wConnectionPoint);
      if(!IsCommute(psConn->wOperatorChannel,psConn->wConnectionPoint))
      {
        Commute(true,psConn->wOperatorChannel,psConn->wConnectionPoint);
        Commute(true,psConn->wConnectionPoint,psConn->wOperatorChannel);
      }
      SetConnectionStateByPtr(psConn,Talk,0);
      break;
    case 2:
      if(psConn->btType==TDC)
      {
        if(!IsCommute(psConn->wOperatorChannel,psConn->wConnectionPoint))
        {
          Commute(true,psConn->wOperatorChannel,psConn->wConnectionPoint);
          Commute(true,psConn->wConnectionPoint,psConn->wOperatorChannel);
        }
        SetConnectionStateByPtr(psConn,Talk,0);
        psConn->dwTimerEnd=0;
      }
      break;
  }
  return(true);
}

