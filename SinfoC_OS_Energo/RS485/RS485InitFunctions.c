#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"

//*******************************************************************
void TestLic(char *strSignature);
void ReadDialplan(void);

//*******************************************************************
static volatile bool s_bPultCfgRead=false;
static volatile bool s_bNeedInitPults=false;

/////////////////////////////////////////////////////////////////////
void InitMassTimer(void)
{
  unsigned char i;
  HANDLE hFile;
  static unsigned int MassTimer0[NumberTimer]=
{ 0x1e,0x1e,0xb4,0x0708,0x0d,0x0258,0x0f,0x0a,0x03,0x0708,0x14,0x0a,0x05 };
 
  memcpy(MassTimer,MassTimer0,sizeof(MassTimer));
  hFile=CreateFile("MTimer1.dts",GENERIC_READ,0);
  if(hFile>=0)
  {
    ReadFile(hFile,MassTimer,sizeof(MassTimer));
    CloseHandle(hFile);
  }
  for(i=0;i<(sizeof(MassTimer)/sizeof(int));i++) MassTimer[i]*=1000;
}

/////////////////////////////////////////////////////////////////////
void SetNeedInitPults(void)
{ s_bNeedInitPults=true; }

bool IsNeedInitPults(void)
{ return(s_bNeedInitPults); }

void InitPults(bool bWait)
{
  unsigned char i;

  LedOn(ledPult);
  if(bWait)
  { // ����� ������ ������ ������������� �������
/*    vPortEnableSwitchTask(false);
    InitTransitVars();
    vPortEnableSwitchTask(true);*/
  }
  s_bNeedInitPults=false;
  s_bPultCfgRead=false;
  CrossCommuteOtboiAll();
//  DelAllFreeHandConf();
//  DeleteAllTransit();
  DeleteAllConnections2Sinfo();
  vTaskDelay(3000);
  vPortEnableSwitchTask(false);
  InitPultRepeatCmd();
  ReadDialplan();
  ResetIPPults();
  InitMassTimer(); // ������������� ��������
  TestLic("POS.lic");
  TestLic("REC.lic");
  s_bPultCfgRead=true;
  vPortEnableSwitchTask(true);
  ReinitConnectionPoint(); // ������ ���������� ������ ��� ����������� ������������ �����!!!
//  RestoreAllTransit();
  LedOff(ledPult);
}

