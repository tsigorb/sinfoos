#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"

///////////////////////////////////////////////////////////////////////////////
void FreeConnection4PultEvent(LIST_MU2PULT_TP *pItem)
{
  CONNECTION_STATE_TP *psConn=pItem->psConn;
  ClrConnectionBinding(psConn,pItem->SN);
  if(IsConnectionBinding(psConn)) return;
  DelConnectionByPtr(psConn);
}

///////////////////////////////////////////////////////////////////////////////
void Passive_PultAnswer2MU(LIST_MU2PULT_TP *pItem,sInfoKadr *psInfo)
{
  FreeConnection4PultEvent(pItem);
}

void Passive_PultNoAnswer2MU(LIST_MU2PULT_TP *pItem)
{
  FreeConnection4PultEvent(pItem);
}

///////////////////////////////////////////////////////////////////////////////
void IncomingCall_PultNoAnswer2MU(LIST_MU2PULT_TP *pItem)
{
  FreeConnection4PultEvent(pItem);
}

void IncomingCall_PultAnswer2MU(LIST_MU2PULT_TP *pItem,sInfoKadr *psInfo)
{
  CONNECTION_STATE_TP *psConn=pItem->psConn;

  if(psInfo->ByteInfoKadr[10])
  { // ����� �� ������������ ���� �����
    IncomingCall_PultNoAnswer2MU(pItem);
  }
}

///////////////////////////////////////////////////////////////////////////////
void Busy_PultNoAnswer2MU(LIST_MU2PULT_TP *pItem)
{
}

void Busy_PultAnswer2MU(LIST_MU2PULT_TP *pItem,sInfoKadr *psInfo)
{
}
      
///////////////////////////////////////////////////////////////////////////////
void Talk_PultNoAnswer2MU(LIST_MU2PULT_TP *pItem)
{
  CONNECTION_STATE_TP *psConn=pItem->psConn;

  ClrConnectionBinding(psConn,pItem->SN);
  if(IsConnectionBinding(psConn)) return;
  // ���� ����� ��������� ==0, �� ����� ���������� �� ����������, �.�. ���� ���������� ��������
  if(psConn->wOperatorChannel) OtboiConnection(psConn);
}

void Talk_PultAnswer2MU(LIST_MU2PULT_TP *pItem,sInfoKadr *psInfo)
{
  CONNECTION_STATE_TP *psConn=pItem->psConn;

  if(psConn->SN!=pItem->SN) ClrConnectionBinding(psConn,pItem->SN);
  else
  {
    if(psConn->wOperatorChannel) psConn->dwTimerEnd=0;
  }
  if(IsConnectionBinding(psConn)) return;
  OtboiConnection(psConn);
}

///////////////////////////////////////////////////////////////////////////////
/*void CallTest_PultAnswer2MU(LIST_MU2PULT_TP *pItem,sInfoKadr *psInfo)
{
  CONNECTION_STATE_TP *psConn=pItem->psConn;

  if(psConn->SN!=pItem->SN) ClrConnectionBinding(psConn,pItem->SN);
  if(IsConnectionBinding(psConn)) return;
  OtboiConnection(psConn);
}
*/

