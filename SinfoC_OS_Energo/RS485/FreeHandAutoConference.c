#include "Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

///////////////////////////////////////////////////////////////////////////////
FreeHandConfTP MassFreeHandConf[NumberKeyboard];

///////////////////////////////////////////////////////////////////////////////
void InitFreeHandConf(void)
{
  unsigned char i;

  for(i=0;i<NumberKeyboard;i++)
  {
    MassFreeHandConf[i].btExistConf=0;
    memset(MassFreeHandConf[i].MassAbntInd,0xff,d_MaxAbnt2FreeHandConf);
  }
}

void DelFreeHandConf(unsigned char PultNb)
{
  DelCommuteConference(PultNb,d_FreeHandConferenceNumb);
  memset(MassFreeHandConf[PultNb].MassAbntInd,0xff,d_MaxAbnt2FreeHandConf);
  MassFreeHandConf[PultNb].btExistConf=0;
}

void DelAllFreeHandConf(void)
{
  unsigned char i;

  for(i=0;i<NumberKeyboard;i++)
  {
    if(MassFreeHandConf[i].btExistConf) DelFreeHandConf(i);
  }
}

unsigned char CountEqualCP2FreeHandConference(unsigned char PultNb,unsigned short wCP)
{
  unsigned char i,btInd,btCntCP=0;
  ABONENT_STATE_TP *psAbnt;

  for(i=0;i<d_MaxAbnt2FreeHandConf;i++)
  {
    btInd=MassFreeHandConf[PultNb].MassAbntInd[i];
    if(btInd!=0xff)
    {
      psAbnt=GetAbonentStatePtrFromIndex(btInd);
      if(psAbnt->wConnectionPoint==wCP) btCntCP++;
    }
  }
  return(btCntCP);
}

bool DelAbntFromFreeHandConference(unsigned char PultNb,ABONENT_STATE_TP *psAbnt)
{
  unsigned char i,btInd=GetAbonentStateIndexFromPtr(psAbnt);
  bool bRet=false;

  if(MassFreeHandConf[PultNb].btExistConf && (btInd!=0xff))
  {
    for(i=0;i<d_MaxAbnt2FreeHandConf;i++)
    {
      if(MassFreeHandConf[PultNb].MassAbntInd[i]==btInd)
      {
        if(CountEqualCP2FreeHandConference(PultNb,psAbnt->wConnectionPoint)==1)
        {
          AbonentConferenceCommuteAddDell(PultNb,d_FreeHandConferenceNumb,
                                          psAbnt->wConnectionPoint,false);
          bRet=true;
        }
        MassFreeHandConf[PultNb].MassAbntInd[i]=0xff;
        break;
      }
    }
    for(i=0;i<d_MaxAbnt2FreeHandConf;i++)
    {
      if(MassFreeHandConf[PultNb].MassAbntInd[i]!=0xff)
      {
        SendAllAbonentConference2IpPult(PultNb,d_FreeHandConferenceNumb);
        return(bRet);
      }
    }
    DelFreeHandConf(PultNb);
  }
  return(bRet);
}

bool AddAbnt2FreeHandConf(unsigned char PultNb,ABONENT_STATE_TP *psAbnt)
{
  unsigned char i,btInd=GetAbonentStateIndexFromPtr(psAbnt);

  if(btInd==0xff) return(false);
  if(MassFreeHandConf[PultNb].btExistConf)
  {
    for(i=0;i<d_MaxAbnt2FreeHandConf;i++)
    {
      if(MassFreeHandConf[PultNb].MassAbntInd[i]==0xff)
      {
        if(CountEqualCP2FreeHandConference(PultNb,psAbnt->wConnectionPoint)==0)
        {
          if(!AbonentConferenceCommuteAddDell(PultNb,d_FreeHandConferenceNumb,
                                              psAbnt->wConnectionPoint,true)) return(false);
        }
        MassFreeHandConf[PultNb].MassAbntInd[i]=btInd;
        SendAllAbonentConference2IpPult(PultNb,d_FreeHandConferenceNumb);
        return(true);
      }
    }
  }
  else
  {
    if(CreateCommuteConference(PultNb,d_FreeHandConferenceNumb,psAbnt->wOperatorChanel,
                               d_MaxAbnt2FreeHandConf,MassFreeHandConf[PultNb].MassAbntInd))
    {
      if(AbonentConferenceCommuteAddDell(PultNb,d_FreeHandConferenceNumb,
                                         psAbnt->wConnectionPoint,true))
      {
        MassFreeHandConf[PultNb].MassAbntInd[0]=btInd;
        MassFreeHandConf[PultNb].btExistConf=1;
        SendAllAbonentConference2IpPult(PultNb,d_FreeHandConferenceNumb);
        return(true);
      }
    }
  }
  return(false);
}

