#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"

bool _xOutgoingCallDTMF(unsigned short NChan,unsigned char *btBufNumb,unsigned char btLn)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.BufCommand[0]=1;
  sCmd.BufCommand[1]=NChan >> 8;
  sCmd.BufCommand[2]=NChan;
  sCmd.BufCommand[3]=CallDTMF;
  sCmd.BufCommand[4]=btLn;
  memcpy(sCmd.BufCommand+5,btBufNumb,btLn);
  sCmd.Length=btLn+4;
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else 
  {
    if(WaitReply(1,0)) return(true);
  }
  return(false);
}

bool _xOutgoingNumberATC(unsigned char NChan,char *pchNumb,unsigned char btTypeNabor)
{ // ����� ������ ��������
  unsigned char i,btDg;
  unsigned int dwTypeCall;
  static sCommand sCmd;

  if(btTypeNabor==1) dwTypeCall=0; // Pulse
  else
  {
    if(btTypeNabor==2) dwTypeCall=0x20; // Dtmf
    else
    { // Channel setting
      dwTypeCall=GetTypeCall4ChanelMKA(NChan);
      dwTypeCall&=0x20;
    }
  }
  sCmd.Source=UserRS485;
  sCmd.Length=21;
  sCmd.A1=0;
  if(dwTypeCall) sCmd.BufCommand[0]=6;  // DTMF
  else sCmd.BufCommand[0]=5; // PULSE
  sCmd.BufCommand[1]=0;
  sCmd.BufCommand[2]=NChan-1;
  memset(sCmd.BufCommand+3,0xff,18);
  i=0;
  while(btDg=pchNumb[i])
  {
    btDg-='0';
    if(!dwTypeCall && (btDg==0)) sCmd.BufCommand[3+i]=10; // ��� ���. ������ 0 = 10
    else sCmd.BufCommand[3+i]=btDg; // ����� ��� �����
    i++;
  }
  if(xQueueSendCmd(&sCmd,0)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(WaitReply(sCmd.BufCommand[0],0)) return(true);
  }
  return(false);
}

#define dPorogBusy      0x100
#define dTimeLevel      20

void ControlATCLevelTOut(void)
{ // ���������� ������� ������ ������� ��� �������� ���(������������ ��� ��������� ������� "������")
  unsigned char btInd,btOff;
  unsigned short wLvl;
  CONNECTION_STATE_TP *psConn;
  static sCommand sCmd;
  static sInfoKadr sReply;

  sCmd.Source=UserRS485;
  sCmd.Length=2;
  sCmd.BufCommand[0]=10;  //-+ - ������ ������� ������� �� ������� � ��
  sCmd.BufCommand[1]=154; //-+
  while(xQueueReceive(xQueueReply[UserRS485],&sReply,1));
  if(xQueueSend(xQueueSportX,&sCmd,10)==errQUEUE_FULL) return;
  else
  {
    if(xQueueReceive(xQueueReply[UserRS485],&sReply,250))
    { // ������ �� ������� �������
      if((sReply.ByteInfoKadr[0]!=11) || (sReply.ByteInfoKadr[1]!=154)) return;
    }
  }
  btInd=0;
  do
  {
    psConn=GetConnectionStatePtrFromIndex(btInd);
    if(!psConn) return;
    btInd++;
    if(psConn->dwId && psConn->wConnectionPoint && (psConn->btType==ATC))
    {
      if(GetConnectionStateFromPtr(psConn)==Talk)
      {
        btOff=(psConn->wConnectionPoint-1)*2+2;
        wLvl=sReply.ByteInfoKadr[btOff];
        wLvl=(wLvl<<8) | sReply.ByteInfoKadr[btOff+1];
        if(wLvl>=dPorogBusy) psConn->btLvlTOut=dTimeLevel;
        else
        {
          if(psConn->btLvlTOut) psConn->btLvlTOut--;
        }
      }
      else psConn->btLvlTOut=dTimeLevel;
    }
  }
  while(1);
}

void SignalATC_Ready(unsigned short wNChan)
{
  CONNECTION_STATE_TP *psConn;

  psConn=GetConnectionStatePtrByFind(0,wNChan,0xffff,ATC,0xff,NULL);
  if(!psConn)
  {
    TrubaUP_Down(DOWN,wNChan);
    Commute(false,wNChan,0);
    return;
  }
  if(GetConnectionStateFromPtr(psConn)==Call)
  { // ����������� ��������� ���������� ������
    if(!_xOutgoingNumberATC(wNChan,psConn->NumbDst,psConn->btTypeNabor))
    {
      SetConnectionStateByPtr(psConn,CallError,0);
      psConn->dwTimerEnd=d_TIMER_BUSY+xTaskGetTickCount();
    }
    else
    {
      SetConnectionStateByPtr(psConn,Talk,0);
      psConn->dwTimerEnd=0;
    }
  }
}

void SignalATC_Busy(unsigned short wNChan)
{
  CONNECTION_STATE_TP *psConn;

  psConn=GetConnectionStatePtrByFind(0,wNChan,0xffff,ATC,0xff,NULL);
  if(psConn)
  {
    if(GetConnectionStateFromPtr(psConn)!=Busy)
    {
      SetConnectionStateByPtr(psConn,Busy,0);
      psConn->dwTimerEnd=d_TIMER_BUSY+xTaskGetTickCount();
//!!!???    if(psConn->btLvlTOut) OtboiConnection(psConn);
    }
  }
  else
  {
    TrubaUP_Down(DOWN,wNChan);
    Commute(false,wNChan,0);
    return;
  }
}

void IncomingCallATC(unsigned short wNChan)
{
  CONNECTION_STATE_TP *psConn;
  
  psConn=GetConnectionStatePtrByFind(0,wNChan,0xffff,ATC,0xff,NULL);
  if(psConn)
  {
    psConn->dwTimerEnd=MassTimer[TIMER_INCOMINGCALL]+xTaskGetTickCount();
    return;
  }
  psConn=GetNewConnection();
  if(!psConn) return;
  psConn->SN=0xff;
  psConn->wConnectionPoint=wNChan;
  psConn->wOperatorChannel=0;
  psConn->btType=ATC;
  SetConnectionStateByPtr(psConn,IncomingCall,0);
  psConn->dwTimerEnd=MassTimer[TIMER_INCOMINGCALL]+xTaskGetTickCount();
}

