#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"

///////////////////////////////////////////////////////////////////////////////
void *pvPortMalloc(size_t xSize);
void vPortFree(void *pv);

///////////////////////////////////////////////////////////////////////////////
static AUDIO_MESSAGE_TP s_MassAMessage[NumberKeyboard];

AUDIO_MESSAGE_LIST_TP *g_psAMessList=NULL;

///////////////////////////////////////////////////////////////////////////////
void InitAudioMessages(void)
{
  unsigned char btId;

  for(btId=0;btId<NumberKeyboard;btId++)
  {
    s_MassAMessage[btId].pbtAudio=0;
    s_MassAMessage[btId].SN=0xff;
  }
}

unsigned char CreateAudioMessage(unsigned char SN,unsigned int dwCnt)
{
  unsigned char btId;

  for(btId=0;btId<NumberKeyboard;btId++)
  {
    if(s_MassAMessage[btId].SN==SN) return(0xff);
  }
  for(btId=0;btId<NumberKeyboard;btId++)
  {
    if(s_MassAMessage[btId].SN==0xff)
    {
      s_MassAMessage[btId].pbtAudio=pvPortMalloc(dwCnt*AUDIO_PACKET_SIZE);
      if(!s_MassAMessage[btId].pbtAudio) return(0xff);
      s_MassAMessage[btId].SN=SN;
      s_MassAMessage[btId].wABlockCnt=dwCnt;
      memset(s_MassAMessage[btId].pbtAudio,0xd5,dwCnt*AUDIO_PACKET_SIZE);
//SendStr2IpLogger("$DEBUG$CreateAudioMessage: SN=%d, dwCnt=%d",SN,dwCnt);
      return(btId+1);
    }
  }
  return(0xff);
}

void DeleteAudioMessages4Pult(unsigned char SN)
{
  unsigned char btId;

  for(btId=0;btId<NumberKeyboard;btId++)
  {
    if(s_MassAMessage[btId].SN==SN)
    {
      unsigned int dwIRQ=cli();
      if(s_MassAMessage[btId].pbtAudio) vPortFree(s_MassAMessage[btId].pbtAudio);
      s_MassAMessage[btId].pbtAudio=0;
      s_MassAMessage[btId].SN=0xff;
      sti(dwIRQ);
    }
  }
}

void DeleteAudioMessage(unsigned char SN)
{ DeleteAudioMessages4Pult(SN); }

unsigned char _xRecieveAudioPacket(unsigned char SN,unsigned int dwPckt,unsigned char *pbtAudio)
{
  unsigned char btId;

  for(btId=0;btId<NumberKeyboard;btId++)
  {
    if((s_MassAMessage[btId].SN==SN) && s_MassAMessage[btId].pbtAudio && (dwPckt<s_MassAMessage[btId].wABlockCnt))
    {
      memcpy(s_MassAMessage[btId].pbtAudio+dwPckt*AUDIO_PACKET_SIZE,pbtAudio,AUDIO_PACKET_SIZE);
//SendStr2IpLogger("$DEBUG$_xRecieveAudioPacket: SN=%d, dwPckt=%d",SN,dwPckt);
      return(0);
    }
  }
  return(d_CommonErr);
}

unsigned int GetAudioTime(unsigned char SN)
{
  unsigned char btId;

  for(btId=0;btId<NumberKeyboard;btId++)
  {
    if((s_MassAMessage[btId].SN==SN) && s_MassAMessage[btId].pbtAudio)
    { return((unsigned int)s_MassAMessage[btId].wABlockCnt*(AUDIO_PACKET_SIZE*125/1000)); }
  }
  return(0);
}

bool StartAudioMessage(CONNECTION_STATE_TP *psConn)
{
  unsigned char btId;
  unsigned short wCP;
  AUDIO_MESSAGE_LIST_TP *psAMess;
  for(btId=0;btId<NumberKeyboard;btId++)
  {
    if((s_MassAMessage[btId].SN==psConn->SN) && s_MassAMessage[btId].pbtAudio) break;
  }
  if(btId>=NumberKeyboard) return false;
  psAMess=pvPortMalloc(sizeof(AUDIO_MESSAGE_LIST_TP));
  if(psAMess)
  {
    psAMess->dwId=psConn->dwId;
    psAMess->btCycle=psConn->btACycle-1;
    wCP=psConn->wConnectionPoint;
    if(wCP>0xFF) wCP=(wCP & 0xFF)+LenHard+LenVirt-1; 
    else wCP=wCP+LenSport1-1;
    psAMess->wNChan=wCP;
    psAMess->dwACnt=(unsigned int)s_MassAMessage[btId].wABlockCnt*AUDIO_PACKET_SIZE;
    psAMess->dwACur=0;
    psAMess->pbtAudio=s_MassAMessage[btId].pbtAudio;
    vPortEnableSwitchTask(false);
    psAMess->psNext=g_psAMessList;
    g_psAMessList=psAMess;
    vPortEnableSwitchTask(true);
/*SendStr2IpLogger("$DEBUG$StartAudioMessage: dwConnId=%d, wCP0=%d, wCP1=%d, btCycle=%d",
          psConn->dwId,psConn->wConnectionPoint,wCP,psConn->btACycle);*/
    return(true);
  }
  return(false);
}

unsigned char StopAudioMessage(unsigned int dwId)
{
  unsigned char btCycle;
  AUDIO_MESSAGE_LIST_TP *psAMess=g_psAMessList,
                        *psPrev=NULL,*psNext;

  while(psAMess)
  {
    psNext=psAMess->psNext;
    if(psAMess->dwId==dwId)
    {
      unsigned int dwIRQ=cli();
      btCycle=psAMess->btCycle;
      if(psAMess==g_psAMessList) g_psAMessList=psNext;
      else
      {
        if(psPrev) psPrev->psNext=psNext;
      }
      sti(dwIRQ);
      vPortFree(psAMess);
      return(btCycle+1);
    }
    else psPrev=psAMess;
    psAMess=psNext;
  }
  return(0);
}

