#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"

///////////////////////////////////////////////////////////////////////////////
void *pvPortMalloc(size_t xSize);
void vPortFree(void *pv);

unsigned char GetCodeFromOperatorChannel(unsigned short wOp);

///////////////////////////////////////////////////////////////////////////////
static LIST_MU2PULT_TP *s_pListMU2Pult=NULL,
                       *s_pItemCur=NULL;

static unsigned short s_wListMU2PultId=0;

///////////////////////////////////////////////////////////////////////////////
LIST_MU2PULT_TP *ListMU2Pult_AddItem(LIST_MU2PULT_TP *pItem0)
{
  LIST_MU2PULT_TP *pItemCur;
  LIST_MU2PULT_TP *pItem=(LIST_MU2PULT_TP *)pvPortMalloc(sizeof(LIST_MU2PULT_TP));
  if(!pItem) return(NULL);
  if(pItem0) memcpy(pItem,pItem0,sizeof(LIST_MU2PULT_TP));
  else memset(pItem,0,sizeof(LIST_MU2PULT_TP));
  pItem->wId=s_wListMU2PultId++;
  pItem->next=NULL;
SendStr2IpLogger("$DEBUG$ListMU2Pult_AddItem: wCmdId=%d",pItem->wId);
  if(s_pListMU2Pult)
  {
    pItemCur=s_pListMU2Pult;
    while(pItemCur->next) pItemCur=pItemCur->next;
    pItemCur->next=pItem;
    pItem->prev=pItemCur;
  }
  else
  {
    pItem->prev=NULL;
    s_pListMU2Pult=pItem;
  }
  return(pItem);
}

void ListMU2Pult_DeleteItem(LIST_MU2PULT_TP *pItem0)
{
  if(!pItem0) return;
  if(pItem0==s_pItemCur) s_pItemCur=pItem0->next;
  if(pItem0==s_pListMU2Pult)
  {
    s_pListMU2Pult=pItem0->next;
    if(s_pListMU2Pult) s_pListMU2Pult->prev=NULL;
  }
  else
  {
    LIST_MU2PULT_TP *pItemPrev,*pItemNext;

    pItemPrev=pItem0->prev;
    pItemNext=pItem0->next;
    pItemPrev->next=pItemNext;
    if(pItemNext) pItemNext->prev=pItemPrev;
  }
  vPortFree(pItem0);
}

LIST_MU2PULT_TP *ListMU2Pult_FindItem(unsigned short wId)
{
  LIST_MU2PULT_TP *pItem;

  pItem=s_pListMU2Pult;
  while(pItem)
  {
    if(pItem->wId==wId) return(pItem);
    pItem=pItem->next;
  }
  return(NULL);
}

void CreateConnectionStateCmd(CONNECTION_STATE_TP *psConn,sInfoKadr *psIK)
{
  unsigned char btState=GetConnectionStateFromPtr(psConn);

  psIK->ByteInfoKadr[6]=psConn->dwId & 0xff;
  psIK->ByteInfoKadr[7]=(psConn->dwId >> 8) & 0xff;
  psIK->ByteInfoKadr[8]=(psConn->dwId >> 16) & 0xff;
  psIK->ByteInfoKadr[9]=(psConn->dwId >> 24) & 0xff;
/* SendStr2IpLogger("$DEBUG$CreateConnectionStateCmd: ConnId=%d, wOp=%d, OpovState=%d, btState=%d",
                 psConn->dwId,psConn->wOperatorChannel,psConn->btOpovState,btState);*/
  if(psConn->wOperatorChannel || (!psConn->btOpovState))
  {
    CONNECTION_INFO4IP_TP *psConnInfo=(CONNECTION_INFO4IP_TP *)(psIK->ByteInfoKadr+10);
    psIK->ByteInfoKadr[5]=7; // ������� 7
    psConnInfo->btState=btState;
    psConnInfo->SN=psConn->SN;
    psConnInfo->CallType=psConn->btType;
    if(psConn->wConnectionPoint & 0xff00)
    { psConnInfo->w_Chan_OpId=((psConn->wConnectionPoint & 0xff)+100) << 8; }
    else psConnInfo->w_Chan_OpId=psConn->wConnectionPoint << 8;
    psConnInfo->w_Chan_OpId|=GetCodeFromOperatorChannel(psConn->wOperatorChannel);
    memcpy(psConnInfo->NumbSrc,psConn->NumbSrc,MAX_DIGIT_TLFN+1);
    memcpy(psConnInfo->NameSrc,psConn->NameSrc,d_NameLen+1);
    memcpy(psConnInfo->NumbDst,psConn->NumbDst,MAX_DIGIT_TLFN+1);
    switch(psConnInfo->CallType)
    {
      case TDC:
      case PultCall:
        LockPrintf();
        if(psConn->dwState & d_IncomingTalkState) sprintf(psConnInfo->NumbSrc,"%d",psConn->NumbAbon);
        else sprintf(psConnInfo->NumbDst,"%d",psConn->NumbAbon);
        UnlockPrintf();
        break;
      default:
        break;
    }
    psIK->L=10+sizeof(CONNECTION_INFO4IP_TP);
  }
  else
  {
    psIK->ByteInfoKadr[5]=12; // ������� 12
    psIK->ByteInfoKadr[10]=psConn->btOpovState;
    psIK->L=11;
  }
}

void CreateEmptyConnectionCmd(sInfoKadr *psIK)
{ // 
  psIK->ByteInfoKadr[5]=7; // ������� 7
  psIK->ByteInfoKadr[6]=0; // dwId=0
  psIK->ByteInfoKadr[7]=0;
  psIK->ByteInfoKadr[8]=0;
  psIK->ByteInfoKadr[9]=0;
  psIK->L=10;
}

bool SendItem2Pult(LIST_MU2PULT_TP *pItem)
{
  CONNECTION_STATE_TP *psConn;
  static sInfoKadr sIK;

  pItem->repeat_cnt++;
  psConn=pItem->psConn;
  if(GetAddressIpPult(pItem->SN,&sIK.A1,&sIK.PORT1))
  {
    sIK.Source=UserRS485;
    sIK.ByteInfoKadr[0]=0xfd;
    sIK.ByteInfoKadr[1]=0;
    sIK.ByteInfoKadr[2]=pItem->SN;
    sIK.ByteInfoKadr[3]=pItem->wId & 0xff;
    sIK.ByteInfoKadr[4]=pItem->wId >> 8;
    if(psConn) CreateConnectionStateCmd(psConn,&sIK);
    else CreateEmptyConnectionCmd(&sIK);
SendStr2IpLogger("$DEBUG$SendItem2Pult: SN=%d, cmdId=%d, repeat_cnt=%d, ConnId=%d",
                 pItem->SN,pItem->wId,pItem->repeat_cnt,psConn ? psConn->dwId : 0);
    LockPrintf();
    sprintf((char*)(sIK.ByteInfoKadr+sIK.L),"<UK Cmd%02d>",sIK.ByteInfoKadr[5]);
    UnlockPrintf();
    sIK.L+=10;
    xQueueSend(xQueueReply[UserUDP],&sIK,10);
    pItem->dwTOut=xTaskGetTickCount()+d_Pult2MUIpTOut;
    return true;
  }
  return false;
}

void ListMU2Pult_Process(void)
{
  bool bOk=true;
  LIST_MU2PULT_TP *pItemDel;

  if(!s_pItemCur) s_pItemCur=s_pListMU2Pult;
  if(s_pItemCur)
  {
    if(s_pItemCur->repeat_cnt)
    {
      if(IsCurrentTickCountGT(s_pItemCur->dwTOut))
      {
        if(s_pItemCur->repeat_cnt>=d_MU2PultMaxRepeat)
        {
          pItemDel=s_pItemCur;
          s_pItemCur=s_pItemCur->next;
          if(pItemDel->pFunc_PultNoAnswer2MU) pItemDel->pFunc_PultNoAnswer2MU(pItemDel);
          if(pItemDel->psConn && GetConnectionStateFromPtr(pItemDel->psConn)==Passive)
          {
            ClrConnectionBinding(pItemDel->psConn,pItemDel->SN);
            if(!IsConnectionBinding(pItemDel->psConn)) DelConnectionByPtr(pItemDel->psConn);
          }
          ListMU2Pult_DeleteItem(pItemDel);
          return;
        }
        else bOk=SendItem2Pult(s_pItemCur);
      }
    }
    else bOk=SendItem2Pult(s_pItemCur);
    if(!bOk)
    {
SendStr2IpLogger("$DEBUG$ListMU2Pult_Process: for pult SN=%d del wCmdId=%d",s_pItemCur->SN,s_pItemCur->wId);
      pItemDel=s_pItemCur;
      s_pItemCur=s_pItemCur->next;
      if(pItemDel->pFunc_PultNoAnswer2MU) pItemDel->pFunc_PultNoAnswer2MU(pItemDel);
      if(pItemDel->psConn && GetConnectionStateFromPtr(pItemDel->psConn)==Passive)
      {
        ClrConnectionBinding(pItemDel->psConn,pItemDel->SN);
        if(!IsConnectionBinding(pItemDel->psConn)) DelConnectionByPtr(pItemDel->psConn);
      }
      ListMU2Pult_DeleteItem(pItemDel);
    }
  }
}

void IpPultEnergo_RcvAnsw(sInfoKadr *psInfo)
{
  unsigned short wCmdId;
  LIST_MU2PULT_TP *pItem;

  if(psInfo->ByteInfoKadr[6]) return; // ����� �� ������������ ��� ��!
  wCmdId=psInfo->ByteInfoKadr[8];
  wCmdId=(wCmdId << 8) | psInfo->ByteInfoKadr[7];
SendStr2IpLogger("$DEBUG$IpPultEnergo_RcvAnsw: wCmdId=%d",wCmdId);
  pItem=ListMU2Pult_FindItem(wCmdId);
  if(pItem)
  {
    if(pItem->pFunc_PultAnswer2MU) pItem->pFunc_PultAnswer2MU(pItem,psInfo);
    ListMU2Pult_DeleteItem(pItem);
  }
}

