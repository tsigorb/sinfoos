#include "..\Sinfo.h"

const char *g_strPusto="                     ";
const char *g_strCallName[]=
{
"Unknown",      // 0
"TDC",          // 1 
"RSDT",         // 2
"ATC",          // 3 
"Selector",     // 4
"Level",        // 5 
"GroupTDC",     // 6
"NoCall",       // 7
"ImitatorTDC",  // 8
"ISDNReserve",  // 9
"Commutator",   // 10
"CB",           // 11
"GroupRSDT",    // 12
"IndCall",      // 13
"ImitatorRSDT", // 14
"Circul2_7",    // 15
"Circul2_8",    // 16
"Circul2_11",   // 17
"Circul2_12",   // 18
"PPSR",         // 19
"GenCallTDC",   // 20
"RD3",          // 21
"TDCIcx",       // 22
"Spec1",        // 23
"Spec2",        // 24
"Spec3",        // 25
"Spec4",        // 26
"Spec5",        // 27
"Spec6",        // 28
"Spec7",        // 29
"Spec8",        // 30
"ISDN"          // 31
};

xTaskHandle thTaskHandles[16];

// --------------- �������� ������ ----------------------------
ERROR_COUNTERS_TP g_sErrCntrs;

// ����������!
short CountErrSendQueueCommand=0; // ������� ������ ��� ���������� � ������� ������� �� ���������
short CountErrMalloc=0; // ������� ������ ��� ��������� ������
short CountErrMalloc1=0; // ������� ������ ��� ��������� ������
short CountErrCommand=0; // ������ ����� �� �� ���������� �������
short CountErrQueueReply=0; // ������� xQueueReply - �����������
short countErrPutUart=0; // ��� ���������� ��� �������� �� Uart
short CountErrTimeOut=0; // ��� ��������� �� ������ ������� �� RS232 
short CountErrNoReplyUart=0; // �� �� ������� �� ������� �� 40 ��
short CountErrNoCommand=0; // "��� ����� �������"  ��� ������
short CountErrSin232=0; // ������. �� ���� �����������
short CountErrCS=0; // ������. �� ������� ����������� �����
short CountErrSendIndic=0;  // ������ ���������� � ������� �� ���������
short CountErrSend485=0; // ������ ��������
short CountErrSend485_Q=0; // ������ ��� ���������� � ������� �� ��������
short CountErrReceive485=0; // ������ ������
short CountErrCRCReceive485=0; // ������ ������ �� ����������� �����
short CountErrSendKeyPr=0; // ������ ���������� � ������� ������� ������
short CountErrReadBuf=0; // ������ ������ ������
short CountErrCommute=0; // ������ ���������� 
short CountErrEDSS=0; // ������ ��� ������ ������ EDSS  
short CountErrCallMemISDN=0; // ������ ���������� � ������� ������� EDSS
short CountErrPaket=0; //������� ������� ��� ������
short CountErrQueueUart=0;
short CountErrLoadCoeffBK=0;
short CountErrLoadPgmBK=0; // ���������� ������ �������� ��������� ��
short CountNoReply =0; //������� �� ������� �� ���������
short ErrReadF=0;
short Count50Err=0;

// ��������
short CountPaket =0; //������� �������� �������
short CountPaketFull =0; //������� �������� ������ �������
short CountPaketSendFull =0; //������� ���������� ������ �������
short CountPaketSend =0; //������� ���������� �������
short Count50=0;
short Count50R=0;
short CountBlock = 0;

unsigned short NumbTabl; // ����� ������� ���������� ��� ��

unsigned int MassTimer[NumberTimer]; // ������ ��������

