#include "..\Sinfo.h"
#include "FunctionsPrototypes.h"
#include "GlobalData.h"

#ifdef __SNMP__
#include "SNMP\Snmp.h"
#endif

/////////////////////////////////////////////////////////////////////
unsigned char CheckRegistration(unsigned char btSN,unsigned char btIpPultCnt);
void ReOpenSocket(unsigned char btSocket,unsigned short wPort,bool bSend);

bool GetMAC4IPPM(unsigned int dwIP,unsigned char *MACAddr);

/////////////////////////////////////////////////////////////////////
#define dIpPultCmdTOut    3000

typedef struct
{
  unsigned long int dwIp;
  unsigned short wPort; //,wPortSnd;
  unsigned char btMAC[6];
  unsigned int dwTOut,dwTOutCmd;
  unsigned char btType,btSN;
/*  unsigned char btDS,btRS;
  unsigned short wCP4DS,wCP4RS;
  unsigned char btName[MAX_DIGIT_TLFN+1],btNumbUniq[MAX_DIGIT_TLFN+1],btNumbSerial[MAX_DIGIT_TLFN+1];*/
} IRP_TP;

/////////////////////////////////////////////////////////////////////
extern int g_iPultPingPong[NumberKeyboard];

static IRP_TP s_MassIRP[IRP_MAX]; // ������ ����������� � 0 �� NumberKeyboard-1, ����� - ��� ���������!!
volatile static bool s_bNeedIpReinit=false;
static unsigned int s_dwUKTOut=0;

/////////////////////////////////////////////////////////////////////
void InitIRP(void)
{
  memset(s_MassIRP,0,sizeof(s_MassIRP));
  s_dwUKTOut=xTaskGetTickCount()+1000;
}

bool IsNeedIpReinit(void)
{
  bool bNeed=s_bNeedIpReinit;
  s_bNeedIpReinit=false;
  return(bNeed);
}

void SetNeedIpReinit(void)
{ s_bNeedIpReinit=true; }

unsigned char GetCodeFromSndCP(unsigned char btCP)
{ return(btCP); }

void _xResetIpPult(unsigned char btInd)
{
  static section ("bigdata") sCommand sCmd;

  vPortEnableSwitchTask(false);
  sCmd.Source=UserUDP;
  sCmd.A1=0;
  sCmd.L=3;
  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[1]=184;
  sCmd.BufCommand[2]=GetPultIndFromSN(s_MassIRP[btInd].btSN);
  xQueueSendCmd(&sCmd,0);        
  memset(s_MassIRP+btInd,0,sizeof(IRP_TP));
  //ReOpenSocket(CommandSocket,g_IpParams.wPortControl,false);
  //ReOpenSocket(SoundSocket,g_IpParams.wPortSound,false);
  vPortEnableSwitchTask(true);
}

void ResetIPPults(void)
{
  unsigned char btInd;

  for(btInd=0;btInd<IRP_MAX;btInd++)
  {
    if(s_MassIRP[btInd].btType==IP_PULT) _xResetIpPult(btInd);
  }
}

void CheckIpSendTime(void)
{
  unsigned char btInd;
  static unsigned short s_wErrCnt=0;
  static unsigned int s_dwTm=0;
  static char strInfo[50];

  if(!s_dwTm) s_dwTm=xTaskGetTickCount()+1500;
  for(btInd=0;btInd<IRP_MAX;btInd++)
  {
    if(s_MassIRP[btInd].btType==IP_PULT)
    {
      if(IsCurrentTickCountGT(s_dwTm))
      {
        s_wErrCnt++;
        LockPrintf();
        sprintf(strInfo,"CheckIpSendTimeErr=%d",s_wErrCnt);
        UnlockPrintf();
        AddToLogWithInfo(lt_Error,ls_Task,lc_Debug,strInfo);
SendStr2IpLogger("$LIFE_INDIC$CheckIpSendTimeErr for IP-pult SN=%d",s_MassIRP[btInd].btSN);
      }
      s_dwTm=xTaskGetTickCount()+1500;
      return;
    }
  }
  s_dwTm=xTaskGetTickCount()+1500;
}

// ���� � ������� 3 ��� �� �������� �� Ip-������ ��������� �����, �� ������������� ��������� �����
void CheckRcvIpPultAlive(unsigned char btInd)
{
  if(s_MassIRP[btInd].dwTOutCmd)
  {
    if(IsCurrentTickCountGT(s_MassIRP[btInd].dwTOutCmd))
    {
      ReOpenSocket(CommandSocket,g_IpParams.wPortControl,true);
      s_MassIRP[btInd].dwTOutCmd=xTaskGetTickCount()+dIpPultCmdTOut;
    }
  }
}

void IRPTOut(void)
{
  unsigned char btInd,PultNb;
  static section ("bigdata") sInfoKadr sInfo;

  for(btInd=0;btInd<IRP_MAX;btInd++)
  {
    if(s_MassIRP[btInd].btType==IP_PULT) CheckRcvIpPultAlive(btInd);
    if(s_MassIRP[btInd].dwTOut && IsCurrentTickCountGT(s_MassIRP[btInd].dwTOut))
    {
      if(s_MassIRP[btInd].btType==IP_PULT)
      {
        AddToLog(lt_Error,ls_Task,lc_IpPultUnreg,s_MassIRP[btInd].btSN);
        SendStr2IpLogger("$LIFE_INDIC$IP-pult SN=%d life indicator timeout!",s_MassIRP[btInd].btSN);
        _xResetIpPult(btInd);
      }
      else
      {
        SendStr2IpLogger("$LIFE_INDIC$IRP type=%d life indicator timeout!",s_MassIRP[btInd].btType);
        memset(s_MassIRP+btInd,0,sizeof(IRP_TP));
      }
    }
  }
  if(IsCurrentTickCountGT(s_dwUKTOut))
  { // ��������� ����������� ��������� ����� 1 ��� � ������� ���� ������������������ �������
    sInfo.Source=UserUDP;
    for(btInd=0;btInd<IRP_MAX;btInd++)
    {
      switch(s_MassIRP[btInd].btType)
      {
        case IP_PULT:
        { // "UK alive for pult xx, L1 chan 1xx, L2 chan 1xx"
          PultNb=GetPultIndFromSN(s_MassIRP[btInd].btSN);
          if(PultNb<NumberKeyboard)
          {
            sInfo.A1=s_MassIRP[btInd].dwIp;
            sInfo.PORT1=s_MassIRP[btInd].wPort;
            sInfo.L=48;
            sInfo.ByteInfoKadr[0]=11;
            sInfo.ByteInfoKadr[1]=0xb4;
            LockPrintf();
            sprintf((char *)sInfo.ByteInfoKadr+2,
                    "UK alive for pult %02d, L1 chan 1%02d, L2 chan 1%02d",
                    (int)s_MassIRP[btInd].btSN,
                    (int)(GetOperatorChannel(PultNb,0) & 0xff),
                    (int)(GetOperatorChannel(PultNb,1) & 0xff));
            UnlockPrintf();
            sInfo.ByteInfoKadr[50]=PultNb; // ����� 50 � 51 � ���� �� ����������, � ������������ ��� ���������� ����!!
            sInfo.ByteInfoKadr[51]=MassM1[PultNb];
            xQueueSend(xQueueReply[UserUDP],&sInfo,0,0);
//SendStr2IpLogger((char *)sInfo.ByteInfoKadr+2);
          }
          break;
        }
        case IP_LOGGER:
        {
          sInfo.L=2;
          sInfo.ByteInfoKadr[0]=254;
          sInfo.ByteInfoKadr[1]=0xff;
          sInfo.A1=s_MassIRP[btInd].dwIp;
          sInfo.PORT1=s_MassIRP[btInd].wPort;
          xQueueSend(xQueueReply[UserUDP],&sInfo,0,0);
          break;
        }
      }
    }
    s_dwUKTOut=xTaskGetTickCount()+1000;
  }
}

bool GetMACAddrFromIp(unsigned int dwIP,unsigned char *MACAddr)
{ // ���������� ������������ ����� �� �����, �.�. ��� �-��� �������� ������ � ������ IPXCHG
  unsigned char btInd,*pbtMAC;
  btInd=dwIP & 0xff;
  if(!dwIP || ((btInd>0xdf) && (btInd<0xf0))) return(false); // e0.xx.xx.xx - ef.xx.xx.xx multicast
  for(btInd=0;btInd<IRP_MAX;btInd++)
  {
    if((s_MassIRP[btInd].btType!=IP_NONE) && (s_MassIRP[btInd].dwIp==dwIP))
    {
      pbtMAC=s_MassIRP[btInd].btMAC;
      if(pbtMAC[0] | pbtMAC[1] | pbtMAC[2] | pbtMAC[3] | pbtMAC[4] | pbtMAC[5])
      { memcpy(MACAddr,pbtMAC,6); return(true); }
      return(false);
    }
  }
  return(GetMAC4IPPM(dwIP,MACAddr));
}

unsigned char FindIpReg(unsigned char btType,
                        unsigned int *pA1,unsigned short *pPORT1,
                        unsigned char *pbtCnt,unsigned char *pbtIndFree)
{
  unsigned char btInd;
  unsigned short PORT1=0;
  unsigned int A1=0;

  if(pbtCnt) *pbtCnt=0;
  if(pbtIndFree) *pbtIndFree=0xff;
  if(pA1) A1=*pA1;
  if(pPORT1) PORT1=*pPORT1;
  for(btInd=0;btInd<IRP_MAX;btInd++)
  {
    if(pbtIndFree && (*pbtIndFree==0xff) && (s_MassIRP[btInd].btType==IP_NONE)) *pbtIndFree=btInd;
    if(s_MassIRP[btInd].btType==btType)
    {
      if(pbtCnt) (*pbtCnt)++;
      if(!A1)
      {
        if(pA1) *pA1=s_MassIRP[btInd].dwIp;
        if(pPORT1) *pPORT1=s_MassIRP[btInd].wPort;
        return(btInd);
      }
      if(s_MassIRP[btInd].dwIp==A1)
      {
        if(PORT1)
        {
          if(s_MassIRP[btInd].wPort==PORT1) return(btInd);
          return(0xff);
        }
        else return(btInd);
      }
    }
  }
  return(0xff);
}

void IRP_Registration(unsigned char btType,unsigned int A1,unsigned short PORT1,unsigned char *pbtMAC)
{
  unsigned char btInd,btCnt,btIndFree,btMaxId,btSN;
  static section("bigdata") sInfoKadr sInfo;

  if(btType==IP_DSPCOM)
  {
    btMaxId=d_MAXDSPCOM;
    btSN=0xff;
  }
  else
  {
    btMaxId=1;
    btSN=0xfe;
  }
  btInd=FindIpReg(btType,&A1,NULL,&btCnt,&btIndFree);
  if(btInd==0xff)
  {
    if((btCnt>=btMaxId) || (btIndFree==0xff)) return;
    s_MassIRP[btIndFree].btType=btType;
    s_MassIRP[btIndFree].btSN=btSN;
    s_MassIRP[btIndFree].wPort=PORT1;
    s_MassIRP[btIndFree].dwIp=A1;
    s_MassIRP[btIndFree].dwTOut=xTaskGetTickCount()+dIpPultTOut;
    s_MassIRP[btIndFree].dwTOutCmd=xTaskGetTickCount()+dIpPultCmdTOut;
    if(pbtMAC) memcpy(s_MassIRP[btIndFree].btMAC,pbtMAC,6);
    else memset(s_MassIRP[btIndFree].btMAC,0,6);
    AddToLog(lt_Notification,ls_Task,lc_IpPultReg,btSN);
    if(btType==IP_LOGGER) SendStr2IpLogger("$REGISTRATION$IP-LOGGER REGISTRATION!!!");
    btInd=btIndFree;
  }
  else
  {
    s_MassIRP[btInd].wPort=PORT1;
    s_MassIRP[btInd].dwTOut=xTaskGetTickCount()+dIpPultTOut;
    if(pbtMAC &&
       (pbtMAC[0] | pbtMAC[1] | pbtMAC[2] | pbtMAC[3] | pbtMAC[4] | pbtMAC[5])) memcpy(s_MassIRP[btInd].btMAC,pbtMAC,6);
  }
/*if(btType==IP_DSPCOM)
{
  SendStr2IpLogger("$REGISTRATION$DspCom registration cmd10.%d: A1=%08x, PORT1=%04d",pbtMAC ? 198 : 100,A1,PORT1);
}*/
}

void IRP_Unregistration(unsigned char btType,unsigned int A1)
{
  unsigned char btInd;

  btInd=FindIpReg(btType,&A1,NULL,NULL,NULL);
  if(btInd!=0xff) memset(s_MassIRP+btInd,0,sizeof(IRP_TP));
}

void IRP_Check(unsigned char btType,unsigned int A1)
{
  unsigned char btInd;

  btInd=FindIpReg(btType,&A1,NULL,NULL,NULL);
  if(btInd!=0xff)
  {
//SendStr2IpLogger("$LIFE_INDIC$IRP type=%d alive",btType);
    s_MassIRP[btInd].dwTOut=xTaskGetTickCount()+dIpPultTOut;
  }
}

void SendDiagnostics2DspCom(unsigned char *pData,unsigned char Len)
{
  unsigned char btInd;
  static section("bigdata") sInfoKadr sInfo;

  if(!Len) return;
  sInfo.L=Len;
  memcpy(sInfo.ByteInfoKadr,pData,Len);
  vPortEnableSwitchTask(false);
  for(btInd=0;btInd<IRP_MAX;btInd++)
  {
    if(s_MassIRP[btInd].btType==IP_DSPCOM)
    {
      sInfo.A1=s_MassIRP[btInd].dwIp;
      sInfo.PORT1=s_MassIRP[btInd].wPort;
      if(xQueueSend(xQueueReply[UserUDP],&sInfo,0,0)==errQUEUE_FULL)
      { ErrQueueAns++; break; }
    }
  }
  vPortEnableSwitchTask(true);
}

bool IpPultRegistration(unsigned char *pbtBufCmd,unsigned char btLn)
{
  unsigned char btRes,btInd,btSN,
                btIpPultCnt,btIndFree;
  unsigned int dwIp;
  unsigned short wPort; //,wPortSnd;
  static section ("bigdata") sInfoKadr sIK;

  btRes=0;
  btIpPultCnt=0;
  btIndFree=0xff;
  
  btSN=pbtBufCmd[0];
  wPort=pbtBufCmd[1]; wPort=(wPort<<8) | pbtBufCmd[2];
  dwIp=pbtBufCmd[6];
  dwIp=(dwIp<<8) | pbtBufCmd[5]; dwIp=(dwIp<<8) | pbtBufCmd[4]; dwIp=(dwIp<<8) | pbtBufCmd[3];
  if(btLn!=13) return false;
/*  if(btLn==13) wPortSnd=0; // ���������� ������ ��� �������� ����� �����
  else 
  {
    if(btLn!=15) return(false);
    // ���������� ������ c ��������� ����� �����
    wPortSnd=pbtBufCmd[13]; wPortSnd=(wPortSnd<<8) | pbtBufCmd[14];
  }*/
//if(s_bDisableReg) return(false);
  
  for(btInd=0;btInd<IRP_MAX;btInd++)
  {
    if(s_MassIRP[btInd].btType==IP_PULT)
    {
      btIpPultCnt++;
      if(s_MassIRP[btInd].btSN==btSN)
      {
        if((s_MassIRP[btInd].wPort!=wPort) ||
           (s_MassIRP[btInd].dwIp!=dwIp)) btRes=1;
        break;
      }
    }
    if((btIndFree==0xff) && (s_MassIRP[btInd].btType==IP_NONE)) btIndFree=btInd;
  }
  if(btInd==IRP_MAX)
  {
    if(btIndFree==0xff) btRes=1;
    else
    {
      btRes=CheckRegistration(btSN,btIpPultCnt);
      if(btRes==0)
      { // ��������� ����� � �������
        vPortEnableSwitchTask(false);
        s_MassIRP[btIndFree].btType=IP_PULT;
        s_MassIRP[btIndFree].wPort=wPort;
//        s_MassIRP[btIndFree].wPortSnd=wPortSnd;
        s_MassIRP[btIndFree].dwIp=dwIp;
        s_MassIRP[btIndFree].btSN=btSN;
        s_MassIRP[btIndFree].dwTOut=xTaskGetTickCount()+dIpPultTOut;
        memcpy(s_MassIRP[btIndFree].btMAC,pbtBufCmd+7,6);
        g_iPultPingPong[GetPultIndFromSN(btSN)]=0;
        vPortEnableSwitchTask(true);
        AddToLog(lt_Notification,ls_Task,lc_IpPultReg,btSN);
        SendStr2IpLogger("$LIFE_INDIC$Pult SN=%d registration",btSN);
      }
    }
  }
  //sIK.Source=UserUDP;
  sIK.A1=dwIp;
  sIK.PORT1=wPort;
  sIK.L=4;
  sIK.ByteInfoKadr[0]=11;
  sIK.ByteInfoKadr[1]=0xb1;
  if(btRes)
  { // ������ �����������
    sIK.ByteInfoKadr[2]=0xff;
    sIK.ByteInfoKadr[3]=btRes;
  }
  else
  { // ����������� �������
    sIK.ByteInfoKadr[2]=1;
    sIK.ByteInfoKadr[3]=0;
  }
  xQueueSend(xQueueReply[UserUDP],&sIK,0,0);
  if(btRes) return(false); // ������ �����������
  if(btInd==IRP_MAX) return(true); // ����������� ������ ������� -> ����������� �������� ����
  return(false); // ����������� ��� ���� -> ������������� �������� ���� �� ����
}

void _xIpPultLife(unsigned char btSN)
{
  unsigned char btInd;
  if(!btSN) return;
  for(btInd=0;btInd<IRP_MAX;btInd++)
  {
    if((s_MassIRP[btInd].btType==IP_PULT) && (s_MassIRP[btInd].btSN==btSN))
    {
      unsigned char PultNb=GetPultIndFromSN(btSN);
      if(g_iPultPingPong[PultNb]<-1) g_iPultPingPong[PultNb]=0;
      g_iPultPingPong[PultNb]++;
      if(g_iPultPingPong[PultNb]>1)
      {
        g_iPultPingPong[PultNb]=1;
        SendStr2IpLogger("$LIFE_INDIC$Pult SN=%d alive",btSN);
      }
      s_MassIRP[btInd].dwTOut=xTaskGetTickCount()+dIpPultTOut;
      s_MassIRP[btInd].dwTOutCmd=xTaskGetTickCount()+dIpPultCmdTOut;
      return;
    }
  }
}

void IpPultLife(unsigned char *pbtBufCmd)
{ // "Pult XX alive"
  unsigned char btSN;

//SendStr2IpLogger((char *)pbtBufCmd);
  if((!memcmp(pbtBufCmd,"Pult ",5)) &&
     (!memcmp(pbtBufCmd+7," alive",6)))
  {
    if(IsDigit(pbtBufCmd[5]) && IsDigit(pbtBufCmd[6]))
    {
      btSN=(pbtBufCmd[5]-'0')*10+(pbtBufCmd[6]-'0');
      _xIpPultLife(btSN);
    }
  }
}

bool IRP_Cmd(unsigned char *pbtBufCmd,unsigned char btLn)
{
  if(pbtBufCmd[0]==10)
  {
    switch(pbtBufCmd[1])
    {
      case 0xb1:
      {
        if(IsPultConfigRead())
        {
          if(IpPultRegistration(pbtBufCmd+2,btLn-2))
          {
            pbtBufCmd[2]=GetPultIndFromSN(pbtBufCmd[2]);
            return(false); // ����������� ������� ���� ��� ��� ����� �� IP-������
          }
        }
        return(true);
      }
      case 0xb2:
      {
        //if(IsPultConfigRead()) IpRecordRegistration(pbtBufCmd+2);
        return(true);
      }
      case 0xb4:
      {
        if(IsPultConfigRead() && (btLn==15)) IpPultLife(pbtBufCmd+2);
        return(true);
      }
    }
  }
  else
  {
    if(pbtBufCmd[0]==254) return(true);
  }
  return(false);
}

bool GetAddressIpPult(unsigned char SN,unsigned int *pdwIP,unsigned short *pwPort)
{
  unsigned char btInd;

  if(SN<16) return(false);
  vPortEnableSwitchTask(false);
  for(btInd=0;btInd<IRP_MAX;btInd++)
  {
    if((s_MassIRP[btInd].btType==IP_PULT) && (s_MassIRP[btInd].btSN==SN))
    {
      *pdwIP=s_MassIRP[btInd].dwIp;
      *pwPort=s_MassIRP[btInd].wPort;
      vPortEnableSwitchTask(true);
      return(true);
    }
  }
  vPortEnableSwitchTask(true);
  return(false);
}

/*bool GetSoundPort4IpPult(unsigned int dwIp,unsigned short *pwPort)
{
  unsigned char btInd;

  vPortEnableSwitchTask(false);
  for(btInd=0;btInd<IRP_MAX;btInd++)
  {
    if((s_MassIRP[btInd].btType==IP_PULT) && (s_MassIRP[btInd].dwIp==dwIp))
    {
      *pwPort=s_MassIRP[btInd].wPortSnd;
      vPortEnableSwitchTask(true);
      return(true);
    }
  }
  vPortEnableSwitchTask(true);
  *pwPort=0;
  return(false);
}*/

bool GetControlIpAddr(unsigned int *pdwIpAddr, unsigned short *pdwIpPort)
{
  unsigned char btInd;
  unsigned int A1;

  memcpy(&A1,g_IpParams.ucDataSNMPrIP,4);
  if(A1)
  {
    btInd=FindIpReg(IP_DSPCOM,&A1,NULL,NULL,NULL);
    if(btInd!=0xff)
    {
      *pdwIpAddr=A1;
      *pdwIpPort=s_MassIRP[btInd].wPort;
      return(true);
    }
  }
  return(false);
}

