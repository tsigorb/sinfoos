#include "..\Sinfo.h"
#include "FunctionsPrototypes.h"
#include "GlobalData.h"

///////////////////////////////////////////////////////////////////////////////
#define NumTekVer " Ver.15.02.10 " // ������� ������ �� (�� ������ ������ ��� ������������ � �������!!!)

#ifdef __ADACE__
    #define ADACE "����� "
#else
    #define ADACE ""
#endif

#define TekVer     NumTekVer, __DATE__ " " __TIME__, ADACE, "" // ������ ��

char FwVersionInfo[4][24] = {TekVer};  // version number

///////////////////////////////////////////////////////////////////////////////
void CSUp(void);

///////////////////////////////////////////////////////////////////////////////
volatile bool g_bPrinfBusy=false;

///////////////////////////////////////////////////////////////////////////////
bool IsDigit(char chSb)
{
  if((chSb>='0') && (chSb<='9')) return(true);
  return(false);
}

char *GetNextStr(char *pBuf)
{
  char chSimb;

  do
  {
    chSimb=*pBuf;
    if(!chSimb) return(NULL);
    if(chSimb==0x0a)
    {
      *pBuf=0;
      return(pBuf+1);
    }
    pBuf++;
  }
  while(chSimb);
  return(NULL);
}

unsigned int atoIP(char *pStr)
{
  int i;
  char *pEnd;
  unsigned int dwIP=0;

  for(i=0;i<4;i++)
  {
    if(!pStr || !pStr[0]) break;
    dwIP<<=8;
    if(i<3)
    {
      pEnd=strchr(pStr,'.');
      if(!pEnd) break;
      *pEnd=0; pEnd++;
    }
    pStr=strclr(pStr);
    if(!pStr) break;
    dwIP|=atoi(pStr);
    if(i!=3) pStr=pEnd;
    else
    {
      dwIP=(dwIP>>24) | ((dwIP & 0xff0000)>>8) | ((dwIP & 0xff00)<<8) | ((dwIP & 0xff)<<24);
      return dwIP;
    }
  }
  return 0;
}

///////////////////////////////////////////////////////////////////////////////
unsigned int GET_DW(unsigned char *pbtBuf)
{
  unsigned int dwCode;
  dwCode=pbtBuf[3];
  dwCode=(dwCode<<8) | pbtBuf[2];
  dwCode=(dwCode<<8) | pbtBuf[1];
  dwCode=(dwCode<<8) | pbtBuf[0];
  return(dwCode);
}

unsigned short GET_W(unsigned char *pbtBuf)
{
  unsigned short wCode;
  wCode=pbtBuf[1];
  wCode=(wCode<<8) | pbtBuf[0];
  return(wCode);
}

unsigned char *SaveInt2Buf(unsigned char *pbtBuf,int iData)
{
  pbtBuf[0]=iData >> 24;
  pbtBuf[1]=iData >> 16;
  pbtBuf[2]=iData >> 8;
  pbtBuf[3]=iData;
  return(pbtBuf+4);
}

unsigned char *RestoreIntFromBuf(unsigned char *pbtBuf,int *piData)
{
  int iData;
  iData=pbtBuf[0];
  iData=(iData << 8) | pbtBuf[1];
  iData=(iData << 8) | pbtBuf[2];
  iData=(iData << 8) | pbtBuf[3];
  *piData=iData;
  return(pbtBuf+4);
}

//################ �-��� ��������� �������� ������� � �������� #################
unsigned char IsTickCountGT(unsigned int dwTick1,unsigned int dwTick0)
{
  unsigned char btState;

  btState=(unsigned char)((dwTick0 >> 30) | ((dwTick1 >> 29) & 0x02));
  switch(btState)
  {
    case 0x00:
    case 0x03:
      if(dwTick1>dwTick0) return(1);
      return(0);
    case 0x01:
      return(1);
    case 0x02:
      if(dwTick1-dwTick0>0x7fffffff) return(0);
      return(1);
  }
  return(0);
}

unsigned char IsCurrentTickCountGT(unsigned int dwTick0)
{ return IsTickCountGT(xTaskGetTickCount(),dwTick0); }

unsigned char IsTickCountLT(unsigned int dwTick1,unsigned int dwTick0)
{
  unsigned char btState;

  btState=(unsigned char)((dwTick0 >> 30) | ((dwTick1 >> 29) & 0x02));
  switch(btState)
  {
    case 0x00:
    case 0x03:
      if(dwTick0>dwTick1) return(1);
      return(0);
    case 0x01:
      return(0);
    case 0x02:
      if(dwTick1-dwTick0>0x7fffffff) return(1);
      return(0);
  }
  return(0);
}

unsigned char IsCurrentTickCountLT(unsigned int dwTick0)
{ return IsTickCountLT(xTaskGetTickCount(),dwTick0); }

// ##### ������ �� ������������ (D0..D3) � SPI CS ��� W6100 (D5) � SC16IS740IPW (D4) #####
volatile static unsigned char LEDdata=0xf0;

// ������������� ����������� - ��� ��������
void LedInit(void)
{
  unsigned int dwIRQ=cli();
  LEDdata=0xf0;
  *pLED=LEDdata;
  ssync();
  sti(dwIRQ);
}

// ��������� ���������� � �������� �������
void LedOn(unsigned char LedNum)
{
  unsigned int dwIRQ=cli();
  LEDdata |= ((1<<(LedNum-1)) & 0x0f);  // ����������� ������, ����������� ������ �����������
  *pLED=LEDdata;
  ssync();
  sti(dwIRQ);
}

// ���������� ���������� � �������� �������
void LedOff(unsigned char LedNum)
{
  unsigned int dwIRQ=cli();
  LEDdata &= ~((1<<(LedNum-1)) & 0x0f);  // ����������� ������, ����������� ������ �����������
  *pLED=LEDdata;
  ssync();
  sti(dwIRQ);
}

// ������������ ���������� � �������� �������
void LedToggle(unsigned char LedNum)
{
  unsigned int dwIRQ=cli();
  LEDdata ^= ((1<<(LedNum-1)) & 0x0f);  // ����������� ������, ����������� ������ �����������
  *pLED=LEDdata;
  ssync();
  sti(dwIRQ);
}

// ��������� ���������� � �������� �������
bool LedStatus(unsigned char LedNum)
{
  unsigned int dwIRQ=cli();
  unsigned char btLed=(LEDdata & (1<<(LedNum-1))) & 0x0f;
  sti(dwIRQ);
  return(btLed);
}

void LedWorkIndicator(void)
{
  static portTickType xTimeToWake=0;

  if(IsCurrentTickCountLT(xTimeToWake)) return;
  if(!LedStatus(ledRabota))
  { // �������� ���������
    xTimeToWake=xTaskGetTickCount()+100;
    LedOn(ledRabota);
  }
  else
  { // ����� ���������
    xTimeToWake=xTaskGetTickCount()+1500;
    LedOff(ledRabota);
  }
}

void Rs485CS_Low(void)
{
  unsigned int dwIRQ=cli();
  LEDdata&=0xef;
  *pLED=LEDdata;
  ssync();
  sti(dwIRQ);
}

void Rs485CS_Hi(void)
{
  unsigned int dwIRQ=cli();
  LEDdata|=0x10;
  *pLED=LEDdata;
  ssync();
  sti(dwIRQ);
}

void IpCS_Low(void)
{
  unsigned int dwIRQ=cli();
  LEDdata&=0xdf;
  *pLED=LEDdata;
  ssync();
  sti(dwIRQ);
}

void IpCS_Hi(void)
{
  unsigned int dwIRQ=cli();
  LEDdata|=0x20;
  *pLED=LEDdata;
  ssync();
  sti(dwIRQ);
}

//###################### ������ � ���������� ����� � ���������� �������� ##########################
#pragma pack(1)
typedef struct
{
  unsigned int dwCycle;
//  unsigned int dwFreeStackSize;
//  unsigned char btMasTaskVar[4];
} TASK_STATE_TP;
#pragma pack()

unsigned int GetFreeTaskStackSize(void);

/////////////////////////////////////////////////////////////////////
static TASK_STATE_TP s_MassTaskState[d_MaxTask];

volatile static unsigned int s_dwWDTaskState=0;

/////////////////////////////////////////////////////////////////////
signed portBASE_TYPE xQueueSendCmd(sCommand *psCmd,portTickType xTicksToWait)
{
  signed portBASE_TYPE Result;

  if(psCmd->BufCommand[0]==10)
  {
    while(!xIsQueueEmpty(xQueueSportX)) vTaskDelay(10);
  }
  if(psCmd->Source==UserRS485)
  {  
    while(xQueueReceive(xQueueReply[UserRS485],NULL,0)==pdPASS); // ������� ������� �������
  }
xTicksToWait=0;
  if(psCmd->BufCommand[0]==10) Result=xQueueSend(xQueueSportX,psCmd,xTicksToWait,0);
  else Result=xQueueSend(xQueueCommand,psCmd,xTicksToWait,0);
  return(Result);
}

void SetTaskVar(unsigned int dwTask,unsigned char btOff,unsigned char btVar)
{
  unsigned char btPos=WDSTATE2NUMB(dwTask);

  if((btPos<d_MaxTask) && (btOff<4))
  {
//    s_MassTaskState[btPos].btMasTaskVar[btOff]=btVar;
  }
}

void SetWDState(unsigned int dwTask)
{
/*  unsigned char btPos=WDSTATE2NUMB(dwTask);
  if(btPos<d_MaxTask)
  {
    vPortEnableSwitchTask(false);
    s_dwWDTaskState|=dwTask;
    s_MassTaskState[btPos].dwCycle++;
//    s_MassTaskState[btPos].dwFreeStackSize=GetFreeTaskStackSize();
    vPortEnableSwitchTask(true);
  }*/
}

void SendTaskState2Ip(void)
{
  unsigned char btPos,btOff;
  static unsigned int s_dwTOut=0;
  static sInfoKadr sInfo;

  if(IsCurrentTickCountLT(s_dwTOut)) return;
  if(GetControlIpAddr(&sInfo.A1,&sInfo.PORT1))
  {
    vPortEnableSwitchTask(false);
    sInfo.Source=UserUDP;
    memset(sInfo.ByteInfoKadr,0,sizeof(sInfo.ByteInfoKadr));
    sInfo.ByteInfoKadr[0]=114;
    btOff=1;
    for(btPos=0;btPos<d_MaxTask;btPos++)
    {
      if(d_WDSTATE_TASKALL & (0x01ul << btPos))
      {
        memcpy(sInfo.ByteInfoKadr+btOff,s_MassTaskState+btPos,sizeof(TASK_STATE_TP));
      }
      btOff+=sizeof(TASK_STATE_TP);
    }
    sInfo.L=btOff;
    vPortEnableSwitchTask(true);
  }
  s_dwTOut=xTaskGetTickCount()+1000;
}

void xTaskGetCurrentTaskName(char *strTaskName);

// ���������� ������ ����������� ������� ��� ����������� �������
void BlockWatchDog(void)
{
char strTaskName[20];
xTaskGetCurrentTaskName(strTaskName);
SendStr2IpLogger("$DEBUG$BlockWatchDog: task=%s",strTaskName);
vTaskDelay(200);
  cli();
  CSUp();
  *pLED=0xff;
  ssync();
  *pFIO_FLAG_S=PF12;
  ssync();
/**pSWRST=0x7;
asm volatile ("RAISE 1;");
asm volatile ("idle;");*/
  while(1);
}

