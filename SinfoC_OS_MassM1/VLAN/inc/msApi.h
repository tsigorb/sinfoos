#include <VLAN\inc\Copyright.h>

/********************************************************************************
* msApi.h
*
* DESCRIPTION:
*       API definitions for QuarterDeck Device
*
* DEPENDENCIES:
*
* FILE REVISION NUMBER:
*
*******************************************************************************/

#ifndef __msApi_h
#define __msApi_h

#include <VLAN\inc\msApiDefs.h>
#include <VLAN\inc\msApi\msApiInternal.h>
#include <VLAN\inc\msApiPrototype.h>

#endif /* __msApi_h */
