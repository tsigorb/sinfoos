//#define __VLAN__

#ifdef __VLAN__

#ifndef VLAN_H_INCLUDED
#define VLAN_H_INCLUDED

#include "inc\msApi.h"
#include "inc\driver\gtDrvConfig.h"
#include "inc\driver\gtHwCntl.h"
#include "inc\platform\platformDeps.h"

#define	vlan_port	2
/*
extern void ResetVLAN();
extern GT_STATUS SetVLANID(GT_U16 *aVLANID, unsigned char *aPri);
extern void CheckVLAN(GT_U16 *aVLANID);
extern GT_STATUS TestVLAN(GT_U16 *aVLANID);
*/

void ResetVLAN();
GT_STATUS SetVLANID(GT_U16 *aVLANID, unsigned char *aPri);
void CheckVLAN(GT_U16 *aVLANID);
//GT_STATUS TestVLAN(GT_U16 *aVLANID);

#endif //VLAN_H_INCLUDED
#endif // VLAN
