#include "VLAN\VLAN.h"
#ifdef	__VLAN__
#include <VLAN\inc\Copyright.h>
/********************************************************************************
* gtOs.c
*
* DESCRIPTION:
*       Semaphore related routines
*
* DEPENDENCIES:
*       OS Dependent.
*
* FILE REVISION NUMBER:
*       $Revision: 3 $
*******************************************************************************/

#include <VLAN\inc\msApi.h>
#include <VLAN\inc\platform\gtSem.h>
//#include "includes.h"

/*******************************************************************************
* gtSemCreate
*
* DESCRIPTION:
*       Create semaphore.
*
* INPUTS:
*		state - beginning state of the semaphore, either GT_SEM_EMPTY or GT_SEM_FULL
*
* OUTPUTS:
*       None
*
* RETURNS:
*       GT_SEM if success. Otherwise, NULL
*
* COMMENTS:
*       None
*
*******************************************************************************/
GT_SEM gtSemCreate
(
	IN GT_QD_DEV    *dev,
	IN GT_SEM_BEGIN_STATE state
)
{
	return (int *)2;
//	return OSSemCreate(state);
}

/*******************************************************************************
* gtSemDelete
*
* DESCRIPTION:
*       Delete semaphore.
*
* INPUTS:
*       smid - semaphore Id
*
* OUTPUTS:
*       None
*
* RETURNS:
*       GT_OK   - on success
*       GT_FAIL - on error
*
* COMMENTS:
*       None
*
*******************************************************************************/
GT_STATUS gtSemDelete
(
	IN GT_QD_DEV    *dev,
	IN GT_SEM smid
)
{
//	INT8U err;

//	OSSemDel(smid, OS_DEL_ALWAYS, &err);

//	if (err == OS_NO_ERR)
		return GT_OK;
//	else
//		return GT_FAIL;
}


/*******************************************************************************
* gtSemTake
*
* DESCRIPTION:
*       Wait for semaphore.
*
* INPUTS:
*       smid    - semaphore Id
*       timeOut - time out in miliseconds or 0 to wait forever
*
* OUTPUTS:
*       None
* RETURNS:
*       GT_OK   - on success
*       GT_FAIL - on error
*       OS_TIMEOUT - on time out
*
* COMMENTS:
*       None
*
*******************************************************************************/
GT_STATUS gtSemTake
(
	IN GT_QD_DEV    *dev,
	IN GT_SEM smid, 
	IN GT_U32 timeOut
)
{
//	INT8U err;

//	OSSemPend(smid, timeOut, &err);
	
//	if (err == OS_NO_ERR)
		return GT_OK;
//	else if (err == OS_TIMEOUT)
//		return GT_TIMEOUT;
//	else
//		return GT_FAIL;
}

/*******************************************************************************
* gtSemGive
*
* DESCRIPTION:
*       release the semaphore which was taken previously.
*
* INPUTS:
*       smid    - semaphore Id
*
* OUTPUTS:
*       None
*
* RETURNS:
*       GT_OK   - on success
*       GT_FAIL - on error
*
* COMMENTS:
*       None
*
*******************************************************************************/
GT_STATUS gtSemGive
(
	IN GT_QD_DEV    *dev,
	IN GT_SEM       smid
)
{
//	OSSemPost(smid);

	return GT_OK;
}
#endif

