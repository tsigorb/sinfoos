#include "VLAN\VLAN.h"
#ifdef	__VLAN__
#include <VLAN\inc\Copyright.h>
/********************************************************************************
* platformDeps.c
*
* DESCRIPTION:
*       platform dependent functions
*
* DEPENDENCIES:   Platform.
*
* FILE REVISION NUMBER:
*
*******************************************************************************/

#include <VLAN\inc\msApi.h>
#include <VLAN\inc\driver\gtDrvEvents.h>
#include <VLAN\inc\driver\gtHwCntl.h>
#include <VLAN\inc\platform\platformDeps.h>
#include "BF533 Flags.h"
// #include "eth/mii.h"


/*******************************************************************************
* gtAssignFunctions
*
* DESCRIPTION:
*       Exchange MII access functions and QuarterDeck Int Handler.
*		MII access functions will be called by QuarterDeck driver and
*		QD Int Handler should be called by BSP when BSP sees an interrupt which is related to
*		QD (This interrupt has to be initialized by BSP, since QD has no idea which
*		interrupt is assigned to QD)
*
* INPUTS:
*       fReadMii 	- function to read MII registers
*		fWriteMii	- functino to write MII registers
*
* OUTPUTS:
*		None.
*
* RETURNS:
*       GT_TRUE, if input is valid. GT_FALSE, otherwise.
*
* COMMENTS:
*       None.
*
*******************************************************************************/
//unsigned int			DataDelay;		// ������ ��� �������� ��������
extern void WDel(void);

GT_BOOL gtAssignFunctions
(
   GT_QD_DEV      *dev,
   FGT_READ_MII   fReadMii,
   FGT_WRITE_MII fWriteMii
)
{
	if((fReadMii == NULL) || (fWriteMii == NULL))
		return GT_FALSE;

	dev->fgtReadMii = fReadMii;
	dev->fgtWriteMii = fWriteMii;
	
	return GT_TRUE;
}

/*
void WDel(void)
{unsigned long i;
 for (i=0;i<500000;i++) DataDelay++;
}
*/

GT_BOOL mii_read(unsigned int DevAddr, unsigned int RegAddr, unsigned short* Data)	//������ ���������� Marvell
{unsigned int data, i, k;
 unsigned char j;
// k=cli();
 *pFIO_INEN &= ~PFXX;	// PFXX - ������ MDIO, PF11 - ����� MDC
 *pFIO_DIR |= 0x1800;	// ��������� MDIO � MDC �� �����
 *pFIO_FLAG_S = PFXX; 
 *pFIO_FLAG_C = PF11;
 for (j=0;j<32;j++)		// ��������� 32 �������
 {WDel(); *pFIO_FLAG_S = PF11; WDel(); *pFIO_FLAG_C = PF11;}

 WDel(); *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11; WDel(); *pFIO_FLAG_C = PF11; //0 �����
 WDel(); *pFIO_FLAG_S = PFXX;  WDel(); *pFIO_FLAG_S = PF11; WDel(); *pFIO_FLAG_C = PF11; //1

 WDel(); *pFIO_FLAG_S = PF11;  WDel(); *pFIO_FLAG_C = PF11; WDel(); //1 Read
 *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11; //0
 // PHY �����
 WDel(); *pFIO_FLAG_C = PF11; if (DevAddr & 0x10) *pFIO_FLAG_S = PFXX; else *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11;
 WDel(); *pFIO_FLAG_C = PF11; if (DevAddr & 0x08) *pFIO_FLAG_S = PFXX; else *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11;
 WDel(); *pFIO_FLAG_C = PF11; if (DevAddr & 0x04) *pFIO_FLAG_S = PFXX; else *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11;
 WDel(); *pFIO_FLAG_C = PF11; if (DevAddr & 0x02) *pFIO_FLAG_S = PFXX; else *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11;
 WDel(); *pFIO_FLAG_C = PF11; if (DevAddr & 0x01) *pFIO_FLAG_S = PFXX; else *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11;
 // ������� �����
 WDel(); *pFIO_FLAG_C = PF11; if (RegAddr & 0x10) *pFIO_FLAG_S = PFXX; else *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11;
 WDel(); *pFIO_FLAG_C = PF11; if (RegAddr & 0x08) *pFIO_FLAG_S = PFXX; else *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11;
 WDel(); *pFIO_FLAG_C = PF11; if (RegAddr & 0x04) *pFIO_FLAG_S = PFXX; else *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11;
 WDel(); *pFIO_FLAG_C = PF11; if (RegAddr & 0x02) *pFIO_FLAG_S = PFXX; else *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11;
 WDel(); *pFIO_FLAG_C = PF11; if (RegAddr & 0x01) *pFIO_FLAG_S = PFXX; else *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11;
 // ��������� MDIO �� ���� 
 WDel(); *pFIO_FLAG_S = PFXX; data = 0; i=0x8000;
 *pFIO_DIR &= ~PFXX; *pFIO_INEN |= PFXX; 
 // ����� 2 �����
 WDel(); *pFIO_FLAG_C = PF11; WDel(); *pFIO_FLAG_S = PF11; 
 WDel(); *pFIO_FLAG_C = PF11; // WDel(); *pFIO_FLAG_C = PF11; 
 // ����� ������
 for (j=0;j<16;j++)
 {WDel(); *pFIO_FLAG_S = PF11; // WDel();
  if (*pFIO_FLAG_D & PFXX) data |= i; i >>= 1;
  *pFIO_FLAG_C = PF11; 
 } 
// sti(k);
 *Data = data;
 return GT_TRUE;
}
					
GT_BOOL mii_write (unsigned int DevAddr, unsigned int RegAddr, unsigned short* data)	//������ ���������� Marvell
{unsigned int i, k, Data;
 unsigned char j;
// k=cli();
 *pFIO_INEN &= ~PFXX;	// PFXX - ������ MDIO, PF11 - ����� MDC
 *pFIO_DIR |= 0x1800;	// ��������� MDIO � MDC �� �����
 *pFIO_FLAG_S = PFXX; 
 *pFIO_FLAG_C = PF11;
 Data = *data;
 for (i=0;i<32;i++)
 {WDel(); *pFIO_FLAG_S = PF11; WDel(); *pFIO_FLAG_C = PF11;}
 WDel();  *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11; //0 �����
 WDel(); *pFIO_FLAG_C = PF11; *pFIO_FLAG_S = PFXX;  WDel(); *pFIO_FLAG_S = PF11; //1
 WDel(); *pFIO_FLAG_C = PF11; *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11; //0 Write
 WDel(); *pFIO_FLAG_C = PF11; *pFIO_FLAG_S = PFXX;  WDel(); *pFIO_FLAG_S = PF11; //1
 WDel(); *pFIO_FLAG_C = PF11; if (DevAddr & 0x10) *pFIO_FLAG_S =PFXX; else *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11;
 WDel(); *pFIO_FLAG_C = PF11; if (DevAddr & 0x08) *pFIO_FLAG_S =PFXX; else *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11;
 WDel(); *pFIO_FLAG_C = PF11; if (DevAddr & 0x04) *pFIO_FLAG_S =PFXX; else *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11;
 WDel(); *pFIO_FLAG_C = PF11; if (DevAddr & 0x02) *pFIO_FLAG_S =PFXX; else *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11;
 WDel(); *pFIO_FLAG_C = PF11; if (DevAddr & 0x01) *pFIO_FLAG_S =PFXX; else *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11;
 WDel(); *pFIO_FLAG_C = PF11; if (RegAddr & 0x10) *pFIO_FLAG_S =PFXX; else *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11;
 WDel(); *pFIO_FLAG_C = PF11; if (RegAddr & 0x08) *pFIO_FLAG_S =PFXX; else *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11;
 WDel(); *pFIO_FLAG_C = PF11; if (RegAddr & 0x04) *pFIO_FLAG_S =PFXX; else *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11;
 WDel(); *pFIO_FLAG_C = PF11; if (RegAddr & 0x02) *pFIO_FLAG_S =PFXX; else *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11;
 WDel(); *pFIO_FLAG_C = PF11; if (RegAddr & 0x01) *pFIO_FLAG_S =PFXX; else *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11;
 WDel(); *pFIO_FLAG_C = PF11; *pFIO_FLAG_S = PFXX;  WDel(); *pFIO_FLAG_S = PF11; //1 TA
 WDel(); *pFIO_FLAG_C = PF11; *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11; //0
 i=0x8000; 
 for (j=0;j<16;j++)
 {WDel(); *pFIO_FLAG_C = PF11; if (Data & i) *pFIO_FLAG_S =PFXX; else *pFIO_FLAG_C = PFXX;  WDel(); *pFIO_FLAG_S = PF11;
  i >>= 1;
 }
 *pFIO_FLAG_C = PF11; *pFIO_FLAG_S = PFXX;
 for (i=0;i<32;i++)
 {WDel(); *pFIO_FLAG_S = PF11; WDel(); *pFIO_FLAG_C = PF11;}
// sti(k);
 return GT_TRUE;
}

GT_BOOL ffReadMii(GT_QD_DEV* dev, unsigned int portNumber, unsigned int miiReg, unsigned int* value) {
	unsigned short val16;

	if (portNumber > GLOBAL_REGS_START_ADDR)
		portNumber -= GLOBAL_REGS_START_ADDR;

	if (portNumber > GLOBAL_REGS_START_ADDR)
		return GT_FALSE;

	if (GT_TRUE == mii_read(0x10 + portNumber, miiReg, &val16)) {
		*value = val16;
		return GT_TRUE;
	} else {
		*value = 0;
		return GT_FALSE;
	}

//		return GT_FALSE;
}

GT_BOOL ffWriteMii(GT_QD_DEV* dev, unsigned int portNumber, unsigned int miiReg, unsigned int value) {
	unsigned short val16;

	if (portNumber > GLOBAL_REGS_START_ADDR)
		portNumber -= GLOBAL_REGS_START_ADDR;

	if (portNumber > GLOBAL_REGS_START_ADDR)
		return GT_FALSE;

	val16 = value & 0xFFFF;

	if (GT_TRUE == mii_write(0x10 + portNumber, miiReg, &val16))
		return GT_TRUE;
	else  
		return GT_FALSE;
}
#endif

