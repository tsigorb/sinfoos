#include <VLAN\VLAN.h>
#ifdef __VLAN__

#include <Sinfo.h>

//extern unsigned short DataSave1[];	// !!!!!!!!!!!!!!!!!!!!!!!!! �������
//extern unsigned short DataSave[];	// !!!!!!!!!!!!!!!!!!!!!!!!! �������
extern unsigned int ReadSwitch (unsigned char DevAddr, unsigned char RegAddr);	//������ ���������� Marvell
extern unsigned short RdFlash(unsigned short AdrFl);
extern bool  WrFlash(unsigned short DanFl,unsigned short AdrFl);
extern bool Marvell;

 GT_QD_DEV   Marvelldev;
 GT_QD_DEV*  dev;
 GT_VTU_ENTRY vtuEntry;
 unsigned char PortLinked[6];

void ResetVLAN()
{
int i;
	LedOff(sigResetVLAN);
	vTaskDelay(100);
	LedOn(sigResetVLAN);
	vTaskDelay(100);
	for (i=0;i<6;i++) PortLinked[i]=0;
}

void   CheckVLAN(GT_U16 *aVLANID)
{
	unsigned short j;
    unsigned short DanMarv;
	dev = &Marvelldev;
	for (j=1;j<=4;j++)
	{
		if (gprtGetPhyReg(dev, j, 1, &DanMarv) == GT_OK)
		{
			  DanMarv = DanMarv & 0x0004;
			  if (DanMarv != PortLinked[j])
			  {
			  	gfdbFlush(dev, GT_FLUSH_ALL);
			  	PortLinked[j] = DanMarv;
			  }
		}
	}
}

/*
GT_STATUS   TestVLAN(GT_U16 *aVLANID)
{
	unsigned short j;
    unsigned short DanMarv;
	dev = &Marvelldev;
    
 	DanMarv = ReadSwitch(16,3);    
 	if (DanMarv != 0xFFFF)	//������ ������� ���������� Marvell
 	{
		dev = &Marvelldev;
 		if (dev->devName == DEV_88E6065) {
			gvlnGetPortVid(dev, vlan_port, &j); 
			if (j == (aVLANID[vlan_port] & 0x7FFF)) return GT_OK;
			else return GT_ERROR;
 		}
 		else return GT_ERROR;  // ���������� �� ����������������
 	}
 	else 
 	{
 	// ���� ���������� �� ����������, �� ���������� ���� ���� �� Flash
   		if (RdFlash(MarvellInfo) != 0xFFFF)
 	  		WrFlash(0xFFFF, MarvellInfo);
 		return GT_ERROR;
 	}
}
*/

GT_STATUS SetVLANID(GT_U16 *aVLANID, unsigned char *aPri)
{
	GT_STATUS status;
    GT_CABLE_STATUS cableStatus; 
    unsigned int ii;
    unsigned int i1, i2;
    int i3;
    unsigned short DanMarv;
    unsigned short vid[4]; 

 DanMarv = ReadSwitch(16,3);    
 if ((DanMarv != 0xFFFF) && (DanMarv != 0))	//������ ������� ���������� Marvell
 {
 	if (RdFlash(MarvellInfo) != DanMarv) WrFlash(DanMarv, MarvellInfo);
	dev = &Marvelldev;
	memset(dev, 0, sizeof(GT_QD_DEV));

	dev->devName = DEV_88E6065;
	dev->validPortVec = 0x3F;
	dev->validPhyVec  = 0x3F;
	dev->numOfPorts = 5;
	dev->maxPorts = 5;
	dev->maxPhyNum = 5;
	dev->fgtReadMii  = ffReadMii;
	dev->fgtWriteMii = ffWriteMii;
	dev->accessMode  = SMI_MANUAL_MODE;
	driverConfig(dev);
    for (ii=0; ii<dev->maxPorts; ii++) {
      gvlnSetPortVid(dev, ii, (aVLANID[ii] & 0x7FFF));   // vid=6
    	if (aVLANID[ii] & 0x8000) {	
			gvlnSetPortVlanDot1qMode(dev, ii, GT_SECURE);	//   	GT_DISABLE = 0, GT_FALLBACK, GT_CHECK, GT_SECURE mode=1
      	}
      	else
			gvlnSetPortVlanDot1qMode(dev, ii, GT_CHECK);	//   	GT_DISABLE = 0, GT_FALLBACK, GT_CHECK, GT_SECURE mode=1
		//	gvlnSetPortVlanDot1qMode(dev, ii, GT_SECURE);	//   	GT_DISABLE = 0, GT_FALLBACK, GT_CHECK, GT_SECURE mode=1
		gvlnSetPortVlanForceDefaultVID(dev, ii, GT_FALSE);	//  mode=1
		gvlnSetForceMap(dev, ii, GT_FALSE);
		gprtSetVTUPriOverride(dev, ii, PRI_OVERRIDE_FRAME);    // PRI_OVERRIDE_FRAME_QUEUE

        gqosSetDefFPri(dev, ii, aPri[ii]);
        gqosSetQPriValue(dev, ii, aPri[ii] / 2);  // QOS_TO_COS(aPri)
        gqosSetForceQPri(dev, ii, GT_TRUE); 
        
        // Default frame QoS to be taken from VLAN tag. If there's no tag, get priority from port's default QoS.
        // ��� ����� ������ �������. ��� ��� ���� ������������ ������� ����� �� ������ �� ����� ���������� QoS �� QoS �� ���������.
        hwSetPortRegField(dev, GT_LPORT_2_PORT(ii), QD_REG_PORT_CONTROL, 4, 2, 0x01); 
		
		// gvlnGetPortVid(dev, ii, &DataSave1[10+ii]);   // ################ �������!!!!!
    }
	gvlnSetNoEgrPolicy(dev, GT_TRUE);	// mode=1   ����� ���� �� ����
    // ��������� ����������� QoS->������������ ������� � ����� �� ��������� ������. ��� ���������� ���:
    // Set Qos to COS mapping
    hwWriteGlobalReg(dev, QD_REG_IEEE_PRI, 0x3210); // Switch global 1 register 0x18
    
	gvlnSetPortVlanDot1qMode(dev, 5, GT_SECURE);	//   	��������� ��������� ��� ����������� ���������� ����� 5
	/* 1) Flush VTU entries and check Entry Count */
	if(gvtuFlush(dev) != GT_OK) ;

	// ���������� ������ ���������� VLAN ID, � ��� ������� �� ��� ������� ������ � ��
	for (ii=0;ii<4;ii++) vid[ii]=0;
	for (i1=0;i1<5;i1++)
	{
//		if ((aVLANID[i1] & 0x8000)!=0)
		{
			i3=-1;
			for (i2=0;i2<4;i2++)
				if (vid[i2]==(aVLANID[i1] & 0x7FFF))
				{
					i3=i2;
					break;
				}
			if (i3<0)
				for (i2=0;i2<4;i2++)
					if (vid[i2]==0)
					{
						vid[i2]=(aVLANID[i1] & 0x7FFF);
						break;
					}
			
		}
	}

    for (i1=0;(i1<4)&&(vid[i1]!=0);i1++)
	{
		memset(&vtuEntry,0,sizeof(GT_VTU_ENTRY));

		vtuEntry.DBNum = i1+1;
		vtuEntry.vid = vid[i1];
	
		vtuEntry.vtuData.memberTagP[0] = MEMBER_EGRESS_UNTAGGED;
		vtuEntry.vidExInfo.useVIDFPri = GT_TRUE;
    	for (ii=1; ii<5; ii++) {
    		if ((aVLANID[ii] & 0x7FFF) == vid[i1]) {
    			if ((aVLANID[ii] & 0x8000)!=0)	
    			{
					vtuEntry.vtuData.memberTagP[ii] = MEMBER_EGRESS_TAGGED;
					vtuEntry.vidExInfo.vidFPri = aPri[ii];
    			}
				else 
					vtuEntry.vtuData.memberTagP[ii] = MEMBER_EGRESS_UNTAGGED; // MEMBER_EGRESS_UNMODIFIED;
    		}
			else	
				vtuEntry.vtuData.memberTagP[ii] = NOT_A_MEMBER;
    	}
		vtuEntry.vidExInfo.useVIDFPri = GT_TRUE;
		// vtuEntry.vidExInfo.vidFPri = aPri[ii];
		//
		//	3) Add a Entry.
		//
		if((gvtuAddEntry(dev,&vtuEntry)) != GT_OK) ;
		//
		//	4) check Entry Count, call EntryFirst and EntryNext
		//
		//	if((gvtuGetEntryCount(dev,(GT_U32 *)&DataSave1[10])) != GT_OK)  ;
	}
 }
 else {
 	// ���� ���������� �� ����������, �� ���������� ���� ���� �� Flash
	if (RdFlash(MarvellInfo) != 0xFFFF)
 	  WrFlash(0xFFFF, MarvellInfo);
 	  Marvell = false;
	return GT_ERROR;
 }
 Marvell = true;
 return GT_OK;
}
#endif // VLAN

