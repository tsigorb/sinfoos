#include "..\Sinfo.h"
#include "ViewCommandFunc.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include ".\RS485\KeyboardFn.h"

/////////////////////////////////////////////////////////////////////
#define d_AudioLvlCnt       (LenSport0+LenVirt)

static unsigned char s_btMasLvl[d_AudioLvlCnt*2];

/////////////////////////////////////////////////////////////////////
void GetAudioMidleLevel(unsigned char *pbtMasLvl,unsigned char btSz)
{
  static sCommand sCmd;
  static sInfoKadr sInfoS;

  sCmd.Source=UserViewCmd; // �������� ������� RS485
  sCmd.L=2; // ���������� ���� � �������
  sCmd.A1=0;
  sCmd.BufCommand[0]=10;  //-+ - ������� ������ ������� ������� ������� �� ������� � ������ �����������
  sCmd.BufCommand[1]=154; //-+
  if(xQueueSend(xQueueSportX,&sCmd,0,0)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(xQueueReceive(xQueueReply[UserViewCmd],&sInfoS,20))
    { // ������ �� ������� �������
      if(sInfoS.ByteInfoKadr[0]==11)
      {
        if(sInfoS.ByteInfoKadr[1]==154) memcpy(pbtMasLvl,sInfoS.ByteInfoKadr+2,btSz);
      }
    }
  }
}

unsigned char GetCPInfoFromMasLvl(ABONENT_STATE_TP *psAbnt,unsigned char *pbtBuf,unsigned char *btMasLvl)
{
  unsigned char btConnP;

  btConnP=(psAbnt->wConnectionPoint & 0xff);
  *pbtBuf++=psAbnt->btKey;
  *pbtBuf++=btConnP;
  btConnP--;
  *pbtBuf++=btMasLvl[LenSport0*2+btConnP*2];
  *pbtBuf=btMasLvl[LenSport0*2+btConnP*2+1];
  return(4);
}

void vCmdCP2SelectorInfo(unsigned char SN,sInfoKadr *psInfoS)
{
  unsigned char PultNb,btInd,btCnt,btOff;
  ABONENT_STATE_TP *psAbnt;

  PultNb=GetPultIndFromSN(SN);
  if((PultNb==0xff) || !IsSelectorActive(PultNb)) return;
  psInfoS->ByteInfoKadr[0]=111;
  psInfoS->ByteInfoKadr[1]=SN;
  GetAudioMidleLevel(s_btMasLvl,sizeof(s_btMasLvl));
  btInd=0; btCnt=0; btOff=3;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt)
    {
      psInfoS->ByteInfoKadr[2]=btCnt;
      psInfoS->L=btOff;
      xQueueSend(xQueueReply[UserUDP],psInfoS,0,0);
      return;
    }
    if(psAbnt->wConnectionPoint<0xfff)
    {
      if((psAbnt->btPultNb==PultNb) && (psAbnt->wConnectionPoint>0x100))
      {
        if(IsKey2Selector(PultNb,psAbnt->btKey,psAbnt->btLayer,d_Abonent2SelectorCurrentPult))
        {
          btOff+=GetCPInfoFromMasLvl(psAbnt,psInfoS->ByteInfoKadr+btOff,s_btMasLvl);
          btCnt++;
        }
      }
    }
    btInd++;
  }
  while(1);
}

/*void vCmdCP2ConferenceInfo(unsigned char SN,unsigned char btConf0,sInfoKadr *psInfoS)
{
  unsigned char PultNb,btInd,btCnt,btOff,btConf;
  ABONENT_STATE_TP *psAbnt;

  PultNb=GetPultIndFromSN(SN);
  if((PultNb==0xff) || !IsSelectorActive(PultNb)) return;
  psInfoS->ByteInfoKadr[0]=???;
  psInfoS->ByteInfoKadr[1]=SN;
  GetAudioMidleLevel(s_btMasLvl,sizeof(s_btMasLvl));
  btInd=0; btCnt=0; btOff=3;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt)
    {
      psInfoS->ByteInfoKadr[2]=btCnt;
      psInfoS->L=btOff;
      xQueueSend(xQueueReply[UserUDP],psInfoS,10,0);
      return;
    }
    if(psAbnt->wConnectionPoint<0xfff)
    {
      if((psAbnt->btPultNb==PultNb) && (psAbnt->wConnectionPoint>0x100))
      {
        if(IsKey2Conference(PultNb,psAbnt->btKey,psAbnt->btLayer,&btConf))
        {
          if(btConf0==btConf)
          {
            GetCPInfoFromMasLvl(psAbnt,psInfoS->ByteInfoKadr+btOff,s_btMasLvl);
            btCnt++;
          }
        }
      }
    }
    btInd++;
  }
  while(1);
}*/

void vCmdPultState(sCommand *psViewCommand,sInfoKadr *psInfoS1)
{
  int iRes;
  unsigned char i,PultNb,btIndex,CountAb,btState;
  unsigned short wChan;
  ABONENT_STATE_TP *psAbnt;
  static sAbntSelectorInfo sAbntInfo;

  switch(psViewCommand->BufCommand[1])
  {
    case 0: // ������ ��������� �������
      psInfoS1->L=6*NumberKeyboard+2;
      psInfoS1->ByteInfoKadr[0] = 99; // ����� 
      psInfoS1->ByteInfoKadr[1] = 0; // ����������
      for(i=0;i<NumberKeyboard;i++) 
      {
        psInfoS1->ByteInfoKadr[i*6+2]=MassM1[i]; // �������� �����
        psInfoS1->ByteInfoKadr[i*6+3]=IsKbdReady(i);
        psInfoS1->ByteInfoKadr[i*6+4]=GetKbdUnlockState(i) | 
                                      (GetLayerUnlockState(i,0)<<1) |
                                      (GetLayerUnlockState(i,1)<<2) |
                                      (GetLayerUnlockState(i,2)<<3);
        psInfoS1->ByteInfoKadr[i*6+5]=GetCurrentLayer(i); // ������� ���������
        switch(GetActiveSelectorState(i))
        {
          case Off:
            btState=0;
            break;
          case d_SelectorLoad:
            btState=1;
            break;
          case d_SelectorWait:
            btState=3;
            break;
          case d_SelectorWork:
            btState=2;
            break;
        }
        psInfoS1->ByteInfoKadr[i*6+6]=btState; // ��������� ������������ ��������� (0-2)
        psInfoS1->ByteInfoKadr[i*6+7]=IsSelectorRecord(MassM1[i]); // ��������� ����������� ���������
      }
      break;
    case 1: // ����������/������������� ������
      PultNb=psViewCommand->BufCommand[2];
      btState=psViewCommand->BufCommand[3];
      SetKbdUnlockState(PultNb,((btState & 0x1)==0));
      PutKbdUnlockLedState(PultNb,0xff);
      SetLayerUnlockState(PultNb,0,((btState & 0x2)==0));
      SetLayerUnlockState(PultNb,1,((btState & 0x4)==0));
      SetLayerUnlockState(PultNb,2,((btState & 0x8)==0));
      IndicateLayerKey(PultNb);
      psInfoS1->L = 3;
      psInfoS1->ByteInfoKadr[0] = 99; // ����� 
      psInfoS1->ByteInfoKadr[1] = 1; // ����������
      psInfoS1->ByteInfoKadr[2] = CodeStatusTrue;
      break;
    case 2: // ������������ ��������� ������
/*      SwitchGroup(psViewCommand->BufCommand[2],psViewCommand->BufCommand[3]);
      psInfoS1->L = 3;
      psInfoS1->ByteInfoKadr[0] = 99; // ����� 
      psInfoS1->ByteInfoKadr[1] = 2; // ����������
      psInfoS1->ByteInfoKadr[2] = CodeStatusTrue;*/
      break;
    case 3: // ������ ������ ��������� ������
      PultNb=psViewCommand->BufCommand[2];
      CountAb=0;
      i=0;
      do
      {
        psAbnt=GetAbonentStatePtrFromIndex(i);
        if(!psAbnt) return;
        if((psAbnt->wConnectionPoint<0xfff) && (psAbnt->btPultNb==PultNb))
        { // ���� �������
          psInfoS1->L=13;
          psInfoS1->ByteInfoKadr[0]=99; // ����� 
          psInfoS1->ByteInfoKadr[1]=3; // ����������
          psInfoS1->ByteInfoKadr[2]=MassM1[PultNb]; // �������� �����
          psInfoS1->ByteInfoKadr[3]=CountAb; // ����� ������
          psInfoS1->ByteInfoKadr[4]=psAbnt->wConnectionPoint >> 8;
          psInfoS1->ByteInfoKadr[5]=psAbnt->wConnectionPoint; // ����� ������
          psInfoS1->ByteInfoKadr[6]=psAbnt->NumbAbon; // ����� ��������
          psInfoS1->ByteInfoKadr[7]=psAbnt->wOperatorChanel >> 8;
          psInfoS1->ByteInfoKadr[8]=psAbnt->wOperatorChanel;  // ����� ������ ��-��
          psInfoS1->ByteInfoKadr[9]=GetAbonentStateFromPtr(psAbnt);
          psInfoS1->ByteInfoKadr[10]=psAbnt->btType;
          psInfoS1->ByteInfoKadr[11]=psAbnt->btKey;
          psInfoS1->ByteInfoKadr[12]=psAbnt->btLedState;
          if(xQueueSend(xQueueReply[psInfoS1->Source],psInfoS1,0,0)==errQUEUE_FULL)
          { CountErrQueueReply++; return; }
          CountAb++;
        }
        i++;
      }
      while(1);
    case 4: // ������ ������ ������� ����
      psInfoS1->L=2;// +CountCallDTMF*5;
      psInfoS1->ByteInfoKadr[0] = 99; // ����� 
      psInfoS1->ByteInfoKadr[1] = 4; // ����������
/*      CountAb = 0;
      for(i=0;i<LengthMassDTMF;i++)
      {
        if(MassCallDTMF[i].NChan1)
        { // ���� �����  !!! ��� ����� ��-������� !!!
          psInfoS1->ByteInfoKadr[CountAb*5+2] = MassCallDTMF[i].NChan1 >> 8;  // ����� ������ ��-�� 
          psInfoS1->ByteInfoKadr[CountAb*5+3] = MassCallDTMF[i].NChan1;  // ����� ������
          psInfoS1->ByteInfoKadr[CountAb*5+4] = MassCallDTMF[i].NumbAbon; // ����� ��������
          psInfoS1->ByteInfoKadr[CountAb*5+5] = MassCallDTMF[i].NChan2 >> 8;  // ����� ������ ��-��
          psInfoS1->ByteInfoKadr[CountAb*5+6] = MassCallDTMF[i].NChan2;  // ����� ������ ��-��
          CountAb++;
        }
      }*/
      break;
    case 5: // ������ ���������� �����������
      PultNb = psViewCommand->BufCommand[2];
      psInfoS1->ByteInfoKadr[0] = 99; // ����� 
      psInfoS1->ByteInfoKadr[1] = 5; // ����������
      psInfoS1->ByteInfoKadr[2] = MassM1[PultNb]; // �������� ����� ������
      CountAb = 0;
      psInfoS1->ByteInfoKadr[3]=CountAb; // ���������� ����������� �� ������
      btIndex = 4;
/*      for(i=0;i<NumberKonf;i++) 
        if(YesKonf[PultNb][i]) CountAb++;
      for(i=0;i<NumberKonf;i++) 
      {
        if(YesKonf[PultNb][i])
        { // ���� �����������
          psInfoS1->ByteInfoKadr[btIndex]=i+1; // ����� �����������
          btIndex+=2;
          CountAb=0;
          for(j=0;j<NumberChanActive;j++)
          {
            NumbCh_Ind=*GetKonfChanAddr(PultNb,i,j);
            if(NumbCh_Ind)
            {
              psInfoS1->ByteInfoKadr[btIndex++]=NumbCh_Ind >> 8; // ����� ������ ��-�� 
              psInfoS1->ByteInfoKadr[btIndex++]=NumbCh_Ind; // ����� ������
              CountAb++;
              if(btIndex>250) break;
            }
          }
          psInfoS1->ByteInfoKadr[btIndex-CountAb*2-1]=CountAb; // ���������� ��������� ����������� i
        }
        if(btIndex>250) break;
      }*/
      psInfoS1->L=btIndex; // ����� �������
      break;
    case 6: // ������ �������� ������������ ���������
      PultNb=psViewCommand->BufCommand[2];
      if(!IsSelectorActive(PultNb)) return;
      psInfoS1->L = 8;
      psInfoS1->ByteInfoKadr[0]=99; // ����� 
      psInfoS1->ByteInfoKadr[1]=6; // ����������
      psInfoS1->ByteInfoKadr[2]=MassM1[PultNb]; // �������� �����
      psInfoS1->ByteInfoKadr[3]=GetSelectorNumb(PultNb)+1; //  ����� �������� 
      psInfoS1->ByteInfoKadr[4]=1; //VarScen[PultNb]; // �������
      wChan=GetSelectorOperator(PultNb);
      psInfoS1->ByteInfoKadr[5]=wChan >> 8;  // ����� ���������
      psInfoS1->ByteInfoKadr[6]=wChan; // ������������ ���������
      psInfoS1->ByteInfoKadr[7]=GetSelectorMemberCount(PultNb); // ���������� ���������� ������������ ���������
      break;
    case 7: // ������ ������ ���������� ������������ ���������
      PultNb=psViewCommand->BufCommand[2];
      if(!IsSelectorActive(PultNb)) return;
      psInfoS1->L=40;
      psInfoS1->ByteInfoKadr[0]=99; // ����� 
      psInfoS1->ByteInfoKadr[1]=7; // ����������
      psInfoS1->ByteInfoKadr[2]=MassM1[PultNb]; // �������� �����
      CountAb=0;
      for(i=0;i<0xff;i++)
      {
        iRes=GetAbntSelectorInfoFromIndex(PultNb,i,&sAbntInfo);
        if(iRes<0) return;
        if(iRes>0)
        {
          psInfoS1->ByteInfoKadr[3]=CountAb; // ����� ������
          memcpy(psInfoS1->ByteInfoKadr+4,sAbntInfo.Name,32); // ��� ��-��
          psInfoS1->ByteInfoKadr[36]=sAbntInfo.wConnectionPoint >> 8;  // ����� ������ 
          psInfoS1->ByteInfoKadr[37]=sAbntInfo.wConnectionPoint;  // ����� ������
          psInfoS1->ByteInfoKadr[38]=sAbntInfo.btNumb; // ����� ��������� (�� ����� �������?) 
          psInfoS1->ByteInfoKadr[39]=sAbntInfo.btStatus; // ������ ���������
          if(xQueueSend(xQueueReply[psInfoS1->Source],psInfoS1,0,0)==errQUEUE_FULL)
          { CountErrQueueReply++; return; }
          CountAb++;
        }
      }
      return;
    case 8: // ������ ����� ����������� ���������� ������������ ���������
      PultNb = psViewCommand->BufCommand[2];
      if(!IsSelectorActive(PultNb)) return;
      psInfoS1->ByteInfoKadr[0]=99; // ����� 
      psInfoS1->ByteInfoKadr[1]=8; // ����������
      CountAb=0; btIndex=3;
      for(i=0;i<0xff;i++) 
      {
        iRes=GetAbntSelectorInfoFromIndex(PultNb,i,&sAbntInfo);
        if(iRes<0) break;
        if((iRes>0) && (sAbntInfo.wConnectionPoint>0x100))
        {
          psInfoS1->ByteInfoKadr[btIndex++]=sAbntInfo.wConnectionPoint; // ����� ������
          psInfoS1->ByteInfoKadr[btIndex++]=sAbntInfo.btStatus; // ������ ���������
          CountAb++;
        }
      }
      psInfoS1->ByteInfoKadr[2]=CountAb;
      psInfoS1->L=btIndex;
      break;
    default:
      return;
  }
  if(xQueueSend(xQueueReply[psInfoS1->Source],psInfoS1,0,0)==errQUEUE_FULL) CountErrQueueReply++;
}

