#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"

//*******************************************************************
xQueueHandle xQueueCommand; // �� ��������� ������
xQueueHandle xQueueTFTP;
xQueueHandle xQueueReply[QueueReplyCnt]; // ������� ��  �������
xQueueHandle xRxedChars; // UART Rx 
xQueueHandle xQueueSportX;
xQueueHandle xKeyProcessing; // ������� �������� ������� ������ �� ���������
xQueueHandle xPort485RX;
xQueueHandle xKeyIndic; // ������� �������� ��� ��������� �� ����������
xQueueHandle xQueueVxCall; // ������� �������� �������
xQueueHandle xQueueISDNCall; // ������� ��� ������� ���������� ���� ISDN
xQueueHandle xQueueAnswYuri;
xQueueHandle xQueueLogger;
xQueueHandle xQueueSoundIpIn; // ������� ��� ������ �������� ������� ����� IP
xQueueHandle xQueueSoundIpOut; // ������� ��� �������� UNICAST �������� ������� ����� IP

/////////////////////////////////////////////////////////////////////
// �������� �������� ��� �����
void CreateQueueForTask(void)
{
  short i;
  unsigned portBASE_TYPE MasQueueReplySz[QueueReplyCnt]=
{
  1,                 // UserNU1
  5,                 // UserSNMP
  25*NumberKeyboard, // UserRS485
  200,               // UserSportX
  5,                 // UserLoaderMKA
  1,                 // UserNU2
  100*NumberKeyboard, // UserUDP
  5*NumberKeyboard   // UserViewCmd
};

  xQueueCommand=xQueueCreate(500,sizeof(sCommand));
  if(!xQueueCommand) BlockWatchDog();
  xQueueTFTP=xQueueCreate(10,sizeof(sCommand));
  if(!xQueueTFTP) BlockWatchDog();
  for(i=0;i<QueueReplyCnt;i++)
  {
    xQueueReply[i]=xQueueCreate(MasQueueReplySz[i],sizeof(sInfoKadr));
    if(!xQueueReply[i]) BlockWatchDog();
  }
  xQueueSportX=xQueueCreate(200,sizeof(sCommand));
  if(!xQueueSportX) BlockWatchDog();
  xRxedChars=xQueueCreate(1,d_UARTRcvBufSz);
  if(!xRxedChars) BlockWatchDog();
  //�������� ������� �� ������ �� ������ ������� ������ LengthRX485
  xPort485RX=xQueueCreate(1,LengthRX485*sizeof(unsigned portCHAR));
  if(!xPort485RX) BlockWatchDog();
  //�������� ������� ��� ��������� �� ���������� �� 4 �����
  xKeyIndic=xQueueCreate(NumberKeyboard*125,sizeof(sIndic));
  if(!xKeyIndic) BlockWatchDog();
  //�������� ������� ������� ������ �� ���������
  xKeyProcessing=xQueueCreate(NumberKeyboard*5,sizeof(PRESS_KEY_TP));
  if(!xKeyProcessing) BlockWatchDog();
  // �������� ������� �������� �������
  xQueueVxCall=xQueueCreate(MAX_MKA_CHANNELS*10,sizeof(sInfoKadr));
  if(!xQueueVxCall) BlockWatchDog();
  xQueueAnswYuri=xQueueCreate(1,sizeof(sCommand));
  if(!xQueueAnswYuri) BlockWatchDog();
  xQueueLogger=xQueueCreate(100,sizeof(sInfoKadr));
  if(!xQueueLogger) BlockWatchDog();
  // �������� ������� �������� �������� �������
  xQueueSoundIpIn=xQueueCreate(NumberKeyboard*10,sizeof(xListRTP));
  if(!xQueueSoundIpIn) BlockWatchDog();
  // �������� ������� ��������� �������� ������� UNICAST
  xQueueSoundIpOut=xQueueCreate(NumberKeyboard*10,sizeof(sSoundIp));
  if(!xQueueSoundIpOut) BlockWatchDog();
}

