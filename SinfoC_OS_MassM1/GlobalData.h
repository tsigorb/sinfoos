// FOR DEBUG
extern unsigned short DataSave1[];

//*******************************************************************
extern ERROR_COUNTERS_TP g_sErrCntrs;

extern const char *g_strPusto;
extern const char *g_strCallName[];

extern unsigned short ReversA[LenSport0]; // ������� ��������
extern unsigned int TestBoard; // ������� ����������������� ������� ���

extern short CountPaket; //������� �������� �������
extern short CountPaketFull; //������� �������� ������ �������
extern short CountPaketSendFull; //������� ���������� ������ �������
extern short CountPaketSend; //������� ���������� �������
extern short GetPaketNewPult[NumberKeyboard]; // ������� �������� �������
extern short SendPaketNewPult[NumberKeyboard]; // ������� ���������� �������


extern short ErrMallocGener; // ������ �������������� ������ ��� �������� ������� ���������
extern short ErrMallocAnal; // ������ �������������� ������ ��� �������� ������� ���������� �������
extern short ErrMallocE1; // ������ �������������� ������ ��� �������� ������� ������ ��� E1
extern short ErrMallocAns; // ������ �������������� ������ ��� �������� ������� ������ � ������� ���������
extern short ErrMallocCom; // ������ �������������� ������ ��� �������� ������� ������ ���������� ���
extern short ErrMallocRecord; // ������ �������������� ������ ��� �������� ������� �����������
extern short ErrMallocPlay; // ������ �������������� ������ ��� �������� ������� ��������������������
extern short ErrMallocSpec; // ������ �������������� ������ ��� �������� ������� ��������� ����������
extern short ErrQueueAns; // ������������ ������� ������ � ������� ���������
extern short ErrQueueSNMP; // ������������ ������� ������ SNMP
extern short ErrQueueEDSS; // ������������ ������� ������ EDSS
extern short ErrAnswerE1; // ���������� ������ ������ �1
extern short ErrSwapE1; // ������ ������ � ������� �1
extern short Err5ms; // ������ ��������� ������� ��������� � ������� 5 ��
extern short ErrSPORT0; // ������ ������ �� SPORT0
extern short CountErr; // ������ �������

extern short Err_Flash;
extern short ErrReadF;

extern short CountErrPaket; //������� ������� ��� ������
extern short Count50Err;
extern short CountErrReadBuf;
extern short CountErrSendKeyPr;
extern short CountNoReply;
extern short CountErrSendIndic;  // ������ ���������� � ������� �� ���������
extern short CountErrSendQueueCommand;
extern short CountErrMalloc; // ������� ������ ��� ��������� ������
extern short CountErrEDSS; // ������ ��� ������ ������ EDSS  
extern short CountErrCommute; // ������ ���������� 
extern short CountErrCallMemISDN; // ������ ���������� � ������� ������� EDSS  
extern short CountErrQueueReply; // ������� xQueueReply - �����������
extern short CountErrLoadCoeffBK;
extern short CountErrLoadPgmBK;
extern short CountErrNoReplyUart;
extern short CountErrQueueUart;
extern short CountErrCommand;
extern short CountErrNoCommand;
extern short Count50;
extern short Count50R;
extern short CountBlock;
extern short ErrReadF;
extern short CountErrMalloc1; // ������� ������ ��� ��������� ������ heap1
extern unsigned short MassErrLoadSel[NumberSelector]; // ������ �������� ���������

extern unsigned portCHAR MassTel[NumberDirectAbon][MAX_DIGIT_TLFN]; // ������ ������ ��������� ���
extern sKBKey MassKey[NumberKeyboard][NumberGroup][NumberKey];
extern unsigned portCHAR MassM1[NumberKeyboard];//    ������ �������� ������� ���������

extern sListAb ListAbntDisplay[NumberKeyboard][2]; // �� 2 ��������� �� ������ ����� ��� ���������� ���������� ��������� �� �������

extern unsigned int MassTimer[NumberTimer]; // ������ ��������
//extern unsigned short g_wISDNPrefix;
extern unsigned short NumbTabl; // ����� ������� ���������� ��� ��

extern xQueueHandle xQueueCommand; // �� ��������� ������
extern xQueueHandle xQueueTFTP;
extern xQueueHandle xQueueReply[QueueReplyCnt]; // ������� �� �������
extern xQueueHandle xQueueISDNCall; // ������� ��� ������� ���������� ���� ISDN 
extern xQueueHandle xQueueVxCall; // ������� �������� �������
extern xQueueHandle xKeyIndic; // ������� �������� ��� ��������� �� ����������
extern xQueueHandle xPort485RX; // ������� ��� ������ �� RS485
extern xQueueHandle xRxedChars; // ������� ��� ������ �� ��� 
extern xQueueHandle xQueueSportX;
extern xQueueHandle xQueueLogger;
extern xQueueHandle xQueueSoundIpIn; // ������� ��� ������ �������� ������� ����� IP
extern xQueueHandle xQueueSoundIpOut; // ������� ��� �������� UNICAST �������� ������� ����� IP

extern xSemaphoreHandle g_SPILock;

