//*****************************************************
// !!! TSIGORB DEBUG ONLY !!!
void DEBUG_Pisk(unsigned char PultNb,char chCntPisk);
//*****************************************************

char *GetNextStr(char *pBuf);
unsigned int atoIP(char *pBuf);

bool IsPultConfigRead(void);

bool IsNeedDestroyAllConnections2Pult(unsigned char PultNb);
unsigned char GetPultIndFromSN(unsigned char SN);

void WriteAddrMU(unsigned char year, unsigned char mon, unsigned char day);
unsigned char FoundPoint(unsigned char chan, unsigned char busy);
void SoundKeep(unsigned short NChan); // ��������� ������� �������� �� ���������
void StopKeep(unsigned short NChan); // ���������� ������� �������� �� ���������
bool ListenDispatcher(unsigned short ChanTechnic,unsigned short ChanDS,unsigned short ChanRS,unsigned char Incl);
bool IsCommute(unsigned short chan1,unsigned short chan2);
void ResetIPPults(void);
bool IsLevelPresent(unsigned short NChan);

void transmit_enable_rs485(void);
void transmit_disable_rs485(void);
bool IsRS485Busy(void);
void SetRS485Busy(bool bBusy);
void SetRS485LastByte(void);
void ClearNeedNextByteRS485(void);
bool WaitNeedNextByteRS485(unsigned int dwTOut);

void CreateErrInfo(unsigned char CodeCommand, unsigned char CodeErr, sInfoKadr *pxInfoS);
bool xUARTPutCmd(unsigned portCHAR *cOutChar);

bool GetControlIpAddr(unsigned int *pdwIpAddr, unsigned short *pdwIpPort);
bool GetAddressIpPult(unsigned char SN,unsigned int *pdwIP,unsigned short *pwPort);
void OutSNMP(unsigned char NMod, unsigned char Port, unsigned char Cod);

unsigned short RdFlash(unsigned short AdrFl);
bool WrFlash(unsigned short DanFl,unsigned short AdrFl);

unsigned char GetPointPult(void);
unsigned char DelPointPult(unsigned char Point);

char Tlfn2Str(char *pStr,unsigned char *pTlfn);
char Str2Tlfn(unsigned char *pTlfn,char *pStr);
bool TlfnEqual(unsigned char *pbtTlfn1,unsigned char *pbtTlfn2);

void SetNeedIpReinit(void);
void SendIpReinitCount2IpPult(unsigned char SN,sInfoKadr *psInfo);

signed portBASE_TYPE xQueueSendCmd(sCommand *psCmd,portTickType xTicksToWait);

void InitIRP(void);
bool IsNeedIpReinit(void);
unsigned char GetCodeFromSndCP(unsigned char btCP);
bool GetControlIpAddr(unsigned int *pdwIpAddr, unsigned short *pdwIpPort);
unsigned char FindIpReg(unsigned char btType,
                        unsigned int *pA1,unsigned short *pPORT1,
                        unsigned char *pbtCnt,unsigned char *pbtIndFree);
void IRPTOut(void);
void IRP_Registration(unsigned char btType,unsigned int A1,unsigned short PORT1,unsigned char *pbtMAC);
void IRP_Unregistration(unsigned char btType,unsigned int A1);
void IRP_Check(unsigned char btType,unsigned int A1);
void _xIpPultLife(unsigned char btSN);
bool IRP_Cmd(unsigned char *pbtBufCmd,unsigned char btLn);
bool GetAddressIpPult(unsigned char SN,unsigned int *pdwIP,unsigned short *pwPort);
unsigned char GetSNFromCP(unsigned short wChan);

void SendReinitCount2Ip(void);
void ReOpenSocket(unsigned char btSocket,unsigned short wPort,bool bSend);

void ClearMKAUartState(unsigned char btMKA);
void PeriodicalSendUartMKACmd200(void);
void TestUart4MKA(unsigned char btMKA);
void ClearTestUartCallFromMKA(unsigned char btMKA);

