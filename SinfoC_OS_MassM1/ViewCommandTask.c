#include "..\Sinfo.h"
#include "ViewCommandFunc.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include ".\RS485\KeyboardFn.h"

/////////////////////////////////////////////////////////////////////
void LedWorkIndicator(void);

void InitRS485(void);
void InitUART(void);

void vIPTask(void *pvParameters);
void vTFTPTask(void *pvParameters);
void vCheckAndMKATask(void *pvParameters);

#ifdef SINFO_ENERGO
void vCallProcess(void *pvParameters);
#else
void vRS485TaskR_T(void *pvParameters);  // ����� ���������, ��������� � �������� ����
void vTaskCall(void *pvParameters);
#endif

void InitTaskSport(void); 

void LoadedMKA(void);

void vProcessJornal(sInfoKadr *psInfoS1,int aSize, unsigned char *BufCommand);
void DetectSpansError(void);

/////////////////////////////////////////////////////////////////////
extern xQueueHandle xKeyProcessing; // ������� �������� ������� ������ �� ���������

/////////////////////////////////////////////////////////////////////
section("bigdata") unsigned char TraceBuf[40000]; // �����������

static bool s_bMasNeedDestroyConn[NumberKeyboard];

/////////////////////////////////////////////////////////////////////
bool IsNeedDestroyAllConnections2Pult(unsigned char PultNb)
{
  if(s_bMasNeedDestroyConn[PultNb])
  {
    s_bMasNeedDestroyConn[PultNb]=false;
    return(true);
  }
  return(false);
}

void vCmdNeedDestroyAllConnections2Pult(unsigned char SN)
{
  unsigned char PultNb;

  if(SN==0xff)
  {
    for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
    {
      if(MassM1[PultNb]) s_bMasNeedDestroyConn[PultNb]=true;
    }
  }
  else
  {
    for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
    {
      if(SN==MassM1[PultNb])
      {
        s_bMasNeedDestroyConn[PultNb]=true;
        return;
      }
    }
  }
}

void vCmdKeyFromIpPult(unsigned char SN,unsigned char btKey,unsigned char btState)
{
  unsigned char PultNb;
  PRESS_KEY_TP sKeyPr;

  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if(SN==MassM1[PultNb])
    {
      sKeyPr.SN=SN;
      sKeyPr.CodeKey[1]=btKey;
      if(btState<2)
      {
        sKeyPr.CodeKey[0]=btState;
        xQueueSend(xKeyProcessing,&sKeyPr,0,0);
      }
      else
      {
        sKeyPr.CodeKey[0]=0;
        xQueueSend(xKeyProcessing,&sKeyPr,0,0);
        sKeyPr.CodeKey[0]=1;
        xQueueSend(xKeyProcessing,&sKeyPr,0,0);
      }
      return;
    }
  }
}

// ������������ ��������� � �������
void CreateErrInfo(unsigned char codeCmd, unsigned char CodeErr, sInfoKadr *pxInfoS)
{
  pxInfoS->L=2;
if(codeCmd==15) codeCmd=16; // �� ���������� ���� ����������� ��� ������� � ��� ������!!!
  pxInfoS->ByteInfoKadr[0]=codeCmd;
  pxInfoS->ByteInfoKadr[1]=CodeErr;
  if(xQueueSend(xQueueReply[(pxInfoS->Source)],pxInfoS,0,0)==errQUEUE_FULL) CountErrQueueReply++;
}

void SendTime2Pult(sInfoKadr *pxInfoS,unsigned char SN)
{
}

void InitAndCreateTask(void)
{
  InitSPI(); // ����������� ����� InitRS485()
  InitRS485();
  InitUART();
  InitRTC();
  InitFRAM();
  if(!InitFileSystem()) BlockWatchDog();

#ifdef SINFO_ENERGO
  AddToLogWithInfo(lt_Notification,ls_OS,lc_TaskManager,"SinfoC_OS_Energo started!");
#else
  AddToLogWithInfo(lt_Notification,ls_OS,lc_TaskManager,"SinfoC_OS_MassM1 started!");
#endif

  if(xTaskCreate(vIPTask,"TCPIP",
                 STACK_TCPIP,NULL,tskIDLE_PRIORITY+2,&thTcpIp)!=pdPASS)
  { BlockWatchDog(); }

  if(xTaskCreate(vTFTPTask,"TFTP",
                 STACK_TFTP,NULL,tskIDLE_PRIORITY+1,&thTFTP)!=pdPASS)
  { BlockWatchDog(); }

  if(xTaskCreate(vCheckAndMKATask,"MKATask",
                 STACK_CheckMKATask,NULL,tskIDLE_PRIORITY+1,&thCheckTask)!=pdPASS)
  { BlockWatchDog(); }

#ifdef SINFO_ENERGO
  //�������� ������ ��������� �������
  //(��� ���� ������ ����� �� IP �� ��������, �.�. �� ������� ����.�������)
  if(xTaskCreate(vCallProcess,"CallProc",
                 STACK_TaskCall,NULL,tskIDLE_PRIORITY+1,&thTaskCall)!=pdPASS)
  { BlockWatchDog(); }
#else
  //�������� ������ ������ ��������� � ���������
  if(xTaskCreate(vRS485TaskR_T,"RS485_R",
                 STACK_RS485,NULL,tskIDLE_PRIORITY+2,&thRS485)!=pdPASS)
  { BlockWatchDog(); }
  
  //�������� ������ ��������� �������, ������ � ������� � ...
  //(��� ���� ������ ����� �� IP �� ��������, �.�. �� ������� ����.�������)
  if(xTaskCreate(vTaskCall,"TaskCall",
                 STACK_TaskCall,NULL,tskIDLE_PRIORITY+1,&thTaskCall)!=pdPASS)
  { BlockWatchDog(); }
#endif
  InitTaskSport(); // ������������� SportX, �������� ����� �� ��������� ����������
}

// ��������� ������, ����� �� �������
void vViewCommandTask(void *pvParameters)
{
  unsigned char codeCmd,
                *pxContinOutCommand; // ��������� �� ����� ����� � ByteInfoKadr ��� ������ ������ �� ���
  unsigned short i,DanFl;
  bool bSend2Bk;
  struct tm time1,*ptime;
  time_t tt;

  static sCommand sCmd;
  static sInfoKadr sInfoS1;
  static unsigned char CommandPort0[260];

  InitAndCreateTask();

  while(1)
  {
//LedWorkIndicator();
    PeriodicalSendUartMKACmd200();
    if(xQueueReceive(xQueueCommand,&sCmd,0)) 
    {
      pxContinOutCommand=NULL;
      bSend2Bk=false;
      sInfoS1.Source=sCmd.Source;
      sInfoS1.A1=sCmd.A1;
      sInfoS1.PORT1=sCmd.PORT1;
      codeCmd=sCmd.BufCommand[0]; // ��� �������
      switch(codeCmd)
      {
        case 1: // ��������� ����� ��������
          if(sCmd.BufCommand[3]==CallInduct)
          { // ����������� �����
            bSend2Bk=vCmdOutInductor(CommandPort0,&sInfoS1,codeCmd,sCmd.BufCommand[2]-1);
            if(bSend2Bk) pxContinOutCommand=sInfoS1.ByteInfoKadr+1;
          }
          else
          {
            if(xQueueSend(xQueueSportX,&sCmd,0,0)==errQUEUE_FULL) CountErrQueueReply++;
          }
          break;
        case 5: // ���������� ����� ������
        case 6: // ��������� ����� ������
          CommandPort0[0]=160+codeCmd-5;
          CommandPort0[1]=sCmd.L+1; // ����� �������
          CommandPort0[2]=sCmd.BufCommand[2]; // � �������.
          memcpy(CommandPort0+3,sCmd.BufCommand+3,sCmd.L-1); // ����� ��������
          CommandStand(&sInfoS1,codeCmd,Tranzit);
          pxContinOutCommand=sInfoS1.ByteInfoKadr+1;
          break;
        case 7: // ������ ������ � ������ �� ����������� ������
//SendStr2IpLogger("$DEBUG$ReadCallInfo: channel #%d",sCmd.BufCommand[1]+1);
          sInfoS1.L=6;
          CommandPort0[0]=2; 
          CommandPort0[1]=3;
          CommandPort0[2]=sCmd.BufCommand[1];
          sInfoS1.ByteInfoKadr[0]=8;
          sInfoS1.ByteInfoKadr[1]=CommandPort0[2]; 
          pxContinOutCommand=sInfoS1.ByteInfoKadr+2;
          break;
        case 10:
          if(xQueueSend(xQueueSportX,&sCmd,0,0)==errQUEUE_FULL) CountErrQueueReply++;
          break;
        case 15: // ������ ���������� ����������� ������
          if(sCmd.BufCommand[1])
          { // ����� �����������
            if(xQueueSend(xQueueSportX,&sCmd,0,0)==errQUEUE_FULL) CountErrQueueReply++;
          }
          else
          {
            CommandPort0[0]=19; 
            CommandPort0[1]=3; // ����� �������
            CommandPort0[2]=sCmd.BufCommand[2];
            sInfoS1.L=18;
            sInfoS1.ByteInfoKadr[0]=16;
            sInfoS1.ByteInfoKadr[1]=0;
            sInfoS1.ByteInfoKadr[2]=sCmd.BufCommand[2];
            pxContinOutCommand=sInfoS1.ByteInfoKadr+3;
          }
          break;
        case 17: // ������ ���������� ���
          CommandPort0[1]=3; // ����� �������
          CommandPort0[2]=63; // �����
          if(!sCmd.BufCommand[3])
          { // �������� 
            CommandPort0[0]=16; 
            sInfoS1.L=5;
          }
          else
          { // �������
            CommandPort0[0]=17; 
            sInfoS1.L=11;
          }
          sInfoS1.ByteInfoKadr[0]=18;
          sInfoS1.ByteInfoKadr[1]=0; // 
          sInfoS1.ByteInfoKadr[2]=63; // �����
          sInfoS1.ByteInfoKadr[3]=sCmd.BufCommand[3];
          pxContinOutCommand=sInfoS1.ByteInfoKadr+4;
          break;
        case 19 : // ���������/������ �������� "�������" ��� ���������
          CommandPort0[0]=57;
          CommandPort0[1]=4; // ����� �������
          CommandPort0[2]=sCmd.BufCommand[2]; // � �������.
          CommandPort0[3]=sCmd.BufCommand[3]; // �������
          CommandStand(&sInfoS1,codeCmd,Tranzit);
          pxContinOutCommand=sInfoS1.ByteInfoKadr+1;
          break;
        case 20: // ��������� ���� ������ �������
          bSend2Bk=vCmdSetChanelType(CommandPort0,&sCmd,&sInfoS1,codeCmd);
          if(!sCmd.BufCommand[1])
          {
            if(bSend2Bk) pxContinOutCommand=sInfoS1.ByteInfoKadr+1;
          }
          break;
        case 21: // ��������� ���������� ������ (�����)
        case 22: // ��������� ������������ �������� ������
        case 23: // ��������� ������������ ��������� ������
        case 24: // ��������� ������ ���������������� ���
        case 25: // ��������� ������ ��������������
        case 26: // ��������� ���������� ������� ��������������
        case 27: // ��������� ������ ������� ������� ������
        case 30: // ��������� ���������� ������ ���������� ������� ��������������
          bSend2Bk=vCmdSetParametrs(CommandPort0,&sCmd,&sInfoS1,codeCmd);
          if(bSend2Bk) pxContinOutCommand=sInfoS1.ByteInfoKadr+1;
          break;
        case 28: // ��������� ������ �������� �� 4-� ���������� ������
          bSend2Bk=vCmdSetReturnCoef(CommandPort0,&sCmd,&sInfoS1,codeCmd);
          if(!sCmd.BufCommand[1])
          {
            if(bSend2Bk) pxContinOutCommand=sInfoS1.ByteInfoKadr+1;
          }
          break;
        case 29: // ��������� ������ ������� ������� � ������ "�� ������"
          bSend2Bk=vCmdSetAnalPorog(CommandPort0,&sCmd,&sInfoS1,codeCmd);
          if(!sCmd.BufCommand[1])
          {
            if(bSend2Bk) pxContinOutCommand=sInfoS1.ByteInfoKadr+1;
          }
          break;
        case 31: // ���������� ���������� ����� ���
          if((sCmd.BufCommand[3]==1) || (sCmd.BufCommand[3]==2))
          {
            CommandPort0[0]=50; 
            CommandPort0[1]=5; // ����� �������
            CommandPort0[2]=sCmd.BufCommand[2]; // � �������.
            CommandPort0[3]=sCmd.BufCommand[3]; // ����������
            CommandPort0[4]=0; 
            CommandStand(&sInfoS1,codeCmd,Tranzit);
            pxContinOutCommand=sInfoS1.ByteInfoKadr+1;
          }
          else CommandStand(&sInfoS1,codeCmd, CodeStatusFalse); // ��������� ������
          break;
        case 32: // ��������� � ��������� ��������������
          if(sCmd.BufCommand[3]==1) WrFlash(1,AdrFiltrEcho+sCmd.BufCommand[2]*2);
          CommandPort0[0]=28; 
          CommandPort0[1]=4; // ����� �������
          CommandPort0[2]=sCmd.BufCommand[2]; // � �������.
          CommandPort0[3]=sCmd.BufCommand[3]; // � ��������
          sInfoS1.L=NEcho*2+5;
          sInfoS1.ByteInfoKadr[0]=32;
          sInfoS1.ByteInfoKadr[1]=0;
          sInfoS1.ByteInfoKadr[2]=sCmd.BufCommand[2]; // � �������.
          sInfoS1.ByteInfoKadr[3]=sCmd.BufCommand[3]; // � ��������
          pxContinOutCommand =sInfoS1.ByteInfoKadr+4; //��������� �� ����� ����������� ������ �������
          break;
        case 33: // ���������/���������� ��������������
          bSend2Bk=vCmdEchoOnOff(CommandPort0,&sCmd,&sInfoS1,codeCmd);
          if(bSend2Bk) pxContinOutCommand=sInfoS1.ByteInfoKadr+1;
          break;
        case 34: // �������� ���������������� (������ ��� ��������)
          CommandPort0[0]=168; 
          CommandPort0[1]=NEcho*2+4; // ����� �������
          CommandPort0[2]=sCmd.BufCommand[2]; // � �������.
          CommandPort0[3]=0;
          for(i=0;i<NEcho*2;i=i+2)
          {
            DanFl=RdFlash(AdrBufEcho+sCmd.BufCommand[2]*LengthEcho+i);
            CommandPort0[4+i]=DanFl>>8;// ������� 
            CommandPort0[4+i+1]=DanFl;// ������� 
          }
          CommandStand(&sInfoS1,codeCmd,Tranzit);
          pxContinOutCommand=sInfoS1.ByteInfoKadr+1;
          break;
        case 37: // ���������/���������� ���
          bSend2Bk=vCmdAGCOnOff(CommandPort0,&sCmd,&sInfoS1,codeCmd);
          if(bSend2Bk) pxContinOutCommand=sInfoS1.ByteInfoKadr+1;
          break;
        case 38: // ���/���� ������ (������ ���������� ������� � ����� ����������� � 0!!!)
          bSend2Bk=vCmdOutputResistance(CommandPort0,&sCmd,&sInfoS1,codeCmd);
          if(bSend2Bk) pxContinOutCommand=sInfoS1.ByteInfoKadr+1;
          break;
        case 40: // ��������� ������� � ��������� (300..3400) � ����������� �������
          if(sCmd.BufCommand[1])
          { // ���� ����� �����������
            if(xQueueSend(xQueueSportX,&sCmd,0,0)==errQUEUE_FULL) CountErrQueueReply++;
          }
          else
          {
            CommandPort0[0]=80; 
            CommandPort0[1]=7; // ����� �������
            CommandPort0[2]=sCmd.BufCommand[2]; // � �������.
            CommandPort0[3]=sCmd.BufCommand[3]; // ��. ���� F1
            CommandPort0[4]=sCmd.BufCommand[4]; // ��. ���� F1
            CommandPort0[5]=sCmd.BufCommand[5]; // ��. ���� F1
            CommandPort0[6]=sCmd.BufCommand[6]; // ��. ���� F1
            CommandStand(&sInfoS1,codeCmd,Tranzit);
            pxContinOutCommand=sInfoS1.ByteInfoKadr+1;
          }
          break;
        case 41: // ���������� ���������
          if(sCmd.BufCommand[1])
          { // ���� ����� �����������
            if(xQueueSend(xQueueSportX,&sCmd,0,0)==errQUEUE_FULL) CountErrQueueReply++;
          }
          else
          {
            CommandPort0[0]=21; 
            CommandPort0[1]=3; // ����� �������
            CommandPort0[2]=sCmd.BufCommand[2]; // � �������.
            CommandStand(&sInfoS1,codeCmd,Tranzit);
            pxContinOutCommand=sInfoS1.ByteInfoKadr+1;
          }
          break;
        case 42: // ��������� �������
          CommandPort0[0]=38; 
          CommandPort0[1]=3; // ����� �������
          CommandPort0[2]=sCmd.BufCommand[2]; // � �������.
          CommandStand(&sInfoS1,codeCmd,Tranzit);
          pxContinOutCommand=sInfoS1.ByteInfoKadr+1;
          break;
        case 50: // ������ �������� �������� �������� ������ �������
          if(sCmd.BufCommand[1])
          { // ���� ����� �����������
            if(xQueueSend(xQueueSportX,&sCmd,0,0)==errQUEUE_FULL) CountErrQueueReply++;
          }
          else
          {
//SendStr2IpLogger("$DEBUG$GetLvl: channel #%x",sCmd.BufCommand[2]);
            if(sCmd.BufCommand[2] & 0x80)
            {
              sCmd.BufCommand[2]&=0x7f;
              CommandPort0[0]=30; // ������� �������� ���
              CommandPort0[3]=2; // 2-�����
            }
            else
            {
              CommandPort0[0]=16; // ������� �������� ���
              CommandPort0[3]=0; // 0-����
            }
            CommandPort0[1]=4; // ����� �������
            CommandPort0[2]=sCmd.BufCommand[2]; // � ������
            sInfoS1.L=6;
            sInfoS1.ByteInfoKadr[0]=51;
            sInfoS1.ByteInfoKadr[1]=0;
            sInfoS1.ByteInfoKadr[2]=sCmd.BufCommand[2]; // � ������
            sInfoS1.ByteInfoKadr[3]=CommandPort0[3];
            pxContinOutCommand=sInfoS1.ByteInfoKadr+4;
          }
          break;
        case 55:  // ������� ����������/������������� ������ �������� ���-�3
          CommandPort0[0]=55;
          CommandPort0[1]=4; // ����� �������
          CommandPort0[2]=sCmd.BufCommand[1]; // � �������.
          CommandPort0[3]=sCmd.BufCommand[2]; // �������
          CommandStand(&sInfoS1,55,Tranzit);
          pxContinOutCommand=sInfoS1.ByteInfoKadr+1;
          break;
        case 60: // ������� ������ ������ �� � ������ ������ ��
          bSend2Bk=vCmdReadSoftVersionMU(CommandPort0,&sInfoS1);
          if(bSend2Bk) pxContinOutCommand=sInfoS1.ByteInfoKadr+5+(4*24);
          break;
        case 64: // ������� ������ ����������������� ����
          sInfoS1.L=5;
          sInfoS1.ByteInfoKadr[0]=65;
          sInfoS1.ByteInfoKadr[1]=(unsigned char) (TestBoard >> 24); // 
          sInfoS1.ByteInfoKadr[2]=(unsigned char) (TestBoard >> 16); // 
          sInfoS1.ByteInfoKadr[3]=(unsigned char) (TestBoard >> 8); // 
          sInfoS1.ByteInfoKadr[4]=(unsigned char) (TestBoard); //
          if(xQueueSend(xQueueReply[sInfoS1.Source],&sInfoS1,0,0)==errQUEUE_FULL) CountErrQueueReply++;
          break;
        case 70: // ������� ������ � ��������� ��������
          vProcessJornal(&sInfoS1,sCmd.L,sCmd.BufCommand+1);
          break;
        case 72: // ������� ������ ������ ��
          vCmdReadDataFromMU(&sCmd,&sInfoS1);
          break;
        case 74: // ������� ������ ������ � ������ ��
          vCmdWriteData2MU(&sCmd,&sInfoS1);
          break;
        case 76: // ������� ������ ������ ���
          if(sCmd.BufCommand[1]) 
          {
            CommandPort0[0]=65; // ������ ��������
            CommandPort0[3]=sCmd.BufCommand[3] / 3; //  ����� � ������
          }
          else 
          {
            CommandPort0[0]=64; //������ ������
            CommandPort0[3]=sCmd.BufCommand[3] / 2; //  ����� � ������
          }
          CommandPort0[1]=6; 
          CommandPort0[2]=sCmd.BufCommand[2]; // � �����������
          CommandPort0[4]=sCmd.BufCommand[4]; // �����
          CommandPort0[5]=sCmd.BufCommand[5];
          sInfoS1.L =6+sCmd.BufCommand[3];
          sInfoS1.ByteInfoKadr[0]=77;
          for(i=1;i<6;i++) sInfoS1.ByteInfoKadr[i]=sCmd.BufCommand[i];
          pxContinOutCommand=sInfoS1.ByteInfoKadr+6;
          break;
        case 78: // ������� ������ ������ � ������ ���
          if(sCmd.BufCommand[1]) CommandPort0[0]=164; // ������ ��������
          else CommandPort0[0]=163; // ������ ������
          CommandPort0[1]=sCmd.BufCommand[3]+7; // ����� �������
          CommandPort0[2]=sCmd.BufCommand[2]; // � �����������
          CommandPort0[3]=sCmd.BufCommand[4]; // �����
          CommandPort0[4]=sCmd.BufCommand[5];
          CommandPort0[5]=CommandPort0[6]=0;
          memcpy(CommandPort0+7,sCmd.BufCommand+6,sCmd.BufCommand[3]);
          sInfoS1.L=6;
          sInfoS1.ByteInfoKadr[0]=79;
          sInfoS1.ByteInfoKadr[1]=sCmd.BufCommand[1]; 
          sInfoS1.ByteInfoKadr[2]=sCmd.BufCommand[2]; 
          sInfoS1.ByteInfoKadr[3]=sCmd.BufCommand[4]; 
          sInfoS1.ByteInfoKadr[4]=sCmd.BufCommand[5]; 
          pxContinOutCommand=sInfoS1.ByteInfoKadr+5;
          break;
        case 80: // �����  ��
          AddToLogWithInfo(lt_Notification,ls_Task,lc_Debug,"Reset MU from IP!");
          BlockWatchDog();
          break;
        case 81: // ����� ���
          CommandPort0[0]=31;
          CommandPort0[1]=3; // ����� �������
          CommandPort0[2]=sCmd.BufCommand[1];
          sInfoS1.L=2;
          sInfoS1.ByteInfoKadr[0]=81;
          pxContinOutCommand=sInfoS1.ByteInfoKadr+1;
          break;
        case 82: // ������ ��������� ���
          i=800-(unsigned short)(xTaskGetTickCount() % (800));
          CommandPort0[0]=49;
          CommandPort0[1]=5; // ����� �������
          CommandPort0[2]=sCmd.BufCommand[1];
          CommandPort0[3]=i >> 8;
          CommandPort0[4]=i;
          sInfoS1.L=2;
          sInfoS1.ByteInfoKadr[0]=82;
          pxContinOutCommand=sInfoS1.ByteInfoKadr+1;
          break;
        case 83: // ����������������� �������
          CommandStand(&sInfoS1,codeCmd,CodeStatusTrue);
          SetNeedInitPults();
          break;
        case 84: // ������������ ��� !!!???
          CommandPort0[0]=17; 
          CommandPort0[1]=3; // ����� �������
          CommandPort0[2]=sCmd.BufCommand[1]; // � �������.
          sInfoS1.L=4;
          sInfoS1.ByteInfoKadr[0]=85;
          sInfoS1.ByteInfoKadr[1]=sCmd.BufCommand[1]; // � �������.
          pxContinOutCommand=sInfoS1.ByteInfoKadr+2;
          break;
        case 85:
        case 86:
          // !! ������� ���������� !!
          break;
        case 87: // ������� ��������� ����� ��������� �������
          memset(&time1,0xff,sizeof(struct tm));
          time1.tm_year=(int)(sCmd.BufCommand[1] + 100); // ��� 2006-1900;
          time1.tm_mon=(int)sCmd.BufCommand[2]-1; // ����� 
          time1.tm_mday=(int)sCmd.BufCommand[3]; // ����
          time1.tm_hour=(int)sCmd.BufCommand[4]; // ���
          time1.tm_min=(int)sCmd.BufCommand[5]; // ������
          time1.tm_sec=(int)sCmd.BufCommand[6]; // �������
          tt=mktime(&time1);
          SetClock(tt);
          for(i=0;i<NumberKeyboard;i++) // ����� ����� �� ������
          { if(MassM1[i]) ClockIndPult(i,&time1); }
          CommandStand(&sInfoS1,codeCmd,CodeStatusTrue);
          WriteAddrMU(sCmd.BufCommand[1],sCmd.BufCommand[2],sCmd.BufCommand[3]);
          break;
        case 88: // ������� ������ ����� ��������� �������
          tt=ReadClock();
          vPortEnableSwitchTask(false);
          ptime=gmtime(&tt);
          sInfoS1.L=8;
          sInfoS1.ByteInfoKadr[0]=89;
          StoreDtTm2Buf(sInfoS1.ByteInfoKadr+1,ptime);
          vPortEnableSwitchTask(true);
          if(xQueueSend(xQueueReply[sInfoS1.Source],&sInfoS1,0,0)==errQUEUE_FULL) CountErrQueueReply++;
          break;
        case 90: // ������ ������� ��������� 25/50 �� �� flash 
          if(sCmd.BufCommand[1]<2) DanFl=sCmd.BufCommand[1];
          else DanFl=0;
          WrFlash(DanFl,AdrFr25);
          CommandStand(&sInfoS1,codeCmd,CodeStatusTrue);
          break;
        case 91: // ������ ������� ��������� 25/50 �� �� flash 
          DanFl=RdFlash(AdrFr25);
          if(DanFl>1)
          {
            DanFl=0;
            WrFlash(DanFl, AdrFr25);
          }
          sInfoS1.L=2;
          sInfoS1.ByteInfoKadr[0]=91;
          sInfoS1.ByteInfoKadr[1]=(unsigned char)DanFl;
          if(xQueueSend(xQueueReply[sInfoS1.Source],&sInfoS1,0,0)==errQUEUE_FULL) CountErrQueueReply++;
          break;
        case 94: // ����������������� ������� ���
DataSave1[19]++;
          CommandPort0[0]=79;
          CommandPort0[1]=6; // ����� �������
          CommandPort0[2]=sCmd.BufCommand[1];
          CommandPort0[3]=0; 
          CommandPort0[4]=0; 
          CommandPort0[5]=0;
          sInfoS1.L=2;
          sInfoS1.ByteInfoKadr[0]=94;
          pxContinOutCommand=sInfoS1.ByteInfoKadr+1;
          break;
        case 96:
          if(xQueueSend(xQueueSportX,&sCmd,0,0)==errQUEUE_FULL) CountErrQueueReply++;
          break;
        case 98:
          vCmdPultState(&sCmd,&sInfoS1);
          break;
        case 100:
          vCmdReadCrash(&sCmd,&sInfoS1);
          break;
        case 102:
          vCmdReadError(&sCmd,&sInfoS1);
          break;
        case 103:
          vCmdReadAllKeyboardState(&sCmd,&sInfoS1);
          break;
        case 105:
          vCmdKeyFromIpPult(sCmd.BufCommand[1],sCmd.BufCommand[3],sCmd.BufCommand[2]);
          break;
        case 110:
          vCmdCP2SelectorInfo(sCmd.BufCommand[1],&sInfoS1);
          break;
        case 113:
          vCmdNeedDestroyAllConnections2Pult(sCmd.BufCommand[1]);
          break;
        case 115:
          SetNeedIpReinit();
          break;
        case 119:
          GetAutoAnswerMarks(sCmd.BufCommand[1],&sInfoS1);
          break;
        case 121:
          SetAutoAnswerMark(sCmd.BufCommand[1],sCmd.BufCommand[14],sCmd.BufCommand[15],&sInfoS1);
          break;
        case 255:
          // ��������� ����� �� DspCom - ������ �� ��������!
          break;
        default:
          CreateErrInfo(codeCmd,ErrNoCommand,&sInfoS1);
          break;
      }
      if(pxContinOutCommand) SendCommand2Uart(CommandPort0,&sInfoS1,pxContinOutCommand,sCmd.BufCommand[2],codeCmd,sCmd.Source);
    }
    SetWDState(d_WDSTATE_ViewCmd);
    vTaskDelay(5);
  }
}

/*        case ???:
          vCmdCP2ConferenceInfo(sCmd.BufCommand[1],sCmd.BufCommand[2],&sInfoS1);
          break;*/

/*        case 92: // ���������� �������
        {
!!!NU          unsigned int SizeBufTrace=sizeof(TraceBuf);

          if(sCmd.BufCommand[1]==1) vTaskList((signed char *)TraceBuf); // ������ �����
          else
          {
            if(sCmd.BufCommand[1]==2) vTaskStartTrace((signed char *)TraceBuf,SizeBufTrace); // ��������� �����������
            if(sCmd.BufCommand[1]==3) SizeBufTrace=ulTaskEndTrace(); // ���������� �����������
//memset(TraceBuf,0,SizeBufTrace); // !!!
          }
          sInfoS1.L=7;
          sInfoS1.ByteInfoKadr[0]=93; // ����� 
          sInfoS1.ByteInfoKadr[1]=((unsigned int)TraceBuf) >> 24;
          sInfoS1.ByteInfoKadr[2]=((unsigned int)TraceBuf) >> 16;
          sInfoS1.ByteInfoKadr[3]=((unsigned int)TraceBuf) >> 8;
          sInfoS1.ByteInfoKadr[4]=((unsigned int)TraceBuf) >> 0;
          sInfoS1.ByteInfoKadr[5]=SizeBufTrace >> 8;
          sInfoS1.ByteInfoKadr[6]=SizeBufTrace & 0xFF;
          if(xQueueSend(xQueueReply[sInfoS1.Source],&sInfoS1,10,0) == errQUEUE_FULL) CountErrQueueReply++;
          break;
        }*/

