#include "..\Sinfo.h"
#include "FunctionsPrototypes.h"
#include "GlobalData.h"
#include "ViewCommandFunc.h"

/////////////////////////////////////////////////////////////////////
unsigned short ReadCrash(unsigned char NumbSt, unsigned char *CrashString); //����� ������ � ������ �� ������
bool ReadInf(unsigned portCHAR SN,unsigned char *BufA4);

/////////////////////////////////////////////////////////////////////
void vCmdReadCrash(sCommand *psViewCommand,sInfoKadr *psInfoS1)
{
  static unsigned char CountCrash=1;

  switch(psViewCommand->BufCommand[1])
  {
    case 1: // ������ ������ ������ 
      psInfoS1->ByteInfoKadr[0] = 101; // ����� 
      psInfoS1->ByteInfoKadr[1] = 1; // ����������
      psInfoS1->ByteInfoKadr[2] = 1; // ����� ������ �� �������
      psInfoS1->L=2+ReadCrash(1,psInfoS1->ByteInfoKadr+2);
      CountCrash=2;
      break;
    case 2: // ������ ��������� ������ �� ������ ������
      psInfoS1->ByteInfoKadr[0] = 101; // ����� 
      psInfoS1->ByteInfoKadr[1] = 2; // ����������
      psInfoS1->ByteInfoKadr[2] = CountCrash; // ����� ������ �� �������
      psInfoS1->L=2+ReadCrash(CountCrash,psInfoS1->ByteInfoKadr+2);
      CountCrash++;
      break;
    default:
      return;
  }
  if(xQueueSend(xQueueReply[psInfoS1->Source],psInfoS1,10,0)==errQUEUE_FULL) CountErrQueueReply++;
}

void vCmdReadError(sCommand *psViewCommand,sInfoKadr *psInfoS1)
{
  unsigned char NumbPult;

  NumbPult=psViewCommand->BufCommand[1];
  psInfoS1->L = 20+32+14;
  psInfoS1->ByteInfoKadr[0] = 103;
  psInfoS1->ByteInfoKadr[1] = 0xff;
  /*if(ReadInf(MassM1[NumbPult],psInfoS1->ByteInfoKadr+2)) psInfoS1->ByteInfoKadr[1]=NumbPult;
  else*/ memset(psInfoS1->ByteInfoKadr+2,0,8);
  psInfoS1->ByteInfoKadr[10] = 0;
  psInfoS1->ByteInfoKadr[11] = 0;
  psInfoS1->ByteInfoKadr[12] = 0;
  psInfoS1->ByteInfoKadr[13] = 0;
  psInfoS1->ByteInfoKadr[14] = CountNoReply >> 8;
  psInfoS1->ByteInfoKadr[15] = CountNoReply;
  psInfoS1->ByteInfoKadr[16] = CountErrPaket >> 8;
  psInfoS1->ByteInfoKadr[17] = CountErrPaket;
  psInfoS1->ByteInfoKadr[18] = CountErrReadBuf >> 8;
  psInfoS1->ByteInfoKadr[19] = CountErrReadBuf;
  psInfoS1->ByteInfoKadr[20] = ErrMallocGener >> 8; // ������ �������������� ������ ��� �������� ������� ���������
  psInfoS1->ByteInfoKadr[21] = ErrMallocGener;
  psInfoS1->ByteInfoKadr[22] = ErrMallocAnal >> 8; // ������ �������������� ������ ��� �������� ������� ���������� �������
  psInfoS1->ByteInfoKadr[23] = ErrMallocAnal;
  psInfoS1->ByteInfoKadr[24] = ErrMallocE1 >> 8; // ������ �������������� ������ ��� �������� ������� ������ ��� E1
  psInfoS1->ByteInfoKadr[25] = ErrMallocE1;
  psInfoS1->ByteInfoKadr[26] = ErrMallocAns >> 8; // ������ �������������� ������ ��� �������� ������� ������ � ������� ���������
  psInfoS1->ByteInfoKadr[27] = ErrMallocAns;
  psInfoS1->ByteInfoKadr[28] = ErrMallocCom >> 8; // ������ �������������� ������ ��� �������� ������� ������ ���������� ���
  psInfoS1->ByteInfoKadr[29] = ErrMallocCom;
  psInfoS1->ByteInfoKadr[30] = ErrMallocRecord >> 8; // ������ �������������� ������ ��� �������� ������� �����������
  psInfoS1->ByteInfoKadr[31] = ErrMallocRecord;
  psInfoS1->ByteInfoKadr[32] = ErrMallocPlay >> 8; // ������ �������������� ������ ��� �������� ������� ��������������������
  psInfoS1->ByteInfoKadr[33] = ErrMallocPlay;
  psInfoS1->ByteInfoKadr[34] = ErrMallocSpec >> 8; // ������ �������������� ������ ��� �������� ������� ��������� ����������
  psInfoS1->ByteInfoKadr[35] = ErrMallocSpec;
  psInfoS1->ByteInfoKadr[36] = ErrQueueAns >> 8; // ������������ ������� ������ � ������� ���������
  psInfoS1->ByteInfoKadr[37] = ErrQueueAns;
  psInfoS1->ByteInfoKadr[38] = ErrQueueSNMP >> 8; // ������������ ������� ������ SNMP
  psInfoS1->ByteInfoKadr[39] = ErrQueueSNMP;
  psInfoS1->ByteInfoKadr[40] = ErrQueueEDSS >> 8; // ������������ ������� ������ EDSS
  psInfoS1->ByteInfoKadr[41] = ErrQueueEDSS; 
  psInfoS1->ByteInfoKadr[42] = ErrAnswerE1 >> 8; // ���������� ������ ������ �1
  psInfoS1->ByteInfoKadr[43] = ErrAnswerE1; 
  psInfoS1->ByteInfoKadr[44] = ErrSwapE1 >> 8; // ������ ������ � ������� �1
  psInfoS1->ByteInfoKadr[45] = ErrSwapE1;
  psInfoS1->ByteInfoKadr[46] = Err5ms >> 8; // ������ ��������� ������� ��������� � ������� 5 ��
  psInfoS1->ByteInfoKadr[47] = Err5ms;
  psInfoS1->ByteInfoKadr[48] = ErrSPORT0 >> 8; // ������ ������ �� SPORT0
  psInfoS1->ByteInfoKadr[49] = ErrSPORT0;
  psInfoS1->ByteInfoKadr[50] = CountErr >> 8; // ������ �������
  psInfoS1->ByteInfoKadr[51] = CountErr;
  psInfoS1->ByteInfoKadr[52] = MassErrLoadSel[0]>> 8; // �������� ����� ������
  psInfoS1->ByteInfoKadr[53] = MassErrLoadSel[0];
  psInfoS1->ByteInfoKadr[54] = MassErrLoadSel[1]>> 8; // ������/ ������
  psInfoS1->ByteInfoKadr[55] = MassErrLoadSel[1];
  psInfoS1->ByteInfoKadr[56] = MassErrLoadSel[2]>> 8; // ���������� ����� �������
  psInfoS1->ByteInfoKadr[57] = MassErrLoadSel[2];
  psInfoS1->ByteInfoKadr[58] = MassErrLoadSel[3]>> 8; // ��� ��������
  psInfoS1->ByteInfoKadr[59] = MassErrLoadSel[3];
  psInfoS1->ByteInfoKadr[60] = MassErrLoadSel[4]>> 8; // ����� ����������
  psInfoS1->ByteInfoKadr[61] = MassErrLoadSel[4];
  psInfoS1->ByteInfoKadr[62] = MassErrLoadSel[5]>> 8; // ��������� ������� ������� �� ����������� ������
  psInfoS1->ByteInfoKadr[63] = MassErrLoadSel[5];
  psInfoS1->ByteInfoKadr[64] = 0; // ���������� ������������ ����������� ������
  psInfoS1->ByteInfoKadr[65] = 0;
  if(xQueueSend(xQueueReply[psInfoS1->Source],psInfoS1,10,0)==errQUEUE_FULL) CountErrQueueReply++;
}

