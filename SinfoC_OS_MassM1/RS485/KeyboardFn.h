void ClearKeySpecial(unsigned char PultNb);

void BellOnOffControl(void);
void BellOn(unsigned char PultNb, unsigned char NumberTune);
void BellOff(unsigned char PultNb);

void InitAbonentStateArray(void);
unsigned char FindFirstAbonentIndex(unsigned char btPultNb,
                                    unsigned short wConnectionPoint,unsigned short wOperatorChanel,
                                    unsigned char TypeAbon,unsigned char NumbAbon,unsigned char *pbtTlfn);
unsigned char FindNextAbonentIndex(unsigned char btIndex,unsigned char btPultNb,
                                   unsigned short wConnectionPoint,unsigned short wOperatorChanel,
                                   unsigned char TypeAbon,unsigned char NumbAbon,unsigned char *pbtTlfn);
unsigned char GetFreeAbonentStateIndex(void);
ABONENT_STATE_TP *GetFreeAbonentStatePtr(void);
unsigned char GetAbonentStateIndexFromPtr(ABONENT_STATE_TP *psAbnt0);
ABONENT_STATE_TP *GetAbonentStatePtrFromIndex(unsigned char btIndex);
ABONENT_STATE_TP *GetAbonentStatePtrByFind(unsigned char btPultNb,
                                     unsigned short wConnectionPoint,unsigned short wOperatorChanel,
                                     unsigned char TypeAbon,unsigned char btInd2TlfnBase,unsigned char *pbtTlfn);
unsigned char GetAbonentStateIndByState(unsigned char PultNb,unsigned char btIndex,unsigned char btState);
ABONENT_STATE_TP *GetAbonentStatePtrFromKey(unsigned char btPultNb,unsigned char btKey,unsigned char btLayer);
unsigned char GetAbonentStateIndFromKey(unsigned char PultNb,unsigned char btKey,unsigned char btLayer);
unsigned char GetAbonentStateFromPtr(ABONENT_STATE_TP *psAbnt);
unsigned char GetAbonentStateFromKey(unsigned char PultNb,unsigned char btKey,unsigned char btLayer);
bool IsAutomaticCallAbonent(ABONENT_STATE_TP *psAbnt);
void SetAutomaticCallAbonent(ABONENT_STATE_TP *psAbnt);
void ClearAbonentStateByIndex(unsigned char btIndex);
void ClearAbonentStateByPtr(ABONENT_STATE_TP *psAbnt);
unsigned short GetAbonentOperatorChannelFromIndex(unsigned char btInd);
unsigned short GetAbonentOperatorChannelFromKey(unsigned char PultNb,unsigned char btKey,unsigned char btLayer);
unsigned short GetAbonentConnectionPointFromIndex(unsigned char btInd);
unsigned short GetAbonentConnectionPointFromKey(unsigned char PultNb,unsigned char btKey,unsigned char btLayer);
bool IsTalk2Pult(unsigned char PultNb,unsigned short wOperator);
bool IsIncomingCall2Pult(unsigned char PultNb);
unsigned char GetCountUknownISDNAbonent(unsigned char PultNb);
unsigned char GetCallKeyState(unsigned char PultNb,unsigned char btKey,unsigned char btLayer);
void InvalidateAbonentState4Pult(unsigned char PultNb);
bool IsCallKeyEqual(sKBKey *psKey0,sKBKey *psKey1);
void SetLedColorIfTalk(ABONENT_STATE_TP *psAbnt);
bool NeedReshowActualMKAAbntState(ABONENT_STATE_TP *psAbnt0);
void PutAbonentsState2AllPult(void);
bool SetAbonentStateByPtr(ABONENT_STATE_TP *psAbnt,unsigned char btState,unsigned char btIncomingTalk);
void SendActiveUnnameAbnt_ATC_ISDN(ABONENT_STATE_TP *psAbnt);
void vCmdReadAllKeyboardState(sCommand *psViewCommand,sInfoKadr *psInfo);

void NeedShowCurrentKbdState(unsigned char PultNb);
void SetKbdReset(unsigned char PultNb,bool bRst);

bool IsKbdReady(unsigned char PultNb);
bool GetKbdUnlockState(unsigned char PultNb);
bool GetLayerUnlockState(unsigned char PultNb,unsigned char btLayer);
void SetKbdUnlockState(unsigned char PultNb,bool bUnlock);
void SetLayerUnlockState(unsigned char PultNb,unsigned char btLayer,bool bUnlock);
bool IsKbdUnlock(unsigned char PultNb);
bool IsKbdLock(unsigned char PultNb);
bool IsKbdLayerLock(unsigned char PultNb,unsigned char btLayer);
bool IsPultWorkingState(unsigned char btPultNb);
void IndicateLayerKey(unsigned char PultNb);
void IndicateServiceKey(unsigned char PultNb);
void PutKbdUnlockLedState(unsigned char PultNb,unsigned char btBlockLed);
void SendLayerAndLock2IpPult(unsigned char PultNb,sInfoKadr *psInfo);
unsigned char GetLayerLedColor(unsigned char PultNb,unsigned char btLayer);
unsigned char CmdEnDisOutCP(unsigned short wCP,unsigned char btEnOut);

bool IsPressChoice(unsigned char PultNb);
bool IsPressAddMember(unsigned char PultNb);
bool IsPressDisEnOut(unsigned char PultNb);
bool IsPressTest(unsigned char PultNb);
bool IsKeyOff(unsigned char PultNb);
unsigned char GetRadioChan(unsigned char PultNb);

void HoldTOut(unsigned char PultNb);
bool HoldOn(unsigned char PultNb,unsigned char Key,unsigned char btLayer,bool bHold);
//void SetCurrentTalkAbonent2Hold(unsigned char PultNb,unsigned char Key,unsigned char btLayer);

unsigned int GetTypeCall4ChanelMKA(unsigned short NChan);
void OtboiAbonentATC(unsigned char PultNb,unsigned char Key,unsigned char btLayer);
void ControlATCLevelTOut(void);
void SignalATC_Busy(unsigned short wNChan);
void SignalATC_Ready(unsigned short wNChan);
void OutgoingCallATC(unsigned char PultNb,unsigned char Key,unsigned char btLayer,unsigned char KeyATCDirect);
void OutgoingCallPressedFreeATS(unsigned char PultNb,unsigned char Key,unsigned char btLayer,
                                unsigned char KeyATCDirect,unsigned char *pbtTlfnNumber);
void IncomingCallATC(unsigned short NChan);
void AnswerIncomingCallATC(unsigned char PultNb,ABONENT_STATE_TP *psAbnt);

bool IsFreeAbonentFromKey(unsigned char PultNb,unsigned char Key,unsigned char btLayer);
void OtboiAbonentISDN(unsigned char PultNb,unsigned char Key,unsigned char Layer,unsigned char btReason);
void PutAbntNumb2Display(unsigned char PultNb,ABONENT_STATE_TP *psAbnt);
void OutgoingCallISDN(unsigned char PultNb,unsigned char Key,unsigned char Layer);
bool RepeatOugoingCall(ABONENT_STATE_TP *psAbnt);
void FreeAbntOutgoingCallEDSS(unsigned short wNChan);
void Busy_EDSS(unsigned short wNChan);
void AnswerOutgoingCallEDSS(unsigned short wNChan,unsigned short wChanOper);
bool _xIncomingCallISDNAnswer(unsigned short wNChan,unsigned short wChanOper,unsigned char btCommute);
void AnswerIncomingCallISDN(unsigned char PultNb,ABONENT_STATE_TP *psAbnt,unsigned char btCommute);

bool _xIncomingCallISDNFree(unsigned short wNChan,unsigned short wChanOper,unsigned char btKPV);

void ControlEDSSTOut(void);
void OtboiEDSS(unsigned short wNChan,unsigned short wChanOper);
void IncomingCall_EDSS(unsigned short wNChan,sInfoKadr *psVxCall);
bool _xOutgoingCallISDN(unsigned short NChan,unsigned short wOper,unsigned char btE1,
                        char *BufNumbDest,char *BufNumbSrs,char *BufNameSrs,unsigned char SN);

unsigned char FindAbntKey2Pult(unsigned char PultNb,unsigned char *pbtLayer,
                               unsigned short wNChan,unsigned char *pTypeAbon,unsigned char AbntNumb);
void IncomingCall_TDC(unsigned short wNChan,unsigned char AbntNumb,unsigned char FreqInd);
void AnswerOutgoingCallTDC(unsigned char PultNb,unsigned short wNChan,unsigned char AbntNumb);
void OutgoingCallTDC(unsigned char PultNb,unsigned char Key,unsigned char btLayer,bool bTest);
void ControlTDCTOut(void);
void PressCallKey_TDC(unsigned char PultNb,unsigned char Key,unsigned char btLayer,bool bTest);
void PressCallKey_GroupTDC(unsigned char PultNb,unsigned char Key,unsigned char btLayer);

void IncomingCall_DTMF(unsigned short wNChan,unsigned short btCode);

void CreateOutgoingAbonentPPSR_RSDT(unsigned char PultNb,unsigned short wNChan,unsigned char btCallType);
void IncomingCall_PPSR_RSDT(unsigned short wNChan,unsigned char btCallType);
bool _xCallPPSR(unsigned short wNChan,unsigned char TypeCall,unsigned char NRst,unsigned char NRChan);
void PressCallKey_PPSR(unsigned char PultNb,unsigned char Key,unsigned char btLayer);

bool _xCallRSDT(unsigned short wNChan,unsigned char NumbAbon);
void PressCallKey_RSDT(unsigned char PultNb,unsigned char Key,unsigned char btLayer);

void IncomingCall_Level(unsigned short wNChan);
void PressCallKey_Level(unsigned char PultNb,unsigned char Key,unsigned char btLayer);
void CallLevelTOut(unsigned char PultNb);

bool OtboiAbonentCommonPart(unsigned char PultNb,unsigned char Key,unsigned char Layer,
                            unsigned short *pwConnectionPoint,unsigned short *pwOperatorChanel);
bool IsFreeMKALine(unsigned char PultNb,unsigned char Key,unsigned char btLayer,unsigned char NumbAbon);
void OtboiAbonentMKA(unsigned char PultNb,unsigned short wNChan,unsigned char AbntNumb,unsigned char btType);
void DelIncomingCallMKAFromOtherPult(unsigned char PultNb,unsigned short NChan,unsigned char AbntNumb);
void AnswerIncomingCallMKA(ABONENT_STATE_TP *psAbnt);

bool IsFreeISDNAbonent(unsigned char PultNb,unsigned char Key,unsigned char btLayer);
void Control_ATC_EDSS_IPPM_TOut(void);
void PressCallKey_ISDN_ATC(unsigned char PultNb,unsigned char Key,unsigned char btLayer);
void SetManualOtboi(unsigned char PultNb,bool bSet);
bool IsManualOtboi(unsigned char PultNb);
void PressCallKey(unsigned char PultNb,unsigned char Key,bool bOff,bool bTest);
void ReleaseCallKey(unsigned char PultNb,unsigned char Key,unsigned char btLayer);

void InitKeyNumeralVars(void);
void SetAbonentIndex4Digit(unsigned char PultNb,unsigned char btInd);
void ClearAbonentIndex4Digit(unsigned char PultNb,unsigned char btInd);
void Key_Numeral(unsigned char PultNb,unsigned char CodeKey);
void Key_BackSpace(unsigned char PultNb);

void Key_Redial(unsigned char btPultNb);
void OtboiAbonent_ATCDirect(unsigned char PultNb,unsigned char btLayer,unsigned char Key);
void Key_ATCDirect(unsigned char btPultNb,unsigned char btLayer,unsigned char Key);

bool IsOutgoingNumberTmpPresent(unsigned char NumbPult);
void OutgoingNumberTmpClear(unsigned char NumbPult);
void OutgoingNumberTmpTOut(unsigned char NumbPult);
void OutgoingNumberTmpTOutClear(unsigned char NumbPult);
bool GetOutgoingNumberTmp(unsigned char NumbPult,unsigned char **ppbtMasDg,unsigned char *pbtNumbLn);
void SetOutgoingNumberTmp(unsigned char NumbPult,unsigned char *pbtMasDg);
void PutOutgoingNumber2Display(unsigned char PultNb,unsigned char *pbtMasDg,unsigned char Ln);
void OutgoingNumberTmpRestore2Display(unsigned char NumbPult);
void RestoreAbonentLedState(unsigned char PultNb);

void InitKeyboard(unsigned portCHAR SN);
void OutputString(unsigned char PultNb, const char *StrOutput, unsigned char Font, unsigned char X, unsigned char Y);
void ClearScreen(unsigned portCHAR PultNb);
void IndicClock(unsigned portCHAR PultNb);
bool SetClockPult(unsigned portCHAR PultNb, struct tm *ptm); 
void ClockIndPult(unsigned portCHAR PultNb, struct tm *ptm); 
void OnOffRejim(unsigned char PultNb, unsigned char Cmd,unsigned char OnOff);

unsigned char GetCurrentLayer(unsigned char PultNb);
unsigned char GetMaxLayer(unsigned char PultNb);
void ResetVirtualLed4Pult(unsigned char PultNb);
void ResetMasVirtualLed(void);
void SimplePiskPult(unsigned char PultNb);
void PutLed2Pult(sIndic *psKBIndicM);
void xSendKeyState108ToIpPult(unsigned char SN,unsigned char btKey,unsigned char btState);
void xSendKeyState104ToIpPult(unsigned char SN,unsigned char btKey,unsigned char btState);
void IndicCmd(unsigned char PultNb,unsigned char btLayer,unsigned char Key,unsigned char DanSwet,
              unsigned char CC_Sound1,unsigned char W_Sound);
void PutStr2Display(unsigned char PultNb,char *pstrAbnt,unsigned char btX);
void PutErrInfo2Display(unsigned char NumbPult,const char *strErr);
void DelConnectionPoint(unsigned short wCP);
unsigned short GetFreeConnectionPoint(void);
bool SendCmdOtboiISDN(unsigned char Reason,unsigned short wNChan,unsigned short wChanOper);
void SetDisplayTOut(char PultNb,unsigned int TOut);
void OtboiAbonent(unsigned char PultNb,unsigned char Key,unsigned char btLayer);
void DeleteAllConnections2Sinfo(void);
void DeleteAllConnections2Pult(unsigned char PultNb);
void DeleteAllConnections2PultLayer(unsigned char PultNb,unsigned char btLayer);
void SwitchGroup(unsigned char NumbPult, unsigned char Group); 
bool WaitReply(unsigned char Command, unsigned char Subcommand);
void Commute(bool Code,unsigned short NChan1, unsigned short NChan2); // ��������� ���������� ������� NChan1 < NChan2
bool TrubaUP_Down(unsigned char UP_Down, unsigned short NChanUD); //�����/ �������� ������ �� ������ NChanUD
unsigned char FindPultActive(unsigned short wChanOper);
unsigned char FindISDNAbonentKey(unsigned char btPultNb,unsigned char *pTlfn,unsigned char btLn,
                                 unsigned char *pNumb,unsigned char *pbtLayer);
unsigned char FindFreeISDNKey(unsigned char PultNb,unsigned char *pbtLayer);
void DSRS_On(unsigned char PultNb,unsigned short DSRS_Chan);
void DSRS_Off(unsigned char PultNb,unsigned short DSRS_Chan);
bool ExistTalk2LineMKA(unsigned char PultNb,unsigned short wChan);
bool ExistTalk2Group(unsigned char PultNb,unsigned char btGroup);
char *strclr(char *str);

void DetectAllTimers(void);

unsigned char GetGroupFromChan(unsigned short NChan);
unsigned char GetTlfnGroup(unsigned char NumbAbon);
bool IsGroupAutoAnswer4PultFromChan(unsigned char PultNb,unsigned short NChan);
bool IsGroupAutoAnswer4PultFromGroup(unsigned char PultNb,unsigned short wGroup);
bool GroupAutoAnswer4Pult(unsigned char PultNb,unsigned short NChan,bool bAA);
void GetAutoAnswerMarks(unsigned char SN,sInfoKadr *psInfo);
void SetAutoAnswerMark(unsigned char SN,unsigned char btGroup,unsigned char btDisableAA,sInfoKadr *psInfo);
unsigned short GetOperatorChannel(unsigned char PultNb,unsigned char btDS_RS);
unsigned short GetOperatorChannelFromCode(unsigned short ChanOper);
unsigned short GetOperatorChannelFromKey(unsigned char PultNb,unsigned char btKey,unsigned char btLayer);
unsigned char *GetPultTlfnPtr(unsigned char PultNb);
char *GetPultNamePtr(unsigned char PultNb);
bool IsPultAutoAnswer(unsigned char PultNb);
void SetNeedInitPults(void);
bool IsNeedInitPults(void);
void InitPults(bool bWait);

bool IsPlaySound2Selector(unsigned char PultNb,unsigned short wNChan);
bool IsSelectorActive(unsigned char PultNb);
bool IsSelectorWorkState(unsigned char PultNb);
unsigned char GetActiveSelectorState(unsigned char PultNb);
unsigned char GetSelectorState(unsigned char PultNb,unsigned char btKey);
unsigned char GetSelectorMemberCount(unsigned char PultNb);
unsigned short GetSelectorOperator(unsigned char PultNb);
unsigned char GetSelectorNumb(unsigned char PultNb);
int GetAbntSelectorInfoFromIndex(unsigned char PultNb,unsigned char btInd,sAbntSelectorInfo *psAbntSInfo);
bool IsKey2Selector(unsigned char PultNb0,unsigned char btKey0,unsigned char btLayer0,unsigned char btWhere);
unsigned char GetAbonentSelectorState(unsigned char PultNb,unsigned char btKey,unsigned char btLayer);
void IsdnIPPMAbntSelectorAnswer(unsigned char PultNb,unsigned char btKey);
void DelAbonentFromSelector(unsigned char PultNb,unsigned char Key,unsigned char btLayer);
void DestroySelector(unsigned char PultNb);
void PutKeyStateAbonentSelector2Pult(unsigned char PultNb,bool bShowAll);
void ControlSelectorTOut(void);
void PressSelectorN(unsigned char PultNb,unsigned char btLayer,unsigned char Key,unsigned char SelectorNb,bool bOff);
bool AddAbonent2Selector(unsigned char PultNb,unsigned char btKey,unsigned char btLayer);
bool AddTDCIPPM2CurrentSelector(unsigned char PultNb,ABONENT_STATE_TP *psAbnt);
void RestoreAbonentToSelector(unsigned char PultNb,unsigned char Key,unsigned char btLayer);
void SendAllAbonentSelector2IpPult(unsigned char PultNb);
bool IsAutomaticCall(unsigned char PultNb);
unsigned char TranslateSelectorState2Led(unsigned char PultNb,unsigned char SelectorNb);
void PressCallKey_Selector(unsigned char PultNb,unsigned char Key,unsigned char btLayer);
void OtboiAbonentSelector(unsigned char PultNb,unsigned char Key,unsigned char btLayer);
void EndSelectorFn(unsigned char PultNb);
bool IsAbonentListener(unsigned char PultNb,unsigned char Key);
bool PressCallKey4Selector(unsigned char PultNb,unsigned char Key,unsigned char btLayer);
bool SetAbnt2SelectorStatus(unsigned char PultNb,unsigned char btKey,unsigned char btLayer);
void Key_BeginSelector(unsigned char PultNb,unsigned char btLayer,unsigned char Key);
void IncomingCallSelectorCtrl(unsigned short wNChan);
void ReplaceCP2Selector(unsigned char PultNb,unsigned short wCP_Old,unsigned short wCP_New);

void _xDelAbntFromSelector(unsigned char PultNb,unsigned short NChan);
bool _xAddAbnt2Selector(unsigned char PultNb,unsigned short NChan);

void OtboiAbonentPultCall(unsigned char PultNb,unsigned char Key,unsigned char btLayer);
void PultCallTOut(unsigned char PultNb);
void PressCallKey_PultCall(unsigned char PultNb,unsigned char Key,unsigned char btLayer,bool bTest);

void SendAllAbonentConference2IpPult(unsigned char PultNb,unsigned char ConfNb);
void InitConferenceVars(void);
unsigned char GetConferenceState(unsigned char PultNb,unsigned char ConfNb);
bool IsKey2Conference(unsigned char PultNb,unsigned char btKey,unsigned char btLayer,unsigned char *pbtConf);
bool IsConferenceActive(unsigned char PultNb);
bool IsConferenceWork(unsigned char PultNb,unsigned char ConfNb);
bool IsAbonent2Conference(unsigned char PultNb,unsigned char btKey,unsigned char btLayer);
bool DelCommuteConference(unsigned char PultNb,unsigned char ConfNb);
bool AbonentConferenceCommuteAddDell(unsigned char PultNb,unsigned char ConfNb,unsigned short wChan,bool bAdd);
bool CreateCommuteConference(unsigned char PultNb,unsigned char ConfNb,unsigned short wOper,
                             unsigned char btMaxAbnt,unsigned char *btMassAbntInd);
void DelAbonentFromConference(unsigned char PultNb,unsigned char btKey,unsigned char btLayer);
void DeleteAllConference2Pult(unsigned char PultNb);
void AddAbonent2Conference(unsigned char PultNb,unsigned char btKey,unsigned char btLayer);
void PressConferenceKey(unsigned char PultNb,unsigned char Key,unsigned char btLayer,
                        unsigned char ConfNb,bool bOtkl,bool bTest);
void UnpressConferenceKey(unsigned char PultNb,unsigned char Key,unsigned char btLayer,unsigned char ConfNb);

void InitFreeHandConf(void);
void DelFreeHandConf(unsigned char PultNb);
void DelAllFreeHandConf(void);
bool DelAbntFromFreeHandConference(unsigned char PultNb,ABONENT_STATE_TP *psAbnt);
bool AddAbnt2FreeHandConf(unsigned char PultNb,ABONENT_STATE_TP *psAbnt);

unsigned char GetTikkerState(unsigned char PultNb);
bool _xCmdSetTikkerState(unsigned char PultNb,bool bOn);
void SetTikkerKeyOff(unsigned char PultNb);
void PressTikkerKey(unsigned char PultNb,unsigned char btLayer,unsigned char Key);

void PressCallKey_OutSPEC(unsigned char PultNb,unsigned char Key,unsigned char btLayer);
void CallOutSPECTOut(unsigned char PultNb);

bool _xOutgoingCallDTMF(unsigned short NChan,unsigned char *btBufNumb,unsigned char btLn);

void InitCrossCommute(void);
bool IsCrossCommuteAbnt(unsigned short wNChan);
void CrossCommuteOtboi(unsigned short wNChan);
void CrossCommuteOtboiAll(void);
bool CrossCallISDN(unsigned short wNChan,unsigned char btE1Srs,
                   char *NumberSrs,char *NameSrs,char *NumberDst);
void CrossCommuteAnswer(unsigned short wNChan,unsigned short wOper);

void InitTransitVars(void);
bool IsCreatedTransit(unsigned char PultNb);
void DeleteAllTransit(void);
void RestoreAllTransit(void);
void RestoreTransitLedState(unsigned char PultNb);
void PressTransitKey(unsigned char PultNb,unsigned char btLayer,unsigned char btKey,unsigned char btOtkl);
void UnPressTransitKey(unsigned char PultNb,unsigned char btLayer,unsigned char btKey);
void ClearCreatedTransit(unsigned char PultNb);
void XorAbnt2Transit(unsigned char PultNb,unsigned char btKey);
void ControlTransitTOut(void);

unsigned short GetMU2IPPMId(void);
LIST_MU2IPPM_TP *ListMU2IPPM_AddItem(LIST_MU2IPPM_TP *pItem0);
void ListMU2IPPM_Process(void);
bool IPPM_RcvAnsw(unsigned short wCmdId,unsigned char btRes);
void ClearQueueCmd4IPPM(unsigned char Numb);

void InitRepeatCmdIPPM(void);
void DelRepeatCmdIPPM(unsigned char Numb);
void SendAnsw4IPPMCmd(unsigned char Numb,unsigned short wCmd,sInfoKadr *psInfo);
bool RepeatCmdIPPM(unsigned char Numb,unsigned short wCmd);

void StartAudioFromMU2IPPM_Private(unsigned char Numb,unsigned short wCP);
void StartAudioFromMU2IPPM_Multicast(unsigned int dwIP,unsigned short wCP);
void StopAudioFromMU2IPPM(unsigned short wCP);
void StartAudioFromIPPM2MU(unsigned short wCP);
void StopAudioFromIPPM2MU(unsigned short wCP);
unsigned int GetMIPAddr(unsigned char SN);
bool IsRegisterAbonentIPPM(unsigned char Numb);
unsigned short CreatVMCP(unsigned char SN);
unsigned short GetCPFromVMCP(unsigned short wVMCP);
unsigned short CreatMCP(unsigned char SN);
void DelMCP(unsigned short wMCP);
unsigned char GetCountMulticastIPPM(unsigned short wVMCP);
void SetCurrentAbonentIPPM(unsigned char Numb,ABONENT_STATE_TP *psAbnt);
unsigned char GetOrderStudioAbonent(unsigned char Numb);
unsigned short GetCP_IPPM2MU(unsigned char Numb);
bool DelCP_IPPM2MU(unsigned char Numb);
void ClearIncomingCallIPPM4AllPults(unsigned char Numb);
void IPPM_Event(sInfoKadr *psVxCallT);
void InitIPPM(void);
void IPPM_IRPTOut(void);
void SendIPPMCmdConnect(unsigned char Numb,ABONENT_STATE_TP *psAbnt);
bool IsMaxCountSpeakIPPM2UK(void);
void SendIPPMCmdConnectPrivate(unsigned char Numb);
void OtboiAbonentIPPM(unsigned char PultNb,unsigned char Key,unsigned char btLayer);

void OutgoingCallIPPM(unsigned char PultNb,unsigned char Key,unsigned char btLayer,bool bTest);
void PressCallKey_IPPM(unsigned char PultNb,unsigned char Key,unsigned char btLayer,bool bTest);

