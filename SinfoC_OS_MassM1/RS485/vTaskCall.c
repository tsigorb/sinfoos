#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

/////////////////////////////////////////////////////////////////////
void ProcessKeyQueue(void);
void InitDisplayVars(void);
void InitKeyVars(void);

/////////////////////////////////////////////////////////////////////
volatile static bool s_bMasNeedShowCurKbdState[NumberKeyboard];

/////////////////////////////////////////////////////////////////////
void NeedShowCurrentKbdState(unsigned char PultNb)
{ s_bMasNeedShowCurKbdState[PultNb]=true; }

void ShowCurrentKbdState(void)
{
  unsigned char PultNb;

  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if(MassM1[PultNb])
    {
      if(s_bMasNeedShowCurKbdState[PultNb])
      {
        ResetVirtualLed4Pult(PultNb);
        IndicCmd(PultNb,0,NO_LED,NO_INDIC,2,1);
        IndicateServiceKey(PultNb);
        IndicateLayerKey(PultNb);
        SwitchGroup(PultNb,GetCurrentLayer(PultNb));
        OutgoingNumberTmpRestore2Display(PultNb);
        RestoreAbonentLedState(PultNb);
        RestoreTransitLedState(PultNb);
      }
      PutKeyStateAbonentSelector2Pult(PultNb,s_bMasNeedShowCurKbdState[PultNb]);
      s_bMasNeedShowCurKbdState[PultNb]=false;
    }
  }
}

///////////////////////////////////////
void ISDN_Event(unsigned short wNChan,sInfoKadr *psVxCallT)
{ // ������� �� PRI
  unsigned short wChanOper;
  unsigned int dwCause;

  switch(psVxCallT->ByteInfoKadr[4])
  { // ����������
    case 201 :  // ����� �� ������� 201
      /*if(sVxCallT.ByteInfoKadr[26]!=1)
      { // ���� ������ -> ��������� ������� ��������
        wChanOper = ((unsigned short)(sVxCallT.ByteInfoKadr[5]))<<8 | sVxCallT.ByteInfoKadr[6];
      }*/
      break;
    case 207:  // ������� �������� ����� c EDSS
      IncomingCall_EDSS(wNChan,psVxCallT);
      break;
    case 208:  // �������� �������������� ����� c EDSS
      break;
    case 209:  // ������� Alert c EDSS
      FreeAbntOutgoingCallEDSS(wNChan);
      break;
    case 210:  // "Progress"...
/*      dwCause=psVxCallT->ByteInfoKadr[18];
      dwCause=(dwCause << 8) | psVxCallT->ByteInfoKadr[19];
      dwCause=(dwCause << 8) | psVxCallT->ByteInfoKadr[20];
      dwCause=(dwCause << 8) | psVxCallT->ByteInfoKadr[21];*/
//      if(dwCause!=0) Busy_EDSS(wNChan);
      break;
    case 211:  // ������� Speaker c EDSS
      //����� ��� �� ����� ��� ���������...
      //wChanOper = ((unsigned short)(sVxCallT.ByteInfoKadr[5]))<<8 | sVxCallT.ByteInfoKadr[6];
      break;
    case 212:  // ������� ���� ������
      wChanOper=((unsigned short)(psVxCallT->ByteInfoKadr[5]))<<8 | psVxCallT->ByteInfoKadr[6];
      AnswerOutgoingCallEDSS(wNChan,wChanOper);
      break; 
    case 213:  // ������� ����� c EDSS (������� ������� ������)
    case 214:  // ������� ����� c EDSS (����� ��� ����������)
      wChanOper=((unsigned short)(psVxCallT->ByteInfoKadr[5]))<<8 | psVxCallT->ByteInfoKadr[6];
      {
        ABONENT_STATE_TP *psAbnt=GetAbonentStatePtrByFind(0xff,wNChan,wChanOper,0xff,0xff,NULL);
        if(!psAbnt) SendStr2IpLogger("$DEBUG$OtboiEDSS: wChan=0x%04x, wOper=%d, in_cause=%d",wNChan,wChanOper,psVxCallT->ByteInfoKadr[7]);
        else SendStr2IpLogger("$DEBUG$OtboiEDSS: Pult=%d, key=%d, in_cause=%d",psAbnt->btPultNb,psAbnt->btKey,psVxCallT->ByteInfoKadr[7]);
      }
      OtboiEDSS(wNChan,wChanOper);
      break;
    default: break;
  }
}

void IPPult_Event(unsigned char *pbtCmdInfo)
{
  unsigned char PultNb;

  switch(pbtCmdInfo[0])
  {
    case 1: // ��������� ������������� ������
      PultNb=pbtCmdInfo[1];
      if(pbtCmdInfo[2]) IndicateServiceKey(PultNb); // ����
      else ClearKeySpecial(PultNb); // ����
      break;
    default: break;
  }
}

void EndSound2ConnectionPoint(unsigned short wNChan)
{
  unsigned char PultNb;

  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if(MassM1[PultNb])
    {
      if(IsPlaySound2Selector(PultNb,wNChan)) DestroySelector(PultNb);
    }
  }
}

void ProcessQueueVxCall(void)
{
  unsigned short wNChan;
  static sInfoKadr sVxCallT; // ��������� ������ � �������� ������

  if(xQueueReceive(xQueueVxCall,&sVxCallT,0))
  { // ���� �������� ������ � �������
    wNChan=((unsigned short)sVxCallT.ByteInfoKadr[1] << 8) | (sVxCallT.ByteInfoKadr[2]+1);
    switch(sVxCallT.ByteInfoKadr[3])
    {
        case 1: // ������ "����� �������"
          SignalATC_Ready(wNChan);
          break;
        case 2: // �������� ����� ���
          IncomingCall_TDC(wNChan,sVxCallT.ByteInfoKadr[4],sVxCallT.ByteInfoKadr[5]);
          break;
        case 3: // ����� ����
          IncomingCall_DTMF(wNChan,sVxCallT.ByteInfoKadr[4]);
          break;
        case 4: // �������� ����� �� ������
          IncomingCall_Level(wNChan);
          break;
        case 5: // ������ ��� "������"
          SignalATC_Busy(wNChan);
          break;
        case 6: // �������� ����� ���
          IncomingCallATC(wNChan);
          break;
        case 7: // �������� ����� "��������"
          IncomingCallSelectorCtrl(wNChan);
          break;
        case 9: // ����� ��������� ���
//          Call_ImitatorTDC(NumbAb_NChanV,sVxCallT.ByteInfoKadr[4],&DanSwetKonf);
          break;
        case 11: // ��� - ������ �������/��������
//          Call_CB(sVxCallT.ByteInfoKadr[4]);
          break;
        case 12: // ����� ��������� ����
//          Call_ImitatorRSDT(NumbAb_NChanV,sVxCallT.ByteInfoKadr[4],&DanSwetKonf);
          break;
        case 15: // ����� �� �������� ����
          if(sVxCallT.ByteInfoKadr[4]==10) IncomingCall_PPSR_RSDT(wNChan,PPSR); // ==10 - ����������� ��������
          break;
        case 16 :
        case 17 :
        case 18 :
        case 19 :
        case 20 :
        case 21 :
        case 22 :
        case 23 : // �������� ����� ���� ���������
//          InCall_Special(sVxCallT.ByteInfoKadr[4],sVxCallT.ByteInfoKadr[3]+7,NumbAb_NChanV);
          break;
        case EndSound:
          EndSound2ConnectionPoint(wNChan);
          break;
        case CallISDN:
          ISDN_Event(wNChan,&sVxCallT);
          break;
        case CompPult: // ������� ������������� ������
          IPPult_Event(sVxCallT.ByteInfoKadr+4);
          break;
        case CallIPPM:
          IPPM_Event(&sVxCallT);
          break;
        default: break;
    }
  }
}

void DestroyAllConnections2Pults(void)
{
  unsigned char PultNb,btInd;
  ABONENT_STATE_TP *psAbnt;

  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if(IsNeedDestroyAllConnections2Pult(PultNb))
    {
      btInd=0;
      do
      {
        psAbnt=GetAbonentStatePtrFromIndex(btInd);
        if(psAbnt && (psAbnt->wConnectionPoint!=0xffff) && (psAbnt->btPultNb==PultNb))
        { OtboiAbonent(psAbnt->btPultNb,psAbnt->btKey,psAbnt->btLayer); }
        btInd++;
      }
      while(psAbnt);
      DestroySelector(PultNb);
      DeleteAllConference2Pult(PultNb);
    }
  }
}

/////////////////////////////////////////////////////////////////////
void vTaskCall(void *pvParameters) 
{
  InitAbonentStateArray();
  InitKeyVars();
  InitDisplayVars();
  InitCrossCommute();
  InitFreeHandConf();

  InitPults(true); // ������������� ���� �������
  while(1)
  {
    if(IsNeedInitPults()) InitPults(false);
    DestroyAllConnections2Pults();
    ShowCurrentKbdState();
    ProcessKeyQueue();
    ProcessQueueVxCall();
    BellOnOffControl();
    PutAbonentsState2AllPult();
    DetectAllTimers();
    ListMU2IPPM_Process();
    IPPM_IRPTOut();
    SetWDState(d_WDSTATE_TaskCall);
    vTaskDelay(5);
  }
}

