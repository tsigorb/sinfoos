#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

/////////////////////////////////////////////////////////////////////
// sc16is740 registers
#define THR_RHR     0x00
#define IER         0x01
#define FCR_IIR     0x02
#define LCR         0x03
#define MCR         0x04
#define LSR         0x05
#define MSR_TCR     0x06
#define SPR_TLR     0x07
#define TXLVL       0x08
#define RXLVL       0x09
#define IODir       0x0A
#define IOState     0x0B
#define IOIntEna    0x0C
#define reserved    0x0D
#define IOControl   0x0E
#define EFCR        0x0F

// Special register set (LCR[7]=1 and LCR!=0xbf)
#define DLL         0x00
#define DLM         0x01

// Enhanced register set (LCR=0xbf)
#define EFR         0x02
#define XON1        0x04
#define XON2        0x05
#define XOFF1       0x06
#define XOFF2       0x07

/////////////////////////////////////////////////////////////////////
void SPI_SpeedNormal(void);
void SPI_SpeedSlow(void);
bool SPIO_Wr(unsigned short Instr);
bool SPIO_WrRd(unsigned short Instr,unsigned short *DanFlash);

/////////////////////////////////////////////////////////////////////
extern xQueueHandle xKeyProcessing; // ������� �������� ������� ������ �� ���������
extern xQueueHandle xPort485RX; // ������� �� �����

/////////////////////////////////////////////////////////////////////
#define d_RS485_SB_TOUT   150

static bool s_bST16C1550;

static unsigned char kbReplyArray[LengthRX485]; // ������ ������
static unsigned char kbCmdArray[LengthTX485]={0xE3}; // ������ ����� �������
static unsigned char SIN[NumberKeyboard]; // ������ ���������� ��� ���������

static xSemaphoreHandle s_RS485Lock=NULL;

static volatile bool s_bNeedRcv=false,
                     s_bRcvOK=false,
                     s_bSendOK=false;
static volatile unsigned char s_btSend;
static volatile short CountTX=0,CountRX=0;

/////////////////////////////////////////////////////////////////////
void Rs485CS_Low(void);
void Rs485CS_Hi(void);

unsigned char SPI_RegRd(unsigned char address)
{
  unsigned short data;
  Rs485CS_Low(); // enable slave chip select
  SPIO_Wr(0x80 | (address<<3)); // read mode and address is sent
  SPIO_WrRd(0,&data); // data is sent
  Rs485CS_Hi(); // disable slave chip select
  return data;
}

void SPI_RegWr(unsigned char address,unsigned char data)
{ // writes data to SC16IS750 register
  Rs485CS_Low(); // enable slave chip select
  SPIO_Wr(address<<3); // address is sent
  SPIO_Wr(data); // data is sent
  Rs485CS_Hi(); // disable slave chip select
}

void SetRS485Busy(bool bBusy)
{
  if(bBusy)
  {
    if(!s_bST16C1550)
    {
      xSemaphoreTake(g_SPILock);
      SPI_SpeedSlow();
    }
  }
  else
  {
    *pFIO_MASKA_C=PF1; // ������ ���������� �� PF1 (RS-485)
    ssync();
    if(!s_bST16C1550)
    {
      SPI_SpeedNormal();
      xSemaphoreGive(g_SPILock);
    }
  }
}

bool IsST16C1550(void)
{ return s_bST16C1550; }

void InitRS485(void)
{
  unsigned char btLSR;

  btLSR=*pLSR1;
  ssync();
  if(btLSR==0x60)
  { // ������������� ST16C1550
    s_bST16C1550=true;
    *pLCR1=0x80;
    *pDLL1=p4r38400_ST16C1550; // ������� ������ 3686.4 ���, �������� 38400
    *pDLM1=0;
    *pLCR1=3; // 8 data bit, 1 stop bit, no parity
    *pISR1=0x06; // ������� ������� FIFO � ������ FIFO
    *pMCR1=0xf; // ��������� RTS � DTR, OP1 (��� ����� ��� �/�� �� ��������!)
    *pIER1=0x01; // ���������� ���������� �� ������
    ssync();
SendStr2IpLogger("$KBD$InitRS485: Old MU (ST16C1550 detected)!");
  }
  else
  { // ������������� sc16is740
    s_bST16C1550=false;
    SetRS485Busy(true); 
    SPI_RegWr(LCR, 0x80); // 0x80 to program baud rate
    SPI_RegWr(DLL, p4r38400_SC16IS740); // ������� ������ 1843.2 ���, �������� 38400
    SPI_RegWr(DLM, 0x00);
    SPI_RegWr(LCR, 0x03); // 8 data bit, 1 stop bit, no parity
    SPI_RegWr(FCR_IIR, 0x06); // reset TXFIFO, reset RXFIFO, non FIFO mode
    SPI_RegWr(IER, 0x01); // ���������� ���������� �� ������
    SetRS485Busy(false); 
  }
}

void setRs485_Data(unsigned char btData)
{
  if(s_bST16C1550)
  {
    *pTHR1=btData;
    ssync();
  }
  else SPI_RegWr(THR_RHR,btData);
}

unsigned char getRs485_Data(void)
{
  unsigned char btData;

  if(s_bST16C1550)
  {
    btData=*pRHR1;
    ssync();
  }
  else btData=SPI_RegRd(THR_RHR);
  return btData;
}

unsigned char getRs485_ISR(void)
{
  unsigned char btData=1; // ��� ����������!
  if(s_bST16C1550)
  {
    btData=*pISR1;
    ssync();
  }
  else
  {
    if(*pSPI_BAUD==7) btData=SPI_RegRd(FCR_IIR); // ���� ����� �� RS-485
  }
  return btData;
}

void transmit_enable_rs485( void)
{
  s_bNeedRcv=false;
  s_bSendOK=false;
  s_bRcvOK=false;
  kbReplyArray[0]=0;
  kbReplyArray[1]=0;
  CountTX=1;
  CountRX=0;
  unsigned int dwIRQ=cli();
  *pFIO_FLAG_S=PF9; // 1 �� ��9 - �������� �� 485
  *pFIO_MASKA_S=PF1; // ���������� ���������� �� PF1 (RS-485)
  ssync();
  sti(dwIRQ);
  setRs485_Data(kbCmdArray[0]); // ��� ������ �������� �� �����������
}

void RcvByte_Rs485(void)
{ // ���������� �� ������ ��������� � ����� �������� ����� - ����������� �����!!!
  unsigned char cByte;
  static unsigned char LnRcvPckt;

  cByte=getRs485_Data(); // ����� �������
  if(!s_bNeedRcv)
  { // ������� ����������� ���������� ���� - ����� ���������� ���������
    if(CountTX<=kbCmdArray[1])
    {
      setRs485_Data(kbCmdArray[CountTX]);
    }
    if(CountTX>kbCmdArray[1])
    { // ��������� �������� ���������� ����� �������!
      *pFIO_FLAG_C=PF9; //0 �� ��9 - ���������� �������� �� 485
      ssync();
      s_bNeedRcv=true;
      s_bSendOK=true;
    }
    else CountTX++;
    return;
  }
  if(CountRX==0)
  { // ������ ������
    if(cByte!=0xE4)
    { CountErrPaket++; return; }
    LnRcvPckt=LengthRX485;
  }
  if(CountRX>=LengthRX485)
  {
    *pFIO_MASKA_C=PF1; // ������ ���������� �� PF1 (RS-485)
    ssync();
    CountErrPaket++;
    s_bNeedRcv=false;
    return;
  }
  kbReplyArray[CountRX]=cByte;
  if(CountRX==1) LnRcvPckt=cByte; // ����� ������
  else
  {
    if(CountRX==LnRcvPckt)
    { // ������ ����� ������
      *pFIO_MASKA_C=PF1; // ������ ���������� �� PF1 (RS-485)
      ssync();
      s_bNeedRcv=false;
      CountPaket++;
      s_bRcvOK=true;
      return;
    }
  }
  CountRX++;
}

//extern unsigned int dwIRQ_PFA_Count;
signed portBASE_TYPE xRS485PutInfo(unsigned char SN)
{
  unsigned int dwMaxTm;
  
  if(SN>=16) return(pdFAIL);
  // ��������� ����� /////////////////////////////////////
  dwMaxTm=xTaskGetTickCount()+150; //kbCmdArray[1]*5; // 5���� �� ����
  transmit_enable_rs485(); // ���������� � ������ �������� ������
  while(IsCurrentTickCountLT(dwMaxTm))
  {
    if(s_bSendOK)
    {
      CountPaketSend++; //���������� ������������ �������
      return(pdPASS);
    }
    vTaskDelay(5);
  }
  unsigned int dwIRQ=cli();

  *pFIO_FLAG_C=PF9; // 0 �� ��9 - ���������� �������� �� 485
  *pFIO_MASKA_C=PF1; // ������ ���������� �� PF1 (RS-485)
  ssync();
  sti(dwIRQ);
SendStr2IpLogger("$KBD$xRS485PutInfo: Kbd %d error!",SN);
  return(pdFAIL);
}

bool RcvPckt_RS485(unsigned int dwTOut)
{
  unsigned int dwMaxTm;

  dwMaxTm=xTaskGetTickCount()+dwTOut;
  while(1)
  {
    if(s_bRcvOK) return true;
    if(dwTOut)
    {
      if(IsCurrentTickCountGT(dwMaxTm))
      {
        unsigned int dwIRQ=cli();
        *pFIO_MASKA_C=PF1; // ������ ���������� �� PF1 (RS-485)
        ssync();
        sti(dwIRQ);
        return false;
      }
    }
    vTaskDelay(10);
  }
}

unsigned char kb_calculate_crc_8(unsigned char *addr, char count)
{
  unsigned char i,j,crc=0xFF;

  while(count--)
  {
    j=*addr++;
    for(i=0;i<8;i++)
    {
      if((crc ^ j) & 1)
      {
        crc >>= 1;
        crc ^= 0x8c;
      } 
      else crc >>= 1;
      j >>= 1;
    }
  }
  return crc;
}

// ������ �� ������� ������� ������ �� ����������
signed portBASE_TYPE xRS485GetChar(unsigned char SN,portTickType xBlockTime)
{
  unsigned char CRC;

  if(RcvPckt_RS485(xBlockTime))
  {
    CRC=kb_calculate_crc_8(kbReplyArray+1,kbReplyArray[1]-1);
    if(CRC==kbReplyArray[kbReplyArray[1]])
    { LedToggle(ledPult); return pdTRUE; }
    else SendStr2IpLogger("$KBD$xRS485GetChar: Kbd %d error CRC (%d!=%d)!",SN,CRC,kbReplyArray[kbReplyArray[1]]);
  }
  else SendStr2IpLogger("$KBD$xRS485GetChar: Kbd %d rcv timeout (CountRX=%d (%d), %02x)!",
                         SN,CountRX,kbReplyArray[1],kbReplyArray[0]);
  return pdFALSE;
}

// ��������� ������ ����������
void BellOn(unsigned char PultNb,unsigned char NumberTone)
{
  sIndic sKBIndic;

  sKBIndic.PultNb=PultNb;
  sKBIndic.AdrSwet=0xff;
  sKBIndic.Buf[0]=5;
  sKBIndic.Buf[1]=MassM1[PultNb];
  sKBIndic.Buf[2]=0x81;
  sKBIndic.Buf[3]=NumberTone;
  sKBIndic.Buf[4]=kb_calculate_crc_8(sKBIndic.Buf,4);
  xQueueSend(xKeyIndic,&sKBIndic,0,0);
}

// ���������� ������ ����������
void BellOff(unsigned char PultNb)
{
  sIndic sKBIndic;

  sKBIndic.PultNb=PultNb;
  sKBIndic.AdrSwet=0xff;
  sKBIndic.Buf[0]=4;
  sKBIndic.Buf[1]=MassM1[PultNb];
  sKBIndic.Buf[2]=0x82;
  sKBIndic.Buf[3]=kb_calculate_crc_8(sKBIndic.Buf,3);
  xQueueSend(xKeyIndic,&sKBIndic,0,0);
}

// ��������� / ���������� ������ ��/��/��������
void OnOffRejim(unsigned char PultNb, unsigned char Cmd,unsigned char OnOff) 
{
  sIndic sKBIndic;
  unsigned char SN=MassM1[PultNb];

  if(SN>=16) return;
  sKBIndic.PultNb=PultNb;
  sKBIndic.AdrSwet=0xff;
  sKBIndic.Buf[0]=5;
  sKBIndic.Buf[1]=SN;
  while(1)
  {
    switch(Cmd)
    {
      case DS: 
        sKBIndic.Buf[2]=0x83;
        Cmd=RS;
        break; 
      case RS: 
        sKBIndic.Buf[2]=0x84;
        Cmd=DS;
        break; 
      case MIKE: 
        sKBIndic.Buf[2]=0x85;
        break; 
    }
    sKBIndic.Buf[3]=OnOff;
    sKBIndic.Buf[4]=kb_calculate_crc_8(sKBIndic.Buf,4);
    xQueueSend(xKeyIndic,&sKBIndic,0,0);
    if((Cmd==MIKE) || (OnOff==OFF)) return;
    OnOff=OFF;
  }
}

//����� ������ �� �����
void OutputString(unsigned char PultNb, const char *StrOutput, unsigned char Font, unsigned char X, unsigned char Y) 
{
  unsigned char LStr,Fn,
                btBuf[30],
                SN=MassM1[PultNb];

  LStr=strlen(StrOutput);
  if(LStr>21) LStr=21;
  btBuf[0]=0x0b;
  btBuf[1]=0xe3;
  btBuf[2]=LStr+6;
  btBuf[3]=SN;
  btBuf[4]=0x90;
  memcpy(btBuf+5,StrOutput,LStr);
  if(Font) Fn=0xF0;
  else Fn=0;
  btBuf[LStr+5]=Y | Fn;
  btBuf[LStr+6]=X;
  btBuf[LStr+7]=kb_calculate_crc_8(btBuf+2,LStr+5);
  if(SN<16)
  {
    sIndic sKBIndic;
    sKBIndic.PultNb=PultNb;
    sKBIndic.AdrSwet=0xff;
    memcpy(sKBIndic.Buf,btBuf+2,btBuf[2]);
    xQueueSend(xKeyIndic,&sKBIndic,0,0);
  }
  else 
  {
    unsigned int dwIP;
    unsigned short wPort;
    static sInfoKadr sInfo;

    if(GetAddressIpPult(SN,&dwIP,&wPort))
    {
      vPortEnableSwitchTask(false);
      sInfo.Source=UserUDP;
      sInfo.A1=dwIP;
      sInfo.PORT1=wPort;
      sInfo.L=LStr+8;
      memcpy(sInfo.ByteInfoKadr,btBuf,LStr+8);
      xQueueSend(xQueueReply[UserUDP],&sInfo,0,0);
      vPortEnableSwitchTask(true);
    }
  }
}

// ������� ������
void ClearScreen(unsigned char PultNb) 
{
  unsigned char SN=MassM1[PultNb];

  if(SN<16)
  {
    sIndic sKBIndic;

    sKBIndic.PultNb=PultNb;
    sKBIndic.AdrSwet=0xff;
    sKBIndic.Buf[0]=4;
    sKBIndic.Buf[1]=SN;
    sKBIndic.Buf[2]=0x91;
    sKBIndic.Buf[3]=kb_calculate_crc_8(sKBIndic.Buf,3);
    xQueueSend(xKeyIndic,&sKBIndic,0,0);
  }
  else
  {
    unsigned int dwIP;
    unsigned short wPort;
    static sInfoKadr sInfo;

    if(GetAddressIpPult(SN,&dwIP,&wPort))
    {
      vPortEnableSwitchTask(false);
      sInfo.Source=UserUDP;
      sInfo.A1=dwIP;
      sInfo.PORT1=wPort;
      sInfo.L=6;
      sInfo.ByteInfoKadr[0]=0x0b;
      sInfo.ByteInfoKadr[1]=0xe3;
      sInfo.ByteInfoKadr[2]=4;
      sInfo.ByteInfoKadr[3]=SN;
      sInfo.ByteInfoKadr[4]=0x91; 
      sInfo.ByteInfoKadr[5]=kb_calculate_crc_8(sInfo.ByteInfoKadr+2,3);
      xQueueSend(xQueueReply[UserUDP],&sInfo,0,0);
      vPortEnableSwitchTask(true);
    }
  }
}

// ��������� �����
void IndicClock(unsigned char PultNb) 
{
  unsigned char SN=MassM1[PultNb];

  ClearScreen(PultNb);
  if(IsKbdUnlock(PultNb))
  {
    sIndic sKBIndic;
    if(SN>=16) return;
    sKBIndic.PultNb=PultNb;
    sKBIndic.AdrSwet=0xff;
    sKBIndic.Buf[0]=4;
    sKBIndic.Buf[1]=SN;
    sKBIndic.Buf[2]=0x98;
    sKBIndic.Buf[3]=kb_calculate_crc_8(sKBIndic.Buf,3);
    xQueueSend(xKeyIndic,&sKBIndic,0,0);
  }
  else
  {
    OutputString(PultNb,"     ����������",0,3,1);
    OutputString(PultNb,"   �������������!",0,3,2);
  }
}

// ���������� ������� ��� ������
/*bool ReadInf(unsigned char PultNb,unsigned char *BufA4)
{
  bool ex=false;
  unsigned char SN=MassM1[PultNb];

  if(SN>=16) return(false);
  SetRS485Busy(true); 
  kbCmdArray[1]=4;
  kbCmdArray[2]=SN;
  kbCmdArray[3]=0xa4;
  kbCmdArray[4]=kb_calculate_crc_8(kbCmdArray+1,3);
  if(xRS485PutInfo(SN))
  {
    if(xRS485GetChar(SN,d_RS485_SB_TOUT)) 
    {
      memcpy(BufA4,kbReplyArray+3,8);
      ex=true;  
    }
  }
  SetRS485Busy(false); 
  return ex;
}*/

// ������������� ������
void InitKeyboard(unsigned char SN)
{
  sIndic sKBIndic;

  if((SN>=16) && (SN!=0xff)) return;
  // !! sKBIndic.PultNb=NO DEFINED !!
  sKBIndic.AdrSwet=0xff;
  sKBIndic.Buf[0]=4;
  sKBIndic.Buf[1]=SN;
  sKBIndic.Buf[2]=0x05;
  sKBIndic.Buf[3]=kb_calculate_crc_8(sKBIndic.Buf,3);
  xQueueSend(xKeyIndic,&sKBIndic,0,0);
}

// ��������� �������
bool SetClockPult(unsigned char PultNb, struct tm *ptm) 
{
  bool result=false;
  unsigned char SN=MassM1[PultNb];

  if(SN<16)
  {
    sIndic sKBIndic;

    sKBIndic.PultNb=PultNb;
    sKBIndic.AdrSwet=0xff;
    sKBIndic.Buf[0]=11;
    sKBIndic.Buf[1]=SN;
    sKBIndic.Buf[2]=0xa0;
    sKBIndic.Buf[3]=(unsigned char)(ptm->tm_year - 100); // ���
    sKBIndic.Buf[4]=(unsigned char)ptm->tm_mon+1; // �����
    sKBIndic.Buf[5]=(unsigned char)ptm->tm_mday; // ����
    sKBIndic.Buf[6]=(unsigned char)ptm->tm_wday; // ���� ������
    sKBIndic.Buf[7]=(unsigned char)ptm->tm_hour; // ��� 
    sKBIndic.Buf[8]=(unsigned char)ptm->tm_min; // ������
    sKBIndic.Buf[9]=(unsigned char)ptm->tm_sec; // ������� 
    sKBIndic.Buf[10]=kb_calculate_crc_8(sKBIndic.Buf,10);
    xQueueSend(xKeyIndic,&sKBIndic,0,0);
    result=true;
  }
  else
  {
    unsigned int dwIP;
    unsigned short wPort;
    static sInfoKadr sInfo;

    if(GetAddressIpPult(SN,&dwIP,&wPort))
    {
      vPortEnableSwitchTask(false);
      sInfo.Source=UserUDP;
      sInfo.A1=dwIP;
      sInfo.PORT1=wPort;
      sInfo.L=13;
      sInfo.ByteInfoKadr[0]=0x0b;
      sInfo.ByteInfoKadr[1]=0xe3;
      sInfo.ByteInfoKadr[2]=11;
      sInfo.ByteInfoKadr[3]=SN;
      sInfo.ByteInfoKadr[4]=0xA0;
      sInfo.ByteInfoKadr[5]=(unsigned char)(ptm->tm_year - 100); // ���
      sInfo.ByteInfoKadr[6]=(unsigned char)ptm->tm_mon+1; // �����
      sInfo.ByteInfoKadr[7]=(unsigned char)ptm->tm_mday; // ����
      sInfo.ByteInfoKadr[8]=(unsigned char)ptm->tm_wday; // ���� ������
      sInfo.ByteInfoKadr[9]=(unsigned char)ptm->tm_hour; // ��� 
      sInfo.ByteInfoKadr[10]=(unsigned char)ptm->tm_min; // ������
      sInfo.ByteInfoKadr[11]=(unsigned char)ptm->tm_sec; // ������� 
      sInfo.ByteInfoKadr[12]=kb_calculate_crc_8(sInfo.ByteInfoKadr+2,10);
      xQueueSend(xQueueReply[UserUDP],&sInfo,0,0);
      vPortEnableSwitchTask(true);
    }
  }
  return result;
}

// ��������� ������� � ����������
void ClockIndPult(unsigned char PultNb,struct tm *ptm) 
{
  if(SetClockPult(PultNb,ptm)) IndicClock(PultNb);
}

// ��������� ���������� � �������� ���� �� ���������� � �������� ������� SN 
// ���� ���������� DanSwet. ���������� ������ �� ������������ W
void Indic_Sound(sIndic *psKBIndicS)
{
  unsigned char SN;

  if(psKBIndicS->AdrSwet==0xff)
  { // �������� ������� Buf
    memcpy(kbCmdArray+1,psKBIndicS->Buf,psKBIndicS->Buf[0]);
    SN=psKBIndicS->Buf[1];
    if(SN==0xff) SN=0; // ���� ����������������� ����� ���������� �������, �� SN=0 ����� ������� xRS485PutInfo!
    SetRS485Busy(true);
    if(xRS485PutInfo(SN))
    {
      if(!SN)
      { SetRS485Busy(false); vTaskDelay(6000); return; }
      else
      {
        if(!xRS485GetChar(SN,d_RS485_SB_TOUT)) CountNoReply++;
      }
    }
    SetRS485Busy(false);
  }
  else
  { // ����� ��������� ��� ����
    SN=MassM1[psKBIndicS->PultNb];
    if(!IsKbdReady(psKBIndicS->PultNb)) return;
    SetRS485Busy(true);
    if(psKBIndicS->DanSwet!=NO_INDIC)
    { // ����� ���������
      kbCmdArray[1]=6;
      kbCmdArray[2]=SN;
      kbCmdArray[3]=0x50;
      kbCmdArray[4]=psKBIndicS->AdrSwet;
      kbCmdArray[5]=psKBIndicS->DanSwet;
      kbCmdArray[6]=kb_calculate_crc_8(kbCmdArray+1,5);
      if(xRS485PutInfo(SN))
      {
//      if((kbCmdArray[3]==0x50) && (kbCmdArray[2]==1) && (kbCmdArray[5]==2)) Count50++;
        if(!xRS485GetChar(SN,d_RS485_SB_TOUT))
        { CountNoReply++; SetRS485Busy(false); return; }
      }
    }
    if(psKBIndicS->CC_Sound)
    { // ����� ����
      kbCmdArray[1]=6;
      kbCmdArray[2]=SN;
      kbCmdArray[3]=0x59;
      kbCmdArray[4]=psKBIndicS->CC_Sound;
      kbCmdArray[5]=psKBIndicS->W_Sound;
      kbCmdArray[6]=kb_calculate_crc_8(kbCmdArray+1,5);
      if(xRS485PutInfo(SN))
      {
        if(!xRS485GetChar(SN,d_RS485_SB_TOUT)) CountNoReply++;
      }
    }
    SetRS485Busy(false);
  }
}

// ������ ������ ����������
// return: 0 - ������
//         1 - � ������ ���������� ������ ������
//         2 - � ������ ���������� ����� ������
unsigned char ReadKbdBuf(unsigned char PultNb,unsigned char SN)
{
  kbCmdArray[1]=5;
  kbCmdArray[2]=SN;
  kbCmdArray[3]=0x5a;
  kbCmdArray[4]=SIN[PultNb];
  kbCmdArray[5]=kb_calculate_crc_8(kbCmdArray+1,4);
  if(xRS485PutInfo(SN))
  { 
    if(xRS485GetChar(SN,d_RS485_SB_TOUT))
    {
      if(kbReplyArray[4]!=SIN[PultNb])
      { SIN[PultNb]=kbReplyArray[4]; return(2); }
      return(1);
    }
    else return(0);
  }
  else return(0);
}

// ����� ���������
bool InterviewKeyboard(unsigned char PultNb,unsigned char SN)
{
  unsigned char btKbdResult,btKeyCode,btKbdBufSz,
                i,btIndex;
  bool bNeedNextRead;
  PRESS_KEY_TP sKeyPr;

  SetRS485Busy(true);
  bNeedNextRead=false;
  sKeyPr.CodeKey[0]=0;  // !!����� ���������� �� ������� ��������������!!
  do
  {
    btKbdResult=ReadKbdBuf(PultNb,SN);
    if(!btKbdResult)  // ���������� �� �������� 
    { CountErrReadBuf++; SetRS485Busy(false); return false; }
    btKbdBufSz=kbReplyArray[1]-5;
    if((btKbdResult==1) || !btKbdBufSz)
    { SetRS485Busy(false); return(true); }
    if(bNeedNextRead)
    {
      bNeedNextRead=false;
      kbReplyArray[4]=sKeyPr.CodeKey[0];
      btKbdBufSz++;
      btIndex=4;
    }
    else btIndex=5;
    for(i=0;i<btKbdBufSz;i+=2)
    {
      sKeyPr.SN=SN;
      sKeyPr.CodeKey[0]=kbReplyArray[btIndex+i];
      if(i+1<btKbdBufSz)
      {
        sKeyPr.CodeKey[1]=kbReplyArray[btIndex+1+i];
        if(xQueueSend(xKeyProcessing,&sKeyPr,0,0)==errQUEUE_FULL)
        { CountErrSendKeyPr++; SetRS485Busy(false); return true; }
      }
      else bNeedNextRead=true;
    }
  }
  while(bNeedNextRead);
  SetRS485Busy(false);
  return true;
}

