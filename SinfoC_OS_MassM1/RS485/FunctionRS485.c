#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"
#include "SelectorN.h"

//*******************************************************************
extern unsigned char MassLayer[NumberKeyboard];

//*******************************************************************
void DEBUG_Pisk(unsigned char PultNb,char chCntPisk)
{
  sIndic sKBIndicI;
  
  sKBIndicI.PultNb=PultNb;
  sKBIndicI.DanSwet=NO_INDIC;
  sKBIndicI.CC_Sound=chCntPisk;
  sKBIndicI.W_Sound=1;
  xQueueSend(xKeyIndic,&sKBIndicI,0,0);
}
//*******************************************************************

/////////////////////////////////////////////////////////////////////
static volatile unsigned char MasVirtualLed[NumberKeyboard][NumberGroup][NumberKey];

/////////////////////////////////////////////////////////////////////
unsigned char GetCurrentLayer(unsigned char PultNb)
{
  if(MassM1[PultNb]>=16) return(0);
  return(MassLayer[PultNb]);
}

unsigned char GetMaxLayer(unsigned char PultNb)
{
  if(MassM1[PultNb]>=16) return(1);
  return(NumberGroup);
}

void ResetVirtualLed4Layer(unsigned char PultNb,unsigned char btLayer)
{
  unsigned char i;

  for(i=0;i<NumberKey;i++) MasVirtualLed[PultNb][btLayer][i]=0;
}

void ResetVirtualLed4Pult(unsigned char PultNb)
{
  unsigned char btLayer,i;

  for(btLayer=0;btLayer<NumberGroup;btLayer++)
  {
    for(i=0;i<NumberKey;i++) MasVirtualLed[PultNb][btLayer][i]=0;
  }
}

void ResetMasVirtualLed(void)
{ memset(MasVirtualLed,0,sizeof(MasVirtualLed)); }

unsigned char GetPultIndFromSN(unsigned char SN)
{
  unsigned char i;

  if(!SN || (SN==0xff)) return(0xff);
  if(SN>100) SN-=100;
  for(i=0;i<NumberKeyboard;i++)
  {
    if(MassM1[i]==SN) return(i);
  }
  return(0xff);
}

void SimplePiskPult(unsigned char PultNb)
{
  sIndic sKBIndicI;
  
  sKBIndicI.PultNb=PultNb;
  sKBIndicI.AdrSwet=NO_LED;
  sKBIndicI.DanSwet=NO_INDIC;
  sKBIndicI.CC_Sound=1;
  sKBIndicI.W_Sound=1;
  if(xQueueSend(xKeyIndic,&sKBIndicI,0,0)==errQUEUE_FULL) CountErrSendIndic++;
}

void xSendKeyState104ToIpPult(unsigned char SN,unsigned char btKey,unsigned char btState)
{
  static sInfoKadr sInfo;

  if(GetAddressIpPult(SN,&sInfo.A1,&sInfo.PORT1))
  {
    sInfo.Source=UserUDP;
    sInfo.L=5;
    sInfo.ByteInfoKadr[0]=104;
    sInfo.ByteInfoKadr[1]=SN;
    sInfo.ByteInfoKadr[2]=1;
    sInfo.ByteInfoKadr[3]=btKey;
    sInfo.ByteInfoKadr[4]=btState;
    xQueueSend(xQueueReply[UserUDP],&sInfo,0,0);
  }
}

void xSendKeyState108ToIpPult(unsigned char SN,unsigned char btKey,unsigned char btState)
{
  unsigned int dwIP;
  unsigned short wPort;
  static sInfoKadr sInfo;

  if(GetAddressIpPult(SN,&dwIP,&wPort))
  {
    sInfo.Source=UserUDP;
    sInfo.A1=dwIP;
    sInfo.PORT1=wPort;
    sInfo.L=5;
    sInfo.ByteInfoKadr[0]=108;
    sInfo.ByteInfoKadr[1]=SN;
    sInfo.ByteInfoKadr[2]=1;
    sInfo.ByteInfoKadr[3]=btKey;
    sInfo.ByteInfoKadr[4]=btState;
    xQueueSend(xQueueReply[UserUDP],&sInfo,0,0);
  }
}

void PutKeyState2IpPult(unsigned char PultNb,unsigned char btKey,unsigned char DanSwet)
{
  unsigned char SN,btLayer;
  sKBKey *psKey;

  if(DanSwet==NO_INDIC) return;
  btLayer=GetCurrentLayer(PultNb);
  psKey=&MassKey[PultNb][btLayer][btKey];
  SN=MassM1[PultNb];
  switch(psKey->CodeKey[0])
  {
    case ServiceKey:
    {
      if((psKey->CodeKey[1]>=Selector1) && (psKey->CodeKey[1]<=Selector16))
      {
        DanSwet=GetSelectorState(PultNb,btKey);
        if(DanSwet==NO_INDIC) return;
      }
      else
      {
        if(psKey->CodeKey[1]==BlockKey) DanSwet=(GetKbdUnlockState(PultNb) ? Off : d_ServiceOn);
        else
        {
          if((psKey->CodeKey[1]>=Conference1) && (psKey->CodeKey[1]<=Conference6))
          { DanSwet=GetConferenceState(PultNb,psKey->CodeKey[1]-Conference1); }
          else
          {
            if(DanSwet) DanSwet=d_ServiceOn;
          }
        }
      }
      break;
    }
    case CallKey:
    {
      if(IsSelectorActive(PultNb) && IsKey2Selector(PultNb,btKey,btLayer,d_Abonent2SelectorCurrentPult))
      {
        DanSwet=GetAbonentSelectorState(PultNb,btKey,btLayer);
        xSendKeyState108ToIpPult(SN,btKey,DanSwet);
      }
      return;
    }
    default: return;
  }
  // ����., �������!!!
  xSendKeyState104ToIpPult(SN,btKey,DanSwet);
}

void PutLed2Pult(sIndic *psKBIndicM)
{
  unsigned char PultNb=psKBIndicM->PultNb;

  if(MassM1[PultNb]<16)
  {
    if(xQueueSend(xKeyIndic,psKBIndicM,0,0)==errQUEUE_FULL) CountErrSendIndic++;
    else
    {
      unsigned char btLayer=GetCurrentLayer(PultNb),
                    AdrSwet=psKBIndicM->AdrSwet;
      if((MassKey[PultNb][btLayer][AdrSwet].Flags & 0x80) &&
         (AdrSwet<120) &&
         (MassKey[PultNb][btLayer][AdrSwet+8].CodeKey[0]==0))
      {
        psKBIndicM->AdrSwet+=8;
        psKBIndicM->CC_Sound=0;
        psKBIndicM->W_Sound=0;
        xQueueSend(xKeyIndic,psKBIndicM,0,0);
      }
    }
  }
}

void IndicCmd(unsigned char PultNb,unsigned char btLayer,unsigned char btKey,unsigned char DanSwet,
              unsigned char CC_Sound,unsigned char W_Sound)
{
  sIndic sKBIndicI;

  if((DanSwet!=NO_INDIC) && (btKey<NumberKey))
  {
    if(DanSwet==(MasVirtualLed[PultNb][btLayer][btKey] & 0x7f)) DanSwet=NO_INDIC;
    else
    {
      MasVirtualLed[PultNb][btLayer][btKey]=DanSwet;
      if(btLayer!=GetCurrentLayer(PultNb))
      {
        DanSwet=NO_INDIC;
        MasVirtualLed[PultNb][btLayer][btKey]|=0x80;
      }
    }
  }
  if((DanSwet==NO_INDIC) && (CC_Sound==0)) return;
  if(MassM1[PultNb]>=16)
  {
    if((btKey<NumberKey) && (DanSwet!=NO_INDIC)) PutKeyState2IpPult(PultNb,btKey,DanSwet);
  }
  else
  {
    sKBIndicI.PultNb=PultNb;
    sKBIndicI.AdrSwet=btKey;
    sKBIndicI.DanSwet=DanSwet;
    sKBIndicI.CC_Sound=CC_Sound;
    sKBIndicI.W_Sound=W_Sound;
    PutLed2Pult(&sKBIndicI);
/*SendStr2IpLogger("$DEBUG$IndicCmd: PultNb=%d, Key=%d, Flags=%x",
                 PultNb,btKey,MassKey[PultNb][btLayer][btKey].Flags);*/
  }
}

bool OtboiAbonentCommonPart(unsigned char PultNb,unsigned char Key,unsigned char Layer,
                            unsigned short *pwConnectionPoint,unsigned short *pwOperatorChanel)
{
  ABONENT_STATE_TP *psAbnt;

  IndicCmd(PultNb,Layer,Key,OFF,0,0);
  psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,Layer);
  if(!psAbnt) return false;
  if(pwOperatorChanel) *pwOperatorChanel=psAbnt->wOperatorChanel;
  if(pwConnectionPoint) *pwConnectionPoint=psAbnt->wConnectionPoint;
  if((psAbnt->btType==ISDN) || (psAbnt->btType==ATC) || (psAbnt->btType==Selector))
  {
    if(GetAbonentStateFromPtr(psAbnt)==Hold) StopKeep(psAbnt->wConnectionPoint);
  }
  psAbnt->dwState=0xff;
  ClearAbonentStateByPtr(psAbnt);
  return true;
}

unsigned char CountMKALineUser(unsigned short NChan,unsigned short wOper)
{
  unsigned char btInd,btCnt;
  ABONENT_STATE_TP *psAbnt;

  btCnt=0;
  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return(btCnt);
    if((psAbnt->wConnectionPoint==NChan) && (psAbnt->wOperatorChanel==wOper)) btCnt++;
    btInd++;
  }
  while(1);
}

void OtboiAbonentMKA(unsigned char PultNb0,unsigned short wNChan,unsigned char AbntNumb,
                     unsigned char btType)
{
  unsigned char btInd,btState,PultNb;
  unsigned short wOp;
  bool bInAutoConference;
  ABONENT_STATE_TP *psAbnt;

  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return;
    btInd++;
    if((PultNb0!=0xff) && (PultNb0!=psAbnt->btPultNb)) continue;
    if((psAbnt->wConnectionPoint==wNChan) && (psAbnt->btType==btType))
    {
      PultNb=psAbnt->btPultNb;
      if(AbntNumb!=0xff)
      {
        if(psAbnt->NumbAbon!=AbntNumb) continue;
      }
      if(GetAbonentStateFromPtr(psAbnt)==CallTest)
      { // ������� TDC �������, ������ �� ��� �����
        SetAbonentStateByPtr(psAbnt,CallTestDown,0);
        psAbnt->btLedState=FAST_BLINK_GREEN;
        IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,1,1);
        psAbnt->dwTimerEnd=d_IndicTestCallResult+xTaskGetTickCount();
      }
      else
      {
        wOp=psAbnt->wOperatorChanel;
        DelAbonentFromSelector(PultNb,psAbnt->btKey,psAbnt->btLayer);
        DelAbonentFromConference(PultNb,psAbnt->btKey,psAbnt->btLayer);
        switch(btType)
        {
          case PPSR:
            if(AbntNumb==0)
            { // ������ �������� ������� ���� � ������� 0!
              _xCallPPSR(psAbnt->wConnectionPoint,ExclPPSR,0,0); // ��������� ��� �����������
              bInAutoConference=DelAbntFromFreeHandConference(PultNb,psAbnt);
              if(!bInAutoConference) Commute(false,wOp,psAbnt->wConnectionPoint);
            }
            else wOp=0;
            break;
          case RSDT:
            if(AbntNumb==0)
            {
              bInAutoConference=DelAbntFromFreeHandConference(PultNb,psAbnt);
              if(!bInAutoConference) Commute(false,wOp,psAbnt->wConnectionPoint);
            }
            else wOp=0;
            break;
          default:
//            if(CountMKALineUser(psAbnt->wConnectionPoint,wOp)==1)
            {
              bInAutoConference=DelAbntFromFreeHandConference(PultNb,psAbnt);
              if(!bInAutoConference) Commute(false,wOp,psAbnt->wConnectionPoint);
            }
            break;
        }
        if(!NeedReshowActualMKAAbntState(psAbnt)) IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKey,OFF,0,0);
        ClearAbonentStateByPtr(psAbnt);
        if(wOp) DSRS_Off(PultNb,wOp);
        if(PultNb0!=0xff) return;
      }
    }
  }
  while(1);
}

void OtboiAbonent(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  unsigned char TypeAbon=MassKey[PultNb][btLayer][Key].TypeAbon;
  unsigned short wOp=GetAbonentOperatorChannelFromKey(PultNb,Key,btLayer),
                 wCP=GetAbonentConnectionPointFromKey(PultNb,Key,btLayer);
  bool bAbnt2Selector=IsKey2Selector(PultNb,Key,btLayer,d_Abonent2SelectorCurrentPult);
  ABONENT_STATE_TP *psAbnt;

  if((TypeAbon==Selector) || (TypeAbon==ATC) || (TypeAbon==PultCall) || (TypeAbon==IPPM))
  {
    if(!FindAbonentSelector(PultNb,Key,btLayer)) Commute(false,wCP,wOp);
  }
  if((TypeAbon==ISDN) || (TypeAbon==TDC) || (TypeAbon==Selector) || (TypeAbon==ATC) || (TypeAbon==IPPM))
  {
    DelAbonentFromSelector(PultNb,Key,btLayer);
    DelAbonentFromConference(PultNb,Key,btLayer);
  }
  psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);
  if(psAbnt) DelAbntFromFreeHandConference(PultNb,psAbnt);
  switch(TypeAbon)
  {
    case IPPM:
      OtboiAbonentIPPM(PultNb,Key,btLayer);
      break;
    case ISDN:
      if(GetAbonentStateFromKey(PultNb,Key,btLayer)==Talk)
      { OtboiAbonentISDN(PultNb,Key,btLayer,PRI_CAUSE_USER_BUSY/*PRI_CAUSE_NORMAL_CLEARING*/); }
      else OtboiAbonentISDN(PultNb,Key,btLayer,PRI_CAUSE_CALL_REJECTED);
      break;
    case ATC:
      OtboiAbonentATC(PultNb,Key,btLayer);
      break;
    case TDC:
      if(GetAbonentStatePtrFromKey(PultNb,Key,btLayer))
      { OtboiAbonentMKA(PultNb,wCP,MassKey[PultNb][btLayer][Key].NumbAbon,TDC); }
      break;
    case PultCall:
      OtboiAbonentPultCall(PultNb,Key,btLayer);
      break;
    case Selector:
      OtboiAbonentSelector(PultNb,Key,btLayer);
      break;
    case Level:
      if(GetAbonentStatePtrFromKey(PultNb,Key,btLayer)) OtboiAbonentMKA(PultNb,wCP,0xff,Level);
      break;
    case PPSR:
    case RSDT:
      psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);
      if(psAbnt) OtboiAbonentMKA(PultNb,wCP,psAbnt->NumbAbon,TypeAbon);
      break;
    default:
      OtboiAbonentCommonPart(PultNb,Key,btLayer,NULL,NULL);
      break;
  }
  if(!bAbnt2Selector) DSRS_Off(PultNb,wOp);
}

void DelIncomingCallMKAFromOtherPult(unsigned char PultNb,unsigned short NChan,unsigned char AbntNumb)
{
  unsigned char btInd;
  ABONENT_STATE_TP *psAbnt;

  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return;
    btInd++;
    if((psAbnt->btPultNb!=PultNb) &&
       (psAbnt->wConnectionPoint==NChan) &&
       (GetAbonentStateFromPtr(psAbnt)==IncomingCall))
    {
      if(AbntNumb!=0xff)
      {
        if(psAbnt->NumbAbon!=AbntNumb) continue;
      }
      Commute(false,psAbnt->wOperatorChanel,psAbnt->wConnectionPoint);
      SetAbonentStateByPtr(psAbnt,Off,0);
      IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,Off,0,0);
      memset(psAbnt,0xff,sizeof(ABONENT_STATE_TP));
    }
  }
  while(1);
}

void AnswerIncomingCallMKA(ABONENT_STATE_TP *psAbnt)
{
  DelIncomingCallMKAFromOtherPult(psAbnt->btPultNb,psAbnt->wConnectionPoint,psAbnt->NumbAbon);
  DSRS_On(psAbnt->btPultNb,psAbnt->wOperatorChanel);
  Commute(true,psAbnt->wOperatorChanel,psAbnt->wConnectionPoint);
  Commute(true,psAbnt->wConnectionPoint,psAbnt->wOperatorChanel);
  SetLedColorIfTalk(psAbnt);
  SetAbonentStateByPtr(psAbnt,Talk,dIncomingTalkFlag);
  psAbnt->dwTimerEnd=0;
  IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,1,1);
}

void DeleteAllUnusedConnections2Pult(unsigned char PultNb,unsigned char btLayer)
{
  unsigned char btInd;
  ABONENT_STATE_TP *psAbnt;

//!!!!!!!!!!!!!!!!???????????????????????
  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return;
    if(psAbnt->wConnectionPoint!=0xffff)
    {
      if((psAbnt->btPultNb==PultNb) && (psAbnt->btLayer==btLayer))
      {
        OtboiAbonent(psAbnt->btPultNb,psAbnt->btKey,psAbnt->btLayer);
        vTaskDelay(20);
      }
    }
    btInd++;
  }
  while(1);
}

void DeleteAllConnections2Sinfo(void)
{
  unsigned char btInd,PultNb;
  ABONENT_STATE_TP *psAbnt;

  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return;
    if(psAbnt->wConnectionPoint!=0xffff)
    {
      PultNb=psAbnt->btPultNb;
      SetManualOtboi(PultNb,true);
      OtboiAbonent(PultNb,psAbnt->btKey,psAbnt->btLayer);
      SetManualOtboi(PultNb,false);
    }
    btInd++;
  }
  while(1);
}

void DeleteAllConnections2Pult(unsigned char PultNb)
{
  unsigned char btInd;
  ABONENT_STATE_TP *psAbnt;

  SetManualOtboi(PultNb,true);
  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt)
    { SetManualOtboi(PultNb,false); return; }
    if((psAbnt->wConnectionPoint!=0xffff) && (psAbnt->btPultNb==PultNb)) OtboiAbonent(PultNb,psAbnt->btKey,psAbnt->btLayer);
    btInd++;
  }
  while(1);
}

void DeleteAllConnections2PultLayer(unsigned char PultNb,unsigned char btLayer)
{
  unsigned char btInd;
  ABONENT_STATE_TP *psAbnt;

  SetManualOtboi(PultNb,true);
  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt)
    { SetManualOtboi(PultNb,false); return; }
    if((psAbnt->wConnectionPoint!=0xffff) &&
       (psAbnt->btPultNb==PultNb) &&
       (psAbnt->btLayer==btLayer)) OtboiAbonent(PultNb,psAbnt->btKey,btLayer);
    btInd++;
  }
  while(1);
}

void IndicateServiceKey(unsigned char PultNb)
{
  unsigned short j;
  unsigned char btLayer,CodeKey1;
  sIndic sKBIndicM;

  SetKbdUnlockState(PultNb,GetKbdUnlockState(PultNb));
  if(MassM1[PultNb]<16)
  {
    for(btLayer=0;btLayer<GetMaxLayer(PultNb);btLayer++)
    { SetLayerUnlockState(PultNb,btLayer,GetLayerUnlockState(PultNb,btLayer)); }
  }
  btLayer=GetCurrentLayer(PultNb);
  for(j=0;j<NumberKey;j++)
  {
    if(MassKey[PultNb][btLayer][j].CodeKey[0]==ServiceKey)
    {
      sKBIndicM.PultNb=PultNb;
      sKBIndicM.AdrSwet=j; 
      sKBIndicM.DanSwet=GREEN; 
      sKBIndicM.CC_Sound=0;
      sKBIndicM.W_Sound=0;
      CodeKey1=MassKey[PultNb][btLayer][j].CodeKey[1];
      switch(CodeKey1)
      {
        case BlockKey:
          if(IsKbdLock(PultNb)) sKBIndicM.DanSwet=RED;
          PutLed2Pult(&sKBIndicM);
          break;
        case Layer1:
        case Layer2:
        case Layer3:
          sKBIndicM.DanSwet=GetLayerLedColor(PultNb,CodeKey1-Layer1);
          PutLed2Pult(&sKBIndicM);
          break;
        case Transit:
          break;
        case TikkerKey:
          if(GetTikkerState(PultNb))
          {
            sKBIndicM.DanSwet=FAST_BLINK_ORANGE;
            PutLed2Pult(&sKBIndicM);
          }
          break;
        case Conference1:
        case Conference2:
        case Conference3:
        case Conference4:
        case Conference5:
        case Conference6:
          if(IsConferenceWork(PultNb,CodeKey1-Conference1)) PutLed2Pult(&sKBIndicM);
          break;
        default:
          if(CodeKey1==Tabl1+NumbTabl) PutLed2Pult(&sKBIndicM);
          if((CodeKey1>=Selector1) && (CodeKey1<=Selector16))
          {
            sKBIndicM.DanSwet=TranslateSelectorState2Led(PultNb,CodeKey1-Selector1);
            PutLed2Pult(&sKBIndicM);
          }
          break;
      }
    }
  }
}

void IndicateLayerKey(unsigned char PultNb)
{
  unsigned short j;
  unsigned char btLayer,CodeKey1;
  sIndic sKBIndicM;

  if(MassM1[PultNb]>=16) return;
  for(btLayer=0;btLayer<GetMaxLayer(PultNb);btLayer++)
  {
    for(j=0;j<NumberKey;j++)
    {
      if(MassKey[PultNb][btLayer][j].CodeKey[0]==ServiceKey)
      {
        CodeKey1=MassKey[PultNb][btLayer][j].CodeKey[1];
        switch(CodeKey1)
        {
          case Layer1:
          case Layer2:
          case Layer3:
            sKBIndicM.PultNb=PultNb;
            sKBIndicM.AdrSwet=j;
            sKBIndicM.CC_Sound=0;
            sKBIndicM.W_Sound=0;
            sKBIndicM.DanSwet=GetLayerLedColor(PultNb,CodeKey1-Layer1);
            PutLed2Pult(&sKBIndicM);
            break;
          default:
            break;
        }
      }
    }
  }
}

// ��������� ��������� ������
bool ExistOldLayerKey2NewLayer(unsigned char PultNb,unsigned char btOldLayer,unsigned char btNewLayer)
{
  unsigned char btKey,btChoice=0;

  for(btKey=0;btKey<(NumberKeyPageSize-1);btKey++)
  {
    if(MassKey[PultNb][btNewLayer][btKey].CodeKey[1]==Choice)
    { btChoice=1; break; }
  }
  if(btChoice==0) return false;
  for(btKey=0;btKey<(NumberKeyPageSize-1);btKey++)
  {
    if(MassKey[PultNb][btNewLayer][btKey].CodeKey[1]==btOldLayer+Layer1) return true;
  }
  return false;
}

void SwitchGroup(unsigned char PultNb,unsigned char btNewLayer) 
{
  unsigned char j,btNewLed,btLayer=GetCurrentLayer(PultNb);

  if(MassM1[PultNb]<16)
  {
    if(btLayer!=btNewLayer)
    {
      if(!ExistOldLayerKey2NewLayer(PultNb,btLayer,btNewLayer) && btNewLayer) return;
//      WrFlash(btNewLayer,AdrMLayer+PultNb*2);
      OutgoingNumberTmpTOutClear(PultNb);
      MassLayer[PultNb]=btNewLayer;
      if(IsCreatedTransit(PultNb)) ClearCreatedTransit(PultNb);
    }
    IndicateLayerKey(PultNb);
    PutKbdUnlockLedState(PultNb,0xff);
    for(j=0;j<NumberKey;j++)
    {
      btNewLed=MasVirtualLed[PultNb][btNewLayer][j] & 0x7f;
      if(MasVirtualLed[PultNb][btLayer][j] & 0x7f)
      {// ����� ��������� �� ����
        if((MasVirtualLed[PultNb][btLayer][j] & 0x7f)!=btNewLed)
        {
          MasVirtualLed[PultNb][btNewLayer][j]=~btNewLed;
          IndicCmd(PultNb,btNewLayer,j,btNewLed,0,0);
        }
        else MasVirtualLed[PultNb][btNewLayer][j]&=0x7f;
      }
      else
      {// �� ����� ��������� �� ����
        MasVirtualLed[PultNb][btNewLayer][j]=OFF;
        if(btNewLed) IndicCmd(PultNb,btNewLayer,j,btNewLed,0,0);
      }
    }
    InvalidateAbonentState4Pult(PultNb);
  }
}

void SetDisplayTOut(char PultNb,unsigned int TOut)
{
  ListAbntDisplay[PultNb][0].dwTOut=xTaskGetTickCount()+TOut;
}

void SendError2IpPult(unsigned char PultNb,unsigned char btErr)
{
  unsigned int dwIP;
  unsigned short wPort;
  unsigned char SN=MassM1[PultNb];
  static sInfoKadr sInfo;

  if(SN<16) return;
  if(GetAddressIpPult(SN,&dwIP,&wPort))
  {
    sInfo.Source=UserUDP;
    sInfo.A1=dwIP;
    sInfo.PORT1=wPort;
    sInfo.L=3;
    sInfo.ByteInfoKadr[0]=109;
    sInfo.ByteInfoKadr[1]=SN;
    sInfo.ByteInfoKadr[2]=btErr;
    xQueueSend(xQueueReply[UserUDP],&sInfo,0,0);
  }
}

void PutErrInfo2Display(unsigned char PultNb,const char *strErr)
{
  if(IsAutomaticCall(PultNb)) return;
  if(strErr)
  {
    ClearScreen(PultNb);
    OutputString(PultNb,strErr,0,3,3);
    SetDisplayTOut(PultNb,MassTimer[TIMER_OUTGOINGNUMBER] >> 1);
  }
  SendError2IpPult(PultNb,0);
  IndicCmd(PultNb,0,NO_LED,NO_INDIC,3,1);
}

// ���������� ����� ������������� � ������            
char Tlfn2Str(char *pStr,unsigned char *pTlfn)
{
  char i=0;
  
  do  
  {
    if(pTlfn[i]==0xFF) break;
    else
    {
      pStr[i]=pTlfn[i]+'0';
      i++;
    }
  }
  while(i<MAX_DIGIT_TLFN-1);
  pStr[i]=0;
  return(i);
}

char Str2Tlfn(unsigned char *pTlfn,char *pStr)
{
  char Ln=0,chSimb;
  
  memset(pTlfn,0xff,MAX_DIGIT_TLFN);
  if(pStr)
  {
    while(*pStr)
    {
      chSimb=*pStr;
      if((chSimb>='0') && (chSimb<='9')) chSimb-='0';
      *pTlfn=chSimb;
      pTlfn++; pStr++; Ln++;
      if(Ln==MAX_DIGIT_TLFN-1) return(Ln);
    }
  }
  return(Ln);
}

bool TlfnEqual(unsigned char *pbtTlfn1,unsigned char *pbtTlfn2)
{
  return(!memcmp(pbtTlfn1,pbtTlfn2,MAX_DIGIT_TLFN-1));
}

unsigned char FindFreeISDNKey(unsigned char PultNb,unsigned char *pbtLayer)
{
  unsigned char i,btState,btLayer;

  *pbtLayer=0xff;
  for(btLayer=0;btLayer<GetMaxLayer(PultNb);btLayer++)
  {
    if(IsKbdLayerLock(PultNb,btLayer)) continue;
    for(i=0;i<NumberKey;i++)
    {
      if((MassKey[PultNb][btLayer][i].CodeKey[0]==CallKey) &&
         (MassKey[PultNb][btLayer][i].TypeAbon==ISDN) &&
         (!MassKey[PultNb][btLayer][i].NumbAbon))
      {
        btState=GetAbonentStateFromKey(PultNb,i,btLayer);
        if(btState==Passive)
        { *pbtLayer=btLayer; return(i); }
      }
    }
  }
  return(0xff);
}

unsigned char FindISDNAbonentKey(unsigned char PultNb,unsigned char *pTlfn,unsigned char btLn,
                                 unsigned char *pNumb,unsigned char *pbtLayer)
{
  unsigned char i,btKey,btLayer;

  if(pNumb) *pNumb=0;
  *pbtLayer=0xff;
  if(btLn)
  {
    for(i=0;i<NumberDirectAbon;i++)
    { // ����� ��-�� �� ������ ��������
      if(TlfnEqual(MassTel[i],pTlfn))
      { // ����� ������
        i++; // ���������� ����� �������� ���������
        for(btLayer=0;btLayer<GetMaxLayer(PultNb);btLayer++)
        {
          if(IsKbdLayerLock(PultNb,btLayer)) continue;
          for(btKey=0;btKey<NumberKey;btKey++)
          {
            if((MassKey[PultNb][btLayer][btKey].NumbAbon==i) &&
               (MassKey[PultNb][btLayer][btKey].TypeAbon==ISDN))
            {
              if(pNumb) *pNumb=i;
              *pbtLayer=btLayer;
              return(btKey);
            }
          }
        }
      }
    }
  }
  FindFreeISDNKey(PultNb,pbtLayer);
  return(0xff);
}

unsigned char FindAbntKey2Pult(unsigned char PultNb,unsigned char *pbtLayer,
                               unsigned short wNChan,unsigned char *pTypeAbon,unsigned char AbntNumb)
{
  unsigned char btKey,btLayer;

  for(btLayer=0;btLayer<GetMaxLayer(PultNb);btLayer++)
  {
    if(IsKbdLayerLock(PultNb,btLayer)) continue;
    for(btKey=0;btKey<NumberKey;btKey++)
    {
      if(MassKey[PultNb][btLayer][btKey].NChan==wNChan)
      {
        if(*pTypeAbon!=0xff)
        {
          if(*pTypeAbon==MassKey[PultNb][btLayer][btKey].TypeAbon)
          {
            if(AbntNumb==0xff) return(btKey);
            if(MassKey[PultNb][btLayer][btKey].NumbAbon==AbntNumb)
            { *pbtLayer=btLayer; return(btKey); }
          }
        }
        else
        {
          *pTypeAbon=MassKey[PultNb][btLayer][btKey].TypeAbon;
          *pbtLayer=btLayer;
          return(btKey);
        }
      }
    }
  }
  return(0xff);
}

// �������� ������ �� ������� Command
bool WaitReply(unsigned char Command, unsigned char Subcommand)
{
  int iRepeat;
  bool Result;
  static sInfoKadr sReply;

  iRepeat=0;
  Result=false;
  do
  {
    if(xQueueReceive(xQueueReply[UserRS485],&sReply,250))
    { // ������ �� ������� �������
      if(sReply.ByteInfoKadr[0]==Command)
      {
        if(Command==11)
        {
          if(Subcommand==sReply.ByteInfoKadr[1]) 
          {
            switch(Subcommand)
            {
              case 123: // ������ �������� ����������
                if(!sReply.ByteInfoKadr[2]) return(true);
                CountErrCommute++;
                return(false);
              case 122: // ��������� ������� ����������
                if(sReply.ByteInfoKadr[2]==NumbTabl) return(true);
                CountErrCommute++;
                return(false);
              default:
                if(sReply.ByteInfoKadr[2]==1) return(true);
                CountErrCommute++;
                return(false);
            }
          }
          else
          { // ����� �� ������ �������
            CountErrNoCommand++;
/*DataSave1[7]=CountErrNoCommand;
DataSave1[8]=sReply.ByteInfoKadr[1];
DataSave1[9]=Subcommand;*/
          }
        }
        else
        {
          if(sReply.ByteInfoKadr[1]==CodeStatusTrue) return(true);
          CountErrCommute++;
          return(false);
        }
      }
      else
      { // ����� �� ������ �������
        CountErrNoCommand++;
/*DataSave1[7]=CountErrNoCommand;
DataSave1[8]=sReply.ByteInfoKadr[0];
DataSave1[9]=Command;*/
      }
    }
    else
    { // ��� ������� � �������
      iRepeat++;
      if(iRepeat>=2)
      {
        CountNoReply++;
//DataSave1[4]=CountNoReply;
//DataSave1[5]=Command;
//if(Command==11) DataSave1[6]=Subcommand;
        return(false);
      }
    }
  }
  while(1);
}

// ��������� ���������� ������� NChan1 <- NChan2
void Commute(bool Code,unsigned short NChan1, unsigned short NChan2)
{
  static sCommand sCmd;

  if((NChan1==0) || (NChan2==0)) return;
  if((NChan1>=d_DummyCP) || (NChan2>=d_DummyCP)) return;
  sCmd.Source=UserRS485; // �������� ������� RS485
  sCmd.L=6; // ���������� ���� � �������
  sCmd.A1=0;
  sCmd.BufCommand[0]=10; // �������
  if(Code) sCmd.BufCommand[1]=129; // ���������� ��������� ����������
  else sCmd.BufCommand[1]=130; // ���������� �������������
  sCmd.BufCommand[2]=NChan1 >> 8; // ��� ������
  sCmd.BufCommand[3]=NChan1; // N ������
  sCmd.BufCommand[4]=NChan2 >> 8; // ��� ������
  sCmd.BufCommand[5]=NChan2; // �����
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
}

// �����/ �������� ������ �� ������ NChanUD
bool TrubaUP_Down(unsigned char UP_Down, unsigned short NChanUD)
{
  static sCommand sCommandT;

  sCommandT.Source=UserRS485;
  sCommandT.L=4;
  sCommandT.A1=0;
  sCommandT.BufCommand[0]=31;
  sCommandT.BufCommand[1]=0;
  sCommandT.BufCommand[2]=NChanUD-1;
  sCommandT.BufCommand[3]=UP_Down;
  if(xQueueSendCmd(&sCommandT,0)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else 
  {
    if(WaitReply(31,0)) return(true);
  }
  return(false);
}

bool SendCmdOtboiISDN(unsigned char Reason,unsigned short wNChan,unsigned short wChanOper)
{
  static sCommand sCmd;
  
  sCmd.L=7;
  sCmd.Source=UserRS485;// �������� ������� RS485
  sCmd.A1=0;
  sCmd.BufCommand[0]=10; // �������
  sCmd.BufCommand[1]=205; // ���������� "�����"
  sCmd.BufCommand[2]=wNChan >> 8;
  sCmd.BufCommand[3]=wNChan;
  sCmd.BufCommand[4]=wChanOper >> 8; // ����� ���������
  sCmd.BufCommand[5]=wChanOper; // ����� ���������
  sCmd.BufCommand[6]=Reason;
  if(xQueueSendCmd(&sCmd,0)==errQUEUE_FULL) CountErrSendQueueCommand++;
  return(false);
}

void DSRS_Off(unsigned char PultNb,unsigned short wChanOper)
{
  if(wChanOper & d_OperatorOffWithoutCondition) wChanOper&=~d_OperatorOffWithoutCondition;
  else
  {
    if(IsTalk2Pult(PultNb,wChanOper)) return;
  }
  if(wChanOper==GetOperatorChannel(PultNb,0)) OnOffRejim(PultNb,DS,OFF);
  else OnOffRejim(PultNb,RS,OFF);
}

void DSRS_On(unsigned char PultNb,unsigned short wChanOper)
{
  if(wChanOper==GetOperatorChannel(PultNb,0)) OnOffRejim(PultNb,DS,ON);
  else OnOffRejim(PultNb,RS,ON);
}

bool ExistTalk2LineMKA(unsigned char PultNb,unsigned short wChan)
{
  unsigned char btInd;
  ABONENT_STATE_TP *psAbnt;

  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return(false);
    btInd++;
    if((psAbnt->wConnectionPoint==wChan) && (psAbnt->btPultNb==PultNb) &&
       (GetAbonentStateFromPtr(psAbnt)==Talk)) return(true);
  }
  while(1);
}

bool ExistTalk2Group(unsigned char PultNb,unsigned char btGroup)
{
  unsigned char btInd;
  ABONENT_STATE_TP *psAbnt;

  if(!btGroup || (btGroup==0xff)) return(false);
  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return(false);
    btInd++;
    if((psAbnt->wConnectionPoint!=0xffff) &&
       (psAbnt->btPultNb==PultNb) && (GetAbonentStateFromPtr(psAbnt)==Talk))
    {
      switch(psAbnt->btType)
      {
        case TDC:
          if(GetGroupFromChan(psAbnt->wConnectionPoint)==btGroup) return(true);
          break;
        case IPPM:
        case ISDN:
          if(GetTlfnGroup(psAbnt->NumbAbon)==btGroup) return(true);
          break;
      }
    }
  }
  while(1);
}

void SendLayerAndLock2IpPult(unsigned char PultNb,sInfoKadr *psInfo)
{
  SetKbdUnlockState(PultNb,GetKbdUnlockState(PultNb));
  psInfo->ByteInfoKadr[0]=106;
  psInfo->ByteInfoKadr[1]=MassM1[PultNb];
  psInfo->ByteInfoKadr[2]=0; // ��� Ip-������ ������ 0!!
  psInfo->ByteInfoKadr[3]=GetKbdUnlockState(PultNb)?Off:d_ServiceOn;
  psInfo->L=4;
  xQueueSend(xQueueReply[psInfo->Source],psInfo,10,0);
}

unsigned char CmdEnDisOutCP(unsigned short wCP,unsigned char btEnOut)
{
extern unsigned short g_wMasDisOut[LenSport0];
  if((wCP<1) || (wCP>48)) return 0;
  g_wMasDisOut[wCP-1]=(btEnOut ? 0xffff : 0);
  return 1;
}

void DelConnectionPoint(unsigned short wCP)
{
  if((wCP >> 8)==0x1)
  {
    static sCommand sCmd;
    sCmd.Source=UserRS485; // �������� ������� RS485
    sCmd.L=4; // ���������� ���� � �������
    sCmd.A1=0;
    sCmd.BufCommand[0]=10; // �������
    sCmd.BufCommand[1]=142; // ����������
    sCmd.BufCommand[2]=wCP >> 8;
    sCmd.BufCommand[3]=wCP & 0xff;
    if(xQueueSendCmd(&sCmd,0)==errQUEUE_FULL) CountErrSendQueueCommand++;
  }
}

// ����� ��������� ����� �����������
unsigned short GetFreeConnectionPoint(void)
{
  unsigned short wCP=0xff;
  static sCommand sCmd;
  static sInfoKadr sReply;

  sCmd.Source=UserRS485; // �������� ������� RS485
  sCmd.L=2; // ���������� ���� � �������
  sCmd.A1=0;
  sCmd.BufCommand[0]=10; // �������
  sCmd.BufCommand[1]=141; // ����������
  if(xQueueSendCmd(&sCmd,0)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(xQueueReceive(xQueueReply[UserRS485],&sReply,250))
    {
      if((sReply.ByteInfoKadr[0]==11) && (sReply.ByteInfoKadr[1]==141))
      {
        wCP=sReply.ByteInfoKadr[2];
        wCP=(wCP << 8) | sReply.ByteInfoKadr[3];
        if(wCP==0xffff) wCP=0xff;
      }
    }
  }
  return(wCP);
}

char *strclr(char *str)
{
  int i,j;
  unsigned char *str1,*str2;

  if(!str || !(*str)) return(NULL);
  i=1; str1=(unsigned char *)str;
  for(j=0;j<2;j++)
  {
    while((*str1) && (*str1<=' ')) str1+=i;
    if(!(*str1)) return(NULL);
    if(!j)
    {
      str2=str1;
      str1=(unsigned char *)(str+strlen((char *)str)-1);
      i=-1;
    }
  }
  str1++; *str1=0;
  return((char *)str2);
}

