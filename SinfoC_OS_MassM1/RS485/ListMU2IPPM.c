#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"

///////////////////////////////////////////////////////////////////////////////
void *pvPortMalloc(size_t xSize);
void vPortFree(void *pv);

///////////////////////////////////////////////////////////////////////////////
static LIST_MU2IPPM_TP *s_pListMU2IPPM=NULL;

static unsigned short s_wListMU2IPPMId=0;

///////////////////////////////////////////////////////////////////////////////
unsigned short GetMU2IPPMId(void)
{
  unsigned short wId=s_wListMU2IPPMId;
  s_wListMU2IPPMId++;
  return(wId);
}

LIST_MU2IPPM_TP *ListMU2IPPM_AddItem(LIST_MU2IPPM_TP *pItem0)
{
  LIST_MU2IPPM_TP *pItemCur;
  LIST_MU2IPPM_TP *pItem=(LIST_MU2IPPM_TP *)pvPortMalloc(sizeof(LIST_MU2IPPM_TP));
  if(!pItem) return(false);
  memset(pItem,0,sizeof(LIST_MU2IPPM_TP));
  if(pItem0) memcpy(pItem,pItem0,sizeof(LIST_MU2IPPM_TP));
  pItem->wId=GetMU2IPPMId();
  pItem->next=NULL;
//SendStr2IpLogger("$DEBUG$ListMU2IPPM_AddItem: wCmdId=%d",pItem->wId);
  if(s_pListMU2IPPM)
  {
    pItemCur=s_pListMU2IPPM;
    while(pItemCur->next) pItemCur=pItemCur->next;
    pItemCur->next=pItem;
    pItem->prev=pItemCur;
  }
  else
  {
    pItem->prev=NULL;
    s_pListMU2IPPM=pItem;
  }
  return(pItem);
}

void _ListMU2IPPM_DeleteItem(LIST_MU2IPPM_TP *pItem0)
{
  if(!pItem0) return;
//SendStr2IpLogger("$DEBUG$ListMU2IPPM_DeleteItem: Numb=%d, cmdId=%d, repeat_cnt=%d",pItem0->Numb,pItem0->wId,pItem0->repeat_cnt);
  if(pItem0==s_pListMU2IPPM)
  {
    s_pListMU2IPPM=pItem0->next;
    if(s_pListMU2IPPM) s_pListMU2IPPM->prev=NULL;
  }
  else
  {
    LIST_MU2IPPM_TP *pItemPrev,*pItemNext;

    pItemPrev=pItem0->prev;
    pItemNext=pItem0->next;
    pItemPrev->next=pItemNext;
    if(pItemNext) pItemNext->prev=pItemPrev;
  }
  vPortFree(pItem0);
}

LIST_MU2IPPM_TP *_ListMU2IPPM_FindItem(unsigned short wId)
{
  LIST_MU2IPPM_TP *pItem;

  pItem=s_pListMU2IPPM;
  while(pItem)
  {
    if(pItem->wId==wId) return(pItem);
    pItem=pItem->next;
  }
  return(NULL);
}

bool _SendItem2IPPM(LIST_MU2IPPM_TP *pItem)
{
  pItem->repeat_cnt++;
//SendStr2IpLogger("$DEBUG$SendItem2IPPM: Numb=%d, cmdId=%d, repeat_cnt=%d",pItem->Numb,pItem->wId,pItem->repeat_cnt);
  xQueueSend(xQueueReply[UserUDP],&(pItem->sIK),0,0);
  pItem->dwTOut=xTaskGetTickCount()+d_MU2IPPM_CmdTOut;
  return true;
}

bool IsEmptyQueueCmd4CurrentIPPM(LIST_MU2IPPM_TP *pItemCur)
{
  LIST_MU2IPPM_TP *pItem;

  pItem=s_pListMU2IPPM;
  do
  {
    if(pItem==pItemCur) return true;
    if(pItem->Numb==pItemCur->Numb) return false;
    pItem=pItem->next;
  }
  while(pItem);
  return true;
}

void ListMU2IPPM_Process(void)
{
  LIST_MU2IPPM_TP *pItemDel,*pItemCur;

  pItemCur=s_pListMU2IPPM;
  while(pItemCur)
  {
    if(pItemCur->repeat_cnt)
    {
      if(IsCurrentTickCountGT(pItemCur->dwTOut))
      {
        if(pItemCur->repeat_cnt>=d_MU2PultMaxRepeat)
        {
          pItemDel=pItemCur;
          pItemCur=pItemCur->next;
          if(pItemDel->pFunc_IPPMNoAnswer2MU) pItemDel->pFunc_IPPMNoAnswer2MU(pItemDel);
          _ListMU2IPPM_DeleteItem(pItemDel);
          continue;
        }
        else
        {
          if(IsEmptyQueueCmd4CurrentIPPM(pItemCur))
          { _SendItem2IPPM(pItemCur); return; }
        }
      }
    }
    else
    {
      if(IsEmptyQueueCmd4CurrentIPPM(pItemCur))
      { _SendItem2IPPM(pItemCur); return; }
    }
    pItemCur=pItemCur->next;
  }
}

bool IPPM_RcvAnsw(unsigned short wCmdId,unsigned char btRes)
{
  bool bRes=false;
  LIST_MU2IPPM_TP *pItem;

//SendStr2IpLogger("$DEBUG$IPPM_RcvAnsw: CmdId=%d",wCmdId);
  pItem=_ListMU2IPPM_FindItem(wCmdId);
  if(pItem)
  {
    bRes=true;
    if(pItem->pFunc_IPPMAnswer2MU) pItem->pFunc_IPPMAnswer2MU(pItem,btRes);
    _ListMU2IPPM_DeleteItem(pItem);
  }
  return bRes;
}

void ClearQueueCmd4IPPM(unsigned char Numb)
{
  LIST_MU2IPPM_TP *pItemDel,*pItem;

  pItem=s_pListMU2IPPM;
  while(pItem)
  {
    pItemDel=pItem;
    pItem=pItem->next;
    if(pItemDel->Numb==Numb) _ListMU2IPPM_DeleteItem(pItemDel);
  }
}

