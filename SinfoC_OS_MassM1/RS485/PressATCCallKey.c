#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

//*******************************************************************

//*******************************************************************
void OtboiAbonentATC(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
//  unsigned char btState;
  ABONENT_STATE_TP *psAbnt;
  
  psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);
  if(!psAbnt) return;
  ClearAbonentIndex4Digit(PultNb,GetAbonentStateIndexFromPtr(psAbnt));
//  btState=GetAbonentStateFromPtr(psAbnt);
//  if(btState==Hold) StopKeep(psAbnt->wConnectionPoint);
  TrubaUP_Down(DOWN,psAbnt->wConnectionPoint);
  if(psAbnt->btKeyATCDirect!=0xff) IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKeyATCDirect,Off,0,0);
  if(psAbnt->NumbAbon==d_OutgoingDigit) SetOutgoingNumberTmp(PultNb,psAbnt->btTlfn); // ���� ���.����� ��� ���������� ������ ��� "REDIAL"
  OtboiAbonentCommonPart(PultNb,Key,btLayer,NULL,NULL);
  IndicClock(PultNb);
}

void DelIncomingCallATCFromOtherPult(unsigned char PultNb,unsigned short NChan)
{
  unsigned char btInd,btState;
  ABONENT_STATE_TP *psAbnt;

  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return;
    btInd++;
    if((psAbnt->wConnectionPoint==NChan) && (psAbnt->btPultNb!=PultNb))
    {
      IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,Off,0,0);
      SetAbonentStateByPtr(psAbnt,Off,0);
      memset(psAbnt,0xff,sizeof(ABONENT_STATE_TP));
    }
  }
  while(1);
}

#define dPorogBusy      0x100
#define dTimeLevel      20

void ControlATCLevelTOut(void)
{ // ���������� ������� ������ ������� ��� �������� ���(������������ ��� ��������� ������� "������")
  unsigned char btInd,btOff;
  unsigned short wLvl;
  ABONENT_STATE_TP *psAbnt;
  static sCommand sCmd;
  static sInfoKadr sReply;

  sCmd.Source=UserRS485;
  sCmd.L=2;
  sCmd.BufCommand[0]=10;  //-+ - ������ ������� ������� �� ������� � ��
  sCmd.BufCommand[1]=154; //-+
  while(xQueueReceive(xQueueReply[UserRS485],&sReply,1));
  if(xQueueSend(xQueueSportX,&sCmd,10,0)==errQUEUE_FULL) return;
  else
  {
    if(xQueueReceive(xQueueReply[UserRS485],&sReply,250))
    { // ������ �� ������� �������
      if((sReply.ByteInfoKadr[0]!=11) || (sReply.ByteInfoKadr[1]!=154)) return;
    }
  }
  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return;
    btInd++;
    if((psAbnt->wConnectionPoint!=0xffff) && (psAbnt->btType==ATC))
    {
      if(GetAbonentStateFromPtr(psAbnt)==Talk)
      {
        btOff=(psAbnt->wConnectionPoint-1)*2+2;
        wLvl=sReply.ByteInfoKadr[btOff];
        wLvl=(wLvl<<8) | sReply.ByteInfoKadr[btOff+1];
        if(wLvl>=dPorogBusy)psAbnt->btLvlTOut=dTimeLevel;
        else
        {
          if(psAbnt->btLvlTOut) psAbnt->btLvlTOut--;
        }
      }
      else psAbnt->btLvlTOut=dTimeLevel;
    }
  }
  while(1);
}

unsigned int GetTypeCall4ChanelMKA(unsigned short NChan)
{
  unsigned int dwHi;

  NChan--; NChan<<=2;
  dwHi=RdFlash(AdrTypeCall+2+NChan);
  return((dwHi << 16) | RdFlash(AdrTypeCall+NChan));
}

bool ProcessOutgoingNumber(unsigned char NChan,unsigned char *pbtMasDg)
{ // ����� ������ ��������
  unsigned char i;
  unsigned int dwTypeCall;
  static sCommand sCommandTaskR;

  dwTypeCall=GetTypeCall4ChanelMKA(NChan);
  dwTypeCall&=0x20;
  sCommandTaskR.Source=UserRS485;
  sCommandTaskR.L=21;
  sCommandTaskR.A1=0;
  if(dwTypeCall) sCommandTaskR.BufCommand[0]=6;  // DTMF
  else sCommandTaskR.BufCommand[0]=5; // PULSE
  sCommandTaskR.BufCommand[1]=0;
  sCommandTaskR.BufCommand[2]=NChan-1;
  memset(sCommandTaskR.BufCommand+3,0xff,18);
  i=0;
  while(pbtMasDg[i]!=0xff)
  {
    if(!dwTypeCall && (pbtMasDg[i]==0)) sCommandTaskR.BufCommand[3+i]=10; // ��� ���. ������ 0 = 10
    else sCommandTaskR.BufCommand[3+i]=pbtMasDg[i]; // ����� ��� �����
    i++;
  }
  if(xQueueSendCmd(&sCommandTaskR,0)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(WaitReply(sCommandTaskR.BufCommand[0],0)) return(true);
  }
  return(false);
}

void SignalATC_Busy(unsigned short wNChan)
{
  unsigned char btInd,btState;
  ABONENT_STATE_TP *psAbnt;

  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return;
    btInd++;
    if(psAbnt->wConnectionPoint==wNChan)
    {
      if(psAbnt->btLvlTOut) OtboiAbonent(psAbnt->btPultNb,psAbnt->btKey,psAbnt->btLayer);
      return;
    }
  }
  while(1);
}

void SignalATC_Ready(unsigned short wNChan)
{
  unsigned char btInd,SN;
  ABONENT_STATE_TP *psAbnt;

  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return;
    btInd++;
    if(psAbnt->wConnectionPoint==wNChan)
    {
      if(GetAbonentStateFromPtr(psAbnt)==Call)
      { // ����������� ��������� ���������� ������
        if(!ProcessOutgoingNumber(wNChan,psAbnt->btTlfn))
        {
          TrubaUP_Down(DOWN,wNChan);
          Commute(false,wNChan,psAbnt->wOperatorChanel);
          DSRS_Off(psAbnt->btPultNb,psAbnt->wOperatorChanel);
          PutErrInfo2Display(psAbnt->btPultNb,"������ ������ ������!");
          if(psAbnt->btKeyATCDirect!=0xff) IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKeyATCDirect,Off,0,0);
          IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,Off,0,0);
          ClearAbonentStateByPtr(psAbnt);
        }
        else
        {
          SetLedColorIfTalk(psAbnt);
          SetAbonentStateByPtr(psAbnt,Talk,0);
          psAbnt->dwTimerEnd=0;
          if(psAbnt->btKeyATCDirect!=0xff) IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKeyATCDirect,psAbnt->btLedState,0,0);
          IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,1,1);
          SendActiveUnnameAbnt_ATC_ISDN(psAbnt);
        }
      }
      return;
    }
  }
  while(1);
}

void OutgoingCallATC(unsigned char PultNb,unsigned char Key,unsigned char btLayer,unsigned char KeyATCDirect)
{
  unsigned short NChan,wOper;
  unsigned char *pbtMasDg,Ln,btInd,NumbAbon;
  ABONENT_STATE_TP *psAbnt;

  wOper=GetOperatorChannelFromKey(PultNb,Key,btLayer);
  NChan=MassKey[PultNb][btLayer][Key].NChan;
  btInd=GetFreeAbonentStateIndex();
  if(btInd!=0xff)
  {
    if(!TrubaUP_Down(UP,NChan))
    {
      PutErrInfo2Display(PultNb,"������ ����� ���!");
      return;
    }
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    psAbnt->wConnectionPoint=NChan;
    psAbnt->wOperatorChanel=wOper;
    psAbnt->btPultNb=PultNb;
    psAbnt->btLayer=btLayer;
    psAbnt->btKey=Key;
    psAbnt->btKeyATCDirect=KeyATCDirect;
    psAbnt->btType=ATC;
    if(IsOutgoingNumberTmpPresent(PultNb) && GetOutgoingNumberTmp(PultNb,&pbtMasDg,&Ln))
    {
      psAbnt->btLedState=dLedStateOutgoingCall;
      SetAbonentStateByPtr(psAbnt,Call,0);
      memcpy(psAbnt->btTlfn,pbtMasDg,Ln);
      psAbnt->dwTimerEnd=MassTimer[TIMER_OUTGOINGCALL]+xTaskGetTickCount();
      DSRS_On(psAbnt->btPultNb,psAbnt->wOperatorChanel);
      Commute(true,NChan,psAbnt->wOperatorChanel);
      Commute(true,psAbnt->wOperatorChanel,NChan);
      OutgoingNumberTmpTOutClear(PultNb); // ��� ������� �������� ��������� �����!
      if(KeyATCDirect!=0xff) IndicCmd(PultNb,psAbnt->btLayer,KeyATCDirect,psAbnt->btLedState,0,0);
      // ����������� ��������� ���������� ������ ����� ��������� ���������� �������!
    }
    else
    {
      SetAbonentIndex4Digit(PultNb,btInd);
      DSRS_On(psAbnt->btPultNb,psAbnt->wOperatorChanel);
      Commute(true,NChan,psAbnt->wOperatorChanel);
      Commute(true,psAbnt->wOperatorChanel,NChan);
//SendStr2IpLogger($DEBUG$OutgoingCallATC: wCP=%d, wOp=%d",NChan,psAbnt->wOperatorChanel);
      SetLedColorIfTalk(psAbnt);
      SetAbonentStateByPtr(psAbnt,Talk,0);
      psAbnt->dwTimerEnd=0;
    }
    IndicCmd(PultNb,psAbnt->btLayer,Key,psAbnt->btLedState,1,1);
    SendActiveUnnameAbnt_ATC_ISDN(psAbnt);
  }
  else PutErrInfo2Display(PultNb,"���� ������!");
}

void OutgoingCallPressedFreeATS(unsigned char PultNb,unsigned char Key,unsigned char btLayer,
                                unsigned char KeyATCDirect,unsigned char *pbtTlfnNumber)
{
  ABONENT_STATE_TP *psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);

  if(psAbnt)
  {
    memcpy(psAbnt->btTlfn,pbtTlfnNumber,MAX_DIGIT_TLFN-2);
    if(!ProcessOutgoingNumber(psAbnt->wConnectionPoint,psAbnt->btTlfn))
    {
      TrubaUP_Down(DOWN,psAbnt->wConnectionPoint);
      Commute(false,psAbnt->wConnectionPoint,psAbnt->wOperatorChanel);
      DSRS_Off(psAbnt->btPultNb,psAbnt->wOperatorChanel);
      PutErrInfo2Display(psAbnt->btPultNb,"������ ������ ������!");
      IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,Off,0,0);
      ClearAbonentStateByPtr(psAbnt);
    }
    else
    {
      psAbnt->btKeyATCDirect=KeyATCDirect;
      IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKeyATCDirect,psAbnt->btLedState,0,0);
      SendActiveUnnameAbnt_ATC_ISDN(psAbnt);
    }
  }
}

bool IsPlacingIncomingCallATC2Layer(unsigned char PultNb,unsigned char btLayer,unsigned short NChan)
{
  unsigned char btKey,btLed,btLedState;
  ABONENT_STATE_TP *psAbnt;

  for(btKey=0;btKey<NumberKey;btKey++)
  {
    if(MassKey[PultNb][btLayer][btKey].NChan==NChan)
    {
      psAbnt=GetAbonentStatePtrFromKey(PultNb,btKey,btLayer);
      if(psAbnt)
      {
        psAbnt->dwTimerEnd=MassTimer[TIMER_INDUCTOR]+xTaskGetTickCount();
        return(true);
      }
      psAbnt=GetFreeAbonentStatePtr();
      if(!psAbnt) return(false);
      psAbnt->wConnectionPoint=NChan;
      psAbnt->wOperatorChanel=GetOperatorChannelFromKey(PultNb,btKey,btLayer);
      psAbnt->btPultNb=PultNb;
      psAbnt->btLayer=btLayer;
      psAbnt->btKey=btKey;
      psAbnt->btType=ATC;
      psAbnt->btLedState=dLedStateIncomingCall;
      SetAbonentStateByPtr(psAbnt,IncomingCall,0);
      psAbnt->dwTimerEnd=MassTimer[TIMER_INDUCTOR]+xTaskGetTickCount();
      IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,1,1);
      return(true);
    }
  }
  return(false);
}

void IncomingCallATC(unsigned short NChan)
{ // ����� ��������� �� ��� ������, � ������������� ������ �����!
  unsigned char PultNb,btLayer;

  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if(MassM1[PultNb] && IsKbdReady(PultNb) && IsKbdUnlock(PultNb))
    {
      for(btLayer=0;btLayer<GetMaxLayer(PultNb);btLayer++)
      {
        if(IsKbdLayerLock(PultNb,btLayer)) continue;
        if(IsPlacingIncomingCallATC2Layer(PultNb,btLayer,NChan)) break;
      }
    }
  }
}

void AnswerIncomingCallATC(unsigned char PultNb,ABONENT_STATE_TP *psAbnt)
{
  unsigned short NChan,wOper;
  unsigned char btInd,btLayer,Key;

  btLayer=psAbnt->btLayer;
  Key=psAbnt->btKey;
  wOper=GetOperatorChannelFromKey(PultNb,Key,btLayer);
  NChan=MassKey[PultNb][btLayer][Key].NChan;
  if(NChan)
  {
    if(!TrubaUP_Down(UP,NChan))
    {
      IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKey,Off,0,0);
      ClearAbonentStateByPtr(psAbnt);
      PutErrInfo2Display(PultNb,"������ ����� ���!");
      return;
    }
    DSRS_On(PultNb,wOper);
    Commute(true,NChan,wOper);
    Commute(true,wOper,NChan);
    SetLedColorIfTalk(psAbnt);
    SetAbonentStateByPtr(psAbnt,Talk,dIncomingTalkFlag);
    psAbnt->dwTimerEnd=0;
    IndicCmd(PultNb,btLayer,Key,psAbnt->btLedState,1,1);
    DelIncomingCallATCFromOtherPult(PultNb,NChan);
  }
}

/*typedef struct
{
  unsigned char btType;
  bool bCall;
  unsigned int dwTOut;
} sMKACallTp;

static sMKACallTp s_MassMKACall[MAX_MKA_CHANNEL];

bool IsIncomingMKACall(unsigned short NChan,unsigned short btType)
{
  if((s_MassMKACall[NChan].btType==btType) && s_MassMKACall[NChan].bCall) return(true);
  return(false);
}

void SetIncomingMKACall(unsigned short NChan,unsigned short btType)
{
  s_MassMKACall[NChan].btType=btType;
  s_MassMKACall[NChan].bCall=true;
  s_MassMKACall[NChan].dwTOut=xTaskGetTickCount();
}

void ClearIncomingMKACall(unsigned short NChan,unsigned short btType)
{
  memset(&s_MassMKACall[NChan],0,sizeof(sMKACallTp));
}
!!!  if(IsIncomingMKACall(NChan,ATC)) return;
  SetIncomingMKACall(NChan,ATC);*/

