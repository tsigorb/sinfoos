#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
void HoldTOut(unsigned char PultNb)
{
  unsigned char btInd;
  unsigned int dwTOut;
  bool bHoldInfo;
  ABONENT_STATE_TP *psAbnt;

  dwTOut=xTaskGetTickCount();
  bHoldInfo=false;
  btInd=0xff;
  do
  {
    btInd=GetAbonentStateIndByState(PultNb,btInd,Hold);
    if(btInd!=0xff)
    {
      psAbnt=GetAbonentStatePtrFromIndex(btInd);
      if(psAbnt->dwTimerEnd<dwTOut)
      { bHoldInfo=true; break; }
    }
  }
  while(btInd!=0xff);
  if(!bHoldInfo) return;
  IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,4,3);
  dwTOut=MassTimer[TIMER_HOLD]+xTaskGetTickCount();
  btInd=0xff;
  do
  {
    btInd=GetAbonentStateIndByState(PultNb,btInd,Hold);
    if(btInd!=0xff)
    {
      psAbnt=GetAbonentStatePtrFromIndex(btInd);
      psAbnt->dwTimerEnd=dwTOut;
    }
  }
  while(btInd!=0xff);
}

bool HoldOn(unsigned char PultNb,unsigned char Key,unsigned char btLayer,bool bOn)
{
  ABONENT_STATE_TP *psAbnt;

  psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);
  if(!psAbnt) return(false);
  if(bOn)
  {
    if(!SetAbonentStateByPtr(psAbnt,Hold,0)) return(false);
    Commute(false,psAbnt->wOperatorChanel,psAbnt->wConnectionPoint);
    psAbnt->dwTimerEnd=MassTimer[TIMER_HOLD]+xTaskGetTickCount();
    DSRS_Off(PultNb,psAbnt->wOperatorChanel);
    if((psAbnt->btType==ISDN) || (psAbnt->btType==ATC) || (psAbnt->btType==Selector)) SoundKeep(psAbnt->wConnectionPoint);
  }
  else
  {
    if(!IsCommute(psAbnt->wOperatorChanel,psAbnt->wConnectionPoint))
    {
      Commute(true,psAbnt->wOperatorChanel,psAbnt->wConnectionPoint);
      Commute(true,psAbnt->wConnectionPoint,psAbnt->wOperatorChanel);
    }
    SetLedColorIfTalk(psAbnt);
    psAbnt->dwTimerEnd=0;
    if((psAbnt->btType==ISDN) || (psAbnt->btType==ATC) || (psAbnt->btType==Selector)) StopKeep(psAbnt->wConnectionPoint);
    DSRS_On(PultNb,psAbnt->wOperatorChanel);
    SetAbonentStateByPtr(psAbnt,Talk,0);
  }
  IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,1,1);
  return(true);
}

