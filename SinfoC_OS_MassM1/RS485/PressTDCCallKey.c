#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

bool _xOutgoingCallTDC(unsigned short NChan,unsigned char NumbAbon,bool bTest)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485;
  sCmd.L=5;
  sCmd.A1=0;
  sCmd.BufCommand[0]=1;
  sCmd.BufCommand[1]=NChan >> 8;
  sCmd.BufCommand[2]=NChan;
  if(bTest) sCmd.BufCommand[3]=TestTDC;
  else sCmd.BufCommand[3]=CallTDC;
  sCmd.BufCommand[4]=NumbAbon-1;
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else 
  {
    if(WaitReply(1,0)) return(true);
  }
  return(false);
}

ABONENT_STATE_TP *GetAbonentTDC_Active(unsigned char PultNb,unsigned short NChan,unsigned char NumbAbon)
{
  ABONENT_STATE_TP *psAbnt;

  psAbnt=GetAbonentStatePtrByFind(PultNb,NChan,0xffff,TDC,NumbAbon,NULL);
  if(psAbnt && (GetAbonentStateFromPtr(psAbnt)==CallTest)) psAbnt=NULL;
  if(!psAbnt) psAbnt=GetAbonentStatePtrByFind(0xff,NChan,0xffff,TDC,NumbAbon,NULL);
  if(psAbnt && (GetAbonentStateFromPtr(psAbnt)==CallTest)) psAbnt=NULL;
  return(psAbnt);
}

void OutgoingCallTDC(unsigned char PultNb,unsigned char Key,unsigned char btLayer,bool bTest)
{
  unsigned short NChan,wOper;
  unsigned char NumbAbon,btSound;
  ABONENT_STATE_TP *psAbnt,*psAbntOld;

  NChan=MassKey[PultNb][btLayer][Key].NChan;
  wOper=GetOperatorChannelFromKey(PultNb,Key,btLayer);
  NumbAbon=MassKey[PultNb][btLayer][Key].NumbAbon;
  psAbnt=GetFreeAbonentStatePtr();
  if(psAbnt)
  {
    psAbnt->btPultNb=PultNb;
    psAbnt->btLayer=btLayer;
    psAbnt->btKey=Key;
    psAbnt->wOperatorChanel=wOper;
    psAbnt->NumbAbon=NumbAbon;
    psAbnt->btType=TDC;
    psAbntOld=GetAbonentTDC_Active(PultNb,NChan,NumbAbon);
    if(psAbntOld)
    {
      if(psAbntOld->btPultNb==PultNb) psAbnt=psAbntOld;
      else psAbnt->wConnectionPoint=NChan;
      SetLedColorIfTalk(psAbnt);
      SetAbonentStateByPtr(psAbnt,Talk,0);
      DSRS_On(PultNb,psAbnt->wOperatorChanel);
      Commute(true,psAbnt->wOperatorChanel,NChan);
      Commute(true,NChan,psAbnt->wOperatorChanel);
      if(IsAutomaticCall(PultNb)) btSound=0;
      else btSound=1;
      IndicCmd(PultNb,psAbnt->btLayer,Key,psAbnt->btLedState,btSound,btSound);
    }
    else
    {
      if(IsAutomaticCall(PultNb)) return; // �������������� ����� ��������� ��� ����������!
      if(_xOutgoingCallTDC(NChan,NumbAbon,bTest))
      {
        psAbnt->wConnectionPoint=NChan; // wConnectionPoint ����� �������������� ������ ���� ����� ��
        if(bTest)
        {
          psAbnt->btLedState=BLINK_RED_GREEN;
          SetAbonentStateByPtr(psAbnt,CallTest,0);
          psAbnt->dwTimerEnd=MassTimer[TIMER_TESTCALL]+xTaskGetTickCount();
        }
        else
        {
          psAbnt->btLedState=dLedStateOutgoingCall;
          SetAbonentStateByPtr(psAbnt,Call,0);
          psAbnt->dwTimerEnd=MassTimer[TIMER_OUTGOINGCALL]+xTaskGetTickCount();
        }
        Commute(true,psAbnt->wOperatorChanel,psAbnt->wConnectionPoint);
        IndicCmd(PultNb,psAbnt->btLayer,Key,psAbnt->btLedState,1,1);
      }
    }
  }
}

void PressCallKey_TDC(unsigned char PultNb,unsigned char Key,unsigned char btLayer,bool bTest)
{
  ABONENT_STATE_TP *psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);

  if(IsKey2Selector(PultNb,Key,btLayer,d_Abonent2SelectorOtherPult))
  { PutErrInfo2Display(PultNb,NULL); return; }
  if(!psAbnt)
  {
    if(IsCreatedTransit(PultNb)) XorAbnt2Transit(PultNb,Key);
    else OutgoingCallTDC(PultNb,Key,btLayer,bTest);
  }
  else
  {
    switch(GetAbonentStateFromPtr(psAbnt))
    {
      case IncomingCall:
      {
        AnswerIncomingCallMKA(psAbnt);
        break;
      }
      case Talk:
      {
        if(IsSelectorActive(PultNb) && IsPressAddMember(PultNb)) AddAbonent2Selector(PultNb,Key,btLayer);
        else HoldOn(PultNb,Key,btLayer,true);
        break;
      }
      case Hold:
      {
        HoldOn(PultNb,Key,btLayer,false);
        break;
      }
      case Call:
      {
        AnswerOutgoingCallTDC(PultNb,psAbnt->wConnectionPoint,psAbnt->NumbAbon);
        break;
      }
      default: break;
    }
  }
}

void PressCallKey_GroupTDC(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  unsigned short NChan,wOper;
  unsigned char btInd,SwetKey,NumbAbon;
  ABONENT_STATE_TP *psAbnt;
  static sCommand sCmd;

  NChan=MassKey[PultNb][btLayer][Key].NChan;
  wOper=GetOperatorChannelFromKey(PultNb,Key,btLayer);
  NumbAbon=MassKey[PultNb][btLayer][Key].NumbAbon;
  btInd=GetFreeAbonentStateIndex();
  if(btInd!=0xff)
  {
    sCmd.Source=UserRS485;
    sCmd.L=5;
    sCmd.A1=0;
    sCmd.BufCommand[0]=1; // �������
    sCmd.BufCommand[1]=MassKey[PultNb][btLayer][Key].NChan >> 8;
    sCmd.BufCommand[2]=MassKey[PultNb][btLayer][Key].NChan;
    sCmd.BufCommand[3]=CallTDC;
    sCmd.BufCommand[4]=MassKey[PultNb][btLayer][Key].NumbAbon-1; // ����� ��������
    if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
    else 
    {
      if(WaitReply(1,0))
      {
        psAbnt=GetAbonentStatePtrFromIndex(btInd);
        psAbnt->btPultNb=PultNb;
        psAbnt->btLayer=btLayer;
        psAbnt->btKey=Key;
        psAbnt->wConnectionPoint=NChan;
        psAbnt->wOperatorChanel=wOper;
        psAbnt->NumbAbon=NumbAbon;
        psAbnt->btType=GroupTDC;
        psAbnt->btLedState=dLedStateOutgoingCall;
        SetAbonentStateByPtr(psAbnt,Call,0);
        psAbnt->dwTimerEnd=MassTimer[TIMER_GROUPCALL]+xTaskGetTickCount();
        IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,1,1);
        Commute(true,psAbnt->wOperatorChanel,psAbnt->wConnectionPoint);
        Commute(true,psAbnt->wConnectionPoint,psAbnt->wOperatorChanel);
      }
    }
  }
}

