/////////////////////////////////////////////////////////////////////
void *pvPortMalloc(size_t xSize);
void vPortFree(void *pv);

unsigned short GetSelectorConnectionPoint(unsigned char SN);
unsigned char PlaySound2ConnectionPoint(unsigned short wNChan,char *pchFName);

/////////////////////////////////////////////////////////////////////
#define d_MaxAddAbnt2Selector       20

#define d_SelectorListener_Bad      0x02
#define d_SelectorIsdnAbnt_NoTalk   0x04

#define d_TmListenerErr             5000

/////////////////////////////////////////////////////////////////////
typedef struct
{
  unsigned short wScenNb,
                 wOperator;
} FileAbntSelectorHead_TP;

typedef struct
{ // ��������� ��� ����� ���������� ������������ ���������
  unsigned short Key; // ����� ������� ���������
  unsigned short Status; // ������ ���������
} FileAbntSelector_TP;

typedef struct // ��������� ��� ����� ���� ���������� ������������ ���������
{
  unsigned short Key; // ����� ������� ���������
  char Name[32]; // ��� ���������
} FileNameAbntSelector_TP;

typedef struct
{
  unsigned short Key, // ����� ������� ���������
                 Status,StatusOld;
  char btPisk,Name[33];
  unsigned char btSelectorCtrl;
  unsigned int dwTOut;
} SELECTOR_ABNT_TP;

typedef struct
{
  unsigned char PultNb,btLayer,btKey,btKeyBeginSelector,SelectorNb,btState,btCnt;
  unsigned short wScenNb,wOperator,wCP;
  unsigned int dwTOut,dwListenerTOut;
  SELECTOR_ABNT_TP *pMassAbnt;
} SELECTOR_TP;

///////////////////////////////////////////////////////////////////////////////
extern SELECTOR_TP s_sSelectors[NumberKeyboard];
extern SELECTOR_ABNT_TP *s_psAbntS;
extern bool s_bAutomaticCall[NumberKeyboard];

///////////////////////////////////////////////////////////////////////////////
// ���������� ������� ����������� ������ ���������
void SetAbonentStatus2Selector(unsigned char PultNb,SELECTOR_ABNT_TP *psAbnt,unsigned char btState);
SELECTOR_ABNT_TP *FindAbonentSelector(unsigned char PultNb,unsigned char Key,unsigned char btLayer);
void DetectListenerState(unsigned char PultNb);
void LoadAbonentSelectorName(unsigned char PultNb,SELECTOR_ABNT_TP *psAbnt);
bool _InsertAbonent2MassAbnt(unsigned char PultNb,ABONENT_STATE_TP *psAbnt);
void SetAutomaticCall(unsigned char PultNb,bool bAuto);
bool CreateCommute4Selector(unsigned char PultNb);
void SetAllAbonentSelector2Member(unsigned char PultNb,unsigned char btMode);
void EnOutAbonentSelector(unsigned char PultNb,SELECTOR_ABNT_TP *psAbntS,ABONENT_STATE_TP *psAbnt,unsigned char btEnOut);
void _xCmdAbntSelectorStatus(unsigned char PultNb,SELECTOR_ABNT_TP *psAbntS,unsigned char btStatus);

