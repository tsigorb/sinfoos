#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

#define d_MAX_TRANSIT           (NumberKeyboard*2)
#define d_MAX_TRANSIT_ABNT      10

/////////////////////////////////////////////////////////////////////
#pragma pack(1)
typedef struct
{ // !! ������ ��������� ������ ���� ������ !!
  unsigned int dwTOut;
  unsigned char PultNb,btLayer,btKey,btTransitNumb,
                MasAbnt[d_MAX_TRANSIT_ABNT];
} TRANSIT_TP;
#pragma pack()

/////////////////////////////////////////////////////////////////////
static TRANSIT_TP sCreatedTransit[NumberKeyboard];

static TRANSIT_TP sMassTransit[d_MAX_TRANSIT];

static char *strTransitFNameFmt="Transit%02d.%03d",
            *strTransitNameFmt="TR%02d%03d";

/////////////////////////////////////////////////////////////////////
void InitTransitVars(void)
{
  unsigned char PultNb,btInd;

  vPortEnableSwitchTask(false);
  memset(sCreatedTransit,0xff,sizeof(sCreatedTransit));
  for(PultNb=0;PultNb<NumberKeyboard;PultNb++) sCreatedTransit[PultNb].dwTOut=0;
  memset(sMassTransit,0xff,sizeof(sMassTransit));
  for(btInd=0;btInd<d_MAX_TRANSIT;btInd++) sMassTransit[btInd].dwTOut=0;
  vPortEnableSwitchTask(true);
}

bool IsCreatedTransit(unsigned char PultNb)
{
  if(sCreatedTransit[PultNb].PultNb!=0xff) return(true);
  return(false);
}

unsigned char GetCountAbntTransit(TRANSIT_TP *psTransit)
{
  unsigned char btInd,btCnt;

  for(btInd=0,btCnt=0;btInd<d_MAX_TRANSIT_ABNT;btInd++)
  {
    if(psTransit->MasAbnt[btInd]!=0xff) btCnt++;
  }
  return(btCnt);
}

bool _xDeleteTransit(unsigned char btInd)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.L=10;
  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[1]=111;
  LockPrintf();
  sprintf((char *)sCmd.BufCommand+2,strTransitNameFmt,sMassTransit[btInd].PultNb,sMassTransit[btInd].btTransitNumb);
  UnlockPrintf();
  sCmd.BufCommand[9]=0;
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(WaitReply(11,111)) return(true);
  }
  return(false);
}

void DeleteTransit(unsigned char btInd,bool bNeedDelFile)
{
  unsigned char btAbnt,btKey;

  _xDeleteTransit(btInd);
  if(bNeedDelFile)
  {
    static char chFName[20];

    LockPrintf();
    sprintf(chFName,strTransitFNameFmt,MassM1[sMassTransit[btInd].PultNb],sMassTransit[btInd].btTransitNumb);
    UnlockPrintf();
    DeleteFile(chFName);
  }
/*  for(btAbnt=0;btAbnt<d_MAX_TRANSIT_ABNT;btAbnt++)
  {
    btKey=sMassTransit[btInd].MasAbnt[btAbnt];
    if(btKey!=0xff)
    { IndicCmd(sMassTransit[btInd].PultNb,sMassTransit[btInd].btLayer,btKey,OFF,0,0); }
  }*/
  sMassTransit[btInd].dwTOut=0;
  sMassTransit[btInd].PultNb=0xff;
}

void ClearCreatedTransit(unsigned char PultNb)
{
  unsigned char btInd,btKey;

  for(btInd=0;btInd<d_MAX_TRANSIT_ABNT;btInd++)
  {
    btKey=sCreatedTransit[PultNb].MasAbnt[btInd];
    if(btKey!=0xff) IndicCmd(PultNb,sCreatedTransit[PultNb].btLayer,btKey,OFF,0,0);
  }
  IndicCmd(PultNb,sCreatedTransit[PultNb].btLayer,sCreatedTransit[PultNb].btKey,OFF,0,0);
  sCreatedTransit[PultNb].dwTOut=0;
  sCreatedTransit[PultNb].PultNb=0xff;
}

void DeleteAllTransit(void)
{
  unsigned char btInd,PultNb;

  for(btInd=0;btInd<d_MAX_TRANSIT;btInd++)
  {
    if(sMassTransit[btInd].PultNb!=0xff)
    {
      IndicCmd(sMassTransit[btInd].PultNb,sMassTransit[btInd].btLayer,sMassTransit[btInd].btKey,OFF,0,0);
      DeleteTransit(btInd,false);
    }
  }
  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if(IsCreatedTransit(PultNb)) ClearCreatedTransit(PultNb);
  }
}

bool _xCreateTransit(TRANSIT_TP *psTransit)
{
  unsigned char btInd,btKey,btOff;
  unsigned short wCP;
  static sCommand sCmd;

  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[1]=185;
  LockPrintf();
  sprintf((char *)sCmd.BufCommand+2,strTransitNameFmt,psTransit->PultNb,psTransit->btTransitNumb);
  memset(sCmd.BufCommand+9,0x0,5);
  sprintf((char *)sCmd.BufCommand+14,"%03d",GetCountAbntTransit(psTransit));
  UnlockPrintf();
  btOff=17;
  for(btInd=0;btInd<d_MAX_TRANSIT_ABNT;btInd++)
  {
    btKey=psTransit->MasAbnt[btInd];
    if(btKey!=0xff)
    {
      wCP=MassKey[psTransit->PultNb][psTransit->btLayer][btKey].NChan;
      if(wCP & 0xff00)
      { sCmd.BufCommand[btOff]=0x31; sCmd.BufCommand[btOff+1]=0x43; }
      else
      { sCmd.BufCommand[btOff]=0x30; sCmd.BufCommand[btOff+1]=0x30; }
      wCP&=0xff;
      sCmd.BufCommand[btOff+2]=(wCP/10)+0x30;
      sCmd.BufCommand[btOff+3]=(wCP%10)+0x30;
      btOff+=4;
    }
  }
  sCmd.L=btOff;
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(WaitReply(11,185)) return(true);
  }
  return(false);
}

bool StoreTransit2File(TRANSIT_TP *psTransit)
{
  int iSz;
  static char chFName[20];
  HANDLE hFile;

  LockPrintf();
  sprintf(chFName,strTransitFNameFmt,MassM1[psTransit->PultNb],psTransit->btTransitNumb);
  UnlockPrintf();
  hFile=CreateFile(chFName,CREATE_ALWAYS,sizeof(TRANSIT_TP));
  if(hFile<0) return false;
  iSz=WriteFile(hFile,psTransit,sizeof(TRANSIT_TP));
  CloseHandle(hFile);
  if(iSz==sizeof(TRANSIT_TP)) return(true);
  DeleteFile(chFName);
  return(false);
}

bool AddTransit(TRANSIT_TP *psTransit,bool bRead)
{
  unsigned char btInd,btAbnt,btKey,btSound;

  for(btInd=0;btInd<d_MAX_TRANSIT;btInd++)
  {
    if(sMassTransit[btInd].PultNb==0xff)
    {
      if(bRead || StoreTransit2File(psTransit))
      {
        if(_xCreateTransit(psTransit))
        {
          memcpy(sMassTransit+btInd,psTransit,sizeof(TRANSIT_TP));
          psTransit->PultNb=0xff;
          psTransit=sMassTransit+btInd;
/*          for(btAbnt=0;btAbnt<d_MAX_TRANSIT_ABNT;btAbnt++)
          { // ���� ���� ������� ��� ������� ��������� ������ ��� ����������� ��������!
            btKey=psTransit->MasAbnt[btAbnt];
            if(btKey!=0xff)
            { IndicCmd(psTransit->PultNb,psTransit->btLayer,btKey,dLedStateAbntTransit,0,0); }
          }*/
          if(!bRead)
          { // ����� ���������� ����� �������� ��������!
            for(btAbnt=0;btAbnt<d_MAX_TRANSIT_ABNT;btAbnt++)
            {
              btKey=psTransit->MasAbnt[btAbnt];
              if(btKey!=0xff)
              { IndicCmd(psTransit->PultNb,psTransit->btLayer,btKey,OFF,0,0); }
            }
          }
          btSound=bRead ? 0 : 1;
          IndicCmd(psTransit->PultNb,psTransit->btLayer,psTransit->btKey,dLedStateTransitWork,btSound,btSound);
          return(true);
        }
      }
    }
  }
  return(false);
}

bool IsTestingKeyTransitOK(unsigned char PultNb,TRANSIT_TP *psTransit)
{
  unsigned char btAbnt,btKey,btLayer,btCnt;

  btLayer=psTransit->btLayer;
  btKey=psTransit->btKey;
  if((MassKey[PultNb][btLayer][btKey].CodeKey[0]!=ServiceKey) ||
     (MassKey[PultNb][btLayer][btKey].CodeKey[1]!=Transit) ||
     (MassKey[PultNb][btLayer][btKey].NChan!=psTransit->btTransitNumb)) return(false);
  btCnt=0;
  for(btAbnt=0;btAbnt<d_MAX_TRANSIT_ABNT;btAbnt++)
  {
     btKey=psTransit->MasAbnt[btAbnt];
     if(btKey!=0xff)
     {
       btCnt++;
/*       if((MassKey[PultNb][btLayer][btKey].CodeKey[0]!=CallKey) ||
          (MassKey[PultNb][btLayer][btKey].TypeAbon!=NoCall))*/
       if((MassKey[PultNb][btLayer][btKey].CodeKey[0]!=CallKey) ||
          (MassKey[PultNb][btLayer][btKey].TypeAbon==ISDN) ||
          (MassKey[PultNb][btLayer][btKey].TypeAbon==ATC) ||
          (MassKey[PultNb][btLayer][btKey].TypeAbon==PultCall))
       { return(false); }
     }
  }
  if(btCnt<2) return(false);
  return(true);
}

void RestoreAllTransit(void)
{
  unsigned char PultNb_Name,TransitNb_Name;
  unsigned short wInd;
  bool bOk;
  HANDLE hFile;
  MYFILE sFile;
  static TRANSIT_TP sTransit;

  wInd=0;
  while(FindNextFile(wInd,&sFile)!=(unsigned long)-1)
  { // ��������� ��� �����
    wInd++;
    if((strncmp(sFile.szFileName,strTransitFNameFmt,7)==0) &&
       (sFile.szFileName[9]=='.'))
    { // ���� ����� ��� ����� � ������� ���������
      LockPrintf();
      sFile.szFileName[9]=0;
      PultNb_Name=GetPultIndFromSN((unsigned char)atoi(sFile.szFileName+7));
      sFile.szFileName[13]=0;
      TransitNb_Name=atoi(sFile.szFileName+10);
      UnlockPrintf();
      if((PultNb_Name<NumberKeyboard) &&
         (TransitNb_Name>0) && (TransitNb_Name<=48))
      { //  ��������� ������������ ����� ����������
        bOk=false;
        sFile.szFileName[9]='.';
        hFile=CreateFile(sFile.szFileName,GENERIC_READ,sFile.FileLen);
        if(hFile>=0) 
        {
          if(ReadFile(hFile,&sTransit,sizeof(TRANSIT_TP))==sizeof(TRANSIT_TP))
          {
            if((sTransit.PultNb==PultNb_Name) &&
               (sTransit.btTransitNumb==TransitNb_Name) &&
               (sTransit.dwTOut==0))
            { // ��������� ������� ������ �� ������ �, ���� ��� ��, ��������� �������
              if(IsTestingKeyTransitOK(PultNb_Name,&sTransit))
              { bOk=AddTransit(&sTransit,true); }
            }
          }
          CloseHandle(hFile);
        }
        if(!bOk) DeleteFile(sFile.szFileName);
      }
    }
  }
}

void RestoreTransitLedState(unsigned char PultNb)
{
  unsigned char btInd,btAbnt,btKey;
  TRANSIT_TP *psTransit;

  if(IsCreatedTransit(PultNb)) ClearCreatedTransit(PultNb);
  psTransit=sMassTransit;
  for(btInd=0;btInd<d_MAX_TRANSIT;btInd++,psTransit++)
  {
    if(PultNb==psTransit->PultNb)
    {
/*      for(btAbnt=0;btAbnt<d_MAX_TRANSIT_ABNT;btAbnt++)
      {  // ���� ���� ������� ��� ������� ��������� ������ ��� ����������� ��������!
        btKey=psTransit->MasAbnt[btAbnt];
        if(btKey!=0xff) IndicCmd(PultNb,psTransit->btLayer,btKey,dLedStateAbntTransit,0,0);
      }*/
      IndicCmd(psTransit->PultNb,psTransit->btLayer,psTransit->btKey,dLedStateTransitWork,0,0);
    }
  }
}

unsigned char FindTransit(unsigned char PultNb,unsigned char btLayer,unsigned char btKey)
{
  unsigned char btInd;

  for(btInd=0;btInd<d_MAX_TRANSIT;btInd++)
  {
    if((PultNb==sMassTransit[btInd].PultNb) &&
       (btLayer==sMassTransit[btInd].btLayer) &&
       (btKey==sMassTransit[btInd].btKey) &&
       (MassKey[PultNb][btLayer][btKey].NChan==sMassTransit[btInd].btTransitNumb))
    { return(btInd); } 
  }
  return(0xff);
}

void ShowTransitAbnts(unsigned char PultNb,unsigned char btLayer,unsigned char btKey)
{
  unsigned char btInd,btAbnt;

  btInd=FindTransit(PultNb,btLayer,btKey);
  if(btInd==0xff) return;
  IndicCmd(sMassTransit[btInd].PultNb,sMassTransit[btInd].btLayer,btKey,dLedStateTransitTest,1,1);
  for(btAbnt=0;btAbnt<d_MAX_TRANSIT_ABNT;btAbnt++)
  {
    btKey=sCreatedTransit[PultNb].MasAbnt[btAbnt];
    if(btKey!=0xff) IndicCmd(PultNb,btLayer,btKey,dLedStateTransitTest,0,0);
  }
  sMassTransit[btInd].dwTOut=3000+xTaskGetTickCount();
}

void ShowTransitErr(TRANSIT_TP *psTransit)
{
  unsigned char btAbnt,btKey;

  IndicCmd(psTransit->PultNb,psTransit->btLayer,psTransit->btKey,dLedStateTransitErr,1,1);
  for(btAbnt=0;btAbnt<d_MAX_TRANSIT_ABNT;btAbnt++)
  {
    btKey=psTransit->MasAbnt[btAbnt];
    if(btKey!=0xff) IndicCmd(psTransit->PultNb,psTransit->btLayer,btKey,dLedStateTransitErr,0,0);
  }
  psTransit->dwTOut=3000+xTaskGetTickCount();
}

void ControlTransitTOut(void)
{
  unsigned char PultNb,btInd,btAbnt,btKey;

  for(btInd=0;btInd<d_MAX_TRANSIT;btInd++)
  {
    if((sMassTransit[btInd].PultNb!=0xff) && sMassTransit[btInd].dwTOut)
    {
      if(IsCurrentTickCountGT(sMassTransit[btInd].dwTOut))
      { // ��������������� ����������� �������� � ������� ������
        sMassTransit[btInd].dwTOut=0;
        IndicCmd(sMassTransit[btInd].PultNb,sMassTransit[btInd].btLayer,sMassTransit[btInd].btKey,dLedStateTransitWork,0,0);
        for(btAbnt=0;btAbnt<d_MAX_TRANSIT_ABNT;btAbnt++)
        {
          btKey=sCreatedTransit[btInd].MasAbnt[btAbnt];
          //if(btKey!=0xff) IndicCmd(sMassTransit[btInd].PultNb,sMassTransit[btInd].btLayer,btKey,dLedStateAbntTransit,0,0);
          if(btKey!=0xff) IndicCmd(sMassTransit[btInd].PultNb,sMassTransit[btInd].btLayer,btKey,OFF,0,0);
        }
      }
    }
  }
  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if(IsCreatedTransit(PultNb) && sCreatedTransit[PultNb].dwTOut)
    {
      if(IsCurrentTickCountGT(sCreatedTransit[PultNb].dwTOut))
      { // ����� ���������� ����� ����������� ������ �������� ��������
        sCreatedTransit[PultNb].dwTOut=0;
        ClearCreatedTransit(PultNb);
      }
    }
  }
}

void PressTransitKey(unsigned char PultNb,unsigned char btLayer,unsigned char btKey,unsigned char btOtkl)
{
  if(btOtkl)
  {
    unsigned char btInd=FindTransit(PultNb,btLayer,btKey);

    if(btInd!=0xff)
    {
      DeleteTransit(btInd,true);
      IndicCmd(PultNb,btLayer,btKey,OFF,1,1);
    }
  }
  else
  {
    if(IsPressTest(PultNb)) ShowTransitAbnts(PultNb,btLayer,btKey);
    else
    {
      if(!IsCreatedTransit(PultNb))
      {
        unsigned char btInd;

        sCreatedTransit[PultNb].dwTOut=0;
        sCreatedTransit[PultNb].PultNb=PultNb;
        sCreatedTransit[PultNb].btLayer=btLayer;
        sCreatedTransit[PultNb].btKey=btKey;
        sCreatedTransit[PultNb].btTransitNumb=MassKey[PultNb][btLayer][btKey].NChan;
        IndicCmd(PultNb,btLayer,btKey,dLedStateTransitAdd,1,1);
        memset(sCreatedTransit[PultNb].MasAbnt,0xff,sizeof(sCreatedTransit[PultNb].MasAbnt));
        btInd=FindTransit(PultNb,btLayer,btKey);
        if(btInd!=0xff)
        {
          memcpy(sCreatedTransit[PultNb].MasAbnt,sMassTransit[btInd].MasAbnt,sizeof(sCreatedTransit[PultNb].MasAbnt));
          DeleteTransit(btInd,true);
          for(btInd=0;btInd<d_MAX_TRANSIT_ABNT;btInd++)
          {
            btKey=sCreatedTransit[PultNb].MasAbnt[btInd];
            if(btKey!=0xff) IndicCmd(PultNb,btLayer,btKey,dLedStateAbntTransitAdd,0,0);
          }
        }
      }
    }
  }
}

void UnPressTransitKey(unsigned char PultNb,unsigned char btLayer,unsigned char btKey)
{
  if(IsCreatedTransit(PultNb))
  {
    if((sCreatedTransit[PultNb].btLayer==btLayer) &&
       (sCreatedTransit[PultNb].btKey==btKey))
    {
      if(GetCountAbntTransit(sCreatedTransit+PultNb)>=2)
      {
        if(!AddTransit(sCreatedTransit+PultNb,false)) ShowTransitErr(sCreatedTransit+PultNb);
      }
      else ShowTransitErr(sCreatedTransit+PultNb);
    }
  }
}

void XorAbnt2Transit(unsigned char PultNb,unsigned char btKey)
{
  unsigned char btInd;

  for(btInd=0;btInd<d_MAX_TRANSIT_ABNT;btInd++)
  {
    if(sCreatedTransit[PultNb].MasAbnt[btInd]==btKey)
    {
      sCreatedTransit[PultNb].MasAbnt[btInd]=0xff;
      IndicCmd(PultNb,sCreatedTransit[PultNb].btLayer,btKey,OFF,1,1);
      return;
    }
  }
  for(btInd=0;btInd<d_MAX_TRANSIT_ABNT;btInd++)
  {
    if(sCreatedTransit[PultNb].MasAbnt[btInd]==0xff)
    {
      sCreatedTransit[PultNb].MasAbnt[btInd]=btKey;
      IndicCmd(PultNb,sCreatedTransit[PultNb].btLayer,btKey,dLedStateAbntTransitAdd,1,1);
      return;
    }
  }
}

