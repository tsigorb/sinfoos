#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

/////////////////////////////////////////////////////////////////////
static unsigned char s_MasFreqTDC[90]=
{
#include "..\DAT\NumberTDC.dat"
};

/////////////////////////////////////////////////////////////////////
unsigned char FindPultGroupTDCCall(unsigned char PultNb,unsigned char *pbtLayer,unsigned short wNChan)
{
  unsigned char btInd;
  ABONENT_STATE_TP *psAbnt;

  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return 0xff;
    if((psAbnt->wConnectionPoint==wNChan) && (psAbnt->btPultNb==PultNb) && (psAbnt->btType==GroupTDC))
    { *pbtLayer=psAbnt->btLayer; return(btInd); }
    btInd++;
  }
  while(1);
}

unsigned char FindKeyTDC(unsigned char PultNb,unsigned char *pbtLayer,unsigned short wNChan,unsigned char AbntNumb)
{
  unsigned char btKey,btLayer;

  for(btLayer=0;btLayer<GetMaxLayer(PultNb);btLayer++)
  {
    if(IsKbdLayerLock(PultNb,btLayer)) continue;
    for(btKey=0;btKey<NumberKey;btKey++)
    {
      if((MassKey[PultNb][btLayer][btKey].NChan==wNChan) && (MassKey[PultNb][btLayer][btKey].TypeAbon==TDC) &&
         (MassKey[PultNb][btLayer][btKey].NumbAbon==AbntNumb))
      { *pbtLayer=btLayer; return(btKey); }
    }
  }
  return(0xff);
}

/*
1.���� ���������� ������ �� �������� ������ �� ���� � �������� ������, �� ����� � ����������� false
2.���� ���� ���������� ������� �� �������� ������ ����� ������, ��:
  2.1.��� �������� ������� ��������� ����� ����� ������ ��������
  2.2.������� �������� ��������� � ��������� ���������
  2.3.����� � ����������� true
3.���� ���, �� ����� � ����������� false
*/
bool AnswerGroupTDC(unsigned char PultNb,unsigned short wNChan,unsigned char AbntNumb)
{
  unsigned char Key,Layer,btState,btInd;
  ABONENT_STATE_TP *psAbnt;

  btInd=FindPultGroupTDCCall(PultNb,&Layer,wNChan);
  if(btInd==0xff) return(false);
  Key=FindKeyTDC(PultNb,&Layer,wNChan,AbntNumb);
  if(Key==0xff) return(false);
  psAbnt=GetFreeAbonentStatePtr();
  if(!psAbnt) return(false);
  psAbnt->wConnectionPoint=wNChan;
  psAbnt->wOperatorChanel=GetOperatorChannelFromKey(PultNb,Key,Layer);
  psAbnt->btPultNb=PultNb;
  psAbnt->btLayer=Layer;
  psAbnt->btKey=Key;
  psAbnt->NumbAbon=AbntNumb;
  psAbnt->btType=MassKey[PultNb][Layer][Key].TypeAbon;
  psAbnt->dwState=Off;
  AnswerIncomingCallMKA(psAbnt);
  psAbnt=GetAbonentStatePtrFromIndex(btInd);
  IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,OFF,0,0);
  ClearAbonentStateByPtr(psAbnt);
  return(true);
}

void ControlTDCTOut(void)
{
  unsigned char btInd,btState;
  ABONENT_STATE_TP *psAbnt;

  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return;
    btInd++;
    if((psAbnt->wConnectionPoint!=0xffff) && (psAbnt->btType==TDC) && psAbnt->dwTimerEnd)
    {
      if(IsCurrentTickCountGT(psAbnt->dwTimerEnd))
      {
        switch(GetAbonentStateFromPtr(psAbnt))
        {
          case IncomingCall:
            DelIncomingCallMKAFromOtherPult(psAbnt->btPultNb,psAbnt->wConnectionPoint,psAbnt->NumbAbon);
          case Call:
            IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,NO_INDIC,3,1);
            OtboiAbonentMKA(psAbnt->btPultNb,psAbnt->wConnectionPoint,psAbnt->NumbAbon,TDC);
            return;
          case CallTest:
            // ����������� ������� �� �������
            psAbnt->btLedState=FAST_BLINK_RED;
            IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,3,1);
            SetAbonentStateByPtr(psAbnt,CallTestError,0);
            psAbnt->dwTimerEnd=d_IndicTestCallResult+xTaskGetTickCount();
            return;
          case CallTestError:
          case CallTestUp:
          case CallTestDown:
            // ��� ����-���� �������
            OtboiAbonentMKA(psAbnt->btPultNb,psAbnt->wConnectionPoint,psAbnt->NumbAbon,TDC);
            return;
        }
      }
    }
  }
  while(1);
}

bool IsOutgoingCallTDC(unsigned char PultNb,unsigned short wNChan,unsigned char AbntNumb)
{
  unsigned char btInd,btState;
  ABONENT_STATE_TP *psAbnt;

  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return(false);
    btInd++;
    if((psAbnt->btPultNb==PultNb) &&
       (psAbnt->wConnectionPoint==wNChan) &&
       (psAbnt->btType==TDC) &&
       (psAbnt->NumbAbon==AbntNumb))
    {
      btState=GetAbonentStateFromPtr(psAbnt);
      if((btState==Call) || (btState==Talk) || (btState==CallTest)) return(true);
    }
  }
  while(1);
}

void AnswerOutgoingCallTDC(unsigned char PultNb,unsigned short wNChan,unsigned char AbntNumb)
{
  unsigned char btInd,btSound;
  ABONENT_STATE_TP *psAbnt;

  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return;
    btInd++;
    if((psAbnt->btPultNb==PultNb) &&
       (psAbnt->wConnectionPoint==wNChan) &&
       (psAbnt->NumbAbon==AbntNumb) &&
       (psAbnt->btType==TDC))
    {
      switch(GetAbonentStateFromPtr(psAbnt))
      {
        case Call:
          if(IsAutomaticCallAbonent(psAbnt)) btSound=0;
          else
          {
            if(IsGroupAutoAnswer4PultFromChan(PultNb,psAbnt->wConnectionPoint)) AddAbnt2FreeHandConf(PultNb,psAbnt);
            else Commute(true,psAbnt->wConnectionPoint,psAbnt->wOperatorChanel);
            DSRS_On(psAbnt->btPultNb,psAbnt->wOperatorChanel);
            btSound=1;
          }
          SetLedColorIfTalk(psAbnt);
          SetAbonentStateByPtr(psAbnt,Talk,0);
          psAbnt->dwTimerEnd=0;
          IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,btSound,btSound);
          return;
        case CallTest: // ����������� ������� ������� - ������ �����
          Commute(false,psAbnt->wOperatorChanel,psAbnt->wConnectionPoint);
          psAbnt->btLedState=GREEN;
          IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,1,1);
          SetAbonentStateByPtr(psAbnt,CallTestUp,0);
          psAbnt->dwTimerEnd=d_IndicTestCallResult+xTaskGetTickCount();
          return;
        default: return;
      }
    }
  }
  while(1);
}

void IncomingCallFromAbonentMKA(unsigned short wNChan,unsigned char TypeAbon,unsigned char AbntNumb)
{ // ����� ��������� �� ��� ������, � ������������� ������ �����!
  unsigned char PultNb,btLayer,btKey,btInd,
                btLed,btLedState;
  bool bKey2Selector;
  ABONENT_STATE_TP *psAbnt;

  if(TypeAbon==TDC)
  { // ����, ���-�� ��������� ����� ��� ����� ��������
//SendStr2IpLogger("$DEBUG$IncomingCall TDC: channel=#%d, AbntNumb=%d",wNChan,AbntNumb);
    for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
    {
      if(MassM1[PultNb] && IsKbdReady(PultNb) && IsKbdUnlock(PultNb))
      {
        btKey=FindAbntKey2Pult(PultNb,&btLayer,wNChan,&TypeAbon,AbntNumb);
        if(btKey!=0xff)
        {
          if(IsOutgoingCallTDC(PultNb,wNChan,AbntNumb))
          { AnswerOutgoingCallTDC(PultNb,wNChan,AbntNumb); return; }
        }
      }
    }
  }
  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  { // ����, ����� �� ���-���� ������������� ������� �����
    if(MassM1[PultNb] && IsKbdReady(PultNb) && IsKbdUnlock(PultNb))
    {
      btKey=FindAbntKey2Pult(PultNb,&btLayer,wNChan,&TypeAbon,AbntNumb);
      if(btKey!=0xff)
      {
        if(!IsSelectorActive(PultNb))
        {
          if(IsKey2Selector(PultNb,btKey,btLayer,d_Abonent2SelectorOtherPult)) continue;
        }
        bKey2Selector=IsKey2Selector(PultNb,btKey,btLayer,d_Abonent2SelectorCurrentPult);
        if(IsGroupAutoAnswer4PultFromChan(PultNb,wNChan) ||
           ExistTalk2LineMKA(PultNb,wNChan) || bKey2Selector)
        {
          btInd=FindPultGroupTDCCall(PultNb,&btLayer,wNChan);
          if(btInd!=0xff)
          {
            psAbnt=GetAbonentStatePtrFromIndex(btInd);
            Commute(false,psAbnt->wOperatorChanel,psAbnt->wConnectionPoint);
            IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,OFF,0,0);
            ClearAbonentStateByPtr(psAbnt);
          }
          psAbnt=GetFreeAbonentStatePtr();
          if(!psAbnt) return;
          psAbnt->wConnectionPoint=wNChan;
          psAbnt->wOperatorChanel=GetOperatorChannelFromKey(PultNb,btKey,btLayer);
          psAbnt->btPultNb=PultNb;
          psAbnt->btLayer=btLayer;
          psAbnt->btKey=btKey;
          psAbnt->NumbAbon=AbntNumb;
          psAbnt->btType=TypeAbon;
          psAbnt->dwTimerEnd=0;
          psAbnt->dwState=Off;
          SetAbonentStateByPtr(psAbnt,Talk,dIncomingTalkFlag);
          SetLedColorIfTalk(psAbnt);
          if(bKey2Selector) AddTDCIPPM2CurrentSelector(PultNb,psAbnt);
          else
          {
            DSRS_On(psAbnt->btPultNb,psAbnt->wOperatorChanel);
            IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,1,1);
//SendStr2IpLogger("$DEBUG$IncomingCall TDC: channel=#%d, AbntNumb=%d, wOp=%x",wNChan,AbntNumb,psAbnt->wOperatorChanel);
            AddAbnt2FreeHandConf(PultNb,psAbnt);
          }
          return;
        }
      }
    }
  }
  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if(MassM1[PultNb] && IsKbdReady(PultNb) && IsKbdUnlock(PultNb))
    {
      btKey=FindAbntKey2Pult(PultNb,&btLayer,wNChan,&TypeAbon,AbntNumb);
      if(btKey!=0xff)
      {
        if(!IsSelectorActive(PultNb))
        {
          if(IsKey2Selector(PultNb,btKey,btLayer,d_Abonent2SelectorOtherPult)) continue;
        }
        if(GetAbonentStatePtrFromKey(PultNb,btKey,btLayer)) break;
        switch(MassKey[PultNb][btLayer][btKey].TypeAbon)
        {
          case TDC:
            if(AnswerGroupTDC(PultNb,wNChan,AbntNumb)) return;
            break;
          default: break;
        }
        psAbnt=GetFreeAbonentStatePtr();
        if(!psAbnt) return;
        psAbnt->wConnectionPoint=wNChan;
        psAbnt->wOperatorChanel=GetOperatorChannelFromKey(PultNb,btKey,btLayer);
        psAbnt->btPultNb=PultNb;
        psAbnt->btLayer=btLayer;
        psAbnt->btKey=btKey;
        psAbnt->NumbAbon=AbntNumb;
        psAbnt->btType=TypeAbon;
        psAbnt->dwState=Off;
        psAbnt->btLedState=dLedStateIncomingCall;
        SetAbonentStateByPtr(psAbnt,IncomingCall,0);
        psAbnt->dwTimerEnd=MassTimer[TIMER_INCOMINGCALL]+xTaskGetTickCount();
        IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,0,0);
      }
    }
  }
}

void IncomingCall_TDC(unsigned short wNChan,unsigned char AbntNumb,unsigned char FreqInd)
{ // ����� ��������� �� ��� ������, � ������������� ������ �����!
  unsigned char i,TrubaUp,TrubaDown;

  i=(AbntNumb-1) << 1;
  TrubaUp=s_MasFreqTDC[i]; // ����� ������� �������� ������
  TrubaDown=s_MasFreqTDC[i+1]; // ����� ������� ���������� ������
  if(FreqInd==TrubaUp) IncomingCallFromAbonentMKA(wNChan,TDC,AbntNumb);
  else
  {
    if(FreqInd==TrubaDown) OtboiAbonentMKA(0xff,wNChan,AbntNumb,TDC);
  }
}

void Test_AutoGenerationIncomingCallTDC(unsigned char PultNb,unsigned char btOff)
{
  unsigned short wNChan;
  static unsigned char s_btKey=0;
  unsigned char btCnt,btKey,AbntNumb,FreqInd;

  if(btOff)
  {
    for(btCnt=0;s_btKey && (btCnt<4);s_btKey--)
    {
      btKey=s_btKey-1;
      if((MassKey[PultNb][0][btKey].CodeKey[0]==CallKey) &&
         (MassKey[PultNb][0][btKey].TypeAbon==TDC))
      {
        wNChan=MassKey[PultNb][0][btKey].NChan;
        AbntNumb=MassKey[PultNb][0][btKey].NumbAbon;
        FreqInd=(AbntNumb-1) << 1;
        IncomingCall_TDC(wNChan,AbntNumb,s_MasFreqTDC[FreqInd+1]);
        btCnt++;
      }
    }
  }
  else
  {
    for(btCnt=0;(s_btKey<NumberKey) && (btCnt<5);s_btKey++)
    {
      if((MassKey[PultNb][0][s_btKey].CodeKey[0]==CallKey) &&
         (MassKey[PultNb][0][s_btKey].TypeAbon==TDC))
      {
        wNChan=MassKey[PultNb][0][s_btKey].NChan;
        AbntNumb=MassKey[PultNb][0][s_btKey].NumbAbon;
        FreqInd=(AbntNumb-1) << 1;
        IncomingCall_TDC(wNChan,AbntNumb,s_MasFreqTDC[FreqInd]);
        btCnt++;
      }
    }
  }
}

