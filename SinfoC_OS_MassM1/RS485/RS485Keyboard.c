#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

/////////////////////////////////////////////////////////////////////
void Indic_Sound(sIndic *psKBIndicS);
bool InterviewKeyboard(unsigned char PultNb,unsigned char SN);

/////////////////////////////////////////////////////////////////////
typedef struct
{
  bool bNoRst,bWork,bUnlock;
  bool bLayerUnlock[NumberGroup];
  unsigned int dwTimeNextRead;
  unsigned char XchgErr;
} KBD_STATE_TP;

#define MAX_XCHG_ERR    3

static KBD_STATE_TP MasKbdState[NumberKeyboard];

/////////////////////////////////////////////////////////////////////
bool IsKbdReset(unsigned char PultNb)
{ return(!MasKbdState[PultNb].bNoRst); }

void SetKbdReset(unsigned char PultNb,bool bRst)
{ MasKbdState[PultNb].bNoRst=!bRst; }

bool IsKbdReady(unsigned char PultNb)
{ // ��������/�� ��������
  unsigned char SN=MassM1[PultNb];
  if(!SN || (SN==0xff)) return(false);
  if(SN>=16)
  {
    unsigned int dwAddr;
    unsigned short wPort;
    return(GetAddressIpPult(SN,&dwAddr,&wPort));
  }
  return(MasKbdState[PultNb].bWork);
}

void _SaveKbdLayerUnlockState(unsigned char PultNb)
{
  unsigned char btLayer;
  unsigned short wBlockState=0;

  for(btLayer=0;btLayer<NumberGroup;btLayer++) wBlockState|=(MasKbdState[PultNb].bLayerUnlock[btLayer] << (btLayer+1));
  wBlockState|=MasKbdState[PultNb].bUnlock;
  WrFlash(wBlockState,AdrBlockPult+PultNb*2);
}

bool GetKbdUnlockState(unsigned char PultNb)
{ return((RdFlash(AdrBlockPult+PultNb*2) & 0x1)!=0); }

bool GetLayerUnlockState(unsigned char PultNb,unsigned char btLayer)
{
  if(MassM1[PultNb]>=16) return(true);
  return((RdFlash(AdrBlockPult+PultNb*2) & (0x1 << (btLayer+1)))!=0);
}

void SetKbdUnlockState(unsigned char PultNb,bool bUnlock)
{
  MasKbdState[PultNb].bUnlock=bUnlock;
  if(bUnlock!=GetKbdUnlockState(PultNb))
  {
    _SaveKbdLayerUnlockState(PultNb);
    //if(!bUnlock) DeleteAllConnections2Pult(PultNb);
  }
}

void SetLayerUnlockState(unsigned char PultNb,unsigned char btLayer,bool bUnlock)
{
  if(MassM1[PultNb]>=16) return;
  MasKbdState[PultNb].bLayerUnlock[btLayer]=bUnlock;
  if(bUnlock!=GetLayerUnlockState(PultNb,btLayer))
  {
    _SaveKbdLayerUnlockState(PultNb);
    //if(!bUnlock) DeleteAllConnections2PultLayer(PultNb,btLayer);
  }
}

bool IsKbdUnlock(unsigned char PultNb)
{
  return(MasKbdState[PultNb].bUnlock);
}

bool IsKbdLock(unsigned char PultNb)
{
  return(!MasKbdState[PultNb].bUnlock);
}

bool IsKbdLayerLock(unsigned char PultNb,unsigned char btLayer)
{
  if(MassM1[PultNb]>=16) return(false);
  return(!MasKbdState[PultNb].bLayerUnlock[btLayer]);
}

void PutKbdUnlockLedState(unsigned char PultNb,unsigned char btBlockLed)
{
  unsigned char j,
                btLayer=GetCurrentLayer(PultNb);

  if(btBlockLed==0xff)
  {
    for(j=0;j<NumberKey;j++)
    {
      if((MassKey[PultNb][btLayer][j].CodeKey[0]==ServiceKey) && (MassKey[PultNb][btLayer][j].CodeKey[1]==BlockKey))
      { btBlockLed=j; break; }
    }
  }
  if(IsKbdUnlock(PultNb))
  {
    if(btBlockLed!=0xff) IndicCmd(PultNb,btLayer,btBlockLed,GREEN,1,1);
  }
  else
  {
    if(btBlockLed!=0xff) IndicCmd(PultNb,btLayer,btBlockLed,RED,1,1);
    OutgoingNumberTmpClear(PultNb);
  }
  IndicClock(PultNb);
}

void vRS485TaskR_T(void *pvParameters)
{
  unsigned char PultNb,SN;
  sIndic sKBIndicI;
  KBD_STATE_TP *pKbd;

  memset(MasKbdState,0,sizeof(MasKbdState));
  vTaskDelay(1000);
  PultNb=0;
  while(1)
  {
    if(xQueueReceive(xKeyIndic,&sKBIndicI,0)==pdPASS) Indic_Sound(&sKBIndicI);
    SN=MassM1[PultNb];
    pKbd=MasKbdState+PultNb;
    if(IsKbdReset(PultNb))
    {
      if(pKbd->bWork)
      {
SendStr2IpLogger("$KBD$vRS485TaskR_T: Kbd %d reset!",SN);
        ClearKeySpecial(PultNb);
        pKbd->bWork=false;
        pKbd->XchgErr=0;
        pKbd->dwTimeNextRead=xTaskGetTickCount()+100;
      }
    }
    else
    {
      if(SN)
      {
        if((SN<16) && IsCurrentTickCountGT(pKbd->dwTimeNextRead))
        {
          if(InterviewKeyboard(PultNb,SN))
          {
            if(!pKbd->bWork)
            {
              pKbd->bWork=true;
              OutSNMP(0xff,SN,true);
SendStr2IpLogger("$KBD$vRS485TaskR_T: Kbd %d exchange init!",SN);
              NeedShowCurrentKbdState(PultNb);
            }
            pKbd->XchgErr=0;
            pKbd->dwTimeNextRead=xTaskGetTickCount()+100;
          }
          else
          {
            if(pKbd->bWork)
            {
              if(pKbd->XchgErr<MAX_XCHG_ERR)
              {
                pKbd->XchgErr++;
                pKbd->dwTimeNextRead=xTaskGetTickCount()+100;
SendStr2IpLogger("$KBD$vRS485TaskR_T: Kbd %d exchange error %d!",SN,pKbd->XchgErr);
              }
              else
              {
SendStr2IpLogger("$KBD$vRS485TaskR_T: Kbd %d restart!",SN);
                pKbd->bWork=false;
                OutSNMP(0xff,SN,false);
                ClearKeySpecial(PultNb);
                pKbd->dwTimeNextRead=xTaskGetTickCount()+1500;
              }
            }
            else pKbd->dwTimeNextRead=xTaskGetTickCount()+1500;
          }
        }
      }
    }
    PultNb++;
    if(PultNb>=NumberKeyboard) PultNb=0;
    SetWDState(d_WDSTATE_Rs485);
    vTaskDelay(5);
  }
}

