#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

///////////////////////////////////////////////////////////
typedef struct
{
  unsigned char btMasDg[MAX_DIGIT_TLFN], // ������ ��������� ���� ������
                btNumbLn; // ���-�� ��������� ���� ������
  unsigned int dwTOut;
} OUTGOINGNUMBERTMP_TP;

static OUTGOINGNUMBERTMP_TP s_MassOutgoingNumber[NumberKeyboard];

static unsigned char s_MasAbntInd4Dg[NumberKeyboard];

///////////////////////////////////////////////////////////
bool IsOutgoingNumberTmpPresent(unsigned char PultNb)
{ return(s_MassOutgoingNumber[PultNb].dwTOut!=0); }

void OutgoingNumberTmpClear(unsigned char PultNb)
{
  s_MassOutgoingNumber[PultNb].btNumbLn=0;
  s_MassOutgoingNumber[PultNb].dwTOut=0;
  memset(s_MassOutgoingNumber[PultNb].btMasDg,0xff,MAX_DIGIT_TLFN);
}

bool GetOutgoingNumberTmp(unsigned char PultNb,
                          unsigned char **ppbtMasDg,
                          unsigned char *pbtNumbLn)
{
  if(s_MassOutgoingNumber[PultNb].btNumbLn==0) return(false);
  if(ppbtMasDg) *ppbtMasDg=s_MassOutgoingNumber[PultNb].btMasDg;
  if(pbtNumbLn) *pbtNumbLn=s_MassOutgoingNumber[PultNb].btNumbLn;
  return(true);
}

void SetOutgoingNumberTmp(unsigned char PultNb,unsigned char *pbtMasDg)
{
  char i=0;

  while(pbtMasDg[i]!=0xff)
  {
    s_MassOutgoingNumber[PultNb].btMasDg[i]=pbtMasDg[i];
    i++;
    if(i==MAX_DIGIT_TLFN-2) break;
  }
  s_MassOutgoingNumber[PultNb].btMasDg[i]=0xff;
  s_MassOutgoingNumber[PultNb].btNumbLn=i;
}

void OutgoingNumberTmpTOutClear(unsigned char PultNb)
{ s_MassOutgoingNumber[PultNb].dwTOut=0; }

void InitKeyNumeralVars(void)
{
  unsigned char i;
  
  memset(s_MasAbntInd4Dg,0xff,sizeof(s_MasAbntInd4Dg));
  for(i=0;i<NumberKeyboard;i++) OutgoingNumberTmpClear(i);
}

void OutgoingNumberTmpTOut(unsigned char PultNb)
{
  if(s_MassOutgoingNumber[PultNb].dwTOut)
  {
    if(IsCurrentTickCountGT(s_MassOutgoingNumber[PultNb].dwTOut))
    {
      OutgoingNumberTmpClear(PultNb);
      PutErrInfo2Display(PultNb,"����-��� ������ ������!");
    }
  }
}

void PutOutgoingDigit2Display(unsigned char PultNb,unsigned char btDigit,unsigned char btNumbLn)
{
  if(btDigit==0xff) OutputString(PultNb," ",0,3+6*btNumbLn,1);
  else
  {
    if(btDigit==16) OutputString(PultNb,"P",0,3+6*btNumbLn,1);
    else
    {
      char chBuf[2];

      chBuf[0]=btDigit+'0';
      chBuf[1]=0;
      OutputString(PultNb,chBuf,0,3+6*btNumbLn,1);
    }
  }
}

void PutOutgoingNumber2Display(unsigned char PultNb,unsigned char *pbtMasDg,unsigned char Ln)
{
  unsigned char i;

  SimplePiskPult(PultNb);
  ClearScreen(PultNb);
  OutputString(PultNb,"����� ������",0,3,0);
  for(i=0;i<Ln;i++) PutOutgoingDigit2Display(PultNb,pbtMasDg[i],i);
  s_MassOutgoingNumber[PultNb].btNumbLn=Ln;
  s_MassOutgoingNumber[PultNb].dwTOut=MassTimer[TIMER_OUTGOINGNUMBER]+xTaskGetTickCount();
}

void OutgoingNumberTmpRestore2Display(unsigned char PultNb)
{ // ��� ����������� ����������� ���������� ������ ����� �������������� ���������� �� � ������
  unsigned char i;

  if(s_MassOutgoingNumber[PultNb].dwTOut)
  {
    ClearScreen(PultNb);
    OutputString(PultNb,"����� ������",0,3,0);
    for(i=0;i<s_MassOutgoingNumber[PultNb].btNumbLn;i++)
    { PutOutgoingDigit2Display(PultNb,s_MassOutgoingNumber[PultNb].btMasDg[i],i); }
  }
  else IndicClock(PultNb);
}

void GenerateDigit(unsigned char btDigit,unsigned short NChan)
{
  bool bTone;
  static sCommand sCommandTaskR;

  if(GetTypeCall4ChanelMKA(NChan) & 0x20) bTone=true;
  else bTone=false;
  if(!bTone && (btDigit==0)) btDigit=10; // ��� ���. ������ 0 = 10
  sCommandTaskR.Source = UserRS485;// �������� ������� RS485
  sCommandTaskR.L = 5; // ���������� ���� � �������
  sCommandTaskR.A1 = 0;
  if(bTone) sCommandTaskR.BufCommand[0]=6; // ���.�����
  else sCommandTaskR.BufCommand[0]=5; // ���.�����
  sCommandTaskR.BufCommand[1]=0;
  sCommandTaskR.BufCommand[2]=NChan-1;
  sCommandTaskR.BufCommand[3]=btDigit;
  sCommandTaskR.BufCommand[4]=0xFA; // ������� ����� ������
  if(xQueueSendCmd(&sCommandTaskR,10)==errQUEUE_FULL) CountErrSendQueueCommand++;
}

unsigned char GetAbonentIndex4Digit(unsigned char PultNb)
{ return(s_MasAbntInd4Dg[PultNb]); }

void SetAbonentIndex4Digit(unsigned char PultNb,unsigned char btInd)
{
  s_MasAbntInd4Dg[PultNb]=btInd;
  s_MassOutgoingNumber[PultNb].btMasDg[0]=0xff;
  s_MassOutgoingNumber[PultNb].btNumbLn=0;
}

void ClearAbonentIndex4Digit(unsigned char PultNb,unsigned char btInd)
{
  if(btInd!=0xff)
  {
    if(btInd==s_MasAbntInd4Dg[PultNb]) s_MasAbntInd4Dg[PultNb]=0xff;
  }
  else s_MasAbntInd4Dg[PultNb]=0xff;
}

void OutgoingCallATC4Digit(unsigned char PultNb,unsigned char btDigit,unsigned char btInd)
{
  unsigned char btNumbLn;
  ABONENT_STATE_TP *psAbnt=GetAbonentStatePtrFromIndex(btInd);

  SimplePiskPult(PultNb);
  GenerateDigit(btDigit,psAbnt->wConnectionPoint);
  if(s_MassOutgoingNumber[PultNb].btMasDg[0]==0xff) 
  {
    btNumbLn=s_MassOutgoingNumber[PultNb].btNumbLn;
    psAbnt->btTlfn[btNumbLn]=btDigit;
    psAbnt->NumbAbon=d_OutgoingDigit; // ������� ���.������ ��� ���������� ������ ��� "REDIAL"
    if(!btNumbLn)
    {
      PutOutgoingNumber2Display(PultNb,psAbnt->btTlfn,1);
      s_MassOutgoingNumber[PultNb].dwTOut=0;
    }
    else
    {
      PutOutgoingDigit2Display(PultNb,btDigit,btNumbLn);
      s_MassOutgoingNumber[PultNb].btNumbLn++;
    }
    SendActiveUnnameAbnt_ATC_ISDN(psAbnt);
  }
}

//---- ��������� �������� �������  -----
void Key_Numeral(unsigned char PultNb,unsigned char btDigit)
{
  unsigned char btInd,btNumbLn;

  if(IsConferenceActive(PultNb)) return;
  btInd=GetAbonentIndex4Digit(PultNb);
  if(btInd!=0xff)
  {
    OutgoingCallATC4Digit(PultNb,btDigit,btInd);
    return;
  }
  if(!s_MassOutgoingNumber[PultNb].dwTOut)
  { // ����� ��� �� ���������
    ClearScreen(PultNb);
    OutputString(PultNb,"����� ������",0,3,0);
    SimplePiskPult(PultNb);
    OutgoingNumberTmpClear(PultNb);
    s_MassOutgoingNumber[PultNb].dwTOut=MassTimer[TIMER_OUTGOINGNUMBER]+xTaskGetTickCount();
  }
  btNumbLn=s_MassOutgoingNumber[PultNb].btNumbLn;
  if(btNumbLn>=MAX_DIGIT_TLFN-1) PutErrInfo2Display(PultNb,NULL);
  else
  {
    PutOutgoingDigit2Display(PultNb,btDigit,btNumbLn);
    SimplePiskPult(PultNb);
    s_MassOutgoingNumber[PultNb].btMasDg[btNumbLn]=btDigit;
    s_MassOutgoingNumber[PultNb].btNumbLn++;
    s_MassOutgoingNumber[PultNb].dwTOut=MassTimer[TIMER_OUTGOINGNUMBER]+xTaskGetTickCount();
  }
}

void Key_BackSpace(unsigned char PultNb)
{
  unsigned char btNumbLn=s_MassOutgoingNumber[PultNb].btNumbLn;

  if(!btNumbLn) PutErrInfo2Display(PultNb,NULL);
  else
  {
    s_MassOutgoingNumber[PultNb].btMasDg[btNumbLn]=0xff;
    s_MassOutgoingNumber[PultNb].btNumbLn--;
    PutOutgoingDigit2Display(PultNb,0xff,s_MassOutgoingNumber[PultNb].btNumbLn);
    SimplePiskPult(PultNb);
    s_MassOutgoingNumber[PultNb].dwTOut=MassTimer[TIMER_OUTGOINGNUMBER]+xTaskGetTickCount();
  }
}

