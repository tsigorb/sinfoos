#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

///////////////////////////////////////////////////////////////////////////////
FreeHandConfTP MassFreeHandConf[NumberKeyboard];

///////////////////////////////////////////////////////////////////////////////
bool IsExistFreeHandConf(unsigned char PultNb)
{
  return MassFreeHandConf[PultNb].btExistConf; 
}

void InitFreeHandConf(void)
{
  unsigned char i;

  for(i=0;i<NumberKeyboard;i++)
  {
    MassFreeHandConf[i].btExistConf=0;
    memset(MassFreeHandConf[i].MassAbntInd,0xff,d_MaxAbnt2FreeHandConf);
  }
}

void DelFreeHandConf(unsigned char PultNb)
{
  DelCommuteConference(PultNb,d_FreeHandConferenceNumb);
  memset(MassFreeHandConf[PultNb].MassAbntInd,0xff,d_MaxAbnt2FreeHandConf);
  MassFreeHandConf[PultNb].btExistConf=0;
}

void DelAllFreeHandConf(void)
{
  unsigned char i;

  for(i=0;i<NumberKeyboard;i++)
  {
    if(MassFreeHandConf[i].btExistConf) DelFreeHandConf(i);
  }
}

unsigned char CountEqualCP2FreeHandConference(unsigned char PultNb,unsigned short wCP)
{
  unsigned char i,btInd,btCntCP=0;
  ABONENT_STATE_TP *psAbnt;

  for(i=0;i<d_MaxAbnt2FreeHandConf;i++)
  {
    btInd=MassFreeHandConf[PultNb].MassAbntInd[i];
    if(btInd!=0xff)
    {
      psAbnt=GetAbonentStatePtrFromIndex(btInd);
      if(psAbnt->wConnectionPoint==wCP) btCntCP++;
    }
  }
  return(btCntCP);
}

bool DelAbntFromFreeHandConference(unsigned char PultNb,ABONENT_STATE_TP *psAbnt)
{
  unsigned char i,btInd=GetAbonentStateIndexFromPtr(psAbnt);
  unsigned short wCP;
  bool bRet=false;

  if(MassFreeHandConf[PultNb].btExistConf && (btInd!=0xff))
  {
    if(psAbnt->btType==IPPM) wCP=GetCP_IPPM2MU(psAbnt->NumbAbon);
    else wCP=psAbnt->wConnectionPoint;
    for(i=0;i<d_MaxAbnt2FreeHandConf;i++)
    {
      if(MassFreeHandConf[PultNb].MassAbntInd[i]==btInd)
      {
        if(CountEqualCP2FreeHandConference(PultNb,wCP)==1)
        {
          AbonentConferenceCommuteAddDell(PultNb,d_FreeHandConferenceNumb,wCP,false);
          if(psAbnt->btType==IPPM)
          {
            DelCP_IPPM2MU(psAbnt->NumbAbon);
            if(GetCountMulticastIPPM(psAbnt->wConnectionPoint)==1)
            {
              wCP=GetCPFromVMCP(psAbnt->wConnectionPoint);
              AbonentConferenceCommuteAddDell(PultNb,d_FreeHandConferenceNumb,wCP,false);
              StopAudioFromMU2IPPM(wCP);
              DelMCP(wCP);
            }
          }
          bRet=true;
        }
        MassFreeHandConf[PultNb].MassAbntInd[i]=0xff;
        break;
      }
    }
    for(i=0;i<d_MaxAbnt2FreeHandConf;i++)
    {
      if(MassFreeHandConf[PultNb].MassAbntInd[i]!=0xff)
      {
        SendAllAbonentConference2IpPult(PultNb,d_FreeHandConferenceNumb);
        return(bRet);
      }
    }
    DelFreeHandConf(PultNb);
  }
  return(bRet);
}

bool AddAbnt2FreeHandConf(unsigned char PultNb,ABONENT_STATE_TP *psAbnt)
{
  unsigned char i,btInd=GetAbonentStateIndexFromPtr(psAbnt);
  unsigned short wCP;
  bool bOk=false;

  if(btInd==0xff) return(false);
  if(psAbnt->btType==IPPM) wCP=GetCP_IPPM2MU(psAbnt->NumbAbon);
  else wCP=psAbnt->wConnectionPoint;
  if(MassFreeHandConf[PultNb].btExistConf)
  {
    for(i=0;i<d_MaxAbnt2FreeHandConf;i++)
    {
      if(MassFreeHandConf[PultNb].MassAbntInd[i]==0xff)
      {
        if(CountEqualCP2FreeHandConference(PultNb,wCP)==0)
        {
          if(!AbonentConferenceCommuteAddDell(PultNb,d_FreeHandConferenceNumb,
                                              wCP,true)) return(false);
        }
        MassFreeHandConf[PultNb].MassAbntInd[i]=btInd;
        SendAllAbonentConference2IpPult(PultNb,d_FreeHandConferenceNumb);
        bOk=true;
      }
    }
  }
  else
  {
    if(CreateCommuteConference(PultNb,d_FreeHandConferenceNumb,psAbnt->wOperatorChanel,
                               d_MaxAbnt2FreeHandConf,MassFreeHandConf[PultNb].MassAbntInd))
    {
      MassFreeHandConf[PultNb].MassAbntInd[0]=btInd;
      MassFreeHandConf[PultNb].btExistConf=1;
      if(AbonentConferenceCommuteAddDell(PultNb,d_FreeHandConferenceNumb,
                                         wCP,true))
      {
        SendAllAbonentConference2IpPult(PultNb,d_FreeHandConferenceNumb);
        bOk=true;
      }
    }
  }
  if(bOk && (psAbnt->btType==IPPM))
  { // �������� � �����������, ���� ����, MCP
    wCP=GetCPFromVMCP(psAbnt->wConnectionPoint);
    if(!wCP)
    {
      unsigned char SN=MassM1[PultNb];
      wCP=CreatMCP(SN);
      if(wCP)
      {
        AbonentConferenceCommuteAddDell(PultNb,d_FreeHandConferenceNumb,wCP,true);
        SendIPPMCmdConnect(psAbnt->NumbAbon,psAbnt);
        StartAudioFromMU2IPPM_Multicast(GetMIPAddr(SN),wCP);
      }
    }
  }
  return(bOk);
}

