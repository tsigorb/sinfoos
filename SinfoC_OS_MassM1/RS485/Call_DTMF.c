#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

bool _xOutgoingCallDTMF(unsigned short NChan,unsigned char *btBufNumb,unsigned char btLn)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.BufCommand[0]=1;
  sCmd.BufCommand[1]=NChan >> 8;
  sCmd.BufCommand[2]=NChan;
  sCmd.BufCommand[3]=CallDTMF;
  sCmd.BufCommand[4]=btLn;
  memcpy(sCmd.BufCommand+5,btBufNumb,btLn);
  sCmd.L=btLn+4;
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else 
  {
    if(WaitReply(1,0)) return(true);
  }
  return(false);
}

void IncomingCall_DTMF(unsigned short wNChan,unsigned short btCode)
{
}

