#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

bool _xCallSPEC(unsigned short wNChan,unsigned char TypeAbon,unsigned char NumbAbnt)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.BufCommand[0]=1; // �������
  sCmd.BufCommand[1]=wNChan >> 8;
  sCmd.BufCommand[2]=wNChan;
  sCmd.BufCommand[3]=CallSpec1+TypeAbon-Spec1;
  sCmd.BufCommand[4]=NumbAbnt;
  sCmd.L=5;
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(WaitReply(1,0)) return(true);
  }
  return(false);
}

void CallOutSPECTOut(unsigned char PultNb)
{
  unsigned char btInd,btState;
  ABONENT_STATE_TP *psAbnt;

  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return;
    if((psAbnt->wConnectionPoint!=0xffff) && (psAbnt->btPultNb==PultNb) &&
       ((psAbnt->btType>=Spec1) && (psAbnt->btType<=Spec8)))
    {
      if(psAbnt->dwTimerEnd && IsCurrentTickCountGT(psAbnt->dwTimerEnd))
      { // ������� � ����� ���������� ��������
        IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKey,OFF,0,0);
        ClearAbonentStateByPtr(psAbnt);
      }
    }
    btInd++;
  }
  while(1);
}

void PressCallKey_OutSPEC(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  ABONENT_STATE_TP *psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);

  if(!psAbnt)
  {
    if(IsCreatedTransit(PultNb)) XorAbnt2Transit(PultNb,Key);
    else
    {
      bool bRes;
      unsigned short wCP;
      unsigned char NumbAbon;

      psAbnt=GetFreeAbonentStatePtr();
      if(psAbnt)
      {
        psAbnt->btPultNb=PultNb;
        psAbnt->btLayer=btLayer;
        psAbnt->btKey=Key;
        wCP=MassKey[PultNb][btLayer][Key].NChan;
        psAbnt->btType=MassKey[PultNb][btLayer][Key].TypeAbon;
        NumbAbon=MassKey[PultNb][btLayer][Key].NumbAbon;
        if(NumbAbon)
        {
          bRes=_xCallSPEC(wCP,psAbnt->btType,NumbAbon);
          if(bRes)
          {
            psAbnt->dwTimerEnd=d_TmTmpOutAbnt+xTaskGetTickCount();
            psAbnt->wConnectionPoint=wCP;
            psAbnt->wOperatorChanel=0;
            psAbnt->btLedState=dLedStateOutgoingCall;
            psAbnt->NumbAbon=NumbAbon;
            SetAbonentStateByPtr(psAbnt,Call,0);
          }
          if(bRes) IndicCmd(PultNb,btLayer,Key,psAbnt->btLedState,1,1);
          else PutErrInfo2Display(PultNb,"������ ����������!");
        }
      }
    }
  }
}

