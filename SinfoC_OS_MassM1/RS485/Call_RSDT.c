#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

bool _xCallRSDT(unsigned short wNChan,unsigned char NumbAbnt)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.BufCommand[0]=1; // �������
  sCmd.BufCommand[1]=wNChan >> 8;
  sCmd.BufCommand[2]=wNChan;
  sCmd.BufCommand[3]=CallPCDT;
  sCmd.BufCommand[4]=NumbAbnt;
  sCmd.L=5;
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(WaitReply(1,0)) return(true);
  }
  return(false);
}

void OutgoingCallRSDT(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  bool bRes;
  unsigned short wCP;
  unsigned char NumbAbon=MassKey[PultNb][btLayer][Key].NumbAbon;
  ABONENT_STATE_TP *psAbnt;

  psAbnt=GetFreeAbonentStatePtr();
  if(psAbnt)
  {
    psAbnt->btPultNb=PultNb;
    psAbnt->btLayer=btLayer;
    psAbnt->btKey=Key;
    wCP=MassKey[PultNb][btLayer][Key].NChan;
    psAbnt->btType=RSDT;
    if(NumbAbon)
    {
      bRes=_xCallRSDT(wCP,NumbAbon);
      if(bRes)
      {
        psAbnt->dwTimerEnd=d_TmTmpOutAbnt+xTaskGetTickCount();
        psAbnt->wConnectionPoint=wCP;
        psAbnt->wOperatorChanel=0;
        psAbnt->btLedState=dLedStateOutgoingCall;
        psAbnt->NumbAbon=NumbAbon;
        SetAbonentStateByPtr(psAbnt,Call,0);
      }
    }
    if(bRes) IndicCmd(PultNb,btLayer,Key,psAbnt->btLedState,1,1);
    else PutErrInfo2Display(PultNb,"������ ����!");
  }
}

void PressCallKey_RSDT(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  ABONENT_STATE_TP *psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);

  if(!psAbnt)
  {
    if(IsCreatedTransit(PultNb)) XorAbnt2Transit(PultNb,Key);
    else OutgoingCallRSDT(PultNb,Key,btLayer);
  }
  else
  {
    switch(GetAbonentStateFromPtr(psAbnt))
    {
      case IncomingCall:
      {
        AnswerIncomingCallMKA(psAbnt);
        psAbnt->dwTimerEnd=MassTimer[TIMER_SPEAKLEVEL]+xTaskGetTickCount();
        psAbnt->dwTimerAnaliz=100+xTaskGetTickCount();
        break;
      }
    }
  }
}

