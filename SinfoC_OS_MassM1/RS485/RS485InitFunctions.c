#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

//*******************************************************************
void *pvPortMalloc(size_t xWantedSize);
void vPortFree(void *pv);

void TestLic(char *strSignature);

void InitKeyVars(void);
void ReadDialplan(void);

//*******************************************************************
extern unsigned char MassLayer[NumberKeyboard];

//*******************************************************************
#define MAX_GROUP_COUNT     63

typedef struct
{
  unsigned short NChan;  // 1..48 - ����� ������ ���. >0x100 - ����� �����������, �� ���������, ���� �� �������...
  unsigned short wGroup; // ������� ���� �� ������������ � ������ ���� ������������� �������!!!
  unsigned short wMassEnableChannel[3]; // ��������� ������ ����������� ���������� ������� ��� ���������������
  unsigned short Reserve;
} sMKAGroupTP_File;

typedef struct
{
  unsigned short NChans[MAX_MKA_CHANNELS];  // 1..48 + ISDN - ������ ������� ��� ���� ������
  unsigned short wGroup; // ������� ���� �� ������������ � ������ ���� ������������� �������!!!
  unsigned short wAA;    // ������������ ��� ������������� ���������� �����������
                         // (��� N==1 - ��� ������ N �������� �� ��� ���� ������)
} sMKAGroupTP;

static sMKAGroupTP MassGroup[MAX_GROUP_COUNT+1]; // ������ ����� ��� ���������� �������, ISDN  � "����� ������"
static sPultInfoTp MassPultInfo[NumberKeyboard];

static volatile bool s_bPultCfgRead=false;

static volatile bool s_bNeedInitPults=false;

//*******************************************************************
bool IsPultConfigRead(void)
{ 
  return(s_bPultCfgRead);
}

/////////////////////////////////////////////////////////////////////
void InitMassTimer(void)
{
  unsigned char i;
  HANDLE hFile;
  static unsigned int MassTimer0[NumberTimer]=
{ 0x1e,0x1e,0xb4,0x0708,0x0d,0x0258,0x0f,0x0a,0x03,0x0708,0x14,0x0a,0x05 };
 
  memcpy(MassTimer,MassTimer0,sizeof(MassTimer));
  hFile=CreateFile("MTimer1.dts",GENERIC_READ,0);
  if(hFile>=0)
  {
    ReadFile(hFile,MassTimer,sizeof(MassTimer));
    CloseHandle(hFile);
  }
  for(i=0;i<(sizeof(MassTimer)/sizeof(int));i++) MassTimer[i]*=1000;
}

void InitMassTel(void)
{
  HANDLE hFile;

  memset(MassTel,0xff,sizeof(MassTel));
  hFile=CreateFile("MTel1.dts", GENERIC_READ, 0); // �������� �����
  if(hFile>=0) 
  {
    ReadFile(hFile,MassTel,sizeof(MassTel));
    CloseHandle(hFile);
  }
}

unsigned char GetTlfnGroup(unsigned char NumbAbon)
{
  unsigned char btGroup;

  if(!NumbAbon || (NumbAbon>NumberDirectAbon) || (NumbAbon==d_OutgoingDigit)) return(0);
  btGroup=MassTel[NumbAbon-1][17];
  if(btGroup==0xff) btGroup=0;
  return(btGroup);
}

// ����� ������, ���������� �����
unsigned char FindPultActive(unsigned short wChanOper)
{
  unsigned char i;
  bool bCP=false;

  if(wChanOper>0x100)
  {
    wChanOper=wChanOper & 0xff;
    bCP=true;
  }
  wChanOper<<=8;
  for(i=0;i<NumberKeyboard;i++)
  {
    if(MassM1[i])
    {
      if(bCP)
      {
        if((MassPultInfo[i].ChanDS & 0xff) &&
           ((MassPultInfo[i].ChanDS & 0xff00)==wChanOper)) return(i);
        if((MassPultInfo[i].ChanRS & 0xff) &&
           ((MassPultInfo[i].ChanRS & 0xff00)==wChanOper)) return(i);
      }
      else
      {
        if(!(MassPultInfo[i].ChanDS & 0xff) &&
           ((MassPultInfo[i].ChanDS & 0xff00)==wChanOper)) return(i);
        if(!(MassPultInfo[i].ChanRS & 0xff) &&
           ((MassPultInfo[i].ChanRS & 0xff00)==wChanOper)) return(i);
      }
    }
  }
  return(0xff);
}

unsigned char GetSNFromCP(unsigned short wChan)
{
  unsigned char PultNb;

  wChan<<=8;
  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if((wChan==(MassPultInfo[PultNb].ChanDS & 0xff00)) ||
       (wChan==(MassPultInfo[PultNb].ChanRS & 0xff00))) return(MassPultInfo[PultNb].SN);
  }
  return(0);
}

unsigned short GetOperatorChannel(unsigned char PultNb,unsigned char btDS_RS)
{
  unsigned short wOper;

  if(PultNb>=NumberKeyboard) return(0);
  if(btDS_RS) wOper=MassPultInfo[PultNb].ChanRS;
  else wOper=MassPultInfo[PultNb].ChanDS;
  if(wOper & 0xff) wOper=(wOper >> 8) | 0x100;
  else wOper>>=8;
  return(wOper);
}

unsigned short GetOperatorChannelFromCode(unsigned short ChanOper)
{
  unsigned char i;

  if(ChanOper<=48) return(ChanOper);
  for(i=0;i<NumberKeyboard;i++)
  {
    if(MassM1[i])
    {
      if((MassPultInfo[i].ChanDS & 0xff)==ChanOper) return(GetOperatorChannel(i,0));
      if((MassPultInfo[i].ChanRS & 0xff)==ChanOper) return(GetOperatorChannel(i,1));
    }
  }
  return(0);
}

unsigned short GetOperatorChannelFromKey(unsigned char PultNb,unsigned char btKey,unsigned char btLayer)
{
  return(GetOperatorChannelFromCode(MassKey[PultNb][btLayer][btKey].ChanOper));
}

/////////////////////////////////////////////////////////////////////
unsigned char *GetPultTlfnPtr(unsigned char PultNb)
{ return(MassPultInfo[PultNb].Tel); }

char *GetPultNamePtr(unsigned char PultNb)
{ return(MassPultInfo[PultNb].Name); }

bool IsPultAutoAnswer(unsigned char PultNb)
{
  if(PultNb>=NumberKeyboard) return(false);
  return((MassPultInfo[PultNb].Flags & d_PultAutoAnswer)!=0);
}

void InitDisplayVars(void)
{
  unsigned char i;

  for(i=0;i<NumberKeyboard;i++)
  {
    ListAbntDisplay[i][0].dwTOut=0;
    ListAbntDisplay[i][0].NChan=ListAbntDisplay[i][1].NChan=0xff;
    ListAbntDisplay[i][0].StrInf[0]=ListAbntDisplay[i][1].StrInf[0]=0;
  }
}

void DeleteCP4IpPults(void)
{
  unsigned char PultNb;
  unsigned short wCP;

  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if(MassPultInfo[PultNb].SN>16)
    {
      wCP=GetOperatorChannel(PultNb,0);
      if(wCP & 0x100) DelConnectionPoint(wCP);
      wCP=GetOperatorChannel(PultNb,1);
      if(wCP & 0x100) DelConnectionPoint(wCP);
    }
  }
}

void CreateCP4IpPults(void)
{
  unsigned char PultNb;

  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if(MassPultInfo[PultNb].SN>16)
    {
      if(MassPultInfo[PultNb].ChanDS>48) MassPultInfo[PultNb].ChanDS|=(unsigned short)GetFreeConnectionPoint() << 8;
      else MassPultInfo[PultNb].ChanDS<<=8;
      if(MassPultInfo[PultNb].ChanRS>48) MassPultInfo[PultNb].ChanRS|=(unsigned short)GetFreeConnectionPoint() << 8;
      else MassPultInfo[PultNb].ChanRS<<=8;
/*
SendStr2IpLogger("$DEBUG$CreateCP4IpPults: SN=%d,PultNb=%d,DS=%d,RS=%d",
          MassPultInfo[PultNb].SN,PultNb,GetOperatorChannel(PultNb,0),GetOperatorChannel(PultNb,1));
*/
    }
  }
}

void PultsInfoRead(void)
{
  unsigned char PultNb;
  HANDLE hFile;

  memset(MassM1,0,sizeof(MassM1));
  memset(MassPultInfo,0,sizeof(MassPultInfo));
  hFile=CreateFile("PultsInfo.dts",GENERIC_READ,0);
  if(hFile<0) return;
  if(ReadFile(hFile,MassPultInfo,sizeof(MassPultInfo)))
  {
    for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
    {
      if(MassPultInfo[PultNb].SN)
      {
        MassM1[PultNb]=MassPultInfo[PultNb].SN;
        if(MassPultInfo[PultNb].SN<16)
        {
          MassPultInfo[PultNb].ChanDS<<=8;
          MassPultInfo[PultNb].ChanRS<<=8;
        }
/*DataSave1[PultNb*2]=MassPultInfo[PultNb].SN;
DataSave1[PultNb*2+1]=MassPultInfo[PultNb].Flags;*/
      }
    }
  }
  CloseHandle(hFile);
}

/////////////////////////////////////////////////////////////////////
static bool ExistChan2MasGroup(unsigned short NChan)
{
  unsigned char i;

  for(i=0;i<MAX_GROUP_COUNT;i++)
  {
    if(MassGroup[i].wGroup)
    {
      if(MassGroup[i].NChans[NChan-1]==1) return(true);
    }
  }
  return(false);
}

unsigned char FindGroup2MasGroup(unsigned short wGroup)
{
  unsigned char i;

  for(i=0;i<MAX_GROUP_COUNT+1;i++)
  {
    if(MassGroup[i].wGroup==wGroup) return(i);
  }
  return(0xff);
}

void InitGroup(void)
{
  unsigned char PultNb,btNumbInd,btInd,btIndFind,NChan;
  unsigned short wAA,wGroup;
  HANDLE hFile;
  sMKAGroupTP_File sMKAGroupF;

  btInd=0;
  memset(MassGroup,0,sizeof(MassGroup));
  hFile=CreateFile("MGroup1.dts",GENERIC_READ,0);
  if(hFile>=0) 
  {
    while(ReadFile(hFile,&sMKAGroupF,sizeof(sMKAGroupTP_File))==sizeof(sMKAGroupTP_File))
    {
      if(sMKAGroupF.NChan && (sMKAGroupF.NChan<=MAX_MKA_CHANNELS))
      {
        sMKAGroupF.wGroup&=0xff;
        NChan=(unsigned char)sMKAGroupF.NChan;
        if(sMKAGroupF.wGroup && (sMKAGroupF.wGroup<d_GroupISDN) && !ExistChan2MasGroup(NChan))
        {
          btIndFind=FindGroup2MasGroup(sMKAGroupF.wGroup);
          if(btIndFind==0xff) btIndFind=btInd;
          MassGroup[btIndFind].NChans[NChan-1]=1; // ����� NChan ����������� ���� ������
          if(btIndFind==btInd)
          {
            MassGroup[btInd].wGroup=sMKAGroupF.wGroup;
            btInd++;
            if(btInd==MAX_GROUP_COUNT) break;
          }
        }
      }
    }
    CloseHandle(hFile);
  }
  for(btNumbInd=0;(btNumbInd<NumberDirectAbon) && (btInd<MAX_GROUP_COUNT);btNumbInd++)
  { // ��������� ������ ����� �� ������� ���������
    wGroup=GetTlfnGroup(btNumbInd+1);
    if(wGroup && (FindGroup2MasGroup(wGroup)==0xff))
    { // ���� ������� � ������ ��� � IPPM � ��������� ��������!
      MassGroup[btInd].NChans[0]=d_GroupISDN;
      MassGroup[btInd].wGroup=wGroup;
      btInd++;
    }
  }
  MassGroup[MAX_GROUP_COUNT].NChans[0]=d_GroupCallPult;
  MassGroup[MAX_GROUP_COUNT].wGroup=d_GroupCallPult;
  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  { // ������������� �� ��������� ��������� ��� ���� ����� ��� ������� � �����������
    if(MassM1[PultNb] && IsPultAutoAnswer(PultNb))
    {
      wAA=(unsigned short)1 << PultNb;
      for(btInd=0;btInd<MAX_GROUP_COUNT+1;btInd++)
      {
        if(MassGroup[btInd].wGroup) MassGroup[btInd].wAA|=wAA;
      }
    }
  }
}

static unsigned char GetGroupIndFromChan(unsigned short NChan)
{
  unsigned char btInd;

  if(!NChan || (NChan>MAX_MKA_CHANNELS)) return(0xff);
  for(btInd=0;btInd<MAX_GROUP_COUNT+1;btInd++)
  {
    if(MassGroup[btInd].NChans[NChan-1]) return(btInd);
  }
  return(0xff);
}

static unsigned char GetGroupIndFromGroup(unsigned short wGroup)
{
  unsigned char btInd;

  if(wGroup)
  {
    for(btInd=0;btInd<MAX_GROUP_COUNT+1;btInd++)
    {
      if(MassGroup[btInd].wGroup==wGroup) return(btInd);
    }
  }
  return(0xff);
}

unsigned char GetGroupFromChan(unsigned short NChan)
{
  unsigned char btInd=GetGroupIndFromChan(NChan);

  if(btInd==0xff) return(0);
  return((unsigned char)(MassGroup[btInd].wGroup));
}

bool IsGroupAutoAnswer4PultFromChan(unsigned char PultNb,unsigned short NChan)
{
  unsigned short wAA=(unsigned short)1 << PultNb;
  unsigned char btInd=GetGroupIndFromChan(NChan);

  if(btInd==0xff) return(false);
  return((MassGroup[btInd].wAA & wAA)!=0);
}

bool IsGroupAutoAnswer4PultFromGroup(unsigned char PultNb,unsigned short wGroup)
{
  unsigned short wAA=(unsigned short)1 << PultNb;
  unsigned char btInd=GetGroupIndFromGroup(wGroup);

  if(btInd==0xff) return(false);
  return((MassGroup[btInd].wAA & wAA)!=0);
}

void GetAutoAnswerMarks(unsigned char SN,sInfoKadr *psInfo)
{
  unsigned char i,btCnt,btOff,
                PultNb=GetPultIndFromSN(SN);
  unsigned short wAA;

  psInfo->ByteInfoKadr[0]=120;
  psInfo->ByteInfoKadr[1]=SN;
  memcpy(psInfo->ByteInfoKadr+2,"SET_MARKS",9);
  btCnt=0; btOff=12;
  if((PultNb!=0xff) && IsPultAutoAnswer(PultNb))
  {
    wAA=(unsigned short)1 << PultNb;
    for(i=0;i<MAX_GROUP_COUNT+1;i++)
    {
      if(MassGroup[i].wGroup)
      {
        psInfo->ByteInfoKadr[btOff++]=(unsigned char)MassGroup[i].wGroup;
        psInfo->ByteInfoKadr[btOff++]=((MassGroup[i].wAA & wAA) ? 0 : 1);
        btCnt++;
      }
    }
  }
  psInfo->ByteInfoKadr[11]=btCnt;
  psInfo->L=btOff;
  xQueueSend(xQueueReply[UserUDP],psInfo,10,0);
}

bool GroupAutoAnswer4Pult(unsigned char PultNb,unsigned short wGroup,bool bAA)
{
  unsigned short wAA=(unsigned short)1 << PultNb;
  unsigned char btInd=GetGroupIndFromGroup(wGroup);

  if((PultNb>=NumberKeyboard) || (btInd==0xff) || !IsPultAutoAnswer(PultNb)) return(true); // 1 - ������ �� ��� ������
  if(bAA) MassGroup[btInd].wAA|=wAA;
  else
  {
    wAA=~wAA;
    MassGroup[btInd].wAA&=wAA;
  }
  return(!bAA);
}

void SetAutoAnswerMark(unsigned char SN,unsigned char btGroup,unsigned char btDisableAA,sInfoKadr *psInfo)
{
  psInfo->ByteInfoKadr[0]=122;
  psInfo->ByteInfoKadr[1]=SN;
  memcpy(psInfo->ByteInfoKadr+2,"SET_MARK",8);
  psInfo->ByteInfoKadr[10]=btGroup;
  psInfo->ByteInfoKadr[11]=GroupAutoAnswer4Pult(GetPultIndFromSN(SN),btGroup,!btDisableAA);
  psInfo->L=12;
  xQueueSend(xQueueReply[UserUDP],psInfo,10,0);
}

// �������� ���������� � ������
void LoadMasKey4Pult(unsigned char PultNb)
{
  unsigned char j,btSz,btLayer,btPage,btOff,
                btMasLayerSz[NumberKeyPage]={NumberKeyPageSize,NumberKeyPageSize-1};
  unsigned short wSz;
  HANDLE hFile;
  static char FName[20];
  static sKBKey sKey;

  NumbTabl=RdFlash(AdrParamUKC+6);
  if(NumbTabl>3) 
  {
    NumbTabl=0;
    WrFlash(NumbTabl,AdrParamUKC+6);
  }
  for(j=0;j<NumberKeyboard;j++) 
  {
/*    MassLayer[j]=RdFlash(AdrMLayer+j*2);
    if(MassLayer[j]>2)
    {*/
      MassLayer[j]=0;
/*      WrFlash(MassLayer[j],AdrMLayer+j*2);
    }*/
    for(btLayer=0;btLayer<NumberGroup;btLayer++) SetLayerUnlockState(j,btLayer,GetLayerUnlockState(j,btLayer));
  }
  LockPrintf();
  sprintf(FName,"Pult1%02d.dts",PultNb);
  UnlockPrintf();
  hFile=CreateFile(FName,GENERIC_READ,0);
  if(hFile>=0)
  {
    for(btPage=0;btPage<NumberKeyPage;btPage++)
    { // ������ �������� �������� 3 ���� �� 129(page0) ��� 128(page1) ������ � ������
      btOff=btPage*btMasLayerSz[btPage];
      for(btLayer=0;btLayer<NumberGroup;btLayer++)
      {
        for(j=0;j<btMasLayerSz[btPage];j++)
        {
          if(btPage && (MassM1[PultNb]<16)) break;
          else btSz=ReadFile(hFile,&sKey,sizeof(sKBKey));
          if(btSz!=sizeof(sKBKey))
          {
            CloseHandle(hFile);
            if(btLayer || j || btSz)
            {
              for(btLayer=0;btLayer<NumberGroup;btLayer++)
              {
                for(j=0;j<NumberKey;j++) memset(&MassKey[PultNb][btLayer][j],0,sizeof(sKBKey));
              }
              ErrReadF++;
            }
            return;
          }
          if((unsigned short)j+btOff<NumberKey)
          { // �������� �� 255 ������ � ����!
            MassKey[PultNb][btLayer][j+btOff]=sKey;
          }
        }
      }
    }
    CloseHandle(hFile);
  }
}

void SetNeedInitPults(void)
{ s_bNeedInitPults=true; }

bool IsNeedInitPults(void)
{ return(s_bNeedInitPults); }

void InitPults(bool bWait)
{
  unsigned char i;

  LedOn(ledPult);
  if(bWait)
  { // ����� ������ ������ ������������� �������
    InitTransitVars();
  }
  s_bNeedInitPults=false;
  s_bPultCfgRead=false;
  CrossCommuteOtboiAll();
  for(i=0;i<NumberKeyboard;i++) SetKbdReset(i,true);
  InitKeyboard(0xff);
  DelAllFreeHandConf();
  DeleteAllConnections2Sinfo();
  DeleteAllTransit();
  DeleteCP4IpPults();
  ReadDialplan();
  InitKeyNumeralVars();
  InitConferenceVars();
  ResetMasVirtualLed();
  ResetIPPults();
  InitKeyVars();
  InitDisplayVars();
  InitMassTimer(); // ������������� ��������
  InitMassTel(); // ������������� ������� ���������
  PultsInfoRead();
  InitGroup(); // ����������� ������ ���������� ����� PultsInfoRead() � InitMassTel() !!
  memset(MassKey,0,sizeof(MassKey));
  vTaskDelay(8000);
  for(i=0;i<NumberKeyboard;i++) 
  {
    if(MassM1[i])
    {
      LoadMasKey4Pult(i);
      InvalidateAbonentState4Pult(i);
      SetKbdReset(i,false);
    }
  }
  TestLic("POS.lic");
  TestLic("REC.lic");
  TestLic("IPPM.lic");
  ReinitConnectionPoint(); // ������ ���������� ������ ��� ����������� ������������ �����!!!
  CreateCP4IpPults();
  RestoreAllTransit();
  InitIPPM();
  LedOff(ledPult);
  s_bPultCfgRead=true;
}

