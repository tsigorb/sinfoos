#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

/////////////////////////////////////////////////////////////////////
extern FreeHandConfTP MassFreeHandConf[NumberKeyboard];

/////////////////////////////////////////////////////////////////////
#define d_AddAbnt2Conference        1
#define d_ConferenceWork            2
#define d_ConferenceIndic           3

typedef struct
{
  unsigned char btState,btKey,btMaxAbnt,btChange,btTalk;
  unsigned short wOper;
  unsigned char btMassAbntInd[MAX_CONF_ABONENT];
} sConferenceTP;

static unsigned char s_MassConferenceActive[NumberKeyboard];

static sConferenceTP s_MassConf[NumberKeyboard][NumberKonf];

/////////////////////////////////////////////////////////////////////
void SendAllAbonentConference2IpPult(unsigned char PultNb,unsigned char ConfNb)
{
  unsigned char i,SN,btCnt,btOff,btMax;
  unsigned char *pbtAbntInd;
  ABONENT_STATE_TP *psAbnt;
  static sInfoKadr sInfo;

  SN=MassM1[PultNb];
  if(SN<16) return;
  if(GetAddressIpPult(SN,&sInfo.A1,&sInfo.PORT1))
  {
    sInfo.Source=UserUDP;
    sInfo.ByteInfoKadr[0]=104;
    sInfo.ByteInfoKadr[1]=SN;
    if(ConfNb==d_FreeHandConferenceNumb)
    {
      btMax=d_MaxAbnt2FreeHandConf;
      pbtAbntInd=MassFreeHandConf[PultNb].MassAbntInd;
    }
    else
    {
      btMax=s_MassConf[PultNb][ConfNb].btMaxAbnt;
      pbtAbntInd=s_MassConf[PultNb][ConfNb].btMassAbntInd;
    }
    for(i=0,btCnt=0,btOff=3;i<btMax;i++)
    {
      psAbnt=GetAbonentStatePtrFromIndex(pbtAbntInd[i]);
      if(psAbnt)
      {
        sInfo.ByteInfoKadr[btOff++]=psAbnt->btKey;
        sInfo.ByteInfoKadr[btOff++]=ConfNb | d_AbntInConference;
        btCnt++;
      }
    }
    sInfo.ByteInfoKadr[2]=btCnt;
    sInfo.L=btOff;
    xQueueSend(xQueueReply[UserUDP],&sInfo,0,0);
  }
}

void InitConferenceVars(void)
{
  memset(s_MassConferenceActive,0xff,sizeof(s_MassConferenceActive));
  memset(s_MassConf,0xff,sizeof(s_MassConf));
}

unsigned char GetConferenceState(unsigned char PultNb,unsigned char ConfNb)
{
  if(s_MassConf[PultNb][ConfNb].btState!=0xff)
  {
    if(s_MassConf[PultNb][ConfNb].btState==d_AddAbnt2Conference) return(d_CreateConference);
    if(s_MassConf[PultNb][ConfNb].btState>d_AddAbnt2Conference)
    {
      if(s_MassConf[PultNb][ConfNb].btTalk==1) return(d_ConferenceWithOperator);
      else return(d_ConferenceWithoutOperator);
    }
  }
  return(d_NoConference);
}

bool IsConferenceActive(unsigned char PultNb)
{
  unsigned char i;

  for(i=0;i<NumberKonf;i++)
  {
    if(s_MassConferenceActive[PultNb]!=0xff) return(true);
  }
  return(false);
}

bool IsConferenceWork(unsigned char PultNb,unsigned char ConfNb)
{
  if(ConfNb>=NumberKonf) return(false);
  if(s_MassConf[PultNb][ConfNb].btState!=0xff) return(true);
  return(false);
}

bool IsKey2Conference(unsigned char PultNb,unsigned char btKey,unsigned char btLayer,unsigned char *pbtConf)
{
  unsigned char btInd,Conf,i;
  
  btInd=GetAbonentStateIndFromKey(PultNb,btKey,btLayer);
  for(Conf=0;Conf<NumberKonf;Conf++)
  {
    if(s_MassConf[PultNb][Conf].btState!=0xff)
    {
      for(i=0;i<s_MassConf[PultNb][Conf].btMaxAbnt;i++)
      {
        if(s_MassConf[PultNb][Conf].btMassAbntInd[i]==btInd)
        {
          if(pbtConf) *pbtConf=Conf;
          return(true);
        }
      }
    }
  }
  return(false);
}

bool IsAbonent2Conference(unsigned char PultNb,unsigned char btKey,unsigned char btLayer)
{
  unsigned char Conf,Abnt,btInd;

  btInd=GetAbonentStateIndFromKey(PultNb,btKey,btLayer);
  if(btInd!=0xff)
  {
    for(Conf=0;Conf<NumberKonf;Conf++)
    {
      for(Abnt=0;Abnt<MAX_CONF_ABONENT;Abnt++)
      {
        if(s_MassConf[PultNb][Conf].btMassAbntInd[Abnt]==btInd) return(true);
      }
    }
  }
  return(false);
}

bool EnableConference(unsigned char PultNb,unsigned char ConfNb)
{
  if(IsSelectorActive(PultNb)) return(false);
  if(ConfNb>=NumberKonf) return(false);
  if(IsPultAutoAnswer(PultNb)) return(false);
  if((s_MassConferenceActive[PultNb]!=0xff) && (s_MassConferenceActive[PultNb]!=ConfNb)) return(false);
  return(true);
}

bool DelCommuteConference(unsigned char PultNb,unsigned char ConfNb)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485;// �������� ������� RS485
  sCmd.L=4; // ���������� ���� � �������
  sCmd.A1=0;
  sCmd.BufCommand[0]=10; // �������
  sCmd.BufCommand[1]=144; // ����������
  sCmd.BufCommand[2]=MassM1[PultNb]; // ����� ����������
  sCmd.BufCommand[3]=ConfNb; // ����� �����������
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(WaitReply(11,144)) return(true);
  }
  return(false);
}

void DelConferenceFromPult(unsigned char PultNb,unsigned char ConfNb)
{
  unsigned char i,btInd,btLayer;
  ABONENT_STATE_TP *psAbnt;

  DelCommuteConference(PultNb,ConfNb);
  btLayer=GetCurrentLayer(PultNb);
  for(i=0;i<MAX_CONF_ABONENT;i++)
  {
    btInd=s_MassConf[PultNb][ConfNb].btMassAbntInd[i];
    if(btInd!=0xff)
    {
      psAbnt=GetAbonentStatePtrFromIndex(btInd);
      if(psAbnt) OtboiAbonent(PultNb,psAbnt->btKey,btLayer);
    }
  }
  s_MassConf[PultNb][ConfNb].btState=0xff;
  IndicCmd(PultNb,btLayer,s_MassConf[PultNb][ConfNb].btKey,Off,1,1);
  DSRS_Off(PultNb,s_MassConf[PultNb][ConfNb].wOper);
  memset(&s_MassConf[PultNb][ConfNb],0xff,sizeof(sConferenceTP));
}

bool AbonentConferenceCommuteAddDell(unsigned char PultNb,unsigned char ConfNb,
                                     unsigned short wChan,bool bAdd)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485;// �������� ������� RS485
  sCmd.L=7; // ���������� ���� � �������
  sCmd.A1=0;
  sCmd.BufCommand[0]=10; // �������
  sCmd.BufCommand[1]=145; // ����������
  sCmd.BufCommand[2]=MassM1[PultNb]; // ����� ����������
  sCmd.BufCommand[3]=ConfNb; // ����� �����������
  sCmd.BufCommand[4]=wChan >> 8;
  sCmd.BufCommand[5]=wChan;
  if(bAdd) sCmd.BufCommand[6]=1;
  else sCmd.BufCommand[6]=0;
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(WaitReply(11,145)) return(true);
  }
  return(false);
}

bool ExistEqualChannelMKA2Conference(unsigned char *MassAbntInd,unsigned char btInd0)
{
  unsigned char i,btInd1;
  unsigned short wChan0,wChan1;

  wChan0=GetAbonentConnectionPointFromIndex(btInd0);
  for(i=0;i<MAX_CONF_ABONENT;i++)
  {
    btInd1=MassAbntInd[i];
    if(btInd1==0xff) continue;
    if(btInd1!=btInd0)
    {
      wChan1=GetAbonentConnectionPointFromIndex(btInd1);
      if(wChan0==wChan1) return(true);
    }
  }
  return(false);
}

void DelAbonentFromConference(unsigned char PultNb,unsigned char btKey,unsigned char btLayer)
{
  unsigned char Conf,Abnt,btInd;
  unsigned short wChan;

  btInd=GetAbonentStateIndFromKey(PultNb,btKey,btLayer);
  if(btInd!=0xff)
  {
    wChan=GetAbonentConnectionPointFromIndex(btInd);
    for(Conf=0;Conf<NumberKonf;Conf++)
    {
      if(!IsConferenceWork(PultNb,Conf)) continue;
      for(Abnt=0;Abnt<MAX_CONF_ABONENT;Abnt++)
      {
        if(s_MassConf[PultNb][Conf].btMassAbntInd[Abnt]==btInd)
        {
          if(!ExistEqualChannelMKA2Conference(s_MassConf[PultNb][Conf].btMassAbntInd,btInd))
          { AbonentConferenceCommuteAddDell(PultNb,Conf,wChan,false); }
          s_MassConf[PultNb][Conf].btMassAbntInd[Abnt]=0xff;
          for(Abnt=0;Abnt<MAX_CONF_ABONENT;Abnt++)
          {
            if(s_MassConf[PultNb][Conf].btMassAbntInd[Abnt]!=0xff) break;
          }
          if(Abnt==MAX_CONF_ABONENT) DelConferenceFromPult(PultNb,Conf);
          break;
        }
      }
    }
  }
}

void DeleteAllConference2Pult(unsigned char PultNb)
{
  unsigned char i;

  for(i=0;i<NumberKonf;i++)
  {
    if(IsConferenceWork(PultNb,i)) DelConferenceFromPult(PultNb,i);
  }
}

void AddAbonent2Conference(unsigned char PultNb,unsigned char btKey,unsigned char btLayer)
{
  unsigned char i,btInd,Conf,btMaxAbnt;
  unsigned short wChan;

  if((GetAbonentStateFromKey(PultNb,btKey,btLayer)==Talk) &&
     !IsKey2Conference(PultNb,btKey,btLayer,NULL))
  {
    btInd=GetAbonentStateIndFromKey(PultNb,btKey,btLayer);
    wChan=GetAbonentConnectionPointFromIndex(btInd);
    Conf=s_MassConferenceActive[PultNb];
    btMaxAbnt=s_MassConf[PultNb][Conf].btMaxAbnt;
/*    for(i=0;i<btMaxAbnt;i++)
    {
      if(s_MassConf[PultNb][Conf].btMassAbntInd[i]==btInd) break;
    }
    if(i==btMaxAbnt)*/
    {
      for(i=0;i<btMaxAbnt;i++)
      {
        if(s_MassConf[PultNb][Conf].btMassAbntInd[i]==0xff)
        {
          s_MassConf[PultNb][Conf].btChange=1;
          if(s_MassConf[PultNb][Conf].btState==d_ConferenceWork)
          {
            if(ExistEqualChannelMKA2Conference(s_MassConf[PultNb][Conf].btMassAbntInd,btInd))
            { Commute(false,wChan,GetAbonentOperatorChannelFromIndex(btInd)); }
            else
            {
              if(!AbonentConferenceCommuteAddDell(PultNb,Conf,wChan,true)) break;
              Commute(false,wChan,GetAbonentOperatorChannelFromIndex(btInd));
            }
          }
          IndicCmd(PultNb,btLayer,btKey,GREEN,1,1);
          s_MassConf[PultNb][Conf].btMassAbntInd[i]=btInd;
          if(s_MassConf[PultNb][Conf].btState==d_ConferenceWork) SendAllAbonentConference2IpPult(PultNb,Conf);
          return;
        }
      }
    }
  }
  PutErrInfo2Display(PultNb,"������ ������.��������!");
}

void AbonentConferenceIndic(unsigned char PultNb,unsigned char ConfNb,unsigned char btColor)
{
  unsigned char i,btInd,btLayer;
  ABONENT_STATE_TP *psAbnt;

  btLayer=GetCurrentLayer(PultNb);
  for(i=0;i<MAX_CONF_ABONENT;i++)
  {
    btInd=s_MassConf[PultNb][ConfNb].btMassAbntInd[i];
    if(btInd!=0xff)
    {
      psAbnt=GetAbonentStatePtrFromIndex(btInd);
      if(psAbnt) IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKey,btColor,0,0);
    }
  }
}

void PressConferenceKey(unsigned char PultNb,unsigned char Key,unsigned char btLayer,
                        unsigned char ConfNb,bool bOtkl,bool bTest)
{
  if(!EnableConference(PultNb,ConfNb))
  { PutErrInfo2Display(PultNb,NULL); return; }
  if(IsConferenceWork(PultNb,ConfNb))
  {
    if(bOtkl) DelConferenceFromPult(PultNb,ConfNb);
    else
    {
      s_MassConferenceActive[PultNb]=ConfNb;
      if(bTest) 
      {
        s_MassConf[PultNb][ConfNb].btState=d_ConferenceIndic;
        AbonentConferenceIndic(PultNb,ConfNb,FAST_BLINK_GREEN_ORANGE);
      }
    }
  }
  else
  {
    if(!bOtkl)
    {
      OutgoingNumberTmpTOutClear(PultNb);
      IndicClock(PultNb);
      s_MassConferenceActive[PultNb]=ConfNb;
      s_MassConf[PultNb][ConfNb].btState=d_AddAbnt2Conference;
      s_MassConf[PultNb][ConfNb].btKey=Key;
      s_MassConf[PultNb][ConfNb].btMaxAbnt=MAX_CONF_ABONENT;
      IndicCmd(PultNb,btLayer,s_MassConf[PultNb][ConfNb].btKey,ORANGE,1,1);
    }
  }
}

void RestoreCommuteWithAbnt(unsigned char PultNb,unsigned char ConfNb)
{
  unsigned char i,btInd,btCnt,btAdd;
  unsigned short wConnectionPoint,wOper;

  for(i=0;i<MAX_CONF_ABONENT;i++)
  {
    btInd=s_MassConf[PultNb][ConfNb].btMassAbntInd[i];
    if(btInd!=0xff)
    {
      wConnectionPoint=GetAbonentConnectionPointFromIndex(btInd);
      wOper=GetAbonentOperatorChannelFromIndex(btInd);
      Commute(true,wOper,wConnectionPoint);
      Commute(true,wConnectionPoint,wOper);
    }
  }
}

bool CreateCommuteConference(unsigned char PultNb,unsigned char ConfNb,unsigned short wOper,
                             unsigned char btMaxAbnt,unsigned char *btMassAbntInd)
{
  unsigned char i,j,btInd,btOff;
  unsigned short wChan;
  static sCommand sCmd;

  sCmd.Source=UserRS485;// �������� ������� RS485
  sCmd.L=7+btMaxAbnt*2; // ���������� ���� � �������
  sCmd.A1=0;
  sCmd.BufCommand[0]=10; // �������
  sCmd.BufCommand[1]=143; // ����������
  sCmd.BufCommand[2]=MassM1[PultNb]; // ����� ����������
  sCmd.BufCommand[3]=ConfNb; // ����� �����������
  sCmd.BufCommand[4]=wOper>>8;
  sCmd.BufCommand[5]=wOper;
  sCmd.BufCommand[6]=btMaxAbnt; // ����� ���������
  for(i=0,btOff=7;i<btMaxAbnt;i++)
  {
    wChan=0x8000;
    btInd=btMassAbntInd[i];
    if(btInd!=0xff)
    {
      wChan=GetAbonentConnectionPointFromIndex(btInd);
      for(j=0;j<i;j++)
      {
        btInd=btMassAbntInd[j];
        if(btInd!=0xff)
        {
          if(GetAbonentConnectionPointFromIndex(btInd)==wChan)
          { wChan=0x8000; break; }
        }
      }
    }
    sCmd.BufCommand[btOff++]=wChan >> 8;
    sCmd.BufCommand[btOff++]=wChan;
  }
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(WaitReply(11,143)) return(true);
  }
  return(false);
}

void CreateConference(unsigned char PultNb,unsigned char Key,unsigned char btLayer,unsigned char ConfNb)
{
  unsigned char i,btInd,btCnt,btAdd;
  unsigned short wOper;

  wOper=GetOperatorChannelFromKey(PultNb,Key,btLayer);
  if(wOper)
  {
    for(i=0,btCnt=0;i<s_MassConf[PultNb][ConfNb].btMaxAbnt;i++)
    {
      btInd=s_MassConf[PultNb][ConfNb].btMassAbntInd[i];
      if(btInd!=0xff)
      {
        Commute(false,GetAbonentOperatorChannelFromIndex(btInd),GetAbonentConnectionPointFromIndex(btInd));
        btCnt++;
      }
    }
    if(btCnt)
    {
      btAdd=btCnt/3;
      if(btAdd<3) btAdd=3;
      btCnt+=btAdd;
      if(btCnt<6) s_MassConf[PultNb][ConfNb].btMaxAbnt=6;
      else s_MassConf[PultNb][ConfNb].btMaxAbnt=btCnt;
      s_MassConf[PultNb][ConfNb].wOper=wOper;
      if(CreateCommuteConference(PultNb,ConfNb,
                                 s_MassConf[PultNb][ConfNb].wOper,
                                 s_MassConf[PultNb][ConfNb].btMaxAbnt,
                                 s_MassConf[PultNb][ConfNb].btMassAbntInd))
      {
        SendAllAbonentConference2IpPult(PultNb,ConfNb);
        s_MassConf[PultNb][ConfNb].btState=d_ConferenceWork;
        s_MassConf[PultNb][ConfNb].btTalk=1;
        s_MassConf[PultNb][ConfNb].btChange=0xff;
        DSRS_On(PultNb,s_MassConf[PultNb][ConfNb].wOper);
        IndicCmd(PultNb,btLayer,s_MassConf[PultNb][ConfNb].btKey,GREEN,1,1);
        return;
      }
      RestoreCommuteWithAbnt(PultNb,ConfNb);
    }
  }
  s_MassConf[PultNb][ConfNb].btState=0xff;
  IndicCmd(PultNb,btLayer,s_MassConf[PultNb][ConfNb].btKey,Off,0,0);
  PutErrInfo2Display(PultNb,"������ �������� ����.");
}

bool ConferenceOperatorOnOff(unsigned char PultNb,unsigned char ConfNb,unsigned char btOn)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485;// �������� ������� RS485
  sCmd.L=7; // ���������� ���� � �������
  sCmd.A1=0;
  sCmd.BufCommand[0]=10; // �������
  sCmd.BufCommand[1]=146; // ����������
  sCmd.BufCommand[2]=MassM1[PultNb]; // ����� ����������
  sCmd.BufCommand[3]=ConfNb; // ����� �����������
  sCmd.BufCommand[4]=btOn;
  sCmd.BufCommand[5]=s_MassConf[PultNb][ConfNb].wOper >> 8;
  sCmd.BufCommand[6]=s_MassConf[PultNb][ConfNb].wOper;
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(WaitReply(11,146)) return(true);
  }
  return(false);
}

void UnpressConferenceKey(unsigned char PultNb,unsigned char Key,unsigned char btLayer,unsigned char ConfNb)
{
  if(s_MassConferenceActive[PultNb]!=ConfNb) return;
  s_MassConferenceActive[PultNb]=0xff;
  if(s_MassConf[PultNb][ConfNb].btState==d_ConferenceIndic)
  {
    s_MassConf[PultNb][ConfNb].btState=d_ConferenceWork;
    AbonentConferenceIndic(PultNb,ConfNb,GREEN);
    return;
  }
  if(s_MassConf[PultNb][ConfNb].btState==d_AddAbnt2Conference) CreateConference(PultNb,Key,btLayer,ConfNb);
  else
  {
    if(s_MassConf[PultNb][ConfNb].btChange!=0xff) s_MassConf[PultNb][ConfNb].btChange=0xff;
    else
    {
      if(s_MassConf[PultNb][ConfNb].btTalk!=0xff)
      { // ��������� ��������� �� �����������
        ConferenceOperatorOnOff(PultNb,ConfNb,0);
        s_MassConf[PultNb][ConfNb].btTalk=0xff;
        IndicCmd(PultNb,btLayer,s_MassConf[PultNb][ConfNb].btKey,BLINK_GREEN,1,1);
      }
      else
      { // �������� ��������� � �����������
        ConferenceOperatorOnOff(PultNb,ConfNb,1);
        s_MassConf[PultNb][ConfNb].btTalk=1;
        IndicCmd(PultNb,btLayer,s_MassConf[PultNb][ConfNb].btKey,GREEN,1,1);
      }
    }
  }
}

