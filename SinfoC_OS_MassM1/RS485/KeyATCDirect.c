#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

//void GroupInfo2DataSave1(unsigned short wGroup);

unsigned char GetKey4CompatibleATCLine(unsigned char PultNb,unsigned char btLayer,unsigned char Group)
{
  unsigned char Key;

  if(!Group || (Group==0xff)) return(0xfe);
  for(Key=0;Key<NumberKey;Key++)
  {
    if((MassKey[PultNb][btLayer][Key].TypeAbon==ATC) &&
       (GetGroupFromChan(MassKey[PultNb][btLayer][Key].NChan)==Group) &&
       IsFreeMKALine(PultNb,Key,btLayer,0xff)) return(Key);
  }
  return(0xff);
}

void OtboiAbonent_ATCDirect(unsigned char PultNb,unsigned char btLayer,unsigned char Key)
{
  unsigned char btInd;
  ABONENT_STATE_TP *psAbnt;

  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return;
    if(psAbnt->wConnectionPoint!=0xffff)
    {
      if((psAbnt->btPultNb==PultNb) && (psAbnt->btLayer==btLayer) && (psAbnt->btKeyATCDirect==Key))
      { OtboiAbonent(psAbnt->btPultNb,psAbnt->btKey,psAbnt->btLayer); }
    }
    btInd++;
  }
  while(1);
}

unsigned char ATSDirectConnected2ATS(unsigned char PultNb,unsigned char btLayer,unsigned char Key)
{
  unsigned char btInd;
  ABONENT_STATE_TP *psAbnt;

  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return(0xff);
    if(psAbnt->wConnectionPoint!=0xffff)
    {
      if((psAbnt->btPultNb==PultNb) && (psAbnt->btLayer==btLayer) && (psAbnt->btKeyATCDirect==Key)) return(psAbnt->btKey);
    }
    btInd++;
  }
  while(1);
}

unsigned char GetPressedFreeATCLine(unsigned char PultNb,unsigned char btLayer)
{
  unsigned char btInd;
  ABONENT_STATE_TP *psAbnt;

  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return(0xff);
    if(psAbnt->wConnectionPoint!=0xffff)
    {
      if((psAbnt->btPultNb==PultNb) && (psAbnt->btLayer==btLayer) && (psAbnt->btType==ATC) &&
         (psAbnt->btKeyATCDirect==0xff) && (psAbnt->btTlfn[0]==0xff)) return(psAbnt->btKey);
    }
    btInd++;
  }
  while(1);
}

void Key_ATCDirect(unsigned char PultNb,unsigned char btLayer,unsigned char KeyATCDirect)
{
  unsigned char Key,Ln,NumbAbon;

  NumbAbon=MassKey[PultNb][btLayer][KeyATCDirect].NumbAbon;
  if(!NumbAbon) return;
  NumbAbon--;
  Ln=0;
  while(MassTel[NumbAbon][Ln]!=0xFF)
  {
    Ln++;
    if(Ln==MAX_DIGIT_TLFN-2) break;
  }
  if(Ln)
  {
    // �������� �� ��� ������� ������� ���_������ � ������������ ������� ��������� ������� ���
    Key=ATSDirectConnected2ATS(PultNb,btLayer,KeyATCDirect);
    if(Key!=0xff)
    { PressCallKey(PultNb,Key,false,false); return; }
    Key=GetPressedFreeATCLine(PultNb,btLayer);
    if(Key!=0xff)
    { OutgoingCallPressedFreeATS(PultNb,Key,btLayer,KeyATCDirect,MassTel[NumbAbon]); return; }
    Key=GetKey4CompatibleATCLine(PultNb,btLayer,MassTel[NumbAbon][MAX_DIGIT_TLFN-1]);
    if(Key!=0xff)
    {
      SetOutgoingNumberTmp(PultNb,MassTel[NumbAbon]);
      PutOutgoingNumber2Display(PultNb,MassTel[NumbAbon],Ln);
      if(Key!=0xfe) OutgoingCallATC(PultNb,Key,btLayer,KeyATCDirect);
    }
    else PutErrInfo2Display(PultNb,"��� ����� ���!");
  }
  else PutErrInfo2Display(PultNb,"��� ������ ���.");
}

