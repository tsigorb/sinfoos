#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

///////////////////////////////////////////////////////////////////////////////
bool AutoAnswer4Call_Level(unsigned short wNChan)
{
  unsigned char PultNb,btLayer,btKey,btCallType;
  bool bRes;
  ABONENT_STATE_TP *psAbnt;

  bRes=false;
  btCallType=0xff;
  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if(MassM1[PultNb] && IsKbdReady(PultNb) && IsKbdUnlock(PultNb))
    {
      btKey=FindAbntKey2Pult(PultNb,&btLayer,wNChan,&btCallType,0xff);
      if((btKey!=0xff) && (btCallType==Level))
      {
        psAbnt=GetAbonentStatePtrFromKey(PultNb,btKey,btLayer);
        if(psAbnt)
        {
          if(GetAbonentStateFromPtr(psAbnt)==Talk)
          { // ���� �������� �� ����� �� ������� -> �� ��������� ����� �������� �� ����
            psAbnt->dwTimerEnd=MassTimer[TIMER_SPEAKLEVEL]+xTaskGetTickCount();
            bRes=true;
          }
        }
        else
        {
          if(IsGroupAutoAnswer4PultFromChan(PultNb,wNChan))
          {
            psAbnt=GetFreeAbonentStatePtr();
            if(psAbnt)
            {
              psAbnt->wConnectionPoint=wNChan;
              psAbnt->wOperatorChanel=GetOperatorChannelFromKey(PultNb,btKey,btLayer);
              psAbnt->btPultNb=PultNb;
              psAbnt->btLayer=btLayer;
              psAbnt->btKey=btKey;
              psAbnt->NumbAbon=0xff;
              psAbnt->btType=btCallType;
              psAbnt->dwState=Off;
              AnswerIncomingCallMKA(psAbnt);
              psAbnt->dwTimerEnd=MassTimer[TIMER_SPEAKLEVEL]+xTaskGetTickCount();
              psAbnt->dwTimerAnaliz=100+xTaskGetTickCount();
              return(true);
            }
          }
        }
      }
    }
  }
  return(bRes);
}

void IncomingCall_Level(unsigned short wNChan)
{ // ����� ��������� �� ��� ������, � ������������� ������ �����!
SendStr2IpLogger("$CALL_LVL$IncomingCall_Level: wChan=%d",wNChan);
  if(AutoAnswer4Call_Level(wNChan)) return;
  else
  {
    unsigned char PultNb,btLayer,btKey,btCallType;
    ABONENT_STATE_TP *psAbnt;

    btCallType=0xff;
    for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
    {
      if(MassM1[PultNb] && IsKbdReady(PultNb) && IsKbdUnlock(PultNb))
      {
        btKey=FindAbntKey2Pult(PultNb,&btLayer,wNChan,&btCallType,0xff);
        if(btKey!=0xff)
        {
          switch(btCallType)
          {
            case Level:
              psAbnt=GetAbonentStatePtrFromKey(PultNb,btKey,btLayer);
              if(psAbnt)
              {
                if(GetAbonentStateFromPtr(psAbnt)==Talk)
                { psAbnt->dwTimerEnd=MassTimer[TIMER_SPEAKLEVEL]+xTaskGetTickCount(); }
              }
              else
              {
                psAbnt=GetFreeAbonentStatePtr();
                if(psAbnt)
                {
                  psAbnt->wConnectionPoint=wNChan;
                  psAbnt->wOperatorChanel=GetOperatorChannelFromKey(PultNb,btKey,btLayer);
                  psAbnt->btPultNb=PultNb;
                  psAbnt->btLayer=btLayer;
                  psAbnt->btKey=btKey;
                  psAbnt->NumbAbon=0xff;
                  psAbnt->btType=btCallType;
                  psAbnt->dwState=Off;
                  Commute(true,psAbnt->wOperatorChanel,psAbnt->wConnectionPoint);
                  psAbnt->btLedState=dLedStateIncomingCall;
                  SetAbonentStateByPtr(psAbnt,IncomingCall,0);
                  psAbnt->dwTimerEnd=MassTimer[TIMER_INCOMINGCALL]+xTaskGetTickCount();
                  IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,0,0);
                }
              }
              break;
            case PPSR:
              btKey=FindAbntKey2Pult(PultNb,&btLayer,wNChan,&btCallType,0);
              if(btKey!=0xff)
              { // ������ �������� ������� ���� � ������� 0!
                psAbnt=GetAbonentStatePtrFromKey(PultNb,btKey,btLayer);
                if(psAbnt)
                {
                  if(GetAbonentStateFromPtr(psAbnt)==Talk)
                  { psAbnt->dwTimerEnd=MassTimer[TIMER_SPEAKLEVEL]+xTaskGetTickCount(); }
                }
              }
              break;
            case RSDT:
              psAbnt=NULL;
              btKey=FindAbntKey2Pult(PultNb,&btLayer,wNChan,&btCallType,0);
              if(btKey!=0xff)
              { // ������ �������� ������� RSDT � ������� 0!
                psAbnt=GetAbonentStatePtrFromKey(PultNb,btKey,btLayer);
                if(psAbnt)
                {
                  if(GetAbonentStateFromPtr(psAbnt)==Talk)
                  { psAbnt->dwTimerEnd=MassTimer[TIMER_SPEAKLEVEL]+xTaskGetTickCount(); }
                }
              }
              if(!psAbnt) IncomingCall_PPSR_RSDT(wNChan,RSDT);
              break;
            default: break;
          }
        }
      }
    }
  }
}

void OutgoingCallLevel(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  ABONENT_STATE_TP *psAbnt;

  psAbnt=GetFreeAbonentStatePtr();
  if(psAbnt)
  {
    psAbnt->btPultNb=PultNb;
    psAbnt->btLayer=btLayer;
    psAbnt->btKey=Key;
    psAbnt->wConnectionPoint=MassKey[PultNb][btLayer][Key].NChan;
    psAbnt->wOperatorChanel=GetOperatorChannelFromKey(PultNb,Key,btLayer);
    psAbnt->NumbAbon=0xff;
    psAbnt->btType=Level;
    psAbnt->dwTimerEnd=MassTimer[TIMER_INCOMINGCALL]+xTaskGetTickCount();
    psAbnt->dwTimerAnaliz=100+xTaskGetTickCount();
    psAbnt->btLedState=dLedStateTalk;
    SetAbonentStateByPtr(psAbnt,Talk,0);
    DSRS_On(psAbnt->btPultNb,psAbnt->wOperatorChanel);
    Commute(true,psAbnt->wOperatorChanel,psAbnt->wConnectionPoint);
    Commute(true,psAbnt->wConnectionPoint,psAbnt->wOperatorChanel);
    IndicCmd(PultNb,psAbnt->btLayer,Key,psAbnt->btLedState,1,1);
  }
}

void PressCallKey_Level(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  ABONENT_STATE_TP *psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);

  if(!psAbnt)
  {
    if(IsCreatedTransit(PultNb)) XorAbnt2Transit(PultNb,Key);
    else OutgoingCallLevel(PultNb,Key,btLayer);
  }
  else
  {
    switch(GetAbonentStateFromPtr(psAbnt))
    {
      case IncomingCall:
      {
        AnswerIncomingCallMKA(psAbnt);
        psAbnt->dwTimerEnd=MassTimer[TIMER_SPEAKLEVEL]+xTaskGetTickCount();
        psAbnt->dwTimerAnaliz=100+xTaskGetTickCount();
        break;
      }
    }
  }
}

void CallLevelTOut(unsigned char PultNb)
{
  unsigned char btInd,btState;
  ABONENT_STATE_TP *psAbnt;

  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return;
    if((psAbnt->wConnectionPoint!=0xffff) && (psAbnt->btPultNb==PultNb) &&
       ((psAbnt->btType==Level) || (psAbnt->btType==PPSR) || (psAbnt->btType==RSDT)))
    {
      if(psAbnt->dwTimerEnd)
      {
        if(IsCurrentTickCountGT(psAbnt->dwTimerEnd))
        {
          if((psAbnt->btType==PPSR) || (psAbnt->btType==RSDT))
          {
            if(psAbnt->NumbAbon)
            { // ������� � ����� ���������� �������� ���� ��� ����, � ������ ���� ������� �������� �� ������
              unsigned char PultNb=psAbnt->btPultNb,btType=psAbnt->btType;
              unsigned short wCP=psAbnt->wConnectionPoint;

              IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKey,OFF,0,0);
              ClearAbonentStateByPtr(psAbnt);
              CreateOutgoingAbonentPPSR_RSDT(PultNb,wCP,btType);
            }
            else OtboiAbonentMKA(psAbnt->btPultNb,psAbnt->wConnectionPoint,0,psAbnt->btType);
          }
          else OtboiAbonentMKA(psAbnt->btPultNb,psAbnt->wConnectionPoint,0xff,Level);
        }
      }
      if(psAbnt->dwTimerAnaliz)
      {
        if(IsCurrentTickCountGT(psAbnt->dwTimerAnaliz))
        {
          if(IsLevelPresent(psAbnt->wConnectionPoint))
          { psAbnt->dwTimerEnd=MassTimer[TIMER_SPEAKLEVEL]+xTaskGetTickCount(); }
          psAbnt->dwTimerAnaliz=100+xTaskGetTickCount();
        }
      }
    }
    btInd++;
  }
  while(1);
}

