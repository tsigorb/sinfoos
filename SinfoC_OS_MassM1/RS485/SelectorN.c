#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"
#include "SelectorN.h"

///////////////////////////////////////////////////////////////////////////////
bool LoadSoundForSelector(unsigned char SN,unsigned char NSel);

///////////////////////////////////////////////////////////////////////////////
SELECTOR_TP s_sSelectors[NumberKeyboard];
SELECTOR_ABNT_TP *s_psAbntS;

bool s_bAutomaticCall[NumberKeyboard];

///////////////////////////////////////////////////////////////////////////////
bool IsWorkSelector(void)
{
  int i;
  
  for(i=0;i<NumberKeyboard;i++)
  {
    if(s_sSelectors[i].btState) return(true);
  }
  return(false);
}

void CreateNameAbntSelector(unsigned char PultNb,SELECTOR_ABNT_TP *pSelAbnt)
{
  unsigned char btLayer,btKey,btType,btNumb,btLen;
  ABONENT_STATE_TP *psAbnt;
  static char strNumb[MAX_DIGIT_TLFN+1];

  strNumb[0]=0;
  btLayer=s_sSelectors[PultNb].btLayer;
  btKey=pSelAbnt->Key;
  btType=MassKey[PultNb][btLayer][btKey].TypeAbon;
  if(btType>ISDN) btType=0;
//SendStr2IpLogger("$SELECTOR$CreateNameAbntSelector: btType=%d",btType);
  psAbnt=GetAbonentStatePtrFromKey(PultNb,btKey,btLayer);
  switch(btType)
  {
    case ISDN:
    case ATC:
      btNumb=MassKey[PultNb][btLayer][btKey].NumbAbon;
      if(btNumb) Tlfn2Str(strNumb,MassTel[btNumb-1]);
      else 
      {
        if(psAbnt) Tlfn2Str(strNumb,psAbnt->btTlfn);
      }
      break;
    default:
      if(psAbnt) 
      {
        LockPrintf();
        sprintf(strNumb,"%d",psAbnt->wConnectionPoint);
        UnlockPrintf();
      }
      break;
  }
  LockPrintf();
  pSelAbnt->Name[32]=0;
  sprintf(pSelAbnt->Name,"%d: %s-%s",btKey,g_strCallName[btType],strNumb);
  btLen=strlen(pSelAbnt->Name);
  if(btLen<32) memset(pSelAbnt->Name+btLen,' ',32-btLen);
  UnlockPrintf();
}

bool AddAbonent2Selector(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  unsigned short wOperator;
  ABONENT_STATE_TP *psAbnt;
  SELECTOR_ABNT_TP *psSAbnt;

  if(btLayer!=s_sSelectors[PultNb].btLayer) return(false);
  psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);
  if(!psAbnt) return false;
  if(_xAddAbnt2Selector(PultNb,psAbnt->wConnectionPoint))
  {
    if(_InsertAbonent2MassAbnt(PultNb,psAbnt))
    {
SendStr2IpLogger("$SELECTOR$AddAbonent2Selector: PultNb=%d, Key=%d",PultNb,Key);
      wOperator=GetOperatorChannelFromKey(PultNb,Key,btLayer);
      Commute(false,psAbnt->wConnectionPoint,wOperator);
      psSAbnt=FindAbonentSelector(PultNb,Key,btLayer);
      if(psSAbnt && (!psSAbnt->Name[0])) CreateNameAbntSelector(PultNb,psSAbnt);
      // ��������� ��������, ���� �� ������� ������ ��������� ������ ��� ����������...
      DSRS_Off(PultNb,wOperator);
      return(true);
    }
    else _xDelAbntFromSelector(PultNb,psAbnt->wConnectionPoint);
  }
else SendStr2IpLogger("$SELECTOR$AddAbonent2Selector error: PultNb=%d, Key=%d",PultNb,Key);
  return(false);
}

/////////// ��� ������ "��������" /////////////////////////////////////////////
void OtboiAbonentSelector(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  ABONENT_STATE_TP *psAbnt;
  
  psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);
  if(!psAbnt) return;
  OtboiAbonentCommonPart(PultNb,Key,btLayer,NULL,NULL);
}

bool OutgoingCallSelector(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  unsigned short NChan;
  unsigned char btSound;
  ABONENT_STATE_TP *psAbnt;

  if(IsKey2Selector(PultNb,Key,btLayer,d_Abonent2SelectorOtherPult))
  { PutErrInfo2Display(PultNb,NULL); return(false); }
  psAbnt=GetFreeAbonentStatePtr();
  if(psAbnt)
  {
    NChan=MassKey[PultNb][btLayer][Key].NChan;
    psAbnt->wConnectionPoint=NChan;
    psAbnt->wOperatorChanel=GetOperatorChannelFromKey(PultNb,Key,btLayer);
    psAbnt->btPultNb=PultNb;
    psAbnt->btLayer=btLayer;
    psAbnt->btKey=Key;
    SetLedColorIfTalk(psAbnt);
    SetAbonentStateByPtr(psAbnt,Talk,0);
    psAbnt->btType=Selector;
    psAbnt->dwTimerEnd=0;
    if(IsAutomaticCall(PultNb)) return(true);
    IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,1,1);
    DSRS_On(PultNb,psAbnt->wOperatorChanel);
    Commute(true,NChan,psAbnt->wOperatorChanel);
    Commute(true,psAbnt->wOperatorChanel,NChan);
    return(true);
  }
  return(false);
}

void PressCallKey_Selector(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  ABONENT_STATE_TP *psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);

  if(IsKey2Selector(PultNb,Key,btLayer,d_Abonent2SelectorOtherPult))
  { PutErrInfo2Display(PultNb,NULL); return; }
  if(!psAbnt)
  {
    if(IsSelectorActive(PultNb) && IsPressAddMember(PultNb))
    {
      SetAutomaticCall(PultNb,true);
      OutgoingCallSelector(PultNb,Key,btLayer);
      SetAutomaticCall(PultNb,false);
      AddAbonent2Selector(PultNb,Key,btLayer);
    }
    else
    {
      if(IsCreatedTransit(PultNb)) XorAbnt2Transit(PultNb,Key);
      else OutgoingCallSelector(PultNb,Key,btLayer);
    }
  }
  else
  {
    switch(GetAbonentStateFromPtr(psAbnt))
    {
      case Talk:
      {
        if(IsSelectorActive(PultNb) && IsPressAddMember(PultNb)) AddAbonent2Selector(PultNb,Key,btLayer);
        else
        {
          if(IsPressDisEnOut(PultNb))
          {
            if(CmdEnDisOutCP(psAbnt->wConnectionPoint,0))
            {
SendStr2IpLogger("$SELECTOR$SelectorDisableOut: PultNb=%d, Key=%d",PultNb,psAbnt->btKey);
              SetAbonentStateByPtr(psAbnt,SelectorDisableOut,0);
              IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,1,1);
            }
          }
          else HoldOn(PultNb,Key,btLayer,true);
        }
        break;
      }
      case Hold:
      {
        HoldOn(PultNb,Key,btLayer,false);
        break;
      }
      case SelectorDisableOut:
      {
        if(CmdEnDisOutCP(psAbnt->wConnectionPoint,1))
        {
SendStr2IpLogger("$SELECTOR$SelectorEnableOut: PultNb=%d, Key=%d",PultNb,psAbnt->btKey);
          SetAbonentStateByPtr(psAbnt,Talk,0);
          SetLedColorIfTalk(psAbnt);
          IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,1,1);
        }
        break;
      }
      default: break;
    }
  }
}

void SetLeader2Member(unsigned char PultNb,SELECTOR_ABNT_TP *psAbntS)
{ // ������ �������� �������� ����������
  _xCmdAbntSelectorStatus(PultNb,psAbntS,StatusMember);
  SetAbonentStatus2Selector(PultNb,psAbntS,StatusMember);
SendStr2IpLogger("$SELECTOR$SetLeader2Member: PultNb=%d, Key=%d",PultNb,psAbntS->Key);
}

void ReplaceLeader(unsigned char PultNb,SELECTOR_ABNT_TP *psAbntS)
{ // ������ �������� �������, � ������� �������� - ����������
  unsigned char btInd;
  SELECTOR_ABNT_TP *pMassAbnt;

  pMassAbnt=s_sSelectors[PultNb].pMassAbnt;
  for(btInd=0;btInd<s_sSelectors[PultNb].btCnt;btInd++)
  {
    if((pMassAbnt[btInd].Status & 0x07)==StatusLeader)
    {
      _xCmdAbntSelectorStatus(PultNb,pMassAbnt+btInd,StatusMember);
      SetAbonentStatus2Selector(PultNb,pMassAbnt+btInd,StatusMember);
SendStr2IpLogger("$SELECTOR$ReplaceLeader: PultNb=%d, OldLeaderKey=%d, NewLeaderKey=%d");
      break;
    }
  }
  psAbntS->btPisk=1;
  _xCmdAbntSelectorStatus(PultNb,psAbntS,StatusLeader);
  SetAbonentStatus2Selector(PultNb,psAbntS,StatusLeader);
}

bool IsAbonentListener(unsigned char PultNb,unsigned char Key)
{
  unsigned char i;
  SELECTOR_ABNT_TP *psAbntS;

  if(s_sSelectors[PultNb].btState)
  {
    psAbntS=s_sSelectors[PultNb].pMassAbnt;
    for(i=0;i<s_sSelectors[PultNb].btCnt;i++,psAbntS++)
    {
      if(psAbntS->Key==Key)
      {
        if(psAbntS->Status & 0x70)
        {
          if((psAbntS->Status & 0x70)!=d_AbntSelectorTalkOn) return true;
        }
        else
        {
          if((psAbntS->Status & 0x07)==StatusListener) return true;
        }
        return false;
      }
    }
  }
  return false;
}

void XchgMemberListener(unsigned char PultNb,SELECTOR_ABNT_TP *psAbntS)
{
  unsigned char btStatus;

  if((psAbntS->Status & 0x07)==StatusMember)
  {
    ABONENT_STATE_TP *psAbnt=GetAbonentStatePtrFromKey(PultNb,psAbntS->Key,GetCurrentLayer(PultNb));

    if(psAbnt && (psAbnt->btType==IPPM))
    { // ������������� ���� �� IPPM
      DelCP_IPPM2MU(psAbnt->NumbAbon);
    }
    btStatus=StatusListener; // ����������
  }
  else btStatus=StatusMember; // �������������
  psAbntS->btPisk=1;
  _xCmdAbntSelectorStatus(PultNb,psAbntS,btStatus);
  SetAbonentStatus2Selector(PultNb,psAbntS,btStatus);
SendStr2IpLogger("$SELECTOR$XchgMemberListener: PultNb=%d, Key=%d, btStatus=%d",
        PultNb,psAbntS->Key,btStatus);
}

void TmpExcludeAbonentFromSelector(unsigned char PultNb,unsigned char btLayer,SELECTOR_ABNT_TP *psAbntS)
{
  unsigned short NChan;
  ABONENT_STATE_TP *psAbnt;

  if(s_psAbntS) return;
  psAbnt=GetAbonentStatePtrFromKey(PultNb,psAbntS->Key,btLayer);
  if(!psAbnt || (psAbnt->wConnectionPoint==d_DummyCP)) return;
  s_psAbntS=psAbntS;
  _xDelAbntFromSelector(PultNb,psAbnt->wConnectionPoint);
  if(psAbnt->wConnectionPoint>d_DummyCP)
  {
    NChan=GetCP_IPPM2MU(psAbnt->NumbAbon);
    if(NChan)
    {
      _xDelAbntFromSelector(PultNb,NChan);
      DelCP_IPPM2MU(psAbnt->NumbAbon);
    }
    psAbnt->wConnectionPoint=GetFreeConnectionPoint();
    if(psAbnt->wConnectionPoint!=0xff) SendIPPMCmdConnectPrivate(psAbnt->NumbAbon);
    else psAbnt->wConnectionPoint=0xffff;
  }
  else
  {
    DSRS_On(PultNb,psAbnt->wOperatorChanel);
    Commute(true,psAbnt->wConnectionPoint,psAbnt->wOperatorChanel);
    Commute(true,psAbnt->wOperatorChanel,psAbnt->wConnectionPoint);
  }
SendStr2IpLogger("$SELECTOR$TmpExcludeAbonentFromSelector: PultNb=%d, Key=%d",PultNb,psAbntS->Key);
}

bool PressCallKey4Selector(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  ABONENT_STATE_TP *psAbnt;
  SELECTOR_ABNT_TP *psAbntS;

  if(s_sSelectors[PultNb].btState==d_SelectorTest) return(true);
  psAbntS=FindAbonentSelector(PultNb,Key,btLayer);
  if(!psAbntS) return(false);
  if((psAbntS->Status & 0x70)==d_AbntSelectorStatusBusy) return(true);
  if((psAbntS->Status & 0x70)==d_AbntSelectorStatusDel) return(true);
  psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);
  if(!psAbnt)
  {
    if(!(psAbntS->Status & 0x07)) return(true); // ������� ��� ��������� �� ��������� - ��������� ����� ����� �����
    DSRS_Off(PultNb,s_sSelectors[PultNb].wOperator | d_OperatorOffWithoutCondition);
    switch(MassKey[PultNb][btLayer][Key].TypeAbon)
    {
      case ISDN:
        OutgoingCallISDN(PultNb,Key,btLayer);
        break;
      case ATC:
        OutgoingCallATC(PultNb,Key,btLayer,0xff);
        break;
      case TDC:
        OutgoingCallTDC(PultNb,Key,btLayer,false);
        break;
      case Selector:
        OutgoingCallSelector(PultNb,Key,btLayer);
        break;
      case IPPM:
        OutgoingCallIPPM(PultNb,Key,btLayer,false);
        break;
      default: return(true);
    }
    SetAbonentStatus2Selector(PultNb,psAbntS,d_AbntSelectorStatusCall);
  }
  else
  {
    if(IsPressAddMember(PultNb))
    { // ������ ������� "+��������"
      if(FindAbonentSelector(PultNb,Key,btLayer))
      { // ������� ���� "��������" ��� � ��������� - ���������� ���
SendStr2IpLogger("$SELECTOR$PressCallKey4Selector (-abonent): PultNb=%d, Key=%d",PultNb,Key);
        OtboiAbonent(PultNb,Key,btLayer);
        return(true);
      }
    }
    if(psAbntS->Status & d_AbntSelectorDisableOut)
    {
      EnOutAbonentSelector(PultNb,psAbntS,psAbnt,true);
      return(true);
    }
    else
    {
      if(IsPressDisEnOut(PultNb))
      {
        EnOutAbonentSelector(PultNb,psAbntS,psAbnt,false);
        return(true);
      }
    }
    switch(psAbntS->Status & 0x70)
    {
      case d_AbntSelectorStatusCall:
        Commute(false,psAbnt->wOperatorChanel,psAbnt->wConnectionPoint);
        DSRS_Off(PultNb,psAbnt->wOperatorChanel | d_OperatorOffWithoutCondition);
        if(psAbnt->btType==TDC) PressCallKey_TDC(PultNb,Key,btLayer,false);
        if(AddAbonent2Selector(PultNb,Key,btLayer))
        { SetAbonentStatus2Selector(PultNb,psAbntS,psAbntS->Status & 0xf); }
        else
        { // ��� ������ ��������� ������� � ��������� ��������
          OtboiAbonent(PultNb,Key,btLayer);
          SetAbonentStatus2Selector(PultNb,psAbntS,d_AbntSelectorStatusWait);
        }
        return(true);
      case d_AbntSelectorStatusWait:
        if(GetAbonentStateFromPtr(psAbnt)==IncomingCall) return(false);
        return(true);
      case d_AbntSelectorTalkOff:
        if(s_sSelectors[PultNb].btState==d_SelectorWait)
        {
SendStr2IpLogger("$SELECTOR$PressCallKey4Selector (AbntSelectorTalkOn): PultNb=%d, Key=%d",PultNb,Key);
          _xCmdAbntSelectorStatus(PultNb,psAbntS,StatusMember);
          SetAbonentStatus2Selector(PultNb,psAbntS,d_AbntSelectorTalkOn);
          return(true);
        }
        break;
      case d_AbntSelectorTalkOn:
        if(s_sSelectors[PultNb].btState==d_SelectorWait)
        {
SendStr2IpLogger("$SELECTOR$PressCallKey4Selector (AbntSelectorTalkOff): PultNb=%d, Key=%d",PultNb,Key);
          _xCmdAbntSelectorStatus(PultNb,psAbntS,StatusListener);
          SetAbonentStatus2Selector(PultNb,psAbntS,d_AbntSelectorTalkOff);
          return(true);
        }
        break;
    }
    if(IsPressTest(PultNb)) TmpExcludeAbonentFromSelector(PultNb,btLayer,psAbntS);
    else
    {
      if(IsPressChoice(PultNb))
      {
        if((psAbntS->Status & 0x07)==StatusLeader) SetLeader2Member(PultNb,psAbntS);
        else ReplaceLeader(PultNb,psAbntS);
      }
      else
      {
        if((psAbntS->Status & 0x07)!=StatusLeader) XchgMemberListener(PultNb,psAbntS);
      }
    }
  }
  return(true);
}

///////////////////////////////////////////////////////////////////////////////
void DelAbonentFromSelector(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  unsigned char btInd,btState;
  SELECTOR_ABNT_TP *psAbntS;
  ABONENT_STATE_TP *psAbnt;

  psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);
  psAbntS=FindAbonentSelector(PultNb,Key,btLayer);
  if(!psAbntS) return;
  if(!IsManualOtboi(PultNb))
  {
    btState=psAbntS->Status & 0x70;
    if((btState==d_AbntSelectorStatusBusy) || (btState==d_AbntSelectorStatusDel)) return;
  }
  if(psAbnt)
  {
    if((psAbntS->Status & d_AbntSelectorDisableOut) && (psAbnt->wConnectionPoint<=48))
    { EnOutAbonentSelector(PultNb,psAbntS,psAbnt,true); }
SendStr2IpLogger("$SELECTOR$DelAbonentFromSelector: PultNb=%d, Key=%d",PultNb,Key);
    if(psAbnt->wConnectionPoint>d_DummyCP)
    { // ��� ���������� �������� IPPM...
      unsigned short NChan=GetCP_IPPM2MU(psAbnt->NumbAbon);
      if(NChan)
      {
        _xDelAbntFromSelector(PultNb,NChan);
        DelCP_IPPM2MU(psAbnt->NumbAbon);
      }
    }
    _xDelAbntFromSelector(PultNb,psAbnt->wConnectionPoint);
  }
  btState=d_AbntSelectorStatusOff;
  if(!IsManualOtboi(PultNb)) 
  {
    if(psAbnt &&
       (((psAbnt->btType==ISDN) || (psAbnt->btType==TDC) || (psAbnt->btType==IPPM)) && psAbnt->NumbAbon))
    { btState=d_AbntSelectorStatusDel; }
  }
  if(btState==d_AbntSelectorStatusOff) psAbntS->Name[0]=0;
  SetAbonentStatus2Selector(PultNb,psAbntS,btState);
}

void DestroySelector(unsigned char PultNb)
{
  unsigned char btInd,btOff,btLayer,btKey;
  SELECTOR_ABNT_TP *pMassAbnt;
  static sCommand sCmd;

  if(!s_sSelectors[PultNb].btState) return;
SendStr2IpLogger("$SELECTOR$DestroySelector: PultNb=%d, Key=%d",PultNb,s_sSelectors[PultNb].btKey);
  SetTikkerKeyOff(PultNb);
  btLayer=s_sSelectors[PultNb].btLayer;
  DSRS_Off(PultNb,s_sSelectors[PultNb].wOperator);
  s_sSelectors[PultNb].btState=Off; // ��� ��������� �������� ��������� �� ����������� ������
  IndicCmd(PultNb,s_sSelectors[PultNb].btLayer,s_sSelectors[PultNb].btKey,OFF,1,1);
  if(s_sSelectors[PultNb].btKeyBeginSelector) IndicCmd(PultNb,s_sSelectors[PultNb].btLayer,s_sSelectors[PultNb].btKeyBeginSelector,OFF,0,0);
  s_sSelectors[PultNb].btState=d_SelectorDestroy; // ��� ���������� ������ �-��� OtboiAbonent
  pMassAbnt=s_sSelectors[PultNb].pMassAbnt;
  if(pMassAbnt)
  {
    SetManualOtboi(PultNb,true);
    for(btInd=0;btInd<s_sSelectors[PultNb].btCnt;btInd++)
    {
      btKey=pMassAbnt[btInd].Key;
      if(btKey!=0xff)
      {
        OtboiAbonent(PultNb,btKey,btLayer);
        vTaskDelay(20);
      }
    }
    SetManualOtboi(PultNb,false);
    vPortFree(pMassAbnt);
  }
  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.BufCommand[0]=10; // �������
  sCmd.BufCommand[1]=156; // �������� ����� ������������ ���������
  sCmd.BufCommand[2]=MassM1[PultNb];
  sCmd.L=3; // ���������� ���� � �������
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  memset(s_sSelectors+PultNb,0,sizeof(SELECTOR_TP));
}

void EnableAllOut(unsigned char PultNb)
{
  unsigned char btInd,btLayer,btKey;
  SELECTOR_ABNT_TP *pMassAbnt;
  ABONENT_STATE_TP *psAbnt;

  pMassAbnt=s_sSelectors[PultNb].pMassAbnt;
  if(pMassAbnt)
  {
    btLayer=s_sSelectors[PultNb].btLayer;
    for(btInd=0;btInd<s_sSelectors[PultNb].btCnt;btInd++)
    {
      btKey=pMassAbnt[btInd].Key;
      if(btKey!=0xff)
      {
        psAbnt=GetAbonentStatePtrFromKey(PultNb,btKey,btLayer);
        if(psAbnt) CmdEnDisOutCP(psAbnt->wConnectionPoint,true);
      }
    }
  }
}

void ControlSelectorTOut(void)
{
  unsigned char PultNb,btLayer,btKey;

  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if(MassM1[PultNb])
    {
      if(s_sSelectors[PultNb].dwTOut)
      {
        if(IsCurrentTickCountGT(s_sSelectors[PultNb].dwTOut)) DestroySelector(PultNb);
      }
      if(s_sSelectors[PultNb].dwListenerTOut)
      {
        if(IsCurrentTickCountGT(s_sSelectors[PultNb].dwListenerTOut))
        {
          DetectListenerState(PultNb);
          s_sSelectors[PultNb].dwListenerTOut=1000+xTaskGetTickCount();
        }
      }
    }
  }
}

unsigned char GetLedColor4Sel(unsigned char btState)
{
  switch(btState & 0x7)
  {
    case d_SelectorError: return FAST_BLINK_RED;
    case d_SelectorWait: return BLINK_RED_GREEN;
    case d_SelectorWork:
    {
      if(btState & d_SelectorAllMember) return FAST_BLINK_GREEN;
      return GREEN;
    }
  }
  return OFF;
}

void PutKeyStateAbonentSelector2Pult(unsigned char PultNb,bool bShowAll)
{
  unsigned char btInd,btLayer,btKey,btPisk,btState,btLedState;
  SELECTOR_ABNT_TP *psAbntS;

  if((!s_sSelectors[PultNb].btState) || (!s_sSelectors[PultNb].pMassAbnt)) return;
  btLayer=s_sSelectors[PultNb].btLayer;
  if(bShowAll)
  {
    IndicCmd(PultNb,btLayer,s_sSelectors[PultNb].btKey,GetLedColor4Sel(s_sSelectors[PultNb].btState),0,0);
    if(s_sSelectors[PultNb].btKeyBeginSelector) IndicCmd(PultNb,btLayer,s_sSelectors[PultNb].btKeyBeginSelector,GREEN,0,0);
  }
  psAbntS=s_sSelectors[PultNb].pMassAbnt;
  for(btInd=0;btInd<s_sSelectors[PultNb].btCnt;btInd++,psAbntS++)
  {
    if((psAbntS->Status & 0x70)==d_AbntSelectorStatusBusy)
    {
      if(IsFreeAbonentFromKey(PultNb,psAbntS->Key,btLayer))
      { SetAbonentStatus2Selector(PultNb,psAbntS,d_AbntSelectorStatusWait); }
    }
    if(bShowAll) psAbntS->Status&=~d_AbntSelectorStatusShow;
    if(psAbntS->Status & d_AbntSelectorStatusShow) continue;
    btKey=psAbntS->Key;
    btLedState=OFF;
    if(psAbntS->Status & d_AbntSelectorDisableOut)
    {
      btPisk=1;
      btLedState=dLedStateDisableOut;
    }
    else
    {
      btPisk=0;
      switch(psAbntS->Status)
      {
        case StatusOff:
          psAbntS->Key=0xff;
          psAbntS->btSelectorCtrl=0;
          btLedState=OFF;
          break;
        case StatusLeader: // �������
          btLedState=RED;
          break;
        case StatusMember: // ��������
          btLedState=GREEN;
          break;
        case StatusListener: // ���������
          btLedState=ORANGE;
          break;
        default: break;
      }
      switch(psAbntS->Status & 0x70)
      {
        case d_AbntSelectorStatusCall:
          btLedState=BLINK_GREEN;
          btPisk=1;
          break;
        case d_AbntSelectorStatusErr:
          btLedState=FAST_BLINK_RED;
          break;
        case d_AbntSelectorStatusWait:
        case d_AbntSelectorStatusDel:
        case d_AbntSelectorStatusBusy:
          if((s_sSelectors[PultNb].btState & 0xf)==d_SelectorWork) btPisk=1;
          btLedState=BLINK_RED;
          break;
        case d_AbntSelectorTalkOff:
          btLedState=ORANGE;
          break;
        case d_AbntSelectorTalkOn:
          btLedState=FAST_BLINK_GREEN;
          break;
      }   
    }
    if(psAbntS->btSelectorCtrl & (d_SelectorListener_Bad | d_SelectorIsdnAbnt_NoTalk)) btLedState=FAST_BLINK_ORANGE;
    if(bShowAll) btPisk=0;
    if(btKey!=0xff)
    {
      ABONENT_STATE_TP *psAbnt=GetAbonentStatePtrFromKey(PultNb,btLayer,btKey); 

      if(psAbntS->btPisk) btPisk=1;
      psAbntS->btPisk=0;
      IndicCmd(PultNb,btLayer,btKey,btLedState,btPisk,btPisk);
      if(psAbnt && (psAbnt->btKeyATCDirect!=0xff)) IndicCmd(PultNb,btLayer,psAbnt->btKeyATCDirect,btLedState,0,0);
    }
    psAbntS->Status|=d_AbntSelectorStatusShow;
  }
}
        
void AutomaticCallAbonent4Selector(unsigned char PultNb,bool bAll)
{
  unsigned char i,btLayer,btKey;
  SELECTOR_ABNT_TP *psAbnt;

  vTaskDelay(100);
  SetAutomaticCall(PultNb,true);
  btLayer=s_sSelectors[PultNb].btLayer;
  psAbnt=s_sSelectors[PultNb].pMassAbnt;
  for(i=0;i<s_sSelectors[PultNb].btCnt;i++,psAbnt++)
  {
    btKey=psAbnt->Key;
    if(btKey!=0xff)
    {
      switch(MassKey[PultNb][btLayer][btKey].TypeAbon)
      {
        case IPPM:
        case ISDN:
          if(bAll ||
             ((psAbnt->btSelectorCtrl & d_SelectorIsdnAbnt_NoTalk) && (GetAbonentStateFromKey(PultNb,btKey,btLayer)!=Call)))
          {
            if(MassKey[PultNb][btLayer][btKey].TypeAbon==ISDN) OutgoingCallISDN(PultNb,btKey,btLayer);
            else OutgoingCallIPPM(PultNb,btKey,btLayer,false);
            if(!bAll)
            { // ���� ����� ��������� - ��������� ����� ������������ ISDN-��������� (��� ��� ������� �� ���������� ���������!)
              AddAbonent2Selector(PultNb,btKey,btLayer);
            }
            SetAbonentStatus2Selector(PultNb,psAbnt,d_AbntSelectorTalkOff);
            psAbnt->btSelectorCtrl=d_SelectorIsdnAbnt_NoTalk;
          }
          break;
        case Selector:
          if(bAll)
          {
            OutgoingCallSelector(PultNb,btKey,btLayer);
            SetAbonentStatus2Selector(PultNb,psAbnt,d_AbntSelectorTalkOff);
          }
          break;
      }
    }
  }
  SetAutomaticCall(PultNb,false);
}

bool LoadSelector2Mem(unsigned char PultNb)
{
  unsigned char btLayer,btCnt,btKey,TypeAbon;
  HANDLE hFile;
  SELECTOR_ABNT_TP *pMassAbnt;
  bool bTest=IsPressTest(PultNb);
  static char FName[30];
  static FileAbntSelectorHead_TP sAbntHeadSel;
  static FileAbntSelector_TP sAbntSel;

  if(bTest) s_sSelectors[PultNb].btState=d_SelectorTest;
  btLayer=s_sSelectors[PultNb].btLayer;
  pMassAbnt=s_sSelectors[PultNb].pMassAbnt;
  memset(pMassAbnt,0,MaxAbonentSelector*sizeof(SELECTOR_ABNT_TP));
  for(btCnt=0;btCnt<MaxAbonentSelector;btCnt++) pMassAbnt[btCnt].Key=0xff;
  LockPrintf();
  sprintf(FName,"Pult%02dSc%02d.dts",PultNb,s_sSelectors[PultNb].SelectorNb);
  UnlockPrintf();
//SendStr2IpLogger("LoadSelector2Mem: FName=%s",FName);
  hFile=CreateFile(FName,GENERIC_READ,0);
  if(hFile<0) return(false);
  if(ReadFile(hFile,&sAbntHeadSel,sizeof(FileAbntSelectorHead_TP))!=sizeof(FileAbntSelectorHead_TP))
  { CloseHandle(hFile); return(false); }
  s_sSelectors[PultNb].wScenNb=sAbntHeadSel.wScenNb;
  s_sSelectors[PultNb].wOperator=GetOperatorChannelFromCode(sAbntHeadSel.wOperator);
  if(!s_sSelectors[PultNb].wOperator)
  { CloseHandle(hFile); return(false); }
//SendStr2IpLogger("LoadSelector2Mem: wScenNb=%d, wOperator=%x",s_sSelectors[PultNb].wScenNb,s_sSelectors[PultNb].wOperator);
  btCnt=0;
  while(ReadFile(hFile,&sAbntSel,sizeof(FileAbntSelector_TP))==sizeof(FileAbntSelector_TP))
  {
    btKey=sAbntSel.Key;
    if(btKey!=0xff)
    {
      TypeAbon=MassKey[PultNb][btLayer][btKey].TypeAbon;
      if((MassKey[PultNb][btLayer][btKey].CodeKey[0]==CallKey) &&
         ((TypeAbon==TDC) || (TypeAbon==ISDN) || (TypeAbon==Selector) || (TypeAbon==IPPM)))
      {
        pMassAbnt[btCnt].Key=btKey;
        pMassAbnt[btCnt].Status=sAbntSel.Status;
        btCnt++;
        if(btCnt>=MaxAbonentSelector) break;
      }
    }
  }
  CloseHandle(hFile);
  if(!btCnt) return(false);
  s_sSelectors[PultNb].btCnt=btCnt;
  LoadAbonentSelectorName(PultNb,NULL);
  for(btCnt=0;btCnt<s_sSelectors[PultNb].btCnt;btCnt++)
  {
//SendStr2IpLogger("LoadSelector2Mem:Key=%d, Name[0]=%02x",pMassAbnt[btCnt].Key,pMassAbnt[btCnt].Name[0]);
    if(pMassAbnt[btCnt].Name[0]==0) CreateNameAbntSelector(PultNb,pMassAbnt+btCnt);
    pMassAbnt[btCnt].Status&=0x07;
    if(bTest) continue;
    if(IsKey2Selector(PultNb,pMassAbnt[btCnt].Key,btLayer,d_Abonent2SelectorOtherPult))
    {
SendStr2IpLogger("LoadSelector2Mem: Busy Key=%d, Name=%s",pMassAbnt[btCnt].Key,pMassAbnt[btCnt].Name);
      s_sSelectors[PultNb].btState=d_SelectorBusy | d_SelectorError;
      pMassAbnt[btCnt].Status|=d_AbntSelectorStatusErr;
    }
    else
    {
      if(!IsFreeAbonentFromKey(PultNb,pMassAbnt[btCnt].Key,btLayer)) pMassAbnt[btCnt].Status|=d_AbntSelectorStatusBusy;
      else pMassAbnt[btCnt].Status|=d_AbntSelectorStatusWait;
    }
  }
  // ����������� ����� ��� ����������� ������������ � ��������� ���������
  if(s_sSelectors[PultNb].btCnt+d_MaxAddAbnt2Selector<MaxAbonentSelector) s_sSelectors[PultNb].btCnt+=d_MaxAddAbnt2Selector;
  else s_sSelectors[PultNb].btCnt=MaxAbonentSelector;
  if(bTest) return(true);
  if(s_sSelectors[PultNb].btState & d_SelectorError)
  {
    for(btCnt=0;btCnt<s_sSelectors[PultNb].btCnt;btCnt++)
    {
      if((pMassAbnt[btCnt].Status & 0x70)!=d_AbntSelectorStatusErr)
      {
        pMassAbnt[btCnt].Key=0xff;
        pMassAbnt[btCnt].Status=StatusOff;
      }
    }
  }
  SendAllAbonentSelector2IpPult(PultNb);
  if(s_sSelectors[PultNb].btState & d_SelectorError) return(false);
  AutomaticCallAbonent4Selector(PultNb,true);
  if(CreateCommute4Selector(PultNb))
  {
    LoadSoundForSelector(MassM1[PultNb],s_sSelectors[PultNb].SelectorNb);
    return(true);
  }
SendStr2IpLogger("LoadSelector2Mem: CreateCommute4Selector error (PultNb=%d)!",PultNb);
  s_sSelectors[PultNb].btState=d_SelectorCreateCommuteErr;
  return(false);
}

void EndSelectorFn(unsigned char PultNb)
{
  unsigned char btRes;
  unsigned short wNChan;

  wNChan=GetSelectorConnectionPoint(MassM1[PultNb]);
  if(wNChan!=0xff)
  {
/*    btRes=PlaySound2ConnectionPoint(wNChan,"EndSelect.wav");
    if(btRes) wNChan=0xff;
    else
    { // !!! ������ - ���� ���� ��������� ��������� �������� �� ������ ������� �������� ����� ��������� ��������� !!!
      s_sSelectors[PultNb].wCP=wNChan;
    }*/
    wNChan=0xff;
  }
  if(wNChan==0xff) DestroySelector(PultNb);
}

void PressSelectorN(unsigned char PultNb,unsigned char btLayer,unsigned char btKey,unsigned char SelectorNb,bool bOff)
{
  unsigned char btRes=0xff;
  unsigned short wNChan;

SendStr2IpLogger("$SELECTOR$PressSelectorN: PultNb=%d, Key=%d",PultNb,btKey);
  if(IsConferenceActive(PultNb)) return;
  if(bOff)
  {
    if(!s_sSelectors[PultNb].btState || (SelectorNb!=s_sSelectors[PultNb].SelectorNb)) return;
    EndSelectorFn(PultNb);
  }
  else
  {
    if(s_sSelectors[PultNb].btState)
    { // �������� ��� ������
      if((s_sSelectors[PultNb].btState & 0x7)==d_SelectorWork)
      {
        if(s_sSelectors[PultNb].btState & d_SelectorAllMember)
        {
          s_sSelectors[PultNb].btState&=~d_SelectorAllMember;
          SetAllAbonentSelector2Member(PultNb,0);
          IndicCmd(PultNb,btLayer,btKey,GREEN,1,1);
SendStr2IpLogger("$SELECTOR$PressSelectorN: Set all to prev state! (PultNb=%d, Key=%d)",PultNb,btKey);
        }
        else
        {
          if(IsPressChoice(PultNb))
          {
            s_sSelectors[PultNb].btState|=d_SelectorAllMember;
            SetAllAbonentSelector2Member(PultNb,StatusMember);
            IndicCmd(PultNb,btLayer,btKey,FAST_BLINK_GREEN,1,1);
SendStr2IpLogger("$SELECTOR$PressSelectorN: Set all to MEMBER! (PultNb=%d, Key=%d)",PultNb,btKey);
          }
          else IndicCmd(PultNb,btLayer,btKey,NO_INDIC,3,1);
        }
      }
      else
      { // �������� ��� ��� ������� �� ���������� ���������
SendStr2IpLogger("$SELECTOR$PressSelectorN: Call not answer abonents (PultNb=%d, Key=%d)",PultNb,btKey);
        AutomaticCallAbonent4Selector(PultNb,false);
        IndicCmd(PultNb,btLayer,btKey,NO_INDIC,3,1);
      }
    }
    else
    { // �������� ���������
      DelFreeHandConf(PultNb);
      DeleteAllConnections2Pult(PultNb);
      s_sSelectors[PultNb].PultNb=PultNb;
      s_sSelectors[PultNb].btLayer=btLayer;
      s_sSelectors[PultNb].btKey=btKey;
      s_sSelectors[PultNb].SelectorNb=SelectorNb;
      s_sSelectors[PultNb].btState=d_SelectorLoad;
      s_sSelectors[PultNb].dwTOut=0;
      s_sSelectors[PultNb].dwListenerTOut=0;
      s_sSelectors[PultNb].pMassAbnt=pvPortMalloc(MaxAbonentSelector*sizeof(SELECTOR_ABNT_TP));
      if(!s_sSelectors[PultNb].pMassAbnt || !LoadSelector2Mem(PultNb))
      {
SendStr2IpLogger("$SELECTOR$PressSelectorN: Create selector ERROR (PultNb=%d, Key=%d)",PultNb,btKey);
        if(!s_sSelectors[PultNb].pMassAbnt) s_sSelectors[PultNb].btState=d_SelectorNoMemory;
        s_sSelectors[PultNb].dwTOut=MassTimer[TIMER_SELECTOR]+xTaskGetTickCount();
        s_sSelectors[PultNb].btState|=d_SelectorError;
        if(MassM1[PultNb]<16) IndicCmd(PultNb,btLayer,btKey,FAST_BLINK_RED,3,1);
        else IndicCmd(PultNb,btLayer,btKey,s_sSelectors[PultNb].btState,0,0);
      }
      else
      {
        if(s_sSelectors[PultNb].btState!=d_SelectorTest)
        {
          s_sSelectors[PultNb].btState=d_SelectorWait;
          EnableAllOut(PultNb);
SendStr2IpLogger("$SELECTOR$PressSelectorN: Create selector OK (PultNb=%d, Key=%d)",PultNb,btKey);
        }
        if(MassM1[PultNb]<16) IndicCmd(PultNb,btLayer,btKey,BLINK_RED_GREEN,1,1);
        else IndicCmd(PultNb,btLayer,btKey,s_sSelectors[PultNb].btState,0,0);
      }
    }
  }
}

void Key_BeginSelector(unsigned char PultNb,unsigned char btLayer,unsigned char Key)
{ // ���.���������
  unsigned char btInd,btCnt,btStatus;
  ABONENT_STATE_TP *psAbnt;
  SELECTOR_ABNT_TP *pMassAbnt;

SendStr2IpLogger("$SELECTOR$Key_BeginSelector: PultNb=%d, Key=%d, SelectorState=%d",PultNb,Key,s_sSelectors[PultNb].btState);
  if((s_sSelectors[PultNb].btState!=d_SelectorWait) || (s_sSelectors[PultNb].btLayer!=btLayer)) return;
  pMassAbnt=s_sSelectors[PultNb].pMassAbnt;
  for(btInd=0,btCnt=0;btInd<s_sSelectors[PultNb].btCnt;btInd++)
  {
    btStatus=pMassAbnt[btInd].Status & 0x70;
    if((btStatus==d_AbntSelectorTalkOn) || (btStatus==d_AbntSelectorTalkOff))
    {
      if(pMassAbnt[btInd].Status & d_AbntSelectorDisableOut)
      {
        psAbnt=GetAbonentStatePtrFromKey(PultNb,pMassAbnt[btInd].Key,btLayer);
        if(psAbnt && (psAbnt->wConnectionPoint<=48))
        { EnOutAbonentSelector(PultNb,pMassAbnt+btInd,psAbnt,true); }
      }
      if(SetAbnt2SelectorStatus(PultNb,pMassAbnt[btInd].Key,btLayer))
      {
        btCnt++;
        vTaskDelay(10);
      }
    }
  }
  if(!btCnt)
  {
SendStr2IpLogger("$SELECTOR$Key_BeginSelector: no answer abonents for selector(PultNb=%d) OK!",PultNb);
    PutErrInfo2Display(PultNb,"��� ���������� ���������!");
  }
  else
  {
    s_sSelectors[PultNb].btState=d_SelectorWork;
    IndicCmd(PultNb,s_sSelectors[PultNb].btLayer,s_sSelectors[PultNb].btKey,GREEN,1,1);
    s_sSelectors[PultNb].btKeyBeginSelector=Key;
    IndicCmd(PultNb,s_sSelectors[PultNb].btLayer,s_sSelectors[PultNb].btKeyBeginSelector,GREEN,0,0);
SendStr2IpLogger("$SELECTOR$Key_BeginSelector: BeginSelector (PultNb=%d, Key=%d) OK!",PultNb,Key);
  }
}

