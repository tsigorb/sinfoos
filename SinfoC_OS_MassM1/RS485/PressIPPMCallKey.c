#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

void OutgoingCallIPPM(unsigned char PultNb,unsigned char Key,unsigned char btLayer,bool bTest)
{
  unsigned char Numb=MassKey[PultNb][btLayer][Key].NumbAbon;
  bool bMCall=!IsAutomaticCall(PultNb);
  ABONENT_STATE_TP *psAbnt;

  if((Numb<1) || (Numb>d_MaxAbntIPPM))
  {
    if(bMCall) IndicCmd(PultNb,btLayer,Key,NO_INDIC,3,3);
    return;
  }
  if(bMCall)
  {
    // !!!��������� �� ����.����� ������������ ��������� ��������� IPPM
    /*if()
    { IndicCmd(PultNb,btLayer,Key,NO_INDIC,3,3); return; }*/
  }
  psAbnt=GetFreeAbonentStatePtr();
  if(psAbnt)
  {
    psAbnt->btPultNb=PultNb;
    psAbnt->btLayer=btLayer;
    psAbnt->btKey=Key;
    psAbnt->wOperatorChanel=GetOperatorChannelFromKey(PultNb,Key,btLayer);
    psAbnt->NumbAbon=Numb;
    psAbnt->btType=IPPM;
    if(bTest)
    {
      psAbnt->wConnectionPoint=d_DummyCP;
      SetAbonentStateByPtr(psAbnt,CallTest,0);
      if(IsRegisterAbonentIPPM(Numb)) psAbnt->btLedState=BLINK_GREEN;
      else psAbnt->btLedState=BLINK_RED;
      IndicCmd(PultNb,btLayer,Key,psAbnt->btLedState,1,1);
      psAbnt->dwTimerEnd=3000+xTaskGetTickCount();
SendStr2IpLogger("$DEBUG$OutgoingCallIPPM Test: Numb=%d",Numb);
      return;
    }
    if(!IsFreeAbonentFromKey(PultNb,Key,btLayer) || !IsRegisterAbonentIPPM(Numb))
    {
SendStr2IpLogger("$DEBUG$OutgoingCallIPPM: Numb=%d not free or not register!",Numb);
      if(bMCall) IndicCmd(PultNb,btLayer,Key,NO_INDIC,3,3);
      return;
    }
//SendStr2IpLogger("$DEBUG$OutgoingCallIPPM: Numb=%d, bmCall=%d",Numb,bMCall);
    psAbnt->btLedState=dLedStateOutgoingCall;
    if(bMCall)
    { // �������������� ����� �������� "CONNECT_PRIVATE cmd"
      unsigned short wCP=GetFreeConnectionPoint();
SendStr2IpLogger("$DEBUG$OutgoingCallIPPM CONNECT_PRIVATE: Numb=%d, wCp=%x",Numb,wCP);
      if(wCP==0xff)
      {
        IndicCmd(PultNb,btLayer,Key,NO_INDIC,3,3);
        return;
      }
      psAbnt->wConnectionPoint=wCP;
      SetAbonentStateByPtr(psAbnt,Call,0);
      SetCurrentAbonentIPPM(Numb,psAbnt);
      IndicCmd(PultNb,psAbnt->btLayer,Key,psAbnt->btLedState,1,1);
      SendIPPMCmdConnectPrivate(Numb);
    }
    else
    { // �������������� ����� ��������� ��������� "CONNECT cmd"
      unsigned short VMCP=CreatVMCP(MassM1[PultNb]);
SendStr2IpLogger("$DEBUG$OutgoingCallIPPM CONNECT: Numb=%d, VMCP=0x%x",Numb,VMCP);
      if(!VMCP) return;
      psAbnt->wConnectionPoint=VMCP;
      SetAbonentStateByPtr(psAbnt,Call,0);
      SetCurrentAbonentIPPM(Numb,psAbnt);
      IndicCmd(PultNb,psAbnt->btLayer,Key,psAbnt->btLedState,0,0);
      SendIPPMCmdConnect(Numb,psAbnt);
    }
    psAbnt->dwTimerEnd=MassTimer[TIMER_OUTGOINGCALL]+xTaskGetTickCount();
  }
}

void AnswerIncomingCallIPPM(ABONENT_STATE_TP *psAbnt)
{
  unsigned short wCP=GetFreeConnectionPoint();
SendStr2IpLogger("$DEBUG$AnswerIncomingCallIPPM: Numb=%d, wCP=%x",psAbnt->NumbAbon,wCP);
  if(wCP==0xff)
  {
    IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,NO_INDIC,3,3);
    return;
  }
  if(IsMaxCountSpeakIPPM2UK())
  {
    IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,NO_INDIC,3,3);
    return;
  }
  psAbnt->wConnectionPoint=wCP;
  psAbnt->dwTimerEnd=0;
  psAbnt->btLedState=dLedStateTalk;
  SetAbonentStateByPtr(psAbnt,Talk,0);
  SetCurrentAbonentIPPM(psAbnt->NumbAbon,psAbnt);
  IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,1,1);
  ClearIncomingCallIPPM4AllPults(psAbnt->NumbAbon);
  Commute(true,psAbnt->wOperatorChanel,psAbnt->wConnectionPoint);
  Commute(true,psAbnt->wConnectionPoint,psAbnt->wOperatorChanel);
  StartAudioFromMU2IPPM_Private(psAbnt->NumbAbon,psAbnt->wConnectionPoint);
  StartAudioFromIPPM2MU(psAbnt->wConnectionPoint);
  DSRS_On(psAbnt->btPultNb,psAbnt->wOperatorChanel);
  SendIPPMCmdConnectPrivate(psAbnt->NumbAbon);
}

void PressCallKey_IPPM(unsigned char PultNb,unsigned char Key,unsigned char btLayer,bool bTest)
{
  ABONENT_STATE_TP *psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);

SendStr2IpLogger("$DEBUG$PressCallKey_IPPM: PultNb=%d, Key=%d, psAbnt=%x",PultNb,Key,psAbnt);
  if(IsKey2Selector(PultNb,Key,btLayer,d_Abonent2SelectorOtherPult))
  { PutErrInfo2Display(PultNb,NULL); return; }
  if(!psAbnt) OutgoingCallIPPM(PultNb,Key,btLayer,bTest);
  else
  {
    switch(GetAbonentStateFromPtr(psAbnt))
    {
      case IncomingCall:
      {
        AnswerIncomingCallIPPM(psAbnt);
        break;
      }
      case Talk:
      {
        if(IsSelectorActive(PultNb))
        {
          if((psAbnt->wConnectionPoint<d_DummyCP) && IsPressAddMember(PultNb))
          {
            unsigned short wCP=CreatVMCP(MassM1[PultNb]);
SendStr2IpLogger("$DEBUG$PressCallKey_IPPM add to selector: PultNb=%d, Key=%d, wCP=%x (VMCP=%x)",PultNb,Key,psAbnt->wConnectionPoint,wCP);
            if(wCP)
            {
              Commute(false,psAbnt->wConnectionPoint,psAbnt->wOperatorChanel);
              StopAudioFromMU2IPPM(psAbnt->wConnectionPoint);
              StopAudioFromIPPM2MU(psAbnt->wConnectionPoint);
              DelConnectionPoint(psAbnt->wConnectionPoint);
              psAbnt->wConnectionPoint=wCP;
              if(AddAbonent2Selector(PultNb,Key,btLayer)) SendIPPMCmdConnect(psAbnt->NumbAbon,psAbnt);
            }
          }
        }
        break;
      }
      default: break;
    }
  }
}

