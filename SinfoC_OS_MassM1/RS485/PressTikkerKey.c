#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

/////////////////////////////////////////////////////////////////////
static unsigned char MasTikkerKey[NumberKeyboard];

/////////////////////////////////////////////////////////////////////
unsigned char GetTikkerState(unsigned char PultNb)
{ return(MasTikkerKey[PultNb]?d_ServiceOn:Off); }

bool _xCmdSetTikkerState(unsigned char PultNb,bool bOn)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485;
  sCmd.L=4;
  sCmd.A1=0;
  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[1]=140;
  sCmd.BufCommand[2]=MassM1[PultNb];
  sCmd.BufCommand[3]=bOn;
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(WaitReply(11,140)) return(true);
  }
  return(false);
}

void SetTikkerKeyOff(unsigned char PultNb)
{
  if(MasTikkerKey[PultNb])
  {
    _xCmdSetTikkerState(PultNb,false);
    IndicCmd(PultNb,GetCurrentLayer(PultNb),MasTikkerKey[PultNb],OFF,0,0);
    MasTikkerKey[PultNb]=0;
  }
}

void PressTikkerKey(unsigned char PultNb,unsigned char btLayer,unsigned char Key)
{
  if(MasTikkerKey[PultNb])
  {
    if(MasTikkerKey[PultNb]!=Key) return;
    if(!_xCmdSetTikkerState(PultNb,false)) return;
    MasTikkerKey[PultNb]=0;
    IndicCmd(PultNb,btLayer,Key,OFF,1,1);
  }
  else
  {
    if(!_xCmdSetTikkerState(PultNb,true)) return;
    MasTikkerKey[PultNb]=Key;
    IndicCmd(PultNb,btLayer,Key,ORANGE,1,1);
  }
}

