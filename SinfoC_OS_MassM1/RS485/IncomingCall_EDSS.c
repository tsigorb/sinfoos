#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

//*******************************************************************
bool IsExistFreeHandConf(unsigned char PultNb);

/////////////////////////////////////////////////////////////////////
#define d_BUSYTOUT  3000

extern unsigned char E1_EDSS;

const char *strAnonim="����� �� ���������";

/////////////////////////////////////////////////////////////////////
void OtboiEDSS(unsigned short wNChan,unsigned short wChanOper)
{
  ABONENT_STATE_TP *psAbnt;

  if(IsCrossCommuteAbnt(wNChan))
  { CrossCommuteOtboi(wNChan); return; }
  psAbnt=GetAbonentStatePtrByFind(0xff,wNChan,wChanOper,0xff,0xff,NULL);
  if(!psAbnt) return;
  if(RepeatOugoingCall(psAbnt)) return;
  if(IsKey2Selector(psAbnt->btPultNb,psAbnt->btKey,psAbnt->btLayer,d_Abonent2SelectorCurrentPult) ||
     IsPultAutoAnswer(psAbnt->btPultNb)) OtboiAbonent(psAbnt->btPultNb,psAbnt->btKey,psAbnt->btLayer);
  else psAbnt->dwTimerEnd=d_BUSYTOUT+xTaskGetTickCount();
}

void FreeAbntOutgoingCallEDSS(unsigned short wNChan)
{
  unsigned char btInd;
  ABONENT_STATE_TP *psAbnt;

  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return;
    btInd++;
    if(psAbnt->wConnectionPoint==wNChan)
    {
      psAbnt->btOutgoingCnt=0xff; // ��������� ����� �������� �� ��������� ����� ���!
      return;
    }
  }
  while(1);
}

void Busy_EDSS(unsigned short wNChan)
{
  unsigned char btInd;
  ABONENT_STATE_TP *psAbnt;

  if(IsCrossCommuteAbnt(wNChan))
  { CrossCommuteOtboi(wNChan); return; }
  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return;
    btInd++;
    if(psAbnt->wConnectionPoint==wNChan)
    {
      if(RepeatOugoingCall(psAbnt)) return;
      if(IsKey2Selector(psAbnt->btPultNb,psAbnt->btKey,psAbnt->btLayer,d_Abonent2SelectorCurrentPult) ||
         IsPultAutoAnswer(psAbnt->btPultNb)) OtboiAbonent(psAbnt->btPultNb,psAbnt->btKey,psAbnt->btLayer);
      else psAbnt->dwTimerEnd=d_BUSYTOUT+xTaskGetTickCount();
      return;
    }
  }
  while(1);
}

void GetNumberFromAddr(char *pchBuf,unsigned char *pParam)
{
  int i;
  unsigned int dwNumb;

  dwNumb=pParam[0];
  dwNumb=(dwNumb<<8) | pParam[1];
  dwNumb=(dwNumb<<8) | pParam[2];
  dwNumb=(dwNumb<<8) | pParam[3];
  LockPrintf();
  sprintf(pchBuf,"%d",dwNumb);
  UnlockPrintf();
}

void GetNameFromAddr(char *pchBuf,unsigned char *pParam,char iMaxLen)
{
  int i;
  unsigned int dwAddr;

  dwAddr=pParam[0];
  dwAddr=(dwAddr<<8) | pParam[1];
  dwAddr=(dwAddr<<8) | pParam[2];
  dwAddr=(dwAddr<<8) | pParam[3];
  i=0;
  pParam=(unsigned char *)dwAddr;
  if(pParam)
  {
    for(;(i<iMaxLen) && pParam[i];i++) pchBuf[i]=pParam[i];
  }
  pchBuf[i]=0;
}

void Byte2Str(unsigned char btData0,char *pBuf)
{
  unsigned char btChastnoe;
  
  btChastnoe=btData0 / 100;
  btData0=btData0 % 100;
  if(btChastnoe) *pBuf++=btChastnoe+'0';
  btChastnoe=btData0 / 10;
  btData0=btData0 % 10;
  if(btChastnoe) *pBuf++=btChastnoe+'0';
  *pBuf++=btData0+'0';
  *pBuf=0;
}

/////////////////////////////////////////////////////////////////////
static char NumberSrs[21],NumberDst[21],NameSrs[21];

unsigned char GetCallingPultNumb(char *strNumb)
{
  unsigned char i,Ln;
  char Tlfn[25];
  
  Ln=Str2Tlfn((unsigned char *)Tlfn,strNumb);
  if(Ln)
  {
    for(i=0;i<NumberKeyboard;i++)
    {
      if(MassM1[i])
      {
        Tlfn2Str(Tlfn,GetPultTlfnPtr(i));
        if(Tlfn[0]=='0') strcpy(Tlfn,Tlfn+1);
        if(!strcmp(Tlfn,strNumb)) return(i);
      }
    }
  }
  return(0xff);
}

bool _xIncomingCallISDNFree(unsigned short wNChan,unsigned short wChanOper,unsigned char btNoKPV)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485;    // �������� ������� RS485
  sCmd.L=7;            // ���������� ���� � �������
  sCmd.A1=0;
  sCmd.BufCommand[0]=10;    // �������
  sCmd.BufCommand[1]=202;   // ���������� - ���������� ������� ��������
  sCmd.BufCommand[2]=wNChan >> 8; // N �����������
  sCmd.BufCommand[3]=wNChan;      // N �����������
  sCmd.BufCommand[4]=wChanOper >> 8; // ����� ���������
  sCmd.BufCommand[5]=wChanOper;      // ����� ���������
  if(btNoKPV) sCmd.BufCommand[6]=0;
  else sCmd.BufCommand[6]=1;
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(WaitReply(11,202)) return(true);
    CountErrEDSS++;
  }
  return(false);
}

bool CallFromKeyAbonent(unsigned char PultNb,unsigned short wNChan,unsigned short wOper,
                        unsigned char btKey,ABONENT_STATE_TP *psAbnt)
{
  unsigned char btCntAbntPult,btGroup,
                btLayer=GetCurrentLayer(PultNb);
  bool bAbnt2Selector,bAbnt2AA;

  psAbnt->btKey=btKey;
  if(FindFirstAbonentIndex(PultNb,0xffff,0xffff,0xff,0xff,NULL)!=0xff) btCntAbntPult=1;
  else btCntAbntPult=0;
  bAbnt2Selector=IsKey2Selector(PultNb,btKey,btLayer,d_Abonent2SelectorCurrentPult);
  btGroup=GetTlfnGroup(psAbnt->NumbAbon);
  bAbnt2AA=IsGroupAutoAnswer4PultFromGroup(PultNb,btGroup);
  if(!_xIncomingCallISDNFree(wNChan,wOper,bAbnt2Selector || bAbnt2AA)) return(false);
  if(bAbnt2AA || bAbnt2Selector)
     //|| ExistTalk2Group(PultNb,btGroup))
  {
    psAbnt->wConnectionPoint=wNChan;
    psAbnt->wOperatorChanel=wOper;
    AnswerIncomingCallISDN(PultNb,psAbnt,0);
    if(bAbnt2Selector) AddAbonent2Selector(PultNb,btKey,btLayer);
    else
    {
      AutoAnswerSignal(wNChan);
      bAbnt2AA=IsExistFreeHandConf(PultNb);
      AddAbnt2FreeHandConf(PultNb,psAbnt);
      if(!bAbnt2AA) AutoAnswerSignal(wOper);
    }
    return(false);
  }
  return(true);
}

void PutIncomingCallInfo2Display(unsigned char PultNb,unsigned char btInd)
{
  char *pstrAbnt,Ln;

  if(GetCountUknownISDNAbonent(PultNb)) return;
  ClearScreen(PultNb);
  OutputString(PultNb,"�������� �����",0,3,0);
  if(NameSrs[0]) pstrAbnt=NameSrs;
  else
  {
    if(NumberSrs[0]) pstrAbnt=NumberSrs;
    else pstrAbnt=(char *)strAnonim;
  }
  ListAbntDisplay[PultNb][0].NChan=btInd;
  Ln=strlen(pstrAbnt);
  if(Ln>18) Ln=18;
  memcpy(ListAbntDisplay[PultNb][0].StrInf,pstrAbnt,Ln);
  ListAbntDisplay[PultNb][0].StrInf[Ln]=(char)0xBB;
  ListAbntDisplay[PultNb][0].StrInf[Ln+1]=0;
  OutputString(PultNb,ListAbntDisplay[PultNb][0].StrInf,0,3,1);
}

bool CallFromUnknownAbonent(unsigned char PultNb,unsigned short wNChan,unsigned short *pwOper,
                            char *NameSrs,ABONENT_STATE_TP *psAbnt)
{
  unsigned char btCntAbntPult,btKey,btLayer,btGroup;
  bool bAbnt2AA;
  
  btKey=FindFreeISDNKey(PultNb,&btLayer);
  if(btKey==0xff)
  {
    SendCmdOtboiISDN(PRI_CAUSE_USER_BUSY,wNChan,*pwOper);
    return(false);
  }
  if(FindFirstAbonentIndex(PultNb,0xffff,0xffff,0xff,0xff,NULL)!=0xff) btCntAbntPult=1;
  else btCntAbntPult=0;
  psAbnt->btKey=btKey;
  psAbnt->btLayer=btLayer;
  *pwOper=GetOperatorChannelFromKey(PultNb,btKey,btLayer);
  btGroup=GetTlfnGroup(psAbnt->NumbAbon);
  bAbnt2AA=IsGroupAutoAnswer4PultFromGroup(PultNb,btGroup);
  if(!_xIncomingCallISDNFree(wNChan,*pwOper,bAbnt2AA)) return(false);
  PutIncomingCallInfo2Display(PultNb,GetAbonentStateIndexFromPtr(psAbnt));
  if(bAbnt2AA)
  {
    psAbnt->wConnectionPoint=wNChan;
    psAbnt->wOperatorChanel=*pwOper;
    AnswerIncomingCallISDN(PultNb,psAbnt,0);
    AutoAnswerSignal(wNChan);
    bAbnt2AA=IsExistFreeHandConf(PultNb);
    AddAbnt2FreeHandConf(PultNb,psAbnt);
    if(!bAbnt2AA) AutoAnswerSignal(*pwOper);
    return(false);
  }
  return(true);
}

bool IsPultCalling(char *NumbSrs)
{
  int i;
  static char NumbPult[25];

  for(i=0;i<NumberKeyboard;i++)
  {
    if(MassM1[i])
    {
      Tlfn2Str(NumbPult,GetPultTlfnPtr(i));
      if(!strcmp(NumbPult,NumbSrs)) return(true);
    }
  }
  return(false);
}

unsigned char FindNumbAbonFromTlfn(unsigned char *btTlfn)
{
  unsigned char btInd;

  for(btInd=0;btInd<NumberDirectAbon;btInd++)
  {
    if(TlfnEqual(MassTel[btInd],btTlfn)) return(btInd+1);
  }
  return(0xff);
}

void IncomingCall_EDSS(unsigned short wNChan,sInfoKadr *psVxCall)
{
  unsigned char PultNb,btKeyAbnt,btLayer,Ln;
  unsigned char Tlfn[20];
  unsigned short wOper;
  ABONENT_STATE_TP *psAbnt;
  bool bRes;
  
//SendStr2IpLogger("$DEBUG$�������� ����� EDSS!");
  strcpy((char *)NumberSrs,(char *)(psVxCall->ByteInfoKadr+28)); // ����� ����������� ������
  GetNameFromAddr(NameSrs,&psVxCall->ByteInfoKadr[9],18); // ��� �����������
  GetNumberFromAddr(NumberDst,&psVxCall->ByteInfoKadr[24]); // ����� �����������
  psVxCall->ByteInfoKadr[18]-=E1_EDSS; // ��������� �������� ����� ������ EDSS
  if(IsPultCalling(NumberSrs))
  { // ����� ���������� ������ ������� ��� ����� �����!
SendStr2IpLogger("$ERROR$IncomingCall_EDSS: Pult calling by another pult or yourself!");
    SendCmdOtboiISDN(PRI_CAUSE_CALL_REJECTED,wNChan,0);
    return;
  }
  PultNb=GetCallingPultNumb(NumberDst);
  if(PultNb==0xff) 
  {
SendStr2IpLogger("$ERROR$IncomingCall_EDSS: Calling pult not found!");
    if(!CrossCallISDN(wNChan,psVxCall->ByteInfoKadr[18],NumberSrs,NameSrs,NumberDst))
    { SendCmdOtboiISDN(PRI_CAUSE_CALL_REJECTED,wNChan,0); }
    return;
  }
  Ln=Str2Tlfn(Tlfn,NumberSrs);
  btKeyAbnt=FindISDNAbonentKey(PultNb,Tlfn,Ln,NULL,&btLayer);
  psAbnt=GetFreeAbonentStatePtr();
  if((!psAbnt) || (btLayer==0xff) || (!IsKbdReady(PultNb)) || IsKbdLock(PultNb))
  {
SendStr2IpLogger("$ERROR$IncomingCall_EDSS: Error \"(!psAbnt) || (btLayer==0xff) || (!IsKbdReady(PultNb)) || IsKbdLock(PultNb)\"");
    SendCmdOtboiISDN(PRI_CAUSE_CALL_REJECTED,wNChan,0);
    return;
  }
  wOper=GetOperatorChannel(PultNb,0);
  psAbnt->btPultNb=PultNb;
  psAbnt->btType=ISDN;
  psAbnt->btLayer=btLayer;
  memcpy(psAbnt->btTlfn,Tlfn,Ln);
  if(btKeyAbnt!=0xff)
  {
    psAbnt->NumbAbon=MassKey[PultNb][btLayer][btKeyAbnt].NumbAbon;
    if(GetAbonentStatePtrFromKey(PultNb,btKeyAbnt,btLayer))
    { // ����� �������� ��� EDSS ��� ��������� ������ ������!
SendStr2IpLogger("$ERROR$IncomingCall_EDSS: Otboi abonenta pri povtornom vyzove pulta!");
      OtboiAbonentISDN(PultNb,btKeyAbnt,btLayer,PRI_CAUSE_CALL_REJECTED);
      return;
    }
    wOper=GetOperatorChannelFromKey(PultNb,btKeyAbnt,psAbnt->btLayer);
    bRes=CallFromKeyAbonent(PultNb,wNChan,wOper,btKeyAbnt,psAbnt);
  }
  else
  {
    psAbnt->NumbAbon=FindNumbAbonFromTlfn(psAbnt->btTlfn);
    bRes=CallFromUnknownAbonent(PultNb,wNChan,&wOper,NameSrs,psAbnt);
  }
  if(bRes)
  {
    psAbnt->btLedState=dLedStateIncomingCall;
    psAbnt->wConnectionPoint=wNChan;
    psAbnt->wOperatorChanel=wOper;
    SetAbonentStateByPtr(psAbnt,IncomingCall,dIncomingTalkFlag);
    psAbnt->dwTimerEnd=MassTimer[TIMER_INCOMINGCALL]+xTaskGetTickCount();
    IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKey,dLedStateIncomingCall,0,0);
    SendActiveUnnameAbnt_ATC_ISDN(psAbnt);
  }
}

