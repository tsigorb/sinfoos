#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

/////////////////////////////////////////////////////////////////////
void ControlDispayTOut(void)
{
  unsigned char PultNb;

  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if(ListAbntDisplay[PultNb][0].dwTOut)
    {
      if(IsCurrentTickCountGT(ListAbntDisplay[PultNb][0].dwTOut))
      {
        ListAbntDisplay[PultNb][0].dwTOut=0;
        ListAbntDisplay[PultNb][0].NChan=ListAbntDisplay[PultNb][1].NChan=0xff;
        ListAbntDisplay[PultNb][0].StrInf[0]=ListAbntDisplay[PultNb][1].StrInf[0]=0;
        IndicClock(PultNb);
      }
    }
  }
}

void ControlPultsTOut(void)
{
  unsigned char PultNb;
  
  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if(MassM1[PultNb])
    {
      PultCallTOut(PultNb);
      CallLevelTOut(PultNb);
      CallOutSPECTOut(PultNb);
      HoldTOut(PultNb);
      OutgoingNumberTmpTOut(PultNb);
    }
  }
}

void DetectAllTimers(void)
{
  static unsigned int s_dwTOut=0;

  if(IsCurrentTickCountLT(s_dwTOut)) return;
  ControlDispayTOut();
  Control_ATC_EDSS_IPPM_TOut();
  ControlATCLevelTOut();
  ControlTDCTOut();
  ControlSelectorTOut();
  ControlPultsTOut();
  ControlTransitTOut();
  s_dwTOut=100+xTaskGetTickCount();
}

