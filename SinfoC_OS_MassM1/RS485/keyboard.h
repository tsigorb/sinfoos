#define LengthRX485         90
#define LengthTX485         90
#define NumberKeyboard      12
#define NumberGroup         3
#define NumberKeyPageSize   129
#define NumberKeyPage       2
#define NumberKey           255 // !!! �� ����� ���� > 255, ����� ��� ������������ !!!
#define MaxAbonentActive    250 //  
#define NumberDirectAbon    192
#define LengthDTMF          9
#define LengthMassDTMF      8
#define NumberGenW          48 //����. ����� ������ "�����. �����" �� ����������
#define NumberSelector      16 // ����.���������� ����������
#define d_OutgoingDigit     0xfe // ������� ���.������ ��� ���������� ������ ��� "REDIAL"
#define MAX_CONF_ABONENT    64

#define NumberTimer                 13  // ����� ��������
#define TIMER_OUTGOINGCALL          0   // ������ ���������� ������
#define TIMER_INCOMINGCALL          1   // ������ ��������� ������
#define TIMER_SPEAKLEVEL            2   // ������ ��������� �� ������
#define TIMER_SPEAKSIGNALS          3   // ������ ��������� �� ��������
#define TIMER_TESTCALL              4   // ������ ��������� ������
#define TIMER_HOLD                  5   // ������ �� ���������
#define TIMER_INDUCTOR              6   // ������ ������������ ������
#define TIMER_OUTGOINGNUMBER        7   // ������ ������������
#define TIMER_ATCDIRECT             8   // ������ ������� �������� ���
#define TIMER_FARABONENT            9   // ������ ��������� ���������� ��������
#define TIMER_GENERALCALL           10  // ������ ������������ ������
#define TIMER_GROUPCALL             11  // ������ ���������� ������
#define TIMER_SELECTOR              12  // ������ ���������

#define d_IndicTestCallResult       3000 // ����� ��������� ���������� ��������� ������

#define NumberKonf                  6   // ����. ���������� ����������� �� ������
#define d_FreeHandConferenceNumb    15
#define d_MaxAbnt2FreeHandConf      MAX_CONF_ABONENT
#define DataConfSize                (MAX_CONF_ABONENT*2)

//----------- ��������� ������� ------------------
// 1 - 16  ( ����� ������������� �� ��������� ����������� (�1��16) )
#define ATCP                17 // ���������� � ��������� ���������� ���� �� �������������� ����� 
#define Transit             105 // �������  
#define BlockKey            107 // ����������
#define Star                108 // *
#define Lattice             109 // #
#define BeginSelector       110 // ���. ���������
#define DisableEnableOut    111 // ����/��� ������ � ���������
#define EndSelector         112 // ����. ���������
#define AddMember           113 // + �������� 
#define TikkerKey           115 // ������
#define BackSpace           116 // ����� ��������� ����� ������
#define KeyOff              134 // ���������� 
#define Layer1              136 // ���� 1
#define Layer2              137 // ���� 2
#define Layer3              138 // ���� 3
#define Tabl1               139 // ������� 1
#define Tabl2               140 // ������� 2
#define Tabl3               141 // ������� 3
#define Tabl4               142 // ������� 4
#define Numeral0            144 // ����� "0"
#define Numeral1            145 // ����� "1"
#define Numeral2            146 // ����� "2"
#define Numeral3            147 // ����� "3"
#define Numeral4            148 // ����� "4"
#define Numeral5            149 // ����� "5"
#define Numeral6            150 // ����� "6"
#define Numeral7            151 // ����� "7"
#define Numeral8            152 // ����� "8"
#define Numeral9            153 // ����� "9"
#define Conference1         154 // ����������� 1
#define Conference2         155 // ����������� 2
#define Conference3         156 // ����������� 3
#define Conference4         157 // ����������� 4
#define Conference5         158 // ����������� 5
#define Conference6         159 // ����������� 6
#define Pause               160 // ����� 
//#define Interception        161 // �������� 
//#define Flash               213 // ����� ��� ���
#define Redial              162 // ������
#define TestCall            209 // �������� �����
#define Selector1           214 // �������� �1
#define Selector2           215 // �������� �2
#define Selector3           216 // �������� �3
#define Selector4           217 // �������� �4
#define Selector5           218 // �������� �5
#define Selector6           219 // �������� �6
#define Selector7           220 // �������� �7
#define Selector8           221 // �������� �8
#define Selector9           222 // �������� �9
#define Selector10          223 // �������� �10
#define Selector11          224 // �������� �11
#define Selector12          225 // �������� �12
#define Selector13          226 // �������� �13
#define Selector14          227 // �������� �14
#define Selector15          228 // �������� �15
#define Selector16          229 // �������� �16
#define Choice              231 // �����
//#define LastKey             0x80

//----------- C�������� ����� ------------------
#define OFF                     0 //�������
#define GREEN                   1 //�������
#define BLINK_GREEN             2 //�������� �������
#define RED                     3 //�������
#define BLINK_RED               4 //�������� �������
#define ORANGE                  5 //���������
#define BLINK_ORANGE            6 //�������� ���������
#define BLINK_RED_GREEN         7 //�������� ������-�������
#define BLINK_GREEN_ORANGE      8 //�������� ������-���������
#define BLINK_RED_ORANGE        9 //�������� ������-���������
#define FAST_BLINK_GREEN        10 //������ �������� �������
#define FAST_BLINK_RED          11 //������ �������� �������
#define FAST_BLINK_ORANGE       12 //������ �������� ���������
#define FAST_BLINK_GREEN_RED    13 //������ �������� ������-�������
#define FAST_BLINK_GREEN_ORANGE 14 //������ �������� ������-���������
#define FAST_BLINK_RED_ORANGE   15 //������ �������� ������-���������

#define NO_INDIC                0x7F //��� ��������� 
#define NO_SOUND                0 //��� ����� 
#define NO_LED                  0xF0 // �������������� ����� ����������

#define dLedStateIncomingCall   FAST_BLINK_RED_ORANGE
#define dLedStateOutgoingCall   BLINK_RED_ORANGE
#define dLedStateTalk           GREEN
#define dLedStateTalkIfSelector BLINK_GREEN
#define dLedStateListenSelector BLINK_ORANGE
#define dLedStateHold           FAST_BLINK_GREEN
#define dLedStatePultListen     FAST_BLINK_ORANGE
#define dLedStateDisableOut     BLINK_GREEN_ORANGE

#define dLedStateTransitAdd     ORANGE
#define dLedStateAbntTransitAdd ORANGE
#define dLedStateTransitWork    GREEN
#define dLedStateAbntTransit    GREEN
#define dLedStateTransitTest    BLINK_ORANGE
#define dLedStateTransitErr     BLINK_RED

#define Off                     0  // ������� ��������, ��������� ������� ��������

// ��������� �������
// ����� ��������_�, Conference_x
#define d_ServiceOn                 1

// ��������_�
#define d_SelectorLoad              1       // �� ������ ���� ��������� � ����� ����� ���� ������ 
#define d_SelectorWait              2       // �������� �������� - ���������� �������������
#define d_SelectorWork              3       // �������� ������� - ���������� 2-� ���������
#define d_SelectorTest              4
#define d_SelectorBusy              5       // �������� ��������� ������ ������ ����������
#define d_SelectorNoMemory          6
#define d_SelectorCreateCommuteErr  7
#define d_SelectorDestroy           8
#define d_SelectorAllMember         0x20    // ��� ��������� ��������� �������� ���������� � ��������� ��������
#define d_SelectorOperatorOff       0x40
#define d_SelectorError             0x80

// Conference_x
#define d_NoConference               0
#define d_CreateConference           1
#define d_ConferenceWithOperator     2
#define d_ConferenceWithoutOperator  3

typedef struct
{
  unsigned char btExistConf;
  unsigned char MassAbntInd[d_MaxAbnt2FreeHandConf];
} FreeHandConfTP,*PFreeHandConfTP;

// --------- ��������� �������� -----------
// ��� dwState
#define d_IncomingTalkState         0x80000000ul
#define d_AutomaticCallAbonent      0x40000000ul

#define Passive                     0xff
#define UnknownState                0xf0

// ������� ��������� ��� ������� 104
// ���� 0..3
#define Call                        1  // ��������� �����
#define Talk                        2  // �������� 
#define Hold                        3  // ���������
#define Busy                        4  // ����� 
#define IncomingCall                5  // �������� �����
#define CallTest                    6  // ��������� �������� ����� ��� ��������� TDC � IPPM
#define PultListen                  7  // ��������� ������
#define SelectorListen              8  // ������� ��������� "���������"
#define SelectorMain                9  // ������� ��������� "�������"
#define CallTestUp                  10
#define CallTestDown                11
#define CallTestError               12
#define SelectorDisableOut          13 

// ���� 4..6
#define d_AbntInSelector            0x10
#define d_AbntInConference          0x20
#define d_AbntInTransit             0x30
// ��� 7
#define d_AbntBusyOtherPult         0x80

// ��������� ��������� ��������� ��� ������� 108 (���� 3..6)
#define d_AbntSelectorStatusOff     0xff
#define d_AbntSelectorStatusShow    0x80
#define d_AbntSelectorStatusDel     0x70
#define d_AbntSelectorStatusErr     0x60
#define d_AbntSelectorTalkOn        0x50
#define d_AbntSelectorStatusCall    0x40
#define d_AbntSelectorStatusBusy    0x30
#define d_AbntSelectorStatusWait    0x20
#define d_AbntSelectorTalkOff       0x10
#define d_AbntSelectorDisableOut    0x08
#define StatusOff                   0 // ������ �������� 
#define StatusLeader                1 // ������ ��������
#define StatusMember                2 // ������ ��������
#define StatusListener              3 // ������ ���������
#define StatusListenerErr           4 // ������ ��������� ���� �������� ���������
// !! �������������� ������� ������ ��� DspCom (�������� �-���� GetAbntSelectorInfoFromIndex) !!
#define StatusDisableOut            4 // ������ ����� �������� (������� �� ������ ������)
#define StatusPrivate               5 // ������ ������� ��� ��������� (� ��� �������� �������� �� ������� ������)
#define StatusBusy                  6 // ������ ������� �����
#define StatusCall                  7 // ������ �������� ���� �����
#define StatusWait                  8 // ������ ������� � ��������� ���������
#define StatusError                 9 // ������ ������� ����� ������ ����������

#define d_Abonent2SelectorCurrentPult   1
#define d_Abonent2SelectorOtherPult     2
#define d_OperatorOffWithoutCondition   0x8000

// ������ ���
#define UP                          1
#define DOWN                        2

// --------- ��� ������� -----------
#define UnusedKey                   0 //- �� ������������
#define CallKey                     1 //- �������� �������
#define ServiceKey                  2 //- ��������� �������
#define ATCDirectKey                3 //- ��� ������

// --------- ��� �������� -----------
#define TDC  			1 
#define RSDT			2
#define ATC				3 
#define Selector		4
#define Level	 		5 
#define GroupTDC		6
#define NoCall			7
#define ImitatorTDC		8
#define PultCall        9
#define Commutator		10
#define CB				11
#define GroupRSDT		12
#define IndCall			13
#define ImitatorRSDT	14
#define Circul2_7		15
#define Circul2_8		16
#define Circul2_11		17
#define Circul2_12		18
#define PPSR			19
#define GenCallTDC		20
#define RD3				21
#define TDCIcx			22
#define Spec1			23
#define Spec2			24
#define Spec3			25
#define Spec4			26
#define Spec5			27
#define Spec6			28
#define Spec7			29
#define Spec8			30
#define ISDN			31
#define IPPM			32

// -------- ������� ����������� ������ ------
#define RejimDS                     0x81
#define RejimRS                     0x82
#define Ustanovka                   0x83
#define RequestTime                 0x84
#define RequestInit                 0x85

#define DS                          1 // ����� ��
#define RS                          2 // ����� ��
#define MIKE                        3 // ��������
#define ON                          1

#define d_PultAutoAnswer            0x01
#define dIncomingTalkFlag           0x01

#define d_GroupISDN                 63
#define d_GroupCallPult             64

#define d_TmTmpOutAbnt              2000 // ����� ������� ��������� ��������

#pragma pack(1)
typedef struct
{ // ��������� ��� ������� ������
  unsigned char SN;
  unsigned short ChanDS,ChanRS; // ������ ������� ��������� ��� ������
  unsigned char Tel[MAX_DIGIT_TLFN]; // ����� �������� ��� ������ ���������
  char Name[d_NameLen]; // ��� ������
  unsigned int Flags; // D0==1 - ����� ���������� ������
} sPultInfoTp;

typedef struct
{ // ��������� ������ �� ������ �������
	unsigned char CodeKey[2]; // 1-� ���� - ��� �������, 2-� ���� - ��� �������
	unsigned short Flags;
	unsigned short NChan; // ����� ��������
	unsigned short ChanOper; // ����� ��������� 
	unsigned char NumbAbon; // ����� ��������
	unsigned char TypeAbon; // ��� �������� ��� �������
} sKBKey;
#pragma pack()

typedef struct
{
  unsigned short wConnectionPoint,wOperatorChanel;
  unsigned char btPultNb,btKey,btLayer,btLedState,btKeyATCDirect;
  unsigned char btType,btHold,btE1,btOutgoingCnt;
  unsigned char NumbAbon;
  unsigned char btLvlTOut;
  unsigned char btTlfn[MAX_DIGIT_TLFN];
  char NumbSrs[MAX_DIGIT_TLFN+1],NameSrs[d_NameLen],NumbDst[MAX_DIGIT_TLFN+1];
  unsigned int dwState; // ��.���� - ��������� ��������, ���� 8-19 �������� ��������� �� ��������������� ������ (==1 - ���������)
  unsigned int dwTimerEnd,dwTimerAnaliz;
  unsigned int dwId;
} ABONENT_STATE_TP;

typedef struct // ��������� ������� ������� ������ ��� ���������
{
  unsigned char SN; // �������� ����� ����������
  unsigned char CodeKey[2]; // [0] - 0(�������) ��� 1(�������); [1] - ��� �������(0-128)
} PRESS_KEY_TP;

typedef struct
{ // ��������� ��� ��������� �� ���������� � ����. �����
	unsigned char PultNb; // ����� ������
	unsigned char AdrSwet; // ����� ����������
	unsigned char DanSwet; // ��������� ����������
	unsigned char CC_Sound; // ���������� ������
	unsigned char W_Sound; // ������������ ������
	unsigned char Buf[30];
} sIndic;

typedef struct
{ // ��������� ��� ���������� ���������� �� �������
  unsigned short NChan;
  char StrInf[25]; // ����� �������� �������� � ���������� ����
  unsigned int dwTOut;
} sListAb;

/////////////////////////////////////////////////////////////////////
#define d_MU2PultMaxRepeat      3
#define d_MaxAbntIPPM           160   // ����.��������� - NumberDirectAbon=192, �� ���� ��� �������� ��� �����!
#define d_MaxConnIPPM           5
#define d_DummyCP               0x1000

typedef struct
{
  unsigned int dwIP_Private;
  unsigned char btMAC[6];
  unsigned short wPort;
  unsigned char btStudioNumb,btSpeeker;
  bool bPgmIPPM,bIncomingCall;
  unsigned int dwTm4Send,
               dwSpeakStart;
  unsigned short wCP2MU;
  ABONENT_STATE_TP *psAbnt;
} IPPM_ABNT_TP;

typedef struct
{
  unsigned char SN;
  unsigned int dwIP;
  unsigned short wCP;
} PULT_MULTICAST_TP;

#define d_MU2IPPM_CmdTOut       350
#define d_MU2IPPMMaxRepeat      3

struct _LIST_MU2IPPM_TP;

struct _LIST_MU2IPPM_TP
{
  unsigned short wId;
  unsigned char Numb,repeat_cnt;
  unsigned int dwTOut;
  sInfoKadr sIK;
  void (*pFunc_IPPMAnswer2MU)(struct _LIST_MU2IPPM_TP *pItem,unsigned char btRes);
  void (*pFunc_IPPMNoAnswer2MU)(struct _LIST_MU2IPPM_TP *pItem);
  struct _LIST_MU2IPPM_TP *prev,*next;
};

typedef struct _LIST_MU2IPPM_TP LIST_MU2IPPM_TP;

#include "keyboardfn.h"

