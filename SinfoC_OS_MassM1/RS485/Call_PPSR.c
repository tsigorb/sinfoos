#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

void CreateOutgoingAbonentPPSR_RSDT(unsigned char PultNb,unsigned short wNChan,unsigned char btCallType)
{
  unsigned char btLayer,btKey;
  ABONENT_STATE_TP *psAbnt;

  btKey=FindAbntKey2Pult(PultNb,&btLayer,wNChan,&btCallType,0);
  if(btKey!=0xff)
  { // ������ �������� ������� ���� ��� ���� � ������� 0!
    psAbnt=GetAbonentStatePtrFromKey(PultNb,btKey,btLayer);
    if(psAbnt)
    { psAbnt->dwTimerEnd=MassTimer[TIMER_SPEAKLEVEL]+xTaskGetTickCount(); }
    else
    {
      psAbnt=GetFreeAbonentStatePtr();
      if(!psAbnt) return;
      psAbnt->wConnectionPoint=wNChan;
      psAbnt->wOperatorChanel=GetOperatorChannelFromKey(PultNb,btKey,btLayer);
      psAbnt->btPultNb=PultNb;
      psAbnt->btLayer=btLayer;
      psAbnt->btKey=btKey;
      psAbnt->NumbAbon=0;
      psAbnt->btType=btCallType;
      psAbnt->dwState=Off;
      AnswerIncomingCallMKA(psAbnt);
      psAbnt->dwTimerEnd=MassTimer[TIMER_SPEAKLEVEL]+xTaskGetTickCount();
      psAbnt->dwTimerAnaliz=100+xTaskGetTickCount();
    }
  }
}

void IncomingCall_PPSR_RSDT(unsigned short wNChan,unsigned char btCallType)
{ // ����� ��������� �� ��� ������, � ������������� ������ �����!
  unsigned char PultNb,btLayer,btKey;
  ABONENT_STATE_TP *psAbnt;

  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if(MassM1[PultNb] && IsKbdReady(PultNb) && IsKbdUnlock(PultNb))
    {
      btKey=FindAbntKey2Pult(PultNb,&btLayer,wNChan,&btCallType,0);
      if(btKey!=0xff)
      { // ������ �������� ������� ���� ��� ���� � ������� 0!
        psAbnt=GetAbonentStatePtrFromKey(PultNb,btKey,btLayer);
        if(psAbnt)
        { psAbnt->dwTimerEnd=MassTimer[TIMER_SPEAKLEVEL]+xTaskGetTickCount(); }
        else
        {
          psAbnt=GetFreeAbonentStatePtr();
          if(!psAbnt) return;
          psAbnt->wConnectionPoint=wNChan;
          psAbnt->wOperatorChanel=GetOperatorChannelFromKey(PultNb,btKey,btLayer);
          psAbnt->btPultNb=PultNb;
          psAbnt->btLayer=btLayer;
          psAbnt->btKey=btKey;
          psAbnt->NumbAbon=0;
          psAbnt->btType=btCallType;
          psAbnt->dwState=Off;
          if(IsGroupAutoAnswer4PultFromChan(PultNb,wNChan))
          {
            AnswerIncomingCallMKA(psAbnt);
            psAbnt->dwTimerEnd=MassTimer[TIMER_SPEAKLEVEL]+xTaskGetTickCount();
            psAbnt->dwTimerAnaliz=100+xTaskGetTickCount();
            return;
          }
          else
          {
            Commute(true,psAbnt->wOperatorChanel,psAbnt->wConnectionPoint);
            psAbnt->btLedState=dLedStateIncomingCall;
            SetAbonentStateByPtr(psAbnt,IncomingCall,0);
            psAbnt->dwTimerEnd=MassTimer[TIMER_INCOMINGCALL]+xTaskGetTickCount();
            IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,0,0);
          }
        }
      }
    }
  }
}

bool _xCallPPSR(unsigned short wNChan,unsigned char TypeCall,unsigned char NRst,unsigned char NRChan)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.BufCommand[0]=1; // �������
  sCmd.BufCommand[1]=wNChan >> 8;
  sCmd.BufCommand[2]=wNChan;
  sCmd.BufCommand[3]=TypeCall;
  if(TypeCall==ChanPPSR)
  {
    sCmd.BufCommand[4]=NRst-1; // ����� ������������ 
    sCmd.BufCommand[5]=NRChan; // ����� ������ 
    sCmd.L=6;
  }
  else
  {
    sCmd.BufCommand[4]=0;
    sCmd.L=5;
  }
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(WaitReply(1,0)) return(true);
  }
  return(false);
}

void OutgoingCallPPSR(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  bool bRes;
  unsigned short wCP;
  unsigned char NRst=MassKey[PultNb][btLayer][Key].NumbAbon,
                NChan=GetRadioChan(PultNb);
  ABONENT_STATE_TP *psAbnt;

  psAbnt=GetFreeAbonentStatePtr();
  if(psAbnt)
  {
    psAbnt->btPultNb=PultNb;
    psAbnt->btLayer=btLayer;
    psAbnt->btKey=Key;
    wCP=MassKey[PultNb][btLayer][Key].NChan;
    psAbnt->btType=PPSR;
    if(NRst)
    {
      if(NChan) bRes=_xCallPPSR(wCP,ChanPPSR,NRst,NChan);
      else bRes=_xCallPPSR(wCP,IndivPPSR,NRst,0);
      if(bRes)
      {
        psAbnt->dwTimerEnd=d_TmTmpOutAbnt+xTaskGetTickCount();
        psAbnt->wConnectionPoint=wCP;
        psAbnt->wOperatorChanel=0;
        psAbnt->btLedState=dLedStateOutgoingCall;
        psAbnt->NumbAbon=NRst;
        SetAbonentStateByPtr(psAbnt,Call,0);
      }
    }
    else
    {
      bRes=_xCallPPSR(wCP,InclPPSR,0,0);
      if(bRes)
      {
        psAbnt->dwTimerEnd=MassTimer[TIMER_SPEAKLEVEL]+xTaskGetTickCount();
        psAbnt->wConnectionPoint=wCP;
        psAbnt->wOperatorChanel=GetOperatorChannelFromKey(PultNb,Key,btLayer);
        psAbnt->btLedState=dLedStateTalk;
        psAbnt->NumbAbon=0;
        psAbnt->dwTimerAnaliz=100+xTaskGetTickCount();
        SetAbonentStateByPtr(psAbnt,Talk,0);
        DSRS_On(psAbnt->btPultNb,psAbnt->wOperatorChanel);
        Commute(true,psAbnt->wOperatorChanel,wCP);
        Commute(true,wCP,psAbnt->wOperatorChanel);
      }
    }
    if(bRes) IndicCmd(PultNb,btLayer,Key,psAbnt->btLedState,1,1);
    else PutErrInfo2Display(PultNb,"������ ����!");
  }
}

void PressCallKey_PPSR(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  ABONENT_STATE_TP *psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);

  if(!psAbnt)
  {
    if(IsCreatedTransit(PultNb)) XorAbnt2Transit(PultNb,Key);
    else OutgoingCallPPSR(PultNb,Key,btLayer);
  }
  else
  {
    switch(GetAbonentStateFromPtr(psAbnt))
    {
      case IncomingCall:
      {
        AnswerIncomingCallMKA(psAbnt);
        psAbnt->dwTimerEnd=MassTimer[TIMER_SPEAKLEVEL]+xTaskGetTickCount();
        psAbnt->dwTimerAnaliz=100+xTaskGetTickCount();
        break;
      }
    }
  }
}
