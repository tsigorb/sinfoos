#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

///////////////////////////////////////////////////////////////
void Test_AutoGenerationIncomingCallTDC(unsigned char PultNb,unsigned char btOff);

/////////////////////////////////////////////////////////////////////
extern xQueueHandle xKeyProcessing;

/////////////////////////////////////////////////////////////////////
typedef struct
{
  unsigned char btState,btKey;
} sKeyStateTP;

static sKeyStateTP MassKeyOff[NumberKeyboard];

static unsigned char PressLayer[NumberKeyboard],PressNRChan[NumberKeyboard];

static bool PressChoice[NumberKeyboard], // ������� ������� ������� "�����"
            PressAddMemeber[NumberKeyboard], // ������� ������� ������� "+��������"
            PressDisEnOut[NumberKeyboard],
            PressTest[NumberKeyboard],
            PressBlockKey[NumberKeyboard],
            NoBlockKey[NumberKeyboard];

/////////////////////////////////////////////////////////////////////
void InitKeyVars(void)
{
  vPortEnableSwitchTask(false);
  memset(MassKeyOff,0,sizeof(MassKeyOff));
  memset(PressLayer,0xff,sizeof(PressLayer));
  memset(PressChoice,0,sizeof(PressChoice));
  memset(PressAddMemeber,0,sizeof(PressAddMemeber));
  memset(PressDisEnOut,0,sizeof(PressTest));
  memset(PressTest,0,sizeof(PressTest));
  memset(PressBlockKey,0,sizeof(PressBlockKey));
  memset(NoBlockKey,0,sizeof(NoBlockKey));
  vPortEnableSwitchTask(true);
}

bool IsPressChoice(unsigned char PultNb)
{ return(PressChoice[PultNb]); }

bool IsPressAddMember(unsigned char PultNb)
{ return(PressAddMemeber[PultNb]); }

bool IsPressDisEnOut(unsigned char PultNb)
{ return(PressDisEnOut[PultNb]); }

bool IsPressTest(unsigned char PultNb)
{ return(PressTest[PultNb]); }

unsigned char GetRadioChan(unsigned char PultNb)
{ return(PressNRChan[PultNb]); }

void ClearKeySpecial(unsigned char PultNb)
{
  vPortEnableSwitchTask(false);
  MassKeyOff[PultNb].btState=0;
  PressChoice[PultNb]=false;
  PressAddMemeber[PultNb]=false;
  PressTest[PultNb]=false;
  PressNRChan[PultNb]=0;
  PressBlockKey[PultNb]=false;
  NoBlockKey[PultNb]=false;
  vPortEnableSwitchTask(true);
}

void Press_KeyOff(unsigned char PultNb,unsigned char btLayer,unsigned char Key)
{
  switch(MassKeyOff[PultNb].btState) 
  {
    case 0:
      if(IsOutgoingNumberTmpPresent(PultNb))
      {
        OutgoingNumberTmpClear(PultNb);
        IndicClock(PultNb);
        PutErrInfo2Display(PultNb,NULL);
      }
      else
      {
        MassKeyOff[PultNb].btState=1;
        MassKeyOff[PultNb].btKey=Key;
        IndicCmd(PultNb,btLayer,Key,GREEN,1,1);
      }
      break;
    case 2:
      MassKeyOff[PultNb].btState=0;
      IndicCmd(PultNb,btLayer,Key,OFF,0,0);
      break;
  }
}

void Unpress_KeyOff(unsigned char PultNb,unsigned char btLayer,unsigned char Key)
{
  if(MassKeyOff[PultNb].btState==1) MassKeyOff[PultNb].btState=2;
  if(MassKeyOff[PultNb].btState==3)
  {
    MassKeyOff[PultNb].btState=0;
    IndicCmd(PultNb,btLayer,MassKeyOff[PultNb].btKey,OFF,0,0);
  }
}

void ClearKeyOff(unsigned char PultNb,unsigned char btLayer)
{
  if(MassKeyOff[PultNb].btState==1) MassKeyOff[PultNb].btState=3;
  if(MassKeyOff[PultNb].btState==2)
  {
    MassKeyOff[PultNb].btState=0;
    IndicCmd(PultNb,btLayer,MassKeyOff[PultNb].btKey,OFF,0,0);
  }
}

bool IsKeyOff(unsigned char PultNb)
{ return(MassKeyOff[PultNb].btState ? true:false); }

void PressLayerKey(unsigned char PultNb,unsigned char btLayer,unsigned char Key)
{
//  if(IsSelectorActive(PultNb) || IsConferenceActive(PultNb)) return;
  if(PressLayer[PultNb]==0xff)
  {
    PressLayer[PultNb]=btLayer;
    if(PressChoice[PultNb]) SwitchGroup(PultNb,btLayer);
    else
    {
      if(PressBlockKey[PultNb])
      {
        NoBlockKey[PultNb]=true;
        SetLayerUnlockState(PultNb,btLayer,false);
      }
      else SetLayerUnlockState(PultNb,btLayer,true);
      IndicCmd(PultNb,btLayer,Key,
               GetLayerLedColor(PultNb,btLayer),
               1,1);
    }
  }
}

void PressServiceKey(unsigned char PultNb,unsigned char btLayer,unsigned char Key,unsigned char CodeKey1,
                     bool bOtkluchenie)
{ // ������� ��������� �������
  if((CodeKey1>=1) && (CodeKey1<=16))
  { // P1..P16
    PressNRChan[PultNb]=CodeKey1;
    IndicCmd(PultNb,btLayer,Key,GREEN,1,1);
  }
  else
  {
    switch(CodeKey1)
    {
      case ATCP:  // 17
        /*PrATCR[PultNb] = true; 
        IndicCmd(SN_Key,SwetKey,GREEN,1,1);*/
        break;
      case Transit:
        PressTransitKey(PultNb,btLayer,Key,bOtkluchenie);
        break;
      case BeginSelector: // ���. ���������
        if(IsSelectorActive(PultNb)/* && IsPressChoice(PultNb)*/) Key_BeginSelector(PultNb,btLayer,Key);
        break;
      case EndSelector:
        if(bOtkluchenie && IsSelectorActive(PultNb)) EndSelectorFn(PultNb); 
        break;
      case AddMember: //113: "+ ��������"
        PressAddMemeber[PultNb] = true;
        IndicCmd(PultNb,btLayer,Key,GREEN,1,1);
        break;
      case TikkerKey: //115 ������
        if(IsSelectorActive(PultNb)) PressTikkerKey(PultNb,btLayer,Key);
        break;
      case BackSpace:
        Key_BackSpace(PultNb);
        break;
      case KeyOff: //134: ����������
        Press_KeyOff(PultNb,btLayer,Key);
        break;
      case Star:
      case Lattice:
          CodeKey1=CodeKey1-Star+14+Numeral0; 
      case Numeral0: //144: �����
      case Numeral1: //145:
      case Numeral2: //146:
      case Numeral3: //147:
      case Numeral4: //148:
      case Numeral5: //149:
      case Numeral6: //150:
      case Numeral7: //151:
      case Numeral8: //152:
      case Numeral9: //153: 
      case Pause:    // �����
        Key_Numeral(PultNb,CodeKey1-Numeral0);
        break;
      case Redial: //162: ������ ������
        Key_Redial(PultNb);
        break;
      case TestCall: //209: ����
        PressTest[PultNb] = true;
        IndicCmd(PultNb,btLayer,Key,GREEN,1,1);
        break;
      case Choice: //231: �����
        PressChoice[PultNb]=true;
        IndicCmd(PultNb,btLayer,Key,GREEN,1,1);
        break;
      case BlockKey:
        PressBlockKey[PultNb]=true;
        break;
      case Layer1://136: ���� 1-3
      case Layer2://137:
      case Layer3://138:
        PressLayerKey(PultNb,CodeKey1-Layer1,Key);
        break;
      case Conference1: //154: ����������� 1-6
      case Conference2: //155:
      case Conference3: //156:
      case Conference4: //157:
      case Conference5: //158:
      case Conference6: //159:
        PressConferenceKey(PultNb,Key,btLayer,CodeKey1-Conference1,bOtkluchenie,PressTest[PultNb]);
        break;
      case Tabl1: // ����� �������
      case Tabl2:
      case Tabl3:
      case Tabl4:
        //Key_TablN(PultNb,CodeKey1-Tabl1,SwetKey,&NumbTabl);
        break;
      case Selector1:  // 214 - �������� �1-�16
      case Selector2:  // 215
      case Selector3:  // 216
      case Selector4:  // 217
      case Selector5:  // 218
      case Selector6:  // 219
      case Selector7:  // 220
      case Selector8:  // 221
      case Selector9:  // 222
      case Selector10: // 223
      case Selector11: // 224
      case Selector12: // 225
      case Selector13: // 226
      case Selector14: // 227
      case Selector15: // 228
      case Selector16: // 229
        PressSelectorN(PultNb,btLayer,Key,CodeKey1-Selector1,bOtkluchenie);
        break;
      case DisableEnableOut:
//        if(IsSelectorActive(PultNb))
        {
          PressDisEnOut[PultNb]=true;
          IndicCmd(PultNb,btLayer,Key,GREEN,1,1);
        }
        break;
      default: break;
    }
  }
}

void ReleaseServiceKey(unsigned char PultNb,unsigned char btLayer,unsigned char CodeKey1,
                       unsigned char Key)
{ // ��� ������� ��������� �������
  if((CodeKey1>=1) && (CodeKey1<=16))
  { // P1..P16
    if(PressNRChan[PultNb]==CodeKey1) PressNRChan[PultNb]=0;
    IndicCmd(PultNb,btLayer,Key,OFF,0,0);
  }
  else
  {
    switch(CodeKey1)
    {
      case BlockKey:
        PressBlockKey[PultNb]=false;
        if(NoBlockKey[PultNb])
        { NoBlockKey[PultNb]=false; break; }
        SetKbdUnlockState(PultNb,false);
        PutKbdUnlockLedState(PultNb,Key);
        break;
      case ATCP: // (17)
        /*PrATCR[PultNb] = false;
        IndicCmd(SN_Key,SwetKey,OFF,0,0);*/
        break;
      case Transit:
        UnPressTransitKey(PultNb,btLayer,Key);
        break;
      case AddMember: //113: "+ ��������"
        PressAddMemeber[PultNb] = false;
        IndicCmd(PultNb,btLayer,Key,OFF,0,0);
        break;
      case KeyOff: //134: ����������
        Unpress_KeyOff(PultNb,btLayer,Key); 
        break;
      case TestCall: //209: ����
        PressTest[PultNb]=false;
        IndicCmd(PultNb,btLayer,Key,OFF,0,0);
//!!!!!!!!!!
//Test_AutoGenerationIncomingCallTDC(PultNb,IsKeyOff(PultNb));
        break;
      case Choice: //231: �����
        PressChoice[PultNb] = false;
        IndicCmd(PultNb,btLayer,Key,OFF,0,0);
        break;
      case Layer1://136: ���� 1-3
      case Layer2://137:
      case Layer3://138:
        PressLayer[PultNb]=0xff;
        break;
      case Conference1: //154: ����������� 1-6
      case Conference2: //155:
      case Conference3: //156:
      case Conference4: //157:
      case Conference5: //158:
      case Conference6: //159:
        UnpressConferenceKey(PultNb,Key,btLayer,CodeKey1-Conference1);
        break;
      case DisableEnableOut:
        PressDisEnOut[PultNb]=false;
        IndicCmd(PultNb,btLayer,Key,OFF,0,0);
        break;
      default: break;
    }
  }
}

void PressDSRS(unsigned char PultNb,unsigned char btDsRs)
{
}

void ProcessKeyQueue(void)
{
  unsigned char i,PultNb,btLayer,
                Key,TypeKey,CodeKey1;
  bool bOtkluchenie,bReleaseKey;
  unsigned short NChan;
  PRESS_KEY_TP sKeyPress;

  if(xQueueReceive(xKeyProcessing,&sKeyPress,0))
  { // ���� ��������� � ������� ������� ������
    PultNb=GetPultIndFromSN(sKeyPress.SN);
    if(PultNb==0xff) return;
    if(sKeyPress.CodeKey[0]) bReleaseKey=true;
    else bReleaseKey=false;
    Key=sKeyPress.CodeKey[1]; // ���������� ����� �������
    btLayer=GetCurrentLayer(PultNb);
    bOtkluchenie=(MassKeyOff[PultNb].btState!=0);
    if((MassM1[PultNb]<17) && (Key & 0x80))
    { // ������� ������� �� ����������� ������ ����������� ������
      switch(Key)
      {
        case 129: // ��
        case 130: // ��
          PressDSRS(PultNb,Key-129);
          break;
        case 131: // ��������� ���������� ������ (���������� ������ �� ����� ���������)
          break;
        case 132: // ������ �� ������ �� ��������� ������� 
          if(bReleaseKey)
          {
            time_t tt_;
            struct tm *ptime_;

            tt_=ReadClock();
            ptime_=gmtime(&tt_);
            ClockIndPult(PultNb,ptime_);
          }
          break;
      }
    } 
    else 
    {
      TypeKey=MassKey[PultNb][btLayer][Key].CodeKey[0]; // ��� �������
      CodeKey1=MassKey[PultNb][btLayer][Key].CodeKey[1]; // ��� ��������
      if(IsKbdUnlock(PultNb))
      { // ���������� ��������������
        switch(TypeKey)
        {
          case ServiceKey:
          {
            if(bReleaseKey) ReleaseServiceKey(PultNb,btLayer,CodeKey1,Key);
            else PressServiceKey(PultNb,btLayer,Key,CodeKey1,bOtkluchenie);
            break;
          }
          case CallKey:
            if(bReleaseKey) ReleaseCallKey(PultNb,Key,btLayer);
            else PressCallKey(PultNb,Key,bOtkluchenie,PressTest[PultNb]);
            break;
          case ATCDirectKey:
            if(!bReleaseKey)
            {
              if(bOtkluchenie) OtboiAbonent_ATCDirect(PultNb,btLayer,Key);
              else Key_ATCDirect(PultNb,btLayer,Key);
            }
            break;
          default: break;
        }
        if(bReleaseKey)
        { if(TypeKey && (CodeKey1!=KeyOff)) ClearKeyOff(PultNb,btLayer); }
      }
      else
      { // ���������� �������������
        if((TypeKey==ServiceKey) && (CodeKey1==BlockKey) && bReleaseKey) 
        {
          SetKbdUnlockState(PultNb,true);
          PutKbdUnlockLedState(PultNb,Key);
        }
      }
    }
  }
}

