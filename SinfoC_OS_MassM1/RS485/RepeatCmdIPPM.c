#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"

//*****************************************************************************
typedef struct
{
  unsigned char Numb;
  unsigned short wCmd;
  bool bSendAnsw;
  sInfoKadr sInfo;
} LAST_IPPM_CMD_TP;

//*****************************************************************************
static LAST_IPPM_CMD_TP MasLastIPPMCmd[d_MaxAbntIPPM];

//*****************************************************************************
void InitRepeatCmdIPPM(void)
{ memset(MasLastIPPMCmd,0,sizeof(MasLastIPPMCmd)); }

void DelRepeatCmdIPPM(unsigned char Numb)
{
  Numb--;
  if(MasLastIPPMCmd[Numb].Numb)
  {
    MasLastIPPMCmd[Numb].Numb=0;
    MasLastIPPMCmd[Numb].bSendAnsw=false;
  }
}

void SendAnsw4IPPMCmd(unsigned char Numb,unsigned short wCmd,sInfoKadr *psInfo)
{
  Numb--;
  if(MasLastIPPMCmd[Numb].Numb)
  {
SendStr2IpLogger("$DEBUG$SendAnsw4IPPMCmd: Numb=%d, CmdId=%d",Numb+1,wCmd);
    MasLastIPPMCmd[Numb].wCmd=wCmd;
    MasLastIPPMCmd[Numb].bSendAnsw=true;
    memcpy(&MasLastIPPMCmd[Numb].sInfo,psInfo,sizeof(sInfoKadr));
  }
/*{
static int s_iCycle=0;
s_iCycle++; s_iCycle&=0x1;
if(s_iCycle) return;
}*/
  xQueueSend(xQueueReply[psInfo->Source],psInfo,0,0);
}

bool RepeatCmdIPPM(unsigned char Numb,unsigned short wCmd)
{
  LAST_IPPM_CMD_TP *psLastCmd=MasLastIPPMCmd+(Numb-1);

  if(psLastCmd->Numb)
  {
    if(wCmd==psLastCmd->wCmd)
    {
      if(psLastCmd->bSendAnsw)
      {
SendStr2IpLogger("$DEBUG$RepeatCmdIPPM repeat answer: Numb=%d, CmdId=%d",Numb,wCmd);
        xQueueSend(xQueueReply[psLastCmd->sInfo.Source],&(psLastCmd->sInfo),0,0);
        return(true);
      }
    }
    else
    {
      psLastCmd->wCmd=wCmd;
      psLastCmd->bSendAnsw=false;
SendStr2IpLogger("$DEBUG$RepeatCmdIPPM new cmd: Numb=%d, CmdId=%d",Numb,wCmd);
    }
  }
  else
  {
    psLastCmd->Numb=Numb;
    psLastCmd->wCmd=wCmd;
    psLastCmd->bSendAnsw=false;
SendStr2IpLogger("$DEBUG$RepeatCmdIPPM new cmd: Numb=%d, CmdId=%d",Numb,wCmd);
  }
  return(false);
}

