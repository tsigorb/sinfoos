#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

///////////////////////////////////////////////////////////////////////////////
void *pvPortMalloc(size_t xSize);
void vPortFree(void *pv);

///////////////////////////////////////////////////////////////////////////////
PULT_MULTICAST_TP g_MasPultMulticast[NumberKeyboard];

static IPPM_ABNT_TP sMasAbntIPPM[d_MaxAbntIPPM];

static unsigned char s_btSpeakIPPM2Selector=3,
                     s_btSpeakIPPM2UK=6,
                     s_btMaxIPPM=32;

static unsigned int s_dwLifeTmIPPM=5000;

/////////////////////////////////////////////////////////////////////
unsigned char GetIndIPPMFromCP2MU(unsigned short wCP2MU)
{
  unsigned char i;

  for(i=0;i<s_btMaxIPPM;i++)
  {
    if(sMasAbntIPPM[i].dwIP_Private && (sMasAbntIPPM[i].wCP2MU==wCP2MU)) return i;
  }
  return 0xff;
}

void SET_DW(unsigned char *pbtBuf,unsigned int dwData)
{
  pbtBuf[3]=dwData >> 24;
  pbtBuf[2]=dwData >> 16;
  pbtBuf[1]=dwData >> 8;
  pbtBuf[0]=dwData;
}

void StartAudioFromMU2IPPM_Private(unsigned char Numb,unsigned short wCP)
{
  unsigned int dwIP=sMasAbntIPPM[Numb-1].dwIP_Private;
  static sCommand sCmd;

  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.L=8;
  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[1]=178;
  sCmd.BufCommand[2]=1;
  sCmd.BufCommand[3]=wCP;
  SET_DW(sCmd.BufCommand+4,dwIP);
  xQueueSendCmd(&sCmd,0);
}

void StartAudioFromMU2IPPM_Multicast(unsigned int dwIP,unsigned short wCP)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.L=8;
  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[1]=178;
  sCmd.BufCommand[2]=1;
  sCmd.BufCommand[3]=wCP;
  SET_DW(sCmd.BufCommand+4,dwIP);
  xQueueSendCmd(&sCmd,0);
}

void StopAudioFromMU2IPPM(unsigned short wCP)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.L=4;
  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[1]=178;
  sCmd.BufCommand[2]=0;
  sCmd.BufCommand[3]=wCP;
  xQueueSendCmd(&sCmd,0);
}

void StartAudioFromIPPM2MU(unsigned short wCP)
{
  unsigned char btInd=GetIndIPPMFromCP2MU(wCP);
  static sCommand sCmd;

  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.L=8;
  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[1]=162;
  sCmd.BufCommand[2]=1;
  sCmd.BufCommand[3]=(wCP & 0xff)-1;
  memset(sCmd.BufCommand+4,0,4); // SSRC==0 - InitRecord ��� ������� SSRC
  xQueueSendCmd(&sCmd,0);
  if(btInd!=0xff) sMasAbntIPPM[btInd].dwSpeakStart=xTaskGetTickCount();
}

void StopAudioFromIPPM2MU(unsigned short wCP)
{
  unsigned char btInd=GetIndIPPMFromCP2MU(wCP);
  static sCommand sCmd;

  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.L=4;
  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[1]=163;
  sCmd.BufCommand[2]=1;
  sCmd.BufCommand[3]=(wCP & 0xff)-1;
  xQueueSendCmd(&sCmd,0);
  if(btInd!=0xff) sMasAbntIPPM[btInd].dwSpeakStart=0;
}

/////////////////////////////////////////////////////////////////////
bool IsW6100(void);

/*
������ ������ PultMulticastInfo:
MAXIPPM = 64
SN : IP address ������ D (224.0.0.0-239.255.255.255)
21 : 224.1.1.1
*/
void PultMulticastDecodeStr(char *pStr0)
{
  char *pPult,*pAddrIP,*pStr;
  unsigned int dwPult,dwIP;
  int i;

  if(!pStr0 || !pStr0[0]) return;
  pStr=strstr(pStr0,"MAXIPPM");
  if(pStr)
  {
    pStr=strchr(pStr+7,'=');
    if(!pStr) return;
    pStr=strclr(pStr+1);
    if(!pStr) return;
    i=atoi(pStr);
    if((i<1) || (i>d_MaxAbntIPPM)) return;
    s_btMaxIPPM=i;
    s_dwLifeTmIPPM=1000*(i>>3)+(d_MU2IPPM_CmdTOut*(d_MU2IPPMMaxRepeat+1));
    return;
  }
  pPult=pStr0;
  pAddrIP=strchr(pStr0,':');
  if(!pAddrIP) return;
  *pAddrIP=0; pAddrIP++;
  pPult=strclr(pPult);
  pAddrIP=strclr(pAddrIP);
  if(pPult && pAddrIP)
  {
    dwPult=atoi(pPult);
    dwIP=atoIP(pAddrIP);
    if(dwPult && dwIP)
    { // ��������� ���� �  sMasAbntIPPM
      for(i=0;i<NumberKeyboard;i++)
      {
        if(g_MasPultMulticast[i].SN==dwPult) return;
      }
      for(i=0;i<NumberKeyboard;i++)
      {
        if(g_MasPultMulticast[i].SN==0)
        {
          g_MasPultMulticast[i].SN=dwPult;
          g_MasPultMulticast[i].dwIP=dwIP;
SendStr2IpLogger("$DEBUG$PultMulticastDecodeStr: SN=%d, multicast IP=%08x",dwPult,dwIP);
          return;
        }
      }
    }
  }
}

void PultMulticastDecode(char *pBuf)
{
  char *pStr;
  while(pBuf)
  {
    pStr=pBuf;
    pBuf=GetNextStr(pBuf);
    PultMulticastDecodeStr(pStr);
  }
}

void ReadPultMulticastInfo(void)
{
  unsigned int dwFSize;
  char *pBuf;
  HANDLE hFile;

  memset(g_MasPultMulticast,0,sizeof(g_MasPultMulticast));
  hFile=CreateFile("pmi.txt",GENERIC_READ,0);
  if(hFile>=0)
  {
    dwFSize=GetFileSize(hFile);
    if(dwFSize)
    {
      pBuf=pvPortMalloc(dwFSize+1);
      if(pBuf)
      {
        if(ReadFile(hFile,pBuf,dwFSize))
        {
          pBuf[dwFSize]=0;
          PultMulticastDecode(pBuf);
        }
        vPortFree(pBuf);
      }
    }
    CloseHandle(hFile);
  }
}

unsigned int GetMIPAddr(unsigned char SN)
{
  int i;
  for(i=0;i<NumberKeyboard;i++)
  {
    if(g_MasPultMulticast[i].SN==SN)
    {
SendStr2IpLogger("$DEBUG$GetMIPAddr: SN=%d, multicast IP=%08x",SN,g_MasPultMulticast[i].dwIP);
      return g_MasPultMulticast[i].dwIP;
    }
  }
SendStr2IpLogger("$DEBUG$GetMIPAddr: SN=%d not found (addr=0)!",SN);
  return 0;
}

/////////////////////////////////////////////////////////////////////
bool IsRegisterAbonentIPPM(unsigned char Numb)
{
  if(Numb && (Numb<=s_btMaxIPPM))
  {
    if(sMasAbntIPPM[Numb-1].dwIP_Private) return true;
  }
  return false;
}

bool GetMAC4IPPM(unsigned int dwIP,unsigned char *MACAddr)
{
  unsigned char i;

  vPortEnableSwitchTask(false);
  for(i=0;i<s_btMaxIPPM;i++)
  {
    if(sMasAbntIPPM[i].dwIP_Private==dwIP)
    {
      memcpy(MACAddr,sMasAbntIPPM[i].btMAC,6);
      vPortEnableSwitchTask(true);
      if(MACAddr[0] || MACAddr[1] || MACAddr[2] || MACAddr[3] || MACAddr[4] || MACAddr[5]) return true;
      return false;
    }
  }
  vPortEnableSwitchTask(true);
  return false;
}

unsigned short GetCP4IPPM(unsigned char Numb)
{
  if(Numb && (Numb<=s_btMaxIPPM))
  {
    Numb--;
    if(sMasAbntIPPM[Numb].dwIP_Private)
    { // IPPM ���������������
      if(sMasAbntIPPM[Numb].psAbnt)
      {
        unsigned short wCP=sMasAbntIPPM[Numb].psAbnt->wConnectionPoint;
        if(wCP!=0xffff)
        {
          if(wCP<d_DummyCP) return wCP;
          else
          { // ����������� multicast CP
            unsigned char i;

            wCP&=0xff; // �������� ����� ������
            for(i=0;i<NumberKeyboard;i++)
            {
              if(g_MasPultMulticast[i].SN==wCP)
              {
                return g_MasPultMulticast[i].wCP;
              }
            }
          }
        }
      }
    }
  }
  return 0;
}

unsigned short GetCPFromVMCP(unsigned short wVMCP)
{
  unsigned char i;

  wVMCP&=0xff; // �������� ����� ������ � ����������� (B11..B08)
  for(i=0;i<NumberKeyboard;i++)
  {
    if(g_MasPultMulticast[i].SN==wVMCP)
    {
      return g_MasPultMulticast[i].wCP;
    }
  }
  return 0;
}

unsigned short CreatVMCP(unsigned char SN)
{
  int i;

  for(i=0;i<NumberKeyboard;i++)
  {
    if(g_MasPultMulticast[i].SN==SN)
    { return (d_DummyCP | SN); }
  }
  return 0;
}

unsigned short CreatMCP(unsigned char SN)
{
  int i;
  unsigned short wCP;

  for(i=0;i<NumberKeyboard;i++)
  {
    if(g_MasPultMulticast[i].SN==SN)
    {
      if(g_MasPultMulticast[i].wCP) return g_MasPultMulticast[i].wCP;
      wCP=GetFreeConnectionPoint();
      if(wCP!=0xff) g_MasPultMulticast[i].wCP=wCP;
      else wCP=0;
      return wCP;
    }
  }
  return 0;
}

void DelMCP(unsigned short wMCP)
{
  int i;
  for(i=0;i<NumberKeyboard;i++)
  {
    if(g_MasPultMulticast[i].SN && (g_MasPultMulticast[i].wCP==wMCP))
    {
      DelConnectionPoint(wMCP);
      g_MasPultMulticast[i].wCP=0;
      return;
    }
  }
}

unsigned char GetCountMulticastIPPM(unsigned short wVMCP)
{
  unsigned char i,btCnt=0;
  ABONENT_STATE_TP *psAbnt;

  for(i=0;i<s_btMaxIPPM;i++)
  {
    if(sMasAbntIPPM[i].dwIP_Private)
    {
      psAbnt=sMasAbntIPPM[i].psAbnt;
      if(psAbnt && (psAbnt->wConnectionPoint==wVMCP)) btCnt++;
    }
  }
  return btCnt;
}

void SetCurrentAbonentIPPM(unsigned char Numb,ABONENT_STATE_TP *psAbnt)
{
  sMasAbntIPPM[Numb-1].psAbnt=psAbnt;
}

void IPPM_CmdConnectNoAnswer2MU(LIST_MU2IPPM_TP *pItem)
{
  ABONENT_STATE_TP *psAbnt=sMasAbntIPPM[pItem->Numb-1].psAbnt;
  if(psAbnt) OtboiAbonent(psAbnt->btPultNb,psAbnt->btKey,psAbnt->btLayer);
}

void SendIPPMCmdConnect(unsigned char Numb,ABONENT_STATE_TP *psAbnt)
{
  unsigned int dwIP=GetMIPAddr(MassM1[psAbnt->btPultNb]);

  if(!dwIP) return;
  LIST_MU2IPPM_TP *pItem=ListMU2IPPM_AddItem(NULL);
  if(pItem)
  {
    pItem->Numb=Numb;
    pItem->sIK.A1=sMasAbntIPPM[Numb-1].dwIP_Private;
    pItem->sIK.PORT1=sMasAbntIPPM[Numb-1].wPort;
    pItem->sIK.L=17;
    pItem->sIK.ByteInfoKadr[0]=0xed;
    pItem->sIK.ByteInfoKadr[1]=0x0;
    pItem->sIK.ByteInfoKadr[2]=Numb;
    pItem->sIK.ByteInfoKadr[3]=pItem->wId >> 8;
    pItem->sIK.ByteInfoKadr[4]=pItem->wId & 0xff;
    pItem->sIK.ByteInfoKadr[5]=3;
    pItem->sIK.ByteInfoKadr[6]=dwIP;
    pItem->sIK.ByteInfoKadr[7]=dwIP >> 8;
    pItem->sIK.ByteInfoKadr[8]=dwIP >> 16;
    pItem->sIK.ByteInfoKadr[9]=dwIP >> 24;
    strcpy((char *)(pItem->sIK.ByteInfoKadr+10),"CONNECT");
    pItem->pFunc_IPPMNoAnswer2MU=IPPM_CmdConnectNoAnswer2MU;
  }
}

void IPPM_CmdConnectPrivateNoAnswer2MU(LIST_MU2IPPM_TP *pItem)
{
  ABONENT_STATE_TP *psAbnt=sMasAbntIPPM[pItem->Numb-1].psAbnt;
  if(psAbnt) OtboiAbonent(psAbnt->btPultNb,psAbnt->btKey,psAbnt->btLayer);
}

void SendIPPMCmdConnectPrivate(unsigned char Numb)
{
  LIST_MU2IPPM_TP *pItem=ListMU2IPPM_AddItem(NULL);
  if(pItem)
  {
    sMasAbntIPPM[Numb-1].bIncomingCall=false;
    pItem->Numb=Numb;
    pItem->sIK.A1=sMasAbntIPPM[Numb-1].dwIP_Private;
    pItem->sIK.PORT1=sMasAbntIPPM[Numb-1].wPort;
    pItem->sIK.L=21;
    pItem->sIK.ByteInfoKadr[0]=0xed;
    pItem->sIK.ByteInfoKadr[1]=0x0;
    pItem->sIK.ByteInfoKadr[2]=Numb;
    pItem->sIK.ByteInfoKadr[3]=pItem->wId >> 8;
    pItem->sIK.ByteInfoKadr[4]=pItem->wId & 0xff;
    pItem->sIK.ByteInfoKadr[5]=4;
    strcpy((char *)(pItem->sIK.ByteInfoKadr+6),"CONNECT_PRIVATE");
    pItem->pFunc_IPPMNoAnswer2MU=IPPM_CmdConnectPrivateNoAnswer2MU;
  }
}

unsigned short GetCP_IPPM2MU(unsigned char Numb)
{
  unsigned short wCP=0;

  if((Numb<1) || (Numb>s_btMaxIPPM)) return 0;
  Numb--;
  vPortEnableSwitchTask(false);
  if(sMasAbntIPPM[Numb].dwIP_Private && sMasAbntIPPM[Numb].psAbnt)
  {
    if(sMasAbntIPPM[Numb].psAbnt->wConnectionPoint<d_DummyCP)
    { wCP=sMasAbntIPPM[Numb].psAbnt->wConnectionPoint; }
    else wCP=sMasAbntIPPM[Numb].wCP2MU;
  }
  vPortEnableSwitchTask(true);
  return(wCP);
}

bool DelCP_IPPM2MU(unsigned char Numb)
{
  unsigned short wCP;
  Numb--;
  wCP=sMasAbntIPPM[Numb].wCP2MU;
  if(wCP)
  { // ���� ����� ����� �� IPPM
    LIST_MU2IPPM_TP *pItem=ListMU2IPPM_AddItem(NULL);
    if(pItem)
    {
      pItem->Numb=Numb+1;
      pItem->sIK.A1=sMasAbntIPPM[Numb].dwIP_Private;
      pItem->sIK.PORT1=sMasAbntIPPM[Numb].wPort;
      pItem->sIK.L=20;
      pItem->sIK.ByteInfoKadr[0]=0xed;
      pItem->sIK.ByteInfoKadr[1]=0x0;
      pItem->sIK.ByteInfoKadr[2]=Numb+1;
      pItem->sIK.ByteInfoKadr[3]=pItem->wId >> 8;
      pItem->sIK.ByteInfoKadr[4]=pItem->wId & 0xff;
      pItem->sIK.ByteInfoKadr[5]=7;
      strcpy((char *)(pItem->sIK.ByteInfoKadr+6),"SWITCH_MIC_OFF");
    }
    StopAudioFromMU2IPPM(wCP); // ���� ����� ������� �������!
    StopAudioFromIPPM2MU(wCP);
    DelConnectionPoint(wCP);
    sMasAbntIPPM[Numb].wCP2MU=0;
    return true;
  }
  return false;
}

bool IncomingCallFromAbonentIPPM(unsigned char AbntNumb,unsigned char *pbtRes)
{ // ����� ��������� �� ��� ������, � ������������� ������ �����!
  unsigned char PultNb,btLayer,btKey,btInd,btGroup,
                TypeAbon,btLed,btLedState;
  unsigned short NChan;
  bool bKey2Selector,bAbnt2AA,bIncoming;
  ABONENT_STATE_TP *psAbnt;

SendStr2IpLogger("$DEBUG$IncomingCallIPPM: Numb=%d",AbntNumb);
  *pbtRes=1;
  TypeAbon=IPPM;
  btGroup=GetTlfnGroup(AbntNumb);
  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  { // ����, ����� �� ���-���� ������������� ������� �����
    if(MassM1[PultNb] && IsKbdReady(PultNb) && IsKbdUnlock(PultNb))
    {
      btKey=FindAbntKey2Pult(PultNb,&btLayer,0,&TypeAbon,AbntNumb);
      if(btKey!=0xff)
      {
        if(!IsSelectorActive(PultNb))
        {
          if(IsKey2Selector(PultNb,btKey,btLayer,d_Abonent2SelectorOtherPult)) continue;
        }
        bAbnt2AA=IsGroupAutoAnswer4PultFromGroup(PultNb,btGroup);
SendStr2IpLogger("$DEBUG$IncomingCall IPPM IsGroupAutoAnswer4PultFromGroup:PultNb=%d, Numb=%d, group=%d, AA=%d",PultNb,AbntNumb,btGroup,bAbnt2AA);
        bKey2Selector=IsKey2Selector(PultNb,btKey,btLayer,d_Abonent2SelectorCurrentPult);
        if(bAbnt2AA || bKey2Selector)
        {
          psAbnt=GetFreeAbonentStatePtr();
          if(!psAbnt) return false;
          NChan=CreatVMCP(MassM1[PultNb]);
          if(NChan)
          {
            psAbnt->wConnectionPoint=NChan;
            psAbnt->wOperatorChanel=GetOperatorChannelFromKey(PultNb,btKey,btLayer);
            psAbnt->btPultNb=PultNb;
            psAbnt->btLayer=btLayer;
            psAbnt->btKey=btKey;
            psAbnt->NumbAbon=AbntNumb;
            psAbnt->btType=IPPM;
            psAbnt->dwTimerEnd=0;
            SetAbonentStateByPtr(psAbnt,Talk,dIncomingTalkFlag);
            SetCurrentAbonentIPPM(AbntNumb,psAbnt);
            SetLedColorIfTalk(psAbnt);
            if(bKey2Selector)
            {
SendStr2IpLogger("$DEBUG$IncomingCall IPPM AddTDCIPPM2CurrentSelector:PultNb=%d, Numb=%d, VMCP=%x",PultNb,AbntNumb,psAbnt->wConnectionPoint);
              if(AddTDCIPPM2CurrentSelector(PultNb,psAbnt))
              {
                SendIPPMCmdConnect(AbntNumb,psAbnt);
                *pbtRes=0;
              }
            }
            else
            {
              unsigned short wCP=GetFreeConnectionPoint();
              if(wCP!=0xff)
              {
                DSRS_On(psAbnt->btPultNb,psAbnt->wOperatorChanel);
                IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,1,1);
                sMasAbntIPPM[AbntNumb-1].wCP2MU=wCP;
SendStr2IpLogger("$DEBUG$IncomingCall IPPM AddAbnt2FreeHandConf:PultNb=%d, Numb=%d, wCP=%x",PultNb,AbntNumb,wCP);
                AddAbnt2FreeHandConf(PultNb,psAbnt);
                *pbtRes=0;
              }
            }
            return false;
          }
        }
      }
    }
  }
  bIncoming=false;
  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if(MassM1[PultNb] && IsKbdReady(PultNb) && IsKbdUnlock(PultNb))
    {
      btKey=FindAbntKey2Pult(PultNb,&btLayer,0,&TypeAbon,AbntNumb);
      if(btKey!=0xff)
      {
        if(!IsSelectorActive(PultNb))
        {
          if(IsKey2Selector(PultNb,btKey,btLayer,d_Abonent2SelectorOtherPult)) continue;
        }
        if(GetAbonentStatePtrFromKey(PultNb,btKey,btLayer)) break;
        psAbnt=GetFreeAbonentStatePtr();
        if(!psAbnt) return false;
        *pbtRes=0;
        psAbnt->wConnectionPoint=d_DummyCP;
        psAbnt->wOperatorChanel=GetOperatorChannelFromKey(PultNb,btKey,btLayer);
        psAbnt->btPultNb=PultNb;
        psAbnt->btLayer=btLayer;
        psAbnt->btKey=btKey;
        psAbnt->NumbAbon=AbntNumb;
        psAbnt->btType=IPPM;
        psAbnt->dwState=Off;
        psAbnt->btLedState=dLedStateIncomingCall;
        SetAbonentStateByPtr(psAbnt,IncomingCall,0);
        psAbnt->dwTimerEnd=MassTimer[TIMER_INCOMINGCALL]+xTaskGetTickCount();
        IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,3,3);
        SetCurrentAbonentIPPM(AbntNumb,psAbnt);
        bIncoming=true;
SendStr2IpLogger("$DEBUG$IncomingCall IPPM: AbntNumb=%d, Pult=%d",AbntNumb,PultNb);
      }
    }
  }
  return bIncoming;
}

void ClearIncomingCallIPPM4AllPults(unsigned char Numb)
{
  unsigned char PultNb;
  ABONENT_STATE_TP *psAbnt;

  sMasAbntIPPM[Numb-1].bIncomingCall=false;
  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if(MassM1[PultNb])
    {
      psAbnt=GetAbonentStatePtrByFind(PultNb,0xffff,0xffff,IPPM,Numb,NULL);
      if(psAbnt && (GetAbonentStateFromPtr(psAbnt)==IncomingCall))
      {
        IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,OFF,0,0);
        ClearAbonentStateByPtr(psAbnt);
      }
    }
  }
}

void MU2IPPM_SendAnswer(unsigned int dwIP,unsigned short wPort,unsigned short wSz,
                        unsigned char Numb,unsigned short wCmdAnsw,unsigned char btCmd,
                        unsigned char btRes,char *strInfo)
{
  static section ("bigdata") sInfoKadr sIK;

  sIK.Source=UserUDP;
  sIK.A1=dwIP;
  sIK.PORT1=wPort;
  sIK.L=wSz;
  sIK.ByteInfoKadr[0]=0xec;
  sIK.ByteInfoKadr[1]=0x0;
  sIK.ByteInfoKadr[2]=Numb;
  sIK.ByteInfoKadr[3]=wCmdAnsw >> 8;
  sIK.ByteInfoKadr[4]=wCmdAnsw & 0xff;
  sIK.ByteInfoKadr[5]=btCmd;
  sIK.ByteInfoKadr[6]=btRes;
  if(btCmd==1)
  { // �������������� ���� � ������ �� ������� ����������� (����� ����� � ���)
    sIK.L++;
    sIK.ByteInfoKadr[7]=1+s_dwLifeTmIPPM/1000;
    strcpy((char *)(sIK.ByteInfoKadr+8),strInfo);
  }
  else strcpy((char *)(sIK.ByteInfoKadr+7),strInfo);
  SendAnsw4IPPMCmd(Numb,wCmdAnsw,&sIK);
}

unsigned char CheckMaxPgmIPPM(void)
{
extern unsigned short LenLicIPPM; // ����� �������� IPPM
  int i,iPgmIPPM=0;

  for(i=0;i<s_btMaxIPPM;i++)
  {
    if(sMasAbntIPPM[i].dwIP_Private && sMasAbntIPPM[i].bPgmIPPM) iPgmIPPM++;
  }
  if(iPgmIPPM>=LenLicIPPM) return 1; // ������ ��������
  return 0;
}

bool ExistIPPM2Pults(unsigned char Numb)
{
  unsigned char PultNb,btKey,btLayer;

  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    for(btLayer=0;btLayer<GetMaxLayer(PultNb);btLayer++)
    {
      for(btKey=0;btKey<NumberKey;btKey++)
      {
        if(IPPM==MassKey[PultNb][btLayer][btKey].TypeAbon)
        {
          if(MassKey[PultNb][btLayer][btKey].NumbAbon==Numb) return true;
        }
      }
    }
  }
  return(false);
}

char *MAC2STR(unsigned char *btMAC)
{
  static char strMAC[80];
  LockPrintf();
  sprintf(strMAC,"0x%02x:%02x:%02x:%02x:%02x:%02x",btMAC[0],btMAC[1],btMAC[2],btMAC[3],btMAC[4],btMAC[5]);
  UnlockPrintf();
  return strMAC;
}

void IPPM_CmdRegistration(unsigned int dwIP,unsigned short wPort,
                          unsigned char Numb,unsigned short wCmdAnsw,unsigned char *pInfo)
{
  unsigned char btRes=0;

  if((dwIP==0) || (wPort==0)) return;
  Numb--;
  vPortEnableSwitchTask(false);
  if(sMasAbntIPPM[Numb].dwIP_Private==0)
  {
    if(pInfo[0]) btRes=CheckMaxPgmIPPM();
    if(btRes==0)
    {
      if(ExistIPPM2Pults(Numb+1))
      {
        memset(sMasAbntIPPM+Numb,0,sizeof(IPPM_ABNT_TP));
        sMasAbntIPPM[Numb].dwIP_Private=dwIP;
        sMasAbntIPPM[Numb].wPort=wPort;
        sMasAbntIPPM[Numb].bPgmIPPM=pInfo[0];
        sMasAbntIPPM[Numb].btStudioNumb=pInfo[1];
        sMasAbntIPPM[Numb].btSpeeker=pInfo[2];
        sMasAbntIPPM[Numb].dwTm4Send=xTaskGetTickCount()+(s_dwLifeTmIPPM >> 1);
        memcpy(sMasAbntIPPM[Numb].btMAC,pInfo+4,6);
SendStr2IpLogger("$DEBUG$IPPM_CmdRegistration: Numb=%d, dwIP_Private=%x, wPort=%d, MAC=%s",
                 Numb+1,sMasAbntIPPM[Numb].dwIP_Private,sMasAbntIPPM[Numb].wPort,MAC2STR(sMasAbntIPPM[Numb].btMAC));
      }
      else btRes=2; // ��� �������� ��� ������� �� �� ����� �� �������
    }
  }
  else
  {
    if(sMasAbntIPPM[Numb].dwIP_Private!=dwIP) btRes=3; // ������� ��������������� ��� ������� IP-������
  }
  vPortEnableSwitchTask(true);
  MU2IPPM_SendAnswer(dwIP,wPort,29,Numb+1,wCmdAnsw,1,btRes,"ANSWER_ON_REGISTRATION");
}

unsigned char IPPM_CmdReqMicON4Connect(unsigned char Numb)
{
  ABONENT_STATE_TP *psAbnt=sMasAbntIPPM[Numb-1].psAbnt;

  if(psAbnt)
  {
    unsigned char SN=MassM1[psAbnt->btPultNb];
    unsigned int dwIP=GetMIPAddr(SN);
    unsigned short wCP=GetCPFromVMCP(psAbnt->wConnectionPoint);

SendStr2IpLogger("$DEBUG$IPPM_CmdReqMicON4Connect: SN=%d, dwIP=%08x, wCP=%x (virtual=%x)",SN,dwIP,wCP,psAbnt->wConnectionPoint);
    if(!wCP)
    {
      wCP=CreatMCP(SN);
      if(!dwIP || !wCP)
      {
        OtboiAbonent(psAbnt->btPultNb,psAbnt->btKey,psAbnt->btLayer);
        return 1;
      }
      StartAudioFromMU2IPPM_Multicast(dwIP,wCP);
    }
    SetLedColorIfTalk(psAbnt);
    IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,0,0);
    SetAbonentStateByPtr(psAbnt,Talk,0);
    IsdnIPPMAbntSelectorAnswer(psAbnt->btPultNb,psAbnt->btKey);
    if(IsSelectorWorkState(psAbnt->btPultNb)) SetAbnt2SelectorStatus(psAbnt->btPultNb,psAbnt->btKey,psAbnt->btLayer);
    psAbnt->dwTimerEnd=0;
    return 0;
  }
  return 1;
}

bool IsMaxCountSpeakIPPM2UK(void)
{
  unsigned char i,btCount=0;
  ABONENT_STATE_TP *psAbnt;

  for(i=0;i<s_btMaxIPPM;i++)
  {
    if(sMasAbntIPPM[i].dwIP_Private)
    {
      psAbnt=sMasAbntIPPM[i].psAbnt;
      if(psAbnt)
      {
        if(GetAbonentStateFromPtr(psAbnt)==Talk)
        {
          if(psAbnt->wConnectionPoint<d_DummyCP) btCount++;
          else
            if(sMasAbntIPPM[i].dwSpeakStart) btCount++;
        }
      }
    }
  }
  return (btCount>=s_btSpeakIPPM2UK);
}

void StopSpeakIPPM2SelectorIfNeed(unsigned char btPultNb)
{
  unsigned char i,btInd=s_btMaxIPPM,btCount=0;
  unsigned int dwSpeakOld=xTaskGetTickCount();
  ABONENT_STATE_TP *psAbnt;

  for(i=0;i<s_btMaxIPPM;i++)
  {
    if(sMasAbntIPPM[i].dwIP_Private)
    {
      psAbnt=sMasAbntIPPM[i].psAbnt;
      if(psAbnt && (psAbnt->btPultNb==btPultNb))
      {
        if(IsKey2Selector(btPultNb,psAbnt->btKey,psAbnt->btLayer,d_Abonent2SelectorCurrentPult))
        {
          if(GetAbonentStateFromPtr(psAbnt)==Talk)
          {
            if(sMasAbntIPPM[i].dwSpeakStart)
            {
              if(IsTickCountLT(sMasAbntIPPM[i].dwSpeakStart,dwSpeakOld))
              { btInd=i; dwSpeakOld=sMasAbntIPPM[i].dwSpeakStart; }
              btCount++;
            }
          }
        }
      }
    }
  }
  if((btCount>=s_btSpeakIPPM2Selector) && (btInd<s_btMaxIPPM))
  {
    unsigned short NChan;
    psAbnt=sMasAbntIPPM[btInd].psAbnt;
    NChan=GetCP_IPPM2MU(psAbnt->NumbAbon);
    if(NChan)
    {
      _xDelAbntFromSelector(btPultNb,NChan);
      DelCP_IPPM2MU(psAbnt->NumbAbon);
      SendIPPMCmdConnect(psAbnt->NumbAbon,psAbnt);
    }
  }
}

unsigned char IPPM_CmdReqMicON4ConnectPrivate(unsigned char Numb)
{
  ABONENT_STATE_TP *psAbnt=sMasAbntIPPM[Numb-1].psAbnt;
  if(psAbnt)
  {
    if(IsMaxCountSpeakIPPM2UK()) return 1;
    Commute(true,psAbnt->wOperatorChanel,psAbnt->wConnectionPoint);
    Commute(true,psAbnt->wConnectionPoint,psAbnt->wOperatorChanel);
    StartAudioFromMU2IPPM_Private(Numb,psAbnt->wConnectionPoint);
    StartAudioFromIPPM2MU(psAbnt->wConnectionPoint);
    DSRS_On(psAbnt->btPultNb,psAbnt->wOperatorChanel);
    SetLedColorIfTalk(psAbnt);
    IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,1,1);
    SetAbonentStateByPtr(psAbnt,Talk,0);
    psAbnt->dwTimerEnd=0;
    return 0;
  }
  return 1;
}

bool IsEnableSpeaker2Studio(unsigned char Numb)
{
  unsigned char i,btStudioNumb,btSpeeker;

  Numb--;
  btStudioNumb=sMasAbntIPPM[Numb].btStudioNumb;
  btSpeeker=sMasAbntIPPM[Numb].btSpeeker;
SendStr2IpLogger("$DEBUG$IsEnableSpeaker2Studio Numb=%d, btStudioNumb=%d, btSpeeker=%d",Numb+1,btStudioNumb,btSpeeker);
  if(btStudioNumb)
  {
    for(i=0;i<s_btMaxIPPM;i++)
    {
      if((i!=Numb) && sMasAbntIPPM[i].dwIP_Private)
      {
        if(sMasAbntIPPM[i].btStudioNumb==btStudioNumb)
        {
          if(sMasAbntIPPM[i].wCP2MU)
          { // ������ ���� ������� � ������ ����� ��������!
            if(sMasAbntIPPM[i].btSpeeker<btSpeeker) return false;
            _xDelAbntFromSelector(sMasAbntIPPM[i].psAbnt->btPultNb,sMasAbntIPPM[i].wCP2MU);
            SendIPPMCmdConnect(i+1,sMasAbntIPPM[i].psAbnt);
            DelCP_IPPM2MU(i+1);
            return true;
          }
        }
      }
    }
  }
  return true;
}

unsigned char EnableSpeakIPPM(unsigned char Numb)
{ // ������ �� ��������� ��������� � ������ ��������� ��� ���������������
  unsigned short wCP;
  ABONENT_STATE_TP *psAbnt=sMasAbntIPPM[Numb-1].psAbnt;

SendStr2IpLogger("$DEBUG$EnableSpeakIPPM Numb=%d, wCP2MU=%x",Numb,sMasAbntIPPM[Numb-1].wCP2MU);
  if(IsKey2Selector(psAbnt->btPultNb,psAbnt->btKey,psAbnt->btLayer,d_Abonent2SelectorCurrentPult))
  { // ������ �� ��������� ��������� IPPM � ���������
    if(sMasAbntIPPM[Numb-1].wCP2MU) return 0;
    if(IsAbonentListener(psAbnt->btPultNb,psAbnt->btKey)) return 1;
    if(IsMaxCountSpeakIPPM2UK()) return 1;
    if(IsEnableSpeaker2Studio(Numb))
    {
      StopSpeakIPPM2SelectorIfNeed(psAbnt->btPultNb);
      wCP=GetFreeConnectionPoint();
      if(wCP!=0xff)
      {
        sMasAbntIPPM[Numb-1].wCP2MU=wCP;
        _xAddAbnt2Selector(psAbnt->btPultNb,wCP);
        SendIPPMCmdConnectPrivate(Numb);
        StartAudioFromIPPM2MU(wCP);
        StartAudioFromMU2IPPM_Private(Numb,wCP);
        return 0;
      }
    }
    return 1;
  }
  else
  {
    if(IsMaxCountSpeakIPPM2UK()) return 1;
    SendIPPMCmdConnectPrivate(Numb);
    // ������� � ��������������� wCP2MU ��� ����!
    StartAudioFromIPPM2MU(sMasAbntIPPM[Numb-1].wCP2MU);
    StartAudioFromMU2IPPM_Private(Numb,sMasAbntIPPM[Numb-1].wCP2MU);
    return 0;
  }
}

void IPPM_CmdReqMicON(unsigned int dwIP,unsigned short wPort,
                      unsigned char Numb,unsigned short wCmdAnsw)
{
  unsigned char btRes=1;

  if((dwIP==0) || (wPort==0)) return;
SendStr2IpLogger("$DEBUG$IPPM_CmdReqMicON Numb=%d, dwIP_Private=%x,bIncomingCall=%d",Numb,sMasAbntIPPM[Numb-1].dwIP_Private,sMasAbntIPPM[Numb-1].bIncomingCall);
  Numb--;
  if(sMasAbntIPPM[Numb].dwIP_Private==dwIP)
  {
    if(sMasAbntIPPM[Numb].bIncomingCall) btRes=1;
    else
    {
      ABONENT_STATE_TP *psAbnt=sMasAbntIPPM[Numb].psAbnt;
      if(psAbnt)
      { // ��� ��������� ����� �������� IPPM ��� ������� ����� �������� �������� � ������ ��������� ��� ���������������
        if(psAbnt->wConnectionPoint<d_DummyCP)
        { // ��� ��������� ����� �������� IPPM � ������ ������
          if(GetAbonentStateFromPtr(psAbnt)==Call) btRes=IPPM_CmdReqMicON4ConnectPrivate(Numb+1);
          else btRes=0;
        }
        else
        {
          unsigned char btState=GetAbonentStateFromPtr(psAbnt);
          if(btState==Call)
          { // ��� ��������� ����� �������� IPPM � �������������� ������ (���� ���������)
            btRes=IPPM_CmdReqMicON4Connect(Numb+1);
          }
          else
          { // ������� ����� �������� �������� � ������ ��������� ��� ���������������
            btRes=EnableSpeakIPPM(Numb+1);
          }
        }
      }
      else
      { sMasAbntIPPM[Numb].bIncomingCall=IncomingCallFromAbonentIPPM(Numb+1,&btRes); }
    }
SendStr2IpLogger("$DEBUG$IPPM_CmdReqMicON Numb=%d, btRes=%d",Numb+1,btRes);
    MU2IPPM_SendAnswer(dwIP,wPort,31,Numb+1,wCmdAnsw,6,btRes,"ANSWER_ON_REQUEST_MIC_ON");
  }
}

void IPPM_CmdUserMicOFF(unsigned int dwIP,unsigned short wPort,
                        unsigned char Numb,unsigned short wCmdAnsw)
{
  if((dwIP==0) || (wPort==0)) return;
  if(sMasAbntIPPM[Numb-1].dwIP_Private==dwIP)
  {
    ABONENT_STATE_TP *psAbnt=sMasAbntIPPM[Numb-1].psAbnt;
    if(psAbnt && (psAbnt->wConnectionPoint>d_DummyCP))
    { // ������� ��������� ��� ���������������
      if(IsKey2Selector(psAbnt->btPultNb,psAbnt->btKey,psAbnt->btLayer,d_Abonent2SelectorCurrentPult))
      {
        _xDelAbntFromSelector(psAbnt->btPultNb,sMasAbntIPPM[Numb-1].wCP2MU);
        SendIPPMCmdConnect(Numb,psAbnt);
        DelCP_IPPM2MU(Numb);
      }
      else
      { // ��� ���������������
        SendIPPMCmdConnect(Numb,psAbnt);
        StopAudioFromMU2IPPM(sMasAbntIPPM[Numb-1].wCP2MU);
        StopAudioFromIPPM2MU(sMasAbntIPPM[Numb-1].wCP2MU);
      }
    }
    MU2IPPM_SendAnswer(dwIP,wPort,29,Numb,wCmdAnsw,8,0,"ANSWER_ON_USER_MIC_OFF");
  }
}

/*void IPPM_CmdConfirmConnect(unsigned int dwIP,unsigned short wPort,
                            unsigned char Numb,unsigned short wCmdAnsw)
{
  unsigned char btRes=1;
  if((dwIP==0) || (wPort==0) || (Numb<1)) return;
  if(sMasAbntIPPM[Numb-1].dwIP_Private==dwIP)
  {
    ABONENT_STATE_TP *psAbnt=sMasAbntIPPM[Numb-1].psAbnt;
    if(psAbnt && (psAbnt->wConnectionPoint>d_DummyCP))
    { // ������� ��������� ��� ���������������
      if(IsKey2Selector(psAbnt->btPultNb,psAbnt->btKey,psAbnt->btLayer,d_Abonent2SelectorCurrentPult))
      { // ������� � ��������� - ������ ��������� � "Call" �� "SelectorListen"
        btRes=0;
        SetAbonentStateByPtr(psAbnt,SelectorListen,0);
        IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,0,0);
        psAbnt->dwTimerEnd=0;
      }
    }
    MU2IPPM_SendAnswer(dwIP,wPort,32,Numb,wCmdAnsw,8,btRes,"ANSWER_ON_CONFIRM_CONNECT");
  }
}*/

void IPPM_AnswGetState(unsigned int dwIP,unsigned short wPort,
                       unsigned char Numb,unsigned short wCmdAnsw,unsigned char btRes)
{
//SendStr2IpLogger("$DEBUG$IPPM ANSWER GET_STATE: Numb=%d, drIP_Private=%x, wCmd=%d",Numb,sMasAbntIPPM[Numb-1].dwIP_Private,wCmdAnsw);
}

void IPPM_AnswConnect(unsigned int dwIP,unsigned short wPort,
                      unsigned char Numb,unsigned short wCmdAnsw,unsigned char btRes)
{
}

void IPPM_AnswConnectPrivate(unsigned int dwIP,unsigned short wPort,
                             unsigned char Numb,unsigned short wCmdAnsw,unsigned char btRes)
{
}

void IPPM_AnswDisconnect(unsigned int dwIP,unsigned short wPort,
                         unsigned char Numb,unsigned short wCmdAnsw,unsigned char btRes)
{
}

void IPPM_AnswSwitchMicOff(unsigned int dwIP,unsigned short wPort,
                           unsigned char Numb,unsigned short wCmdAnsw,unsigned char btRes)
{
}

void IPPM_Event(sInfoKadr *psVxCallT)
{
  unsigned short wCmdAnsw;
  unsigned char Numb,*pUdpBuf=psVxCallT->ByteInfoKadr+4;

  Numb=pUdpBuf[2];
  if((Numb>s_btMaxIPPM) || (Numb<1) || (pUdpBuf[3]!=0)) return;
  wCmdAnsw=pUdpBuf[4]; wCmdAnsw=(wCmdAnsw << 8) | pUdpBuf[5];
  if(pUdpBuf[1]==0xeb)
  { // ������� �� IPPM � ��
    if(sMasAbntIPPM[Numb-1].dwIP_Private)
    {
      if(RepeatCmdIPPM(Numb,wCmdAnsw)) return;
    }
//SendStr2IpLogger("$DEBUG$IPPM_Event CmdEB: Numb=%d, CmdId=%d",Numb,pUdpBuf[6]);
    switch(pUdpBuf[6])
    {
      case 1: // REGISTRATION
        IPPM_CmdRegistration(psVxCallT->A1,psVxCallT->PORT1,Numb,wCmdAnsw,pUdpBuf+7);
        break;
      case 6: // REQUEST_MIC_ON
        IPPM_CmdReqMicON(psVxCallT->A1,psVxCallT->PORT1,Numb,wCmdAnsw);
        break;
      case 8: // USER_MIC_OFF
        IPPM_CmdUserMicOFF(psVxCallT->A1,psVxCallT->PORT1,Numb,wCmdAnsw);
        break;
/*      case 9: // CONFIRM_CONNECT
        IPPM_CmdConfirmConnect(psVxCallT->A1,psVxCallT->PORT1,Numb,wCmdAnsw);
        break;*/
      default: break;
    }
  }
  else
  {
    if(pUdpBuf[1]==0xea)
    { // ����� �� IPPM �� ������� ��
      if(!IPPM_RcvAnsw(wCmdAnsw,pUdpBuf[7])) return;
//if(pUdpBuf[6]!=2) SendStr2IpLogger("$DEBUG$IPPM_Event CmdEA: Numb=%d, cmdId=%d",Numb,pUdpBuf[6]);
      switch(pUdpBuf[6])
      {
        case 2: // 
          IPPM_AnswGetState(psVxCallT->A1,psVxCallT->PORT1,Numb,wCmdAnsw,pUdpBuf[7]);
          break;
        case 3: // 
          IPPM_AnswConnect(psVxCallT->A1,psVxCallT->PORT1,Numb,wCmdAnsw,pUdpBuf[7]);
          break;
        case 4: // 
          IPPM_AnswConnectPrivate(psVxCallT->A1,psVxCallT->PORT1,Numb,wCmdAnsw,pUdpBuf[7]);
          break;
        case 5: // 
          IPPM_AnswDisconnect(psVxCallT->A1,psVxCallT->PORT1,Numb,wCmdAnsw,pUdpBuf[7]);
          break;
        case 7: // 
          IPPM_AnswSwitchMicOff(psVxCallT->A1,psVxCallT->PORT1,Numb,wCmdAnsw,pUdpBuf[7]);
          break;
        default: break;
      }
    }
  }
}

void InitIPPM(void)
{
  InitRepeatCmdIPPM();
  ReadPultMulticastInfo();
  memset(sMasAbntIPPM,0,sizeof(sMasAbntIPPM));
}

void IPPM_CmdGetStateNoAnswer2MU(LIST_MU2IPPM_TP *pItem)
{ // ��� ������ �� IPPM �� GET_STATE
  unsigned char i=pItem->Numb-1;
  ABONENT_STATE_TP *psAbnt;

  vPortEnableSwitchTask(false);
SendStr2IpLogger("$DEBUG$IPPM registration end: Numb=%d, dwIP_Private=%x",i+1,sMasAbntIPPM[i].dwIP_Private);
  ClearQueueCmd4IPPM(i+1);
  psAbnt=sMasAbntIPPM[i].psAbnt;
  if(psAbnt) OtboiAbonent(psAbnt->btPultNb,psAbnt->btKey,psAbnt->btLayer);
  memset(sMasAbntIPPM+i,0,sizeof(IPPM_ABNT_TP));
  vPortEnableSwitchTask(true);
}

void IPPM_IRPTOut(void)
{
  int i;
  unsigned short wId;

  for(i=0;i<s_btMaxIPPM;i++)
  {
    if(sMasAbntIPPM[i].dwIP_Private)
    {
      if(IsCurrentTickCountGT(sMasAbntIPPM[i].dwTm4Send))
      {
        LIST_MU2IPPM_TP *pItem=ListMU2IPPM_AddItem(NULL);
        if(pItem)
        {
          pItem->Numb=i+1;
          pItem->sIK.A1=sMasAbntIPPM[i].dwIP_Private;
          pItem->sIK.PORT1=sMasAbntIPPM[i].wPort;
          pItem->sIK.L=6;
          pItem->sIK.ByteInfoKadr[0]=0xed;
          pItem->sIK.ByteInfoKadr[1]=0x0;
          pItem->sIK.ByteInfoKadr[2]=i+1;
          pItem->sIK.ByteInfoKadr[3]=pItem->wId >> 8;
          pItem->sIK.ByteInfoKadr[4]=pItem->wId & 0xff;
          pItem->sIK.ByteInfoKadr[5]=2;
          pItem->pFunc_IPPMNoAnswer2MU=IPPM_CmdGetStateNoAnswer2MU;
          sMasAbntIPPM[i].dwTm4Send=xTaskGetTickCount()+s_dwLifeTmIPPM;
//SendStr2IpLogger("$DEBUG$IPPM GET_STATE: Numb=%d, wId=%d, dwIP_Private=%x",i+1,pItem->wId,sMasAbntIPPM[i].dwIP_Private);
        }
      }
    }
  }
}

void OtboiAbonentIPPM(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  unsigned char Numb;

  Numb=MassKey[PultNb][btLayer][Key].NumbAbon;
  if(Numb && (Numb<=s_btMaxIPPM) && (sMasAbntIPPM[Numb-1].dwIP_Private))
  {
    Numb--;
    if(sMasAbntIPPM[Numb].psAbnt)
    {
      unsigned short wCP=sMasAbntIPPM[Numb].psAbnt->wConnectionPoint;
      LIST_MU2IPPM_TP *pItem=ListMU2IPPM_AddItem(NULL);
      if(pItem)
      {
        pItem->Numb=Numb+1;
        pItem->sIK.A1=sMasAbntIPPM[Numb].dwIP_Private;
        pItem->sIK.PORT1=sMasAbntIPPM[Numb].wPort;
        pItem->sIK.L=16;
        pItem->sIK.ByteInfoKadr[0]=0xed;
        pItem->sIK.ByteInfoKadr[1]=0x0;
        pItem->sIK.ByteInfoKadr[2]=Numb+1;
        pItem->sIK.ByteInfoKadr[3]=pItem->wId >> 8;
        pItem->sIK.ByteInfoKadr[4]=pItem->wId & 0xff;
        pItem->sIK.ByteInfoKadr[5]=5;
        strcpy((char *)(pItem->sIK.ByteInfoKadr+6),"DISCONNECT");
      }
      // ���������� �� ��� ����������� (���� ��������) � ������� ������ �� ���������,����������� ��� ���������������
      if(wCP && (wCP<d_DummyCP))
      {
        StopAudioFromMU2IPPM(wCP);
        StopAudioFromIPPM2MU(wCP);
        DelConnectionPoint(wCP);
      }
      ClearIncomingCallIPPM4AllPults(Numb+1);
      sMasAbntIPPM[Numb].psAbnt=0;
    }
  }
  OtboiAbonentCommonPart(PultNb,Key,btLayer,NULL,NULL);
}

