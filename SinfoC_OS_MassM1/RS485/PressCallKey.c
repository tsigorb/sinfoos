#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

//*******************************************************************
static bool s_bManualOtboi[NumberKeyboard];

//*******************************************************************
bool IsFreeISDNAbonent(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  unsigned char btInd,NumbAbon;
  ABONENT_STATE_TP *psAbnt;

  NumbAbon=MassKey[PultNb][btLayer][Key].NumbAbon;
  if(!NumbAbon) return(true);
  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return(true);
    if((psAbnt->wConnectionPoint!=0xffff) && (psAbnt->btType==ISDN))
    {
      if(TlfnEqual(psAbnt->btTlfn,MassTel[NumbAbon-1])) return(false);
    }
    btInd++;
  }
  while(1);
}

bool IsFreeMKALine(unsigned char PultNb,unsigned char Key,unsigned char btLayer,unsigned char NumbAbon)
{
  unsigned char NChan,btInd;
  ABONENT_STATE_TP *psAbnt;

  NChan=MassKey[PultNb][btLayer][Key].NChan;
  if(!NChan) return(false);
  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return(true);
    if(psAbnt->wConnectionPoint==NChan)
    {
      if(NumbAbon!=0xff)
      {
        if(!NumbAbon || (NumbAbon==psAbnt->NumbAbon)) return(false);
      }
      else return(false);
    }
    btInd++;
  }
  while(1);
}

bool IsFreeIPPMAbonent(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  unsigned char btInd,NumbAbon;
  ABONENT_STATE_TP *psAbnt;

  NumbAbon=MassKey[PultNb][btLayer][Key].NumbAbon;
  if(!NumbAbon) return(false);
  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return(true);
    if((psAbnt->wConnectionPoint!=0xffff) && (psAbnt->btType==IPPM))
    {
      if(psAbnt->NumbAbon==NumbAbon) return(false);
    }
    btInd++;
  }
  while(1);
}

bool IsFreeAbonentFromKey(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  switch(MassKey[PultNb][btLayer][Key].TypeAbon)
  {
    case ISDN:
      return(IsFreeISDNAbonent(PultNb,Key,btLayer));
    case TDC:
      return(IsFreeMKALine(PultNb,Key,btLayer,MassKey[PultNb][btLayer][Key].NumbAbon));
    case Selector:
      return(!IsKey2Selector(PultNb,Key,btLayer,d_Abonent2SelectorOtherPult));
    case IPPM:
      return(IsFreeIPPMAbonent(PultNb,Key,btLayer));
  }
  return(false);
}

void PutAbntNumb2Display(unsigned char PultNb,ABONENT_STATE_TP *psAbnt)
{
  ListAbntDisplay[PultNb][0].NChan=GetAbonentStateIndexFromPtr(psAbnt);
  Tlfn2Str(ListAbntDisplay[PultNb][0].StrInf,psAbnt->btTlfn);
  if(!ListAbntDisplay[PultNb][0].StrInf[0]) strcpy(ListAbntDisplay[PultNb][0].StrInf,"����� �� ���������!");
  ClearScreen(PultNb);
  OutputString(PultNb,"    �������:",0,3,0);
  OutputString(PultNb,ListAbntDisplay[PultNb][0].StrInf,0,3,1);
}

void Control_ATC_EDSS_IPPM_TOut(void)
{
  unsigned char btInd,btState;
  ABONENT_STATE_TP *psAbnt;

  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return;
    btInd++;
    if((psAbnt->wConnectionPoint!=0xffff) &&
       ((psAbnt->btType==ISDN) || (psAbnt->btType==ATC) || (psAbnt->btType==IPPM)))
    {
      if(psAbnt->dwTimerEnd && IsCurrentTickCountGT(psAbnt->dwTimerEnd))
      {
        OtboiAbonent(psAbnt->btPultNb,psAbnt->btKey,psAbnt->btLayer);
        return;
      }
    }
  }
  while(1);
}

void SendExtensionNumber(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  unsigned char *pbtMasDg,Ln;
  ABONENT_STATE_TP *psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);

  if(psAbnt && GetOutgoingNumberTmp(PultNb,&pbtMasDg,&Ln))
  {
    switch(psAbnt->btType)
    {
      case ATC:
      case ISDN:
{
  static char strNumb[25];
  Tlfn2Str(strNumb,pbtMasDg);
  SendStr2IpLogger("$DEBUG$SendExtensionNumber: PultNb=%d, Key=%d, wChan=%x, Numb=%s",
                   PultNb,Key,psAbnt->wConnectionPoint,strNumb);
}
        _xOutgoingCallDTMF(psAbnt->wConnectionPoint,pbtMasDg,Ln);
        break;
    }
    OutgoingNumberTmpTOutClear(PultNb);
  }
}

void PressCallKey_ISDN_ATC(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  unsigned char TypeAbon=MassKey[PultNb][btLayer][Key].TypeAbon;
  ABONENT_STATE_TP *psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);

  if(!psAbnt)
  {
    if(IsKey2Selector(PultNb,Key,btLayer,d_Abonent2SelectorOtherPult))
    { PutErrInfo2Display(PultNb,NULL); return; }
    if(TypeAbon==ISDN)
    {
      if(MassKey[PultNb][btLayer][Key].NumbAbon)
      {
        OutgoingNumberTmpClear(PultNb);
        IndicClock(PultNb);
      }
      if(IsFreeISDNAbonent(PultNb,Key,btLayer)) OutgoingCallISDN(PultNb,Key,btLayer);
    }
    else
    {
      if(IsFreeMKALine(PultNb,Key,btLayer,0xff)) OutgoingCallATC(PultNb,Key,btLayer,0xff);
    }
  }
  else
  {
    switch(GetAbonentStateFromPtr(psAbnt))
    {
      case IncomingCall:
      {
        if(IsKey2Selector(PultNb,Key,btLayer,d_Abonent2SelectorOtherPult))
        { PutErrInfo2Display(PultNb,NULL); return; }
        if(TypeAbon==ISDN) AnswerIncomingCallISDN(PultNb,psAbnt,true);
        else AnswerIncomingCallATC(PultNb,psAbnt);
        break;
      }
      case Talk:
      {
        if(IsKey2Selector(PultNb,Key,btLayer,d_Abonent2SelectorOtherPult))
        { PutErrInfo2Display(PultNb,NULL); return; }
        if(IsSelectorActive(PultNb) && IsPressAddMember(PultNb))
        {
          if(AddAbonent2Selector(PultNb,Key,btLayer))
          {
//            if(IsOutgoingNumberTmpPresent(PultNb))
            {
              OutgoingNumberTmpClear(PultNb);
              IndicClock(PultNb);
            }
          }
        }
        else
        {
          if(IsOutgoingNumberTmpPresent(PultNb)) SendExtensionNumber(PultNb,Key,btLayer);
          else
          {
            if(IsSelectorActive(PultNb)) return;
            if(HoldOn(PultNb,Key,btLayer,true))
            {
              if(!psAbnt->NumbAbon)
              {
                ListAbntDisplay[PultNb][0].NChan=0xff;
                IndicClock(PultNb);
              }
            }
          }
        }
        break;
      }
      case Hold:
      {
        if(HoldOn(PultNb,Key,btLayer,false))
        {
          if(!psAbnt->NumbAbon) PutAbntNumb2Display(PultNb,psAbnt);
        }
        break;
      }
      default: break;
    }
  }
}

void PressCallKey_NoCall(unsigned char PultNb,unsigned char Key)
{
  if(IsCreatedTransit(PultNb)) XorAbnt2Transit(PultNb,Key);
/*  else
  { ??? 
  }*/
}

void SetManualOtboi(unsigned char PultNb,bool bSet)
{ s_bManualOtboi[PultNb]=bSet; }

bool IsManualOtboi(unsigned char PultNb)
{ return(s_bManualOtboi[PultNb]); }

void PressCallKey(unsigned char PultNb,unsigned char Key,bool bOtklKey,bool bTest)
{
  unsigned char btInd,btLayer,TypeAbon;

  ClearAbonentIndex4Digit(PultNb,0xff);
  btLayer=GetCurrentLayer(PultNb);
  TypeAbon=MassKey[PultNb][btLayer][Key].TypeAbon;
  if((TypeAbon!=ISDN) && (TypeAbon!=ATC) && IsOutgoingNumberTmpPresent(PultNb))
  {
    OutgoingNumberTmpClear(PultNb);
    IndicClock(PultNb);
  }
  if(bOtklKey)
  {
//SendStr2IpLogger("$DEBUG$PressCallKey otboi abonent!");
    SetManualOtboi(PultNb,true);
    OtboiAbonent(PultNb,Key,btLayer);
    SetManualOtboi(PultNb,false);
  }
  else
  {
    if(IsKey2Selector(PultNb,Key,btLayer,d_Abonent2SelectorOtherPult))
    { PutErrInfo2Display(PultNb,NULL); return; }
    if(IsConferenceActive(PultNb))
    { AddAbonent2Conference(PultNb,Key,btLayer); return; }
    if(IsAbonent2Conference(PultNb,Key,btLayer)) return;
    if(!IsSelectorActive(PultNb) && IsPressAddMember(PultNb))
    { // ��������� �������� � ��������������� � ������� ������� "+ ��������" (���� �� ���� �������� �� ���� ������)
      ABONENT_STATE_TP *psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);
      if(psAbnt && (GetAbonentStateFromPtr(psAbnt)==Talk)) AddAbnt2FreeHandConf(PultNb,psAbnt);
      return;
    }
    switch(TypeAbon)
    { // ��� �������� �������
      case ISDN:
      case ATC:
        if(PressCallKey4Selector(PultNb,Key,btLayer)) return;
        PressCallKey_ISDN_ATC(PultNb,Key,btLayer);
        break;
      case TDC:
        if(PressCallKey4Selector(PultNb,Key,btLayer)) return;
        PressCallKey_TDC(PultNb,Key,btLayer,bTest);
        break;
      case GroupTDC:
        PressCallKey_GroupTDC(PultNb,Key,btLayer);
        break;
      case Selector:
        if(PressCallKey4Selector(PultNb,Key,btLayer)) return;
        PressCallKey_Selector(PultNb,Key,btLayer);
        break;
      case PultCall:
//        if(PressCallKey4Selector(PultNb,Key,btLayer)) return;
        PressCallKey_PultCall(PultNb,Key,btLayer,bTest);
        break;
      case Level:
        PressCallKey_Level(PultNb,Key,btLayer);
        break;
      case PPSR:
        PressCallKey_PPSR(PultNb,Key,btLayer);
        break;
      case RSDT:
        PressCallKey_RSDT(PultNb,Key,btLayer);
        break;
      case NoCall:
        PressCallKey_NoCall(PultNb,Key);
        break;
      case Spec1:
      case Spec2:
      case Spec3:
      case Spec4:
      case Spec5:
      case Spec6:
      case Spec7:
      case Spec8:
        PressCallKey_OutSPEC(PultNb,Key,btLayer);
        break;
      case IPPM:
        if(PressCallKey4Selector(PultNb,Key,btLayer)) return;
        PressCallKey_IPPM(PultNb,Key,btLayer,bTest);
        break;
      default:
        break;
    }
  }
}

void ReleaseCallKey(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  if(IsSelectorActive(PultNb)) RestoreAbonentToSelector(PultNb,Key,btLayer);
}

