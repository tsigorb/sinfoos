/////////////////////////////////////////////////////////////////////
/*
������ ������ PultMulticastInfo:
MAXIPPM = 64
SN : IP address ������ D (224.0.0.0-239.255.255.255)
21 : 224.1.1.1
*/
void PultMulticastDecodeStr(char *pStr0)
{
  char *pPult,*pAddrIP,*pStr;
  unsigned int dwPult,dwIP;
  int i;

  if(!pStr0 || !pStr0[0]) return;
  pStr=strstr(pStr0,"MAXIPPM");
  if(pStr)
  {
    pStr=strchr(pStr+7,'=');
    if(!pStr) return;
    pStr=strclr(pStr+1);
    if(!pStr) return;
    i=atoi(pStr);
    if((i<1) || (i>d_MaxAbntIPPM)) return;
    s_btMaxIPPM=i;
    s_dwLifeTmIPPM=1000*(i>>3)+(d_MU2IPPM_CmdTOut*(d_MU2IPPMMaxRepeat+1));
    return;
  }
  pPult=pStr0;
  pAddrIP=strchr(pStr0,':');
  if(!pAddrIP) return;
  *pAddrIP=0; pAddrIP++;
  pPult=strclr(pPult);
  pAddrIP=strclr(pAddrIP);
  if(pPult && pAddrIP)
  {
    dwPult=atoi(pPult);
    dwIP=atoIP(pAddrIP);
    if(dwPult && dwIP)
    { // ��������� ���� �  sMasAbntIPPM
      for(i=0;i<NumberKeyboard;i++)
      {
        if(g_MasPultMulticast[i].SN==dwPult) return;
      }
      for(i=0;i<NumberKeyboard;i++)
      {
        if(g_MasPultMulticast[i].SN==0)
        {
          g_MasPultMulticast[i].SN=dwPult;
          g_MasPultMulticast[i].dwIP=dwIP;
SendStr2IpLogger("$DEBUG$PultMulticastDecodeStr: SN=%d, multicast IP=%08x",dwPult,dwIP);
          return;
        }
      }
    }
  }
}

void PultMulticastDecode(char *pBuf)
{
  char *pStr;
  while(pBuf)
  {
    pStr=pBuf;
    pBuf=GetNextStr(pBuf);
    PultMulticastDecodeStr(pStr);
  }
}

void ReadPultMulticastInfo(void)
{
  unsigned int dwFSize;
  char *pBuf;
  HANDLE hFile;

  memset(g_MasPultMulticast,0,sizeof(g_MasPultMulticast));
  hFile=CreateFile("pmi.txt",GENERIC_READ,0);
  if(hFile>=0)
  {
    dwFSize=GetFileSize(hFile);
    if(dwFSize)
    {
      pBuf=pvPortMalloc(dwFSize+1);
      if(pBuf)
      {
        if(ReadFile(hFile,pBuf,dwFSize))
        {
          pBuf[dwFSize]=0;
          PultMulticastDecode(pBuf);
        }
        vPortFree(pBuf);
      }
    }
    CloseHandle(hFile);
  }
}

unsigned int GetMIPAddr(unsigned char SN)
{
  int i;
  for(i=0;i<NumberKeyboard;i++)
  {
    if(g_MasPultMulticast[i].SN==SN)
    {
SendStr2IpLogger("$DEBUG$GetMIPAddr: SN=%d, multicast IP=%08x",SN,g_MasPultMulticast[i].dwIP);
      return g_MasPultMulticast[i].dwIP;
    }
  }
SendStr2IpLogger("$DEBUG$GetMIPAddr: SN=%d not found (addr=0)!",SN);
  return 0;
}
