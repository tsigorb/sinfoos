#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

bool IsEnableCallPult(unsigned char PultNb0,unsigned char Key,unsigned char btLayer)
{
  unsigned char PultNb1,SN;

  SN=MassKey[PultNb0][btLayer][Key].NumbAbon;
  PultNb1=GetPultIndFromSN(SN);
  if((PultNb1==0xff) || (PultNb0==PultNb1)) return(false);
  SN=MassM1[PultNb0];
  for(btLayer=0;btLayer<GetMaxLayer(PultNb1);btLayer++)
  {
    for(Key=0;Key<NumberKey;Key++)
    {
      if((MassKey[PultNb1][btLayer][Key].TypeAbon==PultCall) &&
         (MassKey[PultNb1][btLayer][Key].NumbAbon==SN)) return(true);
    }
  }
  return(false);
}

unsigned char GetKeyPultCall(unsigned char PultNb,unsigned char *pbtLayer,unsigned char SN)
{
  unsigned char Key,btLayer;

  for(btLayer=0;btLayer<GetMaxLayer(PultNb);btLayer++)
  {
    if(IsKbdLayerLock(PultNb,btLayer)) continue;
    for(Key=0;Key<NumberKey;Key++)
    {
      if((MassKey[PultNb][btLayer][Key].TypeAbon==PultCall) &&
         (MassKey[PultNb][btLayer][Key].NumbAbon==SN))
      { *pbtLayer=btLayer; return(Key); }
    }
  }
  return(0xff);
}

void OtboiAbonentPultCall(unsigned char PultNb0,unsigned char Key,unsigned char btLayer)
{
  unsigned char PultNb;
  unsigned short ChanDS,ChanRS;
  ABONENT_STATE_TP *psAbnt;

  psAbnt=GetAbonentStatePtrFromKey(PultNb0,Key,btLayer);
  if(!psAbnt) return;
  if(GetAbonentStateFromPtr(psAbnt)==PultListen)
  {
    ChanDS=GetOperatorChannel(psAbnt->NumbAbon,0);
    ChanRS=GetOperatorChannel(psAbnt->NumbAbon,1);
    ListenDispatcher(psAbnt->wOperatorChanel,ChanDS,ChanRS,0);
  }
  else
  {
    DelAbonentFromSelector(PultNb0,Key,btLayer);
    DelAbonentFromConference(PultNb0,Key,btLayer);
    Commute(false,psAbnt->wOperatorChanel,psAbnt->wConnectionPoint);
    PultNb=GetPultIndFromSN(MassKey[psAbnt->btPultNb][psAbnt->btLayer][psAbnt->btKey].NumbAbon);
    if(PultNb!=0xff)
    {
      for(btLayer=0;btLayer<GetMaxLayer(PultNb);btLayer++)
      {
        if(IsKbdLayerLock(PultNb,btLayer)) continue;
        for(Key=0;Key<NumberKey;Key++)
        {
          if((MassKey[PultNb][btLayer][Key].TypeAbon==PultCall) &&
             (MassKey[PultNb][btLayer][Key].NumbAbon==MassM1[PultNb0])) break;
        }
        if(Key<NumberKey)
        {
          DelAbonentFromSelector(PultNb,Key,btLayer);
          DelAbonentFromConference(PultNb,Key,btLayer);
          OtboiAbonentCommonPart(PultNb,Key,btLayer,NULL,NULL);
          break;
        }
      }
    }
  }
  OtboiAbonentCommonPart(PultNb0,psAbnt->btKey,psAbnt->btLayer,NULL,NULL);
}

ABONENT_STATE_TP *GetAbntPultListen(unsigned char PultNb,unsigned char btKey)
{
  unsigned short wConnectionPoint=((unsigned short)PultNb << 10) | (0x200 | btKey);
  return(GetAbonentStatePtrFromIndex(FindFirstAbonentIndex(PultNb,wConnectionPoint,0xffff,0xff,0xff,NULL)));
}

void AnswerIncomingCallPult(ABONENT_STATE_TP *psAbnt)
{
  unsigned char PultNb,btLayer,Key;

  PultNb=GetPultIndFromSN(MassKey[psAbnt->btPultNb][psAbnt->btLayer][psAbnt->btKey].NumbAbon);
  if(PultNb!=0xff)
  {
    btLayer=GetCurrentLayer(PultNb);
    for(Key=0;Key<NumberKey;Key++)
    {
      if((MassKey[PultNb][btLayer][Key].TypeAbon==PultCall) &&
         (MassKey[PultNb][btLayer][Key].NumbAbon==MassM1[psAbnt->btPultNb])) break;
    }
    if(Key<NumberKey)
    {
      DSRS_On(psAbnt->btPultNb,psAbnt->wOperatorChanel);
      DSRS_On(PultNb,psAbnt->wConnectionPoint);
      Commute(true,psAbnt->wOperatorChanel,psAbnt->wConnectionPoint);
      Commute(true,psAbnt->wConnectionPoint,psAbnt->wOperatorChanel);
      SetLedColorIfTalk(psAbnt);
      IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,1,1);
      SetAbonentStateByPtr(psAbnt,Talk,dIncomingTalkFlag);
      psAbnt->dwTimerEnd=0;
      
      psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);
      if(!psAbnt) return;
      SetLedColorIfTalk(psAbnt);
      IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,1,1);
      SetAbonentStateByPtr(psAbnt,Talk,0);
      psAbnt->dwTimerEnd=0;
    }
  }
}

void OutgoingCallPult(unsigned char PultNb,unsigned char Key,unsigned char btLayer,bool bTest)
{
  unsigned short NChan,wOper,ChanDS,ChanRS;
  unsigned char btIndSrs,btIndDst,PultNbDst,KeyDst,btLayerDst;
  ABONENT_STATE_TP *psAbnt;

  PultNbDst=GetPultIndFromSN(MassKey[PultNb][btLayer][Key].NumbAbon);
  wOper=GetOperatorChannelFromKey(PultNb,Key,btLayer);
  if(bTest)
  {
    if(MassM1[PultNbDst]<16) return; // ��������� ���������� ������� �� �����������!
    btIndSrs=GetFreeAbonentStateIndex();
    if(btIndSrs!=0xff)
    {
      psAbnt=GetAbonentStatePtrFromIndex(btIndSrs);
      psAbnt->NumbAbon=PultNbDst;
      ChanDS=GetOperatorChannel(psAbnt->NumbAbon,0);
      ChanRS=GetOperatorChannel(psAbnt->NumbAbon,1);
      if(ListenDispatcher(wOper,ChanDS,ChanRS,1))
      {
        // ��������� ��������� ���������� ����� �����������
        psAbnt->wConnectionPoint=PultNb;
        psAbnt->wConnectionPoint<<=10;
        psAbnt->wConnectionPoint|=(0x200 | Key);
        psAbnt->btLedState=dLedStatePultListen;
        psAbnt->btType=PultCall;
        psAbnt->wOperatorChanel=wOper;
        psAbnt->btPultNb=PultNb;
        psAbnt->btLayer=btLayer;
        psAbnt->btKey=Key;
        SetAbonentStateByPtr(psAbnt,PultListen,0);
        IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,1,1);
        psAbnt->dwTimerEnd=0;
      }
    }
  }
  else
  {
    if(!IsKbdReady(PultNbDst) || IsKbdLock(PultNbDst))
    { PutErrInfo2Display(PultNb,"����� ����������!"); return; }
    KeyDst=GetKeyPultCall(PultNbDst,&btLayerDst,MassM1[PultNb]);
    if(KeyDst==0xff)
    { PutErrInfo2Display(PultNb,"��� ������� ������!"); return; }
    NChan=GetOperatorChannelFromKey(PultNbDst,KeyDst,btLayerDst);
    psAbnt=GetAbntPultListen(PultNb,Key);
    if(psAbnt) OtboiAbonentPultCall(PultNb,Key,btLayer);

    btIndDst=0xff;
    btIndSrs=GetFreeAbonentStateIndex();
    if(btIndSrs!=0xff)
    {
      psAbnt=GetAbonentStatePtrFromIndex(btIndSrs);
      psAbnt->wConnectionPoint=NChan; // �������� ��������� �������!
      btIndDst=GetFreeAbonentStateIndex();
      if(btIndDst==0xff) psAbnt->wConnectionPoint=0xffff;
    }
    if((btIndSrs==0xff) || (btIndDst==0xff))
    { PutErrInfo2Display(PultNb,"���� ������!"); return; }

    psAbnt->wOperatorChanel=wOper;
    psAbnt->btPultNb=PultNb;
    psAbnt->btLayer=btLayer;
    psAbnt->btKey=Key;
    psAbnt->btType=PultCall;
    psAbnt->btLedState=dLedStateOutgoingCall;
    SetAbonentStateByPtr(psAbnt,Call,0);
    IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,1,1);
    psAbnt->dwTimerEnd=MassTimer[TIMER_INCOMINGCALL]+xTaskGetTickCount();

    psAbnt=GetAbntPultListen(PultNbDst,KeyDst);
    if(psAbnt) OtboiAbonentPultCall(PultNbDst,KeyDst,btLayerDst);

    psAbnt=GetAbonentStatePtrFromIndex(btIndDst);
    psAbnt->wConnectionPoint=wOper;
    psAbnt->wOperatorChanel=NChan;
    psAbnt->btPultNb=PultNbDst;
    psAbnt->btLayer=btLayerDst;
    psAbnt->btKey=KeyDst;
    psAbnt->btType=PultCall;
    if(IsGroupAutoAnswer4PultFromGroup(PultNbDst,d_GroupCallPult))
    {
      AnswerIncomingCallPult(psAbnt);
//      AddAbnt2FreeHandConf(PultNb,psAbnt);
    }
    else
    {
      psAbnt->btLedState=dLedStateIncomingCall;
      SetAbonentStateByPtr(psAbnt,IncomingCall,0);
      IndicCmd(psAbnt->btPultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,1,1);
      psAbnt->dwTimerEnd=MassTimer[TIMER_INCOMINGCALL]+xTaskGetTickCount();
    }
  }
}

void PultCallTOut(unsigned char PultNb)
{
  unsigned char btInd,btState;
  ABONENT_STATE_TP *psAbnt;

  btInd=0;
  do
  {
    psAbnt=GetAbonentStatePtrFromIndex(btInd);
    if(!psAbnt) return;
    if((psAbnt->wConnectionPoint!=0xffff) && (psAbnt->btPultNb==PultNb) && (psAbnt->btType==PultCall))
    {
      btState=GetAbonentStateFromPtr(psAbnt);
      if((btState==IncomingCall) || (btState==Call))
      {
        if(IsCurrentTickCountGT(psAbnt->dwTimerEnd)) OtboiAbonentPultCall(psAbnt->btPultNb,psAbnt->btKey,psAbnt->btLayer);
      }
    }
    btInd++;
  }
  while(1);
}

void PressCallKey_PultCall(unsigned char PultNb,unsigned char Key,unsigned char btLayer,bool bTest)
{
  ABONENT_STATE_TP *psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);

  if(!psAbnt)
  {
    if(IsEnableCallPult(PultNb,Key,btLayer)) OutgoingCallPult(PultNb,Key,btLayer,bTest);
    else PutErrInfo2Display(PultNb,NULL);
  }
  else
  {
    switch(GetAbonentStateFromPtr(psAbnt))
    {
      case IncomingCall:
      {
        if(IsKey2Selector(PultNb,Key,btLayer,d_Abonent2SelectorOtherPult))
        { PutErrInfo2Display(PultNb,NULL); return; }
        AnswerIncomingCallPult(psAbnt);
        break;
      }
      case Talk:
      {
        if(IsKey2Selector(PultNb,Key,btLayer,d_Abonent2SelectorOtherPult))
        { PutErrInfo2Display(PultNb,NULL); return; }
        if(IsSelectorActive(PultNb) && IsPressAddMember(PultNb)) AddAbonent2Selector(PultNb,Key,btLayer);
        else HoldOn(PultNb,Key,btLayer,true);
        break;
      }
      case Hold:
      {
        HoldOn(PultNb,Key,btLayer,false);
        break;
      }
      case PultListen:
      {
        OtboiAbonentPultCall(PultNb,Key,btLayer);
        OutgoingCallPult(PultNb,Key,btLayer,false);
        break;
      }
      default: break;
    }
  }
}

