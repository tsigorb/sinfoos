#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"
#include "SelectorN.h"

/////////////////////////////////////////////////////////////////////
void SetAbonentStatus2Selector(unsigned char PultNb,SELECTOR_ABNT_TP *psAbnt,unsigned char btState)
{
  unsigned char Key=psAbnt->Key,SN=MassM1[PultNb];

  if(btState==d_AbntSelectorStatusOff) psAbnt->Status=0x0;
  else
  {
    if(btState & 0x07) psAbnt->Status=btState;
    else
    {
      if(btState==d_AbntSelectorDisableOut)
      {
        psAbnt->Status|=d_AbntSelectorDisableOut;
        psAbnt->Status&=~d_AbntSelectorStatusShow;
      }
      else
      { psAbnt->Status=(psAbnt->Status & 0x07) | btState; }
    }
  }
  if(psAbnt->Status==0x0)
  {
    psAbnt->Name[0]=0;
    psAbnt->btSelectorCtrl=0;
    psAbnt->Key=0xff;
    IndicCmd(PultNb,GetCurrentLayer(PultNb),Key,OFF,0,0);
  }
  if(psAbnt->Status & d_AbntSelectorStatusShow) return;
  if(SN<16)
  {
    if(psAbnt->Status==0x0) psAbnt->Status|=d_AbntSelectorStatusShow;
  }
  else
  {
    if(psAbnt->btSelectorCtrl & (d_SelectorListener_Bad | d_SelectorIsdnAbnt_NoTalk)) xSendKeyState108ToIpPult(SN,Key,StatusListenerErr);
    else xSendKeyState108ToIpPult(SN,Key,psAbnt->Status);
    psAbnt->Status|=d_AbntSelectorStatusShow;
  }
}

bool IsSelectorChanelNeedControl(unsigned short wNChan)
{
  static sCommand sCmd;
  static sInfoKadr sReply;

  sCmd.L=3;
  sCmd.Source=UserRS485;
  sCmd.BufCommand[0]=15;
  sCmd.BufCommand[1]=wNChan >> 8;
  sCmd.BufCommand[2]=wNChan-1;
  xQueueSendCmd(&sCmd,10);
  if(xQueueReceive(xQueueReply[UserRS485],&sReply,500))
  {
    if(sReply.ByteInfoKadr[0]==16)
    {
      if(sReply.ByteInfoKadr[8] & 0x04) return(true);
    }
  }
  return(false);
}

void DetectListenerState(unsigned char PultNb)
{
  unsigned char i;
  ABONENT_STATE_TP *psAbnt;
  SELECTOR_ABNT_TP *psAbntS;

  psAbntS=s_sSelectors[PultNb].pMassAbnt;
  for(i=0;i<s_sSelectors[PultNb].btCnt;i++,psAbntS++)
  {
    if(psAbntS->Key==0xff) continue;
    psAbnt=GetAbonentStatePtrFromKey(PultNb,psAbntS->Key,s_sSelectors[PultNb].btLayer);
    if(psAbnt)
    {
      if(psAbntS->btSelectorCtrl & 0x01)
      { // ���� ������������� ���������
        if(psAbntS->btSelectorCtrl & d_SelectorListener_Bad)
        { // StatusListenerErr
          if(!IsSelectorChanelNeedControl(psAbnt->wConnectionPoint))
          {
            psAbntS->btSelectorCtrl&=~d_SelectorListener_Bad;
            if(s_sSelectors[PultNb].btState==d_SelectorWait) SetAbonentStatus2Selector(PultNb,psAbntS,d_AbntSelectorTalkOff);
            else SetAbonentStatus2Selector(PultNb,psAbntS,psAbntS->Status & 0x7);
            psAbntS->dwTOut=0;
          }
        }
        else
        { // StatusListener
          if(IsSelectorChanelNeedControl(psAbnt->wConnectionPoint))
          {
            if(psAbntS->dwTOut)
            {
              if(IsCurrentTickCountGT(psAbntS->dwTOut))
              {
                psAbntS->btSelectorCtrl|=d_SelectorListener_Bad;
                if(s_sSelectors[PultNb].btState==d_SelectorWait) SetAbonentStatus2Selector(PultNb,psAbntS,d_AbntSelectorTalkOff);
                else SetAbonentStatus2Selector(PultNb,psAbntS,psAbntS->Status & 0x7);
                psAbntS->dwTOut=d_TmListenerErr+xTaskGetTickCount();
              }
            }
            else psAbntS->dwTOut=d_TmListenerErr+xTaskGetTickCount();
          }
          else psAbntS->dwTOut=0;
        }
      }
    }
  }
}

bool _xDelLeaderSelector(unsigned char SN,sCommand *psCmd)
{ // ������� �������� � ����� ��������
  psCmd->L=7;
  psCmd->BufCommand[1]=176;
  psCmd->BufCommand[3]=0; // �� ������������
  psCmd->BufCommand[4]=0; // �� ������������
  psCmd->BufCommand[5]=0xff;
  psCmd->BufCommand[6]=0;
  xQueueSendCmd(psCmd,30);
  return(WaitReply(11,psCmd->BufCommand[1]));
}

void _xCmdAbntSelectorStatus(unsigned char PultNb,SELECTOR_ABNT_TP *psAbntS,unsigned char btStatus)
{
  unsigned short NChan1;
  unsigned char SN,btCmd;
  ABONENT_STATE_TP *psAbnt;
  static sCommand sCmd;

  psAbnt=GetAbonentStatePtrFromKey(PultNb,psAbntS->Key,s_sSelectors[PultNb].btLayer);
  if(!psAbnt) return;
  SN=MassM1[PultNb];
  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.BufCommand[0]=10;
  sCmd.BufCommand[2]=SN;
  switch(btStatus)
  {
    case StatusLeader:
      NChan1=psAbnt->wConnectionPoint;
      if(IsLeaderSelector(SN,NChan1)) return;
      if(!_xDelLeaderSelector(SN,&sCmd)) return;
      btCmd=176;
      psAbntS->btSelectorCtrl=0x0;
      break;
    case StatusMember:
      if((psAbntS->Status & 0xf)==StatusLeader)
      {
        NChan1=0xff00;
        btCmd=176;
      }
      else btCmd=158; // ������������� ����� ��������
      psAbntS->btSelectorCtrl=0x0;
      break;
    case StatusListener:
      if(IsLeaderSelector(SN,psAbnt->wConnectionPoint))
      {
        if(!_xDelLeaderSelector(SN,&sCmd)) return;
      }
      if(psAbnt->btType==IPPM)
      {
        NChan1=GetCP_IPPM2MU(psAbnt->NumbAbon);
        if(NChan1<d_DummyCP) _xDelAbntFromSelector(PultNb,NChan1);
        if(DelCP_IPPM2MU(psAbnt->NumbAbon)) SendIPPMCmdConnect(psAbnt->NumbAbon,psAbnt);
        return;
      }
      btCmd=157; // ���������� ����� ��������
      if((psAbnt->btType==Selector) && (!psAbntS->btSelectorCtrl))
      {
        psAbntS->btSelectorCtrl=0x01;
        psAbntS->dwTOut=d_TmListenerErr+xTaskGetTickCount();
        if(!s_sSelectors[PultNb].dwListenerTOut) s_sSelectors[PultNb].dwListenerTOut=1000+xTaskGetTickCount();
      }
      break;
    default: return;        
  }
  sCmd.BufCommand[1]=btCmd;
  if((btCmd==157) || (btCmd==158))
  {
    sCmd.L=6;
    sCmd.BufCommand[3]=1;
    sCmd.BufCommand[4]=psAbnt->wConnectionPoint >> 8;
    sCmd.BufCommand[5]=psAbnt->wConnectionPoint;
  }
  else
  {
    sCmd.L=7;
    sCmd.BufCommand[3]=0;
    sCmd.BufCommand[4]=0;
    sCmd.BufCommand[5]=NChan1 >> 8;
    sCmd.BufCommand[6]=NChan1;
  }
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
}

void IncomingCallSelectorCtrl(unsigned short wNChan)
{
  unsigned char PultNb,btKey,btLayer,btInd,btStatus;
  ABONENT_STATE_TP *psAbnt;
  SELECTOR_ABNT_TP *psAbntS;

  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if(!s_sSelectors[PultNb].btState) continue;
    btLayer=s_sSelectors[PultNb].btLayer;
    psAbntS=s_sSelectors[PultNb].pMassAbnt;
    for(btInd=0;btInd<s_sSelectors[PultNb].btCnt;btInd++,psAbntS++)
    {
      psAbnt=GetAbonentStatePtrFromKey(PultNb,psAbntS->Key,s_sSelectors[PultNb].btLayer);
      if(psAbnt && (wNChan==psAbnt->wConnectionPoint))
      {
        if(psAbntS->btSelectorCtrl & 0x01)
        {
          if(psAbntS->btSelectorCtrl & d_SelectorListener_Bad)
          {
            psAbntS->btSelectorCtrl&=~d_SelectorListener_Bad;
            if(s_sSelectors[PultNb].btState==d_SelectorWait) SetAbonentStatus2Selector(PultNb,psAbntS,d_AbntSelectorTalkOff);
            else SetAbonentStatus2Selector(PultNb,psAbntS,psAbntS->Status & 0x7);
          }
          psAbntS->dwTOut=d_TmListenerErr+xTaskGetTickCount();
          return;
        }
        else _xCmdAbntSelectorStatus(PultNb,psAbntS,psAbntS->Status & 0x7);
      }
    }
  }
}

/////////////////////////////////////////////////////////////////////
bool IsPlaySound2Selector(unsigned char PultNb,unsigned short wNChan)
{
  if(s_sSelectors[PultNb].btState && (s_sSelectors[PultNb].wCP==wNChan)) return(true);
  return(false);
}

bool IsSelectorActive(unsigned char PultNb)
{
  return(s_sSelectors[PultNb].btState!=0);
}

bool IsSelectorWorkState(unsigned char PultNb)
{ return((s_sSelectors[PultNb].btState==d_SelectorWork) ? true : false); }

unsigned char GetActiveSelectorState(unsigned char PultNb)
{ return(s_sSelectors[PultNb].btState & 0x0f); }

unsigned char GetSelectorState(unsigned char PultNb,unsigned char btKey)
{
  if(s_sSelectors[PultNb].btState!=Off)
  {
    if(s_sSelectors[PultNb].btKey==btKey) return(s_sSelectors[PultNb].btState);
  }
  return(Off);
}

unsigned char GetSelectorMemberCount(unsigned char PultNb)
{
  unsigned char btInd,btCnt=0;
  SELECTOR_ABNT_TP *pMasAbnt=s_sSelectors[PultNb].pMassAbnt;

  for(btInd=0;btInd<s_sSelectors[PultNb].btCnt;btInd++)
  {
    if(pMasAbnt[btInd].Status & 0x07) btCnt++;
  }
  return(btCnt);
}

unsigned short GetSelectorOperator(unsigned char PultNb)
{ return(s_sSelectors[PultNb].wOperator); }

unsigned char GetSelectorNumb(unsigned char PultNb)
{ return(s_sSelectors[PultNb].SelectorNb); }

int GetAbntSelectorInfoFromIndex(unsigned char PultNb,unsigned char btInd,sAbntSelectorInfo *psAbntSInfo)
{
  SELECTOR_ABNT_TP *psAbnt;

  if(btInd>=s_sSelectors[PultNb].btCnt) return(-1);
  psAbnt=&(s_sSelectors[PultNb].pMassAbnt[btInd]);
  if(!(psAbnt->Status & 0x07)) return(0);
  memcpy(psAbntSInfo->Name,psAbnt->Name,32);
  psAbntSInfo->btNumb=psAbnt->Key;
  psAbntSInfo->wConnectionPoint=GetAbonentConnectionPointFromKey(PultNb,psAbnt->Key,s_sSelectors[PultNb].btLayer);
  if(s_psAbntS==psAbnt) psAbntSInfo->btStatus=StatusPrivate;
  else
  {
    if(psAbnt->Status & d_AbntSelectorDisableOut) psAbntSInfo->btStatus=StatusDisableOut;
    else
    {
      switch(psAbnt->Status & 0x70)
      {
        case d_AbntSelectorTalkOn:
          psAbntSInfo->btStatus=StatusMember;
          break;
        case d_AbntSelectorTalkOff:
          psAbntSInfo->btStatus=StatusListener;
          break;
        case d_AbntSelectorStatusErr:
          psAbntSInfo->btStatus=StatusError;
          break;
        case d_AbntSelectorStatusCall:
          psAbntSInfo->btStatus=StatusCall;
          break;
        case d_AbntSelectorStatusDel:
        case d_AbntSelectorStatusBusy:
          psAbntSInfo->btStatus=StatusBusy;
          break;
        case d_AbntSelectorStatusWait:
          psAbntSInfo->btStatus=StatusWait;
          break;
      }
      if(psAbnt->Status & 0x70) return(1);
      psAbntSInfo->btStatus=psAbnt->Status & 0x7;
    }
  }
  return(1);
}

bool IsKey2Selector(unsigned char PultNb0,unsigned char btKey0,unsigned char btLayer0,unsigned char btWhere)
{
  unsigned char PultNb,btLayer,i;
  sKBKey *psKey0,*psKey1;
  SELECTOR_ABNT_TP *psAbntS;
  
  psKey0=&MassKey[PultNb0][btLayer0][btKey0];
  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if((btWhere==d_Abonent2SelectorOtherPult) && (PultNb==PultNb0)) continue;
    if((btWhere==d_Abonent2SelectorCurrentPult) && (PultNb!=PultNb0)) continue;
    if(MassM1[PultNb] && s_sSelectors[PultNb].btState)
    {
      btLayer=s_sSelectors[PultNb].btLayer;
      psAbntS=s_sSelectors[PultNb].pMassAbnt;
      for(i=0;i<s_sSelectors[PultNb].btCnt;i++,psAbntS++)
      {
        if(psAbntS->Key==0xff) continue;
        if(!(psAbntS->Status & 0x7f)) continue;
        if(btWhere==d_Abonent2SelectorCurrentPult)
        {
          if(psAbntS->Key==btKey0) return(true);
        }
        else
        {
          psKey1=&MassKey[PultNb][btLayer][psAbntS->Key];
          if(IsCallKeyEqual(psKey0,psKey1))
          {
            return(true);
          }
        }
      }
    }
  }
  return(false);
}

void SetAutomaticCall(unsigned char PultNb,bool bAuto)
{ s_bAutomaticCall[PultNb]=bAuto; }

bool IsAutomaticCall(unsigned char PultNb)
{ return(s_bAutomaticCall[PultNb]); }

SELECTOR_ABNT_TP *FindAbonentSelector(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{
  unsigned char btInd;
  SELECTOR_ABNT_TP *pMassAbnt;

  if(!s_sSelectors[PultNb].btState || (s_sSelectors[PultNb].btLayer!=btLayer)) return(NULL);
  pMassAbnt=s_sSelectors[PultNb].pMassAbnt;
  for(btInd=0;btInd<s_sSelectors[PultNb].btCnt;btInd++)
  {
    if(pMassAbnt[btInd].Key==Key) return(pMassAbnt+btInd);
  }
  return(NULL);
}

unsigned char GetAbonentSelectorState(unsigned char PultNb,unsigned char btKey,unsigned char btLayer)
{
  SELECTOR_ABNT_TP *psAbnt=FindAbonentSelector(PultNb,btKey,btLayer);

  if(psAbnt) return(psAbnt->Status & 0x7f);
  return(Off);
}

void _xDelAbntFromSelector(unsigned char PultNb,unsigned short NChan)
{
  bool bVMCP=false;
  static sCommand sCmd;

  if(!NChan) return;
  if(NChan>d_DummyCP)
  { // MULTICAST
    if(GetCountMulticastIPPM(NChan)>1) return;
    NChan=GetCPFromVMCP(NChan);
    if(!NChan) return;
    StopAudioFromMU2IPPM(NChan);
    bVMCP=true;
  }
  if(s_sSelectors[PultNb].btState!=d_SelectorDestroy)
  {
    sCmd.Source=UserRS485;
    sCmd.A1=0;
    sCmd.BufCommand[0]=10; // �������
    sCmd.BufCommand[1]=159; // �������� ���������� ������������ ��������� � ������������ � �������� 
    sCmd.BufCommand[2]=MassM1[PultNb];
    sCmd.BufCommand[3]=1;
    sCmd.BufCommand[4]=NChan >> 8;
    sCmd.BufCommand[5]=NChan;
    sCmd.L=6; // ���������� ���� � �������
    xQueueSendCmd(&sCmd,100);
  }
  if(bVMCP) DelMCP(NChan);
}

void IsdnIPPMAbntSelectorAnswer(unsigned char PultNb,unsigned char btKey)
{
  unsigned char btInd;
  SELECTOR_ABNT_TP *psAbnt;

  psAbnt=s_sSelectors[PultNb].pMassAbnt;
  for(btInd=0;btInd<s_sSelectors[PultNb].btCnt;btInd++,psAbnt++)
  {
    if(psAbnt->Key==btKey)
    {
      psAbnt->btSelectorCtrl&=~d_SelectorIsdnAbnt_NoTalk;
      psAbnt->Status&=~d_AbntSelectorStatusShow;
      return;
    }
  }
}

void LoadAbonentSelectorName(unsigned char PultNb,SELECTOR_ABNT_TP *psAbnt)
{ // �������� ���� ���������� ���.���������
  unsigned char btLayer,btCnt,btLen;
  HANDLE hFile;
  SELECTOR_ABNT_TP *pMassAbnt;
  static char FName[20];
  static FileNameAbntSelector_TP sAbntSelName;

  pMassAbnt=s_sSelectors[PultNb].pMassAbnt;
  btLayer=s_sSelectors[PultNb].btLayer;
  LockPrintf();
  sprintf(FName,"Pult1%02dR%d.dts",PultNb,btLayer+1);
  UnlockPrintf();
  hFile=CreateFile(FName,GENERIC_READ,0);
  if(hFile>=0)
  {
    while(ReadFile(hFile,&sAbntSelName,sizeof(FileNameAbntSelector_TP))==sizeof(FileNameAbntSelector_TP))
    {
      if(psAbnt)
      {
        if(sAbntSelName.Key==psAbnt->Key)
        {
          psAbnt->Name[32]=0;
          if(!psAbnt->Name[0]) strncpy(psAbnt->Name,sAbntSelName.Name,32);
          btLen=strlen(psAbnt->Name);
          if(btLen<32) memset(psAbnt->Name+btLen,' ',32-btLen);
          break;
        }
      }
      else
      {
        for(btCnt=0;btCnt<s_sSelectors[PultNb].btCnt;btCnt++)
        {
          if(sAbntSelName.Key==pMassAbnt[btCnt].Key)
          {
            pMassAbnt[btCnt].Name[32]=0;
            if(!pMassAbnt[btCnt].Name[0]) strncpy(pMassAbnt[btCnt].Name,sAbntSelName.Name,32);
            btLen=strlen(pMassAbnt[btCnt].Name);
            if(btLen<32) memset(pMassAbnt[btCnt].Name+btLen,' ',32-btLen);
            break;
          }
        }
      }
    }
    CloseHandle(hFile);
  }
}

bool _InsertAbonent2MassAbnt(unsigned char PultNb,ABONENT_STATE_TP *psAbnt)
{
  unsigned char btInd;
  SELECTOR_ABNT_TP *pMassAbnt;
        
  pMassAbnt=s_sSelectors[PultNb].pMassAbnt;
  for(btInd=0;btInd<s_sSelectors[PultNb].btCnt;btInd++)
  {
    if((pMassAbnt[btInd].Status & 0x07) && (pMassAbnt[btInd].Key==psAbnt->btKey))
    {
      if(IsSelectorWorkState(PultNb)) SetAbnt2SelectorStatus(PultNb,psAbnt->btKey,psAbnt->btLayer);
      else
      {
        if(psAbnt->btType==ISDN)
        { // ���� ����� ��������� - ��������� ����� ISDN-��������
          _xCmdAbntSelectorStatus(PultNb,pMassAbnt+btInd,StatusListener);
        }
      }
      return(true);
    }
  }
  for(btInd=0;btInd<s_sSelectors[PultNb].btCnt;btInd++)
  {
    if(!(pMassAbnt[btInd].Status & 0x07))
    { // ����� ��������� ����� � ������� ��������� ���������
      pMassAbnt[btInd].Key=psAbnt->btKey;
      pMassAbnt[btInd].dwTOut=0;
      pMassAbnt[btInd].btSelectorCtrl=0x0;
      LoadAbonentSelectorName(PultNb,pMassAbnt+btInd);
      SetAbonentStatus2Selector(PultNb,pMassAbnt+btInd,StatusMember);
      return(true);
    }
  }
SendStr2IpLogger("$SELECTOR$_InsertAbonent2MassAbnt error: PultNb=%d, Key=%d",PultNb,psAbnt->btKey);
  PutErrInfo2Display(PultNb,NULL);
  return(false);
}

bool _xAddAbnt2Selector(unsigned char PultNb,unsigned short NChan0)
{
  unsigned short NChan,wMSB=NChan0 & 0x8000;
  static sCommand sCmd;

  NChan0&=0x1fff;
  if(NChan0>d_DummyCP)
  { // virtual multicast CP
    NChan=GetCPFromVMCP(NChan0);
    if(NChan)
    {
SendStr2IpLogger("$DEBUG$_xAddAbnt2Selector multicast CP already used: PultNb=%d, wCP=%x (VMCP=%x)",PultNb,NChan,NChan0);
      return true;
    }
    NChan=CreatMCP(MassM1[PultNb]);
    if(!NChan)
    {
SendStr2IpLogger("$DEBUG$_xAddAbnt2Selector multicast error: PultNb=%d, wCP=0 (VMCP=%x)",PultNb,NChan0);
      return false;
    }
SendStr2IpLogger("$DEBUG$_xAddAbnt2Selector multicast: PultNb=%d, wCP=%x (VMCP=%x)",PultNb,NChan,NChan0);
    StartAudioFromMU2IPPM_Multicast(GetMIPAddr(MassM1[PultNb]),NChan);
  }
  else NChan=NChan0;
  NChan|=wMSB;
  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.BufCommand[0]=10; // �������
  sCmd.BufCommand[1]=160; // ���������� ��������� �� ����� ��������
  sCmd.BufCommand[2]=MassM1[PultNb];
  sCmd.BufCommand[3]=1;
  sCmd.BufCommand[4]=NChan >> 8;
  sCmd.BufCommand[5]=NChan;
  sCmd.L=6; // ���������� ���� � �������
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(WaitReply(11,160)) return(true);
    else CountErrCommute++;
  }
  return(false);
}

void ReplaceCP2Selector(unsigned char PultNb,unsigned short wCP_Old,unsigned short wCP_New)
{ // ������ ��� ISDN-����������
  if(s_sSelectors[PultNb].btState)
  {
    _xDelAbntFromSelector(PultNb,wCP_Old);
    _xAddAbnt2Selector(PultNb,wCP_New | 0x8000);
  }
}

bool AddTDCIPPM2CurrentSelector(unsigned char PultNb,ABONENT_STATE_TP *psAbnt)
{
  if(s_sSelectors[PultNb].btState)
  {
    if(_xAddAbnt2Selector(PultNb,psAbnt->wConnectionPoint | 0x8000))
    {
      SELECTOR_ABNT_TP *psAbntS=FindAbonentSelector(PultNb,psAbnt->btKey,psAbnt->btLayer);
      if(psAbntS)
      {
        _xCmdAbntSelectorStatus(PultNb,psAbntS,StatusListener);
        if(s_sSelectors[PultNb].btState==d_SelectorWait) SetAbonentStatus2Selector(PultNb,psAbntS,StatusListener | d_AbntSelectorTalkOff);
        else SetAbonentStatus2Selector(PultNb,psAbntS,StatusListener);
      }
      return(true);
    }
  }
  return(false);
}

unsigned short GetConnectionPoint4AbonentSelector(unsigned char PultNb,SELECTOR_ABNT_TP *psAbntSel)
{
  ABONENT_STATE_TP *psAbnt;

  if(psAbntSel->Key==0xff) return(0);
  psAbnt=GetAbonentStatePtrFromKey(PultNb,psAbntSel->Key,s_sSelectors[PultNb].btLayer);
  if(!psAbnt) return(0);
  if(psAbnt->wConnectionPoint<d_DummyCP) return(psAbnt->wConnectionPoint);
  else
  {
    unsigned short NChan=GetCPFromVMCP(psAbnt->wConnectionPoint);
    if(!NChan)
    {
      NChan=CreatMCP(MassM1[PultNb]);
      if(NChan) StartAudioFromMU2IPPM_Multicast(GetMIPAddr(MassM1[psAbnt->btPultNb]),NChan);
    }
    return NChan;
  }
}

bool _xExistConnectionPoint2CommuteArray(unsigned char *BufCommand,unsigned char btCnt,unsigned short NChan0)
{
  unsigned char i;
  unsigned short NChan1;

  for(i=0;i<btCnt;i++)
  {
    NChan1=*BufCommand++;
    NChan1=((NChan1 & 0x7f) << 8) | (*BufCommand++);
    if(NChan1==NChan0) return(true);
  }
  return(false);
}

bool CreateCommute4Selector(unsigned char PultNb)
{
  unsigned char btLayer,btKey,btCnt;
  unsigned short i,j,wOperator,NChan;
  bool bRes;
  SELECTOR_ABNT_TP *psAbntS=s_sSelectors[PultNb].pMassAbnt;
  ABONENT_STATE_TP *psAbnt;
  static sCommand sCmd;

  btLayer=s_sSelectors[PultNb].btLayer;
  wOperator=s_sSelectors[PultNb].wOperator;
  btCnt=s_sSelectors[PultNb].btCnt;
  sCmd.Source=UserRS485;
  sCmd.A1=0;
  sCmd.L=7+btCnt*2; // ���������� ���� � �������
  sCmd.BufCommand[0]=10; // �������
  sCmd.BufCommand[1]=155; // ���������� ���������� � ������������ ������� � ������������ � �������� 
  sCmd.BufCommand[2]=MassM1[PultNb];
  sCmd.BufCommand[3]=s_sSelectors[PultNb].SelectorNb;
  sCmd.BufCommand[4]=btCnt;
  sCmd.BufCommand[5]=wOperator >> 8; 
  sCmd.BufCommand[6]=wOperator;
  for(i=0,j=7;i<btCnt;i++,j+=2,psAbntS++)
  {
    NChan=GetConnectionPoint4AbonentSelector(PultNb,psAbntS);
    if(NChan && _xExistConnectionPoint2CommuteArray(sCmd.BufCommand+7,i,NChan)) NChan=0;
    sCmd.BufCommand[j]=(NChan >> 8) | 0x80; // � ����������� �����
    sCmd.BufCommand[j+1]=NChan & 0xff;
//SendStr2IpLogger("$SELECTOR$CreateCommute4Selector: PultNb=%d, NChan=%x",PultNb,sCmd.BufCommand[j]*256+sCmd.BufCommand[j+1]);
    if(psAbntS->Key!=0xff)
    {
      psAbnt=GetAbonentStatePtrFromKey(PultNb,psAbntS->Key,s_sSelectors[PultNb].btLayer);
      if(psAbnt->btType==Selector)
      {
        psAbntS->btSelectorCtrl=0x01;
        psAbntS->dwTOut=d_TmListenerErr+xTaskGetTickCount();
        if(!s_sSelectors[PultNb].dwListenerTOut) s_sSelectors[PultNb].dwListenerTOut=1000+xTaskGetTickCount();
      }
    }
  }
  bRes=false;
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(WaitReply(11,155)) bRes=true;
  }
  return(bRes);
}

void SendAllAbonentSelector2IpPult(unsigned char PultNb)
{
  unsigned char i,btOff,btCnt,btKey,btState,
                SN=MassM1[PultNb];
  unsigned int dwIP;
  unsigned short wPort;
  SELECTOR_ABNT_TP *psAbntS;
  static sInfoKadr sInfo;

  if(SN<16) return;
  if(s_sSelectors[PultNb].btState && GetAddressIpPult(SN,&dwIP,&wPort))
  {
    btCnt=s_sSelectors[PultNb].btCnt;
    sInfo.Source=UserUDP;
    sInfo.A1=dwIP;
    sInfo.PORT1=wPort;
    sInfo.L=3+2*btCnt;
    sInfo.ByteInfoKadr[0]=108;
    sInfo.ByteInfoKadr[1]=SN;
    sInfo.ByteInfoKadr[2]=btCnt;
    psAbntS=s_sSelectors[PultNb].pMassAbnt;
    for(i=0,btOff=3;i<btCnt;i++,psAbntS++)
    {
      btKey=psAbntS->Key;
      if(btKey==0xff) btState=0;
      else
      {
        if(psAbntS->btSelectorCtrl & d_SelectorListener_Bad) btState=StatusListenerErr;
        else btState=psAbntS->Status & 0x7f;
      }
      sInfo.ByteInfoKadr[btOff++]=btKey;
      sInfo.ByteInfoKadr[btOff++]=btState;
    }
    xQueueSend(xQueueReply[UserUDP],&sInfo,10,0);
  }
}

unsigned char TranslateSelectorState2Led(unsigned char PultNb,unsigned char SelectorNb)
{
  if(s_sSelectors[PultNb].SelectorNb!=SelectorNb) return(Off);
  if(s_sSelectors[PultNb].btState & d_SelectorError) return(FAST_BLINK_RED);
  if(s_sSelectors[PultNb].btState & d_AbntSelectorDisableOut) return(BLINK_RED_ORANGE);
  switch(s_sSelectors[PultNb].btState & 0x07)
  {
    case d_SelectorWork:
      if(s_sSelectors[PultNb].btState & d_SelectorOperatorOff) return(ORANGE);
      else return(GREEN);
    case d_SelectorWait: return(BLINK_RED_GREEN);
  }
  return(Off);
}

void SetAllAbonentSelector2Member(unsigned char PultNb,unsigned char btMode)
{
  unsigned char btInd,btStatus;
  SELECTOR_ABNT_TP *psAbntS;

  psAbntS=s_sSelectors[PultNb].pMassAbnt;
  for(btInd=0;btInd<s_sSelectors[PultNb].btCnt;btInd++,psAbntS++)
  {
    btStatus=0;
    if(btMode)
    {
      if((psAbntS->Status & 0x07)==StatusListener)
      {
        psAbntS->StatusOld=StatusListener;
        btStatus=StatusMember;
      }
    }
    else
    {
      if(psAbntS->StatusOld)
      {
        btStatus=StatusListener;
        psAbntS->StatusOld=0;
      }
    }
    if(btStatus)
    {
      _xCmdAbntSelectorStatus(PultNb,psAbntS,btStatus);
      SetAbonentStatus2Selector(PultNb,psAbntS,btStatus);
    }
  }
}

void EnOutAbonentSelector(unsigned char PultNb,SELECTOR_ABNT_TP *psAbntS,ABONENT_STATE_TP *psAbnt,unsigned char btEnOut)
{
  if(CmdEnDisOutCP(psAbnt->wConnectionPoint,btEnOut))
  {
    if(btEnOut) SetAbonentStatus2Selector(PultNb,psAbntS,psAbntS->Status & 0x70);
    else SetAbonentStatus2Selector(PultNb,psAbntS,d_AbntSelectorDisableOut);
SendStr2IpLogger("$SELECTOR$CmdEnDisOut: PultNb=%d, Key=%d, btEnOut=%d",PultNb,psAbnt->btKey,btEnOut);
  }
  else
  {
SendStr2IpLogger("$SELECTOR$CmdEnDisOut ERROR!: PultNb=%d, Key=%d, btEnOut=%d",PultNb,psAbnt->btKey,btEnOut);
  }
}

void RestoreAbonentToSelector(unsigned char PultNb,unsigned char Key,unsigned char btLayer)
{ // ��������������� �������� � ��������� ����� TmpExcludeAbonentFromSelector
  ABONENT_STATE_TP *psAbnt;

  if(s_psAbntS)
  {
    if(s_psAbntS->Key!=Key) return;
    psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,btLayer);
    if(!psAbnt) return;
    Commute(false,psAbnt->wConnectionPoint,psAbnt->wOperatorChanel);
    DSRS_Off(PultNb,psAbnt->wOperatorChanel | d_OperatorOffWithoutCondition);
    _xAddAbnt2Selector(PultNb,psAbnt->wConnectionPoint);
SendStr2IpLogger("$SELECTOR$RestoreAbonentToSelector: PultNb=%d, Key=%d",PultNb,Key);
    s_psAbntS=NULL;
  }
}

bool SetAbnt2SelectorStatus(unsigned char PultNb,unsigned char btKey,unsigned char btLayer)
{
  if((btKey!=0xff) && (GetAbonentStateFromKey(PultNb,btKey,btLayer)==Talk))
  {
    unsigned char btStatus;
    SELECTOR_ABNT_TP *psAbntS=FindAbonentSelector(PultNb,btKey,btLayer);
    if(psAbntS)
    {
      btStatus=psAbntS->Status & 0x07;
      _xCmdAbntSelectorStatus(PultNb,psAbntS,btStatus);
      SetAbonentStatus2Selector(PultNb,psAbntS,btStatus);
SendStr2IpLogger("$SELECTOR$SetAbnt2SelectorStatus: PultNb=%d, Key=%d, Status=%d",PultNb,btKey,btStatus);
      return(true);
    }
  }
  return(false);
}

