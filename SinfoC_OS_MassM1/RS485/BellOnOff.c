#include "..\Sinfo.h"
#include "GlobalData.h" 
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

typedef struct
{
  unsigned char btState;
  unsigned int dwTOut;
} BELL_ONOFF_TP;

#define d_IncomingCallPause         3000

static BELL_ONOFF_TP s_sMasBell[NumberKeyboard];

void BellOnOff(unsigned char PultNb)
{
  if(IsIncomingCall2Pult(PultNb))
  {
    if(s_sMasBell[PultNb].btState==0)
    {
      IndicCmd(PultNb,0,NO_INDIC,0,6,1);
      s_sMasBell[PultNb].btState=1;
      s_sMasBell[PultNb].dwTOut=d_IncomingCallPause+xTaskGetTickCount();
    }
    else
    {
      if(IsCurrentTickCountGT(s_sMasBell[PultNb].dwTOut))
      {
        if(!IsTalk2Pult(PultNb,0xffff))
        { 
          if(s_sMasBell[PultNb].btState!=2)
          { BellOn(PultNb,2); s_sMasBell[PultNb].btState=2; }
        }
        else
        {
          if(s_sMasBell[PultNb].btState==2)
          { BellOff(PultNb); s_sMasBell[PultNb].btState=1; }
          IndicCmd(PultNb,0,NO_INDIC,0,6,1);
        }
        s_sMasBell[PultNb].dwTOut=d_IncomingCallPause+xTaskGetTickCount();
      }
      else
      {
        if((s_sMasBell[PultNb].btState==2) && IsTalk2Pult(PultNb,0xffff))
        { BellOff(PultNb); s_sMasBell[PultNb].btState=1; }
      }
    }
  }
  else
  {
    if(s_sMasBell[PultNb].btState==2) BellOff(PultNb);
    s_sMasBell[PultNb].btState=0;
  }
}

void BellOnOffControl(void)
{
  static unsigned char PultNb=0;
  static unsigned int dwTOut=0;

  if(IsCurrentTickCountLT(dwTOut)) return;
  if((MassM1[PultNb]<16) && IsKbdReady(PultNb))
  {
    BellOnOff(PultNb);
    dwTOut=xTaskGetTickCount()+10;
  }
  PultNb++;
  if(PultNb>=NumberKeyboard) PultNb=0;
}

