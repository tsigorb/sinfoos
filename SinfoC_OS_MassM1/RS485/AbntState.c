#include "..\Sinfo.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

//////////////////////////////////////////////////////////////
// ���������� true � IP-����� � ����� ����� �������� ��� ��������� �����������,
// ���� ������ ���������������� � ����-�� ���� �� ������� ������ ������ ������������
// ����� - false
/*bool GetAddrInfo4Record(unsigned char PultNb,unsigned char RecordNumb,
                        unsigned int *pdwIpAddr,unsigned short *pwPort);*/
                        
// ��������� ����� pBuf ����� ��� ������������� �������� � ��������� �������
// � ���������� ������ ������ �������������
unsigned char GetRecordTimeStamp(unsigned char *pBuf);

// ����� ������ ���� ������� ����, ��� ������� �������!!!
unsigned char IsAbonentLevelGT(unsigned short wConnectionPoint)
{ return(0); }

//////////////////////////////////////////////////////////////
static ABONENT_STATE_TP MassAbntState[MaxAbonentActive];

//////////////////////////////////////////////////////////////
void InitAbonentStateArray(void)
{
  memset(MassAbntState,0xff,sizeof(MassAbntState));
}

unsigned char FindFirstAbonentIndex(unsigned char btPultNb,
                                    unsigned short wConnectionPoint,unsigned short wOperatorChanel,
                                    unsigned char TypeAbon,unsigned char NumbAbon,unsigned char *pbtTlfn)
{
  unsigned char btIndex;
  ABONENT_STATE_TP *psAbnt;
  
  if((btPultNb==0xff) && (wConnectionPoint==0xffff) && (wOperatorChanel==0xffff) &&
     (NumbAbon!=0xff) && !pbtTlfn) return(0xff);
  psAbnt=MassAbntState;
  for(btIndex=0;btIndex<MaxAbonentActive;btIndex++,psAbnt++)
  {
    if(psAbnt->wConnectionPoint==0xffff) continue;
    if(btPultNb!=0xff)
    {
      if(btPultNb!=psAbnt->btPultNb) continue;
    }
    if(wConnectionPoint!=0xffff)
    {
      if(wConnectionPoint!=psAbnt->wConnectionPoint) continue;
    }
    if(wOperatorChanel!=0xffff)
    {
      if(wOperatorChanel!=psAbnt->wOperatorChanel) continue;
    }
    if(TypeAbon!=0xff)
    {
      if(TypeAbon!=psAbnt->btType) continue;
    }
    if(NumbAbon!=0xff)
    {
      if(NumbAbon!=psAbnt->NumbAbon) continue;
    }
    if(pbtTlfn)
    {
      if(!TlfnEqual(pbtTlfn,psAbnt->btTlfn)) continue;
    }
    return(btIndex);
  }
  return(0xff);
}

unsigned char FindNextAbonentIndex(unsigned char btIndex,unsigned char btPultNb,
                                   unsigned short wConnectionPoint,unsigned short wOperatorChanel,
                                   unsigned char TypeAbon,unsigned char NumbAbon,unsigned char *pbtTlfn)
{
  ABONENT_STATE_TP *psAbnt;

  if((btPultNb==0xff) && (wConnectionPoint==0xffff) && (wOperatorChanel==0xffff) &&
     (NumbAbon!=0xff) && !pbtTlfn) return(0xff);
  btIndex++;
  psAbnt=MassAbntState+btIndex;
  for(;btIndex<MaxAbonentActive;btIndex++,psAbnt++)
  {
    if(psAbnt->wConnectionPoint==0xffff) continue;
    if(btPultNb!=0xff)
    {
      if(btPultNb!=psAbnt->btPultNb) continue;
    }
    if(wConnectionPoint!=0xffff)
    {
      if(wConnectionPoint!=psAbnt->wConnectionPoint) continue;
    }
    if(wOperatorChanel!=0xffff)
    {
      if(wOperatorChanel!=psAbnt->wOperatorChanel) continue;
    }
    if(TypeAbon!=0xff)
    {
      if(TypeAbon!=psAbnt->btType) continue;
    }
    if(NumbAbon!=0xff)
    {
      if(NumbAbon!=psAbnt->NumbAbon) continue;
    }
    if(pbtTlfn)
    {
      if(!TlfnEqual(pbtTlfn,psAbnt->btTlfn)) continue;
    }
    return(btIndex);
  }
  return(0xff);
}

ABONENT_STATE_TP *GetAbonentStatePtrByFind(unsigned char PultNb,
                                     unsigned short wConnectionPoint,unsigned short wOperatorChanel,
                                     unsigned char TypeAbon,unsigned char NumbAbon,unsigned char *pbtTlfn)
{
  unsigned char btIndex;
  
  btIndex=FindFirstAbonentIndex(PultNb,wConnectionPoint,wOperatorChanel,TypeAbon,NumbAbon,pbtTlfn);
  if(btIndex>=MaxAbonentActive) return(NULL);
  return(MassAbntState+btIndex);
}

unsigned char GetFreeAbonentStateIndex(void)
{
  unsigned char btIndex;
  ABONENT_STATE_TP *psAbnt=MassAbntState;

  for(btIndex=0;btIndex<MaxAbonentActive;btIndex++,psAbnt++)
  {
    if(psAbnt->wConnectionPoint==0xffff)
    { psAbnt->dwState=0; return(btIndex); }
  }
  return(0xff);
}

ABONENT_STATE_TP *GetAbonentStatePtrFromIndex(unsigned char btIndex)
{
  if(btIndex>=MaxAbonentActive) return(NULL);
  return(MassAbntState+btIndex);
}

ABONENT_STATE_TP *GetFreeAbonentStatePtr(void)
{ return(GetAbonentStatePtrFromIndex(GetFreeAbonentStateIndex())); }

unsigned char GetAbonentStateIndexFromPtr(ABONENT_STATE_TP *psAbnt0)
{
  unsigned char btIndex;
  ABONENT_STATE_TP *psAbnt=MassAbntState;

  for(btIndex=0;btIndex<MaxAbonentActive;btIndex++,psAbnt++)
  {
    if(psAbnt0==psAbnt) return(btIndex);
  }
  return(0xff);
}

ABONENT_STATE_TP *GetAbonentStatePtrFromKey(unsigned char PultNb,unsigned char btKey,unsigned char btLayer)
{
  unsigned char btIndex;
  ABONENT_STATE_TP *psAbnt=MassAbntState;

  if((PultNb==0xff) || (btLayer==0xff)) return(NULL);
  for(btIndex=0;btIndex<MaxAbonentActive;btIndex++,psAbnt++)
  {
    if(psAbnt->wConnectionPoint!=0xffff)
    {
      if((psAbnt->btPultNb==PultNb) && (psAbnt->btKey==btKey) && (psAbnt->btLayer==btLayer)) return(psAbnt);
    }
  }
  return(NULL);
}

unsigned char IsIncomingAbonentStateFromPtr(ABONENT_STATE_TP *psAbnt)
{
  if(psAbnt->dwState & d_IncomingTalkState) return(1);
  return(0);
}

unsigned char GetAbonentStateFromPtr(ABONENT_STATE_TP *psAbnt)
{
  return(psAbnt->dwState & 0xff);
}

unsigned char GetAbonentStateIndFromKey(unsigned char PultNb,unsigned char btKey,unsigned char btLayer)
{
  ABONENT_STATE_TP *psAbnt=GetAbonentStatePtrFromKey(PultNb,btKey,btLayer);

  if(psAbnt) return(GetAbonentStateIndexFromPtr(psAbnt));
  return(0xff);
}

unsigned char GetAbonentStateFromKey(unsigned char PultNb,unsigned char btKey,unsigned char btLayer)
{
  ABONENT_STATE_TP *psAbnt=GetAbonentStatePtrFromKey(PultNb,btKey,btLayer);

  if(psAbnt) return(GetAbonentStateFromPtr(psAbnt));
  return(Passive);
}

bool IsAutomaticCallAbonent(ABONENT_STATE_TP *psAbnt)
{ return((psAbnt->dwState & d_AutomaticCallAbonent)!=0); }

void SetAutomaticCallAbonent(ABONENT_STATE_TP *psAbnt)
{ psAbnt->dwState|=d_AutomaticCallAbonent; }

unsigned char GetAbonentStateIndByState(unsigned char PultNb,unsigned char btIndex,unsigned char btState)
{
  ABONENT_STATE_TP *psAbnt;

  if(btIndex==0xff) btIndex=0;
  else btIndex++;
  for(;btIndex<MaxAbonentActive;btIndex++)
  {
    psAbnt=MassAbntState+btIndex;
    if((psAbnt->wConnectionPoint!=0xffff) && (psAbnt->btPultNb==PultNb))
    {
      if(GetAbonentStateFromPtr(psAbnt)==btState) return(btIndex);
    }
  }
  return(0xff);
}

unsigned short GetAbonentOperatorChannelFromIndex(unsigned char btInd)
{
  ABONENT_STATE_TP *psAbnt;

  if(btInd==0xff) return(0xffff);
  psAbnt=MassAbntState+btInd;
  return(psAbnt->wOperatorChanel);
}

unsigned short GetAbonentOperatorChannelFromKey(unsigned char PultNb,unsigned char btKey,unsigned char btLayer)
{
  ABONENT_STATE_TP *psAbnt=GetAbonentStatePtrFromKey(PultNb,btKey,btLayer);

  if(psAbnt) return(psAbnt->wOperatorChanel);
  return(0xffff);
}

unsigned short GetAbonentConnectionPointFromIndex(unsigned char btInd)
{
  ABONENT_STATE_TP *psAbnt;

  if(btInd==0xff) return(0xffff);
  psAbnt=MassAbntState+btInd;
  return(psAbnt->wConnectionPoint);
}

unsigned short GetAbonentConnectionPointFromKey(unsigned char PultNb,unsigned char btKey,unsigned char btLayer)
{
  ABONENT_STATE_TP *psAbnt=GetAbonentStatePtrFromKey(PultNb,btKey,btLayer);

  if(psAbnt) return(psAbnt->wConnectionPoint);
  return(0xffff);
}

bool IsTalk2Pult(unsigned char PultNb,unsigned short wOperator)
{
  unsigned char btIndex,btState;
  ABONENT_STATE_TP *psAbnt=MassAbntState;

  for(btIndex=0;btIndex<MaxAbonentActive;btIndex++,psAbnt++)
  {
    if(psAbnt->wConnectionPoint!=0xffff)
    {
      btState=GetAbonentStateFromPtr(psAbnt);
      if((psAbnt->btPultNb==PultNb) &&
         ((btState==Talk) || (btState==Call)))
      {
        if((wOperator==0xffff) || (psAbnt->wOperatorChanel==wOperator))
        {
          if(IsSelectorActive(PultNb) &&
             IsKey2Selector(PultNb,psAbnt->btKey,psAbnt->btLayer,d_Abonent2SelectorCurrentPult)) continue;
          return(true);
        }
      }
    }
  }
  return(false);
}

bool IsIncomingCall2Pult(unsigned char PultNb)
{
  unsigned char btIndex;
  ABONENT_STATE_TP *psAbnt=MassAbntState;

  for(btIndex=0;btIndex<MaxAbonentActive;btIndex++,psAbnt++)
  {
    if((psAbnt->wConnectionPoint!=0xffff) && (psAbnt->btPultNb==PultNb))
    {
      if(GetAbonentStateFromPtr(psAbnt)==IncomingCall) return(true);
    }
  }
  return(false);
}

unsigned char GetCountUknownISDNAbonent(unsigned char PultNb)
{
  unsigned char btIndex,btCnt;
  ABONENT_STATE_TP *psAbnt=MassAbntState;

  for(btIndex=0,btCnt=0;btIndex<MaxAbonentActive;btIndex++,psAbnt++)
  {
    if((psAbnt->wConnectionPoint!=0xffff) && (psAbnt->btPultNb==PultNb))
    {
      if((psAbnt->btType==ISDN) && !psAbnt->NumbAbon) btCnt++;
    }
  }
  return(btCnt);
}

void RestoreAbonentLedState(unsigned char PultNb)
{
  unsigned char btIndex,btLayer;
  ABONENT_STATE_TP *psAbnt=MassAbntState;

  btLayer=GetCurrentLayer(PultNb);
  for(btIndex=0;btIndex<MaxAbonentActive;btIndex++,psAbnt++)
  {
    if(psAbnt->wConnectionPoint!=0xffff)
    {
      if((psAbnt->btPultNb==PultNb) && (psAbnt->btLayer==btLayer))
      {
        if(!IsKey2Selector(PultNb,psAbnt->btKey,btLayer,d_Abonent2SelectorCurrentPult))
        { IndicCmd(PultNb,btLayer,psAbnt->btKey,psAbnt->btLedState,0,0); }
      }
    }
  }
}

void InvalidateAbonentState4Pult(unsigned char PultNb)
{
  unsigned char btIndex;
  ABONENT_STATE_TP *psAbnt=MassAbntState;

  for(btIndex=0;btIndex<MaxAbonentActive;btIndex++,psAbnt++)
  {
    if(psAbnt->wConnectionPoint!=0xffff) psAbnt->dwState&=~(1<<(8+PultNb));
  }
}

bool IsCallKeyEqual(sKBKey *psKey0,sKBKey *psKey1)
{
  if((psKey1->CodeKey[0]==CallKey) && (psKey0->TypeAbon==psKey1->TypeAbon))
  {
    switch(psKey0->TypeAbon)
    {
      case Selector:
      case ATC:
        if(psKey0->NChan==psKey1->NChan) return(true);
        break;
      case PultCall:
      case IPPM:
      case ISDN:
        if(psKey0->NumbAbon && (psKey0->NumbAbon==psKey1->NumbAbon)) return(true);
        break;
      case GroupTDC:
      case TDC:
        if((psKey0->NChan==psKey1->NChan) && (psKey0->NumbAbon==psKey1->NumbAbon)) return(true);
        break;
      case Level:
      case PPSR:
      case RSDT:
        if(psKey0->NChan==psKey1->NChan) return(true);
        break;
      default: break;
    }
  }
  return(false);
}

bool FindAbonent2Kbd(ABONENT_STATE_TP *psAbnt0,unsigned char PultNb,unsigned char btLayer,unsigned char *pbtKey)
{
  unsigned char btKey;
  sKBKey *psKey0,*psKey1;

  psKey0=&MassKey[psAbnt0->btPultNb][psAbnt0->btLayer][psAbnt0->btKey];
  for(btKey=0;btKey<NumberKey;btKey++)
  {
    psKey1=&MassKey[PultNb][btLayer][btKey];
    *pbtKey=btKey;
    if(IsCallKeyEqual(psKey0,psKey1)) return(true);
  }
  return(false);
}

void SetLedColorIfTalk(ABONENT_STATE_TP *psAbnt)
{
  if(IsSelectorActive(psAbnt->btPultNb) &&
     (!IsKey2Selector(psAbnt->btPultNb,psAbnt->btKey,psAbnt->btLayer,d_Abonent2SelectorCurrentPult)))
  { psAbnt->btLedState=dLedStateTalkIfSelector; }
  else psAbnt->btLedState=dLedStateTalk;
}

// ����������� ��������� ���������� �� ��������� �������
#define d_MaxDecodeAbntState    6
unsigned char LedState4OtherPult(ABONENT_STATE_TP *psAbnt)
{
  unsigned char i,btLedState,
               btMassLedState[d_MaxDecodeAbntState][2]=
{
{dLedStateTalk,ORANGE}, // ������� ���� ������ �������
{dLedStateTalkIfSelector,ORANGE}, // ������� ���� ������ �������
{dLedStateIncomingCall,dLedStateIncomingCall},  // �������� �����
{dLedStateOutgoingCall,ORANGE},  // ��������� �����
{dLedStatePultListen,OFF}, // ��������� ������
{dLedStateDisableOut,ORANGE} // ������� ���� ������ �������
//{RED,},
//{ORANGE,},

//{BLINK_RED,},
//{BLINK_ORANGE,},

//{FAST_BLINK_GREEN,},
//{FAST_BLINK_RED,},
//{FAST_BLINK_ORANGE,},

//{BLINK_RED_GREEN,},

//{FAST_BLINK_GREEN_RED,},
//{FAST_BLINK_GREEN_ORANGE,},
//{FAST_BLINK_RED_ORANGE,},
};

  btLedState=psAbnt->btLedState;
  for(i=0;i<d_MaxDecodeAbntState;i++)
  {
    if((btLedState==dLedStateIncomingCall) && (psAbnt->btType==ISDN)) return(ORANGE);
    if(btLedState==btMassLedState[i][0]) return(btMassLedState[i][1]);
  }
  btLedState=OFF;
  return(btLedState);
}

bool NeedReshowActualMKAAbntState(ABONENT_STATE_TP *psAbnt0)
{
  unsigned char PultNb,btLayer,btKey;
  bool bExistAbnt=false;
  ABONENT_STATE_TP *psAbnt;

  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if(MassM1[PultNb] && (psAbnt0->btPultNb!=PultNb))
    {
      btLayer=GetCurrentLayer(PultNb);
      if(FindAbonent2Kbd(psAbnt0,PultNb,btLayer,&btKey))
      {
        psAbnt=GetAbonentStatePtrFromKey(PultNb,btKey,btLayer);
        if(psAbnt)
        {
          psAbnt->dwState&=(~(1<<(8+psAbnt0->btPultNb)));
          bExistAbnt=true;
        }
      }
    }
  }
  return(bExistAbnt);
}

void PutAbntState2AllPult(ABONENT_STATE_TP *psAbnt)
{
  unsigned char SN,PultNb,btLayer,btKey,btLedState,btIndex,btState0,btState;
  bool bIndicate;

  if(GetAbonentStateFromPtr(psAbnt)==Off) btState0=Off;
  else btState0=d_AbntBusyOtherPult;
  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    SN=MassM1[PultNb];
    if(SN && (psAbnt->btPultNb!=PultNb))
    {
      bIndicate=!(psAbnt->dwState & (1<<(8+PultNb)));
      if(bIndicate)
      {
        btLayer=GetCurrentLayer(PultNb);
        if(FindAbonent2Kbd(psAbnt,PultNb,btLayer,&btKey))
        {
          btState=GetAbonentStateFromKey(PultNb,btKey,btLayer);
          if((btState==Passive) || (btState==Off))
          {
            if(SN<16)
            {
              btLedState=OFF;
              if(!IsKey2Selector(PultNb,btKey,btLayer,d_Abonent2SelectorOtherPult))
              {
                if(GetAbonentStateFromPtr(psAbnt)!=Off) btLedState=LedState4OtherPult(psAbnt);
              }
              if(psAbnt->btKeyATCDirect!=0xff) IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKeyATCDirect,btLedState,0,0);
              IndicCmd(PultNb,btLayer,btKey,btLedState,0,0);
            }
            else
            {
              xSendKeyState104ToIpPult(SN,btKey,btState0);
              if(psAbnt->btKeyATCDirect!=0xff) xSendKeyState104ToIpPult(SN,psAbnt->btKeyATCDirect,btState0);
            }
          }
        }
        psAbnt->dwState|=1<<(8+PultNb);
      }
    }
  }
}

void PutAbonentsState2AllPult(void)
{
  unsigned char btIndex;
  ABONENT_STATE_TP *psAbnt=MassAbntState;

  for(btIndex=0;btIndex<MaxAbonentActive;btIndex++,psAbnt++)
  {
    if(psAbnt->wConnectionPoint!=0xffff) PutAbntState2AllPult(psAbnt);
  }
}

unsigned char CallKeyState4IpPult(unsigned char PultNb0,unsigned char btKey,unsigned char btLayer)
{
  unsigned char btState,PultNb,btConf;
  sKBKey *psKey0,*psKey1;
  ABONENT_STATE_TP *psAbnt;

  psAbnt=GetAbonentStatePtrFromKey(PultNb0,btKey,btLayer);
  if(psAbnt)
  { // ������� ������� �� ������� ������
    btState=psAbnt->dwState & 0xf;
    if(IsKey2Selector(PultNb0,btKey,btLayer,d_Abonent2SelectorCurrentPult)) btState|=d_AbntInSelector;
    if(IsKey2Conference(PultNb0,btKey,btLayer,&btConf)) btState=d_AbntInConference | btConf;
    // �������!!!
    return btState;
  }
  btState=Off;
  psKey0=&MassKey[PultNb0][btLayer][btKey];
  for(PultNb=0;PultNb<NumberKeyboard;PultNb++)
  {
    if(!MassM1[PultNb] || (PultNb==PultNb0)) continue;
    btLayer=GetCurrentLayer(PultNb);
    for(btKey=0;btKey<NumberKey;btKey++)
    {
      psKey1=&MassKey[PultNb][btLayer][btKey];
      if(IsCallKeyEqual(psKey0,psKey1))
      {
        psAbnt=GetAbonentStatePtrFromKey(PultNb,btKey,btLayer);
        if(psAbnt)
        { // ������� ������� �� ������ ������
          btState=(psAbnt->dwState & 0xf) | d_AbntBusyOtherPult;
          if(IsKey2Selector(PultNb,btKey,btLayer,d_Abonent2SelectorCurrentPult)) btState|=d_AbntInSelector;
          // �������!!!
          return btState;
        }
      }
    }
  }
  return btState;
}

bool OtherAbonentUsedMKALine(ABONENT_STATE_TP *psAbnt0)
{
  unsigned char btIndex;
  ABONENT_STATE_TP *psAbnt=MassAbntState;

  for(btIndex=0;btIndex<MaxAbonentActive;btIndex++,psAbnt++)
  {
    if(psAbnt!=psAbnt0)
    {
      if((psAbnt->wConnectionPoint==psAbnt0->wConnectionPoint) &&
         (psAbnt->btPultNb==psAbnt0->btPultNb)) return(true);
    }
  }
  return(false);
}

/*
������ � ������:
���� 0 = 117 - �������
���� 1 - ���������� ����� ������
���� 2,3,4,5 - TimeStamp
���� N -  ��������� N-������ HI
���� N+1 - ��������� N-������ LOW

����������: � ������ ���������� ��������� ���� ������ ������, ������ ���� ������ �� ��������, �� ��������� ������ ==0!
��.����� ��������� ������ ������ ���������� � ������������ � ������ keyboard.h,
������� - ��� D0==1 ���� ������� ������� �� �������� ��������� ��������� ��������
*/
void _xSendKeyState2Record(ABONENT_STATE_TP *psAbnt,unsigned int dwIpAddr,unsigned short wPort)
{
  unsigned char btStateL,btStateH,
                btKey,PultNb=psAbnt->btPultNb,
                btLayer=GetCurrentLayer(PultNb);
  unsigned short wOff;
  static sSoundIp sInfo;

  sInfo.A1=dwIpAddr;
  sInfo.PORT1=wPort;
  sInfo.ByteInfo[0]=117;
  sInfo.ByteInfo[1]=MassM1[PultNb];
  wOff=2+GetRecordTimeStamp(sInfo.ByteInfo+2);
  for(btKey=0;btKey<NumberKey;btKey++)
  {
    if(MassKey[PultNb][btLayer][btKey].CodeKey[0]==CallKey)
    {
      btStateL=CallKeyState4IpPult(PultNb,btKey,btLayer);
      if(btStateL & d_AbntBusyOtherPult) btStateH=btStateL=0;
      else btStateH=IsAbonentLevelGT(psAbnt->wConnectionPoint);
    }
    else
    { btStateH=btStateL=0; }
    sInfo.ByteInfo[wOff++]=btStateH;
    sInfo.ByteInfo[wOff++]=btStateL;
  }
  sInfo.L=wOff;
  xQueueSend(xQueueSoundIpOut,&sInfo,0,0);
}

void SendAbntState2Ip(ABONENT_STATE_TP *psAbnt)
{
  unsigned char SN,btState;
  unsigned int dwIpAddr;
  unsigned short wPort;

  SN=MassM1[psAbnt->btPultNb];
  if(SN>=16)
  {
    btState=CallKeyState4IpPult(psAbnt->btPultNb,psAbnt->btKey,psAbnt->btLayer);
    xSendKeyState104ToIpPult(SN,psAbnt->btKey,btState);
  }
  else IndicateLayerKey(psAbnt->btPultNb);
//  if(GetAddrInfo4Record(psAbnt->btPultNb,0,&dwIpAddr,&wPort)) _xSendKeyState2Record(psAbnt,dwIpAddr,wPort);
//  if(GetAddrInfo4Record(psAbnt->btPultNb,1,&dwIpAddr,&wPort)) _xSendKeyState2Record(psAbnt,dwIpAddr,wPort);
}

void SetAbonentMKALine2TalkIfNeed(ABONENT_STATE_TP *psAbnt0)
{
  unsigned char btIndex,btState;
  ABONENT_STATE_TP *psAbnt=MassAbntState;

  for(btIndex=0;btIndex<MaxAbonentActive;btIndex++,psAbnt++)
  {
    if(psAbnt==psAbnt0) continue;
    if((psAbnt->wConnectionPoint==psAbnt0->wConnectionPoint) && (psAbnt->btPultNb==psAbnt0->btPultNb))
    {
      btState=GetAbonentStateFromPtr(psAbnt);
      if((btState==Hold) || (btState==IncomingCall))
      {
        psAbnt->dwState=Talk;
        SendAbntState2Ip(psAbnt);
      }
    }
  }
}

bool SetAbonentStateByPtr(ABONENT_STATE_TP *psAbnt,unsigned char btState,unsigned char btExt)
{
  if(GetAbonentStateFromPtr(psAbnt)==btState) return(true);
  switch(btState)
  {
    case Off:
      psAbnt->btLedState=OFF;
      btExt&=~dIncomingTalkFlag;
      break;
    case Hold:
      if(psAbnt->dwState & d_IncomingTalkState) btExt|=dIncomingTalkFlag;
      switch(psAbnt->btType)
      {
        case GroupTDC:
        case TDC:
          if(OtherAbonentUsedMKALine(psAbnt)) return(false);
          break;
      }
      psAbnt->btLedState=dLedStateHold;
      break;
    case Talk:
      if(psAbnt->dwState & d_IncomingTalkState) btExt|=dIncomingTalkFlag;
      switch(psAbnt->btType)
      {
        case TDC:
          SetAbonentMKALine2TalkIfNeed(psAbnt);
          break;
      }
      break;
    case SelectorListen:
      psAbnt->btLedState=dLedStateListenSelector;
      break;
    case SelectorDisableOut:
      psAbnt->btLedState=dLedStateDisableOut;
      break;
  }
  psAbnt->dwState=btState;
  if(btExt & dIncomingTalkFlag) psAbnt->dwState|=d_IncomingTalkState;
  SendAbntState2Ip(psAbnt);
  return(true);
}

void ClearAbonentStateByPtr(ABONENT_STATE_TP *psAbnt)
{
  if((psAbnt>=MassAbntState) && (psAbnt<(MassAbntState+MaxAbonentActive)))
  {
    if(psAbnt->wConnectionPoint!=0xffff)
    {
      CmdEnDisOutCP(psAbnt->wConnectionPoint,true);
      SetAbonentStateByPtr(psAbnt,Off,0);
      PutAbntState2AllPult(psAbnt);
    }
    else SetAbonentStateByPtr(psAbnt,Off,0);
    memset(psAbnt,0xff,sizeof(ABONENT_STATE_TP));
  }
}

void ClearAbonentStateByIndex(unsigned char btIndex)
{
  if(btIndex<MaxAbonentActive) ClearAbonentStateByPtr(MassAbntState+btIndex);
}

unsigned char ServiceKeyState4IpPult(unsigned char PultNb,unsigned char btKey,unsigned char btLayer)
{
  sKBKey *psKey;

  psKey=&MassKey[PultNb][btLayer][btKey];
  if((psKey->CodeKey[1]>=Selector1) && (psKey->CodeKey[1]<=Selector16)) return(GetSelectorState(PultNb,btKey));
  if((psKey->CodeKey[1]>=Conference1) && (psKey->CodeKey[1]<=Conference6))
  { return(GetConferenceState(PultNb,psKey->CodeKey[1]-Conference1)); }
  if(psKey->CodeKey[1]==TikkerKey) return(GetTikkerState(PultNb));
  if(psKey->CodeKey[1]==DisableEnableOut) return(IsPressDisEnOut(PultNb) ? d_ServiceOn : Off);
  if(psKey->CodeKey[1]==Choice) return(IsPressChoice(PultNb) ? d_ServiceOn : Off);
  if(psKey->CodeKey[1]==KeyOff) return(IsKeyOff(PultNb) ? d_ServiceOn : Off);
  // �������!!!
  return(Off);
}

void SendAllKey2IpPult(unsigned char SN,unsigned char PultNb,unsigned char btLayer,sInfoKadr *psInfo)
{
  unsigned char btKey,btState,btPage,btKeyOff;

  psInfo->ByteInfoKadr[0]=104;
  psInfo->ByteInfoKadr[1]=SN;
  for(btPage=0;btPage<NumberKeyPage;btPage++)
  {
    if(btPage)
    { // �������� �� 255 ������ � ����!
      psInfo->ByteInfoKadr[2]=NumberKey-NumberKeyPageSize;
      psInfo->L=3+2*(NumberKey-NumberKeyPageSize);
    }
    else
    {
      psInfo->ByteInfoKadr[2]=NumberKeyPageSize;
      psInfo->L=3+2*NumberKeyPageSize;
    }
    btKeyOff=btPage*NumberKeyPageSize;
    for(btKey=0;btKey<NumberKeyPageSize;btKey++)
    {
      if(btPage)
      {
        if((unsigned short)btKey+btKeyOff>=NumberKey) break; // �������� �� 255 ������ � ����!
      }
      btState=Off;
      switch(MassKey[PultNb][btLayer][btKey+btKeyOff].CodeKey[0])
      {
        case CallKey:
          btState=CallKeyState4IpPult(PultNb,btKey+btKeyOff,btLayer);
          if(btState & 0x80) btState=0x80; // ���������� ��� ���������
          break;
        case ServiceKey:
          btState=ServiceKeyState4IpPult(PultNb,btKey+btKeyOff,btLayer);
          break;
      }
      psInfo->ByteInfoKadr[3+btKey*2]=btKey+btKeyOff;
      psInfo->ByteInfoKadr[4+btKey*2]=btState;
    }
    xQueueSend(xQueueReply[psInfo->Source],psInfo,0,0);
  }
}

void _xSendActiveUnnameAbnt_ATC_ISDN(unsigned char SN,ABONENT_STATE_TP *psAbnt,sInfoKadr *psInfo)
{
  psInfo->L=23;
  psInfo->ByteInfoKadr[0]=107;
  psInfo->ByteInfoKadr[1]=SN;
  psInfo->ByteInfoKadr[2]=psAbnt->btKey;
  psInfo->ByteInfoKadr[3]=GetAbonentStateFromPtr(psAbnt);
  psInfo->ByteInfoKadr[4]=IsIncomingAbonentStateFromPtr(psAbnt); // 0 - ��������� �����!
  psInfo->ByteInfoKadr[5]=psAbnt->wConnectionPoint;
  memcpy(psInfo->ByteInfoKadr+6,psAbnt->btTlfn,17);
  xQueueSend(xQueueReply[psInfo->Source],psInfo,0,0);
}

void SendActiveUnnameAbnt_ATC_ISDN(ABONENT_STATE_TP *psAbnt)
{
  unsigned char SN=MassM1[psAbnt->btPultNb];
  static sInfoKadr sInfo;

  if((SN>=16) && GetAddressIpPult(SN,&sInfo.A1,&sInfo.PORT1))
  {
    sInfo.Source=UserUDP;
    _xSendActiveUnnameAbnt_ATC_ISDN(SN,psAbnt,&sInfo);
  }
}

void SendAllActiveUnnameAbnt_ATC_ISDN(unsigned char SN,unsigned char PultNb,unsigned char btLayer,sInfoKadr *psInfo)
{
  unsigned char btIndex;
  ABONENT_STATE_TP *psAbnt=MassAbntState;

  for(btIndex=0;btIndex<MaxAbonentActive;btIndex++,psAbnt++)
  {
    if((psAbnt->wConnectionPoint!=0xffff) && (psAbnt->btPultNb==PultNb))
    {
      if(((psAbnt->btType==ISDN) && !psAbnt->NumbAbon) || (psAbnt->btType==ATC))
      { _xSendActiveUnnameAbnt_ATC_ISDN(SN,psAbnt,psInfo); }
    }
  }
}

void vCmdReadAllKeyboardState(sCommand *psViewCommand,sInfoKadr *psInfo)
{
  unsigned int dwIP;
  unsigned short wPort;
  unsigned char PultNb,SN,btLayer;

  SN=psViewCommand->BufCommand[1];
  if(SN<16) return;
  PultNb=GetPultIndFromSN(SN);
  if(PultNb==0xff) return;
  if(GetAddressIpPult(SN,&dwIP,&wPort))
  {
    vPortEnableSwitchTask(false);
    btLayer=GetCurrentLayer(PultNb);
    SendLayerAndLock2IpPult(PultNb,psInfo);
    SendAllKey2IpPult(SN,PultNb,btLayer,psInfo);
    SendAllActiveUnnameAbnt_ATC_ISDN(SN,PultNb,btLayer,psInfo);
    SendAllAbonentSelector2IpPult(PultNb);
    SendIpReinitCount2IpPult(SN,psInfo);
    vPortEnableSwitchTask(true);
  }
}

/*
�������� �������� ������ �����:
GREEN           - ������� ����
RED             - ��������������� ������� ����
BLINK_RED       - ��������������� ���������� ����
NONE            - ���� �� �������
ORANGE          - ���� �� �������, �� �� ��� ���� ���������
BLINK_ORANGE    - ���� �� �������, �� �� ���� �������� �������� �����
*/
unsigned char GetLayerLedColor(unsigned char PultNb,unsigned char btLayer)
{
  if(btLayer==GetCurrentLayer(PultNb))
  {
    if(IsKbdLayerLock(PultNb,btLayer)) return(RED);
    return(GREEN);
  }
  else
  {
    unsigned char btLedColor,btInd,btState;
    ABONENT_STATE_TP *psAbnt=MassAbntState;

    if(IsKbdLayerLock(PultNb,btLayer)) return(BLINK_RED);
    btLedColor=OFF;
    for(btInd=0;btInd<MaxAbonentActive;btInd++,psAbnt++)
    {
      if((psAbnt->wConnectionPoint!=0xffff) && (psAbnt->btPultNb==PultNb) && (psAbnt->btLayer==btLayer))
      {
        btState=psAbnt->dwState & 0x0f;
        if(btState==IncomingCall) return(BLINK_ORANGE);
        if(btState) btLedColor=ORANGE;
      }
    }
    return(btLedColor);
  }
}

