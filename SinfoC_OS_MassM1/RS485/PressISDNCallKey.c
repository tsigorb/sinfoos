#include "..\Sinfo.h"
#include "..\EDSS/pri_config.h"
#include "..\EDSS/libpri.h"
#include "GlobalData.h"
#include "FunctionsPrototypes.h"
#include "KeyboardFn.h"

/////////////////////////////////////////////////////////////////////
unsigned char FindE1fromDialplan(char *NumbDst);

/////////////////////////////////////////////////////////////////////
void OtboiAbonentISDN(unsigned char PultNb,unsigned char Key,unsigned char Layer,
                      unsigned char btReason)
{
  ABONENT_STATE_TP *psAbnt;
  
  psAbnt=GetAbonentStatePtrFromKey(PultNb,Key,Layer);
  if(!psAbnt) return;
//  if(GetAbonentStateFromPtr(psAbnt)==Hold) StopKeep(psAbnt->wConnectionPoint);
  SendCmdOtboiISDN(btReason,psAbnt->wConnectionPoint,psAbnt->wOperatorChanel);
  if(ListAbntDisplay[PultNb][0].NChan==GetAbonentStateIndexFromPtr(psAbnt))
  {
    IndicClock(PultNb);
    ListAbntDisplay[PultNb][0].NChan=0xff;
  }
  OtboiAbonentCommonPart(PultNb,Key,Layer,NULL,NULL);
}

bool _xOutgoingCallISDN(unsigned short NChan,unsigned short wOper,unsigned char btE1,
                        char *BufNumbDest,char *BufNumbSrs,char *BufNameSrs,unsigned char SN)
{
  static sCommand sCmd;

  sCmd.Source=UserRS485; // �������� ������� RS485
  sCmd.L=21; // ���������� ���� � �������
  sCmd.A1=0;
  sCmd.BufCommand[0]=10; // �������
  sCmd.BufCommand[1]=201; // ����������
  sCmd.BufCommand[2]=NChan >> 8; // N �����������
  sCmd.BufCommand[3]=NChan; // N �����������
  sCmd.BufCommand[4]=wOper >> 8; //����� ������ ���������
  sCmd.BufCommand[5]=wOper; // ����� ������ ���������
  sCmd.BufCommand[6]=btE1;
  sCmd.BufCommand[7]=((unsigned int)BufNumbDest) >> 24; // ��������� �� ��������� �����
  sCmd.BufCommand[8]=((unsigned int)BufNumbDest) >> 16;
  sCmd.BufCommand[9]=((unsigned int)BufNumbDest) >> 8;
  sCmd.BufCommand[10]=(unsigned int)BufNumbDest;
  sCmd.BufCommand[11]=1; // ��� �����
  sCmd.BufCommand[12]=((unsigned int)BufNumbSrs) >> 24; // ��������� �� ����� �����������
  sCmd.BufCommand[13]=((unsigned int)BufNumbSrs) >> 16;
  sCmd.BufCommand[14]=((unsigned int)BufNumbSrs) >> 8;
  sCmd.BufCommand[15]=(unsigned int)BufNumbSrs;
  sCmd.BufCommand[16]=((unsigned int)BufNameSrs) >> 24; // ��������� �� ��� �����������
  sCmd.BufCommand[17]=((unsigned int)BufNameSrs) >> 16;
  sCmd.BufCommand[18]=((unsigned int)BufNameSrs) >> 8;
  sCmd.BufCommand[19]=(unsigned int)BufNameSrs;
  sCmd.BufCommand[20]=SN; // SN ������ ��� �������� ��������� ��� 0, ���� ��� ������� �������
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else 
  {
    if(WaitReply(11,201)) return(true);
    else CountErrEDSS++;
  }
  return(false);
}

void OutgoingCallISDN(unsigned char PultNb,unsigned char Key,unsigned char Layer)
{
  unsigned short NChan,wOper;
  unsigned char btNewKey,btLayer,NumbAbon,btE1,btE1Cnt,
                Ln,btSound,*pbtMasDg;
  ABONENT_STATE_TP *psAbnt;

  wOper=GetOperatorChannelFromKey(PultNb,Key,Layer);
  if(!wOper)
  {
    PutErrInfo2Display(PultNb,"�������� �����������");
    return;
  }
  NumbAbon=MassKey[PultNb][Layer][Key].NumbAbon;// ����� �������� � ������� ���������+1 ��� 0
  if(!NumbAbon)
  {
    if(IsAutomaticCall(PultNb)) return;
    if(!IsOutgoingNumberTmpPresent(PultNb) || !GetOutgoingNumberTmp(PultNb,&pbtMasDg,&Ln)) return;
    OutgoingNumberTmpTOutClear(PultNb);
    btNewKey=FindISDNAbonentKey(PultNb,pbtMasDg,Ln,&NumbAbon,&btLayer);
    if(btNewKey!=0xff)
    {
      IndicClock(PultNb);
      if(IsFreeISDNAbonent(PultNb,btNewKey,btLayer))
      {
        Key=btNewKey;
        Layer=btLayer;
        wOper=GetOperatorChannelFromKey(PultNb,Key,Layer);
        if(!wOper)
        {
          PutErrInfo2Display(PultNb,"�������� �����������");
          return;
        }
      }
      else
      { PutErrInfo2Display(PultNb,NULL); return; }
    }
  }
  NChan=GetFreeConnectionPoint();
  psAbnt=GetFreeAbonentStatePtr();
  if((NChan!=0xFF) && psAbnt)
  {
    if(NumbAbon) memcpy(psAbnt->btTlfn,MassTel[NumbAbon-1],MAX_DIGIT_TLFN);
    else
    {
      memcpy(psAbnt->btTlfn,pbtMasDg,Ln);
      psAbnt->btTlfn[Ln]=0xff;
    }
    Tlfn2Str(psAbnt->NumbDst,psAbnt->btTlfn);
    Tlfn2Str(psAbnt->NumbSrs,GetPultTlfnPtr(PultNb));
    strncpy(psAbnt->NameSrs,GetPultNamePtr(PultNb),d_NameLen);
    btE1Cnt=0;
    btE1=FindE1fromDialplan(psAbnt->NumbDst);
    if(btE1==0xff) btE1=GetFirstE1();
    else btE1Cnt=NUM_TRUNKS;
    do
    { // ���� �� ���� ��������� ������� E1
SendStr2IpLogger("$ERROR$OutgoingCallISDN: btE1=%d,wChan=%d",btE1,btE1Cnt);
      if(btE1!=0xff)
      {
        if(_xOutgoingCallISDN(NChan,wOper,btE1,
                              psAbnt->NumbDst,psAbnt->NumbSrs,psAbnt->NameSrs,
                              IsAutomaticCall(PultNb) ? MassM1[PultNb] : 0)) break;
      }
      if(btE1Cnt>=NUM_TRUNKS)
      {
        DelConnectionPoint(NChan);
SendStr2IpLogger("$ERROR$OutgoingCallISDN: Error ISDN call numb=%s",psAbnt->NumbDst);
        if(IsAutomaticCall(PultNb)) return;
        PutErrInfo2Display(PultNb,"������ ISDN-����������");
        return;
      }
      btE1=GetNextE1(btE1);
      btE1Cnt++;
    }
    while(1);
//SendStr2IpLogger("$DEBUG$OutgoingCallISDN: PultNb=%d, wChan=%d, wOper=%d (Key=%d)",PultNb,NChan,wOper,Key);
    psAbnt->btE1=btE1;
    psAbnt->btOutgoingCnt=1;
    psAbnt->wConnectionPoint=NChan;
    psAbnt->wOperatorChanel=wOper;
    psAbnt->btPultNb=PultNb;
    psAbnt->btLayer=Layer;
    psAbnt->btKey=Key;
    psAbnt->NumbAbon=NumbAbon;
    psAbnt->btLedState=dLedStateOutgoingCall;
    psAbnt->btType=ISDN;
    SetAbonentStateByPtr(psAbnt,Call,0);
    psAbnt->dwTimerEnd=MassTimer[TIMER_OUTGOINGCALL]+xTaskGetTickCount();
    if(IsAutomaticCall(PultNb))
    {
      btSound=0;
      SetAutomaticCallAbonent(psAbnt);
    }
    else btSound=1;
    IndicCmd(PultNb,psAbnt->btLayer,Key,psAbnt->btLedState,btSound,btSound);
    if(!NumbAbon) PutAbntNumb2Display(PultNb,psAbnt);
    SendActiveUnnameAbnt_ATC_ISDN(psAbnt);
    return;
  }
  else
  {
SendStr2IpLogger("$ERROR$OutgoingCallISDN: wCP=0x%02x, psAbnt=%p",NChan,psAbnt);
    if(NChan!=0xFF) DelConnectionPoint(NChan);
    OutgoingNumberTmpTOutClear(PultNb);
    if(IsAutomaticCall(PultNb)) return;
    PutErrInfo2Display(PultNb,"���� ������");
    return;
  }
}

bool RepeatOugoingCall(ABONENT_STATE_TP *psAbnt)
{
  unsigned char btE1,PultNb;
  unsigned short NChan;

  if((GetAbonentStateFromPtr(psAbnt)==Call) && (psAbnt->btOutgoingCnt<NUM_TRUNKS))
  {
    PultNb=psAbnt->btPultNb;
    btE1=GetNextE1(psAbnt->btE1);
    if(btE1!=0xff)
    {
      NChan=GetFreeConnectionPoint();
      if(NChan!=0xff)
      {
        SendCmdOtboiISDN(PRI_CAUSE_CALL_REJECTED,psAbnt->wConnectionPoint,psAbnt->wOperatorChanel);
        psAbnt->btE1=btE1;
        _xOutgoingCallISDN(NChan,psAbnt->wOperatorChanel,btE1,
                           psAbnt->NumbDst,psAbnt->NumbSrs,psAbnt->NameSrs,
                           IsAutomaticCall(PultNb) ? MassM1[PultNb] : 0);
        if(IsKey2Selector(PultNb,psAbnt->btKey,psAbnt->btLayer,d_Abonent2SelectorCurrentPult))
        { ReplaceCP2Selector(PultNb,psAbnt->wConnectionPoint,NChan); }
        psAbnt->wConnectionPoint=NChan;
        psAbnt->dwTimerEnd=MassTimer[TIMER_OUTGOINGCALL]+xTaskGetTickCount();
        psAbnt->btOutgoingCnt++;
        return(true);
      }
      else SendStr2IpLogger("$ERROR$RepeatOugoingCall: CP=0xff");
    }
  }
  return(false);
}

void AnswerOutgoingCallEDSS(unsigned short wNChan,unsigned short wOper)
{
  unsigned char PultNb,btSound;
  ABONENT_STATE_TP *psAbnt;
  
  if(IsCrossCommuteAbnt(wNChan))
  {
    CrossCommuteAnswer(wNChan,wOper);
    return;
  }
//SendStr2IpLogger("$DEBUG$AnswerOutgoingCallEDSS");
  PultNb=FindPultActive(wOper);
  if(PultNb!=0xff)
  {
//SendStr2IpLogger("$DEBUG$AnswerOutgoingCallEDSS: PultNb=%d, wChan=%d, wOper=%d",PultNb,wNChan,wOper);
    psAbnt=GetAbonentStatePtrByFind(PultNb,wNChan,wOper,0xff,0xff,NULL);
    if(psAbnt)
    {
      if(IsAutomaticCallAbonent(psAbnt)) btSound=0;
      else
      {
        DSRS_On(PultNb,wOper);
        btSound=1;
      }
      SetLedColorIfTalk(psAbnt);
      psAbnt->dwTimerEnd=0;
      SetAbonentStateByPtr(psAbnt,Talk,0);
//SendStr2IpLogger("$DEBUG$AnswerOutgoingCallEDSS talk");
      if(IsKey2Selector(PultNb,psAbnt->btKey,psAbnt->btLayer,d_Abonent2SelectorCurrentPult))
      {
//SendStr2IpLogger("$DEBUG$IsdnAbntSelectorAnswer");
        Commute(false,wNChan,wOper); // ������� ��������� ���������� ����� ISDN-��������� � ����������
        IsdnIPPMAbntSelectorAnswer(PultNb,psAbnt->btKey);
        if(IsSelectorWorkState(PultNb)) SetAbnt2SelectorStatus(PultNb,psAbnt->btKey,psAbnt->btLayer);
      }
      else
      {
        IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,btSound,btSound);
        if(IsGroupAutoAnswer4PultFromGroup(PultNb,GetTlfnGroup(psAbnt->NumbAbon))) AddAbnt2FreeHandConf(PultNb,psAbnt);
      }
      SendActiveUnnameAbnt_ATC_ISDN(psAbnt);
      return;
    }
//SendStr2IpLogger("$DEBUG$AnswerOutgoingCallEDSS abonent not found!!!");
    SendCmdOtboiISDN(PRI_CAUSE_INVALID_CALL_REFERENCE,wNChan,wOper);
    return;
  }
  SendCmdOtboiISDN(89,wNChan,wOper);
}

bool _xIncomingCallISDNAnswer(unsigned short wNChan,unsigned short wOper,unsigned char btCommute)
{
  static sCommand sCmd;
  
  sCmd.Source=UserRS485;// �������� ������� RS485
  sCmd.L=7; // ���������� ���� � �������
  sCmd.A1=0;
  sCmd.BufCommand[0]=10; // �������
  sCmd.BufCommand[1]=203; // ���������� ���������� ������� �������� � ������ ������
  sCmd.BufCommand[2]=wNChan >> 8;
  sCmd.BufCommand[3]=wNChan;
  sCmd.BufCommand[4]=wOper >> 8;
  sCmd.BufCommand[5]=wOper;
  sCmd.BufCommand[6]=btCommute;
  if(xQueueSendCmd(&sCmd,100)==errQUEUE_FULL) CountErrSendQueueCommand++;
  else
  {
    if(WaitReply(11,203)) return(true);
    else CountErrEDSS++;
  }
  return(false);
}

void AnswerIncomingCallISDN(unsigned char PultNb,ABONENT_STATE_TP *psAbnt,unsigned char btCommute)
{
  if(_xIncomingCallISDNAnswer(psAbnt->wConnectionPoint,psAbnt->wOperatorChanel,btCommute))
  {
    SetLedColorIfTalk(psAbnt);
    SetAbonentStateByPtr(psAbnt,Talk,dIncomingTalkFlag);
    psAbnt->dwTimerEnd=0;
    if(IsKey2Selector(PultNb,psAbnt->btKey,psAbnt->btLayer,d_Abonent2SelectorCurrentPult))
    {
      if(!IsSelectorWorkState(PultNb)) DSRS_On(PultNb,psAbnt->wOperatorChanel);
      IsdnIPPMAbntSelectorAnswer(PultNb,psAbnt->btKey);
    }
    else
    {
      DSRS_On(PultNb,psAbnt->wOperatorChanel);
      IndicCmd(PultNb,psAbnt->btLayer,psAbnt->btKey,psAbnt->btLedState,1,1);
    }
    if(!psAbnt->NumbAbon || (psAbnt->NumbAbon==0xff)) PutAbntNumb2Display(PultNb,psAbnt);
    SendActiveUnnameAbnt_ATC_ISDN(psAbnt);
    return;
  }
  PutErrInfo2Display(PultNb,"������ ISDN-������!");
}

