#include "..\Sinfo.h"
#include "FunctionsPrototypes.h"
#include "ViewCommandFunc.h"
#include "GlobalData.h"

/////////////////////////////////////////////////////////////////////
void StopWaitReplyUART(void);

unsigned char IsLoadedMKA(void);
unsigned char IsFullLoadMKA(unsigned short wMKA);

unsigned char IsLoadParamCmdMKA(unsigned char btCmd);

/////////////////////////////////////////////////////////////////////
bool CommandStand(sInfoKadr *psInfoS1,unsigned char codeCommand,unsigned char Result)
{
  psInfoS1->L=2;
  psInfoS1->ByteInfoKadr[0]=codeCommand;
  if(Result!=Tranzit)
  {
    psInfoS1->ByteInfoKadr[1]=Result;
    if(xQueueSend(xQueueReply[psInfoS1->Source],psInfoS1,0,0)==errQUEUE_FULL)
    { CountErrQueueReply++; return false; }
  }
  return true;
}

void SendArray2PC(sInfoKadr *psInfoS1,unsigned short *DataBuf,unsigned char btCnt)
{
  unsigned char i,j;
  
  psInfoS1->L=(btCnt<<1)+2;
  psInfoS1->ByteInfoKadr[0]=11;
  psInfoS1->ByteInfoKadr[1]=179;
  for(i=0,j=2;i<btCnt;i++,j+=2)
  {
    psInfoS1->ByteInfoKadr[j]=DataBuf[i];
    psInfoS1->ByteInfoKadr[j+1]=DataBuf[i]>>8;
  }
  if(xQueueSend(xQueueReply[psInfoS1->Source],psInfoS1,0,0)==errQUEUE_FULL) CountErrQueueReply++;
  return;
}

bool vCmdOutputResistance(unsigned char *CommandPort0,
                         sCommand *psViewCommand,sInfoKadr *psInfoS1,unsigned char codeCommand)
{
  unsigned short DanFl;
  unsigned int Addr2Flash;

  if(psViewCommand->BufCommand[1])
  { // ���� ����� �����������
    xQueueSend(xQueueSportX,psViewCommand,0,0);
    return true;
  }
  Addr2Flash=AdrTypeQuit+2*psViewCommand->BufCommand[2];
  DanFl=RdFlash(Addr2Flash); // ������ ��� ������ ����������� ������ �� ����
  if(Err_Flash) DanFl=1; // ���� ������ ��� ������ � ����, �� ������ ������ ����� 600 ��!
  if(psViewCommand->Source==UserLoaderMKA) psViewCommand->BufCommand[3]=DanFl;
  if(psViewCommand->BufCommand[3]>1) psViewCommand->BufCommand[3]=1;
  if(DanFl!=psViewCommand->BufCommand[3]) WrFlash(psViewCommand->BufCommand[3],Addr2Flash);
//SendStr2IpLogger("$DEBUG$vCmdOutputResistance: Chan=%d, btEnOut=%d",psViewCommand->BufCommand[2]+1,psViewCommand->BufCommand[3]);
  CommandPort0[0] = 81; 
  CommandPort0[1] = 7; // ����� �������
  CommandPort0[2] = psViewCommand->BufCommand[2]; // ����� ������
  CommandPort0[3] = psViewCommand->BufCommand[3]; // ����������
  CommandPort0[4] = 0; 
  CommandPort0[5] = 0; 
  CommandPort0[6] = 0; 
  CommandStand(psInfoS1,codeCommand,Tranzit);
  return true;
}

bool vCmdAGCOnOff(unsigned char *CommandPort0,sCommand *psViewCommand,sInfoKadr *psInfoS1,unsigned char codeCommand)
{
  unsigned short DanFl;
  unsigned int Addr2Flash;

  if(!psViewCommand->BufCommand[1]) Addr2Flash=AdrARU;
  else Addr2Flash=AdrARUTP;
  Addr2Flash+=2*psViewCommand->BufCommand[2];
  DanFl=RdFlash(Addr2Flash);
  if(psInfoS1->Source==UserLoaderMKA)
  {
    if(DanFl>2) 
    {
      DanFl = 0;
      WrFlash(DanFl,Addr2Flash);
    }
    psViewCommand->BufCommand[3]=(unsigned char)DanFl; // ���������/����������
  }
  else
  {
    if(psViewCommand->BufCommand[3]>2)
    { // ��������� ������
      CommandStand(psInfoS1,codeCommand,CodeStatusFalse);
      return false;
    }
    if((DanFl!=psViewCommand->BufCommand[3]) || Err_Flash) WrFlash(psViewCommand->BufCommand[3],Addr2Flash);
  }
  if(psViewCommand->BufCommand[1]==1)
  { // ����� �����������
    if(xQueueSend(xQueueSportX,psViewCommand,0,0)==errQUEUE_FULL)
    {
      CountErrQueueReply++;
      return false;
    }
    return true;
  }
  else 
  {
    if(!psViewCommand->BufCommand[3])
    {
      CommandPort0[0] = 27; // ���������� ���
      CommandPort0[1] = 3; // ����� �������
      CommandPort0[2] = psViewCommand->BufCommand[2];
    }
    else
    {
      CommandPort0[0] = 26; // ���������
      CommandPort0[1] = 4; // ����� �������
      CommandPort0[2] = psViewCommand->BufCommand[2];
      CommandPort0[3] = psViewCommand->BufCommand[3]; // ��� ���
    }
    CommandStand(psInfoS1,codeCommand,Tranzit);
    return true;
  }
}

// ������������� (0 - ����, 1-9 �������)
bool vCmdEchoOnOff(unsigned char *CommandPort0,sCommand *psViewCommand,sInfoKadr *psInfoS1,unsigned char codeCommand)
{
  unsigned short DanFl;
  unsigned int Addr2Flash;
  
  Addr2Flash=AdrFiltrEcho+psViewCommand->BufCommand[2]*2;
  DanFl=RdFlash(Addr2Flash);
  if(psInfoS1->Source==UserLoaderMKA)
  {
    if(DanFl>NumbVar) 
    {
      DanFl = 0;
      WrFlash(DanFl,Addr2Flash);
    }
    psViewCommand->BufCommand[3]=(unsigned char)DanFl;
  }
  else
  {
    if(psViewCommand->BufCommand[3]>NumbVar)
    { // ��������� ������
      CommandStand(psInfoS1,codeCommand, CodeStatusFalse);
      return false;
    }
    if((DanFl!=psViewCommand->BufCommand[3]) || Err_Flash) WrFlash(psViewCommand->BufCommand[3],Addr2Flash);
  }
  if(!psViewCommand->BufCommand[3])
  { // ���������� ��������������
    CommandPort0[0]=25;
    CommandPort0[1]=3; // ����� �������
    CommandPort0[2]=psViewCommand->BufCommand[2];
    CommandStand(psInfoS1,codeCommand, Tranzit);
  }
  else
  { // ��������� ��������������
    CommandPort0[0] = 54; // ��������� ��������������
    CommandPort0[1] = 5; // ����� �������
    CommandPort0[2] = psViewCommand->BufCommand[2];
    CommandPort0[3] = psViewCommand->BufCommand[3]; // �������
    CommandPort0[4] = 0; 
    CommandStand(psInfoS1,codeCommand,Tranzit);
  }
  return true;
}

bool vCmdSetAnalPorog(unsigned char *CommandPort0,sCommand *psViewCommand,sInfoKadr *psInfoS1,unsigned char codeCommand)
{
  unsigned short DanFl;
  unsigned int Addr2Flash;

  if(psViewCommand->BufCommand[1])
  { // ���� ����� �����������
    Addr2Flash=AdrAnalPorog+(psViewCommand->BufCommand[2]+LenSport0)*2;
    DanFl=RdFlash(Addr2Flash);
    if(psViewCommand->BufCommand[3]>50)
    {
      DanFl=0;
      WrFlash(DanFl,Addr2Flash);
    }
    else
      if((DanFl!=psViewCommand->BufCommand[3]) || Err_Flash)
      { // ������ ���������� ���� ������ ������ ����
        DanFl=psViewCommand->BufCommand[3];
        WrFlash(DanFl,Addr2Flash);
      }
    if(xQueueSend(xQueueSportX,psViewCommand,0,0)==errQUEUE_FULL)
    { // ������� �����������
      CountErrQueueReply++;
      return false;
    }
    return true;
  }
  else
  {
    Addr2Flash=AdrAnalPorog+psViewCommand->BufCommand[2]*2;
    DanFl=RdFlash(Addr2Flash);
    if(psInfoS1->Source==UserLoaderMKA)
    {
      if(DanFl>50) 
      {
        DanFl=0;
        WrFlash(DanFl, AdrAnalPorog+psViewCommand->BufCommand[2]*2);
      }
      psViewCommand->BufCommand[3]=(unsigned char)DanFl; // ������� (0-50)
    }
    else
    {
      if(psViewCommand->BufCommand[3]>50)
      { // ��������� ������
        CommandStand(psInfoS1,codeCommand,CodeStatusFalse);
        return false;
      }
      if((DanFl!=psViewCommand->BufCommand[3]) || Err_Flash)
      { // ������ ���������� ���� ������ ������ ����
        DanFl=psViewCommand->BufCommand[3];
        WrFlash(DanFl,Addr2Flash);
      } 
    }
    CommandPort0[0] = 48; 
    CommandPort0[1] = 5; // ����� �������
    CommandPort0[2] = psViewCommand->BufCommand[2];
    CommandPort0[3] = psViewCommand->BufCommand[3]; // ������� (0-50)
    CommandPort0[4] = 0;
    CommandStand(psInfoS1,codeCommand,Tranzit);
    return true;
  }
}

bool vCmdSetReturnCoef(unsigned char *CommandPort0,
                       sCommand *psViewCommand,sInfoKadr *psInfoS1,unsigned char codeCommand)
{
  unsigned short DanFl;
  unsigned int Addr2Flash;

  if(psViewCommand->BufCommand[1])
  { // ���� ����� �����������
    Addr2Flash=AdrReturnCoef+(psViewCommand->BufCommand[2]+LenSport0)*2;
    DanFl=RdFlash(Addr2Flash);
    if(psViewCommand->BufCommand[3]>100) 
    {
      psViewCommand->BufCommand[3]=DanFl=0;
      WrFlash(DanFl,Addr2Flash);
    }
    else
    {
      if((DanFl!=psViewCommand->BufCommand[3]) || Err_Flash)
      { // ������ ���������� ���� ������ ������ ����?
        DanFl = psViewCommand->BufCommand[3];
        WrFlash(DanFl,AdrReturnCoef+(psViewCommand->BufCommand[2]+LenSport0)*2);
      }
    }
    if(xQueueSend(xQueueSportX,psViewCommand,0,0)==errQUEUE_FULL)
    { // ������� �����������
      CountErrQueueReply++;
      return false;
    }
    return true;
  }
  else
  {
    Addr2Flash=AdrReturnCoef+psViewCommand->BufCommand[2]*2;
    DanFl=RdFlash(Addr2Flash);
    if(psInfoS1->Source==UserLoaderMKA)
    {
      if(DanFl>100) 
      {
        DanFl=0;
        WrFlash(DanFl,Addr2Flash);
      }
      psViewCommand->BufCommand[3]=(unsigned char)DanFl; // ������� �������� 
    }
    else
    {
      if(psViewCommand->BufCommand[3]>100)
      { // ��������� ������
        CommandStand(psInfoS1,codeCommand,CodeStatusFalse);
        return false;
      }
      else
      {
        if((DanFl!=psViewCommand->BufCommand[3]) || Err_Flash)
        { // ������ ���������� ���� ������ ������ ����
          WrFlash(psViewCommand->BufCommand[3],Addr2Flash);
        }
      }
    }
    ReversA[psViewCommand->BufCommand[2]]=655*psViewCommand->BufCommand[3]; // ������� �������� 0-100
    CommandPort0[0] = 36; 
    CommandPort0[1] = 4;
    CommandPort0[2] = psViewCommand->BufCommand[2];
    CommandPort0[3] = psViewCommand->BufCommand[3]; // ������� �������� 0-100
    CommandStand(psInfoS1,codeCommand,Tranzit);
    return true;
  }
}

/////////////////////////////////////////////////////////////////////
typedef struct
{
  unsigned char CmdSrs;
  unsigned char CmdDst;
  unsigned int OffsetPar;
  unsigned char MinPar,MaxPar,DefPar;
} VCMDSETPAR_TP;

VCMDSETPAR_TP s_vCmdSetPars[]=
{
{21,44,AdrConditionKey,0,CorrectKey,0},
{22,37,AdrKIn,0,CorrectKIn,0},
{23,39,AdrKOut,1,CorrectKOut,1},
{24,42,AdrMinARU,0,CorrectKIn,0},
{25,43,AdrNoise4dB,0,CorrectNoise,0},
{26,40,AdrKoefNoise,0,CorrectKoefNoise,15},
{27,46,AdrCoefCorrF,0,CorrectCorrF,0},
{0,0,0,0,0,0}, // 28 
{0,0,0,0,0,0}, // 29
{30,29,AdrNoiseUp,CorrectNoiseUp,0x7f,CorrectNoiseUp},
{0,0,0,0,0,0}
};

bool vCmdSetParametrs(unsigned char *CommandPort0,
                      sCommand *psViewCommand,sInfoKadr *psInfoS1,unsigned char codeCommand)
{
  unsigned short DanFl;
  unsigned int Addr2Flash;
  VCMDSETPAR_TP *psvCmdSetPars=s_vCmdSetPars+(codeCommand-21);

  if((codeCommand==24) && psViewCommand->BufCommand[1]) Addr2Flash=AdrMinARUTP;
  else Addr2Flash=psvCmdSetPars->OffsetPar;
  Addr2Flash+=2*psViewCommand->BufCommand[2];
  DanFl=RdFlash(Addr2Flash);
  if(psInfoS1->Source==UserLoaderMKA)
  {
    if((DanFl<psvCmdSetPars->MinPar) || (DanFl>psvCmdSetPars->MaxPar))
    {
      DanFl=psvCmdSetPars->MinPar;
      WrFlash(DanFl,Addr2Flash);
    }
    psViewCommand->BufCommand[3]=(unsigned char)DanFl;
  }
  else
  {
    if((psViewCommand->BufCommand[3]<psvCmdSetPars->MinPar) || (psViewCommand->BufCommand[3]>psvCmdSetPars->MaxPar))
    { // ��������� ������
      CommandStand(psInfoS1,codeCommand,CodeStatusFalse);
      return false;
    }
    if((DanFl!=psViewCommand->BufCommand[3]) || Err_Flash) WrFlash(psViewCommand->BufCommand[3],Addr2Flash);
  }
  CommandPort0[0] = psvCmdSetPars->CmdDst; 
  CommandPort0[1] = 4; // ����� �������
  CommandPort0[2] = psViewCommand->BufCommand[2];
  CommandPort0[3] = psViewCommand->BufCommand[3]; // ��� ������ (0-28)
  CommandStand(psInfoS1,codeCommand,Tranzit);
  return true;
}

bool vCmdSetChanelType(unsigned char *CommandPort0,
                       sCommand *psViewCommand,sInfoKadr *psInfoS1,unsigned char codeCommand)
{
  unsigned int TypeCallFlash,TypeCallCmd;

  if(psViewCommand->BufCommand[1])
  { // ����� �����������
    TypeCallFlash=RdFlash(AdrTypeCall+2+(psViewCommand->BufCommand[2]+LenSport0)*4);
    TypeCallFlash=(TypeCallFlash << 16) | RdFlash(AdrTypeCall+(psViewCommand->BufCommand[2]+LenSport0)*4);
    TypeCallCmd=psViewCommand->BufCommand[5];
    TypeCallCmd=(TypeCallCmd << 8) | psViewCommand->BufCommand[4];
    TypeCallCmd=(TypeCallCmd << 8) | psViewCommand->BufCommand[3];
    if((TypeCallFlash!=TypeCallCmd) || Err_Flash)
    { // ������ ���������� ���� ������ ������ ����
      WrFlash((unsigned short)(TypeCallCmd & 0xffff),AdrTypeCall+(psViewCommand->BufCommand[2]+LenSport0)*4);
      WrFlash((unsigned short)(TypeCallCmd >> 16),AdrTypeCall+2+(psViewCommand->BufCommand[2]+LenSport0)*4);
    }
    if(xQueueSend(xQueueSportX,psViewCommand,0,0)==errQUEUE_FULL)
    { // ������� �����������
      CountErrQueueReply++;
      return false;
    }
    return true;
  }
  else
  {
    TypeCallFlash=RdFlash(AdrTypeCall+2+psViewCommand->BufCommand[2]*4);
    TypeCallFlash=(TypeCallFlash << 16) | RdFlash(AdrTypeCall+psViewCommand->BufCommand[2]*4);
    if(psInfoS1->Source==UserLoaderMKA)
    {
      if(TypeCallFlash>0x00FFFFFF) 
      {
        TypeCallFlash=0;
        WrFlash(0, AdrTypeCall+psViewCommand->BufCommand[2]*4);
        WrFlash(0, AdrTypeCall+2+psViewCommand->BufCommand[2]*4);
      }
      psViewCommand->BufCommand[3]=(unsigned char)(TypeCallFlash & 0xff); // ��. ���� ���
      psViewCommand->BufCommand[4]=(unsigned char)(TypeCallFlash >> 8); // ��. ���� ���
      psViewCommand->BufCommand[5]=(unsigned char)(TypeCallFlash >> 16); // ��. ���� ���
    }
    else
    {
      TypeCallCmd=psViewCommand->BufCommand[5];
      TypeCallCmd=(TypeCallCmd << 8) | psViewCommand->BufCommand[4];
      TypeCallCmd=(TypeCallCmd << 8) | psViewCommand->BufCommand[3];
      if((TypeCallFlash!=TypeCallCmd) || Err_Flash)
      {
        WrFlash((unsigned short)(TypeCallCmd & 0xffff),AdrTypeCall+psViewCommand->BufCommand[2]*4);
        WrFlash((unsigned short)(TypeCallCmd >> 16),AdrTypeCall+2+psViewCommand->BufCommand[2]*4);
      }
    }
    CommandPort0[0] = 82; 
    CommandPort0[1] = 7; // ����� �������
    CommandPort0[2] = psViewCommand->BufCommand[2];
    CommandPort0[3] = psViewCommand->BufCommand[3]; // ��. ���� ���
    CommandPort0[4] = psViewCommand->BufCommand[4]; // ��. ���� ���
    CommandPort0[5] = psViewCommand->BufCommand[5]; // ��. ���� ���
    CommandPort0[6] = 0; 
    CommandStand(psInfoS1,codeCommand,Tranzit);
    return(true);
  }
}

bool vCmdOutInductor(unsigned char *CommandPort0,
                     sInfoKadr *psInfoS1,unsigned char codeCommand,unsigned char NChan)
{ 
  unsigned short DanFl;
  
  CommandPort0[0] = 83; 
  CommandPort0[1] = 7; // ����� �������
  CommandPort0[2] = NChan;
  if(MassTimer[0]) CommandPort0[3] = MassTimer[0]/5000; // ���������� ������� ���������
  else CommandPort0[3]=1;
  DanFl=RdFlash(AdrFr25);  // ������ ������� ��������� 25/50 �� �� flash 
  if(DanFl>1)
  {
    DanFl = 0;
    WrFlash(DanFl,AdrFr25);
  }
  CommandPort0[4] = DanFl; // ������� ��������� 25/50 ��
  CommandPort0[5] = 0; 
  CommandPort0[6] = 0; 
  CommandStand(psInfoS1,codeCommand,Tranzit);
  return(true);
}

void SendCommand2Uart(unsigned char *CommandPort0,
                      sInfoKadr *psInfoS1,unsigned char *pxContinOutCommand,unsigned char NChan,
                      unsigned char codeCmdMU,unsigned char btCmdSource)
{
  char i;
  unsigned char btCode,codeCmd,CountByte,
                btIndMKA,btStateMKA,btCmdSendEn;
  unsigned short DanFl,wTOut;
  bool pdRes;
  static unsigned char pInBuf[d_UARTRcvBufSz];
  
//  while(uxQueueMessagesWaiting(xRxedChars)) xQueueReceive(xRxedChars,pInBuf,0); // ����������� ������� ������ (�� ������ ������)
  codeCmd=CommandPort0[0]; // ���  ���������� �������
  if(CommandPort0[2]==0xff) btIndMKA=0;
  else btIndMKA=CommandPort0[2] >> 2;
  btStateMKA=(TestBoard >> (btIndMKA<<1)) & 0x03;
  btCmdSendEn=0;
  if(IsLoadedMKA())
  {
    if(btCmdSource==UserLoaderMKA)
    {
      if((codeCmd==164) || (codeCmd==163) || (codeCmd==49)) btCmdSendEn=1;
      else
      {
        if(IsLoadParamCmdMKA(codeCmdMU) && (btStateMKA==2)) btCmdSendEn=1;
      }
    }
  }
  else 
  {
    if(btStateMKA==2) btCmdSendEn=1;
  }
  if(!btCmdSendEn)
  {
      CreateErrInfo(codeCmdMU,CodeStatusFalse,psInfoS1);
      return;
  }
//if(codeCmdMU!=15) // !!! ��������� �������� ������ ��� �� ������� 15
{
  vPortEnableSwitchTask(false);
  while(xQueueReceive(xRxedChars,NULL,0)==pdPASS); // ����������� ������� ������ (�� ������ ������)
  pdRes=xUARTPutCmd(CommandPort0);
  vPortEnableSwitchTask(true);
}
//else pdRes=pdPASS;  // !!! ��������� �������� ������ ��� �� ������� 15
  if(!pdRes)
  {
SendStr2IpLogger("$DEBUG$xUARTPutCmd: ERROR!");
    CountErrQueueUart++; // ������� �� Uart �� ��������
    CreateErrInfo(codeCmdMU,CodeStatusFalse,psInfoS1);
    return;
  }
  { // �������� ������
//SendStr2IpLogger("$DEBUG$SendCommand2Uart: wait answer...");
    if(codeCmd==28) wTOut=3000; // ���������� ������� ������� ������������� �������!
    else wTOut=200;
    if(xQueueReceive(xRxedChars,pInBuf,wTOut)==pdTRUE)
    { // ����� ������
      if(pInBuf[0]==codeCmd)
      { //����� �� ���������� �������
        CountByte=pInBuf[1]; // ���������� ���� ������
        if((CountByte==3) && (pInBuf[2]==codeCmd)) pxContinOutCommand[0]=CodeStatusTrue;
        else
        {
          if((CountByte==3) && ((pInBuf[2]==-codeCmd) || (pInBuf[2]==NoCommand)))
          {
            CreateErrInfo(codeCmdMU,CodeStatusFalse,psInfoS1);
            return;
          }
          else 
          {
            if((codeCmd==28) && (psInfoS1->ByteInfoKadr[3]==1))
            { // ��� �������� ��������� = 1
              for(i=0;i<CountByte-3;i=i+2)
              {
                DanFl=pInBuf[3+i];
                DanFl=(DanFl << 8) | pInBuf[3+i+1];
                WrFlash(DanFl,AdrBufEcho+NChan*LengthEcho+i);
              }
              memcpy(pxContinOutCommand,pInBuf+2,CountByte-2);
            }
            else
            { 
              if(codeCmd==17) memcpy(pxContinOutCommand,pInBuf+3,CountByte-3); // ���
              else
              {
                if(codeCmd==53)
                {
                  LockPrintf();
                  sprintf((char *)pxContinOutCommand,"%2u.%2u",pInBuf[3],pInBuf[4]); // ������ ��
                  UnlockPrintf();
                }
                else
                {
                  memcpy(pxContinOutCommand,pInBuf+2,CountByte-2);
/*if(psInfoS1->ByteInfoKadr[0]==51)
{ // ������� ��������� ������ ������� �� ���
  unsigned short wLvl=pxContinOutCommand[0]*256+pxContinOutCommand[1];

SendStr2IpLogger("$GET_LVL$GetLvlADC: chan=%d,wLvl=%d",psInfoS1->ByteInfoKadr[2],wLvl);

51: begin  // ������� �������� �������� ������ ����������� ������
  if Mode=30 then begin
    if IsCurrentNumber(Ord(Info[2])*256 + Ord(Info[3])) then begin
     rrr:= Ord(Info[5])*256+Ord(Info[6]);
     if rrr>0 then rrr:= 20.* Log10(rrr/4700. ) else rrr:= -75.;
     rrr:= rrr+LevelIndicatorCorrection; // ��������� �������� � �� �� INI-�����
     rrr:= rrr+(LevelIn1.Position-LevelIn1.Max)*1.5+LevelInMax;
     rrr1:=rrr;
     if (OldMiddle-rrr) >= deltai then begin  // ������� ���������
      rrr:=OldMiddle-deltai;
      if deltai<10.0 then deltai:=deltai+0.5;
     end else deltai:=0.0;
     OldMiddle := rrr;
     ProgressBar1.Position:= round(rrr*10);
     ProgressBar2.Position:= round(rrr1*10);
     Label9.Caption:= Format('%2.1f ��',[rrr]);
     Label15.Caption:= Format('%2.1f ��',[rrr1]);
     end;
    end;
  end;
}*/
                }
              }
            }
          }
        }
        if(xQueueSend(xQueueReply[psInfoS1->Source],psInfoS1,0,0)==errQUEUE_FULL) CountErrQueueReply++;
      }
      else
      {
        CountErrCommand++;
        CreateErrInfo(codeCmdMU,ErrInReply,psInfoS1);
      }
    }
    else
    { // ����� �� ������ �� ���������� ����� - ����� ��������������� ����� � �������
      StopWaitReplyUART();
      CountErrNoReplyUart++;
      CreateErrInfo(codeCmdMU,ErrNoReplyUart,psInfoS1);
SendStr2IpLogger("$ERROR$SendCommand2Uart: answer timeout for block #%d, codeCmdMU=%d, codeCmd=%d",btIndMKA,codeCmdMU,codeCmd);
    }
  }
//SendStr2IpLogger("$DEBUG$SendCommand2Uart: end!");
}

