#include "Sinfo.h"
#include "GlobalData.h"

typedef struct
{
  unsigned int dwIP;
  unsigned char btMAC[6];
  unsigned short wCmdP,
                 wSndP;
  unsigned char btStudio,
                btTOutS,
                btSate,
                btReg;
  unsigned int dwTOut;
} INFO4UK_TP;

/////////////////////////////////////////////////////////////////////
const INFO4UK_TP c_Info4UK=
{
  0x00000000, //0x2001a8c0,
  {0,0,0,0,0,0},
  4000,
  5002,
  51,
  5,
  0,
  0,
  0
};

static INFO4UK_TP sInfo4UK;

/////////////////////////////////////////////////////////////////////
void SetStudioNumb(unsigned short wPar)
{ sInfo4UK.btStudio=wPar; }

void SetUPLvlIpAddr(unsigned int dwIP)
{ sInfo4UK.dwIP=dwIP; }

void SetUPLvlPortCtrl(unsigned short wPort)
{
  if(wPort) sInfo4UK.wCmdP=wPort;
}

void SetUPLvlPortSound(unsigned short wPort)
{
  if(wPort) sInfo4UK.wSndP=wPort;
}

bool GetMACAddr4UK(unsigned int dwIP,unsigned char *MACAddr)
{
  bool bRes=false;
  vPortEnableSwitchTask(false);
  if(sInfo4UK.btReg==2)
  {
    if(dwIP==sInfo4UK.dwIP)
    {
      memcpy(MACAddr,sInfo4UK.btMAC,6);
      bRes=true;
    }
  }
  vPortEnableSwitchTask(true);
  return bRes;
}

void RegistrationDSC2UK_NoAnsw(LIST_UK_IPPM_TP *pItem)
{
//SendStr2IpLogger("$DEBUG$RegistrationDSC2UK_NoAnsw: Numb=%d",pItem->Numb);
  sInfo4UK.btReg=0;
  sInfo4UK.dwTOut=xTaskGetTickCount()+(1000*sInfo4UK.btTOutS);
}

void RegistrationDSC2UK_Answ(LIST_UK_IPPM_TP *pItem,unsigned char *pbtAnsw)
{
//SendStr2IpLogger("$DEBUG$RegistrationDSC2UK_Answ: Numb=%d, btRes=%d",pItem->Numb,pbtAnsw[0]);
  if(pbtAnsw[0]) return;    // !=0 - ������ �����������
  sInfo4UK.btSate=0;
  sInfo4UK.btTOutS=pbtAnsw[1];
  memcpy(sInfo4UK.btMAC,pbtAnsw+2,6);
  sInfo4UK.dwTOut=xTaskGetTickCount()+(1000*sInfo4UK.btTOutS);
  sInfo4UK.btReg=2;
}

void RegistrationDSC2UK(void)
{
  LIST_UK_IPPM_TP *pItem;

  if(sInfo4UK.dwIP==0) return;
  pItem=ListUK_IPPM_AddItem(0,NULL);
  if(pItem)
  {
    sInfo4UK.btReg=1;
    pItem->Numb=sInfo4UK.btStudio;
    pItem->sIK.A1=sInfo4UK.dwIP;
    pItem->sIK.PORT1=sInfo4UK.wCmdP;
    pItem->sIK.L=28;
    pItem->sIK.ByteInfoKadr[0]=0xeb;
    pItem->sIK.ByteInfoKadr[1]=pItem->Numb;
    pItem->sIK.ByteInfoKadr[2]=0x0;
    pItem->sIK.ByteInfoKadr[3]=pItem->wId >> 8;
    pItem->sIK.ByteInfoKadr[4]=pItem->wId & 0xff;
    pItem->sIK.ByteInfoKadr[5]=1;   // ��� �������
    pItem->sIK.ByteInfoKadr[6]=0;   // ���������� DSC
    pItem->sIK.ByteInfoKadr[7]=pItem->Numb;
    pItem->sIK.ByteInfoKadr[8]=1;
    pItem->sIK.ByteInfoKadr[9]=1;
    GetMAC_DSC(0,pItem->sIK.ByteInfoKadr+10);
    strcpy((char *)(pItem->sIK.ByteInfoKadr+16),"REGISTRATION");
    pItem->pFunc_UK_IPPMAnswer=RegistrationDSC2UK_Answ;
    pItem->pFunc_UK_IPPMNoAnswer=RegistrationDSC2UK_NoAnsw;
SendStr2IpLogger("$DEBUG$RegistrationDSC2UK: Numb=%d, CmdId=%d",pItem->Numb,pItem->wId);
  }
}

void SendAnswer4UK(unsigned int dwIP,unsigned short wPort,
                   unsigned short wCmdAnsw,unsigned char btCmd,
                   unsigned char btRes,char *strInfo)
{
  static section ("bigdata") sInfoKadr sIK;

  sIK.Source=UserUDP;
  sIK.A1=dwIP;
  sIK.PORT1=wPort;
  sIK.L=7+strlen(strInfo);
  sIK.ByteInfoKadr[0]=0xea;
  sIK.ByteInfoKadr[1]=sInfo4UK.btStudio;
  sIK.ByteInfoKadr[2]=0x0;
  sIK.ByteInfoKadr[3]=wCmdAnsw >> 8;
  sIK.ByteInfoKadr[4]=wCmdAnsw & 0xff;
  sIK.ByteInfoKadr[5]=btCmd;
  sIK.ByteInfoKadr[6]=btRes;
  strcpy((char *)(sIK.ByteInfoKadr+7),strInfo);
  SendAnswUK_IPPMCmd(0,sInfo4UK.btStudio,wCmdAnsw,&sIK);
}

void UK_DSC_Event(sCommand *psCmd)
{
  unsigned short wCmdAnsw;
  unsigned char Numb,*pUdpBuf=psCmd->BufCommand;
  unsigned int dwIP;

  wCmdAnsw=pUdpBuf[3]; wCmdAnsw=(wCmdAnsw << 8) | pUdpBuf[4];
  Numb=pUdpBuf[2];
  if(Numb!=sInfo4UK.btStudio) return;
  if(pUdpBuf[0]==0xed)
  { // ������� �� UK � DSC
//SendStr2IpLogger("$DEBUG$UK_DSC_Event Cmd_ED: Numb=%d, Cmd=%d, btReg=%d",Numb,pUdpBuf[5],sInfo4UK.btReg);
    if(sInfo4UK.btReg==2)
    { // DSC ��������������� �� ��
      if(RepeatCmdUK_IPPM(0,Numb,wCmdAnsw)) return;
      switch(pUdpBuf[5])
      {
        case 2: // GET_STATE
//SendStr2IpLogger("$DEBUG$UK_DSC_Event Cmd_GET_STATE: Numb=%d, dwIP=%08x, btState=%d",Numb,psCmd->A1,sInfo4UK.btSate);
          SendAnswer4UK(psCmd->A1,psCmd->PORT1,wCmdAnsw,2,sInfo4UK.btSate,"");
          sInfo4UK.dwTOut=xTaskGetTickCount()+(1000*sInfo4UK.btTOutS);
          break;
        case 3: // CONNECT
          sInfo4UK.btSate=1;
          memcpy(&dwIP,pUdpBuf+6,4);
SendStr2IpLogger("$DEBUG$UK_DSC_Event Cmd_CONNECT: Numb=%d, dwIP=%08x",Numb,dwIP);
          CreateUKRTPMulticast(dwIP);
          StopAudio4UK();
          StartAudioFromUK();
//StartAudioFromIPPM(2); //!!! TEST !!!
          SendAnswer4UK(psCmd->A1,psCmd->PORT1,wCmdAnsw,3,0,"ANSWER_ON_CONNECT");
          break;
        case 4: // CONNECT_PRIVATE
          sInfo4UK.btSate=3;
          SendAnswer4UK(psCmd->A1,psCmd->PORT1,wCmdAnsw,4,0,"ANSWER_ON_CONNECT_PRIVATE");
          CreateUKRTPUnicast(sInfo4UK.dwIP);
          StartAudio4UK();
          StartAudioFromUK();
          break;
        case 5: // DISCONNECT
          sInfo4UK.btSate=0;
          SendAnswer4UK(psCmd->A1,psCmd->PORT1,wCmdAnsw,5,0,"ANSWER_ON_DISCONNECT");
          StopAudio4UK();
          StopAudioFromUK();
//StopAudioFromIPPM(2); //!!! TEST !!!
          CloseUKRTP();
          break;
        default: break;
      }
    }
  }
  else
  {
    if(pUdpBuf[0]==0xec)
    { // ����� �� UK �� ������� DSC
//if(pUdpBuf[5]!=2) SendStr2IpLogger("$DEBUG$UK_DSC_Event Cmd_EC: Numb=%d, Cmd=%d",Numb,pUdpBuf[5]);
      if(!UK_IPPM_RcvAnsw(0,wCmdAnsw,pUdpBuf+6)) return;
    }
  }
}

void InitCall_UK(void)
{ // ������ ������������ ��� ������ � ��
  vPortEnableSwitchTask(false);
  if(sInfo4UK.btReg==2)
  {
    StopAudio4UK();
    StopAudioFromUK();
//StopAudioFromIPPM(2); //!!! TEST !!!
    CloseUKRTP();
    ClearQueueCmdUK_IPPM(0,sInfo4UK.btStudio);
  }
  sInfo4UK=c_Info4UK;
  sInfo4UK.dwTOut=xTaskGetTickCount()+(1000*sInfo4UK.btTOutS);
  vPortEnableSwitchTask(true);
}

void UK_IRPTOut(void)
{
  if(sInfo4UK.btReg==1) return;
  if(IsCurrentTickCountGT(sInfo4UK.dwTOut))
  {
    if(sInfo4UK.btReg==0) RegistrationDSC2UK();
    else
    {
      vPortEnableSwitchTask(false);
      StopAudio4UK();
      StopAudioFromUK();
//StopAudioFromIPPM(2); //!!! TEST !!!
      CloseUKRTP();
      ClearQueueCmdUK_IPPM(0,sInfo4UK.btStudio);
      sInfo4UK.btReg=0;
      sInfo4UK.btTOutS=5;
      sInfo4UK.dwTOut=xTaskGetTickCount()+(1000*sInfo4UK.btTOutS);
      vPortEnableSwitchTask(true);
    }
  }
}

