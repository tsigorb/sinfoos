#include "Sinfo.h"
#include "GlobalData.h" 

//*******************************************************************
xQueueHandle xQueueReply[QueueReplyCnt]; // ������� ��  �������
xQueueHandle xQueueCommand; // �� ��������� ������
xQueueHandle xQueueTFTP;
xQueueHandle xQueueSportX;
xQueueHandle xQueueLogger;
xQueueHandle xQueueVCS;

/////////////////////////////////////////////////////////////////////
// �������� �������� ��� �����
void CreateQueueForTask(void)
{
  short i;
  unsigned portBASE_TYPE MasQueueReplySz[QueueReplyCnt]=
{
  20,                // UserIPPM
  20,                // UserUDP
  20,                // UserViewCmd
  1,                 // UserVCS
/*  1,                 // UserMU2
  1,                 // UserNU3
  1,                 // UserNU4
  1,                 // UserNU5*/
};

  for(i=0;i<QueueReplyCnt;i++)
  {
    xQueueReply[i]=xQueueCreate(MasQueueReplySz[i],sizeof(sInfoKadr));
    if(!xQueueReply[i]) BlockWatchDog();
  }
  xQueueCommand=xQueueCreate(20,sizeof(sCommand));
  if(!xQueueCommand) BlockWatchDog();
  xQueueTFTP=xQueueCreate(20,sizeof(sCommand));
  if(!xQueueTFTP) BlockWatchDog();
  xQueueSportX=xQueueCreate(20,sizeof(sCommand));
  if(!xQueueSportX) BlockWatchDog();
  xQueueVCS=xQueueCreate(3,sizeof(sCommand));
  if(!xQueueVCS) BlockWatchDog();

  xQueueLogger=xQueueCreate(10,sizeof(sInfoKadr));
  if(!xQueueLogger) BlockWatchDog();
}

