#include "Sinfo.h"
#include "GlobalData.h"

/////////////////////////////////////////////////////////////////////
bool vCmdReadSoftVersionMU(unsigned char *CommandPort0,sInfoKadr *psInfoS1);
void vCmdReadDataFromMU(sCommand *psViewCommand,sInfoKadr *psInfoS1);
void vCmdWriteData2MU(sCommand *psViewCommand,sInfoKadr *psInfoS1);

void SetNeedIpReinit(void);

void IPPM_DSC_Event(sCommand *psCmd);
void IPPM_IRPTOut(void);

void UK_DSC_Event(sCommand *psCmd);
void UK_IRPTOut(void);

/////////////////////////////////////////////////////////////////////
bool CommandStand(sInfoKadr *psInfoS1,unsigned char codeCommand,unsigned char Result)
{
  psInfoS1->L=2;
  psInfoS1->ByteInfoKadr[0]=codeCommand;
  psInfoS1->ByteInfoKadr[1]=Result;
  xQueueSend(xQueueReply[psInfoS1->Source],psInfoS1,10,0);
  return true;
}

// ������������ ��������� � �������
void CreateErrInfo(unsigned char codeCmd, unsigned char CodeErr, sInfoKadr *pxInfoS)
{
  pxInfoS->L=2;
  pxInfoS->ByteInfoKadr[0]=codeCmd;
  pxInfoS->ByteInfoKadr[1]=CodeErr;
  xQueueSend(xQueueReply[(pxInfoS->Source)],pxInfoS,10,0);
}

unsigned int GetSN(void)
{
  unsigned int dwSN;

  dwSN=RdFlash(AdrSN+2);
  dwSN=(dwSN<<16) | RdFlash(AdrSN);
  return dwSN;
}

void WriteSN_MU(unsigned char year, unsigned char mon, unsigned char day)
{ // ��������� ��������� ������ ������ ����������
  unsigned int data;

  if(GetSN()==0xFFFFFFFF)
  {
    data=ReadClock();
    WrFlash(AdrSN,(unsigned short)data);
    WrFlash(AdrSN+2,(unsigned short)(data >> 16));
  }
}

unsigned short GetStudioNumber(void)
{ // !!! ������ ������ ���������� � 201
  return 201;
}

// ��������� ������, ����� �� �������
void vViewCommandTask(void *pvParameters)
{
  unsigned char codeCmd;
  unsigned short i,DanFl;
  struct tm time1,*ptime;
  time_t tt;

  static sCommand sCmd;
  static sInfoKadr sInfoS1;

  WaitWhileReadDSCCfg();
  vTaskDelay(100);
  while(1)
  {
    if(xQueueReceive(xQueueCommand,&sCmd,0)) 
    {
      sInfoS1.Source=sCmd.Source;
      sInfoS1.A1=sCmd.A1;
      sInfoS1.PORT1=sCmd.PORT1;
      codeCmd=sCmd.BufCommand[0]; // ��� �������
      switch(codeCmd)
      {
        case 60: // ������� ������ ������ �� � ������ ������ ��
          vCmdReadSoftVersionMU(NULL,&sInfoS1);
          break;
        case 70: // ������� ������ � ��������� ��������
          break;
        case 72: // ������� ������ ������ ��
          vCmdReadDataFromMU(&sCmd,&sInfoS1);
          break;
        case 74: // ������� ������ ������ � ������ ��
          vCmdWriteData2MU(&sCmd,&sInfoS1);
          break;
        case 80: // �����  ��
          BlockWatchDog();
          break;
        case 87: // ������� ��������� ����� ��������� �������
          memset(&time1,0xff,sizeof(struct tm));
          time1.tm_year=(int)(sCmd.BufCommand[1]+100); // ��� 2006-1900;
          time1.tm_mon=(int)sCmd.BufCommand[2]-1; // ����� 
          time1.tm_mday=(int)sCmd.BufCommand[3]; // ����
          time1.tm_hour=(int)sCmd.BufCommand[4]; // ���
          time1.tm_min=(int)sCmd.BufCommand[5]; // ������
          time1.tm_sec=(int)sCmd.BufCommand[6]; // �������
          tt=mktime(&time1);
          SetClock(tt);
          CommandStand(&sInfoS1,codeCmd,CodeStatusTrue);
          WriteSN_MU(sCmd.BufCommand[1],sCmd.BufCommand[2],sCmd.BufCommand[3]);
          break;
        case 88: // ������� ������ ����� ��������� �������
          tt=ReadClock();
          ptime=gmtime(&tt);
          sInfoS1.L=8;
          sInfoS1.ByteInfoKadr[0]=89;
          StoreDtTm2Buf(sInfoS1.ByteInfoKadr+1,ptime);
          xQueueSend(xQueueReply[sInfoS1.Source],&sInfoS1,10,0);
          break;
        case 96:
//          if(xQueueSend(xQueueSportX,&sCmd,100)==errQUEUE_FULL) CountErrQueueReply++;
          break;
        case 115:
//          SetNeedIpReinit();
          break;
        case 127:
          CommandStand(&sInfoS1,codeCmd,CodeStatusTrue);
          vTaskDelay(50);
          if(sCmd.L==1) ReadDSCCfg();
          else
          {
            if(sCmd.L>=d_MaxCmdBufSz) sCmd.L=d_MaxCmdBufSz-1;
            sCmd.BufCommand[sCmd.L]=0;
            ReadDSCCfgFromBuf((char *)sCmd.BufCommand+1);
          }
          break;
        case 0xea:
        case 0xeb:
          // ������/������� IPPM ���/�� DSC
          IPPM_DSC_Event(&sCmd);
          break;
        case 0xec:
        case 0xed:
          // ������/������� UK ���/�� DSC
          UK_DSC_Event(&sCmd);
          break;
        case 255:
          // ��������� ����� �� DspCom - ������ �� ��������!
          break;
        default:
          CreateErrInfo(codeCmd,ErrNoCommand,&sInfoS1);
          break;
      }
    }
    UK_IRPTOut();
    ListUK_IPPM_Process(0);

    IPPM_IRPTOut();
    ListUK_IPPM_Process(1);

//    SetWDState(d_WDSTATE_ViewCmd);
    vTaskDelay(2);
  }
}

