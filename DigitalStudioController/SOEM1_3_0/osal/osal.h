/******************************************************************************
 *                *          ***                    ***
 *              ***          ***                    ***
 * ***  ****  **********     ***        *****       ***  ****          *****
 * *********  **********     ***      *********     ************     *********
 * ****         ***          ***              ***   ***       ****   ***
 * ***          ***  ******  ***      ***********   ***        ****   *****
 * ***          ***  ******  ***    *************   ***        ****      *****
 * ***          ****         ****   ***       ***   ***       ****          ***
 * ***           *******      ***** **************  *************    *********
 * ***             *****        ***   *******   **  **  ******         *****
 *                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
 *
 * http://www.rt-labs.com
 * Copyright (C) 2009. rt-labs AB, Sweden. All rights reserved.
 *------------------------------------------------------------------------------
 * $Id: osal.h 452 2013-02-26 21:02:58Z smf.arthur $
 *------------------------------------------------------------------------------
 */

#ifndef _osal_
#define _osal_

#include "Sinfo.h"
#include "GlobalData.h"

#define CFG_TICKS_PER_SECOND   8000

/*
//General types
typedef char                int8_t;
typedef short               int16_t;
//typedef int                 int32_t;
typedef unsigned char       uint8_t;
typedef unsigned short      uint16_t;
//typedef unsigned int        uint32_t;

typedef int8_t              int8;
typedef int16_t             int16;
typedef int32_t             int32;
typedef uint8_t             uint8;
typedef uint16_t            uint16;
typedef uint32_t            uint32;
*/
typedef long long           int64_t;
typedef unsigned long long  uint64_t;
typedef int64_t             int64;
typedef uint64_t            uint64;
typedef float               float32;
//typedef double              float64;

typedef uint8_t             boolean;
#define TRUE                1
#define FALSE               0

typedef long int            __time_t;
typedef unsigned int        tick_t;

struct timeval
{
  __time_t tv_sec;  // Seconds
  long int tv_usec; // Microseconds
};

struct timezone
{
  int tz_minuteswest; // minutes W of Greenwich
  int tz_dsttime;     // type of dst correction
};

/*#ifndef PACKED
  #define PACKED_BEGIN    #pragma (push)
  #define PACKED_END      #pragma (pop)
#else*/
  #define PACKED_BEGIN
  #define PACKED  __attribute__((__packed__))
  #define PACKED_END
//#endif


typedef struct
{
    uint32 sec;     /*< Seconds elapsed since the Epoch (Jan 1, 1970) */
    uint32 usec;    /*< Microseconds elapsed since last second boundary */
} ec_timet;

typedef struct osal_timer
{
  ec_timet stop_time;
} osal_timert;

int gettimeofday(struct timeval *tv, struct timezone *tz);
void osal_timer_start(osal_timert *self, uint32 timeout_us);
boolean osal_timer_is_expired(osal_timert *self);
int osal_usleep(uint32 usec);
ec_timet osal_current_time(void);

#endif
