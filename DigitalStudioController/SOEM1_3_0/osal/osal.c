/******************************************************************************
 *                *          ***                    ***
 *              ***          ***                    ***
 * ***  ****  **********     ***        *****       ***  ****          *****
 * *********  **********     ***      *********     ************     *********
 * ****         ***          ***              ***   ***       ****   ***
 * ***          ***  ******  ***      ***********   ***        ****   *****
 * ***          ***  ******  ***    *************   ***        ****      *****
 * ***          ****         ****   ***       ***   ***       ****          ***
 * ***           *******      ***** **************  *************    *********
 * ***             *****        ***   *******   **  **  ******         *****
 *                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
 *
 * http://www.rt-labs.com
 * Copyright(C) 2009. rt-labs AB, Sweden. All rights reserved.
 *------------------------------------------------------------------------------
 * $Id: osal.c 452 2013-02-26 21:02:58Z smf.arthur $
 *------------------------------------------------------------------------------
 */

#include "osal.h"
#include <time.h>

#define  timercmp(a, b, CMP)                                \
 (((a)->tv_sec ==(b)->tv_sec) ?                           \
  ((a)->tv_usec CMP(b)->tv_usec) :                        \
  ((a)->tv_sec CMP(b)->tv_sec))

#define  timeradd(a, b, result)                             \
  do {                                                      \
   (result)->tv_sec=(a)->tv_sec +(b)->tv_sec;           \
   (result)->tv_usec=(a)->tv_usec +(b)->tv_usec;        \
    if((result)->tv_usec >= 1000000)                       \
    {                                                       \
       ++(result)->tv_sec;                                  \
      (result)->tv_usec -= 1000000;                        \
    }                                                       \
  } while(0)

#define  timersub(a, b, result)                             \
  do {                                                      \
   (result)->tv_sec=(a)->tv_sec -(b)->tv_sec;           \
   (result)->tv_usec=(a)->tv_usec -(b)->tv_usec;        \
    if((result)->tv_usec < 0) {                            \
      --(result)->tv_sec;                                   \
     (result)->tv_usec += 1000000;                         \
    }                                                       \
  } while(0)

#define USECS_PER_SEC   1000000
#define USECS_PER_TICK (USECS_PER_SEC / CFG_TICKS_PER_SECOND)

/* Workaround for rt-labs defect 776.
 * Default implementation of udelay() didn't work correctly when tick was
 * shorter than one millisecond.
 */
void tick_delay(tick_t tiks)
{
  //!!! �������� �� ��������� ����� ����� (1 ���==125 us)
}

void udelay(uint32 us)
{
  tick_t ticks=(us / USECS_PER_TICK) + 1;
  tick_delay(ticks);
}

int osal_usleep(uint32 usec)
{
  udelay(usec);
  return 0;
}

int gettimeofday(struct timeval *tv, struct timezone *tz)
{
  if(tv)
  {
    portTickType t=xTaskGetTickCount();
    tv->tv_sec =(long)(t / 1000);
    tv->tv_usec=(long)(t % 1000) * 1000;        
  }
  return 0;
}

int osal_gettimeofday(struct timeval *tv, struct timezone *tz)
{
   return gettimeofday(tv, tz);
}

ec_timet osal_current_time(void)
{
   struct timeval current_time;
   ec_timet return_value;

   gettimeofday(&current_time, 0);
   return_value.sec=current_time.tv_sec;
   return_value.usec=current_time.tv_usec;
   return return_value;
}

void osal_timer_start(osal_timert *self,uint32 timeout_usec)
{
   struct timeval start_time;
   struct timeval timeout;
   struct timeval stop_time;

   gettimeofday(&start_time, 0);
   timeout.tv_sec=timeout_usec / USECS_PER_SEC;
   timeout.tv_usec=timeout_usec % USECS_PER_SEC;
   timeradd(&start_time, &timeout, &stop_time);

   self->stop_time.sec=stop_time.tv_sec;
   self->stop_time.usec=stop_time.tv_usec;
}

boolean osal_timer_is_expired(osal_timert * self)
{
   struct timeval current_time;
   struct timeval stop_time;
   int is_not_yet_expired;

   gettimeofday(&current_time, 0);
   stop_time.tv_sec=self->stop_time.sec;
   stop_time.tv_usec=self->stop_time.usec;
   is_not_yet_expired=timercmp(&current_time, &stop_time, <);
   return(is_not_yet_expired == false);
}

