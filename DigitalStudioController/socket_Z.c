#ifdef LOADER_BF533
  #include "LoaderBF533.h"
  #include "..\FreeRTOS/FreeRTOS.h"
  #include "..\FreeRTOS/queue.h"
  #include "..\FreeRTOS/task.h"

  #include "spi_Z.h"
  #include "socket_Z.h"
  #include "Led.h"
#else
  #include "Sinfo.h"
#endif

/////////////////////////////////////////////////////////////////////
extern unsigned int g_dwStatusLedTOut;

xSemaphoreHandle s_WiznetLock=NULL;

// ��� 2-� W6100
unsigned char g_SockSendBusy[_WIZCHIP_SOCK_NUM_*2];
unsigned int g_dwSockTOut[_WIZCHIP_SOCK_NUM_*2];

/////////////////////////////////////////////////////////////////////
void WaitReadyW6100(void)
{
  while(getCIDR()!=0x6100)
  {
    g_dwStatusLedTOut=d_LedTOutErr;
    ResetWatchDog(true);
  }
  g_dwStatusLedTOut=d_LedTOutOk;
}

void initWiznet(unsigned short wCS,uint8 *pbtSHAR,uint8 *pbtGAR,uint8 *pbtMSR,uint8 *pbtSIPR)
{
    if(!s_WiznetLock) vSemaphoreCreateBinary(s_WiznetLock);
    xSemaphoreTake(s_WiznetLock);
    SetIpCS(wCS);
    // !!! SETTING PHY MODE !!!
    if(!wizphy_getphylink())
    {
      wiz_PhyConf phyconf;

      wizphy_reset();
      xSemaphoreGive(s_WiznetLock);
      vTaskDelay(200);
      xSemaphoreTake(s_WiznetLock);
      SetIpCS(wCS);
      PHYUNLOCK();
      setPHYCR1(getPHYCR1() & ~PHYCR1_PWDN);
      wizphy_getphyconf(&phyconf);
      phyconf.mode=PHY_MODE_AUTONEGO;
      wizphy_setphyconf(&phyconf);
      PHYLOCK();
    }
    // !!!

    NETUNLOCK();
    setSHAR(pbtSHAR);
    setGAR(pbtGAR);
    setSUBR(pbtMSR);
    setSIPR(pbtSIPR);
    NETLOCK();
    xSemaphoreGive(s_WiznetLock);
}

uint8 socketUDP(SOCKET s, uint16 port,uint8 flag)
{
  uint8 Result;
  xSemaphoreTake(s_WiznetLock);
  if(s<8) SetIpCS(PF8);
  else
  { s-=8; SetIpCS(PF3); }
  Result=socketW6100(s,Sn_MR_UDP,port,flag | SF_IO_NONBLOCK);
  xSemaphoreGive(s_WiznetLock);
  return Result;
}

uint8 socket(SOCKET s,uint32 protocol, uint16 port,uint8 flag)
{
  uint8 Result;
  xSemaphoreTake(s_WiznetLock);
  if(s<8) SetIpCS(PF8);
  else
  { s-=8; SetIpCS(PF3); }
  Result=socketW6100(s,protocol,port,flag | SF_IO_NONBLOCK);
  xSemaphoreGive(s_WiznetLock);
  return Result;
}

void close(SOCKET s)
{
  xSemaphoreTake(s_WiznetLock);
  if(s<8) SetIpCS(PF8);
  else
  { s-=8; SetIpCS(PF3); }
  closeW6100(s);
  xSemaphoreGive(s_WiznetLock);
}

int8_t listen(uint8_t sn)
{
  int8_t Result;
  xSemaphoreTake(s_WiznetLock);
  if(sn<8) SetIpCS(PF8);
  else
  { sn-=8; SetIpCS(PF3); }
  Result=listenW6100(sn);
  xSemaphoreGive(s_WiznetLock);
  return Result;
}

int8_t connect(uint8_t sn, uint8_t *addr,uint16_t port,uint8_t addrlen)
{
  int8_t Result;
  xSemaphoreTake(s_WiznetLock);
  if(sn<8) SetIpCS(PF8);
  else
  { sn-=8; SetIpCS(PF3); }
  Result=connectW6100(sn,addr,port,addrlen);
  xSemaphoreGive(s_WiznetLock);
  return Result;
}

int8_t disconnect(uint8_t sn)
{
  int8_t Result;
  xSemaphoreTake(s_WiznetLock);
  if(sn<8) SetIpCS(PF8);
  else
  { sn-=8; SetIpCS(PF3); }
  Result=disconnectW6100(sn);
  xSemaphoreGive(s_WiznetLock);
  return Result;
}

datasize_t send(uint8_t sn,uint8_t *buf,datasize_t len)
{
  datasize_t Result;
  xSemaphoreTake(s_WiznetLock);
  if(sn<8) SetIpCS(PF8);
  else
  { sn-=8; SetIpCS(PF3); }
  Result=sendW6100(sn,buf,len);
  xSemaphoreGive(s_WiznetLock);
  return Result;
}

datasize_t recv(uint8_t sn,uint8_t *buf,datasize_t len)
{
  datasize_t Result;
  xSemaphoreTake(s_WiznetLock);
  if(sn<8) SetIpCS(PF8);
  else
  { sn-=8; SetIpCS(PF3); }
  Result=recvW6100(sn,buf,len);
  xSemaphoreGive(s_WiznetLock);
  return Result;
}

uint16 sendto(SOCKET s,uint8 *buf, uint16 len, uint8 *addr, uint16 port)
{
  uint16 Result;
  xSemaphoreTake(s_WiznetLock);
  if(s<8) SetIpCS(PF8);
  else
  { s-=8; SetIpCS(PF3); }
  Result=sendtoW6100(s,buf,len,addr,port,4);
  xSemaphoreGive(s_WiznetLock);
  return Result;
}

uint16 recvfrom(SOCKET s, uint8 *buf, uint16 len, uint8 *addr, uint16 *port)
{
  uint8 btAddrLen;
  uint16 Result;
  xSemaphoreTake(s_WiznetLock);
  if(s<8) SetIpCS(PF8);
  else
  { s-=8; SetIpCS(PF3); }
  Result=recvfromW6100(s,buf,len,addr,port,&btAddrLen);
  xSemaphoreGive(s_WiznetLock);
  return Result;
}

uint16 getsizeIP(SOCKET s,bool bSend)
{
  uint16 Result;
  xSemaphoreTake(s_WiznetLock);
  if(s<8) SetIpCS(PF8);
  else
  { s-=8; SetIpCS(PF3); }
  if(bSend) Result=getSn_TX_FSR(s);
  else Result=getSn_RX_RSR(s);
  xSemaphoreGive(s_WiznetLock);
  return Result;
}

int8_t getsockopt(uint8_t s, sockopt_type sotype, void* arg)
{
  int8_t Result;
  xSemaphoreTake(s_WiznetLock);
  if(s<8) SetIpCS(PF8);
  else
  { s-=8; SetIpCS(PF3); }
  Result=getsockoptW6100(s,sotype,arg);
  xSemaphoreGive(s_WiznetLock);
  return Result;
}

int8_t ctlsocket(uint8_t s, ctlsock_type cstype, void* arg)
{
  int8_t Result;
  xSemaphoreTake(s_WiznetLock);
  if(s<8) SetIpCS(PF8);
  else
  { s-=8; SetIpCS(PF3); }
  Result=ctlsocketW6100(s,cstype,arg);
  xSemaphoreGive(s_WiznetLock);
  return Result;
}

/////////////////////////////////////////////////////////////////////
bool isSocketSendEnd(SOCKET s)
{
  unsigned char btStatus;
  unsigned short off;
  bool bSend;

  if(!g_SockSendBusy[s]) return(true);
  off=0;
  xSemaphoreTake(s_WiznetLock);
  if(s<8) SetIpCS(PF8);
  else
  { off=8; s-=8; SetIpCS(PF3); }
  if(getSn_CR(s))
  {
    xSemaphoreGive(s_WiznetLock);
    return(false);
  }
  btStatus=getSn_IR(s);
  if(btStatus & Sn_IR_SENDOK)
  {
    g_SockSendBusy[s+off]=0;
    setSn_IRCLR(s,Sn_IR_SENDOK | Sn_IR_TIMEOUT);
    xSemaphoreGive(s_WiznetLock);
    return true;
  }  
  xSemaphoreGive(s_WiznetLock);
  return(false);
}

bool isSocketTOut(SOCKET s)
{
  bool bTOut;
  unsigned short off;

  off=0;
  xSemaphoreTake(s_WiznetLock);
  if(s<8) SetIpCS(PF8);
  else
  { off=8; s-=8; SetIpCS(PF3); }
  bTOut=IsCurrentTickCountGT(g_dwSockTOut[s+off]) || (getSn_IR(s) & Sn_IR_TIMEOUT);
  if(bTOut)
  {
    g_SockSendBusy[s+off]=0;
    setSn_IRCLR(s,Sn_IR_TIMEOUT | Sn_IR_SENDOK);
  }
  xSemaphoreGive(s_WiznetLock);
  return(bTOut);
}

