#include "Sinfo.h"
#include "GlobalData.h"

/////////////////////////////////////////////////////////////////////
void InitCall_UK(void);
void InitCall_IPPM(void);
void InitVCSInfo(void);

/////////////////////////////////////////////////////////////////////
volatile static bool s_bReadDSCCfg=false;

/////////////////////////////////////////////////////////////////////
void ReadSectionDSC(char *pStart)
{
  char *pEnd,*pCur;
  static IPPARAMS_TP IpParams;

  pStart=FindSection(pStart,"[DSC]");
  if(!pStart) return;
  pEnd=FindEndSection(pStart);
  InitIpParams(&IpParams);
  pCur=GetParam(pStart,"IPADDR");
  if(pCur) GetIPAddr(pCur,IpParams.btSIPR);
  pCur=GetParam(pStart,"IPMCST");
  if(pCur) GetIPAddr(pCur,IpParams.btMcstIP);
  pCur=GetParam(pStart,"IPMASK");
  if(pCur) GetIPAddr(pCur,IpParams.btMSR);
  pCur=GetParam(pStart,"IPGATE");
  if(pCur) GetIPAddr(pCur,IpParams.btGAR);
  pCur=GetParam(pStart,"PORTCTRL");
  if(pCur) IpParams.wPortControl=GetIPPort(pCur,IpParams.wPortControl);
  pCur=GetParam(pStart,"PORTSOUND");
  if(pCur) IpParams.wPortSound=GetIPPort(pCur,IpParams.wPortSound);
  SetIpParams(&IpParams);
  if(pEnd) *pEnd=0x0a;
}

void ReadSectionCH(char *pStart,char iCh)
{
  char strSectionName[6];
  char *pEnd,*pCur;

  strcpy(strSectionName,"[CH ]");
  strSectionName[3]=iCh+'0';
  pStart=FindSection(pStart,strSectionName);
  if(!pStart) return;
  pEnd=FindEndSection(pStart);
  pCur=GetParam(pStart,"ADCGAIN");
  if(pCur) SetAdcGain4Ch(iCh,GetParamWord(pCur));
  pCur=GetParam(pStart,"DACVOL");
  if(pCur) SetDacVol4Ch(iCh,GetParamWord(pCur));
  pCur=GetParam(pStart,"GENERATOR");
  if(pCur) StartGenerator(iCh,GetParamWord(pCur));
  pCur=GetParam(pStart,"INPUT");
  if(pCur) ChanInputEnable(iCh,GetParamBool(pCur));
  pCur=GetParam(pStart,"OUTPUT");
  if(pCur) ChanOutputEnable(iCh,GetParamBool(pCur));
  if(pEnd) *pEnd=0x0a;
}

void ReadSectionIPPM(char *pStart)
{
  char *pEnd,*pCur;

  pStart=FindSection(pStart,"[IPPM]");
  if(!pStart) return;
  InitCall_IPPM();
  pEnd=FindEndSection(pStart);
  pCur=GetParam(pStart,"MAXIPPM");
  if(pCur) SetMaxIPPM(GetParamWord(pCur));
  pCur=GetParam(pStart,"MAXSPEAK");
  if(pCur) SetMaxSpeakIPPM(GetParamWord(pCur));
  pCur=GetParam(pStart,"IPPM_MASTER");
  if(pCur) SetMasterIPPM(GetParamWord(pCur));
  pCur=GetParam(pStart,"MASTER_LVL");
  if(pCur) SetMasterLvl(GetParamWord(pCur));
  pCur=GetParam(pStart,"GENERATOR");
  if(pCur) StartGenerator(6,GetParamWord(pCur));
  pCur=GetParam(pStart,"INPUT");
  if(pCur) ChanInputEnable(6,GetParamBool(pCur));
  pCur=GetParam(pStart,"OUTPUT");
  if(pCur) ChanOutputEnable(6,GetParamBool(pCur));
  if(pEnd) *pEnd=0x0a;
}

void ReadSectionSERVER(char *pStart)
{
  char *pEnd,*pCur;

  pStart=FindSection(pStart,"[SERVER]");
  if(!pStart) return;
  InitCall_UK();
  pEnd=FindEndSection(pStart);
  pCur=GetParam(pStart,"IPADDR");
  if(pCur) SetUPLvlIpAddr(Str2IP(pCur));
  pCur=GetParam(pStart,"PORTCTRL");
  if(pCur) SetUPLvlPortCtrl(Str2Port(pCur));
  pCur=GetParam(pStart,"PORTSOUND");
  if(pCur) SetUPLvlPortSound(Str2Port(pCur));
  pCur=GetParam(pStart,"STUDIONUMBER");
  if(pCur) SetStudioNumb(GetParamWord(pCur));
  pCur=GetParam(pStart,"FS16");
//  if(pCur) SetStudioFS(GetParamBool(pCur));
  pCur=GetParam(pStart,"GENERATOR");
  if(pCur) StartGenerator(5,GetParamWord(pCur));
  pCur=GetParam(pStart,"INPUT");
  if(pCur) ChanInputEnable(5,GetParamBool(pCur));
  pCur=GetParam(pStart,"OUTPUT");
  if(pCur) ChanOutputEnable(5,GetParamBool(pCur));
  if(pEnd) *pEnd=0x0a;
}

bool DecodePresetPar(char *pCur,unsigned char *pbtIPPM,unsigned char *pbtVCam,unsigned char *pbtPreset)
{ // IPPM_1,1,1 (IPPM,Camera,Preset)
  char *pStr,*pPar;
  int iPar;

  pStr=strchr(pCur,',');
  if(!pStr) return false;
  *pStr++=0;
  pCur=strclr(pCur);
  if(!pCur) return false;
  if(strstr(pCur,"IPPM_")!=pCur) return false;
  pCur+=5;
  iPar=atoi(pCur);
  if((iPar<1) || (iPar>d_MaxAbntIPPM)) return false;
  *pbtIPPM=iPar & 0xff;
  pCur=pStr;

  pStr=strchr(pCur,',');
  if(!pStr) return false;
  *pStr++=0;
  pCur=strclr(pCur);
  if(!pCur) return false;
  iPar=atoi(pCur);
  if((iPar<1) || (iPar>d_MAX_VCAM)) return false;
  *pbtVCam=iPar & 0xff;
  pCur=pStr;

  pCur=strclr(pCur);
  if(!pCur) return false;
  iPar=atoi(pCur);
  if((iPar<1) || (iPar>127)) return false;
  *pbtPreset=iPar & 0xff;

  return true;
}

void ReadPresets(char *pStart)
{
  char *pCur;
  unsigned char btIPPM,btVCam,btPreset;

  ClearVCSPresets();
  do
  {
    pStart=GetString2Buf(pStart);
    pCur=GetParamFromBuf("PRESET");
    if(pCur)
    {
      if(DecodePresetPar(pCur,&btIPPM,&btVCam,&btPreset)) AddVCSPreset(btIPPM,btVCam,btPreset);
    }
  }
  while(pStart);
}

void ReadSectionVCS(char *pStart)
{
  char *pEnd,*pCur,Type=d_VCS_None;

  pStart=FindSection(pStart,"[VCS]");
  if(!pStart) return;
SendStr2IpLogger("$INFO$Section VCS:");
  pEnd=FindEndSection(pStart);
  InitVCSInfo();
  pCur=GetParam(pStart,"TYPE");
  if(pCur)
  {
SendStr2IpLogger("$INFO$Type=%s",pCur);
    if(strcmp(pCur,"YEALINKMEETINGEYE")==0)
    { // YeaLink
      Type=d_VCS_YeaLink;
      pCur=GetParam(pStart,"IPADDR");
      if(pCur) SetVCSIpAddr(Str2IP(pCur));
      pCur=GetParam(pStart,"PORT");
      if(pCur) SetVCSPort(Str2Port(pCur));
    }
    else
    {
      if(strcmp(pCur,"GRANAT")==0)
      { // Granat
        Type=d_VCS_Granat;
        pCur=GetParam(pStart,"PORT");
        if(pCur)
        {
SendStr2IpLogger("$INFO$Port=%s",pCur);
          SetVCSPort(Str2Port(pCur));
        }
      }
      else
      {
        if(strcmp(pCur,"VISCA")==0)
        { // Visca
          Type=d_VCS_Visca;
          pCur=GetParam(pStart,"SPEED");
          if(pCur) SetViscaSpeed(atoi(pCur));
        }
      }
    }
  }
  if(Type!=d_VCS_None)
  {
    static sInfoKadr sInfo;
 
    pCur=GetParam(pStart,"CAMERADEFAULT");
    if(pCur) SetVCSCameraDefault(GetParamWord(pCur));
    ReadPresets(pStart);
    SetVCSType(Type);
    if(Type!=d_VCS_Visca)
    { // ����� � ������� ������ - ����������������� TCP-���������� � VCS
      sInfo.L=0;
      xQueueSend(xQueueReply[UserTCP],&sInfo,0,0);
    }
  }
  if(pEnd) *pEnd=0x0a;
}

void ReadDSCCfgFromBuf(char *pchBuf)
{
  char i;

  ReadSectionDSC(pchBuf);
  ReadSectionIPPM(pchBuf);
  ReadSectionVCS(pchBuf);
  ReadSectionSERVER(pchBuf);
  for(i=1;i<5;i++) ReadSectionCH(pchBuf,i);
}

void ReadDSCCfg(void)
{
  int iSz;
  char *pchBuf;
  void *pBuf;
  HANDLE hFileIn;

  s_bReadDSCCfg=false;
  hFileIn=CreateFile("dsccfg.ini",GENERIC_READ,0);
  if(hFileIn>=0) 
  {
    iSz=GetFileSize(hFileIn);
    if(iSz>0)
    {
      pBuf=pvPortMalloc(iSz+1);
      if(pBuf)
      {
        iSz=ReadFile(hFileIn,pBuf,iSz);
        if(iSz>0)
        {
          pchBuf=(char *)pBuf;
          pchBuf[iSz]=0;
          ReadDSCCfgFromBuf(pchBuf);
        }
        vPortFree(pBuf);
      }
    }
    CloseHandle(hFileIn);
  }
  s_bReadDSCCfg=true;
}

void WaitWhileReadDSCCfg(void)
{
  while(!s_bReadDSCCfg) vTaskDelay(10);
}

