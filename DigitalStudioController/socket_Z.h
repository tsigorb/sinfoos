#ifndef _SOCKET_H_
#define _SOCKET_H_

#include "..\TCPIP\types.h"
#include "..\TCPIP\socketW6100.h"

/**********************************************************
* define function of socket API 
***********************************************************/
void initWiznet(uint16 wCS,uint8 *pbtSHAR,uint8 *pbtGAR,uint8 *pbtMSR,uint8 *pbtSIPR);
uint8 socketUDP(SOCKET s, uint16 port,uint8 flag); // Opens a socket(TCP or UDP or IP_RAW mode)
uint8 socket(SOCKET s,uint32 protocol, uint16 port,uint8 flag);
void close(SOCKET s); // Close socket

int8_t listen(uint8_t sn);
int8_t connect(uint8_t sn, uint8_t *addr,uint16_t port,uint8_t addrlen);
int8_t disconnect(uint8_t sn);
datasize_t send(uint8_t sn,uint8_t *buf,datasize_t len);
datasize_t recv(uint8_t sn,uint8_t *buf,datasize_t len);

uint16 sendto(SOCKET s, uint8 * buf, uint16 len, uint8 * addr, uint16 port); // Send data (UDP/IP RAW)
uint16 recvfrom(SOCKET s, uint8 * buf, uint16 len, uint8 * addr, uint16  *port); // Receive data (UDP/IP RAW)
uint16 getsizeIP(SOCKET s,bool bSend);
int8_t getsockopt(uint8_t s, sockopt_type sotype, void* arg);
int8_t ctlsocket(uint8_t s, ctlsock_type cstype, void* arg);

bool isSocketSendEnd(SOCKET s);
bool isSocketTOut(SOCKET s);

#endif
/* _SOCKET_H_ */

