#ifdef LOADER_BF533
  #include "LoaderBF533.h"

  #include "..\FreeRTOS/FreeRTOS.h"
  #include "..\FreeRTOS/queue.h"
  #include "..\FreeRTOS/task.h"
  #include "..\FreeRTOS/semphr.h"
  #include "..\FreeRTOS/FileSys.h"
#else
  #include "Sinfo.h"
#endif

#define SPI_Err     (MODF | TXE | RBSY | TXCOL)

// !!������ �������� ��� �������� �������, �� �/�� AT45DB161 �������� � ������ 528 ���� �� ��������!!
#define d_FlashPageSize     512 


/////////////////////////////////////////////////////////////////////
static bool s_bEnWr2PgmFlash=false;

xSemaphoreHandle g_SPILock=NULL;

//--------------------------------------------------------------------------//
unsigned short g_wIpCS=PF8,
               g_wFlashCS=PF2;
unsigned char g_btSockOffset=0; // ��� ���������� W6100

//--------------------------------------------------------------------------//
unsigned short GetIpCS(void)
{ return g_wIpCS; }

void SetIpCS(unsigned short wCS)
{
  if(wCS==PF8) g_btSockOffset=0;
  else g_btSockOffset=8;
  g_wIpCS=wCS; 
}

void IpCS_Low(void)
{
  *pFIO_FLAG_C=g_wIpCS;
  ssync();
}

void IpCS_Hi(void)
{
  *pFIO_FLAG_S=g_wIpCS;
  ssync();
}

void FlashCS_Low(unsigned int Addr)
{
  int i;
  switch(Addr >> 20)
  {
    case 0: case 1:
      g_wFlashCS=PF2;
      break;
    case 2: case 3:
      g_wFlashCS=PF4;
      break;
    default: return;
  }
  *pFIO_FLAG_C=g_wFlashCS;
  ssync();
  for(i=0;i<25;i++); // �������� ~80 ��
}

void FlashCS_Hi(void)
{
  *pFIO_FLAG_S=g_wFlashCS;
  ssync();
}

void EnableWrite2ProgrammArea(bool bEn)
{ s_bEnWr2PgmFlash=bEn; }

bool IsEnableWrite2ProgrammArea(void)
{ return(s_bEnWr2PgmFlash); }

/*
CPOL=0 � ������ ������������� ���������� � ������� ������;
CPOL=1 � ������ ������������� ���������� � �������� ������;
CPHA=0 � ������� ������ ������������ �� ��������� ������ ������� �������������;
CPHA=1 � ������� ������ ������������ �� ������� ������ ������� �������������.

��� ����������� ������� ������ ���������� SPI ������� ��������� ����������:
����� 0 (CPOL=0, CPHA=0);
����� 1 (CPOL=0, CPHA=1);
����� 2 (CPOL=1, CPHA=0);
����� 3 (CPOL=1, CPHA=1).
*/

void Init_SPI(void)
{
  if(!g_SPILock) vSemaphoreCreateBinary(g_SPILock);
  *pSPI_BAUD=5; // ��������� ������� ������ SPI SCK = SCLK/(2*SPIBAUD0) = 49152/10 ~ 4915 kHz
  *pSPI_CTL=0x5C05; // ��������� ������������ � ��������� SPI
  ssync();
  *pSPI_FLG=0xFF00;
  ssync();
}

bool SPIO_WrRd(unsigned short Instr,unsigned short *DanFlash)
{ // ��������/����� ����� �� SPI
  int i;

  *pSPI_STAT=SPI_Err; // ����� ������
  ssync();
  *pSPI_TDBR=Instr;
  ssync();
  for(i=0;i<100;i++)
  {
    if(*pSPI_STAT & RXS)
    {
      *DanFlash=*pSPI_RDBR;
      ssync();
      return(true);
    }
    ssync();
  }
  *pSPI_STAT=SPI_Err; // ����� ������
  ssync();
  return false; // ���������� SPI ���
}

bool SPIO_Wr(unsigned short Instr)
{ // �������� ����� �� SPI
  unsigned short DanFlash;

  if(SPIO_WrRd(Instr,&DanFlash)) return true;
  return false;
}

//////////////////////////////////////////////////////////////////////////
// ������ � Flash ������� AT45DB161
///// ������ � �/�� AT45DB161 ///////////////////////////////////////
// ����������� ����� ������ � ������ Flash (����� ��������, ����� ����� � ��������)
// �������: ����� � ������� Flash
// ���������: unsigned int Addr - ����� ������
unsigned int AdrToPageAT45DB161(unsigned int Addr)
{
  Addr&=0x1FFFFF;   // � �������� ����� AT45DB161
  if(d_FlashPageSize==512)
  { return(((Addr << 1) & 0xFFFC00) + (Addr & 0x1ff)); }
  else
  { return ((Addr / 528)<<10)+(Addr%528); }
}

unsigned short _xGetStatusAT45DB161(unsigned int Addr)
{
  short i;
  unsigned short  DanFlash;

  DanFlash=0;
  FlashCS_Low(Addr); // ��������� �������
  if(SPIO_Wr(0xD7)) // ������� ������ �������� ���������
  { // ��, �������� �������� � ������ �������� ������� ����
    if(SPIO_WrRd(0,&DanFlash)) ;
  }
  FlashCS_Hi(); // ���������� �������
  for (i=0; i < 25; i++); // �������� ~80 ��
  return DanFlash;
}

bool ResetFlash(void)
{
  return true;
}

// �������� ��������� Flash ������ ��� ���������� ������� ������\��������
// �������: TRUE - ���� �������� ��������� �������; FALSE - ���� �������� ��������� � �������
// ���������: unsigned short *lpAddr - ����� ������, ����������� �� ������� 2 ����
bool _xCheckState(unsigned int Addr)
{
  portTickType xTimeToWake;

  xTimeToWake=xTaskGetTickCount()+200; // �������� �� 200 ��
  while(IsCurrentTickCountLT(xTimeToWake))
  {
    if((_xGetStatusAT45DB161(Addr) & 0x80)!=0) return true;
  }
  return false;
}

// ������� �������� ������� Flash ������.
// �������: TRUE - ���� �����, FALSE -���� ������
// ���������: unsigned int Addr - ����� ������, ����������� �� ������� �������� 528 (512) ����
//                     int How - ����� ��������� ������ � ������ ������������� �� ������� 528 (512) ����
bool WrDeviceErase(unsigned int Addr0, unsigned int How)
{
//  bool bRet;
  unsigned int Address,PageNum;

  for(Address=Addr0;Address<(Addr0 + How);Address+=d_FlashPageSize)
  {
    PageNum=AdrToPageAT45DB161(Address);
    xSemaphoreTake(g_SPILock);
    FlashCS_Low(Address); // ��������� �������
    SPIO_Wr(0x81); // ������� �������� ��������
    SPIO_Wr(PageNum>>16);
    SPIO_Wr(PageNum>>8);
    SPIO_Wr(PageNum);
    FlashCS_Hi(); // ���������� �������
    if(!_xCheckState(Addr0))
    {
      xSemaphoreGive(g_SPILock);
      return(false);
    }
    xSemaphoreGive(g_SPILock);
#ifdef LOADER_BF533
    ResetWatchDog(true);
#endif
  }
  return true;
}

//////////////////////////////////////////////////////////////////////////
// ������ ������� ������ �� Flash.
// �������: TRUE - ���� �����, FALSE -���� ������
// ���������: unsigned int Addr - ����� ����-������, ����������� �� ������� 2 ����
//			unsigned short *lpInp - ��������� �� ������ ������� ������
//			int HowInp - ���������� ������������ ����
//////////////////////////////////////////////////////////////////////////
bool _xWrDeviceArray(unsigned int Addr0, unsigned short *lpInp, unsigned int HowInp)
{
  unsigned int Data;
  unsigned int Address,PageNum;
  unsigned int i,j,j_Start,j_End,
                 k,k_Start,k_End;

  if(Addr0 & 0x01) return(false);
  Address=Addr0 & 0x1FFFFF; // �������� ������� ����� ������, ������ ������ ����������
  if(!IsEnableWrite2ProgrammArea())
  {
    if(!(Addr0 & 0xF00000))
    {
      if(Address<dAddrFS2Flash)
      {
        if((Address==AdrSN) || (Address==AdrSN+2) || (Address==AdrFlashLastFile) || (Address==AdrFlashLastFile+2))
        {
          if(HowInp!=1) return false;
        }
        else
        {
          if((Address<dAddrPgm2Flash) || (Address+HowInp>dAddrPgm2Flash+2)) return false;
        }
      }
    }
  }
  *pSPI_BAUD=2; // SPI SCK=SCLK/(2*SPIBAUD0)=49152/4 ~ 12288 kHz
  j_Start=Address/d_FlashPageSize;
  j_End=(Address+HowInp*2)/d_FlashPageSize;
  for(j=j_Start;j<=j_End;j++)
  { // �������� �� ���������� �������
    k_Start=(j==j_Start)?(Address%d_FlashPageSize):0;
    k_End=(j==j_End)?((Address+HowInp*2)-j_End*d_FlashPageSize):d_FlashPageSize;
    if(((j==j_Start) && (k_Start!=0)) ||
       ((j==j_End) && (k_End!=d_FlashPageSize)))
    { // ��������� ������ � ��������� �������� ���� � �����
      FlashCS_Low(Addr0); // ��������� �������
      SPIO_Wr(0x53); // ������� Memory Page to Buffer 1 Transfer
      SPIO_Wr((j<<10)>>16);
      SPIO_Wr((j<<10)>>8);
      SPIO_Wr(0);
      FlashCS_Hi(); // ���������� �������
      if(!_xCheckState(Addr0)) break;
    }
	for(k=k_Start;k<k_End;k+=2)
	{ // �������� ������ ��������
		FlashCS_Low(Addr0); // ��������� �������
		SPIO_Wr(0x84); // ������� Write to Buffer 1
		SPIO_Wr(0);
		SPIO_Wr(k>>8);
		SPIO_Wr(k);
		Data=*lpInp++;
		SPIO_Wr(Data);
		SPIO_Wr(Data >> 8);
		FlashCS_Hi(); // ���������� �������
		for (i=0; i < 25; i++); // �������� ~80 ��
	}
	FlashCS_Low(Addr0); // ��������� �������
	SPIO_Wr(0x83); // ������� Buffer1 To Memory Page without build-in erase
	SPIO_Wr((j<<10)>>16);
	SPIO_Wr((j<<10)>>8);
	SPIO_Wr(0);
	FlashCS_Hi(); // ���������� �������
	if(!_xCheckState(Addr0)) break;
    Addr0+=d_FlashPageSize;
#ifdef LOADER_BF533
    ResetWatchDog(true);
#endif
  }
  *pSPI_BAUD=5; // SPI SCK=SCLK/(2*SPIBAUD0)=49152/10 ~ 4915 kHz
  if(j>j_End) return true;
  return(false);
}

//////////////////////////////////////////////////////////////////////////
// ������ ����� �� Flash ������
// �������: TRUE - ���� �����, FALSE -���� ������
// ���������: unsigned int Addr - ����� ������, ����������� �� ������� 2 ����
//			unsigned short bInp - �������(������������) �����
//////////////////////////////////////////////////////////////////////////
bool _xWrDeviceShort(unsigned int Addr, unsigned short wInp)
{ return _xWrDeviceArray(Addr, &wInp, 1); }

// ������������ � FileSys.C
bool _xWrFlash(unsigned int Addr,unsigned short wDan)
{ return _xWrDeviceArray(Addr, &wDan, 1); }

/////////////////////////////////////////////////////////////////////////////////
// ������ *lpHowOut ���� �� Flash �� ���������� ������
// �������: ������ TRUE
// ���������: unsigned short *lpInpAddr - ����� ������. ������ ���� �������� �� ������� 2 ����
//			unsigned short *lpOut - ����� ��������� ������(�� ����� 2 ����)
// 			int *lpHowOut ���������� ���� ��� ������
/////////////////////////////////////////////////////////////////////////////////
bool _xRdFlashP(unsigned int InpAddr, unsigned short *lpOut, unsigned int *lpHowOut)
{
  unsigned int Index,Address,AddrCur;
  unsigned int PageNum,LastPage,FirstPage;
  unsigned int i,j,k,cnt;
  unsigned short DanFlash,Data;

  AddrCur=InpAddr;
  Address=AddrCur & 0x1FFFFF; // �������� ������� ����� ������, ������ ������ ����������
  Index=0;
  cnt=(*lpHowOut)*2;
  LastPage=(Address+cnt)/d_FlashPageSize;
  FirstPage=Address/d_FlashPageSize;
  *pSPI_BAUD=2; // SPI SCK=SCLK/(2*SPIBAUD0)=49152/4 ~ 12288 kHz
  for(j=FirstPage;j<=LastPage;j++)
  { // �������� �� ���������� �������
    PageNum=AdrToPageAT45DB161(j*d_FlashPageSize) + ((j==FirstPage)?(Address%d_FlashPageSize):0);
    FlashCS_Low(AddrCur); // ��������� �������
    SPIO_Wr(0x03); // ������� Continuous Array Read Low Frequency
    SPIO_Wr(PageNum>>16);
    SPIO_Wr(PageNum>>8);
    SPIO_Wr(PageNum);
    for(k=((j==FirstPage)?(Address%d_FlashPageSize):0);
        k<((j==LastPage)?((Address+cnt)-LastPage*d_FlashPageSize):d_FlashPageSize);
        k+=2)
    { // �������� ������ ��������
      SPIO_WrRd(0,&DanFlash);
      Data=DanFlash;
      SPIO_WrRd(0,&DanFlash);
      Data=Data | (DanFlash << 8);
      lpOut[Index++]=Data;
    }
    FlashCS_Hi(); // ���������� �������
    for(i=0;i<25;i++); // �������� ~80 ��
    AddrCur+=d_FlashPageSize;
#ifdef LOADER_BF533
    ResetWatchDog(true);
#endif
  }
  *pSPI_BAUD=5; // SPI SCK=SCLK/(2*SPIBAUD0)=49152/10 ~ 4915 kHz
  *lpHowOut=Index;
  return true;
}

unsigned short _xRdFlash(unsigned int AdrFl)
{
  unsigned short wRet=0x1234;
  unsigned int dwCnt=1;

  _xRdFlashP(AdrFl,&wRet,&dwCnt);
  return wRet;
}

///////////////////////////////////////////////////////////////////////
// ��������� �����. ���������� ������ �������� � ������� �� Flash ������
// �������: TRUE - ���� ��������� ������; FALSE - ���� �� �����
// ���������: const unsigned short *lpInp - ��������� �� ������ � ������
//			unsigned int AddrFlashInp - ����� ������ �� Flash ������
//			int Len - ����� ��������� 
///////////////////////////////////////////////////////////////////////
bool _xWithFlashMemCmp(const unsigned short *lpInp,unsigned int AddrFlashInp,unsigned int Len)
{
  unsigned short wOut;
  unsigned int How, Index, bHowOut;

  bHowOut=1;
  if(Len & 0x1) How=Len/2 + 1;
  else How=Len/2;
  for(Index=0; Index < How; Index++)
  {
    _xRdFlashP(AddrFlashInp+Index*2, &wOut, &bHowOut);
    if(((lpInp[Index] ^ wOut) & 0x00ff) != 0) return false;
    if((((lpInp[Index] ^ wOut) & 0xff00) != 0) && (Len < (Index *2))) return false;
  }
  return true;
}

// ���������� ������� ��� ������ � SPI-���� /////////////////////////
unsigned short RdFlash(unsigned int AdrFl)
{
  unsigned short wRet;

  xSemaphoreTake(g_SPILock);
  wRet=_xRdFlash(AdrFl);
  xSemaphoreGive(g_SPILock);
  return wRet;
}

bool WrFlash(unsigned int AdrFl,unsigned short DanFl)
{
  bool bRet;

  if((AdrFl<AdrParam2Flash) && !s_bEnWr2PgmFlash) return false;
  xSemaphoreTake(g_SPILock);
  bRet=_xWrFlash(AdrFl,DanFl);
  xSemaphoreGive(g_SPILock);
  return bRet;
}

bool RdFlashP(unsigned int InpAddr, unsigned short *lpOut,unsigned int *lpHowOut)
{ // AT45DB161
  bool bRet;

  xSemaphoreTake(g_SPILock);
  bRet=_xRdFlashP(InpAddr,lpOut,lpHowOut);
  xSemaphoreGive(g_SPILock);
  return bRet;
}

bool WrDeviceArray(unsigned int Addr0, unsigned short *lpInp,unsigned int HowInp)
{ // AT45DB161
  bool bRet;

  xSemaphoreTake(g_SPILock);
  bRet=_xWrDeviceArray(Addr0,lpInp,HowInp);
  xSemaphoreGive(g_SPILock);
  return bRet;
}

bool WithFlashMemCmp(const unsigned short *lpInp, const unsigned int AddrFlashInp,unsigned int Len)
{ // AT45DB161
  bool bRet;

  xSemaphoreTake(g_SPILock);
  bRet=_xWithFlashMemCmp(lpInp,AddrFlashInp,Len);
  xSemaphoreGive(g_SPILock);
  return bRet;
}

