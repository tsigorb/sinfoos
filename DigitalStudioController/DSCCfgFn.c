#include "Sinfo.h"

static char s_strBuf[250];

char *GetString2Buf(char *pStart)
{
  int i;
  char chSb;

  for(i=0;i<249;i++)
  {
    chSb=*pStart++;
    if(chSb==0)
    { s_strBuf[i]=0; return NULL; }
    if((chSb==0x0d) || (chSb==0x0a))
    {
      s_strBuf[i]=0;
      chSb=*pStart;
      if(chSb==0) return NULL;
      if((chSb==0x0d) || (chSb==0x0a)) pStart++;
      return pStart;
    }
    s_strBuf[i]=chSb;
  }
  s_strBuf[249]=0;
  return pStart;
}

char *FindSection(char *pStart,char *pName)
{
  char *pStrS;
  do
  {
    pStart=GetString2Buf(pStart);
    pStrS=strclr(s_strBuf);
    if(pStrS)
    {
      if(strcmp(pStrS,pName)==0) return pStart;
    }
  }
  while(pStart);  
  return NULL;
}

char *FindEndSection(char *pStart)
{  
  char *pStrS,*pStr;
  do
  {
    pStrS=GetString2Buf(pStart);
    pStr=strclr(s_strBuf);
    if(pStr && (pStr[0]=='[') && (pStr[strlen(pStr)-1]==']'))
    {
      *(pStart-1)=0;
      return pStart-1;
    }
    pStart=pStrS;
  }
  while(pStart);  
  return NULL;
}

char *GetParamFromBuf(char *pName)
{
  char *pStrS,*pStr;

  pStrS=strclr(s_strBuf);
  if(!pStrS) return NULL;
  pStr=strstr(pStrS,"//");
  if(pStr)
  {
    *pStr=0;
     pStrS=strclr(pStrS);
     if(!pStrS) return NULL;
  }
  pStr=strchr(pStrS,'=');
  if(!pStr) return NULL;
  *pStr=0;
  pStrS=strclr(pStrS);
  if(!pStrS) return NULL;
  if(strcmp(pStrS,pName)==0)
  {
    pStrS=strclr(pStr+1);
    return pStrS;
  }
  return NULL;
}

char *GetParam(char *pStart,char *pName)
{
  char *pStrS;
  do
  {
    pStart=GetString2Buf(pStart);
    pStrS=GetParamFromBuf(pName);
    if(pStrS) return pStrS;
  }
  while(pStart);  
  return NULL;
}

unsigned int Str2IP(char *pStr)
{
  int i;
  char *pEnd;
  unsigned int dwIP=0;

  for(i=0;i<4;i++)
  {
    if(!pStr || !pStr[0]) break;
    dwIP<<=8;
    if(i<3)
    {
      pEnd=strchr(pStr,'.');
      if(!pEnd) break;
      *pEnd=0; pEnd++;
    }
    pStr=strclr(pStr);
    if(!pStr) break;
    dwIP|=atoi(pStr);
    if(i!=3) pStr=pEnd;
    else
    {
      dwIP=(dwIP>>24) | ((dwIP & 0xff0000)>>8) | ((dwIP & 0xff00)<<8) | ((dwIP & 0xff)<<24);
      return dwIP;
    }
  }
  return 0;
}

void GetIPAddr(char *pStart,unsigned char *pAddr)
{
  unsigned int dwIP=Str2IP(pStart);
  if(dwIP) memcpy(pAddr,&dwIP,4);
}

unsigned short Str2Port(char *pStart)
{
  if(pStart) return atoi(pStart);
  return 0;
}

unsigned short GetIPPort(char *pStart,unsigned short wPort0)
{
  if(pStart)
  {
    unsigned short wPort=Str2Port(pStart);
    if(wPort) return wPort;
  }
  return wPort0;
}

unsigned short GetParamWord(char *pStart)
{
  if(pStart) return (unsigned short)atoi(pStart);
  return 0;
}

bool GetParamBool(char *pStart)
{
  if(pStart)
  {
    strlwr(pStart);
    if(strcmp(pStart,"enable")==0) return true;
  }
  return false;
}

