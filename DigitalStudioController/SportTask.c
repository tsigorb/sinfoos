#include "Sinfo.h"
#include "SPort.h"

/////////////////////////////////////////////////////////////////////
void Init_SportsData(void);
void StartAudio(void);
bool isSpeakMaster(void);

/////////////////////////////////////////////////////////////////////
extern unsigned short g_wMasterIPPM;

extern short g_shSinTable[];

/////////////////////////////////////////////////////////////////////
xOutputIpFromISR MasOutputIpFromISR[d_MaxCPOut];

void InitMasOutputIpFromISR(void)
{
  xRTPHeadUK *pRtpH;
  xRTPTail *pRtpT;

  memset(MasOutputIpFromISR,0,sizeof(MasOutputIpFromISR));
  pRtpH=(xRTPHeadUK *)(MasOutputIpFromISR[0].wIpBufs[0]);
  pRtpH->Fmt=0x0890; pRtpH->Profile=ReverseShort(GetStudioNumber());
  pRtpH=(xRTPHeadUK *)(MasOutputIpFromISR[0].wIpBufs[1]);
  pRtpH->Fmt=0x0890; pRtpH->Profile=ReverseShort(GetStudioNumber());

  pRtpT=(xRTPTail *)(&MasOutputIpFromISR[1].wIpBufs[0][d_LenSoundIPPM]);
  pRtpT->Fmt=0x0890; pRtpT->Profile=2;
  pRtpT=(xRTPTail *)(&MasOutputIpFromISR[1].wIpBufs[1][d_LenSoundIPPM]);
  pRtpT->Fmt=0x0890; pRtpT->Profile=2;
}

unsigned char FreeAudioPckt4Ip(unsigned char btCPOut)
{
  unsigned char Overrun;
  unsigned int dwIRQ;
  xOutputIpFromISR *pAdr;

  if(!btCPOut || (btCPOut>2)) return NULL;
#define _ pAdr->
  pAdr=MasOutputIpFromISR+btCPOut-1;
  dwIRQ=cli();
  Overrun=_ Overrun;
  if(Overrun) _ NumBuf^=1;
  else _ NewData=0;
  _ Overrun=0;
  sti(dwIRQ);
#undef _
  return Overrun;
}

// btCPOut== 0-none, 1-UK, 2-IPPM multicast
unsigned char *GetAudioPcktAddr4Ip(unsigned char btCPOut)
{ // ���������� ����� ������ ��� UK ��� Multicast IPPM (���������� � IP-������)
  unsigned char *pbtBuf;
  xOutputIpFromISR *pAdr;

  if(!btCPOut || (btCPOut>2)) return NULL;
#define _ pAdr->
  pAdr=MasOutputIpFromISR+btCPOut-1;
  if(_ NewData)
  {
    pbtBuf=(unsigned char *) _ wIpBufs[ _ NumBuf^1];
    pAdr->Number++;
    if(pAdr->Number==0) pAdr->Number=1; 
    if(btCPOut>1)
    {
      xRTPTail *pRtp=(xRTPTail *)(pbtBuf+(2*d_LenSoundIPPM));
      pRtp->Number=ReverseShort(pAdr->Number);
//pRtp->Number=ReverseShort(xTaskGetTickCount() & 0xffff);
    }
    else
    {
      xRTPHeadUK *pRtp=(xRTPHeadUK *)pbtBuf;
      pRtp->Number=ReverseShort(pAdr->Number);
    }
    return pbtBuf;
  }
#undef _
  return NULL;
}

/////////////////////////////////////////////////////////////////////
static section("bigdata") InputIpBufTP sMasInputIpBuf[d_MaxCPIn*d_MaxIpBufs];

xInputIp4ISR MasInputIp4ISR[d_MaxCPIn];

void InitInputIpBuf(void)
{
  unsigned char btInd;

  for(btInd=0;btInd<d_MaxCPIn*d_MaxIpBufs;btInd++)
  {
    sMasInputIpBuf[btInd].Chan=0;
    sMasInputIpBuf[btInd].Ind=btInd;
  }
}

void SetFreeInputBufs(unsigned char btChan)
{ // ����������� ��� �������� ������ ��� �� (������������ ����� ��������� ����� ������� ���� �������)
  unsigned char btInd;

  for(btInd=0;btInd<d_MaxCPIn*d_MaxIpBufs;btInd++)
  {
    if(sMasInputIpBuf[btInd].Chan==btChan) sMasInputIpBuf[btInd].Chan=0;
  }
}

InputIpBufTP *GetFreeInputIpBufAddr(void)
{ // ���������� � IP-������
  unsigned char btInd;

  for(btInd=0;btInd<d_MaxCPIn*d_MaxIpBufs;btInd++)
  {
    if(sMasInputIpBuf[btInd].Chan==0) return sMasInputIpBuf+btInd;
  }
  return NULL;
}

void InitMasInputIp4ISR(void)
{
  int i;
  memset(MasInputIp4ISR,0,sizeof(MasInputIp4ISR));
}

// ��������������, ��� ������� �������� ������� ��������� � �������� ������������!
void AddInd2MasInputIp4ISR(unsigned char Chan,unsigned char btInd)
{ // ���������� � IP-������
  unsigned short i,wInd;
  xInputIp4ISR *pAdr;

#define _ pAdr->
  pAdr=MasInputIp4ISR;
  for(i=0;i<d_MaxCPIn;i++,pAdr++)
  {
    if(_ Chan==Chan)
    {
      vPortEnableSwitchTask(false);
      if(_ Start<3)
      {
        _ MasIpBufsInd[_ Start]=btInd;
        _ Start++;
        if(_ Start==3) _ Head=3;
      }
      else
      {
        _ MasIpBufsInd[_ Head]=btInd;
        _ Head++;
        if(_ Head>=d_MaxIpBufs) _ Head=0;
        if(_ Head== _ Tail)
        { // ���� ������ ����� ���������� ������� �����, ��
SendStr2IpLogger("$AUDIO_IN$AddInd2MasInputIp4ISR: very slow plays for chan=%d",_ Chan);
          wInd=_ MasIpBufsInd[_ Tail];
          sMasInputIpBuf[wInd].Chan=0; // ����������� ���������� ����� ������ IPPM
          _ Tail++;                    // � �������� ����� 
          if(_ Tail>=d_MaxIpBufs) _ Tail=0;
        }
      }
      vPortEnableSwitchTask(true);
    }
  }
#undef _
}

void ExpandPacketFromUK(short *DstIpBuf,unsigned char *btSrcIpBuf)
{ // ��������� �������� �� �� ����� �� 16 ��� 16 ��� linear
  int i;
  short *Buf=DstIpBuf;

  btSrcIpBuf+=sizeof(xRTPHeadUK);
  for(i=0;i<d_LenSoundUK;i++)
  { // ����������� a-Law � linear, ��������� ������ ������ ������ � DstIpBuf ��� ����������� ������������
    *Buf=a_expand(*btSrcIpBuf);
    Buf+=2; btSrcIpBuf++;
  }
  for(i=0;i<d_LenSoundIPPM-2;i+=2)
  { // ����������� ������==�������� ����� ����� ���������
    DstIpBuf[i+1]=((int)DstIpBuf[i]+DstIpBuf[i+2])/2;
  }
  DstIpBuf[i+1]=DstIpBuf[i]; // ��������� ������ ������ ���������
}

void GetData4Play(void)
{ // ���������� ��������������� ����� �� �������
  unsigned short i;
  xInputIp4ISR *pAdr;

#define _ pAdr->
  pAdr=MasInputIp4ISR;
  for(i=0;i<d_MaxCPIn;i++,pAdr++)
  {
    if(_ Chan && _ NewData)
    { // ���� �������� ����� �������� ����� ��� ISR
      short NumbBuf= _ NumBuf^1;

      vPortEnableSwitchTask(false);
      if( _ Tail== _ Head)
      { // ��� �������� ������� -> ������� ����
        _ Sync=0;
        _ Takt4Pckt=0;
        _ OverrunIP++;
//SendStr2IpLogger("$AUDIO_IN$GetData4Play: CP=%d, OverrunIP=%d",_ Chan,_ OverrunIP);
        memset(_ wIpBufs+(NumbBuf*d_LenSoundIPPM),0,d_LenSoundIPPM*2);
      }
      else
      { // ���� �������� �����
        int iDt,Ind;
        Ind=_ MasIpBufsInd[_ Tail];
        if( _ Chan>1)
        { // ����� �� IPPM. ���� ������� IPPM �������, �� ������ �� ���� ��������� IPPM ������������!
          if(sMasInputIpBuf[Ind].IPPM_Numb==g_wMasterIPPM)
          { memcpy(_ wIpBufs+(NumbBuf*d_LenSoundIPPM),sMasInputIpBuf[Ind].wIpBuf,d_LenSoundIPPM*2); }
          else 
          {
            if(isSpeakMaster()) memset(_ wIpBufs+(NumbBuf*d_LenSoundIPPM),0,d_LenSoundIPPM*2);
            else memcpy(_ wIpBufs+(NumbBuf*d_LenSoundIPPM),sMasInputIpBuf[Ind].wIpBuf,d_LenSoundIPPM*2);
          }
        }
        else
        { // ��������� �������� �� �� ����� �� 16 ��� 16 ��� linear
          ExpandPacketFromUK((short *)(_ wIpBufs+(NumbBuf*d_LenSoundIPPM)),(unsigned char *)sMasInputIpBuf[Ind].wIpBuf); 
        }
        sMasInputIpBuf[Ind].Chan=0; // ����������� ����� ������
        _ Tail++;
        if( _ Tail>=d_MaxIpBufs) _ Tail=0;
        // ���������� ������� ����� Head � Tail �� 2 �� 4
        // ���� ������� ����� ��������������� ������ <=1, �� ��������� �������� ���������������
        // ���� ������� ����� ��������������� ������ >=5, �� ����������� �������� ���������������
        iDt= _ Head - _ Tail;
        if(iDt<0) iDt+=d_MaxIpBufs;
        if(iDt<=1)
        { // ���� ��������� ��������������� �� 10%
//          _ Sync=-1;
SendStr2IpLogger("$DEBUG$GetData4Play: CP=%d, Ind=%d, iDt=%d", _ Chan,Ind,iDt);
        }
        else
        {
          if(iDt>=5)
          { // ���� �������� ��������������� �� 10%
//            _ Sync=1;
SendStr2IpLogger("$DEBUG$GetData4Play: CP=%d, Ind=%d, iDt=%d", _ Chan,Ind,iDt);
          }
          else
          { // ��� �� 
          }
        }
//_ Sync=0;
      }
      vPortEnableSwitchTask(true);
      if(_ OverrunISR)
      {
SendStr2IpLogger("$AUDIO_IN$GetData4Play: CP=%d - overrunISR!!!",_ Chan);
        _ NumBuf^=1; _ OverrunISR=0;
      }
      else _ NewData=0;
    }
  }
#undef _
}

/////////////////////////////////////////////////////////////////////
static unsigned char s_MasCP[d_MaxCPIn]; // ������ ������� ����� ����������� (s_MasCP[i]==0 - �� ��������)

void Init_MasCP(void)
{ memset(s_MasCP,0,sizeof(s_MasCP)); }

unsigned char GetFreeCP(void)
{
  int i;

  vPortEnableSwitchTask(false);
  for(i=1;i<d_MaxCPIn;i++)
  {
    if(s_MasCP[i]==0)
    {
      s_MasCP[i]=i+1;
      vPortEnableSwitchTask(true);
      return i+1;
    }
  }
  vPortEnableSwitchTask(true);
  return 0;
}

void SetFreeCP(unsigned char btCP)
{ // ������������ ����� ����� ������� ���� �-��� ������ ���� ���������!!
  int i;

  for(i=1;i<d_MaxCPIn;i++)
  {
    if(s_MasCP[i]==btCP)
    {
      SetFreeInputBufs(btCP); // ����������� ��� �������� ������ ��� ��
      s_MasCP[i]=0;
      break;
    }
  }
}

/////////////////////////////////////////////////////////////////////
static int s_iCntr=0;

unsigned int g_dwCntMcst=0,
             g_dwTmStart=0;

void StartAudio4IPPM_Multicast(void)
{
  s_iCntr++;
  if(s_iCntr==1)
  { // �������� ���������� multicast-������ � ���� ������ IPPM
    vPortEnableSwitchTask(false);
g_dwTmStart=xTaskGetTickCount();
g_dwCntMcst=0;
    MasOutputIpFromISR[1].Number=0;
    MasOutputIpFromISR[1].NumBuf=0;
    MasOutputIpFromISR[1].NewData=0;
    MasOutputIpFromISR[1].Ind=0;
    MasOutputIpFromISR[1].Overrun=0;
    MasOutputIpFromISR[1].Chan=2;
    vPortEnableSwitchTask(true);
  }
}

void StopAudio4IPPM_Multicast(void)
{
  if(s_iCntr)
  {
    s_iCntr--;
    if(s_iCntr==0)
    { // ���������� �������� multicast-������� � ���� ������ IPPM
      vPortEnableSwitchTask(false);
      MasOutputIpFromISR[1].Chan=0;
      MasOutputIpFromISR[1].NewData=0;
      vPortEnableSwitchTask(true);
    }
  }
}

void StartAudioFromIPPM(unsigned char btCP)
{
  unsigned int dwIRQ;
  xInputIp4ISR *pAdr;

  pAdr=MasInputIp4ISR+btCP-1;
  dwIRQ=cli();
  memset(pAdr,0,sizeof(xInputIp4ISR));
  pAdr->Chan=btCP;
  sti(dwIRQ);
}

void StopAudioFromIPPM(unsigned char btCP)
{ // ������������ ����� ����� ������� ���� �-��� ������ ���� ���������!!
  btCP--;
  MasInputIp4ISR[btCP].Chan=0;
  MasInputIp4ISR[btCP].NewData=0;
}

void StartAudio4UK(void)
{ // �������� ���������� �������� ������ ��� ��
  vPortEnableSwitchTask(false);
  if(MasOutputIpFromISR[0].Chan==0)
  {
    MasOutputIpFromISR[0].Number=0;
    MasOutputIpFromISR[0].NumBuf=0;
    MasOutputIpFromISR[0].NewData=0;
    MasOutputIpFromISR[0].Ind=0;
    MasOutputIpFromISR[0].Overrun=0;
    MasOutputIpFromISR[0].Chan=1;
  }
  vPortEnableSwitchTask(true);
}

void StopAudio4UK(void)
{ // ���������� ���������� �������� ������ ��� ��
  vPortEnableSwitchTask(false);
  MasOutputIpFromISR[0].Chan=0;
  MasOutputIpFromISR[0].NewData=0;
  vPortEnableSwitchTask(true);
}

void StartAudioFromUK(void)
{
  vPortEnableSwitchTask(false);
  if(MasInputIp4ISR[0].Chan==0)
  {
    memset(MasInputIp4ISR,0,sizeof(xInputIp4ISR));
    MasInputIp4ISR[0].Chan=1;
  }
  vPortEnableSwitchTask(true);
}

void StopAudioFromUK(void)
{
  vPortEnableSwitchTask(false);
  MasInputIp4ISR[0].Chan=0;
  MasInputIp4ISR[0].NewData=0;
  vPortEnableSwitchTask(true);
}

/////////////////////////////////////////////////////////////////////
short round(double fX)
{
  if(fX>0.) return (short)ceil(fX);
  else return (short)floor(fX);
}

/*short shSin500[32]=
{    0, 6390, 12534, 18197, 23161, 27236, 30266, 32133, 32767, 32133, 30266, 27236, 23161, 18197, 12534, 6390};*/
/*     0,-6390,-12534,-18197,-23161,-27236,-30266,-32133,-32767,-32133,-30266,-27236,-23161,-18197,-12534,-6390 };*/
void SportsTask(void *pvParameters)
{
  { // ��������� �������� ������� ������ ��� ���������
    int i,j;
    double fX;

    for(i=0,j=0;i<d_Fs/2;i+=d_FStep,j++)
    {
      fX=32767.*sin(TWO_PI*(double)i/(double)d_Fs);
      g_shSinTable[j]=round(fX);
    }
  }
  Init_SportsData();
  Init_MasCP();
  InitMasOutputIpFromISR();
  InitInputIpBuf();
  InitMasInputIp4ISR();
  StartAudio();
//vTaskDelay(1000);
//StartAudio4IPPM_Multicast(); //!!! ������� ������� �������� �������� �������!!!
//StartGenerator(1,1); //!!! �������� ��������� ������ �� ������� ������ !!!
  while(1)
  {
    GetData4Play();
    vTaskDelay(1);
  }
}

