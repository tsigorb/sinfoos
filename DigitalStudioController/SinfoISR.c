#include "Sinfo.h"

/////////////////////////////////////////////////////////////////////
void vTickISR(void);

/////////////////////////////////////////////////////////////////////
extern time_t Time_Sys; 

/////////////////////////////////////////////////////////////////////
static unsigned char s_btUARTState=0;

static xSemaphoreHandle s_UartLock=NULL;

/////////////////////////////////////////////////////////////////////
void InitUART(void)
{
  *pUART_IER=0; // ������ ����������
  if(!s_UartLock) vSemaphoreCreateBinary(s_UartLock);
  *pUART_GCTL=UCEN; // ��������� UART CLOCK
  ssync();
}

void SpeedUART(unsigned int dwSpeed)
{
  *pUART_LCR=DLAB | WLS(8);
  dwSpeed=SCLK/(dwSpeed << 4);
  *pUART_DLH=(dwSpeed >> 8) & 0xff;
  *pUART_DLL=dwSpeed & 0xff;
  *pUART_LCR=WLS(8);
//  *pUART_IER=ERBFI; // ���������� ���������� �� ������
  ssync();
}

void SendErrFromUart(unsigned char cRxByte,unsigned char *pInBufUart,unsigned char ErrCode)
{
/*  pInBufUart[0]=ErrCode;
  pInBufUart[1]=3;
  pInBufUart[2]=cRxByte;
  CountErrNoReplyUart++;
  s_btUARTState=0;
  xQueueSendFromISR(xRxedChars,pInBufUart,pdFALSE,0);*/
}

// ��������� ���������� UART �� ������
void vCOM_1_Rx_ISR(void)
{
  static unsigned char btRxByte;
//  static unsigned char LenGet=0,Num=0;
//  static unsigned char pInBufUart[d_UARTRcvBufSz]; // ����� ��� ������ ������ �� ������� �� ���

  btRxByte=*pUART_RBR;
  if(*pUART_LSR);
  ssync();

/*  switch(s_btUARTState)
  {
    case 0:
      pInBufUart[0]=s_btUartCmd;
      if(cRxByte==s_btUartCmd) s_btUARTState=1; // ����� �� ������� ������
      else SendErrFromUart(cRxByte,pInBufUart,ErrAnswBadCmd);
      break;
    case 1:
    case 2:
      LenGet=cRxByte;
      if(LenGet<3) SendErrFromUart(cRxByte,pInBufUart,ErrAnswBadLen);
      else
      {
        pInBufUart[1]=LenGet;
        Num=2;
        s_btUARTState=3;
      }
      break;
    case 3:
      pInBufUart[Num++]=cRxByte;
      if(Num==LenGet)
      {
        s_btUARTState=0;
        xQueueSendFromISR(xRxedChars,pInBufUart,0,0);
      }
      break;
    default:
      SendErrFromUart(cRxByte,pInBufUart,ErrInReply);
      break;
  }*/
}

// ��������� ���������� UART �� ��������
void vCOM_1_Tx_ISR(void)
{ // ����� ���������� �� �������� ������� ��� ��� ����������� ��������� ����������
  *pUART_IER&=~ETBEI; // ���������� ���������� �� ��������
  ssync();
}

void StopWaitReplyUART(void)
{
  static unsigned int dwIRQ;
  dwIRQ=cli();
  s_btUARTState=0;
  sti(dwIRQ);
}

static inline unsigned short GetUartLSR(void)
{
  static unsigned short wLSR;
  
  wLSR=*pUART_LSR;
  ssync();
  return(wLSR);
}

#define d_UartDelay     2500ul
void UartDelay(void)
{ // �������� ����� �������, ����� ��� ������� �� ��������� (1 ���� ������� ==20 ����������)
  unsigned int dwStart,dwStop;
  bool bGE;

  do
  { // ����, ���� ������ �������� ����� 0
    dwStart=(*pTIMER0_COUNTER);
    ssync();
    dwStop=dwStart+d_UartDelay;
  }
  while(dwStop<dwStart);
  dwStop=dwStart+d_UartDelay;
  if(dwStart>=d_UartDelay) bGE=true;
  else bGE=false;
  do
  {
    dwStart=(*pTIMER0_COUNTER);
    ssync();
    if(dwStop<dwStart) break;
    if((dwStart<d_UartDelay) && bGE) break;
  }
  while(1);
}

bool xPutUART(unsigned int dwSpeed,unsigned char *btOut,int iSz)
{
  static unsigned int xLastWakeTime,i;

//SendStr2IpLogger("$DEBUG$xUARTPutCmd: start...");
  *pUART_IER=0; // ������ ����������
  xLastWakeTime=xTaskGetTickCount()+10;
  while(!(GetUartLSR() & TEMT))
  { // ���� ������������ ����������� UART
    if(IsCurrentTickCountGT(xLastWakeTime))
    { xSemaphoreGive(s_UartLock); return(false); }
  }
  SpeedUART(dwSpeed);
  s_btUARTState=0;
  ssync();
//SendStr2IpLogger("$DEBUG$xUARTPutCmd: send packet start (iSz=%d)",iSz);
  for(i=0;i<iSz;i++)
  {
    *pUART_THR=btOut[i];
    ssync();
    if(i<iSz-1)
    {
      xLastWakeTime=xTaskGetTickCount()+10;
      while(!(GetUartLSR() & TEMT))
      {
        if(IsCurrentTickCountGT(xLastWakeTime))
        { xSemaphoreGive(s_UartLock); return(false); }
      }
      UartDelay();
    }
  }
//  *pUART_IER|=ERBFI; // ���������� ���������� �� ������
//SendStr2IpLogger("$DEBUG$xUARTPutCmd: finish!");
  return true;
}

bool PutUART(unsigned int dwSpeed,unsigned char *btOut,int iSz)
{
  bool bRet;
  xSemaphoreTake(s_UartLock);
  bRet=xPutUART(dwSpeed,btOut,iSz);
  xSemaphoreGive(s_UartLock);
  return bRet;
}

// ��������� ���������� �� Timer0, Uart
void ISR_SinfoIVG10(void)
{
  unsigned char i;
  static unsigned short s_wTmTakt=0;

  // ���������� �� Uart /////////////////////////////////////////////
  i=*pUART_IIR; //����������� ��������� ����������
  ssync();
  i&=0x7;
  switch(i)
  {
    case 0x06:
      i=*pUART_LSR;
      break;
    case 0x04:
      vCOM_1_Rx_ISR();
      break;
    case 0x02:
      vCOM_1_Tx_ISR();
      break;
  }

  // ���������� �� ������� //////////////////////////////////////////
  i=*pTIMER_STATUS;
  ssync();
  if(i & 0x0001)
  {
    if(s_wTmTakt<1000) s_wTmTakt++;
    else
    { Time_Sys++; s_wTmTakt=0; }
    vTickISR();
    *pFIO_FLAG_T=PF0; // toogle watchdog
    *pTIMER_STATUS = 0x0001; // confirm interrupt handling
    ssync();
  }
}

// ��������� ���������� IVG10 (Timer0)
void ISR_InterrIVG10(void);
void ISR_InterrIVG10Container(void)
{
  asm volatile ( ".global _ISR_InterrIVG10;");
  asm volatile ( ".extern _ISR_SinfoIVG10;");
  asm volatile ( "_ISR_InterrIVG10:");

  asm volatile ( "[--SP] = ASTAT;");
  asm volatile ( "[--SP] = FP;");
  asm volatile ( "[--SP] = RETS;" );
  asm volatile ( "CALL _CpuSaveContext;");

  asm volatile ( "CALL _ISR_SinfoIVG10;");

  asm volatile ( "CALL _CpuRestoreContext;");
  asm volatile ( "RETS = [SP++];" );
  asm volatile ( "FP = [SP++];" );
  asm volatile ( "ASTAT = [SP++];" );
  asm volatile ( "RTI;" );
}

// ��������� ���������� IVG8 (SPORT)
void ISR_InterrIVG8(void);
void ISR_InterrIVG8Container(void)
{
  asm volatile ( ".global _ISR_InterrIVG8;");
  asm volatile ( ".extern _Sport_Int;");

  asm volatile ( "_ISR_InterrIVG8:");
  asm volatile ( "[--SP] = ASTAT;");
  asm volatile ( "[--SP] = FP;");
  asm volatile ( "[--SP] = RETS;" );
  asm volatile ( "CALL _CpuSaveContext;");

  asm volatile ( "CALL _Sport_Int;");

  asm volatile ( "CALL _CpuRestoreContext;");
  asm volatile ( "RETS = [SP++];" );
  asm volatile ( "FP = [SP++];" );
  asm volatile ( "ASTAT = [SP++];" );
  asm volatile ( "RTI;" );
}

// ��������� �������������� �������� ////////////////////////////////
void ISR_Exclude_Sinfo(void)
{
  unsigned int mySEQSTAT;

  mySEQSTAT=sysreg_read(reg_SEQSTAT) & 0x3f;
  if(mySEQSTAT==0x25) return;  // ��������� ����������, �� ����������
  sysreg_read(reg_RETX);
}

void ISR_Exclude(void);
void ISR_Exclude_Container(void)
{
  asm volatile ( ".extern _CpuRestoreContext;");
  asm volatile ( ".extern _CpuSaveContext;");
  asm volatile ( "_ISR_Exclude:");
  asm volatile ( ".global _ISR_Exclude;");
  asm volatile ( ".global _CpuSaveContext;");
  asm volatile ( ".global _CpuRestoreContext;");

  asm volatile ( "[--SP] = ASTAT;");
  asm volatile ( "[--SP] = FP;");
  asm volatile ( "[--SP] = RETS;" );
  asm volatile ( "CALL _CpuSaveContext;");
  asm volatile ( "[--SP] = RETX;");

  asm volatile ( "CALL _ISR_Exclude_Sinfo;");

  asm volatile ( "RETX = [SP++];" );
  asm volatile ( "CALL _CpuRestoreContext;");
  asm volatile ( "RETS = [SP++];" );
  asm volatile ( "FP = [SP++];" );
  asm volatile ( "ASTAT = [SP++];" );

  asm volatile ( "[--SP] = RETS;" );
  asm volatile ( "RETX = [SP++];" );
  asm volatile ( "CLI R0;" );
  asm volatile ( "idle;" );
  asm volatile ( "RTX;" );
}

