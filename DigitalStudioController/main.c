#include "Sinfo.h"
#include "GlobalData.h"

/////////////////////////////////////////////////////////////////////
#define d_ID_USERHEAP       1

/////////////////////////////////////////////////////////////////////
void *pvPortMalloc(size_t xWantedSize);

void ISR_Exclude(void);
void ISR_InterrIVG8(void);
void ISR_InterrIVG10(void);

void LedInit(void);
void CreateQueueForTask(void);

void vIPTaskM(void *pvParameters);
void vViewCommandTask(void *pvParameters);
void vTFTPTask(void *pvParameters);
void SportsTask(void *pvParameters);
void VCSTask(void *pvParameters);
//void EtherCatTask(void *pvParameters);

void InitUART(void);

/////////////////////////////////////////////////////////////////////
extern int g_iHeapIndex;
extern unsigned int ldf_heap_user_base,ldf_heap_user_size;
extern unsigned int ldf_super_fast_base,ldf_super_fast_size;

/////////////////////////////////////////////////////////////////////
// ���������� �-��� (����������� ���� � ��������������� WD)
void ToogleWD_Main(void)
{
  unsigned i;

  cli();
  while(1)
  {
    for(i=0;i<0x4;i++)
    {
      *pFIO_FLAG_S=PF0;
      ssync();
    }
    *pFIO_FLAG_C=PF0;
    ssync();
    for(i=0;i<0xff;i++)
    {
      *pFIO_FLAG_C=PF0;
      ssync();
    }
  }
}

/////////////////////////////////////////////////////////////////////
// ����� IDLE ��� RTOS
void vApplicationIdleHook(void)
{ idle(); }

// ������ � ������������ SDRAM //////////////////////////////////////
#define L1_IMEMORY          (PAGE_SIZE_1MB | CPLB_LOCK  | CPLB_VALID)
#define SDRAM_IGENERIC      (PAGE_SIZE_4MB | CPLB_L1_CHBL | CPLB_USER_RD | CPLB_VALID)

static const int I_cplb_cnt=3;

static unsigned int I_cplb_addrs[16] =
{
	0xFFA00000, 
	0x00000000,
	0x00400000,
};

static unsigned int I_cplb_data[16] =
{
	L1_IMEMORY,
	SDRAM_IGENERIC,
	SDRAM_IGENERIC
};

// Lock Control for the ICache: 1-Lock the way, 0-Don't Lock
int Lock_Control[4]=
{0,0,0,0};

/****************************************************************/
/*	Routine for Configuring the ICPLBs and Enabling Instr Cache	*/
/*																*/
/*	Parameters:													*/
/*																*/
/*	cplb_addr : Pointer to the Page Table for ICPLB_ADDR		*/
/*	cplb_data : Pointer to the Page Table for ICPLB_DATA		*/
/*	cplb_cnt  : Number of CPLB entries to be configured			*/
/*	no_lru	  : The modified LRU policy disabled if (no_lru==1) */
/*	W_Lock	  : Way-n will be locked if ( *(W_Lock + n) == 1 )	*/
/*																*/
/*																*/
/*									Initial Version				*/
/*									Kunal Singh					*/
/*									DSP applications, ADI		*/
/*									Dated 3 April, 2005			*/
/****************************************************************/
void Config_I_Cache(unsigned int *cblb_addr, unsigned int *cplb_data, int cplb_cnt, int no_lru, int *W_Lock)
{
  int temp;
  unsigned int imem_cntrl;

  // CPLBs/Cache disabled, invalidate Cache
  *pIMEM_CONTROL = 0x1;
  csync();

  // Configure the ICPLB_ADDR [n]
  for (temp=0; temp<cplb_cnt; temp++)
  { *((unsigned int *)(pICPLB_ADDR0+temp))=*(cblb_addr + temp); csync(); }

  // Configure the ICPLB_DATA [n]
  for(temp=0;temp<cplb_cnt;temp++)
  { *((unsigned int *)(pICPLB_DATA0+temp))=*(cplb_data+temp); csync(); }
  imem_cntrl = 0x7; // Enable the ICache and ICPLBs

  // Select the LRU Policy
  if(no_lru == 1) imem_cntrl |= 0x2000; // Disable the Modified LRU policy
    
  // Select the Way Locking
  if(*W_Lock == 1) imem_cntrl |= 0x8; // Lock Way 0
  if(*(W_Lock+1)==1) imem_cntrl |= 0x10; // Lock Way 1
  if(*(W_Lock+2)==1) imem_cntrl |= 0x20; // Lock Way 2
  if(*(W_Lock+3)==1) imem_cntrl |= 0x40; // Lock Way 3

  // Enable the Instruction Cache and CPLBs
  *pIMEM_CONTROL = imem_cntrl;
  ssync();
}

void InitInterrupts(void)
{
  // assign core IDs to interrupts
  *pSIC_IAR0=IVG_PLL_WAKEUP(22) | IVG_DMA_ERROR(22) | IVG_PPI_ERROR(22) | IVG_SPT0_ERROR(22) |
             IVG_SPI_ERROR(22) | IVG_SPT1_ERROR(22) |IVG_UART_ERROR(22) | IVG_RTC(22);
  *pSIC_IAR1=IVG_PPI(22) | IVG_SPT0_RX(8) | IVG_SPT0_TX(22) | IVG_SPT1_TX(22) | IVG_SPT1_RX(22) | 
             IVG_SPI(22) | IVG_UART_RX(10) | IVG_UART_TX(22);
  *pSIC_IAR2=IVG_TIMER0(10) |IVG_TIMER1(22) | IVG_TIMER2(22) | IVG_PFA(22) | IVG_PFB(22) |
             IVG_MEMDMA0(22) | IVG_MEMDMA1(22) | IVG_SWDT(22);

  *pEVT3=ISR_Exclude;       // �������� ����������� ����������
  *pEVT8=ISR_InterrIVG8;    // �������� ����������� ���������� SPORT
  *pEVT10=ISR_InterrIVG10;  // ������ ���������� 10

  // ���������� ������� ���������� ������� 
  *pSIC_IMASK=IRQ_TIMER0 | IRQ_SPT0_RX /*| IRQ_UART_ERROR | IRQ_UART_RX*/ ;
}

/////////////////////////////////////////////////////////////////////
unsigned int GetSCLK(void)
{ return SCLK; }

void main(void)
{
  sysreg_write(reg_SYSCFG,0x30); //Initialize System Configuration Register
  csync();

  memset(0,0xff,0x1000);
  memset(&ldf_super_fast_base,0,(unsigned int)(&ldf_super_fast_size));

  *pFIO_DIR= ~PF6; // ��������� �� ����� ���� PFx, ����� PF6
  *pFIO_INEN= PF6; // ���������� ����� PF6
  *pFIO_FLAG_S = 0xffff; // ��������� �������� ������ �� ���� PFx
  *pFIO_FLAG_C = PF0; // ����� ���� ���������� ��������
  ssync();

*pPLL_DIV=((*pPLL_DIV) & 0xfff0) | 5; // SCLK=CCLK/5 (491520/5=98304)
ssync();

*pPLL_CTL=0x8000 | (*pPLL_CTL); // set SPORT input pin hysteresis bit
ssync();
//idle();

  // ������������� ���� SDRAM
  Config_I_Cache(I_cplb_addrs, I_cplb_data, I_cplb_cnt, 0, Lock_Control);

  g_iHeapIndex=heap_install(&ldf_heap_user_base,(unsigned int)(&ldf_heap_user_size),d_ID_USERHEAP);
  if(g_iHeapIndex<0) BlockWatchDog();
  _heap_init(g_iHeapIndex);

  InitInterrupts();
  InitUART();
  Init_SPI();
  LedInit();

  CreateQueueForTask();
  if(!InitFileSystem()) BlockWatchDog();
  if(xTaskCreate(vIPTaskM,"IpTaskM",
                 STACK_IP,NULL,tskIDLE_PRIORITY+2,&thIpM)!=pdPASS)
  { BlockWatchDog(); }

  if(xTaskCreate(vViewCommandTask,"ViewCommand",
                 STACK_ViewCommand,NULL,tskIDLE_PRIORITY+1,thViewCmd)!=pdPASS)
  { BlockWatchDog(); }

  if(xTaskCreate(vTFTPTask,"TFTP",
                 STACK_TFTP,NULL,tskIDLE_PRIORITY+1,&thTFTP)!=pdPASS)
  { BlockWatchDog(); }

  if(xTaskCreate(SportsTask,"Sports",
                 STACK_Sports,NULL,tskIDLE_PRIORITY+3,&thSports)!=pdPASS)
  { BlockWatchDog(); }

  if(xTaskCreate(VCSTask,"VCS",
                 STACK_VCS,NULL,tskIDLE_PRIORITY+1,&thVCS)!=pdPASS)
  { BlockWatchDog(); }

/*  if(xTaskCreate(EtherCatTask,"EtherCat",
                 STACK_ETHERCAT,NULL,tskIDLE_PRIORITY+2,&thEthCat)!=pdPASS)
  { BlockWatchDog(); }*/

//ToogleWD_Main();

  *pIMASK|=0x400; // ���������� ���������� 10
  while(1) vTaskStartScheduler(); // � ����� ����� ����������� ������!
}

