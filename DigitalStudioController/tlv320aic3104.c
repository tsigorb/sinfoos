#include "Sinfo.h"

//#define ADSP_MASTER

/////////////////////////////////////////////////////////////////////
const unsigned char s_btMasAIC104Init[]=
{
  0,0x00, // set page 0
//  1,0x80, // soft reset
/* MCLK = 12 MHz and fS(ref) = 48 kHz
Select P = 1, R = 1, K = 8.192, which results in J=8, D=1920 */
101,0x00, // CODEC_CLKIN uses PLLDIV_OUT
102,0x02, // PLLDIV_IN uses MCLK
  2,0x44, // codec sample rate 16kHz (Fs_ref/3)
  3,0x81, // PLL enable, P=1
 11,0x01, // PLL R=1
  4,0x20, // J=8
  5,0x1e, //-\
//  6,0x00, //-/ -> D=1920
6,0x04, // D= - ������ IPPM ����� �� �����
  7,0x0a, // Left-DAC data path plays left-channel input data, Right-DAC data path plays right-channel input data
  8,0x20, // Place DOUT in high-impedance state when valid data is not being sent
          // or set codec to master mode (in programm code!)
//  9,0x48, // Serial data bus uses DSP mode 256 bit
//  9,0xc0, // Serial data bus uses left-justified mode
  9,0x00, // Serial data bus uses I2S mode
 10,0x00, // ������� ��������� ���������� ������ BCLK
 12,0x50, // ADC high-pass filter config (0.0045*Fs), DAC digital effect filter disable
  // ADC PGA control (D7==1 - mute, D6..D0 - 1 step==0.5db)
 15,0x00,
 16,0x00,
  // ADC config (D7==1 - diff mode, D2==1 - ADC power up)
 19,0x84,
 22,0x84,
/* 19,0x04,
 22,0x04,*/
  // AGC config left
// 26,0xbf,           // AGC enable, -12 dB target level, attack tm=20ms, decay tm=500ms
 26,0x3f,           // AGC disable, -12 dB target level, attack tm=20ms, decay tm=500ms
103,0xc8,           // uses this attack tm=40ms
104,0xcc,           // uses this decay tm=250ms*8
// 27,0xba,           // AGC max gain=46,5dB
 27,0x00,           // AGC max gain=0dB
 28,0xc0,           // Noise gate gisteresis disable, AGC noise/silence detection disabled
 34,0x00,
  // AGC config rigth
// 29,0xbf,           // AGC enable, -12 dB target level, attack tm=20ms, decay tm=500ms
 29,0x3f,           // AGC disable, -12 dB target level, attack tm=20ms, decay tm=500ms
105,0xc8,           // uses this attack tm=40ms
106,0xcc,           // uses this decay tm=250ms*8
// 30,0xba,           // AGC max gain=46,5dB
 30,0x00,           // AGC max gain=0dB
 31,0xc0,           // Noise gate gisteresis disable, AGC noise/silence detection disabled
 35,0x00,           // Noise Gate Debounce

 37,0xc0,            // DAC power ctrl
 41,0x50,            // D7D6 00: Left-DAC output selects DAC_L1 path to PGA_L
                     //      01: Left-DAC output selects DAC_L3 path to left line output driver
                     //      10: Left-DAC output selects DAC_L2 path to left high-power output drivers
                     //      11: reserved
                     // D6D4 00: Right-DAC output selects DAC_L1 path to PGA_R
                     //      01: Right-DAC output selects DAC_L3 path to right line output driver
                     //      10: Right-DAC output selects DAC_L2 path to right high-power output drivers
                     //      11: reserved
  // DAC volume ctrl (1 step==-0.5db)
 43,0,
 44,0,

 82,0x80,            // DAC_L to L_LOPM
 86,0x08,            // L_LOPM unmute
 92,0x80,            // DAC_R to R_LOPM
 93,0x08,            // R_LOPM unmute

107,0x30,            // Left and right analog microphone used

//108,0x33,            // Left and right bypass

  // ����� �������� ==255 - ����� ������� �������������
255,00
};

/////////////////////////////////////////////////////////////////////
static xSemaphoreHandle s_I2CLock=NULL;

static unsigned short s_wSCL=PF14;
static bool s_bCodecInit=false;

/////////////////////////////////////////////////////////////////////
void Delay_us(unsigned int uSec)
{
  unsigned short i,wPF;
  do
  {
    for(i=0;i<26;i++)
    {
      wPF=*pFIO_FLAG_D;
      ssync();
    }
    uSec--;
  }
  while(uSec);
}

/*void TestDelay(void)
{
  while(1)
  {
    *pFIO_FLAG_T=PF13;
    ssync();
    Delay_us(2);
  }
}*/

void SetSCL(unsigned short wSCL)
{ s_wSCL=wSCL; }

void I2C_Start(void)
{
  *pFIO_FLAG_C=PF13; // SDA low
  ssync();
  Delay_us(2);
  *pFIO_FLAG_C=s_wSCL; // SCL low
  ssync();
  Delay_us(2);
}

void I2C_Stop(void)
{
  Delay_us(2);
  *pFIO_FLAG_C=PF13; // SDA low
  ssync();
  Delay_us(2);
  *pFIO_FLAG_S=s_wSCL; // SCL hi
  ssync();
  Delay_us(2);
  *pFIO_FLAG_S=PF13; // SDA hi
  ssync();
}

unsigned short I2C_Write(unsigned char btData)
{
  unsigned short ic1,wACK;

  for(ic1=0;ic1<8;ic1++)
  {
    if(btData & 0x80) *pFIO_FLAG_S=PF13; // SDA hi
    else *pFIO_FLAG_C=PF13; // SDA low
    ssync();
    Delay_us(2);
    btData<<=1;
    *pFIO_FLAG_S=s_wSCL; // SCL hi
    ssync();
    Delay_us(2);
    *pFIO_FLAG_C=s_wSCL; // SCL low
    ssync();
    Delay_us(2);
  }
  *pFIO_DIR= ~(PF6 | PF13); // ��������� �� ����� ���� PFx, ����� PF6 � PF13
  *pFIO_INEN= PF6 | PF13; // ���������� ������ PF6 � PF13
  ssync();
  *pFIO_FLAG_S=s_wSCL; // SCL hi
  ssync();
  Delay_us(2);
  wACK=*pFIO_FLAG_D & PF13;
  ssync();
  *pFIO_FLAG_C=s_wSCL; // SCL low
  ssync();
  Delay_us(2);
  *pFIO_INEN= PF6; // ���������� ����� PF6
  *pFIO_DIR= ~PF6; // ��������� �� ����� ���� PFx, ����� PF6
  ssync();
  return(wACK);
}

void Init_TLV320AIC104(void)
{
  int iCodec,iIndex,iErr=0;
  unsigned char btData;

  if(!s_I2CLock) vSemaphoreCreateBinary(s_I2CLock);
  xSemaphoreTake(s_I2CLock);
  SetSCL(PF14);
  for(iCodec=0;iCodec<2;iCodec++)
  {
    iIndex=0;
    while(s_btMasAIC104Init[iIndex]!=255)
    {
      I2C_Start();
      if(I2C_Write(0x30)) // I2C address codec (0x18) << 1
      { iErr=1; break; }
      btData=s_btMasAIC104Init[iIndex];
      if(I2C_Write(btData))
      { iErr=2; break; }
#ifdef ADSP_MASTER
      btData=s_btMasAIC104Init[iIndex+1];
#else
      if((iCodec==1) && (btData==8)) btData=0xf0; // 2-� ����� �������
      else btData=s_btMasAIC104Init[iIndex+1];
#endif
      if(I2C_Write(btData))
      { iErr=3; break; }
      I2C_Stop();
      Delay_us(2);
      iIndex+=2;
    }
    if(iErr)
    {
      I2C_Stop();
      xSemaphoreGive(s_I2CLock);
      s_bCodecInit=true;
      SendStr2IpLogger("$ERROR$Init_TLV320AIC104 error=%d for codec=%d",iErr,iCodec);
      return;
    }
    SetSCL(PF15);
  }
  xSemaphoreGive(s_I2CLock);
  s_bCodecInit=true;
  SendStr2IpLogger("$ERROR$Init_TLV320AIC104 OK!");
}

void SetAdcGain4Ch(char Chan,unsigned char btCode)
{ // 0..119, 1 step == 0.5dB
  unsigned char btReg;

  if((Chan<1) || (Chan>4)) return;
  if(btCode>119) btCode=119;
  while(!s_bCodecInit) vTaskDelay(10);
  xSemaphoreTake(s_I2CLock);
  if(Chan<3) SetSCL(PF14);
  else SetSCL(PF15);
  I2C_Start();
  if(I2C_Write(0x30)==0) // I2C address codec (0x18) << 1
  {
    if(Chan & 1) btReg=15;
    else btReg=16;
    if(I2C_Write(btReg)==0) I2C_Write(btCode);
  }
  I2C_Stop();
  xSemaphoreGive(s_I2CLock);
}

void SetDacVol4Ch(char Chan,unsigned char btCode)
{ // 0..127, 1 step == -0.5dB
  unsigned char btReg;

  if((Chan<1) || (Chan>4)) return;
  if(btCode>127) btCode=127;
  while(!s_bCodecInit) vTaskDelay(10);
  xSemaphoreTake(s_I2CLock);
  if(Chan<3) SetSCL(PF14);
  else SetSCL(PF15);
  I2C_Start();
  if(I2C_Write(0x30)==0) // I2C address codec (0x18) << 1
  {
    if(Chan & 1) btReg=43;
    else btReg=44;
    if(I2C_Write(btReg)==0)
    {
      I2C_Write(btCode);
    }
  }
  I2C_Stop();
  xSemaphoreGive(s_I2CLock);
}

/////////////////////////////////////////////////////////////////////
extern short RxBuf0[d_LenBuf],RxBuf1[d_LenBuf]; // receive buffers SPORT0 DMA1 and SPORT1 DMA3 
extern short TxBuf0[d_LenBuf],TxBuf1[d_LenBuf]; // transmit buffers SPORT0 DMA2 and SPORT1 DMA4 

#define RDTYPE_B3B2  0x00  // B3B2: 00 - Zero fill, 01 - Sign-extend, ...

void Init_Sport0(void)
{
  // Set SPORT0 in Multichannel mode
  *pSPORT0_MCMC2=0x0000;    // Multichannel disable
  ssync();
/*  *pSPORT0_MCMC1=0x0000;    // Window Size 8, Offset 0
  ssync();

  // set 1 channels TX
  *pSPORT0_MTCS0=0x00000003;
  *pSPORT0_MTCS1=0x00000000;
  *pSPORT0_MTCS2=0x00000000;
  *pSPORT0_MTCS3=0x00000000;
  ssync();

  // set 2 channels RX
  *pSPORT0_MRCS0=0x00000003;
  *pSPORT0_MRCS1=0x00000000;
  *pSPORT0_MRCS2=0x00000000;
  *pSPORT0_MRCS3=0x00000000;
  ssync();*/
  
  // Sport0, �����
  *pSPORT0_RCLKDIV=0;
  *pSPORT0_RFSDIV=0;
  *pSPORT0_RCR1= RFSR | RCKFE;              // ������� CLK, ������� RFS, RFSR and RCKFE for I2S
  *pSPORT0_RCR2=SLEN_16 | RXSE | RSFSE;     // 16 ���, SEC enabled, FS in l/r stereo mode
  ssync();

  // Sport0, ��������
#ifdef ADSP_MASTER
  *pSPORT0_TCLKDIV=47;       // for SCLK=98304
  *pSPORT0_TFSDIV=31;
  *pSPORT0_TCR1 = ITFS | ITCLK | TFSR | TCKFE; // ���������� RFS, ���������� CLK, TFSR and TCKFE for I2S
#else
  *pSPORT0_TCLKDIV=0;
  *pSPORT0_TFSDIV=0;
  *pSPORT0_TCR1 = TFSR | TCKFE;           // ������� CLK, ������� RFS, TFSR and TCKFE for I2S
#endif
  *pSPORT0_TCR2 = SLEN_16 | TXSE | TSFSE; // 16 ���, SEC enabled, FS in l/r stereo mode
  ssync();

}

void Init_Sport1(void)
{
  // Set SPORT1 in Multichannel mode
  *pSPORT1_MCMC2=0x0000;    // Multichannel disable
  ssync();
/*  *pSPORT1_MCMC1=0x0000;    // Window Size 8, Offset 0
  ssync();

  // set 2 channels TX
  *pSPORT1_MTCS0=0x00000003;
  *pSPORT1_MTCS1=0x00000000;
  *pSPORT1_MTCS2=0x00000000;
  *pSPORT1_MTCS3=0x00000000;
  ssync();

  // set 2 channels RX
  *pSPORT1_MRCS0=0x00000003;
  *pSPORT1_MRCS1=0x00000000;
  *pSPORT1_MRCS2=0x00000000;
  *pSPORT1_MRCS3=0x00000000;
  ssync();*/

  // Sport1, �����
  *pSPORT1_RCLKDIV=0;
  *pSPORT1_RFSDIV=0;
  *pSPORT1_RCR1= RFSR | RCKFE;          // ������� CLK, ������� RFS, RFSR and RCKFE for I2S
  *pSPORT1_RCR2=SLEN_16 | RXSE | RSFSE; // 16 ���, SEC enabled, FS in l/r stereo mode
  ssync();
  
  // Sport1, ��������
  *pSPORT1_TCLKDIV=0;
  *pSPORT1_TFSDIV=0;
  *pSPORT1_TCR1 = TFSR | TCKFE;    // ������� CLK, ������� RFS, TFSR and TCKFE for I2S
  *pSPORT1_TCR2 = SLEN_16 | TXSE | TSFSE; // 16 ���, SEC enabled, FS in l/r stereo mode
  ssync();
}

void Init_DMA12(void)
{
  // ��������� DMA1 �� ����� �� Sport0
  // ���������������, ������. �� ���������� ������� ����� ������, 1-������ �����
  // 16-�� ��������� ��������, ������ � ������ �� ����������
  *pDMA1_PERIPHERAL_MAP = 0x1000;
  *pDMA1_CONFIG = FLOW_1 | WDSIZE_16 | WNR | DI_EN;
  *pDMA1_START_ADDR = RxBuf0; // ��������� �����
  *pDMA1_X_COUNT = 4;         // ���������� ������� ����
  *pDMA1_X_MODIFY= 2;         // ����������� ������ ����������� �������� � ������
  ssync();

  // ��������� DMA2 �� �������� �� Sport0
  // ���������������, 1-������ �����
  // 16-�� ��������� ��������
  *pDMA2_PERIPHERAL_MAP = 0x2000;
  *pDMA2_CONFIG = FLOW_1 | WDSIZE_16;
  *pDMA2_START_ADDR = TxBuf0; // ��������� �����
  *pDMA2_X_COUNT = 4;         // ���������� ������� ����
  *pDMA2_X_MODIFY = 2;        // ����������� ������ ����������� �������� � ������
  ssync();
}

void Init_DMA34(void)
{
  // ��������� DMA3 �� ����� �� Sport1
  // ���������������, 1-������ �����
  // 16-�� ��������� ��������, ������ � ������ �� ����������
  *pDMA3_PERIPHERAL_MAP = 0x3000;
  *pDMA3_CONFIG = FLOW_1 | WDSIZE_16 | WNR;
  *pDMA3_START_ADDR = RxBuf1; // ��������� �����
  *pDMA3_X_COUNT = 4;         // ���������� ������� ����
  *pDMA3_X_MODIFY= 2;         // ����������� ������ ����������� �������� � ������
  ssync();

  // ��������� DMA4 �� �������� �� Sport1
  // ���������������, 1-������ �����
  // 16-�� ��������� ��������
  *pDMA4_PERIPHERAL_MAP = 0x4000;
  *pDMA4_CONFIG = FLOW_1 | WDSIZE_16;
  *pDMA4_START_ADDR = TxBuf1; // ��������� �����
  *pDMA4_X_COUNT = 4;         // ���������� ������� ����
  *pDMA4_X_MODIFY= 2;         // ����������� ������ ����������� �������� � ������
  ssync();
}

void Enable_DMA_Sport0(void)
{
  int i;
  unsigned int dwIRQ=cli();

  for(i=0;i<d_LenBuf;i++) TxBuf0[i]=0;

  // enable DMAs
  *pDMA2_CONFIG |= DMAEN;
  *pDMA1_CONFIG |= DMAEN;
  ssync();
  
  //���������� ������ /�������� SPORT0
  *pSPORT0_RCR1 |= RSPEN;
  *pSPORT0_TCR1 |= TSPEN;
  ssync();
  
  sti(dwIRQ);
}

/*void Disable_DMA_Sport0(void)
{
  unsigned int dwIRQ=cli();
  // disable DMAs
  *pDMA2_CONFIG &= ~DMAEN;
  *pDMA1_CONFIG &= ~DMAEN;
  ssync();
  // disable SPORT
  *pSPORT0_TCR1 &= ~TSPEN;
  ssync();
  *pSPORT0_RCR1 &= ~RSPEN;
  ssync();
  sti(dwIRQ);
}

void RestartSPORT0(void)
{
//SendStr2IpLogger("RestartSPORT0");
  Disable_DMA_Sport0();
  Enable_DMA_Sport0();
}*/

void Enable_DMA_Sport1(void)
{
  int i;
  unsigned int dwIRQ=cli();

  for(i=0;i<d_LenBuf;i++) TxBuf1[i]=0;

//  *pSPORT1_MCMC2=0x0010;    // Multichannel enable
  ssync();

  // enable DMAs
  *pDMA4_CONFIG |= DMAEN;
  *pDMA3_CONFIG |= DMAEN;
  ssync();

  //���������� ������ /�������� SPORT1
  *pSPORT1_RCR1 |= RSPEN;
  *pSPORT1_TCR1 |= TSPEN;
  ssync();

  sti(dwIRQ);
}

/*void Disable_DMA_Sport1(void)
{
  unsigned int dwIRQ=cli();
  // disable DMAs
  *pDMA3_CONFIG &= ~DMAEN;
  *pDMA4_CONFIG &= ~DMAEN;
  ssync();
  // disable SPORT
  *pSPORT1_TCR1 &= ~TSPEN;
  ssync();
  *pSPORT1_RCR1 &= ~RSPEN;
  ssync();
  sti(dwIRQ);
}

void RestartSPORT1(void)
{
//SendStr2IpLogger("RestartSPORT1");
  Disable_DMA_Sport1();
  Enable_DMA_Sport1();
}*/

void StartSPORTs(void)
{ // ����������������� SPORT
  *pDMA1_CONFIG=0; ssync();
  *pDMA2_CONFIG=0; ssync();
  *pDMA3_CONFIG=0; ssync();
  *pDMA4_CONFIG=0; ssync();

  *pSPORT0_TCR1=0; ssync();
  *pSPORT0_RCR1=0; ssync();
  *pSPORT1_TCR1=0; ssync();
  *pSPORT1_RCR1=0; ssync();

  Init_Sport0();
  Init_DMA12();

  Init_Sport1();
  Init_DMA34();

  Enable_DMA_Sport1();
  Enable_DMA_Sport0();

  *pIMASK |= 0x0100; // ���������� ������������ ���������� 8 (SPORT)
  ssync();
}

void StartAudio(void)
{
#ifdef ADSP_MASTER
  vTaskDelay(100);
  Init_TLV320AIC104();
  vTaskDelay(100);
  StartSPORTs();
#else
  StartSPORTs();
  vTaskDelay(100);
  Init_TLV320AIC104();
#endif
}

