#include "Sinfo.h"
#include "GlobalData.h"

///////////////////////////////////////////////////////////////////////////////
static LIST_UK_IPPM_TP *s_pListUK_IPPM[2]={NULL,NULL};

static unsigned short s_wListUK_IPPMId[2]={0,0};

///////////////////////////////////////////////////////////////////////////////
unsigned short _GetUK_IPPMId(unsigned char btIF)
{
  unsigned short wId=s_wListUK_IPPMId[btIF];
  s_wListUK_IPPMId[btIF]++;
  return(wId);
}

LIST_UK_IPPM_TP *ListUK_IPPM_AddItem(unsigned char btIF,LIST_UK_IPPM_TP *pItem0)
{
  LIST_UK_IPPM_TP *pItemCur;
  LIST_UK_IPPM_TP *pItem=(LIST_UK_IPPM_TP *)pvPortMalloc(sizeof(LIST_UK_IPPM_TP));

  if(!pItem) return(false);
  memset(pItem,0,sizeof(LIST_UK_IPPM_TP));
  if(pItem0) memcpy(pItem,pItem0,sizeof(LIST_UK_IPPM_TP));
  pItem->wId=_GetUK_IPPMId(btIF);
  pItem->next=NULL;
//SendStr2IpLogger("$DEBUG$ListUK_IPPM_AddItem: wCmdId=%d",pItem->wId);
  if(s_pListUK_IPPM[btIF])
  {
    pItemCur=s_pListUK_IPPM[btIF];
    while(pItemCur->next) pItemCur=pItemCur->next;
    pItemCur->next=pItem;
    pItem->prev=pItemCur;
  }
  else
  {
    pItem->prev=NULL;
    s_pListUK_IPPM[btIF]=pItem;
  }
  return(pItem);
}

void _ListUK_IPPM_DeleteItem(unsigned char btIF,LIST_UK_IPPM_TP *pItem0)
{
  if(!pItem0) return;
//SendStr2IpLogger("$DEBUG$ListUK_IPPM_DeleteItem: Numb=%d, cmdId=%d, repeat_cnt=%d",pItem0->Numb,pItem0->wId,pItem0->repeat_cnt);
  if(pItem0==s_pListUK_IPPM[btIF])
  {
    s_pListUK_IPPM[btIF]=pItem0->next;
    if(s_pListUK_IPPM[btIF]) s_pListUK_IPPM[btIF]->prev=NULL;
  }
  else
  {
    LIST_UK_IPPM_TP *pItemPrev,*pItemNext;

    pItemPrev=pItem0->prev;
    pItemNext=pItem0->next;
    pItemPrev->next=pItemNext;
    if(pItemNext) pItemNext->prev=pItemPrev;
  }
  vPortFree(pItem0);
}

LIST_UK_IPPM_TP *_ListUK_IPPM_FindItem(unsigned char btIF,unsigned short wId)
{
  LIST_UK_IPPM_TP *pItem;

  pItem=s_pListUK_IPPM[btIF];
  while(pItem)
  {
    if(pItem->wId==wId) return(pItem);
    pItem=pItem->next;
  }
  return(NULL);
}

bool _SendItem2UK_IPPM(unsigned char btIF,LIST_UK_IPPM_TP *pItem)
{
  pItem->repeat_cnt++;
//SendStr2IpLogger("$DEBUG$SendItem2IPPM: Numb=%d, cmdId=%d, repeat_cnt=%d",pItem->Numb,pItem->wId,pItem->repeat_cnt);
  if(btIF) btIF=UserIPPM;
  else btIF=UserUDP;
  xQueueSend(xQueueReply[btIF],&(pItem->sIK),0,0);
  pItem->dwTOut=xTaskGetTickCount()+d_UK_IPPM_CmdTOut;
  return true;
}

bool _IsEmptyQueueCmd4UK_CurrentIPPM(unsigned char btIF,LIST_UK_IPPM_TP *pItemCur)
{
  LIST_UK_IPPM_TP *pItem;

  pItem=s_pListUK_IPPM[btIF];
  do
  {
    if(pItem==pItemCur) return true;
    if(pItem->Numb==pItemCur->Numb) return false;
    pItem=pItem->next;
  }
  while(pItem);
  return true;
}

void ListUK_IPPM_Process(unsigned char btIF)
{
  LIST_UK_IPPM_TP *pItemDel,*pItemCur;

  pItemCur=s_pListUK_IPPM[btIF];
  while(pItemCur)
  {
    if(pItemCur->repeat_cnt)
    {
      if(IsCurrentTickCountGT(pItemCur->dwTOut))
      {
        if(pItemCur->repeat_cnt>=d_UK_IPPMMaxRepeat)
        {
          pItemDel=pItemCur;
          pItemCur=pItemCur->next;
          if(pItemDel->pFunc_UK_IPPMNoAnswer) pItemDel->pFunc_UK_IPPMNoAnswer(pItemDel);
          _ListUK_IPPM_DeleteItem(btIF,pItemDel);
          continue;
        }
        else
        {
          if(_IsEmptyQueueCmd4UK_CurrentIPPM(btIF,pItemCur))
          { _SendItem2UK_IPPM(btIF,pItemCur); return; }
        }
      }
    }
    else
    {
      if(_IsEmptyQueueCmd4UK_CurrentIPPM(btIF,pItemCur))
      { _SendItem2UK_IPPM(btIF,pItemCur); return; }
    }
    pItemCur=pItemCur->next;
  }
}

bool UK_IPPM_RcvAnsw(unsigned char btIF,unsigned short wCmdId,unsigned char *pbtAnsw)
{
  bool bRes=false;
  LIST_UK_IPPM_TP *pItem;

//SendStr2IpLogger("$DEBUG$IPPM_RcvAnsw: CmdId=%d",wCmdId);
  pItem=_ListUK_IPPM_FindItem(btIF,wCmdId);
  if(pItem)
  {
    bRes=true;
    if(pItem->pFunc_UK_IPPMAnswer) pItem->pFunc_UK_IPPMAnswer(pItem,pbtAnsw);
    _ListUK_IPPM_DeleteItem(btIF,pItem);
  }
  return bRes;
}

void ClearQueueCmdUK_IPPM(unsigned char btIF,unsigned char Numb)
{
  LIST_UK_IPPM_TP *pItemDel,*pItem;

  pItem=s_pListUK_IPPM[btIF];
  while(pItem)
  {
    pItemDel=pItem;
    pItem=pItem->next;
    if(pItemDel->Numb==Numb) _ListUK_IPPM_DeleteItem(btIF,pItemDel);
  }
}

