#include "Sinfo.h"
#include "SPort.h"

/////////////////////////////////////////////////////////////////////
short g_shSinTable[d_Fs/(2*d_FStep)]; // �������� ������� ������

static unsigned char DisableInp[d_LenBuf+d_MaxCPIn],
                     EnableOut[d_LenBuf+d_MaxCPOut];

// ������������������ ���������� ������ � �������� TxBuf � RxBuf:
// TxBuf[0] - Pri left
// TxBuf[1] - Sec left
// TxBuf[2] - Pri right
// TxBuf[3] - Sec right
short RxBuf0[d_LenBuf],RxBuf1[d_LenBuf], // receive buffers SPORT0 DMA1 and SPORT1 DMA3 
      TxBuf0[d_LenBuf],TxBuf1[d_LenBuf]; // transmit buffers SPORT0 DMA2 and SPORT1 DMA4

static short CPIn[d_MaxCPIn],
             CPOut[d_MaxCPOut];

static unsigned short Generator4Out[d_LenBuf+d_MaxCPOut],
                      Info4Gen[d_LenBuf+d_MaxCPOut];

static int s_iSumInp;

/////////////////////////////////////////////////////////////////////
void Init_SportsData(void)
{
  memset(CPIn,0,sizeof(CPIn));
  memset(DisableInp,0,sizeof(DisableInp));
  memset(EnableOut,1,sizeof(EnableOut));
  memset(Generator4Out,0,sizeof(Generator4Out));
  memset(Info4Gen,0,sizeof(Info4Gen));
}

// Chan 1..4 - ���������� ������, 5 - UK, 6.. - IPPM
void ChanInputEnable(unsigned char btChan,unsigned short wPar)
{
  unsigned int dwIRQ;
  if((btChan<1) || (btChan>d_LenBuf+d_MaxCPIn)) return;
  btChan--;
  dwIRQ=cli();
  if(wPar) DisableInp[btChan]=false;
  else DisableInp[btChan]=true;
  sti(dwIRQ);
}

// Chan 1..4 - ���������� ������, 5 - UK, 6 - multicast IPPM
void ChanOutputEnable(unsigned char btChan,unsigned short wPar)
{
  unsigned int dwIRQ;
  if((btChan<1) || (btChan>6)) return;
  btChan--;
  dwIRQ=cli();
  if(wPar) EnableOut[btChan]=true;
  else EnableOut[btChan]=false;
  sti(dwIRQ);
}

// Chan 1..4 - ���������� ������, 5 - UK, 6 - multicast IPPM
void StartGenerator(unsigned char btChan,unsigned short wGenMode)
{
  unsigned int dwIRQ;
  static bool bSRand=false;

  if((btChan<1) || (btChan>6)) return;
  btChan--;
  dwIRQ=cli();
  if((wGenMode & 0xff)==2)
  {
    Info4Gen[btChan]=1;  // ��� ���������� ������ ����
    if(!bSRand)
    { srand(xTaskGetTickCount()); bSRand=true; }
  }
  else Info4Gen[btChan]=0;                      // ��� ������
  Generator4Out[btChan]=wGenMode;
  sti(dwIRQ);
}

void StopGenerator(unsigned char btChan)
{
  unsigned int dwIRQ;
  if((btChan<1) || (btChan>6)) return;
  btChan--;
  dwIRQ=cli();
  Generator4Out[btChan]=0;
  sti(dwIRQ);
}

void GetSampleFromCPOut2OutBuf(void)
{
extern xOutputIpFromISR MasOutputIpFromISR[d_MaxCPOut];

  int i;
  xOutputIpFromISR *pAdr;

#define _ pAdr->
  pAdr=MasOutputIpFromISR;
  for(i=0;i<d_MaxCPOut;i++,pAdr++)
  {
    if( _ Chan)
    {
      if(_ Overrun) continue;
      if(_ Chan==2)
      { // for IPPM
        _ wIpBufs[_ NumBuf][_ Ind]=CPOut[1];
      }
      else
      { // for UK
        if(_ Ind & 1)
        { // �.�. Fdiscr UK == 8kHz, �� ����� ������� ����� 1
          unsigned char *pbtBuf=(unsigned char *)(_ wIpBufs[_ NumBuf]);
          pbtBuf+=sizeof(xRTPHeadUK) + (_ Ind >> 1);
          *pbtBuf=a_compress(CPOut[0]);
        }
      }
      _ Ind++;
      if(_ Ind >= d_LenSoundIPPM)
      {
        _ Ind=0;
        if(_ NewData) _ Overrun=1;
        else
        { _ NumBuf^=1; _ NewData=1; }
      }
    }
  }
#undef _
}

void GetSampleFromInBuf2CPIn(void)
{
extern xInputIp4ISR MasInputIp4ISR[d_MaxCPIn];

  int i;
  xInputIp4ISR *pAdr;

#define _ pAdr->
  pAdr=MasInputIp4ISR;
  for(i=0;i<d_MaxCPIn;i++,pAdr++)
  {
    if(_ Chan)
    {
      if(_ OverrunISR) continue;
      _ Takt4Pckt++;
      CPIn[_ Chan-1]=_ wIpBufs[_ NumBuf*d_LenSoundIPPM + _ Ind];
      if(_ Sync == 0) _ Ind++;  // ������� �������� ���������������
      else
      { // ����������� ��� ��������� �������� ���������������
        if(_ Takt4Pckt<250) _ Ind++;
        else 
        {
          _ Takt4Pckt=0;
          if(_ Sync > 0)
          { // ��������� �������
            _ Ind+=2;
          }
          else
          { // ���������� ������� 
          }
        }
      }
      if(_ Ind>=d_LenSoundIPPM)
      {
        _ Takt4Pckt=0;
        _ Ind=0;
        if(_ NewData) _ OverrunISR=1;
        else
        { _ NumBuf^=1; _ NewData=1; }
      }
    }
  }
#undef _
}

unsigned short GaloisLfsr(unsigned short lfsr)
{ return ((lfsr >> 1) ^ (0 - (lfsr & 1) & 0xb8)) & 0xff; }

/////////////////////////////////////////////////////////////////////
/*short round(double fX);

float g_fScale = 2.0f / 0xffffffff;
int g_x1 = 0x67452301;
int g_x2 = 0xefcdab89;


short WhiteNoise(float _fLevel) // Noiselevel (0.0 ... 1.0)
{
  short y;
  _fLevel *= g_fScale;
  g_x1 ^= g_x2;
  y=(short)round(g_x2 * _fLevel);
  g_x2 += g_x1;
  return y;
}
*/
/////////////////////////////////////////////////////////////////////

static bool s_bRand=false;
static short s_shRand;

void _OutSample(unsigned char Chan,short *pwOut,int iSub)
{
  int iOff;
  if(EnableOut[Chan])
  {
    switch(Generator4Out[Chan] & 0xff)
    { 
      case 1: // ����� d_FGen ��
//        if(g_shSinTable)
        {
          iOff=Info4Gen[Chan];
          if(iOff<(d_Fs/d_FStep)/2)
          { // ������������� ���������
/*            if(iOff>(d_Fs/d_FStep)/4)
            { // 2-� ��������
              iOff=(d_Fs/d_FStep)/2-iOff-1;
            }*/
            *pwOut=g_shSinTable[iOff];
          }
          else
          { // ������������� ���������
            iOff-=(d_Fs/d_FStep)/2;
/*            if(iOff<3*(d_Fs/d_FStep)/4)
            { // 3-� ��������
              iOff-=(d_Fs/d_FStep)/2;
            }
            else
            { // 4-� ��������
              iOff=(d_Fs/d_FStep)-iOff-1;
            }*/
            *pwOut=-g_shSinTable[iOff];
          }
if(Chan==5) *pwOut/=10;
          Info4Gen[Chan]+=d_FGen/d_FStep;
          if(Info4Gen[Chan]>=d_Fs/d_FStep) Info4Gen[Chan]-=d_Fs/d_FStep;
        }
        break;
      case 2: // ��������� ������ ����
/*        Info4Gen[Chan]=GaloisLfsr(Info4Gen[Chan]);
        *pwOut=Info4Gen[Chan] << 7;*/
        if(!s_bRand)
        { s_shRand=2*(rand()-16384); s_bRand=true; }
        *pwOut=s_shRand;
if(Chan==5) *pwOut/=10;
        break;
      default: // ������� ������
        *pwOut=s_iSumInp-iSub;
        break;
    }
  }
  else *pwOut=0;
}

void Sport_Int(int val)
{ // ��������� ���������� �� 16 ��� �� SPORT
  static int i,iSumIPPM;

*pFIO_FLAG_S=PF10; ssync();  //!!! �������

  s_bRand=false;
  for(i=0;i<d_LenBuf+d_MaxCPIn;i++)
  { // ��������� ������� �������� ��� ������� �����
    if(DisableInp[i])
    {
      if(i>=d_LenBuf) CPIn[i-d_LenBuf]=0;
      else
      {
        switch(i)
        {
          case 0: RxBuf0[0]=0; break;
          case 1: RxBuf0[2]=0; break;
          case 2: RxBuf1[0]=0; break;
          case 3: RxBuf1[2]=0; break;
        }
      }
    }
  }

  s_iSumInp=iSumIPPM=0;
  // ������������ ������� ��������
  for(i=1;i<d_MaxCPIn;i++) iSumIPPM+=CPIn[i];
  s_iSumInp+=RxBuf0[0]+     // DR0PRI L (1 �����=Sout)
             RxBuf0[2]+     // DR0PRI R (2 �����)
             RxBuf1[0]+     // DR1PRI L (3 �����)
             RxBuf1[2]+     // DR1PRI R (4 �����)
             CPIn[0]+       // CP UK
             iSumIPPM;

  // ����� �������� � ������ � CPOut
  // DT0PRI L (1 �����=Rin)
  _OutSample(0,TxBuf0,RxBuf0[0]);

  // DT0PRI R (2 �����)
  _OutSample(1,TxBuf0+2,RxBuf0[2]);

  // DT1PRI L (3 �����)
  _OutSample(2,TxBuf1,RxBuf1[0]);
//TxBuf1[0]=CPIn[1]; // !!! TEST !!!

  // DT1PRI R (4 �����)
  _OutSample(3,TxBuf1+2,RxBuf1[2]);

  // UK
  _OutSample(4,CPOut,CPIn[0]);

  // Multicast IPPM
  _OutSample(5,CPOut+1,iSumIPPM);

  GetSampleFromCPOut2OutBuf();  
  GetSampleFromInBuf2CPIn();

*pFIO_FLAG_C=PF10; ssync(); //!!! �������

  *pDMA1_IRQ_STATUS|=DMA_DONE; // ����� �������� ����������
  ssync();
}

