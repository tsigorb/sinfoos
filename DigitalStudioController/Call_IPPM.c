#include "Sinfo.h"
#include "GlobalData.h"

///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
static IPPM_ABNT_TP sMasAbntIPPM[d_MaxAbntIPPM];

unsigned short g_wMaxSpeakIPPM=0,
               g_wMaxIPPM=0,
               g_wMasterIPPM=0xff,
               g_wMasterLvl=8;

static unsigned int s_dwLiveTmIPPM=5000;

/////////////////////////////////////////////////////////////////////
void SetMaxIPPM(unsigned short wPar)
{
  if(wPar && (wPar<=d_MaxAbntIPPM)) g_wMaxIPPM=wPar;
}

void SetMaxSpeakIPPM(unsigned short wPar)
{
  if(wPar && (wPar<=d_MaxSpeakIPPM)) g_wMaxSpeakIPPM=wPar;
}

void SetMasterIPPM(unsigned short wPar)
{
  if(wPar && (wPar<=g_wMaxIPPM)) g_wMasterIPPM=wPar;
}

void SetMasterLvl(unsigned short wPar)
{
  if(wPar && (wPar<33)) g_wMasterLvl=wPar;
}

bool isSpeakMaster(void)
{
  if(g_wMasterIPPM && (g_wMasterIPPM<=g_wMaxIPPM))
  {
    IPPM_ABNT_TP *pAdr=sMasAbntIPPM+g_wMasterIPPM-1;
    if(pAdr->dwIP_Private && pAdr->btCP2UK && (pAdr->Lvl>=g_wMasterLvl)) return true;
  }
  return false;
}

void SetLvlIPPM(unsigned short IPPM_Numb,unsigned int Lvl)
{
  vPortEnableSwitchTask(false);
  if(IPPM_Numb && (IPPM_Numb<=g_wMaxIPPM))
  {
    IPPM_ABNT_TP *pAdr=sMasAbntIPPM+IPPM_Numb-1;
    if(pAdr->dwIP_Private && pAdr->btCP2UK) pAdr->Lvl=Lvl;
  }
  vPortEnableSwitchTask(true);
}

unsigned char GetSpeakIPPM(void)
{
  unsigned char i,Ind=0;
  unsigned int MaxLvl=0;
  IPPM_ABNT_TP *pAdr=sMasAbntIPPM;

  if(isSpeakMaster()) return g_wMasterIPPM;
  for(i=0;i<g_wMaxIPPM;i++,pAdr++)
  {
    if(pAdr->dwIP_Private && pAdr->btCP2UK)
    {
      if(pAdr->Lvl>MaxLvl)
      { MaxLvl=pAdr->Lvl; Ind=i+1; }
    }
  }
  return Ind;
}

bool GetMAC4IPPM(unsigned int dwIP,unsigned char *MACAddr)
{
  unsigned char i;

  vPortEnableSwitchTask(false);
  for(i=0;i<g_wMaxIPPM;i++)
  {
    if(sMasAbntIPPM[i].dwIP_Private==dwIP)
    {
      memcpy(MACAddr,sMasAbntIPPM[i].btMAC,6);
      vPortEnableSwitchTask(true);
      if(MACAddr[0] || MACAddr[1] || MACAddr[2] || MACAddr[3] || MACAddr[4] || MACAddr[5]) return true;
      return false;
    }
  }
  vPortEnableSwitchTask(true);
  return false;
}

void RenumbSpeakOnIPPM(unsigned char NbOn)
{
  int i;
  IPPM_ABNT_TP *pAdr=sMasAbntIPPM;

  for(i=0;i<g_wMaxIPPM;i++,pAdr++)
  {
    if(pAdr->dwIP_Private && pAdr->btCP2UK)
    {
      if(pAdr->NbOn==NbOn) pAdr->NbOn=0;
      else
        if(pAdr->NbOn>NbOn) pAdr->NbOn--;
    }
  }
}

void SendCmd4IPPM_MicOff(unsigned char Numb)
{
  LIST_UK_IPPM_TP *pItem=ListUK_IPPM_AddItem(1,NULL);
  if(pItem)
  {
    pItem->Numb=Numb;
    pItem->sIK.A1=sMasAbntIPPM[Numb-1].dwIP_Private;
    pItem->sIK.PORT1=sMasAbntIPPM[Numb-1].wPort;
    pItem->sIK.L=20;
    pItem->sIK.ByteInfoKadr[0]=0xed;
    pItem->sIK.ByteInfoKadr[1]=0x0;
    pItem->sIK.ByteInfoKadr[2]=pItem->Numb;
    pItem->sIK.ByteInfoKadr[3]=pItem->wId >> 8;
    pItem->sIK.ByteInfoKadr[4]=pItem->wId & 0xff;
    pItem->sIK.ByteInfoKadr[5]=7;
    strcpy((char *)(pItem->sIK.ByteInfoKadr+6),"SWITCH_MIC_OFF");
  }
}

void DelCP4IPPM(unsigned char Numb,bool bSendCmd)
{
  unsigned char btCP;
  Numb--;
  btCP=sMasAbntIPPM[Numb].btCP2UK;
  if(btCP)
  { // ���� ����� ����� �� IPPM
    if(bSendCmd) SendCmd4IPPM_MicOff(Numb+1);
    vPortEnableSwitchTask(false);
    sMasAbntIPPM[Numb].btCP2UK=0;
    StopAudioFromIPPM(btCP);
    SetFreeCP(btCP);
    RenumbSpeakOnIPPM(sMasAbntIPPM[Numb].NbOn);
    vPortEnableSwitchTask(true);
  }
}

unsigned char GetNumbSpeakOnIPPM(void)
{
  unsigned short i;
  unsigned char NbOn,NbMin=d_MaxSpeakIPPM+1,NbMax=0,
                IndF=0;
  IPPM_ABNT_TP *pAdr=sMasAbntIPPM;

  for(i=0;i<g_wMaxIPPM;i++,pAdr++)
  {
    if(pAdr->dwIP_Private && pAdr->btCP2UK)
    {
      NbOn=pAdr->NbOn;
      if(NbOn)
      {
        if((NbOn<NbMin) && (i+1!=g_wMasterIPPM))
        { IndF=i+1; NbMin=NbOn; }
        if(NbOn>NbMax) NbMax=NbOn;
        if(NbMax==g_wMaxSpeakIPPM)
        { // �������� ����. ����������� �-�� ���������� -> ��������� ����� ������ ���������� (����� ��������)
          if(IndF<1) return 0;
          DelCP4IPPM(IndF,true);
          return g_wMaxSpeakIPPM;
        }
      }
    }
  }
  return NbMax+1;
}

unsigned char GetInputCP4Numb(unsigned char Numb)
{
  int i;
  unsigned char btCP;

  if(!Numb || (Numb>=d_MaxAbntIPPM)) return 0;
  vPortEnableSwitchTask(false);
  btCP=sMasAbntIPPM[Numb-1].btCP2UK;
  vPortEnableSwitchTask(true);
  return btCP;
}

void OtboiAbonentIPPM(unsigned char Numb)
{
  if(Numb && (Numb<=d_MaxAbntIPPM) && (sMasAbntIPPM[Numb-1].dwIP_Private))
  {
    LIST_UK_IPPM_TP *pItem=ListUK_IPPM_AddItem(1,NULL);
    Numb--;
    if(pItem)
    {
      pItem->Numb=Numb+1;
      pItem->sIK.A1=sMasAbntIPPM[Numb].dwIP_Private;
      pItem->sIK.PORT1=sMasAbntIPPM[Numb].wPort;
      pItem->sIK.L=16;
      pItem->sIK.ByteInfoKadr[0]=0xed;
      pItem->sIK.ByteInfoKadr[1]=0x0;
      pItem->sIK.ByteInfoKadr[2]=pItem->Numb;
      pItem->sIK.ByteInfoKadr[3]=pItem->wId >> 8;
      pItem->sIK.ByteInfoKadr[4]=pItem->wId & 0xff;
      pItem->sIK.ByteInfoKadr[5]=5;
      strcpy((char *)(pItem->sIK.ByteInfoKadr+6),"DISCONNECT");
    }
    DelCP4IPPM(Numb+1,true);
    StopAudio4IPPM_Multicast(); // ���� ������ ��� ������������������ IPPM, �� ������������ �������� ������� � ���� ������
SendStr2IpLogger("$DEBUG$OtboiAbonentIPPM: Numb=%d",pItem->Numb);
  }
}

void IPPM_CmdConnectNoAnswer(LIST_UK_IPPM_TP *pItem)
{ OtboiAbonentIPPM(pItem->Numb); }

void IPPM_AnswConnect(LIST_UK_IPPM_TP *pItem,unsigned char *pbtAnsw)
{
SendStr2IpLogger("$DEBUG$IPPM_AnswConnect: Numb=%d",pItem->Numb);
  StartAudio4IPPM_Multicast();
{ // !!! ��� �������� ��������� ������ ������ ���� ����� IpXchg (8 kHz, 8 bit, a-Law) !!!
//  StartAudio4UK();
//  StartAudioFromUK();
}
/*{ // !!! ��� �������� ��������� ������ ������ ���� ����� IpXchg (16 kHz, 16 bit, linear) !!!
  unsigned char btCP=GetFreeCP();
  if(btCP)
  {
    sMasAbntIPPM[0].btCP2UK=btCP;
    StartAudioFromIPPM(btCP);
  }
}*/
}

void SendIPPMCmdConnect(unsigned char Numb)
{
  unsigned int dwMIP=GetMIPAddr(1);

  if(!dwMIP) return;
  LIST_UK_IPPM_TP *pItem=ListUK_IPPM_AddItem(1,NULL);
  if(pItem)
  {
    pItem->Numb=Numb;
    pItem->sIK.A1=sMasAbntIPPM[Numb-1].dwIP_Private;
    pItem->sIK.PORT1=sMasAbntIPPM[Numb-1].wPort;
    pItem->sIK.L=17;
    pItem->sIK.ByteInfoKadr[0]=0xed;
    pItem->sIK.ByteInfoKadr[1]=0x0;
    pItem->sIK.ByteInfoKadr[2]=Numb;
    pItem->sIK.ByteInfoKadr[3]=pItem->wId >> 8;
    pItem->sIK.ByteInfoKadr[4]=pItem->wId & 0xff;
    pItem->sIK.ByteInfoKadr[5]=3;
    SET_DW(pItem->sIK.ByteInfoKadr+6,dwMIP);
    strcpy((char *)(pItem->sIK.ByteInfoKadr+10),"CONNECT");
    pItem->pFunc_UK_IPPMNoAnswer=IPPM_CmdConnectNoAnswer;
    pItem->pFunc_UK_IPPMAnswer=IPPM_AnswConnect;
SendStr2IpLogger("$DEBUG$SendIPPMCmdConnect Numb=%d, wId=%d, MIP=%08X",Numb,pItem->wId,dwMIP);
  }
}

/*void IPPM_CmdConnectPrivateNoAnswer(LIST_MU2IPPM_TP *pItem)
{ OtboiAbonentIPPM(pItem->Numb); }

void SendIPPMCmdConnectPrivate(unsigned char Numb)
{
  LIST_UK_IPPM_TP *pItem=ListUK_IPPM_AddItem(1,NULL);
  if(pItem)
  {
    pItem->Numb=Numb;
    pItem->sIK.A1=sMasAbntIPPM[Numb-1].dwIP_Private;
    pItem->sIK.PORT1=sMasAbntIPPM[Numb-1].wPort;
    pItem->sIK.L=21;
    pItem->sIK.ByteInfoKadr[0]=0xed;
    pItem->sIK.ByteInfoKadr[1]=0x0;
    pItem->sIK.ByteInfoKadr[2]=Numb;
    pItem->sIK.ByteInfoKadr[3]=pItem->wId >> 8;
    pItem->sIK.ByteInfoKadr[4]=pItem->wId & 0xff;
    pItem->sIK.ByteInfoKadr[5]=4;
    strcpy((char *)(pItem->sIK.ByteInfoKadr+6),"CONNECT_PRIVATE");
    pItem->pFunc_UK_IPPMNoAnswer=IPPM_CmdConnectPrivateNoAnswer;
  }
}*/

void SendAnswer4IPPM(unsigned int dwIP,unsigned short wPort,unsigned short wSz,
                     unsigned char Numb,unsigned short wCmdAnsw,unsigned char btCmd,
                     unsigned char btRes,char *strInfo)
{
  static section ("bigdata") sInfoKadr sIK;

  sIK.Source=UserIPPM;
  sIK.A1=dwIP;
  sIK.PORT1=wPort;
  sIK.L=wSz;
  sIK.ByteInfoKadr[0]=0xec;
  sIK.ByteInfoKadr[1]=0x0;
  sIK.ByteInfoKadr[2]=Numb;
  sIK.ByteInfoKadr[3]=wCmdAnsw >> 8;
  sIK.ByteInfoKadr[4]=wCmdAnsw & 0xff;
  sIK.ByteInfoKadr[5]=btCmd;
  sIK.ByteInfoKadr[6]=btRes;
  if(btCmd==1)
  { // �������������� ����� � ������ �� ������� ����������� (����� ����� � ���, ���-����� DSC, ��������� ����)
    sIK.L+=8;
    sIK.ByteInfoKadr[7]=1+s_dwLiveTmIPPM/1000;
    GetMAC_DSC(1,sIK.ByteInfoKadr+8);
    sIK.ByteInfoKadr[14]=0;
    strcpy((char *)(sIK.ByteInfoKadr+16),strInfo);
  }
  else strcpy((char *)(sIK.ByteInfoKadr+7),strInfo);
  SendAnswUK_IPPMCmd(1,Numb,wCmdAnsw,&sIK);
}

unsigned char CheckMaxPgmIPPM(void)
{
/* !!! extern unsigned short LenLicIPPM; // ����� �������� IPPM
  int i,iPgmIPPM=0;

  for(i=0;i<g_wMaxIPPM;i++)
  {
    if(sMasAbntIPPM[i].dwIP_Private && sMasAbntIPPM[i].bPgmIPPM) iPgmIPPM++;
  }
  if(iPgmIPPM<LenLicIPPM)*/ return 0; // OK
  //return 1; // ������ ��������
}

char *MAC2STR(unsigned char *btMAC)
{
  static char strMAC[80];
  LockPrintf();
  sprintf(strMAC,"0x%02x:%02x:%02x:%02x:%02x:%02x",btMAC[0],btMAC[1],btMAC[2],btMAC[3],btMAC[4],btMAC[5]);
  UnlockPrintf();
  return strMAC;
}

void IPPM_CmdRegistration(unsigned int dwIP,unsigned short wPort,
                          unsigned char Numb,unsigned short wCmdAnsw,unsigned char *pInfo)
{
  unsigned char btRes=1;

  if((dwIP==0) || (wPort==0)) return;
  Numb--;
  vPortEnableSwitchTask(false);
  if(sMasAbntIPPM[Numb].dwIP_Private)
  {
    if(sMasAbntIPPM[Numb].dwIP_Private!=dwIP) btRes=3; // ������� ��������������� ��� ������� IP-������
  }
  else
  {
    btRes=0;
    if(pInfo[0]) btRes=CheckMaxPgmIPPM();
    if(btRes==0)
    {
      memset(sMasAbntIPPM+Numb,0,sizeof(IPPM_ABNT_TP));
      sMasAbntIPPM[Numb].dwIP_Private=dwIP;
      sMasAbntIPPM[Numb].wPort=wPort;
      sMasAbntIPPM[Numb].bPgmIPPM=pInfo[0];
//        pInfo[1]; �� ������������ � ����������� ������!
      sMasAbntIPPM[Numb].dwTm4Send=xTaskGetTickCount()+(s_dwLiveTmIPPM >> 2);
      memcpy(sMasAbntIPPM[Numb].btMAC,pInfo+4,6);
SendStr2IpLogger("$DEBUG$IPPM_CmdRegistration: Numb=%d, dwIP_Private=%x, wPort=%d, MAC=%s",
                 Numb+1,sMasAbntIPPM[Numb].dwIP_Private,sMasAbntIPPM[Numb].wPort,MAC2STR(sMasAbntIPPM[Numb].btMAC));
      // �������������� ������������ ���������� ����� ����������� IPPM
      SendIPPMCmdConnect(Numb+1);
    }
  }
  vPortEnableSwitchTask(true);
  SendAnswer4IPPM(dwIP,wPort,29,Numb+1,wCmdAnsw,1,btRes,"ANSWER_ON_REGISTRATION");
}

unsigned char EnableSpeakIPPM(unsigned char Numb)
{ // ������ �� ��������� ���������
  unsigned char btCP,NbOn;

  if(sMasAbntIPPM[Numb-1].btCP2UK) return 0;
  NbOn=GetNumbSpeakOnIPPM();
  if(NbOn<1) return 1;
  btCP=GetFreeCP();
  if(btCP!=0)
  {
    sMasAbntIPPM[Numb-1].btCP2UK=btCP;
    sMasAbntIPPM[Numb-1].NbOn=NbOn;
    StartAudioFromIPPM(btCP);
    return 0;
  }
  return 1;
}

void IPPM_CmdReqMicON(unsigned int dwIP,unsigned short wPort,
                      unsigned char Numb,unsigned short wCmdAnsw)
{
  unsigned char btRes=1;

  if((dwIP==0) || (wPort==0)) return;
  Numb--;
  if(sMasAbntIPPM[Numb].dwIP_Private==dwIP)
  {
    btRes=EnableSpeakIPPM(Numb+1);
SendStr2IpLogger("$DEBUG$IPPM_CmdReqMicON Numb=%d, btCP2UK=%d, btRes=%d",Numb+1,sMasAbntIPPM[Numb].btCP2UK,btRes);
    SendAnswer4IPPM(dwIP,wPort,31,Numb+1,wCmdAnsw,6,btRes,"ANSWER_ON_REQUEST_MIC_ON");
  }
}

void IPPM_CmdUserMicOFF(unsigned int dwIP,unsigned short wPort,
                        unsigned char Numb,unsigned short wCmdAnsw)
{
  if((dwIP==0) || (wPort==0)) return;
  if(sMasAbntIPPM[Numb-1].dwIP_Private==dwIP)
  {
SendStr2IpLogger("$DEBUG$IPPM_CmdReqMicOFF Numb=%d, btCP2UK=%d",Numb,sMasAbntIPPM[Numb-1].btCP2UK);
    DelCP4IPPM(Numb,false);
    SendAnswer4IPPM(dwIP,wPort,29,Numb,wCmdAnsw,8,0,"ANSWER_ON_USER_MIC_OFF");
  }
}

void IPPM_DSC_Event(sCommand *psCmd)
{
  unsigned short wCmdAnsw;
  unsigned char Numb,*pUdpBuf=psCmd->BufCommand;

  Numb=pUdpBuf[1];
  if((Numb>g_wMaxIPPM) || (Numb<1) || (pUdpBuf[2]!=0)) return;
  wCmdAnsw=pUdpBuf[3]; wCmdAnsw=(wCmdAnsw << 8) | pUdpBuf[4];
  if(pUdpBuf[0]==0xeb)
  { // ������� �� IPPM � DSC
    if(sMasAbntIPPM[Numb-1].dwIP_Private)
    {
      if(RepeatCmdUK_IPPM(1,Numb,wCmdAnsw)) return;
    }
//SendStr2IpLogger("$DEBUG$IPPM_Event Cmd_EB: Numb=%d, CmdId=%d",Numb,pUdpBuf[5]);
    switch(pUdpBuf[5])
    {
      case 1: // REGISTRATION
        IPPM_CmdRegistration(psCmd->A1,psCmd->PORT1,Numb,wCmdAnsw,pUdpBuf+6);
        break;
      case 6: // REQUEST_MIC_ON
        IPPM_CmdReqMicON(psCmd->A1,psCmd->PORT1,Numb,wCmdAnsw);
        break;
      case 8: // USER_MIC_OFF
        IPPM_CmdUserMicOFF(psCmd->A1,psCmd->PORT1,Numb,wCmdAnsw);
        break;
      default: break;
    }
  }
  else
  {
    if(pUdpBuf[0]==0xea)
    { // ����� �� IPPM �� ������� DSC
      if(!UK_IPPM_RcvAnsw(1,wCmdAnsw,pUdpBuf+6)) return;
//if(pUdpBuf[5]!=2) SendStr2IpLogger("$DEBUG$IPPM_Event Cmd_EA: Numb=%d, cmdId=%d",Numb,pUdpBuf[5]);
    }
  }
}

void IPPM_CmdGetStateNoAnswer(LIST_UK_IPPM_TP *pItem)
{ // ��� ������ �� IPPM �� GET_STATE
  unsigned char i=pItem->Numb-1;

  vPortEnableSwitchTask(false);
SendStr2IpLogger("$DEBUG$IPPM registration end: Numb=%d, dwIP_Private=%x",i+1,sMasAbntIPPM[i].dwIP_Private);
  ClearQueueCmdUK_IPPM(1,i+1);
  OtboiAbonentIPPM(i+1);
  memset(sMasAbntIPPM+i,0,sizeof(IPPM_ABNT_TP));
  vPortEnableSwitchTask(true);
}

void OtboiAllIPPM(void)
{

}

void InitCall_IPPM(void)
{
  int i;
  g_wMaxIPPM=0;
  g_wMaxSpeakIPPM=0;
  for(i=0;i<d_MaxAbntIPPM;i++)
  {
    if(sMasAbntIPPM[i].dwIP_Private) OtboiAbonentIPPM(i+1);
  }
  vTaskDelay(500);
  vPortEnableSwitchTask(false);
  InitRepeatCmdUK_IPPM();
  memset(sMasAbntIPPM,0,sizeof(sMasAbntIPPM));
  vPortEnableSwitchTask(true);
}

void IPPM_IRPTOut(void)
{
  int i;
  unsigned short wId;

  for(i=0;i<g_wMaxIPPM;i++)
  {
    if(sMasAbntIPPM[i].dwIP_Private)
    {
      if(IsCurrentTickCountGT(sMasAbntIPPM[i].dwTm4Send))
      {
        LIST_UK_IPPM_TP *pItem=ListUK_IPPM_AddItem(1,NULL);
        if(pItem)
        {
          pItem->Numb=i+1;
          pItem->sIK.A1=sMasAbntIPPM[i].dwIP_Private;
          pItem->sIK.PORT1=sMasAbntIPPM[i].wPort;
          pItem->sIK.L=6;
          pItem->sIK.ByteInfoKadr[0]=0xed;
          pItem->sIK.ByteInfoKadr[1]=0x0;
          pItem->sIK.ByteInfoKadr[2]=i+1;
          pItem->sIK.ByteInfoKadr[3]=pItem->wId >> 8;
          pItem->sIK.ByteInfoKadr[4]=pItem->wId & 0xff;
          pItem->sIK.ByteInfoKadr[5]=2;
          pItem->pFunc_UK_IPPMNoAnswer=IPPM_CmdGetStateNoAnswer;
          sMasAbntIPPM[i].dwTm4Send=xTaskGetTickCount()+(s_dwLiveTmIPPM>>2);
//SendStr2IpLogger("$DEBUG$SendGET_STATE4IPPM: Numb=%d, dwIP_Private=%x",i+1,sMasAbntIPPM[i].dwIP_Private);
        }
      }
    }
  }
}

