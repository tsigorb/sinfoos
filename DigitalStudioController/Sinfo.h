#include <cdefBF533.h>
#include <ccblkfn.h>
#include <sysreg.h>
#include <sys\exception.h>
#include <signal.h>
#include <time.h>
#include <stdio.h> 
#include <string.h> 
#include <heapinfo.h>

#include "..\FreeRTOS/FreeRTOS.h"
#include "..\FreeRTOS/queue.h"
#include "..\FreeRTOS/task.h"
#include "..\FreeRTOS/semphr.h"
#include "..\FreeRTOS/FileSys.h"
#include "..\MD5/md5.h" 
#include "..\LockPrintf.h"
#include "..\RTC.h"

#include "SPI_Z.h"
#include "socket_Z.h"

#define CCLK                491520
#define SCLK                 98304  // CCLK/5

#define IVG_PLL_WAKEUP	P0_IVG
#define IVG_DMA_ERROR	P1_IVG
#define IVG_PPI_ERROR	P2_IVG
#define IVG_SPT0_ERROR	P3_IVG
#define IVG_SPI_ERROR	P4_IVG
#define IVG_SPT1_ERROR	P5_IVG
#define IVG_UART_ERROR	P6_IVG
#define IVG_RTC			P7_IVG
#define IVG_PPI			P8_IVG
#define IVG_SPT0_RX		P9_IVG
#define IVG_SPT0_TX		P10_IVG
#define IVG_SPT1_RX		P11_IVG
#define IVG_SPT1_TX		P12_IVG
#define IVG_SPI			P13_IVG
#define IVG_UART_RX		P14_IVG
#define IVG_UART_TX		P15_IVG
#define IVG_TIMER0		P16_IVG
#define IVG_TIMER1		P17_IVG
#define IVG_TIMER2		P18_IVG
#define IVG_PFA			P19_IVG
#define IVG_PFB			P20_IVG
#define IVG_MEMDMA0		P21_IVG
#define IVG_MEMDMA1		P22_IVG
#define IVG_SWDT		P23_IVG

//����� ��� SIC_IMASK 
#define IRQ_PLL_WAKEUP	0x00000001
#define IRQ_DMA_ERROR	0x00000002
#define IRQ_PPI_ERROR	0x00000004
#define IRQ_SPT0_ERROR	0x00000008
#define IRQ_SPT1_ERROR	0x00000010
#define IRQ_SPI_ERROR	0x00000020
#define IRQ_UART_ERROR	0x00000040
#define IRQ_RTC			0x00000080
#define IRQ_PPI			0x00000100
#define IRQ_SPT0_RX		0x00000200
#define IRQ_SPT0_TX		0x00000400
#define IRQ_SPT1_RX		0x00000800
#define IRQ_SPT1_TX		0x00001000
#define IRQ_SPI			0x00002000
#define IRQ_UART_RX		0x00004000
#define IRQ_UART_TX		0x00008000
#define IRQ_TIMER0		0x00010000
#define IRQ_TIMER1		0x00020000
#define IRQ_TIMER2		0x00040000
#define IRQ_PFA			0x00080000
#define IRQ_PFB			0x00100000
#define IRQ_MEMDMA0		0x00200000
#define IRQ_MEMDMA1		0x00400000
#define IRQ_SWDT		0x00800000

// SPORTx word length
#define SLEN_16         0x000f

// DMA flow mode=1 - ����� ���������������
#define FLOW_1          0x1000

#define THR             0x20 // ��� ���������� TX

#define pRHR	(volatile unsigned char *)0x202f0000
#define pTHR	(volatile unsigned char *)0x202f0000
#define pIER	(volatile unsigned char *)0x202f0002
#define pFCR	(volatile unsigned char *)0x202f0004
#define pISR	(volatile unsigned char *)0x202f0004
#define pLCR	(volatile unsigned char *)0x202f0006
#define pMCR	(volatile unsigned char *)0x202f0008
#define pLSR	(volatile unsigned char *)0x202f000a
#define pMSR	(volatile unsigned char *)0x202f000c
#define pSPR	(volatile unsigned char *)0x202f000e
#define pDLL	(volatile unsigned char *)0x202f0000
#define pDLM	(volatile unsigned char *)0x202f0002
#define pLED	(volatile unsigned char *)0x202f0010

#define pRHR1	(volatile unsigned char *)0x203f0000
#define pTHR1	(volatile unsigned char *)0x203f0000
#define pIER1	(volatile unsigned char *)0x203f0002
#define pFCR1	(volatile unsigned char *)0x203f0004
#define pISR1	(volatile unsigned char *)0x203f0004
#define pLCR1	(volatile unsigned char *)0x203f0006
#define pMCR1	(volatile unsigned char *)0x203f0008
#define pLSR1	(volatile unsigned char *)0x203f000a
#define pMSR1	(volatile unsigned char *)0x203f000c
#define pSPR1	(volatile unsigned char *)0x203f000e
#define pDLL1	(volatile unsigned char *)0x203f0000
#define pDLM1	(volatile unsigned char *)0x203f0002

#define CodeStatusTrue     0       //-+ - ��� dsp_comm
#define CodeStatusFalse    255     //-+

#define MAX_BUF_SIZE       1518    // ������ ������ ��� UDP - ������ ������!
#define SynchroByte        0x16

#define MAX(a,b)           ((a)>(b)?(a):(b))

#define d_NameLen          20
#define LenFileName        16 // ������������ ����� ����� �����

#define d_MaxIpBufs         7  // ����� ������� �� ����� ��� ������ ������� �� UK � IPPM
#define d_LenSoundIPPM    480  // ����� ������ ��� IPPM � ������ (30 ���� �� 16 ��� linear)
#define d_LenSoundUK      240  // ����� ������ ��� UK � ������ (30 ���� �� 8 ��� a-Law)

#define d_LenBuf            4  // ������ ������ SPORTx � ������ (PRI L+R, SLAVE L+R)
#define d_MaxCPIn           9  // 1 �� ��� UK + 8 �� ��� ��������� IPPM
#define d_MaxCPOut          2  // 1 �� ��� UK + 1 �� ��� multicast out ��������� IPPM

#pragma pack(2)
typedef struct
{// ��������� ������ ������ RTP � BigEndian ������� (������� ������)
  unsigned short Fmt;          // == 0x9008
/* 
   10..............					- V ������ ��������� = 2
   ..0.............					- P ������� �������������� ������� - ������������
   ...1............					- X ������� ���������� ���������
   ....0000........					- �� ����� �������� ��������������� ����������
   ........0.......					- M ������ 
   .........1100000					- PT ����������� ������� ������ (96 - ��� ��� Linear 16 ���,16 ���,30 ���� �����)
*/ 
  unsigned short Number;            // ���������� ����� ������
  unsigned int   Timer;             // ��������� �����
  unsigned int   SSRC;              // ������������� ��������� ������������� (SSRC)
  unsigned short Profile;           // �� ������ !!!. �� �������� !!!
  unsigned short Len;               // == 0 ����� �������������� �������� (�� 32 ����)
} xRTPHeadUK;

typedef struct
{// ��������� ������ 16 ��� 16 ��� linear (LSB)
  unsigned short Fmt;               // == 0x9008 �� ��������, ==0x9108 �� ������
  unsigned short Number;            // ���������� ����� ������
  unsigned short Profile;           // �� ������: >0-����� IPPM. �� ��������: ==2-multicast
  unsigned int Lvl;
} xRTPTail;
#pragma pack()


typedef struct
{
  short Chan;                    // ����� ������
  unsigned short wIpBufs[2][d_LenSoundIPPM+(sizeof(xRTPTail)/2)];
  unsigned short Ind;            // ������ � ������� ������
  unsigned short Number;         // ����� ��������� ������
  short NumBuf;                  // ����� �������� ������
  short NewData;                 // ������� �������� ���������� ������
  short Overrun;
} xOutputIpFromISR;

typedef struct
{
  short Chan,                    // ����� ������ c 1
        IPPM_Numb,               // ����� IPPM, �� �������� ������ �����
        Ind;                     // ����������� ������ �������� � ������� MasIpBufsInd
  unsigned short wIpBuf[d_LenSoundIPPM+(sizeof(xRTPTail)/2)]; // ������� �������� ������� �� �� � IPPM ����������:
  // ��� ������� IPPM ������� ���� ���� 16 ��� 16 bit linear, � �� ��� ��������� ����� xRTPTail
  // ��� ������� �� ������� ���� ��������� xRTPHeadUK, � ����� ���� 8 ��� 8 bit a-law
} InputIpBufTP;

typedef struct
{ // ------------- ������� ������� ������ � ���������� d_MaxCPIn ------------------------------------
  short Chan;                    // ����� ������ c 1
  short NumBuf;                  // ����� �������� ������ ���������������
  short NewData;                 // ������� �������� ���������� ������
  short OverrunISR,OverrunIP;
  unsigned short wIpBufs[2*d_LenSoundIPPM];
  unsigned short Ind;                    // ������ � ������� ������
  unsigned short Takt4Pckt;              // ����� ������ ��������������� ������
  short Sync;                            // ������� ������������� �������������
  unsigned short MasIpBufsInd[d_MaxIpBufs]; // ������ ������� ������� � MasInputIpBuf ��� ������
  unsigned short Start,
                 Head,Tail;               // ������ � ����� � ������� MasIpBufsInd
} xInputIp4ISR;

#define d_MaxCmdBufSz               280
#define d_MaxReplyBufSz             280
#define d_MaxSoundIpPcktSz          ((2*d_LenSoundIPPM)+sizeof(xRTPTail))  // 960 bytes sound + size RTP tail
#define d_UKSoundIpPcktSz           256 // size UK RTP head + 240 bytes sound

//--------- ��������� ��������������� ����� ��� ������
typedef struct
{
  unsigned char Source; //�������� �������
  unsigned short L; // �����
  unsigned int A1; // ����� ����������
  unsigned short PORT1; // ���� ����������
  unsigned char ByteInfoKadr[d_MaxReplyBufSz]; //����� �����
} sInfoKadr;

//--------------- ��������� �������
typedef struct
{
  unsigned char Source; //�������� �������
  unsigned short L; // ����� ������ �������
  unsigned int A1; // ����� ����������
  unsigned short PORT1; // ���� ����������
  unsigned char BufCommand[d_MaxCmdBufSz]; //����� �������
} sCommand;

#pragma pack(1)
typedef struct
{
unsigned char btSIPR[4]; // IP address
unsigned char btMcstIP[4]; // Multicast IP address
unsigned char btMSR[4]; // Subnet mask
unsigned char btGAR[4]; // Gateway address
unsigned char btSHAR[6]; // MAC address - DO NOT USE THIS ON A PUBLIC NETWORK!
unsigned short wPortControl,wPortSound;
} IPPARAMS_TP;

/*typedef struct
{
  int L;
  unsigned char Buf[MAX_BUF_SIZE];
} ECAT_XCHG_TP;*/
#pragma pack()

#define d_MAXDSPCOM         5
#define d_MAXLOGGER         2
#define IRP_MAX             (d_MAXDSPCOM+d_MAXLOGGER)

#define STACK_ViewCommand   (configMINIMAL_STACK_SIZE+672)
#define STACK_IP            (configMINIMAL_STACK_SIZE+896)
#define STACK_TFTP          (configMINIMAL_STACK_SIZE+512)
#define STACK_Sports        (configMINIMAL_STACK_SIZE+546)
#define STACK_VCS           (configMINIMAL_STACK_SIZE+512)
//#define STACK_ETHERCAT      (configMINIMAL_STACK_SIZE+4000)

/*
#define STACK_ALL           (STACK_ViewCommand+STACK_TCPIP++STACK_TFTP+STACK_SportX+STACK_SportXCmd+STACK_ETHERCAT)
#if (STACK_ALL > 0x8000)
  #error "ERROR: stack for create tasks too large!!!"
#endif
*/

// �������������� ����� ��� �������� � �����������
#define d_MaxTask               16

extern xTaskHandle              thTaskHandles[d_MaxTask];

#define thIpM                   thTaskHandles[0]
#define thIpS                   thTaskHandles[1]
#define thTFTP                  thTaskHandles[2]
#define thViewCmd               thTaskHandles[3]
#define thSports                thTaskHandles[4]
#define thVCS                   thTaskHandles[5]
//#define thEthCat                thTaskHandles[6]

#define d_WDSTATE_NU1           0x0001
#define d_WDSTATE_NU2           0x0002
#define d_WDSTATE_Sports        0x0004
#define d_WDSTATE_NU5           0x0008
#define d_WDSTATE_ViewCmd       0x0010
#define d_WDSTATE_NU3           0x0020
#define d_WDSTATE_Ip            0x0040
#define d_WDSTATE_NU4           0x0080
#define d_WDSTATE_TFTP          0x0100

#define d_WDSTATE_TASKALL (d_WDSTATE_Sports |  \
                           d_WDSTATE_Ip | d_WDSTATE_ViewCmd | d_WDSTATE_TFTP)

inline unsigned char WDSTATE2NUMB(unsigned int dwState)
{
  unsigned char btNumb=0;

  for(btNumb=0;btNumb<d_MaxTask;btNumb++)
  {
    if(dwState & 0x01) return(btNumb);
    dwState>>=1;
  }
  return(d_MaxTask);
}

// ���� ������ � ������ ����� ����������� � ��
#define ErrNoCommand 			8 // �� ���������� �������� �������
/*
#define ErrCS 					3 // ������ ����������� ����� �� ������ RS232 
#define ErrCodeQueueSend 		4 // ������� xQueueCommand �����������
#define ErrNoSinByte 			5 // �� ���� ����������� � �������
#define ErrProtokolUart 		6 // ������ ��������� Uart
#define ErrTimeOut 				7 // ����������� ����� �������� ����� ������� �� RS232
#define ErrQueueUart 			251 // ������� xCharsForTx �� �����, � ����� ���������� ����� ������� 
#define ErrInReply 				11 // ������ � ������ ���
#define ErrAnswBadCmd           250 // ����� �� �� �� �������
#define ErrAnswBadLen           251 // ������������ ����� ������
#define ErrNoReplyUart 			252 // �� ������ ����� �� ���
#define NoCommand 				0xB0 //��� ����� �������
*/

////////////////////////////////////////////////////////////////////////////////////////////
#define QueueReplyCnt           4                                                         //
                                                                                          //
#define UserIPPM                0                                                         //
#define UserUDP                 1                                                         //
#define UserViewCmd             2                                                         //
#define UserTCP                 3                                                         //
                                                                                          //
#define UserNU2                 4                                                         //
#define UserNU3                 5                                                         //
#define UserNU4                 6                                                         //
#define UserNU5                 7                                                         //
////////////////////////////////////////////////////////////////////////////////////////////

// ������������� ������� Flash-������ 45DB161D �� ����������� � ��������
//   0x000000 - 0x1FFFFF ������ ���������� (SF1-PF2) (���������,���������,�������� ��������� � ������ �������� �������)
//   0x000000 - 0x00DFFF ��������� ���������
//   0x00E000 - 0x00FFFF ������� ����������
//   0x010000 - 0x0FFFFF �������� ��������� ������
//   0x100000 - 0x1FFFFF �������� ������� (������)
//   0x200000 - 0x3FFFFF ������ ���������� �������� ������� (SF2-PF4)

#define AdrParam2Flash   0xe000
#define AdrSN            (AdrParam2Flash+0x0)    // 4 ����� - �������� �����
#define AdrFlashLastFile (AdrParam2Flash+0x200)  // 4 ����� - ����� ���������� ����� �� ����
#define dAddrPgm2Flash   0x10000
#define dAddrFS2Flash    0x100000

// !! ������ 0..7 - ������ W6100, 8..15 - ������ W6100 !!
#define UDPCmdSocket   ((SOCKET)1)  // ����� ��� ������ (������� ����)
#define UKRTPSocket    ((SOCKET)2)  // ����� ��� ����� (�����/��������) ��� ���.���������� � ��
#define VCSTcpSocket   ((SOCKET)3)  // ����� ��� ������ � ��� (������� ����)

#define IPPMCmdSocket  ((SOCKET)9)  // ����� ��� ������ IPPM (���������� ����)
#define IPPMRTPSocket  ((SOCKET)10) // ����� ��� ������ � �������� ����� ��/� IPPM
// !!! �� ����� !!! #define IPPMMcstSocket ((SOCKET)11) // ����� ��� �������� ���������� ����� �� IPPM

#define IP_NONE             0
//#define IP_PULT             1
#define IP_DSPCOM           2
#define IP_LOGGER           4

#define IP_UNKNOWN          0xfe

//#define dIpPultTOut         4000
#define d_LedTOutOk         100
#define d_LedTOutErr        500

/////////////////////////////////////////////////////////////////////
#define d_MAX_PRESET            64
#define d_MAX_VCAM              15

#define d_VCS_None              0
#define d_VCS_YeaLink           1
#define d_VCS_Granat            2
#define d_VCS_Visca             3

/////////////////////////////////////////////////////////////////////
#define d_MaxAbntIPPM       64
#define d_UK_IPPM_CmdTOut   350
#define d_UK_IPPMMaxRepeat  3
#define d_MaxSpeakIPPM      (d_MaxCPIn-1)

typedef struct
{
  unsigned char btMAC[6];
  unsigned int dwIP_Private;
  unsigned short wPort;
  unsigned char NbOn;                   // ���������� ����� IPPM �� ���������� ����������
  bool bPgmIPPM;
  unsigned int dwTm4Send;
  unsigned char btCP2UK;
  unsigned int Lvl;                    // ������� ������� � ��������� IPPM
} IPPM_ABNT_TP;

typedef struct _LIST_UK_IPPM_TP
{
  unsigned short wId;
  unsigned char Numb,repeat_cnt;
  unsigned int dwTOut;
  sInfoKadr sIK;
  void (*pFunc_UK_IPPMAnswer)(struct _LIST_UK_IPPM_TP *pItem,unsigned char *pbtAnsw);
  void (*pFunc_UK_IPPMNoAnswer)(struct _LIST_UK_IPPM_TP *pItem);
  struct _LIST_UK_IPPM_TP *prev,*next;
} LIST_UK_IPPM_TP;

LIST_UK_IPPM_TP *ListUK_IPPM_AddItem(unsigned char btIF,LIST_UK_IPPM_TP *pItem0);
void ListUK_IPPM_Process(unsigned char btIF);
bool UK_IPPM_RcvAnsw(unsigned char btIF,unsigned short wCmdId,unsigned char *pbtAnsw);
void ClearQueueCmdUK_IPPM(unsigned char btIF,unsigned char Numb);

void InitRepeatCmdUK_IPPM(void);
void DelRepeatCmdUK_IPPM(unsigned char btIF,unsigned char Numb);
void SendAnswUK_IPPMCmd(unsigned char btIF,unsigned char Numb,unsigned short wCmd,sInfoKadr *psInfo);
bool RepeatCmdUK_IPPM(unsigned char btIF,unsigned char Numb,unsigned short wCmd);

/////////////////////////////////////////////////////////////////////
void *pvPortMalloc(size_t xSize);
void vPortFree(void *pv);

/////////////////////////////////////////////////////////////////////
unsigned int GET_DW(unsigned char *pbtBuf);
void SET_DW(unsigned char *pbtBuf,unsigned int dwData);
unsigned short GET_W(unsigned char *pbtBuf);

unsigned short ReverseShort(unsigned short data);
unsigned int ReverseInt(unsigned int data);

bool IsDigit(char chSb);
unsigned char *SaveInt2Buf(unsigned char *pbtBuf,int iData);
unsigned char *RestoreIntFromBuf(unsigned char *pbtBuf,int *piData);

void SendTaskState2Ip(void);
void SetTaskVar(unsigned int dwTask,unsigned char btOff,unsigned char btVar);
void SetWDState(unsigned int dwStateTask);

// ����� ����������� �������
void ResetWatchDog(bool bNow);
// ���������� ������ ����������� ������� ��� ����������� �������
void BlockWatchDog(void);

char *strclr(char *str);

unsigned char IsTickCountGT(unsigned int dwTick1,unsigned int dwTick0);
unsigned char IsCurrentTickCountGT(unsigned int dwTick0);
unsigned char IsTickCountLT(unsigned int dwTick1,unsigned int dwTick0);
unsigned char IsCurrentTickCountLT(unsigned int dwTick0);

signed portBASE_TYPE xQueueSendCmd(sCommand *psCmd,portTickType xTicksToWait);

unsigned int GetMIPAddr(unsigned char btIF);
bool GetMAC_DSC(unsigned char btIF,unsigned char *pbtMAC);

void SendDiagnostics2DspCom(unsigned char *pData,unsigned char Len);
void SendStr2IpLogger(const char *fmt,...);

unsigned char FreeAudioPckt4Ip(unsigned char btCPOut);
unsigned char *GetAudioPcktAddr4Ip(unsigned char btCPOut);
unsigned char GetFreeCP(void);
void SetFreeCP(unsigned char btCP);
InputIpBufTP *GetFreeInputIpBufAddr(void);
void AddInd2MasInputIp4ISR(unsigned char Chan,unsigned char btInd);
void StartAudio4IPPM_Multicast(void);
void StopAudio4IPPM_Multicast(void);
void StartAudioFromIPPM(unsigned char btCP);
void StopAudioFromIPPM(unsigned char btCP);
void StartAudio4UK(void);
void StopAudio4UK(void);
void StartAudioFromUK(void);
void StopAudioFromUK(void);

unsigned char GetInputCP4Numb(unsigned char Numb);

unsigned short a_compress(unsigned short input);
unsigned short a_expand (unsigned short input);

unsigned short GetStudioNumber(void);

// Chan 1..4 - ���������� ������, 5 - UK, 6 - multicast IPPM
// GenMode: 0 - ���, 1 - �����, 2 - ����� ���
void StartGenerator(unsigned char btChan,unsigned short wGenMode);
void StopGenerator(unsigned char btChan);

void CloseUKRTP(void);
void CreateUKRTPUnicast(unsigned int dwIP);
void CreateUKRTPMulticast(unsigned int dwIP);

void SetLvlIPPM(unsigned short IPPM_Numb,unsigned int Lvl);

/////////////////////////////////////////////////////////////////////
void Init_EBIU(void);
void Init_SPI(void);
void Init_Timer0(void);
void Init_IP(void);
void Init_Interrupts(void);

void Timer0(int val);

/////////////////////////////////////////////////////////////////////
void WaitWhileReadDSCCfg(void);
void ReadDSCCfg(void);

char *GetString2Buf(char *pStart);
char *FindSection(char *pStart,char *pName);
char *FindEndSection(char *pStart);
char *GetParamFromBuf(char *pName);
char *GetParam(char *pStart,char *pName);
unsigned int Str2IP(char *pStr);
void GetIPAddr(char *pStart,unsigned char *pAddr);
unsigned short Str2Port(char *pStart);
unsigned short GetIPPort(char *pStart,unsigned short wPort0);
unsigned short GetParamWord(char *pStart);
bool GetParamBool(char *pStart);

void InitIpParams(IPPARAMS_TP *pIpParams);
void SetIpParams(IPPARAMS_TP *pIpParams);
void SetStudioNumb(unsigned short wPar);
void SetUPLvlIpAddr(unsigned int dwIP);
void SetUPLvlPortCtrl(unsigned short wPort);
void SetUPLvlPortSound(unsigned short wPort);
void SetAdcGain4Ch(char Chan,unsigned char btCode);
void SetDacVol4Ch(char Chan,unsigned char btCode);
void ChanOutputEnable(unsigned char btChan,unsigned short wPar); // Chan 1..4 - ���������� ������, 5 - UK, 6 - multicast IPPM
void ChanInputEnable(unsigned char btChan,unsigned short wPar); // Chan 1..4 - ���������� ������, 5 - UK, 6.. - IPPM
void SetMaxIPPM(unsigned short wPar);
void SetMaxSpeakIPPM(unsigned short wPar);
void SetMasterIPPM(unsigned short wPar);
void SetMasterLvl(unsigned short wPar);
void SetVCSIpAddr(unsigned int dwIP);
void SetVCSPort(unsigned short wPort);
void SetViscaSpeed(int iSpeed);
void SetVCSCameraDefault(unsigned short wVCam);
void SetVCSType(unsigned char btType);
void ClearVCSPresets(void);
void AddVCSPreset(unsigned char btIPPM,unsigned char btVCam,unsigned char btPreset);
void ReadDSCCfgFromBuf(char *pchBuf);

/////////////////////////////////////////////////////////////////////

