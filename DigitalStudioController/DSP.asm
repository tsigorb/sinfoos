.section/DOUBLEANY program;
.align 2;

/*******************************************************************************
Function Name  : _a_compress

Synopsis       : unsigned short a_compress (input);

Description    : This function computes A-law compression of the given
                 input vector.

Operands       : R0 - input data

Registers Used : R0 - Current input data
                 R1 - ABS(Current input value)
                 R2 - Output value
                 R3 - Exponent of input data
                 R4 - 4095 (maximum input value)
                 R5 -   63 (constant used to test for input data less than 64)
                 R6 -   26 (constant used to convert signbits to chord)
                 R7 - 0x55 (pattern used for inverting bit values)
********************************************************************************/
.global  _a_compress;
_a_compress:
      [--SP] = (R7:4);                      // Preserve R7-R4 on the stack
    /* Initialize Registers */
      R7 = 0x55;                            // Initialize R7 to 0x55
      R6 = 26;                              // Initialize R6 to 26
      R5 = 63;                              // Initialize R5 to 63
      R4 = 4095;                            // Initialize R4 to 4095
      R3 = 0;                               // Initialize R3 to 0

      R0=R0.L (X);
    /* Clip to Maximum */

      R1 = ABS R0;                       // R0 = ABS(input data)
      R1 = MIN(R1, R4);                  // R0 = MIN(ABS(input data),4095)

    /* IF (Input Data < 64) ... */

        R2 = R1 >> 1;

        CC = R1 <= R5;
        IF CC R1 = R2;                     // Drop the least significant bit
        IF CC JUMP GET_SIGN;               // Jump to finalize the output

//GET_CHORD:
      /* ... ELSE Get Chord and Step */

        R3.L = SIGNBITS R1;                // Count redundant sign bits
        R2 = R6 - R3;                      // Chord = 26 - Signbits(input)
        R3 = R1;                           // Step =
        R3 >>= R2;                         //        (ABS(input) >> chord)
        BITCLR(R3,4);                      //        & 0xffffffef

        R1 = R2 << 4;                      // Position Chord
        R1 = R1 | R3;                      // R1 = (Chord<<4) | Step

GET_SIGN:
      /* Get Sign and Invert every other Bit */

        CC = R0 < 0;                       // Test if input data is negative
        IF CC JUMP NEGATIVE;               // Skip if it is
        BITSET(R1,7);

NEGATIVE:
        R1 = R1 ^ R7;                      // Invert every other bit
      R0=R1;   // Return R0
      (R7:4) = [SP++];                      // Restore registers
      rts;  
._a_compress.end:

/******************************************************************************
Function Name  : _a_expand

Synopsis       : unsigned short a_expand (unsigned short input);
                 
Description    : This function computes A-law expansion of the given
                 input vector.

Operands       : R0 - input dsts

Registers Used : R0 - Current input value, and work register
                 R1 - Chord from input value
                 R2 - Step from input value, and output value
                 R3 - 0x0f   (pattern used to extract the step value)
                 R6 - 0x0403 (bit extraction mask for the chord - pos=4, len=3)
                 R7 - 0x55   (pattern used for inverting bit values)
                 I1 - Pointer to current output
                 P0 - Loop counter through input data
                 P1 - Pointer to current input

********************************************************************************/
.global          _a_expand;
_a_expand:
      [--SP] = (R7:5);                      // Preserve R7-R5 on the stack
      /* Initialize Registers */
      R7 = 0x55;                            // Initialize R7 to 0x55
      R6 = 0x0403;                          // Initialize R6 to 0x0403
      R3 = 0x0F;                            // Initialize R3 to 15

      R0 = R0.L (X);
      R0 = R0 ^ R7;                      // Undo bit inversion by a_compress

      /* Extract the Step */

      R2 = R0 & R3;                      // Extract Step (bits 0,1,2,3)

      /* Prepare Step */

      R2 <<= 1;                          // Step = Step << 1;
      R2 += 1;                           // Step = Step + 1;

      /* Extract the Chord */
      R1 = EXTRACT(R0,R6.L) (Z);         // Extract Chord (bits 4,5,6)

      /* IF (Chord \= 0) ... */

      CC = R1 == 0;
      R1 += -1;
      R5 = R2; 
      BITSET(R5,5);
      R5 <<= R1;
      IF !CC R2 = R5;

      /* Check Sign bit in Input */

      CC = BITTST(R0,7);
      R1 = -R2;
      IF !CC R2 = R1;

      R0 = R2;                    // Store the result in output array
     (R7:5) = [SP++];             // Restore registers
     rts;
._a_expand.end:
