#include "Sinfo.h"
#include "GlobalData.h"

/////////////////////////////////////////////////////////////////////
#define d_IpLiveTOut     4000

typedef struct
{
  unsigned long int dwIp;
  unsigned short wPort;
  unsigned char btMAC[6];
  unsigned int dwTOut;
  unsigned char btType,btSN;
} IRP_TP;

bool GetMAC4IPPM(unsigned int dwIP,unsigned char *MACAddr);
bool GetMACAddr4UK(unsigned int dwIP,unsigned char *MACAddr);

/////////////////////////////////////////////////////////////////////
static IRP_TP s_MassIRP[IRP_MAX]; // ������ ����������� � 0 �� NumberKeyboard-1, ����� - ��� ���������!!
volatile static bool s_bNeedIpReinit=false;
static unsigned int s_dwUKTOut=0;

static char s_strInfo[128];

////////////////////////////////////////////////////////////////////
void InitIRP(void)
{
  memset(s_MassIRP,0,sizeof(s_MassIRP));
  s_dwUKTOut=xTaskGetTickCount()+1000;
}

bool IsNeedIpReinit(void)
{
  bool bNeed=s_bNeedIpReinit;
  s_bNeedIpReinit=false;
  return(bNeed);
}

void SetNeedIpReinit(void)
{ s_bNeedIpReinit=true; }

void IRPTOut(void)
{
  unsigned char btInd,PultNb;
  static section ("bigdata") sInfoKadr sInfo;

  for(btInd=0;btInd<IRP_MAX;btInd++)
  {
    if(s_MassIRP[btInd].dwTOut && IsCurrentTickCountGT(s_MassIRP[btInd].dwTOut))
    {
      memset(s_MassIRP+btInd,0,sizeof(IRP_TP));
    }
  }
  if(IsCurrentTickCountGT(s_dwUKTOut))
  { // ��������� ����������� ��������� ����� 1 ��� � ������� ���� ������������������ �����������
    for(btInd=0;btInd<IRP_MAX;btInd++)
    {
      switch(s_MassIRP[btInd].btType)
      {
        case IP_LOGGER:
          sInfo.L=2;
          sInfo.ByteInfoKadr[0]=254;
          sInfo.ByteInfoKadr[1]=0xff;
          sInfo.A1=s_MassIRP[btInd].dwIp;
          sInfo.PORT1=s_MassIRP[btInd].wPort;
          xQueueSend(xQueueReply[UserUDP],&sInfo,0,0);
          break;
        default:
          break;
      }
    }
    s_dwUKTOut=xTaskGetTickCount()+1000;
  }
}

unsigned char FindIpReg(unsigned char btType,
                        unsigned int *pA1,unsigned short *pPORT1,
                        unsigned char *pbtCnt,unsigned char *pbtIndFree)
{
  unsigned char btInd,btIndMax;
  unsigned short PORT1=0;
  unsigned int A1=0;

  if(pbtCnt) *pbtCnt=0;
  if(pbtIndFree) *pbtIndFree=0xff;
  if(pA1) A1=*pA1;
  if(pPORT1) PORT1=*pPORT1;
  btIndMax=IRP_MAX;
  for(btInd=0;btInd<btIndMax;btInd++)
  {
    if(pbtIndFree && (*pbtIndFree==0xff) && (s_MassIRP[btInd].btType==IP_NONE)) *pbtIndFree=btInd;
    if(s_MassIRP[btInd].btType==btType)
    {
      if(pbtCnt) (*pbtCnt)++;
      if(!A1)
      {
        if(pA1) *pA1=s_MassIRP[btInd].dwIp;
        if(pPORT1) *pPORT1=s_MassIRP[btInd].wPort;
        return(btInd);
      }
      if(s_MassIRP[btInd].dwIp==A1)
      {
        if(PORT1)
        {
          if(s_MassIRP[btInd].wPort==PORT1) return(btInd);
          return(0xff);
        }
        else return(btInd);
      }
    }
  }
  return(0xff);
}

void IRP_Registration(unsigned char btType,unsigned int A1,unsigned short PORT1,unsigned char *pbtMAC)
{
  unsigned char btInd,btCnt,btIndFree,btMaxId,btSN;

  switch(btType)
  {
    case IP_DSPCOM:
      btMaxId=d_MAXDSPCOM;
      btSN=0xff;
      break;
    case IP_LOGGER:
      btMaxId=d_MAXLOGGER;
      btSN=0xfc;
      break;
    default: return;
  }
  btInd=FindIpReg(btType,&A1,NULL,&btCnt,&btIndFree);
  if(btInd==0xff)
  {
    if((btCnt>=btMaxId) || (btIndFree==0xff)) return;
    s_MassIRP[btIndFree].btType=btType;
    s_MassIRP[btIndFree].btSN=btSN;
    s_MassIRP[btIndFree].wPort=PORT1;
    s_MassIRP[btIndFree].dwIp=A1;
    s_MassIRP[btIndFree].dwTOut=xTaskGetTickCount()+d_IpLiveTOut;
    if(pbtMAC) memcpy(s_MassIRP[btIndFree].btMAC,pbtMAC,6);
    else memset(s_MassIRP[btIndFree].btMAC,0,6);
    if(btType==IP_LOGGER) SendStr2IpLogger("$REGISTRATION$IP-LOGGER REGISTRATION!!!");
  }
  else
  {
    s_MassIRP[btInd].wPort=PORT1;
    s_MassIRP[btInd].dwTOut=xTaskGetTickCount()+d_IpLiveTOut;
    if(pbtMAC &&
       (pbtMAC[0] | pbtMAC[1] | pbtMAC[2] | pbtMAC[3] | pbtMAC[4] | pbtMAC[5])) memcpy(s_MassIRP[btInd].btMAC,pbtMAC,6);
  }
}

void IRP_Unregistration(unsigned char btType,unsigned int A1)
{
  unsigned char btInd;

  btInd=FindIpReg(btType,&A1,NULL,NULL,NULL);
  if(btInd!=0xff) memset(s_MassIRP+btInd,0,sizeof(IRP_TP));
}

void IRP_Check(unsigned char btType,unsigned int A1)
{
  unsigned char btInd;

  btInd=FindIpReg(btType,&A1,NULL,NULL,NULL);
  if(btInd!=0xff) s_MassIRP[btInd].dwTOut=xTaskGetTickCount()+d_IpLiveTOut;
}

void SendDiagnostics2DspCom(unsigned char *pData,unsigned char Len)
{
  unsigned char btInd;
  static section("bigdata") sInfoKadr sInfo;

  if(!Len) return;
  vPortEnableSwitchTask(false);
  sInfo.L=Len;
  memcpy(sInfo.ByteInfoKadr,pData,Len);
  for(btInd=0;btInd<IRP_MAX;btInd++)
  {
    if(s_MassIRP[btInd].btType==IP_DSPCOM)
    {
      sInfo.A1=s_MassIRP[btInd].dwIp;
      sInfo.PORT1=s_MassIRP[btInd].wPort;
      xQueueSend(xQueueReply[UserUDP],&sInfo,0,0);
    }
  }
  vPortEnableSwitchTask(true);
}


bool GetMACAddrFromIp(unsigned int dwIP,unsigned char *MACAddr)
{ // ���������� ������������ ����� �� �����, �.�. ��� �-��� �������� ������ � IP ������
  unsigned char btInd,*pbtMAC;
  btInd=dwIP & 0xff;
  if(!dwIP || ((btInd>0xdf) && (btInd<0xf0))) return(false); // e0.xx.xx.xx - ef.xx.xx.xx multicast
//if(dwIP==0x2101a8c0) return GetMAC_DSC(0,MACAddr); //!!!
  for(btInd=0;btInd<IRP_MAX;btInd++)
  {
    if((s_MassIRP[btInd].btType!=IP_NONE) && (s_MassIRP[btInd].dwIp==dwIP))
    {
      pbtMAC=s_MassIRP[btInd].btMAC;
      if(pbtMAC[0] | pbtMAC[1] | pbtMAC[2] | pbtMAC[3] | pbtMAC[4] | pbtMAC[5])
      { memcpy(MACAddr,pbtMAC,6); return(true); }
      return(false);
    }
  }
  if(GetMAC4IPPM(dwIP,MACAddr)) return true;
  return GetMACAddr4UK(dwIP,MACAddr);
}

