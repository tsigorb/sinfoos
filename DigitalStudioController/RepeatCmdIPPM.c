#include "Sinfo.h"
#include "GlobalData.h" 

//*****************************************************************************
typedef struct
{
  unsigned char Numb;
  unsigned short wCmd;
  bool bSendAnsw;
  sInfoKadr sInfo;
} LAST_UK_IPPM_CMD_TP;

//*****************************************************************************
static LAST_UK_IPPM_CMD_TP MasLastIPPMCmd[d_MaxAbntIPPM],
                           LastUKCmd;

//*****************************************************************************
void InitRepeatCmdUK_IPPM(void)
{
  memset(MasLastIPPMCmd,0,sizeof(MasLastIPPMCmd));
  memset(&LastUKCmd,0,sizeof(LastUKCmd));
}

void DelRepeatCmdUK_IPPM(unsigned char btIF,unsigned char Numb)
{
  LAST_UK_IPPM_CMD_TP *psLastCmd;
  
  if(btIF) psLastCmd=MasLastIPPMCmd+(Numb-1);
  else psLastCmd=&LastUKCmd;
  if(psLastCmd->Numb)
  {
    psLastCmd->Numb=0;
    psLastCmd->bSendAnsw=false;
  }
}

void SendAnswUK_IPPMCmd(unsigned char btIF,unsigned char Numb,unsigned short wCmd,sInfoKadr *psInfo)
{
  LAST_UK_IPPM_CMD_TP *psLastCmd;
  
  if(btIF) psLastCmd=MasLastIPPMCmd+(Numb-1);
  else psLastCmd=&LastUKCmd;
  if(psLastCmd->Numb)
  {
//SendStr2IpLogger("$DEBUG$SendAnsw4UK_IPPMCmd: IF=%d, Numb=%d, CmdId=%d",btIF,Numb,wCmd);
    psLastCmd->wCmd=wCmd;
    psLastCmd->bSendAnsw=true;
    memcpy(&psLastCmd->sInfo,psInfo,sizeof(sInfoKadr));
  }
/*{
static int s_iCycle=0;
s_iCycle++; s_iCycle&=0x1;
if(s_iCycle) return;
}*/
  xQueueSend(xQueueReply[psInfo->Source],psInfo,0,0);
}

bool RepeatCmdUK_IPPM(unsigned char btIF,unsigned char Numb,unsigned short wCmd)
{
  LAST_UK_IPPM_CMD_TP *psLastCmd;
  
  if(btIF) psLastCmd=MasLastIPPMCmd+(Numb-1);
  else psLastCmd=&LastUKCmd;
  if(psLastCmd->Numb)
  {
    if(wCmd==psLastCmd->wCmd)
    {
      if(psLastCmd->bSendAnsw)
      {
//SendStr2IpLogger("$DEBUG$RepeatCmdUK_IPPM repeat answer: IF=%d, Numb=%d, CmdId=%d",btIF,Numb,wCmd);
        xQueueSend(xQueueReply[psLastCmd->sInfo.Source],&(psLastCmd->sInfo),0,0);
        return(true);
      }
    }
    else
    {
      psLastCmd->wCmd=wCmd;
      psLastCmd->bSendAnsw=false;
//SendStr2IpLogger("$DEBUG$RepeatCmdUK_IPPM new cmd: IF=%d, Numb=%d, CmdId=%d",btIF,Numb,wCmd);
    }
  }
  else
  {
    psLastCmd->Numb=Numb;
    psLastCmd->wCmd=wCmd;
    psLastCmd->bSendAnsw=false;
//SendStr2IpLogger("$DEBUG$RepeatCmdUK_IPPM new cmd: IF=%d, Numb=%d, CmdId=%d",btIF,Numb,wCmd);
  }
  return(false);
}

