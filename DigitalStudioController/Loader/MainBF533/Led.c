#include "LoaderBF533.h"

// ��������� ���������� � �������� �������
void LedOn(void)
{
  *pFIO_FLAG_S=PF1;
  ssync();
}

// ���������� ���������� � �������� �������
void LedOff(void)
{
  *pFIO_FLAG_C=PF1;
  ssync();
}

// ������������ ���������� � �������� �������
void LedToggle(void)
{
  *pFIO_FLAG_T=PF1;
  ssync();
}

void LedPulse(unsigned int dwLedTOut0)
{
  static unsigned int dwLedTOut=0;
  
  if(dwLedTOut) dwLedTOut--;
  else
  {
    dwLedTOut=dwLedTOut0;
    LedToggle();
  }
}

