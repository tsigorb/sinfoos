#include <cdefBF533.h>
#include <sys\exception.h>
#include <ccblkfn.h>
#include <sysreg.h>
#include "Led.h"

// SPI transfer mode
#define TIMOD_01        0x0001

#define CoeffMult       30 //����������� ��������� ����������

#define CCLK            491520
#define SCLK            49152

#define IVG_PLL_WAKEUP	P0_IVG
#define IVG_DMA_ERROR	P1_IVG
#define IVG_PPI_ERROR	P2_IVG
#define IVG_SPT0_ERROR	P3_IVG
#define IVG_SPI_ERROR	P4_IVG
#define IVG_SPT1_ERROR	P5_IVG
#define IVG_UART_ERROR	P6_IVG
#define IVG_RTC			P7_IVG
#define IVG_PPI			P8_IVG
#define IVG_SPT0_RX		P9_IVG
#define IVG_SPT0_TX		P10_IVG
#define IVG_SPT1_RX		P11_IVG
#define IVG_SPT1_TX		P12_IVG
#define IVG_SPI			P13_IVG
#define IVG_UART_RX		P14_IVG
#define IVG_UART_TX		P15_IVG
#define IVG_TIMER0		P16_IVG
#define IVG_TIMER1		P17_IVG
#define IVG_TIMER2		P18_IVG
#define IVG_PFA			P19_IVG
#define IVG_PFB			P20_IVG
#define IVG_MEMDMA0		P21_IVG
#define IVG_MEMDMA1		P22_IVG
#define IVG_SWDT		P23_IVG

//����� ��� SIC_IMASK 
#define IRQ_PLL_WAKEUP	0x00000001
#define IRQ_DMA_ERROR	0x00000002
#define IRQ_PPI_ERROR	0x00000004
#define IRQ_SPT0_ERROR	0x00000008
#define IRQ_SPT1_ERROR	0x00000010
#define IRQ_SPI_ERROR	0x00000020
#define IRQ_UART_ERROR	0x00000040
#define IRQ_RTC			0x00000080
#define IRQ_PPI			0x00000100
#define IRQ_SPT0_RX		0x00000200
#define IRQ_SPT0_TX		0x00000400
#define IRQ_SPT1_RX		0x00000800
#define IRQ_SPT1_TX		0x00001000
#define IRQ_SPI			0x00002000
#define IRQ_UART_RX		0x00004000
#define IRQ_UART_TX		0x00008000
#define IRQ_TIMER0		0x00010000
#define IRQ_TIMER1		0x00020000
#define IRQ_TIMER2		0x00040000
#define IRQ_PFA			0x00080000
#define IRQ_PFB			0x00100000
#define IRQ_MEMDMA0		0x00200000
#define IRQ_MEMDMA1		0x00400000
#define IRQ_SWDT		0x00800000

// DMA flow mode=1 - ����� ���������������
#define FLOW_1          0x1000

#define pFlash         (volatile unsigned char *)0x20000000
#define pIP            (volatile unsigned char *)0x20100000

#define pRHR	       (volatile unsigned char *)0x202f0000
#define pTHR	       (volatile unsigned char *)0x202f0000
#define pIER	       (volatile unsigned char *)0x202f0002
#define pFCR	       (volatile unsigned char *)0x202f0004
#define pISR	       (volatile unsigned char *)0x202f0004
#define pLCR	       (volatile unsigned char *)0x202f0006
#define pMCR	       (volatile unsigned char *)0x202f0008
#define pLSR	       (volatile unsigned char *)0x202f000a
#define pMSR	       (volatile unsigned char *)0x202f000c
#define pSPR	       (volatile unsigned char *)0x202f000e
#define pDLL	       (volatile unsigned char *)0x202f0000
#define pDLM	       (volatile unsigned char *)0x202f0002
#define pLED	       (volatile unsigned char *)0x202f0010

#define pRHR1	       (volatile unsigned char *)0x203f0000
#define pTHR1	       (volatile unsigned char *)0x203f0000
#define pIER1	       (volatile unsigned char *)0x203f0002
#define pFCR1	       (volatile unsigned char *)0x203f0004
#define pISR1	       (volatile unsigned char *)0x203f0004
#define pLCR1	       (volatile unsigned char *)0x203f0006
#define pMCR1	       (volatile unsigned char *)0x203f0008
#define pLSR1	       (volatile unsigned char *)0x203f000a
#define pMSR1	       (volatile unsigned char *)0x203f000c
#define pSPR1	       (volatile unsigned char *)0x203f000e
#define pDLL1	       (volatile unsigned char *)0x203f0000
#define pDLM1	       (volatile unsigned char *)0x203f0002

#define CodeStatusTrue      0
#define CodeStatusFalse     255 

#define d_BufMaxSz          1500
#define d_XchgMaxSz         260

//--------- ��������� ��������������� ����� ��� ������
typedef struct 
{ 	unsigned char Source; //�������� �������
	unsigned char L; // �����
	unsigned int A1; // ����� ����������
	unsigned short PORT1; // ���� ����������
	unsigned char ByteInfoKadr[d_XchgMaxSz]; //��������� �� ����� �����
} sInfoKadr;

//--------------- ��������� �������
typedef struct 
{ 	unsigned char Source; //�������� �������
	unsigned char L; // ����� ������ �������
	unsigned int A1; // ����� ����������
	unsigned short PORT1; // ���� ����������
	unsigned char BufCommand[d_XchgMaxSz]; //��������� �� ����� �������
} sCommand;

/////////////////////////////////////////////////////////////////////
#define SEL_CONTROL     0   // Confirm socket status
#define SEL_SEND        1   // Confirm Tx free buffer size
#define SEL_RECV        2   // Confirm Rx data size

#define d_CommandSocket 1   // ����� ������ ��� �������� ������
// !! ������������ ����� ������ - 3 !!

typedef struct
{
unsigned char ucDataSIPR[4]; // IP address
unsigned char ucDataMSR[4]; // Subnet mask
unsigned char ucDataGAR[4]; // Gateway address
unsigned char ucDataSHAR[6]; // MAC address - DO NOT USE THIS ON A PUBLIC NETWORK!
unsigned short wPortControl,wPortSound;
} IPPARAMS_TP;

// ������������� ������� Flash-������ AT45DB161D �� ����������� � ��������
//   0x000000 - 0x1FFFFF ������ ���������� (SF1==PF2) (���������,���������,�������� ��������� � ������ �������� �������)
//   0x000000 - 0x00DFFF ��������� ���������
//   0x00E000 - 0x00FFFF ������� ����������
//   0x010000 - 0x0FFFFF �������� ��������� ������
//   0x100000 - 0x1FFFFF �������� ������� (������)
//   0x200000 - 0x3FFFFF ������ ���������� �������� ������� (SF2-PF4)

#define AdrParam2Flash   0xe000
#define AdrSN            AdrParam2Flash+0x0    // 4 ����� - �������� �����
#define AdrFlashLastFile AdrParam2Flash+0x200  // 4 ����� - ����� ���������� ����� �� ����
#define dAddrPgm2Flash   0x10000
#define dAddrFS2Flash    0x100000
#define dAddrPgm2SDRAM   0xA00000

#define d_SyncByte       0x16

typedef void (*FuncPtr)(void);

void EnableWrite2ProgrammArea(bool bEn);
void SetIpCS(unsigned short wCS);
void Init_SPI(void);
bool RdFlashP(unsigned int InpAddr,unsigned short *lpOut,unsigned int *lpHowOut);
bool WrDeviceArray(unsigned int Addr0, unsigned short *lpInp, unsigned int HowInp);

void memcpy(void *pAdrDst0,const void *pAdrSrs0,unsigned int CountDan);
unsigned int GetPgmAddress(void);
bool IsProgramInFlash(void);
bool CheckLdr(bool bFlash);
bool ZagrLdr(bool bFlash);

void ResetWatchDog(bool bNow);

bool IsCurrentTickCountGT(unsigned int dwTm);
bool IsCurrentTickCountLT(unsigned int dwTm);
//void vTaskDelay(unsigned int xTicksToDelay);

