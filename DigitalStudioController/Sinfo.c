#include "Sinfo.h"
#include "GlobalData.h"

#define NumTekVer "Ver 01.00.00 "   // ������� ������ ��

#define TekVer NumTekVer, __DATE__ " " __TIME__,"DigitalStudioController "," " // ������ ��

char FwVersionInfo[4][24]={TekVer}; // version number

///////////////////////////////////////////////////////////////////////////////
volatile bool g_bPrinfBusy=false;

///////////////////////////////////////////////////////////////////////////////
unsigned int GET_DW(unsigned char *pbtBuf)
{
  unsigned int dwCode;
  dwCode=pbtBuf[3];
  dwCode=(dwCode<<8) | pbtBuf[2];
  dwCode=(dwCode<<8) | pbtBuf[1];
  dwCode=(dwCode<<8) | pbtBuf[0];
  return(dwCode);
}

void SET_DW(unsigned char *pbtBuf,unsigned int dwData)
{
  pbtBuf[3]=dwData >> 24;
  pbtBuf[2]=dwData >> 16;
  pbtBuf[1]=dwData >> 8;
  pbtBuf[0]=dwData;
}

unsigned short GET_W(unsigned char *pbtBuf)
{
  unsigned short wCode;
  wCode=pbtBuf[1];
  wCode=(wCode<<8) | pbtBuf[0];
  return(wCode);
}

unsigned short ReverseShort(unsigned short data)
{ return ((data >> 8) & 0x00FF) | ((data << 8) & 0xFF00); }

unsigned int ReverseInt(unsigned int data)
{return ((data >> 24) & 0x000000FF) | ((data >> 8) & 0x0000FF00) | ((data << 8) & 0x00FF0000) | ((data << 24) & 0xFF000000);}

///////////////////////////////////////////////////////////////////////////////
unsigned char *SaveInt2Buf(unsigned char *pbtBuf,int iData)
{
  pbtBuf[0]=iData >> 24;
  pbtBuf[1]=iData >> 16;
  pbtBuf[2]=iData >> 8;
  pbtBuf[3]=iData;
  return(pbtBuf+4);
}

unsigned char *RestoreIntFromBuf(unsigned char *pbtBuf,int *piData)
{
  int iData;
  iData=pbtBuf[0];
  iData=(iData << 8) | pbtBuf[1];
  iData=(iData << 8) | pbtBuf[2];
  iData=(iData << 8) | pbtBuf[3];
  *piData=iData;
  return(pbtBuf+4);
}

bool IsDigit(char chSb)
{
  if((chSb>='0') && (chSb<='9')) return(true);
  return(false);
}

unsigned char ConvertStrNumb2Byte(unsigned char *btMasNumb,unsigned char btLn)
{
  if(btLn==1)
  {
    if(IsDigit((char)btMasNumb[0])) return(btMasNumb[0]-'0');
  }
  else
  {
    if(btLn>=2)
    {
      if(IsDigit((char)btMasNumb[0]) && IsDigit((char)btMasNumb[1])) return((btMasNumb[0]-'0')*10+(btMasNumb[1]-'0'));
    }
  }
  return(0);
}

char *strclr(char *str)
{
  int i,j;
  unsigned char *str1,*str2;

  if(!str || !(*str)) return(NULL);
  i=1; str1=(unsigned char *)str;
  for(j=0;j<2;j++)
  {
    while((*str1) && (*str1<=' ')) str1+=i;
    if(!(*str1)) return(NULL);
    if(!j)
    {
      str2=str1;
      str1=(unsigned char *)(str+strlen((char *)str)-1);
      i=-1;
    }
  }
  str1++; *str1=0;
  return((char *)str2);
}

//################ �-��� ��������� �������� ������� � �������� #################
unsigned char IsCurrentTickCountGT(unsigned int dwTick0)
{
  unsigned char btState;
  unsigned int dwTick1=xTaskGetTickCount();

  btState=(unsigned char)((dwTick0 >> 30) | ((dwTick1 >> 29) & 0x02));
  switch(btState)
  {
    case 0x00:
    case 0x03:
      if(dwTick1>dwTick0) return(1);
      return(0);
    case 0x01:
      return(1);
    case 0x02:
      if(dwTick1-dwTick0>0x7fffffff) return(0);
      return(1);
  }
  return(0);
}

unsigned char IsCurrentTickCountLT(unsigned int dwTick0)
{
  unsigned char btState;
  unsigned int dwTick1=xTaskGetTickCount();

  btState=(unsigned char)((dwTick0 >> 30) | ((dwTick1 >> 29) & 0x02));
  switch(btState)
  {
    case 0x00:
    case 0x03:
      if(dwTick0>dwTick1) return(1);
      return(0);
    case 0x01:
      return(0);
    case 0x02:
      if(dwTick1-dwTick0>0x7fffffff) return(1);
      return(0);
  }
  return(0);
}

// ##### ������ �� ������������ #####
unsigned int g_dwStatusLedTOut=d_LedTOutOk;
volatile static unsigned char LEDdata;

// ������������� ����������� - ��� ��������
void LedInit(void)
{
  LEDdata=0;  // ��������� ������, ����������� ������ �����������
  *pFIO_FLAG_S=PF1;
  ssync();
}

// ��������� ���������� � �������� �������
void LedOn(void)
{
  LEDdata=1;  // ����������� ������, ����������� ������ �����������
  *pFIO_FLAG_C=PF1;
  ssync();
}

// ���������� ���������� � �������� �������
void LedOff(void)
{
  LEDdata=0;  // ����������� ������, ����������� ������ �����������
  *pFIO_FLAG_S=PF1;
  ssync();
}
// ������������ ���������� � �������� �������
void LedToggle(void)
{
  LEDdata^=1;  // ����������� ������, ����������� ������ �����������
  *pFIO_FLAG_T=PF1;
  ssync();
}

// ��������� ���������� � �������� �������
bool LedStatus(void)
{
  return (LEDdata & 1);  // ������ ������, ����������� ������ �����������
}

void LedWorkIndicator(void)
{
  static portTickType xTimeToWake=0;

  if(IsCurrentTickCountLT(xTimeToWake)) return;
  if(LedStatus())
  { // ����� ���������
    xTimeToWake=xTaskGetTickCount()+1500;
    LedOff();
  }
  else
  { // �������� ���������
    xTimeToWake=xTaskGetTickCount()+g_dwStatusLedTOut;
    LedOn();
  }
}

//###################### ������ � ���������� ����� � ���������� �������� ##########################
#pragma pack(1)
typedef struct
{
  unsigned int dwCycle;
//  unsigned int dwFreeStackSize;
//  unsigned char btMasTaskVar[4];
} TASK_STATE_TP;
#pragma pack()

unsigned int GetFreeTaskStackSize(void);

/////////////////////////////////////////////////////////////////////
static TASK_STATE_TP s_MassTaskState[d_MaxTask];

volatile static unsigned int s_dwWDTaskState=0;

/////////////////////////////////////////////////////////////////////
signed portBASE_TYPE xQueueSendCmd(sCommand *psCmd,portTickType xTicksToWait)
{
  signed portBASE_TYPE Result;
  static sInfoKadr sReply;

/*  if(psCmd->Source==UserRS485)
  {  
    while(xQueueReceive(xQueueReply[UserRS485],&sReply,1));
  }*/
  if(psCmd->BufCommand[0]==10) Result=xQueueSend(xQueueSportX,psCmd,xTicksToWait,0);
  else Result=xQueueSend(xQueueCommand,psCmd,xTicksToWait,0);
  vTaskDelay(1);
  return(Result);
}

void SetTaskVar(unsigned int dwTask,unsigned char btOff,unsigned char btVar)
{
  unsigned char btPos=WDSTATE2NUMB(dwTask);

  if((btPos<d_MaxTask) && (btOff<4))
  {
//    s_MassTaskState[btPos].btMasTaskVar[btOff]=btVar;
  }
}

void SetWDState(unsigned int dwTask)
{
  unsigned char btPos=WDSTATE2NUMB(dwTask);
  if(btPos<d_MaxTask)
  {
    vPortEnableSwitchTask(false);
    s_dwWDTaskState|=dwTask;
    s_MassTaskState[btPos].dwCycle++;
//    s_MassTaskState[btPos].dwFreeStackSize=GetFreeTaskStackSize();
    vPortEnableSwitchTask(true);
  }
}

/*void SendTaskState2Ip(void)
{
  unsigned char btPos,btOff;
  static unsigned int s_dwTOut=0;
  static sInfoKadr sInfo;

  if(IsCurrentTickCountLT(s_dwTOut)) return;
  if(GetControlIpAddr(&sInfo.A1,&sInfo.PORT1))
  {
    vPortEnableSwitchTask(false);
    sInfo.Source=UserUDP;
    memset(sInfo.ByteInfoKadr,0,sizeof(sInfo.ByteInfoKadr));
    sInfo.ByteInfoKadr[0]=114;
    btOff=1;
    for(btPos=0;btPos<d_MaxTask;btPos++)
    {
      if(d_WDSTATE_TASKALL & (0x01ul << btPos))
      {
        memcpy(sInfo.ByteInfoKadr+btOff,s_MassTaskState+btPos,sizeof(TASK_STATE_TP));
      }
      btOff+=sizeof(TASK_STATE_TP);
    }
    sInfo.L=btOff;
//    xQueueSend(xQueueReply[sInfo.Source],&sInfo,10);
    vPortEnableSwitchTask(true);
  }
  s_dwTOut=xTaskGetTickCount()+1000;
}*/

// ����� ����������� �������
void ResetWatchDog(bool bNow)
{
  static portTickType xTimeToWatchDog=0;
  
  if(bNow || IsCurrentTickCountGT(xTimeToWatchDog))
  {
    unsigned int i;

    vPortEnableSwitchTask(false);
    for(i=0;i<0x4;i++)
    {
      *pFIO_FLAG_S=PF0;
      ssync();
    }
    *pFIO_FLAG_C=PF0;
    ssync();
    s_dwWDTaskState=0;
    vPortEnableSwitchTask(true);
    xTimeToWatchDog=xTaskGetTickCount()+100; // �������� 100 ��
  }
}

// ���������� ������ ����������� ������� ��� ����������� �������
void BlockWatchDog(void)
{
  cli();
  while(1);
}

