#include "../Sinfo.h"
#include "../GlobalData.h"
#include <math.h>

/////////////////////////////////////////////////////////////////////
typedef struct
{
  unsigned char btMic,btVCam,btPreset;
} PRESETINFO_TP;

typedef struct
{
  unsigned short wType; // 0 - none, 1 - YeaLink, 2 - Granat, 3 - Visca RS-232
  unsigned int dwIpAddr;
  unsigned short wPort;
  unsigned int dwSpeed; // for Visca
  unsigned short wVCamDef;
  unsigned short wPresetCnt;
  PRESETINFO_TP MasPreset[d_MAX_PRESET];
} VCSINFO_TP;

/////////////////////////////////////////////////////////////////////
unsigned char GetSpeakIPPM(void);

/////////////////////////////////////////////////////////////////////
static VCSINFO_TP sVCSInfo;

/////////////////////////////////////////////////////////////////////
bool isVCS_YeaLink(void)
{ return (sVCSInfo.wType==d_VCS_YeaLink) ? true : false; }

bool isVCS_Granat(void)
{ return (sVCSInfo.wType==d_VCS_Granat) ? true : false; }

unsigned short VCSGetPort(void)
{ return sVCSInfo.wPort; }

unsigned int VCSGetIpAddr(void)
{ return sVCSInfo.dwIpAddr; }

void SetVCSIpAddr(unsigned int dwIP)
{ sVCSInfo.dwIpAddr=dwIP; }

void SetVCSPort(unsigned short wPort)
{ sVCSInfo.wPort=wPort; }

void SetViscaSpeed(int iSpeed)
{
  unsigned int i;
  const int MasSpeed[8]={1200,2400,4800,9600,19200,38400,57600,115200};

  for(i=0;i<8;i++)
  {
    if(iSpeed==MasSpeed[i])
    {
      sVCSInfo.dwSpeed=i;
      return;
    }
  }
}

void SetVCSCameraDefault(unsigned short wVCam)
{
  if(wVCam<=d_MAX_VCAM) sVCSInfo.wVCamDef=wVCam;
}

void SetVCSType(unsigned char btType)
{ sVCSInfo.wType=btType; }

void ClearVCSPresets(void)
{ sVCSInfo.wPresetCnt=0; }

void AddVCSPreset(unsigned char btIPPM,unsigned char btVCam,unsigned char btPreset)
{
  if(sVCSInfo.wPresetCnt<d_MAX_PRESET)
  {
    PRESETINFO_TP *pPreset=sVCSInfo.MasPreset+sVCSInfo.wPresetCnt;
    pPreset->btMic=btIPPM;
    pPreset->btVCam=btVCam;
    pPreset->btPreset=btPreset;
    sVCSInfo.wPresetCnt++;
  }
}

/////////////////////////////////////////////////////////////////////
void IP_SendStr(char *pStr)
{
  static sInfoKadr sInfo;

  sInfo.L=strlen(pStr);
  memcpy(sInfo.ByteInfoKadr,pStr,sInfo.L);
  xQueueSend(xQueueReply[UserTCP],&sInfo,0,0);
}

bool IP_Recive(sCommand *psCmd,unsigned int dwTOut)
{
  if(xQueueReceive(xQueueVCS,psCmd,dwTOut)==pdPASS) return true;
  return false;
}

/////////////////////////////////////////////////////////////////////
// For YeaLink
/*
Camera name MUST BE "Camera_x" !!

send: camera near get_id_list
rcv: camera near get_id_list "id:1" "id:2"
send: camera near get_id_detial "id:1"
rcv: camera near get_id_detial "id:1" ... "name:Camera_1" ...
*/
uint8_t DetectCameraIdFromName(uint8_t btCamNumb)
{
  int i,iCnt;
  char *pStr0,*pStr,strCam[30];
  uint8_t btMasId[d_MAX_VCAM];
  static sCommand sCmd;

  IP_SendStr("camera near get_id_list\r\n");
  if(!IP_Recive(&sCmd,200)) return 0;
  memset(btMasId,0,sizeof(btMasId));
  pStr0=(char *)sCmd.BufCommand;
  iCnt=0;
  do
  {
    pStr=strstr(pStr0,"\"id:");
    if(!pStr) break;
    pStr0=pStr+4;
    pStr=strstr(pStr0,"\"");
    *pStr=0;
    btMasId[iCnt]=atoi(pStr0);
    pStr0=pStr+1;
    iCnt++;
    if(iCnt>=d_MAX_VCAM) break;
  }
  while(1);
  LockPrintf();
  sprintf(strCam,"\"name:Camera_%d\"",btCamNumb);
  UnlockPrintf();
  pStr0=(char *)sCmd.BufCommand;
  for(i=0;i<iCnt;i++)
  {
    LockPrintf();
    sprintf(pStr0,"camera near get_id_detial \"id:%d\"\r\n",btMasId[i]);
    UnlockPrintf();
    IP_SendStr(pStr0);
    if(!IP_Recive(&sCmd,200)) return 0;
    if(strstr(pStr0,strCam)) return btMasId[i];
  }
  return 0;
}

void YeaLinkGotoPreset(uint8_t btVCam,uint8_t btPreset)
{ // control VCams by Yealink MeetingEye
  static char StrBuf[80];

  btVCam=DetectCameraIdFromName(btVCam);
  if(btVCam)
  {
    LockPrintf();
    sprintf(StrBuf,"preset near go %d\r\n",btPreset & 0x7f);
    UnlockPrintf();
    IP_SendStr(StrBuf);
    LockPrintf();
    sprintf(StrBuf,"camera near set_active_status \"id:%d\"\r\n",(int)btVCam);
    UnlockPrintf();
    IP_SendStr(StrBuf);
  }
}

/////////////////////////////////////////////////////////////////////
bool PutUART(unsigned int dwSpeed,unsigned char *btOut,int iSz);

void XchgWithUART(sInfoKadr *psInfo,unsigned char *btBuf,unsigned short wSz0)
{ // ByteInfoKadr[0] ����� ����� �������. ������ �� RS-232 ���!
  psInfo->ByteInfoKadr[1]=PutUART(sVCSInfo.dwSpeed,btBuf,wSz0);
  psInfo->L=2;
}

/////////////////////////////////////////////////////////////////////
void VCamGotoPreset(unsigned char btVCam,unsigned char btPreset)
{
  unsigned char btBuf[8];

  switch(sVCSInfo.wType)
  {
    case d_VCS_YeaLink:
      YeaLinkGotoPreset(btVCam,btPreset);
      break;
    case d_VCS_Visca: // Visca command RECALL PRESET - 8x 01 04 3F 02 pp FF - pp: Memory Number preset 0..127
      btBuf[0]=7;
      btBuf[1]=0x80 | (btVCam & 0x07);
      btBuf[2]=0x01;
      btBuf[3]=0x04;
      btBuf[4]=0x3f;
      btBuf[5]=0x02;
      btBuf[6]=btPreset & 0x7f;
      btBuf[7]=0xff;
      PutUART(sVCSInfo.dwSpeed,btBuf,8);
      break;
    default: break;
  }
}

void VCamNewPosition(unsigned char btMic)
{
  unsigned short i;
  if(btMic==0xff)
  { // no active microphones -> set ALWAYS default camera position (preset 0)!
    if(sVCSInfo.wVCamDef>0) VCamGotoPreset(sVCSInfo.wVCamDef,0);
  }
  else
  {
    for(i=0;i<sVCSInfo.wPresetCnt;i++)
    {
      if(btMic==sVCSInfo.MasPreset[i].btMic)
      {
        VCamGotoPreset(sVCSInfo.MasPreset[i].btVCam,sVCSInfo.MasPreset[i].btPreset);
        return;
      }
    }
    if(sVCSInfo.wVCamDef>0) VCamGotoPreset(sVCSInfo.wVCamDef,0);
  }
}

void InitVCSInfo(void)
{ memset(&sVCSInfo,0,sizeof(VCSINFO_TP)); }

void VCSTask(void *pvParameters)
{
  unsigned char btSpeakIPPM;
  static unsigned char s_btSpeakIPPM=0xff;
  static sCommand sCmd;
  static sInfoKadr sInfo;

  WaitWhileReadDSCCfg();
  while(1)
  {
    if(xQueueReceive(xQueueVCS,&sCmd,0))
    {
      switch(sVCSInfo.wType)
      {
        case d_VCS_Granat:
        {
          if(strstr((char *)sCmd.BufCommand,"DSC VCS test string")) IP_SendStr((char *)sCmd.BufCommand);
          else
          {
            // !!!
          }
          break;
        }
        case d_VCS_YeaLink:
        {
          IP_SendStr((char *)sCmd.BufCommand); // Echo recieve string for DSC-VCS test
          break;
        }
        case d_VCS_Visca:
        { // ������ ����� ��� �������� PTZ-������ �� RS-232
          sInfo.Source=sCmd.Source;
          sInfo.A1=sCmd.A1;
          sInfo.PORT1=sCmd.PORT1;
          sInfo.ByteInfoKadr[0]=sCmd.BufCommand[0];
          XchgWithUART(&sInfo,sCmd.BufCommand+1,sCmd.L-1);
          xQueueSend(xQueueReply[sInfo.Source],&sInfo,10,0);
          break;
        }
        default: break;
      }
    }
    else
    {
      btSpeakIPPM=GetSpeakIPPM();
      if(btSpeakIPPM!=s_btSpeakIPPM)
      {
        s_btSpeakIPPM=btSpeakIPPM;
        VCamNewPosition(btSpeakIPPM);
      }
    }
    vTaskDelay(10);
  }
}

