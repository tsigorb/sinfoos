#include "Sinfo.h"
#include "GlobalData.h"

/////////////////////////////////////////////////////////////////////
#define D_SOCK_YEALINK          3

#define d_MaxMicActive          9
#define d_MaxMicVCam            64

typedef struct
{
  uint16_t wMic;
  uint8_t btVCam,btPreset;
} MICVCAM_TP;

/////////////////////////////////////////////////////////////////////
MICVCAM_TP MasMicVCam[d_MaxMicVCam];

volatile uint16_t g_wTask;

uint8_t g_btYealinkSock=0xff;

/////////////////////////////////////////////////////////////////////
// FOR GRANAT VCS
uint8_t FindMic(uint8_t btMic,uint8_t *btMasMic)
{
  int i;

  for(i=0;i<d_MaxMicActive;i++)
  {
    if(btMic==btMasMic[i]) return 1;
  }
  return 0;
}

int32_t SendPckt2IpIfNeed(void)
{
  int32_t i,ret;
  uint8_t btMic;

  if(g_wTask & d_Mic2VCSState)
  {
    g_wTask&=~d_Mic2VCSState;
    for(i=0;i<d_MaxMicActive;i++)
    {
      if((MasMicActive[i]!=0) && (MasMicActive[i]!=0x80) && (MasMicActive[i]!=0xff)) break;
    }
    if(i==d_MaxMicActive)
    {
      ret=send(g_btTCPSock,(uint8_t *)"mic_all_off\r\n",13);
      if(ret<0)
      { g_wTask|=d_NeedReinitTCP; return ret; }
    }
    else
    {
      for(i=0;i<d_MaxMicActive;i++)
      {
        btMic=MasMicActive[i];
        if((btMic==0xff) || (btMic==0x80)) break; // no active microphones
        if(btMic && (!FindMic(btMic,MasMicActiveOld)))
        { // mic_on<SP><NMic><SP>Seat<SP><NSeat><CR><LF>
          sprintf((char *)g_btIpBuf,"mic_on %d Seat %d\r\n",btMic,btMic);
          ret=send(g_btTCPSock,g_btIpBuf,strlen((char *)g_btIpBuf));
          if(ret<0)
          { g_wTask|=d_NeedReinitTCP; return ret; }
        }
      }
      for(i=0;i<d_MaxMicActive;i++)
      {
        btMic=MasMicActiveOld[i];
        if((btMic==0xff) || (btMic==0x80)) break;
        if(btMic && (!FindMic(btMic,MasMicActive)))
        { // mic_off<SP><NMic><CR><LF>
          sprintf((char *)g_btIpBuf,"mic_off %d\r\n",btMic);
          ret=send(g_btTCPSock,g_btIpBuf,strlen((char *)g_btIpBuf));
          if(ret<0)
          { g_wTask|=d_NeedReinitTCP; return ret; }
        }
      }
    }
  }
  return SOCK_OK;
}

// FOR YARLINK VCS
datasize_t reciveYealink(uint16_t wTOut)
{
  datasize_t received_size;
  wTOut>>=4; wTOut++;
  do
  {
    getsockopt(g_btYealinkSock,SO_RECVBUF,&received_size);
    if(received_size<0) return 0;
    if(received_size>0)
    { // read rcv data, but no used it!
      if(received_size>DATA_BUF_SIZE) received_size=DATA_BUF_SIZE;
      recv(g_btYealinkSock,g_btIpBuf,received_size);
      g_btIpBuf[received_size]=0;
      return received_size;
    }
    HAL_Delay(16);
    wTOut--;
  }
  while(wTOut);
  return 0;
}

/* Camera name MUST BE "Camera_x" !!
send: camera near get_id_list
rcv: camera near get_id_list "id:1" "id:2"
send: camera near get_id_detial "id:1"
rcv: camera near get_id_detial "id:1" ... "name:Camera_1" ...
*/
uint8_t DetectCameraIdFromName(uint8_t btCamNumb)
{
  int i,iCnt;
  char *pStr0,*pStr,strCam[30];
  uint8_t btMasId[10];

  sprintf(strCam,"\"name:Camera_%d\"",btCamNumb);
  sprintf((char *)g_btIpBuf,"camera near get_id_list\r\n");
  send(g_btYealinkSock,g_btIpBuf,strlen((char *)g_btIpBuf));
  if(!reciveYealink(200)) return 0;
  memset(btMasId,0,sizeof(btMasId));
  pStr0=(char *)g_btIpBuf;
  do
  {
    pStr=strstr(pStr0,"\"id:");
    if(!pStr) break;
    pStr0=pStr+4;
    pStr=strstr(pStr0,"\"");
    *pStr=0;
    btMasId[iCnt]=atoi(pStr0);
    pStr0=pStr+1;
    iCnt++;
  }
  while(1);
  for(i=0;i<iCnt;i++)
  {
    pStr0=(char *)g_btIpBuf;
    sprintf(pStr0,"camera near get_id_detial \"id:%d\"\r\n",btMasId[i]);
    send(g_btYealinkSock,g_btIpBuf,strlen(pStr0));
    if(!reciveYealink(200)) return 0;
    pStr=strstr(pStr0,strCam);
    if(pStr) return btMasId[i];
  }
  return 0;
}

void YeaLinkGotoPreset(uint8_t btVCam,uint8_t btPreset)
{ // control VCams by Yealink MeetingEye
  sprintf((char *)g_btIpBuf,"preset near go %d\r\n",btPreset & 0x7f);
  send(g_btYealinkSock,g_btIpBuf,strlen((char *)g_btIpBuf));
  btVCam=DetectCameraIdFromName(btVCam);
  if(btVCam)
  {
    sprintf((char *)g_btIpBuf,"camera near set_active_status \"id:%d\"\r\n",(int)btVCam);
    send(g_btYealinkSock,g_btIpBuf,strlen((char *)g_btIpBuf));
  }
}

