void EnableWrite2ProgrammArea(bool bEn);

void SetIpCS(unsigned short wCS);

// ��������� ������������� ����������� SPI ��� �������������� � Flash-�������
void Init_SPI(void);

// ������ ������
unsigned short RdFlash(unsigned int AdrFl);

// ������ ������ 
bool WrFlash(unsigned int AdrFl,unsigned short DanFl);

// ������� �������� ������� Flash ������ AT45DB161
// �������: TRUE - ���� �����, FALSE -���� ������
// ���������: unsigned int Addr - ����� ������, ����������� �� ������� �������� 528 (512) ����
//            unsigned int How - ����� ��������� ������ � ������ ������������� �� ������� 528 (512) ����
bool WrDeviceErase(unsigned int Addr,unsigned int How);

// ������ ������� ������ �� Flash  AT45DB161
// �������: TRUE - ���� �����, FALSE -���� ������
// ���������: unsigned int Addr - ����� ������, ����������� �� ������� 2 ����
//            unsigned short *lpInp - ��������� �� ������ ������� ������
//            unsigned int HowInp - ���������� ������������ ����
bool WrDeviceArray(unsigned int Addr, unsigned short *lpInp,unsigned int HowInp);

// ������ *lpHowOut ���� �� Flash �� ���������� ������ ��  AT45DB161
// �������: ������ TRUE
// ���������: unsigned int AddrInpAddr - ����� ������. ������ ���� �������� �� ������� 2 ������
//            unsigned short *lpOut - ����� ��������� ������(�� ����� 2 ����)
//            unsigned int *lpHowOut ���������� ���� ��� ������
bool RdFlashP(unsigned int InpAddr, unsigned short *lpOut,unsigned int *lpHowOut);

bool WithFlashMemCmp(const unsigned short *lpInp,unsigned int AddrFlashInp,unsigned int Len);

