#include "Sinfo.h"
#include "GlobalData.h"

/////////////////////////////////////////////////////////////////////
void InitIRP(void);
void SetNeedIpReinit(void);
bool IsNeedIpReinit(void);
void IRPTOut(void);
unsigned char FindIpReg(unsigned char btType,
                        unsigned int *pA1,unsigned short *pPORT1,
                        unsigned char *pbtCnt,unsigned char *pbtIndFree);
void IRP_Registration(unsigned char btType,unsigned int A1,unsigned short PORT1,unsigned char *pbtMAC);
void IRP_Unregistration(unsigned char btType,unsigned int A1);
void IRP_Check(unsigned char btType,unsigned int A1);
void SendDiagnostics2DspCom(unsigned char *pData,unsigned char Len);

void LedWorkIndicator(void);

unsigned short GetIpCS(void);

bool isVCS_YeaLink(void);
bool isVCS_Granat(void);
unsigned short VCSGetPort(void);
unsigned int VCSGetIpAddr(void);

void InitCall_UK(void);
void InitCall_IPPM(void);
void InitVCSInfo(void);

/////////////////////////////////////////////////////////////////////
#define dIpTOut             6000ul

/////////////////////////////////////////////////////////////////////
const IPPARAMS_TP c_IpParams=
{
{ 192, 168, 1, 33 }, // IP address
{ 224, 0, 0, 3 }, // Multicast IP address
{ 255, 255, 255, 0 }, // Subnet mask
{ 0, 0, 0, 0 }, // Gateway address
{ 0, 8, 0xDC, 0, 0, 0 }, // MAC address - DO NOT USE THIS ON A PUBLIC NETWORK!
4000, // wPortControl
5002  // wPortSound
};

IPPARAMS_TP g_IpParams[2];

static unsigned char UdpBuf[2][MAX_BUF_SIZE];

volatile static unsigned int s_dwIpTOut[2]={0,0};
volatile static unsigned short s_wIpReinit[2]={0,0};
volatile static unsigned char s_btCntIP[16];

static unsigned int s_dwVCSTOut=0;
static bool s_bVCSTcpReinit=false;

/////////////////////////////////////////////////////////////////////
unsigned int GetMIPAddr(unsigned char btIF)
{
/*  unsigned int *pdwIP;

  if(btIF) pdwIP=(unsigned int *)(g_IpParams[1].btMcstIP);
  else pdwIP=(unsigned int *)(g_IpParams[0].btMcstIP);*/
  unsigned int dwIP;

  if(btIF) memcpy(&dwIP,g_IpParams[1].btMcstIP,4);
  else memcpy(&dwIP,g_IpParams[0].btMcstIP,4);
  return dwIP;
}

bool GetMAC_DSC(unsigned char btIF,unsigned char *pbtMAC)
{
  if(btIF) memcpy(pbtMAC,g_IpParams[1].btSHAR,6);
  else memcpy(pbtMAC,g_IpParams[0].btSHAR,6);
  return true;
}

void WiznetSoftReset(unsigned char btIpInit)
{
  unsigned char txsize[_WIZCHIP_SOCK_NUM_],rxsize[_WIZCHIP_SOCK_NUM_];

  if(btIpInit & 1)
  { // W6100 for IP (������ � �������� <8)
    g_IpParams[0].btSHAR[5]=g_IpParams[0].btSIPR[3];  // !! ��������� ���� ���_������==���������� ����� IP-������ !!
    initWiznet(PF8,g_IpParams[0].btSHAR,g_IpParams[0].btGAR,g_IpParams[0].btMSR,g_IpParams[0].btSIPR);
    // ������ ������� ������� ��� ������������ ������
    memset(txsize,0,sizeof(txsize));
    memset(rxsize,0,sizeof(rxsize));
    txsize[UDPCmdSocket]=rxsize[UDPCmdSocket]=4;
    txsize[UKRTPSocket]=rxsize[UKRTPSocket]=8;
    txsize[VCSTcpSocket]=rxsize[VCSTcpSocket]=4;
    s_bVCSTcpReinit=true;
    s_dwVCSTOut=xTaskGetTickCount();
    wizchip_init(txsize,rxsize);
  }

  if(btIpInit & 2)
  { // W6100 for IPPM (������ � �������� >=8)
    g_IpParams[1].btSHAR[5]=g_IpParams[1].btSIPR[3];  // !! ��������� ���� ���_������==���������� ����� IP-������ !!
    initWiznet(PF3,g_IpParams[1].btSHAR,g_IpParams[1].btGAR,g_IpParams[1].btMSR,g_IpParams[1].btSIPR);
    // ������ ������� ������� ��� ������������ ������
    memset(txsize,0,sizeof(txsize));
    memset(rxsize,0,sizeof(rxsize));
    txsize[IPPMCmdSocket-8]=rxsize[IPPMCmdSocket-8]=4;
    txsize[IPPMRTPSocket-8]=rxsize[IPPMRTPSocket-8]=8;
    wizchip_init(txsize,rxsize);
  }
}

/*********************************************************************************
*               Initialization function to appropriate channel
*
* Description : Initialize designated channel and wait until W3100 has done.
* Arguments   : s - channel number
*               protocol - designate protocol for channel
*               port     - designate source port for appropriate channel
*               flag     - designate option to be used in appropriate.
* Returns     : When succeeded: pdPASS, failed: pdFAIL
*********************************************************************************/
bool xCreateSocket(unsigned char s,unsigned char protocol,unsigned short port,unsigned char flag)
{
  if(socket(s,protocol,port,flag)==SOCK_OK) return true;
  return false;
}

//-------------------------------------------------------------------
static unsigned char s_btUKRTP_Socket=0xff;
static unsigned int s_dwUKRTP_Ip=0,
                    s_dwUKRTP_Ip0=0;
static bool s_bUK_Unicast=false,
            s_bCloseUKRTP=false,
            s_bCreateUKRTPUnicast=false,
            s_bCreateUKRTPMulticast=false;

void _CloseUKRTP(void)
{
  unsigned char btDHAR[6]={0,0,0,0,0,0};
  if(s_btUKRTP_Socket!=0xff)
  {
    SetIpCS(PF8);
    close(s_btUKRTP_Socket);
    // �������� ����������-����� ��� ������
    setSn_DHAR(s_btUKRTP_Socket,btDHAR);
    setSn_DIPR(s_btUKRTP_Socket,btDHAR);
    s_btUKRTP_Socket=0xff;
    s_dwUKRTP_Ip=0;
    s_bUK_Unicast=false;
  }
}

void _CreateUKRTPUnicast(unsigned int dwIP)
{
  if(s_dwUKRTP_Ip) CloseUKRTP();
  s_dwUKRTP_Ip=dwIP;
  s_btUKRTP_Socket=UKRTPSocket;
  s_bUK_Unicast=true;
  xCreateSocket(s_btUKRTP_Socket,Sn_MR_UDP,g_IpParams[0].wPortSound,0);
}

void _CreateUKRTPMulticast(unsigned int dwIP)
{
  unsigned char *pbtIP,
                btDHAR[6]={0x01,0x00,0x5e,0,0,0};
  if(s_dwUKRTP_Ip) CloseUKRTP();
  s_dwUKRTP_Ip=dwIP;
  s_btUKRTP_Socket=UKRTPSocket;
  s_bUK_Unicast=false;

  // ������������� ����������-����� ����� ��������� ������
  SetIpCS(PF8);
  pbtIP=(unsigned char *)&dwIP;
  btDHAR[3]=pbtIP[1] & 0x7f;
  btDHAR[4]=pbtIP[2];
  btDHAR[5]=pbtIP[3];
//SendStr2IpLogger("$DEBUG$CreateUKRTPMulticast: DHAR=%02x%02x%02x%02x%02x%02x",btDHAR[0],btDHAR[1],btDHAR[2],btDHAR[3],btDHAR[4],btDHAR[5]);
  setSn_DHAR(s_btUKRTP_Socket,btDHAR);
  setSn_DIPR(s_btUKRTP_Socket,pbtIP);
  setSn_DPORTR(s_btUKRTP_Socket,g_IpParams[0].wPortSound);
  if(!xCreateSocket(s_btUKRTP_Socket,Sn_MR_UDP,g_IpParams[0].wPortSound,SF_MULTI_ENABLE | SF_BROAD_BLOCK | SF_UNI_BLOCK))
  { SendStr2IpLogger("$DEBUG$CreateUKRTPMulticast: Error!"); }
}

void CloseUKRTP(void)
{
  s_bCloseUKRTP=true;
  while(s_bCloseUKRTP) vTaskDelay(1);
}

void CreateUKRTPUnicast(unsigned int dwIP)
{
  s_dwUKRTP_Ip0=dwIP;
  s_bCreateUKRTPUnicast=true;
  while(s_bCreateUKRTPUnicast) vTaskDelay(1);
}

void CreateUKRTPMulticast(unsigned int dwIP)
{
  s_dwUKRTP_Ip0=dwIP;
  s_bCreateUKRTPMulticast=true;
  while(s_bCreateUKRTPMulticast) vTaskDelay(1);
}

void ProcessUKRTPSocket(void)
{
  if(s_bCloseUKRTP)
  {
    _CloseUKRTP();
    s_bCloseUKRTP=false;
  }
  if(s_bCreateUKRTPUnicast)
  {
    _CreateUKRTPUnicast(s_dwUKRTP_Ip0);
    s_bCreateUKRTPUnicast=false;
  }
  if(s_bCreateUKRTPMulticast)
  {
    _CreateUKRTPMulticast(s_dwUKRTP_Ip0);
    s_bCreateUKRTPMulticast=false;
  }
}

//-------------------------------------------------------------------
static unsigned char s_btVCS_Socket=0xff;

void VCSCloseSocket(void)
{
  if(s_btVCS_Socket!=0xff)
  {
    disconnect(s_btVCS_Socket);
    close(s_btVCS_Socket);
    s_btVCS_Socket=0xff;
    s_dwVCSTOut=xTaskGetTickCount()+1000;
  }
  s_bVCSTcpReinit=true;
}

void VCSReinitSocket(void)
{
  unsigned short wPort=VCSGetPort();

  VCSCloseSocket();
  if((wPort==0) || !(isVCS_YeaLink() || isVCS_Granat())) return;
  if(IsCurrentTickCountLT(s_dwVCSTOut)) return;
  if(socket(VCSTcpSocket,Sn_MR_TCP4,wPort,SF_IO_NONBLOCK)==SOCK_OK)
  {
    if(isVCS_YeaLink())
    {
      unsigned int dwIpAddr=VCSGetIpAddr();
SendStr2IpLogger("$INFO$Init TCP socket for VCS (Addr=0x%08x, port=%d)",VCSGetIpAddr(),VCSGetPort());
      if(dwIpAddr==0) return;
      if(connect(VCSTcpSocket,(unsigned char *)&dwIpAddr,wPort,4)==SOCK_OK)
      {
SendStr2IpLogger("$INFO$DSC connected to VCS YeaLink (Addr=0x%08x, port=%d)",VCSGetIpAddr(),VCSGetPort());
        s_btVCS_Socket=VCSTcpSocket;
        s_bVCSTcpReinit=false;
        return;
      }
    }
    else
    {
      if(isVCS_Granat())
      {
//SendStr2IpLogger("$DEBUG$VCS Granat listen DSC port=%d...",VCSGetPort());
        if(listen(VCSTcpSocket)==SOCK_OK)
        {
//SendStr2IpLogger("$INFO$VCS Granat connected to DSC (port=%d)",VCSGetPort());
          s_btVCS_Socket=VCSTcpSocket;
          s_bVCSTcpReinit=false;
          s_dwVCSTOut=xTaskGetTickCount()+20000;
          return;
        }
//SendStr2IpLogger("$DEBUG$VCS Granat end listen DSC port!");
      }
    }
    close(VCSTcpSocket);
  }
  else SendStr2IpLogger("$DEBUG$Error open TCP socket %d!",VCSTcpSocket);
  s_dwVCSTOut=xTaskGetTickCount()+5000;
}

void VCSRcvSocket(void)
{
  short rcv_size;
  static section ("bigdata") sCommand sCmd;

  getsockopt(s_btVCS_Socket,SO_RECVBUF,&rcv_size);
  if(rcv_size>0)
  { // ����� ������
    if(rcv_size>d_MaxCmdBufSz-1) rcv_size=d_MaxCmdBufSz-1;
    rcv_size=recv(s_btVCS_Socket,sCmd.BufCommand,rcv_size);
    if(rcv_size>0)
    {
      sCmd.BufCommand[rcv_size]=0;
SendStr2IpLogger("$DEBUG$Rcv from VCS: %s",(char *)sCmd.BufCommand);
      sCmd.L=rcv_size;
      xQueueSend(xQueueVCS,&sCmd,0,0);
    }
  }
}

void ProcessVCSTcpSocket(void)
{
  unsigned char btStatus;
  static section ("bigdata") sInfoKadr sInfo;

  if(xQueueReceive(xQueueReply[UserTCP],&sInfo,0)!=pdPASS) sInfo.L=0;
  else
  {
    if(sInfo.L==0) s_bVCSTcpReinit=true; // ����� � ������� ������ - ����������������� ������ VCS � ������ �����������!
  }
  if(s_bVCSTcpReinit) VCSReinitSocket();
  else
  {
    if(s_btVCS_Socket==0xff) return;
    if(getSn_IR(s_btVCS_Socket) & Sn_IR_TIMEOUT) setSn_IRCLR(s_btVCS_Socket,Sn_IR_TIMEOUT);
    getsockopt(s_btVCS_Socket,SO_STATUS,&btStatus);
    switch(btStatus)
    {
      case SOCK_ESTABLISHED:
        ctlsocket(s_btVCS_Socket,CS_GET_INTERRUPT,&btStatus);
        if(btStatus & Sn_IR_CON)
        {
          btStatus=Sn_IR_CON;
          ctlsocket(s_btVCS_Socket,CS_CLR_INTERRUPT,&btStatus);
        }
        if(sInfo.L)
        {
SendStr2IpLogger("$DEBUG$Send to VCS: %s",(char *)sInfo.ByteInfoKadr);
          send(s_btVCS_Socket,sInfo.ByteInfoKadr,sInfo.L); // �������� ������
        }
        VCSRcvSocket();
        return;
      case SOCK_INIT:
        break;
      case SOCK_CLOSE_WAIT:
        if(isVCS_Granat()) VCSRcvSocket();
      case SOCK_CLOSED:
        s_bVCSTcpReinit=true;
        break;
      case SOCK_LISTEN:
        if(IsCurrentTickCountGT(s_dwVCSTOut))
        {
SendStr2IpLogger("$INFO$Tcp socket reinit listen!");
          s_bVCSTcpReinit=true;
        }
        break;
      default:
        break;
    }
  }
}

//-------------------------------------------------------------------
void InitIP(unsigned char btIpInit)
{ // ������������� IP
  short i;

  if(btIpInit & 1) VCSCloseSocket();
  WiznetSoftReset(btIpInit);
  if(btIpInit & 1)
  {
    for(i=0;i<8;i++) s_btCntIP[i]=0;
    xCreateSocket(UDPCmdSocket,Sn_MR_UDP,g_IpParams[0].wPortControl,0);
    s_dwIpTOut[0]=xTaskGetTickCount()+dIpTOut;
    s_wIpReinit[0]++;
  }
  if(btIpInit & 2)
  {
    vPortEnableSwitchTask(false);
    for(i=8;i<16;i++) s_btCntIP[i]=0;
    xCreateSocket(IPPMCmdSocket,Sn_MR_UDP,g_IpParams[1].wPortControl,0);
    xCreateSocket(IPPMRTPSocket,Sn_MR_UDP,g_IpParams[1].wPortSound,0);
    s_dwIpTOut[1]=xTaskGetTickCount()+dIpTOut;
    s_wIpReinit[1]++;
    vPortEnableSwitchTask(true);
  }
  if(btIpInit & 3)
  {
// !!!   SendStr2IpLogger("$INFO$Wiznet btIpInit=%d, counters: [0]=%d, [1]=%d",btIpInit,s_wIpReinit[0],s_wIpReinit[1]);
  }
}

/////////////////////////////////////////////////////////////////////
void ReOpenSocket(unsigned char btSocket,unsigned short wPort,bool bSend)
{
  close(btSocket);
  vTaskDelay(5);
  xCreateSocket(btSocket,Sn_MR_UDP,wPort,0);
  if(bSend) SendStr2IpLogger("$ERROR$Reopen socket=%d for port=%d",btSocket,wPort);
  s_btCntIP[btSocket]=0;
}

void SendIpCmdPacket(unsigned char btSocket)
{
  unsigned char userQueue,ind;
  unsigned short i,SocketSize;
  unsigned int csum;
  static section ("bigdata") sInfoKadr sInfo[2];

  if(btSocket==UDPCmdSocket)
  { ind=0; userQueue=UserUDP; }
  else
  { ind=1; userQueue=UserIPPM; }
  if(!isSocketSendEnd(btSocket))
  {
    if(isSocketTOut(btSocket)) ReOpenSocket(btSocket,g_IpParams[ind].wPortControl,true);
    return;
  }
  if(getsizeIP(btSocket,true)<sizeof(sInfoKadr))
  {
    if(s_btCntIP[btSocket]<10) s_btCntIP[btSocket]++;
    else ReOpenSocket(btSocket,g_IpParams[ind].wPortControl,true);
    return;
  }
  s_btCntIP[btSocket]=0;
  if(xQueueReceive(xQueueReply[userQueue],&sInfo[ind],0)==pdPASS)
  {
    if(sInfo[ind].L && sInfo[ind].A1 && sInfo[ind].PORT1)
    {
      SocketSize=sInfo[ind].L;
      if(SocketSize>MAX_BUF_SIZE-2) SocketSize=MAX_BUF_SIZE-2;
      UdpBuf[ind][0]=0x16;
      for(i=1;i<=SocketSize;i++) UdpBuf[ind][i]=sInfo[ind].ByteInfoKadr[i-1];
      for(i=0,csum=0;i<=SocketSize;i++) csum+=UdpBuf[ind][i];
      UdpBuf[ind][i]=csum & 0xFF;
      SocketSize+=2;
      sendto(btSocket,UdpBuf[ind],SocketSize,(unsigned char *)(&sInfo[ind].A1),sInfo[ind].PORT1);
    }
  }
}

void SendIPPMMcstPacket(void)
{
  unsigned char *pBuf;
/*{//!!!!
  pBuf=GetAudioPcktAddr4Ip(2); // 0-none, 1-UK, 2-IPPM multicast
  if(pBuf)
  {
unsigned int dwIRQ=cli();
*pFIO_FLAG_T=PF11; ssync(); //!!! SF5 (R16) - �������
sti(dwIRQ);
    FreeAudioPckt4Ip(2);
  }
  return;
}*/

  if(!isSocketSendEnd(IPPMRTPSocket))
  {
    if(isSocketTOut(IPPMRTPSocket)) ReOpenSocket(IPPMRTPSocket,g_IpParams[1].wPortSound,true);
    return;
  }
  if(getsizeIP(IPPMRTPSocket,true)<d_MaxSoundIpPcktSz)
  {
    if(s_btCntIP[IPPMRTPSocket]<10) s_btCntIP[IPPMRTPSocket]++;
    else ReOpenSocket(IPPMRTPSocket,g_IpParams[1].wPortSound,true);
    return;
  }
  s_btCntIP[IPPMRTPSocket]=0;
  pBuf=GetAudioPcktAddr4Ip(2); // 0-none, 1-UK, 2-IPPM multicast
//pBuf=GetAudioPcktAddr4Ip(1); // 0-none, 1-UK, 2-IPPM multicast !!! TEST !!!
  if(pBuf)
  {
//xRTPTail *pRtp=(xRTPTail *)(pBuf+(2*d_LenSoundIPPM));
//int overrun;
    sendto(IPPMRTPSocket,pBuf,d_MaxSoundIpPcktSz,g_IpParams[1].btMcstIP,g_IpParams[1].wPortSound);
//overrun=
    FreeAudioPckt4Ip(2);
//SendStr2IpLogger("$AUDIO_OUT$SendIPPMMcstPacket: wTm=%5d, overrun=%d",ReverseShort(pRtp->Number),overrun);
{
extern unsigned int g_dwCntMcst,g_dwTmStart;
  unsigned int dwIRQ=cli();
  *pFIO_FLAG_T=PF11; ssync(); //!!! SF5 (R16) - �������
  sti(dwIRQ);
  g_dwCntMcst++;
  if((g_dwCntMcst % 1000)==0)
  {
    SendStr2IpLogger("$AUDIO_OUT$SendIPPMMcstPacket: average time between packet (1000 packets)=%f",
                     (float)(xTaskGetTickCount()-g_dwTmStart)/(float)g_dwCntMcst);
  }
}
//!!! TEST !!!
//sendto(IPPMRTPSocket,pBuf,d_UKSoundIpPcktSz,g_IpParams[0].btSIPR,g_IpParams[0].wPortSound);
//FreeAudioPckt4Ip(1);
  }
}

void SendUKRTPPacket(void)
{
  unsigned char *pBuf;

  vPortEnableSwitchTask(false);
  if(s_bUK_Unicast && (s_btUKRTP_Socket!=0xff))
  {
    if(!isSocketSendEnd(s_btUKRTP_Socket))
    {
      if(isSocketTOut(s_btUKRTP_Socket)) ReOpenSocket(s_btUKRTP_Socket,g_IpParams[0].wPortSound,true);
      vPortEnableSwitchTask(true);
      return;
    }
    if(getsizeIP(s_btUKRTP_Socket,true)<d_UKSoundIpPcktSz)
    {
      if(s_btCntIP[s_btUKRTP_Socket]<10) s_btCntIP[s_btUKRTP_Socket]++;
      else ReOpenSocket(s_btUKRTP_Socket,g_IpParams[0].wPortSound,true);
      vPortEnableSwitchTask(true);
      return;
    }
    s_btCntIP[s_btUKRTP_Socket]=0;
    pBuf=GetAudioPcktAddr4Ip(1); // 0-none, 1-UK, 2-IPPM multicast
    if(pBuf)
    {
      sendto(s_btUKRTP_Socket,pBuf,d_UKSoundIpPcktSz,(unsigned char *)&s_dwUKRTP_Ip,g_IpParams[0].wPortSound);
      FreeAudioPckt4Ip(1);
//SendStr2IpLogger("$AUDIO_OUT$SendUKRTPPacket: IP=%08x, port=%d",GetAddrUK(),g_IpParams[0].wPortSound));
    }
  }
  vPortEnableSwitchTask(true);
}

void SendIpLoggerPacket(void)
{
  unsigned char btInd;
  unsigned short i,SocketSize,PORT1=0;
  unsigned int A1=0,csum;
  static section ("bigdata") sInfoKadr sInfo;

  if(!isSocketSendEnd(UDPCmdSocket))
  {
    if(isSocketTOut(UDPCmdSocket)) ReOpenSocket(UDPCmdSocket,g_IpParams[0].wPortControl,true);
    return;
  }
  if(getsizeIP(UDPCmdSocket,true)<sizeof(sInfoKadr))
  {
    if(s_btCntIP[UDPCmdSocket]<10) s_btCntIP[UDPCmdSocket]++;
    else ReOpenSocket(UDPCmdSocket,g_IpParams[0].wPortControl,true);
    return;
  }
  s_btCntIP[UDPCmdSocket]=0;
  btInd=FindIpReg(IP_LOGGER,&A1,&PORT1,NULL,NULL);
  if((btInd!=0xff) && A1 && PORT1)
  {
    if(xQueueReceive(xQueueLogger,&sInfo,0)==pdPASS)
    {
      SocketSize=strlen((char *)sInfo.ByteInfoKadr);
      if(SocketSize>MAX_BUF_SIZE-4) SocketSize=MAX_BUF_SIZE-4;
      sInfo.ByteInfoKadr[SocketSize]=0;
      UdpBuf[0][0]=SynchroByte;
      UdpBuf[0][1]=123;  // ��� ������� IP-������
      memcpy(&UdpBuf[0][2],sInfo.ByteInfoKadr,SocketSize+1);
      SocketSize+=4;
      for(i=0,csum=0;i<SocketSize-1;i++) csum+=UdpBuf[0][i];
      UdpBuf[0][i]=csum & 0xFF;
      sendto(UDPCmdSocket,UdpBuf[0],SocketSize,(unsigned char *)&A1,PORT1);
    }
  }
}

void SendArray2PC(sInfoKadr *psInfoS1,unsigned short *DataBuf,unsigned char btCnt)
{
  unsigned char i,j;
  
  psInfoS1->L=(btCnt<<1)+2;
  psInfoS1->ByteInfoKadr[0]=11;
  psInfoS1->ByteInfoKadr[1]=179;
  for(i=0,j=2;i<btCnt;i++,j+=2)
  {
    psInfoS1->ByteInfoKadr[j]=DataBuf[i];
    psInfoS1->ByteInfoKadr[j+1]=DataBuf[i]>>8;
  }
  xQueueSend(xQueueReply[psInfoS1->Source],psInfoS1,10,0);
  return;
}

void ReciveIpCmdPckt(unsigned char btSocket)
{
  unsigned char btCmd,Off;
  unsigned short i,wChan,RxSocketSize;
  unsigned int csum;
  static section ("bigdata") sCommand sCmd[2];

  if(btSocket<8) Off=0;
  else Off=1;
  RxSocketSize=recvfrom(btSocket,UdpBuf[Off],MAX_BUF_SIZE,(unsigned char *)&sCmd[Off].A1,&sCmd[Off].PORT1);
  if(RxSocketSize<3) return; // ����������� ������ ����: ����������,��� �������,CRC
  if(RxSocketSize-2>d_MaxCmdBufSz) return;
  if(UdpBuf[Off][0]!=0x16) return;
  // ������� ����������� �����
  for(i=0,csum=0;i<RxSocketSize-1;i++) csum+=UdpBuf[Off][i];
  // �������� ����������� �����
  if((csum & 0xFF)!=UdpBuf[Off][RxSocketSize-1]) return;
  btCmd=UdpBuf[Off][1];
  sCmd[Off].L=RxSocketSize-2; // ����� ������ �������
  memcpy(sCmd[Off].BufCommand,&UdpBuf[Off][1],sCmd[Off].L);
  if(btSocket==UDPCmdSocket)
  { // ����� �� ������� ����
    sCmd[0].Source=UserUDP;
    switch(btCmd)
    {
      case 10:
        switch(UdpBuf[0][2])
        {
          case 100: // ������ ����� ����������� DspComm - ��� ���� F12 (cmd100) ��� ���� ��������� ��������� (cmd154)
          //case 154:
            IRP_Registration(IP_DSPCOM,sCmd[0].A1,sCmd[0].PORT1,0);
            break;
          case 198: // ����� ����� ����������� DspComm
            IRP_Registration(IP_DSPCOM,sCmd[0].A1,sCmd[0].PORT1,&UdpBuf[0][3]);
            break;
          case 199:
            IRP_Unregistration(IP_DSPCOM,sCmd[0].A1);
            break;
          case 179:
          {
            static section ("bigdata") sInfoKadr sInfoS1;
            sInfoS1.Source=UserUDP;
            sInfoS1.A1=sCmd[0].A1;
            sInfoS1.PORT1=sCmd[0].PORT1;
            SendArray2PC(&sInfoS1,DataSave1,20);
            break;
          }
          default: break;
        }
        break;
      case 12:
         xQueueSend(xQueueTFTP,&sCmd[0],0,0); // ������� ������ � �������
        break;
      case 128:
         xQueueSend(xQueueVCS,&sCmd[0],0,0); // ������� ������ � PTZ-������������ �� RS-232
        break;
      case 252: // ������ �����(252) ��� �������(253) �� ������-������
      case 253:
        break;
      case 254:
        switch(UdpBuf[0][2])
        {
          case 198:
            IRP_Registration(IP_LOGGER,sCmd[0].A1,sCmd[0].PORT1,&UdpBuf[0][3]);
            break;
          case 199:
            IRP_Unregistration(IP_LOGGER,sCmd[0].A1);
            break;
          case 255:
            IRP_Check(IP_LOGGER,sCmd[0].A1);
            break;
          default:
            break;
        }
        break;
      case 255:
        IRP_Check(IP_DSPCOM,sCmd[0].A1);
        break;
      default:
        xQueueSendCmd(&sCmd[0],0);
        break;  
    }
    s_dwIpTOut[0]=xTaskGetTickCount()+dIpTOut;
  }
  else
  { // IPPMCmdSocket
    sCmd[1].Source=UserIPPM;
    xQueueSendCmd(&sCmd[1],0);
    vPortEnableSwitchTask(false);
    s_dwIpTOut[1]=xTaskGetTickCount()+dIpTOut;
    vPortEnableSwitchTask(true);
  }
}

void RecieveUKRTP()
{
  unsigned char InIPadr[4];
  unsigned short InPort,wChan;
  unsigned short RxSocketSize;

  InputIpBufTP *pAdr=GetFreeInputIpBufAddr();
  if(pAdr)
  {
//recvfrom(s_btUKRTP_Socket,UdpBuf[0],d_UKSoundIpPcktSz,InIPadr,&InPort);
/*    RxSocketSize=recvfrom(s_btUKRTP_Socket,(uint8 *)(pAdr->wIpBuf),d_UKSoundIpPcktSz,InIPadr,&InPort);
    if(RxSocketSize==d_UKSoundIpPcktSz)
    {
      xRTPHeadUK *pRTP=(xRTPHeadUK *)(pAdr->wIpBuf);
      if(pRTP->Fmt==0x0890)
      { // ����� �� ��
        pAdr->Chan=1;
        AddInd2MasInputIp4ISR(1,pAdr->Ind);
      }
    }
*/
// !!! TEST 16 kHz, 16 bit, linear
/*RxSocketSize=recvfrom(s_btUKRTP_Socket,(uint8 *)(pAdr->wIpBuf),d_MaxSoundIpPcktSz,InIPadr,&InPort);
if(RxSocketSize==d_MaxSoundIpPcktSz)
{
  xRTPTail *pRTP=(xRTPTail *)(pAdr->wIpBuf+d_LenSoundIPPM);
//SendStr2IpLogger("$AUDIO_IN$RecieveUKRTP: Fmt=%x, Profile=%x, Number=%d",pRTP->Fmt,pRTP->Profile,ReverseShort(pRTP->Number));
  if(pRTP->Fmt==0x0890)
  {
//    wChan=GetInputCP4Numb(1);
wChan=2;
    if(wChan)
    {
      pAdr->Chan=wChan;
      AddInd2MasInputIp4ISR(pAdr->Chan,pAdr->Ind);
    }
  }
}*/
  }
  s_dwIpTOut[0]=xTaskGetTickCount()+dIpTOut;
}

unsigned short NormalizeLvl(unsigned int dwLvl)
{
  unsigned short i,wPwr=0;

  for(i=0;i<32;i++)
  {
    dwLvl>>=1;
    if(dwLvl) wPwr++;
    else return wPwr;
  }
  return wPwr;
}

void RecieveIPPMRTP(void)
{
  unsigned char InIPadr[4];
  unsigned short InPort,wChan;
  unsigned short RxSocketSize;

  InputIpBufTP *pAdr=GetFreeInputIpBufAddr();
  if(pAdr)
  {
    RxSocketSize=recvfrom(IPPMRTPSocket,(uint8 *)(pAdr->wIpBuf),d_MaxSoundIpPcktSz,InIPadr,&InPort);
    if(RxSocketSize==d_MaxSoundIpPcktSz)
    {
      xRTPTail *pRTP=(xRTPTail *)(pAdr->wIpBuf+d_LenSoundIPPM);
/*SendStr2IpLogger("$AUDIO_IN$RecieveIPPMRTP: Fmt=%x, Profile=%x, Number=%d",pRTP->Fmt,pRTP->Profile,ReverseShort(pRTP->Number));*/
      if(pRTP->Fmt==0x0891)
      {
        pAdr->IPPM_Numb=pRTP->Profile;
        wChan=GetInputCP4Numb(pAdr->IPPM_Numb);
        if(wChan)
        {
          SetLvlIPPM(pAdr->IPPM_Numb,NormalizeLvl(pRTP->Lvl));
          pAdr->Chan=wChan;
          AddInd2MasInputIp4ISR(pAdr->Chan,pAdr->Ind);
        }
      }
      vPortEnableSwitchTask(false);
      s_dwIpTOut[1]=xTaskGetTickCount()+dIpTOut;
      vPortEnableSwitchTask(true);
    }
  }
}

void InitIpParams(IPPARAMS_TP *pIpParams)
{
  memcpy(pIpParams,g_IpParams,sizeof(IPPARAMS_TP));
}

void SetIpParams(IPPARAMS_TP *pIpParams)
{
  vPortEnableSwitchTask(false);
  memcpy(g_IpParams,pIpParams,sizeof(IPPARAMS_TP));
  memcpy(g_IpParams+1,g_IpParams,sizeof(IPPARAMS_TP));
g_IpParams[1].btSIPR[3]++; // !!!
  SetNeedIpReinit();
  vPortEnableSwitchTask(true);
}

void GetCfg(void)
{
  memcpy(g_IpParams,&c_IpParams,sizeof(IPPARAMS_TP));
  memcpy(g_IpParams+1,g_IpParams,sizeof(IPPARAMS_TP));
g_IpParams[1].btSIPR[3]++; // !!!
  InitCall_UK();
  InitCall_IPPM();
  InitVCSInfo();
  ReadDSCCfg();
}

void vIPTaskS(void *pvParameters)
{ // ����� ��� ����������� Ethernet (IPPM-������)
  while(1)
  {
//    if(IsCurrentTickCountGT(s_dwIpTOut[1])) InitIP(2);
    if(getsizeIP(IPPMCmdSocket,false)) ReciveIpCmdPckt(IPPMCmdSocket);
    if(getsizeIP(IPPMRTPSocket,false)) RecieveIPPMRTP();
    SendIpCmdPacket(IPPMCmdSocket);
    SendIPPMMcstPacket();
//    SetWDState(d_WDSTATE_IpS);
    vTaskDelay(1);
  }
}

void vIPTaskM(void *pvParameters)
{
  unsigned char btIpInit;

  GetCfg();
  InitIP(3);
  InitIRP();
  s_wIpReinit[0]=s_wIpReinit[1]=0;
  vTaskDelay(500);
  if(xTaskCreate(vIPTaskS,"IpTaskS",
                 STACK_IP,NULL,tskIDLE_PRIORITY+2,&thIpS)!=pdPASS)
  { BlockWatchDog(); }
  while(1)
  {
    LedWorkIndicator();
    btIpInit=0;
    if(!(*pFIO_FLAG_D & PF6) || IsNeedIpReinit())
    {
      btIpInit=3;
      if(!(*pFIO_FLAG_D & PF6))
      { // ��� ������� ������ "����." ������������� Wiznet � ����������� �� ���������!
        memcpy(g_IpParams,&c_IpParams,sizeof(IPPARAMS_TP));
        memcpy(g_IpParams+1,&c_IpParams,sizeof(IPPARAMS_TP));
      }
    }
    else
    {
      if(IsCurrentTickCountGT(s_dwIpTOut[0])) btIpInit|=1;
    }
    if(btIpInit) InitIP(btIpInit);
    IRPTOut();
    if(getsizeIP(UDPCmdSocket,false)) ReciveIpCmdPckt(UDPCmdSocket);
    if(s_btUKRTP_Socket!=0xff)
    {
      if(getsizeIP(s_btUKRTP_Socket,false)) RecieveUKRTP();
    }
    SendIpCmdPacket(UDPCmdSocket);
    SendUKRTPPacket();
    SendIpLoggerPacket();
    ProcessUKRTPSocket();
    ProcessVCSTcpSocket();
//    SetWDState(d_WDSTATE_IpM);
    vTaskDelay(1);
  }
}

/////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdarg.h>
extern int vsprintf(char *buf, const char *fmt, va_list ap);
void SendStr2IpLogger(const char *fmt,...)
{
  char *pStr;
  static section ("bigdata") sInfoKadr sInfoE;
  va_list ap;

  LockPrintf();
  if(xIsQueueFull(xQueueLogger)) xQueueReceive(xQueueLogger,&sInfoE,0);
  pStr=(char *)sInfoE.ByteInfoKadr;
  va_start(ap,fmt);
  vsprintf(pStr,fmt,ap);
  va_end(ap);
  sprintf(pStr+strlen(pStr)," (%s)",ConvertCurrentTM2Str(NULL));
  sInfoE.L=strlen(pStr);
  xQueueSend(xQueueLogger,&sInfoE,0,0);
  UnlockPrintf();
}

