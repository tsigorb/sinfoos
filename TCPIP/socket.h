#ifndef _SOCKET_H_
#define _SOCKET_H_

#include "types.h"
#include "w3150a.h"
#include "socketW6100.h"

/////////////////////////////////////////////////////////////////////
uint8 socketW3150a(SOCKET s, uint8 protocol, uint16 port, uint8 flag);
void closeW3150a(SOCKET s);
uint16 sendtoW3150a(SOCKET s, const uint8 * buf, uint16 len, uint8 * addr, uint16 port); // Send data (UDP/IP RAW)
uint16 recvfromW3150a(SOCKET s, uint8 * buf, uint16 len, uint8 * addr, uint16  *port); // Receive data (UDP/IP RAW)

/**********************************************************
* define function of socket API 
***********************************************************/
void initWiznet(uint8 *pbtSHAR,uint8 *pbtGAR,uint8 *pbtMSR,uint8 *pbtSIPR);
uint8 socketUDP(SOCKET s, uint16 port,uint8 flag); // Opens a socket(TCP or UDP or IP_RAW mode)
void close(SOCKET s); // Close socket
uint16 sendto(SOCKET s, uint8 * buf, uint16 len, uint8 * addr, uint16 port); // Send data (UDP/IP RAW)
uint16 recvfrom(SOCKET s, uint8 * buf, uint16 len, uint8 * addr, uint16  *port); // Receive data (UDP/IP RAW)
uint16 getsizeUDP(SOCKET s,bool bSend);

bool isSocketSendEnd(SOCKET s);
bool isSocketTOut(SOCKET s);

#endif
/* _SOCKET_H_ */

