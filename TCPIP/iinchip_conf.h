#ifndef	_IINCHIP_CONF_H_
#define	_IINCHIP_CONF_H_

#define d_BASEADDR_WIZNET               0x20100000ul

#define LITTLE_ENDIAN //  This must be defined if system is little-endian alignment

/**
* __DEF_IINCHIP_xxx__ : define option for iinchip driver
*/
#define __DEF_IINCHIP_DIRECT_MODE__     1
#define __DEF_IINCHIP_INDIRECT_MODE__   2
#define __DEF_IINCHIP_BUS__             __DEF_IINCHIP_DIRECT_MODE__
//#define __DEF_IINCHIP_BUS__ __DEF_IINCHIP_INDIRECT_MODE__

/**
* __DEF_IINCHIP_MAP_xxx__ : define memory map for iinchip 
*/
#define __DEF_IINCHIP_MAP_BASE__       d_BASEADDR_WIZNET
 
#if (__DEF_IINCHIP_BUS__ == __DEF_IINCHIP_DIRECT_MODE__)
 #define COMMON_BASE __DEF_IINCHIP_MAP_BASE__
#else
 #define COMMON_BASE 0x00000000
#endif

#define __DEF_IINCHIP_MAP_TXBUF__       (COMMON_BASE + 0x4000) /* Internal Tx buffer address of the iinchip */
#define __DEF_IINCHIP_MAP_RXBUF__       (COMMON_BASE + 0x6000) /* Internal Rx buffer address of the iinchip */

#endif

