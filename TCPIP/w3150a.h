#ifndef	_W3150A_H_
#define	_W3150A_H_

#include "iinchip_conf.h"
#include "types.h"

#define TMODE __DEF_IINCHIP_MAP_BASE__
#define IDM_OR ((__DEF_IINCHIP_MAP_BASE__ + 0x00))
#define IDM_AR0 ((__DEF_IINCHIP_MAP_BASE__ + 0x02))
#define IDM_AR1 ((__DEF_IINCHIP_MAP_BASE__ + 0x04))
#define IDM_DR ((__DEF_IINCHIP_MAP_BASE__ + 0x06))

/*
* Maxmium number of socket 
*/
#define MAX_SOCK_NUM            4

/**
 * \brief Gateway IP Register address
 */
#define GATEWAY_PTR				(COMMON_BASE + 0x0001)
/**
 * \brief Subnet mask Register address
 */
#define SUBNET_MASK_PTR			(COMMON_BASE + 0x0005)
/**
 * \brief Source MAC Register address
 */
#define SRC_HA_PTR				(COMMON_BASE + 0x0009)
/**
 * \brief Source IP Register address
 */
#define SRC_IP_PTR				(COMMON_BASE + 0x000F)
/**
 * \brief Interrupt Register
 */
#define INT_REG					(COMMON_BASE + 0x0015)
/**
 * \brief Interrupt mask register
 */
#define INTMASK					(COMMON_BASE + 0x0016)
/**
 * \brief Timeout register address
 *
 * 1 is 100us
 */
#define TIMEOUT_PTR				(COMMON_BASE + 0x0017)
/**
 * \brief Retry count reigster
 */
#define RCR						(COMMON_BASE + 0x0019)
/**
 * \brief Receive memory size reigster
 */
#define RX_DMEM_SIZE			(COMMON_BASE + 0x001A)
/**
 * \brief Transmit memory size reigster
 */
#define TX_DMEM_SIZE			(COMMON_BASE + 0x001B)
/**
 * \brief Authentication type register address in PPPoE mode
 */
#define PPPAUTH					(COMMON_BASE + 0x001C)
#define PPPALGO					(COMMON_BASE + 0x001D)
#define PPP_TIMEOUT				(COMMON_BASE + 0x0028)
#define PPP_MAGIC				(COMMON_BASE + 0x0029)
/**
 * \brief Unreachable IP register address in UDP mode
 */
#define UNREACH_IP				(COMMON_BASE + 0x002A)
/**
 * \brief Unreachable Port register address in UDP mode
 */
#define UNREACH_PORT			(COMMON_BASE + 0x002E)

/* socket register */
#define CH_BASE (COMMON_BASE + 0x0400)
/**
 * size of each channel register map
 */
#define CH_SIZE		0x0100
/**
 * \brief socket option register
 */
#define OPT_PROTOCOL(ch)		(CH_BASE + ch * CH_SIZE + 0x0000)
/**
 * \brief channel command register
 */
#define COMMAND(ch)				(CH_BASE + ch * CH_SIZE + 0x0001)
/**
 * \brief channel interrupt register
 */
#define INT_STATUS(ch)			(CH_BASE + ch * CH_SIZE + 0x0002)
/**
 * \brief channel status register
 */
#define SOCK_STATUS(ch)			(CH_BASE + ch * CH_SIZE + 0x0003)
/**
 * \brief source port register
 */
#define SRC_PORT_PTR(ch)		(CH_BASE + ch * CH_SIZE + 0x0004)
/**
 * \brief Peer MAC register address
 */
#define DST_HA_PTR(ch)			(CH_BASE + ch * CH_SIZE + 0x0006)
/**
 * \brief Peer IP register address
 */
#define DST_IP_PTR(ch)			(CH_BASE + ch * CH_SIZE + 0x000C)
/**
 * \brief Peer port register address
 */
#define DST_PORT_PTR(ch)		(CH_BASE + ch * CH_SIZE + 0x0010)
/**
 * \brief Maximum Segment Size(MSS) register address
 */
#define MSS(ch)					(CH_BASE + ch * CH_SIZE + 0x0012)
/**
 * \brief Protocol of IP Header field register in IP raw mode
 */
#define IP_PROTOCOL(ch)			(CH_BASE + ch * CH_SIZE + 0x0014)

/* \brief IP Type of Service(TOS) Register 
 */
#define IP_TOS(ch)						(CH_BASE + ch * CH_SIZE + 0x0015)
/**
 * \brief IP Time to live(TTL) Register 
 */
#define IP_TTL(ch)						(CH_BASE + ch * CH_SIZE + 0x0016)

/**
 * \brief Transmit free memory size register
 */
#define TX_FREE_SIZE_PTR(ch)	(CH_BASE + ch * CH_SIZE + 0x0020)
/**
 * \brief Transmit memory read pointer register address
 */
#define TX_RD_PTR(ch)			(CH_BASE + ch * CH_SIZE + 0x0022)
/**
 * \brief Transmit memory write pointer register address
 */
#define TX_WR_PTR(ch)			(CH_BASE + ch * CH_SIZE + 0x0024)
/**
 * \brief Received data size register
 */
#define RX_RECV_SIZE_PTR(ch)	(CH_BASE + ch * CH_SIZE + 0x0026)
/**
 * \brief Read point of Receive memory
 */
#define RX_RD_PTR(ch)			(CH_BASE + ch * CH_SIZE + 0x0028)
/**
 * \brief Write point of Receive memory
 */
#define RX_WR_PTR(ch)			(CH_BASE + ch * CH_SIZE + 0x002A)
/** @} */

/* TMODE register values */
#define TMODE_INDIRECT			0x01
#define TMODE_AUTOINC			0x02
#define TMODE_LITTLEENDIAN		0x04
#define TMODE_PPPOE				0x08
#define TMODE_PINGBLOCK			0x10
#define TMODE_MEMTEST			0x20
#define TMODE_STATIC_ISN		0x40
#define TMODE_SWRESET			0x80

/* INT_REG register values */
#define INT_CH(ch)				(0x01 << ch)
#define INT_PPPTERM				0x20
#define INT_UNREACH				0x40
#define INT_IPCONFLICT			0x80

/* OPT_PROTOCOL values */
#define	SOCK_CLOSEDM			0x00		// unused socket
#define	SOCK_STREAM				0x01		// TCP
#define	SOCK_DGRAM				0x02		// UDP
#define	SOCK_ICMPM				0x03		// icmp
#define	SOCK_IPL_RAWM			0x03		// IP LAYER RAW SOCK
#define	SOCK_MACL_RAWM			0x04		// MAC LAYER RAW SOCK
#define	SOCK_PPPOEM				0x05		// PPPoE
#define SOCKOPT_ZEROCHKSUM		0x10
#define SOCKOPT_NDACK			0x20		// No Delayed Ack(TCP) flag
#define SOCKOPT_MULTI			0x80		// support multicating

/* COMMAND values */
#define CSOCKINIT				0x01		// initialize or open socket
#define CLISTEN					0x02		// wait connection request in tcp mode(Server mode)
#define CCONNECT				0x04		// send connection request in tcp mode(Client mode)
#define CDISCONNECT				0x08		// send closing reqeuset in tcp mode
#define CCLOSE					0x10		// close socket
#define CSEND					0x20		// updata txbuf pointer, send data
#define CSENDMAC				0x21		// send data with MAC address
#define CSENDKEEPALIVE			0x22		// send keep alive message
#define CRECV					0x40		// update rxbuf pointer, recv data

#ifdef __DEF_IINCHIP_PPP__
	#define CPPPCON					0x23		// updata txbuf pointer, send data
	#define CPPPDISCON				0x24		// updata txbuf pointer, send data
	#define CPPPCR					0x25		// updata txbuf pointer, send data
	#define CPPPCN					0x26		// updata txbuf pointer, send data
	#define CPPPCJ					0x27		// updata txbuf pointer, send data
#endif

/* INT_STATUS values */
#define ISR_CON					0x01		// established connection
#define ISR_DISCON				0x02		// closed socket
#define ISR_RECV				0x04		// receiving data
#define ISR_TIMEOUT				0x08		// assert timeout
#define ISR_SENDOK  			0x10		// complete sending

#ifdef __DEF_IINCHIP_PPP__
	#define ISR_PPP_NXT				0x20		// receiving data
	#define ISR_PPP_FAIL			0x40		// receiving data
	#define ISR_PPP_RECV			0x80		// receiving data
#endif

/* IP PROTOCOL */
#define IPPROTO_IP              0           /* Dummy for IP */
#define IPPROTO_ICMP            1           /* Control message protocol */
#define IPPROTO_IGMP            2           /* Internet group management protocol */
#define IPPROTO_GGP             3           /* Gateway^2 (deprecated) */
#define IPPROTO_TCP             6           /* TCP */
#define IPPROTO_PUP             12          /* PUP */
#define IPPROTO_UDP             17          /* UDP */
#define IPPROTO_IDP             22          /* XNS idp */
#define IPPROTO_ND              77          /* UNOFFICIAL net disk protocol */
#define IPPROTO_RAW             255         /* Raw IP packet */

/*********************************************************
* iinchip access function
*********************************************************/
uint8 IINCHIP_READ(uint32 addr);
uint8 IINCHIP_WRITE(uint32 addr,uint8 data);
uint16 wiz_read_buf(uint32 addr, uint8* buf,uint16 len);
uint16 wiz_write_buf(uint32 addr,uint8* buf,uint16 len);

void iinchip_init(void); // reset iinchip
void sysinit(uint8 tx_size, uint8 rx_size); // setting tx/rx buf size
uint8 getISR(uint8 s);
uint16 getIINCHIP_RxMAX(uint8 s);
uint16 getIINCHIP_TxMAX(uint8 s);
uint16 getIINCHIP_RxMASK(uint8 s);
uint16 getIINCHIP_TxMASK(uint8 s);
uint32 getIINCHIP_RxBASE(uint8 s);
uint32 getIINCHIP_TxBASE(uint8 s);
void setgateway(uint8 * addr); // set gateway address
void setsubmask(uint8 * addr); // set subnet mask address
uint8 issubnet(uint8 *addr);
uint8 issubnet_gw(void);
void setMACAddr(uint8 * addr); // set local MAC address
void setIP(uint8 * addr); // set local IP address
void settimeout(uint16 timeout); // set retry duration for data transmission, connection, closing ...
void setRCR(uint8 retry); // set retry count (above the value, assert timeout interrupt)
void setINTMask(uint8 mask); // set interrupt mask. 
void getGWIP(uint8 * addr);
void getGWMAC_processing(void);
void getGWMAC(uint8 * addr);
void getLocalAddr(uint8 * addr);
void getDestMAC(SOCKET s, uint8 * addr);
void getDestAddr(SOCKET s, uint8 * addr);
void getDestPort(SOCKET s, uint8 * addr);
void setMSS(SOCKET s, uint16 mss); // set maximum segment size
void setIPprotocol(SOCKET s, uint8 proto); // set IP Protocol value using IP-Raw mode
uint16 selectW3150a(SOCKET s, uint8 func); // Get socket status/Tx free buffer size/ Rx buffer size

#ifdef __DEF_IINCHIP_PPP__
   uint8 pppinit(uint8 *id, uint8 idlen, uint8 *passwd, uint8 passwdlen);
   uint8 pppterm(uint8 *mac,uint8 *sessionid);
#endif

void send_data_processing(SOCKET s, uint8 *data, uint16 len);
void recv_data_processing(SOCKET s, uint8 *data, uint16 len);
void read_data(SOCKET s, uint32 src, uint8 * dst, uint16 len);
void write_data(SOCKET s, uint8 * src, uint32 dst, uint16 len);

/* select func value */	
#define SEL_CONTROL 0 // socket status
#define SEL_SEND 1 // free size in tx buf
#define SEL_RECV 2 // receving data size in rx buf

#endif
