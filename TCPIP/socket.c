#include "socket.h"

/////////////////////////////////////////////////////////////////////
void vTaskDelay(int xTicksToDelay);

/////////////////////////////////////////////////////////////////////
unsigned char g_SockSendBusy[_WIZCHIP_SOCK_NUM_];
unsigned int g_dwSockTOut[_WIZCHIP_SOCK_NUM_];

static uint16 s_W6100=0;

/////////////////////////////////////////////////////////////////////
bool IsW6100(void)
{ return s_W6100; }

void initWiznet(uint8 *pbtSHAR,uint8 *pbtGAR,uint8 *pbtMSR,uint8 *pbtSIPR)
{
  s_W6100=getCIDR();
  if(s_W6100==0x6100)
  {
    // !!! SETTING PHY MODE !!!
    if(!wizphy_getphylink())
    {
      wiz_PhyConf phyconf;

      wizphy_reset();
      vTaskDelay(200);
      PHYUNLOCK();
      setPHYCR1(getPHYCR1() & ~PHYCR1_PWDN);
      wizphy_getphyconf(&phyconf);
      phyconf.mode=PHY_MODE_AUTONEGO;
      wizphy_setphyconf(&phyconf);
      PHYLOCK();
    }
    // !!!

    NETUNLOCK();
    setSHAR(pbtSHAR);
    setGAR(pbtGAR);
    setSUBR(pbtMSR);
    setSIPR(pbtSIPR);
    NETLOCK();
  }
  else
  { // W3150a
    s_W6100=0;
    // Reset the internal WIZnet registers
    iinchip_init();
    // Now we can configure the protocol. Here the MAC address, gateway address, subnet mask and IP address are configured
    setMACAddr(pbtSHAR);
    setgateway(pbtGAR);
    setsubmask(pbtMSR);
    setIP(pbtSIPR);
    // Next the memory buffers are configured to give all the WIZnet internal memory over to a single socket.
    // This gives the socket the maximum internal Tx and Rx buffer space
    sysinit(0x55, 0x55);
  }
}

uint8 socketUDP(SOCKET s, uint16 port,uint8 flag)
{
  if(s_W6100) return socketW6100(s,Sn_MR_UDP,port,SF_IO_NONBLOCK | flag);
  return socketW3150a(s,SOCK_DGRAM,port,0);
}

void close(SOCKET s)
{
  if(s_W6100) closeW6100(s);
  else closeW3150a(s);
}

uint16 sendto(SOCKET s,uint8 *buf, uint16 len, uint8 *addr, uint16 port)
{
  if(s_W6100) sendtoW6100(s,buf,len,addr,port,4);
  return sendtoW3150a(s,buf,len,addr,port);
}

uint16 recvfrom(SOCKET s, uint8 *buf, uint16 len, uint8 *addr, uint16 *port)
{
  uint8 btAddrLen;

  if(s_W6100) return recvfromW6100(s,buf,len,addr,port,&btAddrLen);
  return recvfromW3150a(s,buf,len,addr,port);
}

uint16 getsizeUDP(SOCKET s,bool bSend)
{
  if(bSend)
  {
    if(s_W6100) return getSn_TX_FSR(s);
    return selectW3150a(s,SEL_SEND);
  }
  else
  {
    if(s_W6100) return getSn_RX_RSR(s);
    return selectW3150a(s,SEL_RECV);
  }
}

/////////////////////////////////////////////////////////////////////
bool IsCurrentTickCountGT(uint32 dwTm);

extern uint8 is_gw_samenet;
extern uint8 GW_IP[6]; // gateway mac address

extern unsigned char g_SockSendBusy[];
extern unsigned int g_dwSockTOut[],g_dwTmWait_W3150[5];
extern bool s_bSendtoFirst,s_bW3150Plus;

bool isSocketSendEnd(SOCKET s)
{
  unsigned char btStatus;
  bool bSend;

  if(!g_SockSendBusy[s]) return(true);
  if(s_W6100)
  {
    if(getSn_CR(s)) return(false);
    btStatus=getSn_IR(s);
    if(btStatus & Sn_IR_SENDOK)
    {
      g_SockSendBusy[s]=0;
      setSn_IRCLR(s,Sn_IR_SENDOK | Sn_IR_TIMEOUT);
      return true;
    }  
    return(false);
  }
  else
  {
    if(IINCHIP_READ(COMMAND(s))) return(false);
    btStatus=IINCHIP_READ(INT_STATUS(s));
    if((btStatus & ISR_TIMEOUT)==ISR_TIMEOUT) return(false);
    bSend=btStatus & ISR_SENDOK;
    if(!s_bW3150Plus)
    {
      if(IsCurrentTickCountGT(g_dwTmWait_W3150[s])) bSend|=true;
    }
    if(bSend)
    {
      IINCHIP_WRITE(INT_STATUS(s),ISR_TIMEOUT | ISR_SENDOK);
      g_SockSendBusy[s]=0;
      if(is_gw_samenet==0)
      { // 2005.10.25 added fix subnet check error
        IINCHIP_WRITE((GATEWAY_PTR+0),GW_IP[0]);
        IINCHIP_WRITE((GATEWAY_PTR+1),GW_IP[1]);
        IINCHIP_WRITE((GATEWAY_PTR+2),GW_IP[2]);
        IINCHIP_WRITE((GATEWAY_PTR+3),GW_IP[3]);
      }
      return(true);
    }
    return(false);
  }
}

bool isSocketTOut(SOCKET s)
{
  bool bTOut;

  if(s_W6100)
  {
    bTOut=IsCurrentTickCountGT(g_dwSockTOut[s]) || (getSn_IR(s) & Sn_IR_TIMEOUT);
    if(bTOut)
    {
      g_SockSendBusy[s]=0;
      setSn_IRCLR(s,Sn_IR_TIMEOUT | Sn_IR_SENDOK);
    }
    return(bTOut);
  }
  else
  { // W3150 or W3150+
    bTOut=(IINCHIP_READ(INT_STATUS(s)) & ISR_TIMEOUT) || IsCurrentTickCountGT(g_dwSockTOut[s]);
    if(bTOut || (IINCHIP_READ(SOCK_STATUS(s))==SOCK_CLOSED))
    {
      if(bTOut)
      {
        IINCHIP_WRITE(INT_STATUS(s),ISR_TIMEOUT | ISR_SENDOK);
        g_SockSendBusy[s]=0;
        if(is_gw_samenet==0)
        { // 2005.10.25 added fix subnet check error
          IINCHIP_WRITE((GATEWAY_PTR+0),GW_IP[0]);
          IINCHIP_WRITE((GATEWAY_PTR+1),GW_IP[1]);
          IINCHIP_WRITE((GATEWAY_PTR+2),GW_IP[2]);
          IINCHIP_WRITE((GATEWAY_PTR+3),GW_IP[3]);
        }
        if(s_bSendtoFirst)
        {
          s_bSendtoFirst=false;
          s_bW3150Plus=false;
        }
      }
      return(true);
    }
    return(false);
  }
}

