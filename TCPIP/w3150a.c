/*
 * (c)COPYRIGHT
 * ALL RIGHT RESERVED
 *
 * FileName : w3150a.c
 * Revision History :
 * ----------  -------  -----------  ---------------------------------------------
 * Date        version  Name         Description
 * ----------  -------  -----------  ---------------------------------------------
 * 09/23/2005  1.0      Bong         Create version
 * ----------  -------  -----------  ---------------------------------------------
 * 10/12/2005  2.0      Woo          Release version
 * ----------  -------  -----------  ---------------------------------------------
 * 10/13/2005  2.0.1    Bong         added function getting GW_IP, GW_MAC for UDP defection
 * ----------  -------  -----------  ---------------------------------------------
 * 10/25/2005  2.0.2    Bong         added function fix subnet bug.
 * ----------  -------  -----------  ---------------------------------------------
 * 11/03/2005  2.0.3    Bong         modify issubnet() function.
 * ----------  -------  -----------  ---------------------------------------------
 * 11/22/2005  2.0.4    Bong         #define _20051121_
 *                                   modify pppinit_in() function.
 *                                   support CHAP mode
 *                                   added md5.c, md5.h
 * ----------  -------  -----------  ---------------------------------------------
 * 
 * last update : 11/03/2005
 */

// #define _20051121_ /* chap included */

#include "ccblkfn.h"
#include <string.h>
#include "w3150a.h"
//#include "timer.h" // for wait function

#ifdef _20051121_ /* chap included */
      #include "md5.h"
#endif

/////////////////////////////////////////////////////////////////////
#define d_BASEADDR_WIZNET               0x20100000ul

/////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
uint8 L_IP[4]; // local ip
uint8 SUBNET[4]; // subnet
uint8 GW_IP[4]; // gateway ip address
uint8 GW_MAC[6]; // gateway mac address
// 2005.10.25 added fix subnet check error
uint8 is_gw_samenet;
uint8 I_STATUS[MAX_SOCK_NUM];
uint16 SMASK[MAX_SOCK_NUM]; /* Variable for Tx buffer MASK in each channel */
uint16 RMASK[MAX_SOCK_NUM]; /* Variable for Rx buffer MASK in each channel */
uint16 SSIZE[MAX_SOCK_NUM]; /* Max Tx buffer size by each channel */
uint16 RSIZE[MAX_SOCK_NUM]; /* Max Rx buffer size by each channel */
uint32 SBUFBASEADDRESS[MAX_SOCK_NUM]; /* Tx buffer base address by each channel */
uint32 RBUFBASEADDRESS[MAX_SOCK_NUM]; /* Rx buffer base address by each channel */

uint8 getISR(uint8 s)
{
   return I_STATUS[s];
}
uint16 getIINCHIP_RxMAX(uint8 s)
{
   return RSIZE[s];
}
uint16 getIINCHIP_TxMAX(uint8 s)
{
   return SSIZE[s];
}
uint16 getIINCHIP_RxMASK(uint8 s)
{
   return RMASK[s];
}
uint16 getIINCHIP_TxMASK(uint8 s)
{
   return SMASK[s];
}
uint32 getIINCHIP_RxBASE(uint8 s)
{
   return RBUFBASEADDRESS[s];
}
uint32 getIINCHIP_TxBASE(uint8 s)
{
   return SBUFBASEADDRESS[s];
}

uint8 IINCHIP_WRITE(uint32 addr,uint8 data)
{
#if (__DEF_IINCHIP_BUS__ == __DEF_IINCHIP_DIRECT_MODE__)
//  unsigned j;
  addr= (addr & 0xffff0000) | ((addr & 0x00007FFF)<<1); // преобразование к архитектруре BF533
//  j= cli();
//  ssync();
  *((vuint8*)addr)=data;
  ssync();
//  sti(j);
#else
/*	IINCHIP_ISR_DISABLE();
	*((vuint8*)IDM_AR0) = (uint8)((addr & 0xFF00) >> 8);
	*((vuint8*)IDM_AR1) = (uint8)(addr & 0x00FF);
	*((vuint8*)IDM_DR)  = data;
	IINCHIP_ISR_ENABLE();*/
#endif
  return 1;
}

uint8 IINCHIP_READ(uint32 addr)
{
  uint8 data;
#if (__DEF_IINCHIP_BUS__ == __DEF_IINCHIP_DIRECT_MODE__)
//  unsigned j;
  addr=(addr & 0xffff0000) | ((addr & 0x00007FFF)<<1); // преобразование к архитектруре BF533
//  j= cli();
//  ssync();
  data=*((uint8 *)addr);
  ssync();
//  sti(j);
#else   
/*	IINCHIP_ISR_DISABLE();
	*((vuint8*)IDM_AR0) = (uint8)((addr & 0xFF00) >> 8);
	*((vuint8*)IDM_AR1) = (uint8)(addr & 0x00FF);
	data = *((vuint8*)IDM_DR);
	IINCHIP_ISR_ENABLE();*/
#endif
  return data;
}

uint16 wiz_write_buf(uint32 addr,uint8* buf,uint16 len)
{
#if (__DEF_IINCHIP_BUS__ == __DEF_IINCHIP_DIRECT_MODE__)
    uint16 i;
    uint8 *pBt;
    
    addr=(addr & 0xffff0000) | ((addr & 0x00007FFF)<<1); // преобразование к архитектруре BF533
    pBt=(uint8 *)addr;
    for(i=0;i<len;i++)
    {
      ssync();
      *pBt=*buf++;
      ssync();
      pBt+=2;
    }
#else
/*	uint16 idx = 0;
	IINCHIP_ISR_DISABLE();
	*((vuint8*)IDM_AR0) = (uint8)((addr & 0xFF00) >> 8);
	*((vuint8*)IDM_AR1) = (uint8)(addr & 0x00FF);
	for (idx = 0; idx < len ; idx++) *((vuint8*)IDM_DR) = buf[idx];
	IINCHIP_ISR_ENABLE();*/
#endif
	return len;
}

uint16 wiz_read_buf(uint32 addr, uint8* buf,uint16 len)
{
#if (__DEF_IINCHIP_BUS__ == __DEF_IINCHIP_DIRECT_MODE__)
    uint16 i;
    uint8 *pBt;
    
    addr=(addr & 0xffff0000) | ((addr & 0x00007FFF)<<1); // преобразование к архитектруре BF533
    pBt=(uint8 *)addr;
    for(i=0;i<len;i++)
    {
      ssync();
      *buf++=*pBt;
      ssync();
      pBt+=2;
    }
#else
/*	uint16 idx = 0;
	IINCHIP_ISR_DISABLE();
	*((vuint8*)IDM_AR0) = (uint8)((addr & 0xFF00) >> 8);
	*((vuint8*)IDM_AR1) = (uint8)(addr & 0x00FF);
	for (idx = 0; idx < len ; idx++) buf[idx] = *((vuint8*)IDM_DR);
	IINCHIP_ISR_ENABLE();*/
#endif
	return len;
}

// write data from src to dst
void write_data(SOCKET s, uint8 *src, uint32 dst, uint16 len)
{
	uint16 size;
	uint32 dst_mask,dst_ptr;

	dst_mask = dst & getIINCHIP_TxMASK(s);
	dst_ptr = getIINCHIP_TxBASE(s) + dst_mask;
	if (dst_mask + len > getIINCHIP_TxMAX(s)) 
	{
		size = getIINCHIP_TxMAX(s) - dst_mask;
		wiz_write_buf(dst_ptr, src, size);
		src += size;
		size = len - size;
		dst_ptr = getIINCHIP_TxBASE(s);
		wiz_write_buf(dst_ptr, src, size);
	} 
	else wiz_write_buf(dst_ptr, src, len);
}

// read data from src to dst
void read_data(SOCKET s, uint32 src, uint8 * dst, uint16 len)
{
	uint16 size;
	uint32 src_mask,src_ptr;

	src_mask = src & getIINCHIP_RxMASK(s);
	src_ptr = getIINCHIP_RxBASE(s) + src_mask;
	
	if( (src_mask + len) > getIINCHIP_RxMAX(s) ) 
	{
		size = getIINCHIP_RxMAX(s) - src_mask;
        if(size>len) size=len;
		wiz_read_buf(src_ptr, dst,size);
		dst += size;
		size = len - size;
		if(size)
		{
          src_ptr = getIINCHIP_RxBASE(s);
          wiz_read_buf(src_ptr, dst,size);
		}
	} 
	else wiz_read_buf(src_ptr, dst,len);
}

/*
* Initializes the iinchip
* This function is for resetting of the iinchip.
*/
void iinchip_init(void)
{
  *((volatile uint8*)(TMODE)) = TMODE_SWRESET;

#if (__DEF_IINCHIP_BUS__ == __DEF_IINCHIP_INDIRECT_MODE__)
  *((volatile uint8*)(TMODE)) = TMODE_INDIRECT | TMODE_AUTOINC;
#endif
}

/**
 * Set the flexible memory
 * This function sets the Tx, Rx memory size by each channel
 * \param tx_size Tx memory size (00 - 1KByte, 01- 2KByte, 10 - 4KByte, 11 - 8KByte)
 * \param rx_size Rx memory size (00 - 1KByte, 01- 2KByte, 10 - 4KByte, 11 - 8KByte)
 * \note
 * bit 1-0 : memory size of channel #0 \n
 * bit 3-2 : memory size of channel #1 \n
 * bit 5-4 : memory size of channel #2 \n
 * bit 7-6 : memory size of channel #3 \n
 * Maximum memory size for Tx, Rx in the W3150 is 8K Bytes,
 * In the range of 8KBytes, the memory size could be allocated dynamically by each channel.
 * Be attentive to sum of memory size shouldn't exceed 8Kbytes
 * and to data transmission and receiption from non-allocated channel may cause some problems.
 * If the 8KBytes memory is already  assigned to centain channel, 
 * other 3 channels couldn't be used, for there's no available memory.
 * If two 4KBytes memory are assigned to two each channels, 
 * other 2 channels couldn't be used, for there's no available memory.
 *
 */
void sysinit(uint8 tx_size, uint8 rx_size)
{
	int16 i;
	int16 ssum,rsum;

	ssum = 0;
	rsum = 0;
	
	IINCHIP_WRITE(TX_DMEM_SIZE,tx_size); /* Set Tx memory size for each channel */
	IINCHIP_WRITE(RX_DMEM_SIZE,rx_size);  /* Set Rx memory size for each channel */

	SBUFBASEADDRESS[0] = (uint32)(__DEF_IINCHIP_MAP_TXBUF__);  /* Set base address of Tx memory for channel #0 */
	RBUFBASEADDRESS[0] = (uint32)(__DEF_IINCHIP_MAP_RXBUF__);  /* Set base address of Rx memory for channel #0 */

	for (i = 0 ; i < MAX_SOCK_NUM; i++)
	{ // Set maximum memory size for Tx and Rx, mask, base address of memory by each channel
		SSIZE[i] = (int16)(0);
		RSIZE[i] = (int16)(0);
		if (ssum < 8192)
		{
			switch((tx_size >> i*2) & 0x03)
			{ // Set maximum Tx memory size
			case 0:
				SSIZE[i] = (int16)(1024);
				SMASK[i] = (uint16)(0x03FF);
				break;
			case 1:
				SSIZE[i] = (int16)(2048);
				SMASK[i] = (uint16)(0x07FF);
				break;
			case 2:
				SSIZE[i] = (int16)(4096);
				SMASK[i] = (uint16)(0x0FFF);
				break;
			case 3:
				SSIZE[i] = (int16)(8192);
				SMASK[i] = (uint16)(0x1FFF);
				break;
			}
		}
		if (rsum < 8192)
		{
			switch((rx_size >> i*2) & 0x03)
			{ // Set maximum Rx memory size
			case 0:
				RSIZE[i] = (int16)(1024);
				RMASK[i] = (uint32)(0x000003FF);
				break;
			case 1:
				RSIZE[i] = (int16)(2048);
				RMASK[i] = (uint32)(0x000007FF);
				break;
			case 2:
				RSIZE[i] = (int16)(4096);
				RMASK[i] = (uint32)(0x00000FFF);
				break;
			case 3:
				RSIZE[i] = (int16)(8192);
				RMASK[i] = (uint32)(0x00001FFF);
				break;
			}
		}
		ssum += SSIZE[i];
		rsum += RSIZE[i];

		if(i!=0)
		{ // Sets base address of Tx and Rx memory for channel #1,#2,#3
			SBUFBASEADDRESS[i] = SBUFBASEADDRESS[i-1] + SSIZE[i-1];
			RBUFBASEADDRESS[i] = RBUFBASEADDRESS[i-1] + RSIZE[i-1];
		}
	}
    // for updating gw arp cache
	getGWMAC_processing();
	is_gw_samenet = issubnet_gw();
}

/**
 * \brief Sets up Gateway IP address
 * This function sets up gateway IP address.
 * \param addr A pointer to a 4-byte array that will be filled in with
 * the Gateway IP address.
 */
void setgateway(uint8 * addr)
{
	GW_IP[0] = addr[0];
	GW_IP[1] = addr[1];
	GW_IP[2] = addr[2];
	GW_IP[3] = addr[3];
	IINCHIP_WRITE((GATEWAY_PTR + 0),addr[0]);
	IINCHIP_WRITE((GATEWAY_PTR + 1),addr[1]);
	IINCHIP_WRITE((GATEWAY_PTR + 2),addr[2]);
	IINCHIP_WRITE((GATEWAY_PTR + 3),addr[3]);
}

void getGWIP(uint8 * addr)
{
	addr[0] = GW_IP[0];
	addr[1] = GW_IP[1];
	addr[2] = GW_IP[2];
	addr[3] = GW_IP[3];
}

void getGWMAC(uint8 * addr)
{
	addr[0] = GW_MAC[0];
	addr[1] = GW_MAC[1];
	addr[2] = GW_MAC[2];
	addr[3] = GW_MAC[3];
	addr[4] = GW_MAC[4];
	addr[5] = GW_MAC[5];
}

void getGWMAC_processing(void)
{
   uint8 i,j=0;
   
  do
  {
     IINCHIP_WRITE(OPT_PROTOCOL(3),SOCK_STREAM);
     IINCHIP_WRITE(COMMAND(3),CSOCKINIT);
     IINCHIP_WRITE(DST_IP_PTR(3),L_IP[0]+1);
     IINCHIP_WRITE(COMMAND(3),CCONNECT);
     for (i = 0; i < 6; i++) GW_MAC[i] = IINCHIP_READ(DST_HA_PTR(3)+i);
     IINCHIP_WRITE(COMMAND(3),CCLOSE);
     IINCHIP_WRITE(DST_IP_PTR(3),0x00);
//ResetWatchDog();
   }
   while(IINCHIP_READ(DST_HA_PTR(3)==0xff) && (j++<3));
}

/**
 * \brief Sets up subnet mask
 * This function sets subnet mask.
 * \param addr A pointer to a 4-byte array that will be filled in with
 * the subnet mask.
 */
void setsubmask(uint8 * addr)
{
	SUBNET[0] = addr[0];
	SUBNET[1] = addr[1];
	SUBNET[2] = addr[2];
	SUBNET[3] = addr[3];
	IINCHIP_WRITE((SUBNET_MASK_PTR + 0),addr[0]);
	IINCHIP_WRITE((SUBNET_MASK_PTR + 1),addr[1]);
	IINCHIP_WRITE((SUBNET_MASK_PTR + 2),addr[2]);
	IINCHIP_WRITE((SUBNET_MASK_PTR + 3),addr[3]);
}

/*
* return 
* 0 => different subnet
* 1 => same subnet
*/
uint8 issubnet(uint8 *addr)
{
// modify 2005.11.03 
   if (
      (   ((addr[0] & SUBNET[0]) == (L_IP[0] & SUBNET[0]))
          && ((addr[1] & SUBNET[1]) == (L_IP[1] & SUBNET[1]))
          && ((addr[2] & SUBNET[2]) == (L_IP[2] & SUBNET[2]))
          && ((addr[3] & SUBNET[3]) == (L_IP[3] & SUBNET[3]))
      ) ||
      ( (addr[0] == 0xFF) && (addr[1] == 0xFF) && (addr[2] == 0xFF) && (addr[3] == 0xFF) )
   )
   return 1;
   else return 0;
}

// 2005.10.25 added fix subnet check error
/*
* return 
* 0 => different subnet
* 1 => same subnet
*/
uint8 issubnet_gw(void)
{
   if (   ((L_IP[0] & SUBNET[0]) == (GW_IP[0] & SUBNET[0]))
       && ((L_IP[1] & SUBNET[1]) == (GW_IP[1] & SUBNET[1]))
       && ((L_IP[2] & SUBNET[2]) == (GW_IP[2] & SUBNET[2]))
       && ((L_IP[3] & SUBNET[3]) == (GW_IP[3] & SUBNET[3]))
   ) return 1;
   else return 0;
}

/**
 * \brief Sets up source MAC address
 * This function sets up the source MAC Address.
 * \param addr A pointer to a 6-byte array that will be filled in with
 * the host MAC address.
 */
void setMACAddr(uint8 * addr)
{
	IINCHIP_WRITE((SRC_HA_PTR + 0),addr[0]);
	IINCHIP_WRITE((SRC_HA_PTR + 1),addr[1]);
	IINCHIP_WRITE((SRC_HA_PTR + 2),addr[2]);
	IINCHIP_WRITE((SRC_HA_PTR + 3),addr[3]);
	IINCHIP_WRITE((SRC_HA_PTR + 4),addr[4]);
	IINCHIP_WRITE((SRC_HA_PTR + 5),addr[5]);
}

/**
 * \brief Sets up source IP address
 * This function sets up the source IP Address
 * \param addr A pointer to a 4-byte array that will be filled in with
 * the host IP address.
 */
void setIP(uint8 * addr)
{
	L_IP[0] = addr[0];
	L_IP[1] = addr[1];
	L_IP[2] = addr[2];
	L_IP[3] = addr[3];
	IINCHIP_WRITE((SRC_IP_PTR + 0),addr[0]);
	IINCHIP_WRITE((SRC_IP_PTR + 1),addr[1]);
	IINCHIP_WRITE((SRC_IP_PTR + 2),addr[2]);
	IINCHIP_WRITE((SRC_IP_PTR + 3),addr[3]);
}

/**
 * \brief Set timeout value
 * This function sets up the TCP timeout. \n
 * Timeout Interrupt occurs if the number of retransmission exceed the limit \n
 * when establishing connection and data transmission. 
 * \param timeout timeout values
 */
void setTimeout(uint16 timeout)
{
	IINCHIP_WRITE(TIMEOUT_PTR,(uint8)((timeout & 0xff00) >> 8));
	IINCHIP_WRITE((TIMEOUT_PTR + 1),(uint8)(timeout & 0x00ff));
}

/**
 * \brief Set retry count value
 * This function sets up the TCP retry count.
 * \param retry retry count values
 */
void setRCR(uint8 retry)
{
	IINCHIP_WRITE(RCR,retry);
}

/**
 * \brief Set interrupt mask
 * This function sets up the interrupt mask Enable/Disable appropriate Interrupt.
 * \param mask mask value to setup ('1' : interrupt enable)
 */
void setINTMask(uint8 mask)
{
	IINCHIP_WRITE(INTMASK,mask); // must be setted 0x10.
}

/**
 * \brief Get destination IP address
 * This function returns the destination IP of the socket.
 * \param ch channel
 * \param addr A pointer to store destination IP
 */
void getLocalAddr(uint8 * addr)
{
	addr[0] = IINCHIP_READ(SRC_IP_PTR);
	addr[1] = IINCHIP_READ(SRC_IP_PTR+1);
	addr[2] = IINCHIP_READ(SRC_IP_PTR+2);
	addr[3] = IINCHIP_READ(SRC_IP_PTR+3);
}

void getDestMAC(SOCKET s, uint8 * addr)
{
	addr[0] = IINCHIP_READ(DST_HA_PTR(s));
	addr[1] = IINCHIP_READ(DST_HA_PTR(s)+1);
	addr[2] = IINCHIP_READ(DST_HA_PTR(s)+2);
	addr[3] = IINCHIP_READ(DST_HA_PTR(s)+3);
	addr[4] = IINCHIP_READ(DST_HA_PTR(s)+4);
	addr[5] = IINCHIP_READ(DST_HA_PTR(s)+5);
}

void getDestAddr(SOCKET s, uint8 * addr)
{
	addr[0] = IINCHIP_READ(DST_IP_PTR(s));
	addr[1] = IINCHIP_READ(DST_IP_PTR(s)+1);
	addr[2] = IINCHIP_READ(DST_IP_PTR(s)+2);
	addr[3] = IINCHIP_READ(DST_IP_PTR(s)+3);
}

void getDestPort(SOCKET s, uint8 * addr)
{
	addr[0] = IINCHIP_READ(DST_PORT_PTR(s));
	addr[1] = IINCHIP_READ(DST_PORT_PTR(s)+1);
}

/**
 * \brief Set MSS(Maximum Segment Size)
 * This function sets up the MSS value.
 * \param s channel
 * \param mss MSS value
 */
void setMSS(SOCKET s, uint16 mss)
{
	IINCHIP_WRITE(MSS(s),(uint8)((mss & 0xff00) >> 8));
	IINCHIP_WRITE((MSS(s) + 1),(uint8)(mss & 0x00ff));
}

/**
 * \brief Set IP-Protocol Field
 * This function sets up IP-Protocol field using ip raw mode.
 * \param s channel
 * \param proto value of IP-Protocol field
 */
void setIPprotocol(SOCKET s, uint8 proto)
{
	IINCHIP_WRITE(IP_PROTOCOL(s),proto);
}

/**
 * This function returns socket information.
 * socket status or Tx free buffer size or received data size
 * func 
 * SEL_CONTROL(0x00) -> return socket status 
 * SEL_SEND(0x01)    -> return Tx free buffer size
 * SEL_RECV(0x02)    -> return received data size
 */
uint16 selectW3150a(SOCKET s, uint8 func)
{
	uint16 val=0,val1=0;

	switch (func)
	{
	case SEL_CONTROL :
		val = (uint16)IINCHIP_READ(SOCK_STATUS(s));
		break;
	case SEL_SEND :
		do
		{
			val1 = IINCHIP_READ(TX_FREE_SIZE_PTR(s));
			val1 = (val1 << 8) + IINCHIP_READ(TX_FREE_SIZE_PTR(s) + 1);
    		if(val1 != 0)
			{
				val = IINCHIP_READ(TX_FREE_SIZE_PTR(s));
				val = (val << 8) + IINCHIP_READ(TX_FREE_SIZE_PTR(s) + 1);
			}
			//if (val != val1) printf("diff recv len : [%.4x] [%.4x]", val, val1);
		} while (val != val1);
		break;
	case SEL_RECV :
		do
		{
			val1 = IINCHIP_READ(RX_RECV_SIZE_PTR(s));
			val1 = (val1 << 8) + IINCHIP_READ(RX_RECV_SIZE_PTR(s) + 1);
		    if(val1 != 0)
			{
				val = IINCHIP_READ(RX_RECV_SIZE_PTR(s));
				val = (val << 8) + IINCHIP_READ(RX_RECV_SIZE_PTR(s) + 1);
			}
			//if (val != val1) printf("diff recv len : [%.4x] [%.4x]", val, val1);
		} while (val != val1);
		break;
	default :
		val = 0;
		break;
	}
	return val;
}

/*
* data, pointer processing
*/
void send_data_processing(SOCKET s, uint8 *data, uint16 len)
{
	uint32 ptr;
	ptr = IINCHIP_READ(TX_WR_PTR(s));
	ptr = ((ptr & 0x00ff) << 8) + IINCHIP_READ(TX_WR_PTR(s) + 1);
	write_data(s,data,ptr,len);
	ptr += len;
	IINCHIP_WRITE(TX_WR_PTR(s),(uint8)((ptr & 0xff00) >> 8));
	IINCHIP_WRITE((TX_WR_PTR(s) + 1),(uint8)(ptr & 0x00ff));
}

/*
* data, pointer processing
*/
void recv_data_processing(SOCKET s, uint8 *data, uint16 len)
{
	uint32 ptr;
	ptr = IINCHIP_READ(RX_RD_PTR(s));
	ptr = ((ptr & 0x00ff) << 8) + IINCHIP_READ(RX_RD_PTR(s) + 1);
#ifdef __DEF_IINCHIP_DBG__
	printf("ISR_RX: rd_ptr : %.4x\r\n", ptr);
#endif
	read_data(s,ptr,data,len); // read data
	ptr+=len;
	IINCHIP_WRITE(RX_RD_PTR(s),(uint8)((ptr & 0xff00) >> 8));
	IINCHIP_WRITE((RX_RD_PTR(s) + 1),(uint8)(ptr & 0x00ff));
}

// socket interrupt routine
#ifdef __DEF_IINCHIP_INT__
void iinchip_irq(void)
{
	uint8 int_val;

	IINCHIP_ISR_DISABLE();
	int_val = IINCHIP_READ(INT_REG);

	if (int_val & INT_IPCONFLICT)
	{
//		printf("IP conflict : %.2x\r\n", int_val);
	}
	if (int_val & INT_UNREACH)
	{
//		printf("INT Port Unreachable : %.2x\r\n", int_val);
//		printf("UNREACH_IP : %d.%d.%d.%d\r\n", IINCHIP_READ(UNREACH_IP), IINCHIP_READ(UNREACH_IP+1), IINCHIP_READ(UNREACH_IP+2), IINCHIP_READ(UNREACH_IP+3));
//		printf("UNREACH_PORT : %.2x %.2x\r\n", IINCHIP_READ(UNREACH_PORT), IINCHIP_READ(UNREACH_PORT+1));
	}

	if (int_val & INT_CH(0))
	{
		I_STATUS[0] = IINCHIP_READ(INT_STATUS(0));
	}
	else if (int_val & INT_CH(1))
	{
		I_STATUS[1] = IINCHIP_READ(INT_STATUS(1));
	}
	else if (int_val & INT_CH(2))
	{
		I_STATUS[2] = IINCHIP_READ(INT_STATUS(2));
	}
	else if (int_val & INT_CH(3))
	{
		I_STATUS[3] = IINCHIP_READ(INT_STATUS(3));
	}
	IINCHIP_ISR_ENABLE();
}
#endif


#ifdef __DEF_IINCHIP_PPP__
/*
* make PPPoE connection
* return :
* 1 => success to connect
* 2 => Auth fail
* 3 => timeout
* 4 => Auth type not support
*/
#define PPP_OPTION_BUF_LEN 64
uint8 pppinit_in(uint8 * id, uint8 idlen, uint8 * passwd, uint8 passwdlen);

uint8 pppinit(uint8 * id, uint8 idlen, uint8 * passwd, uint8 passwdlen)
{
	uint8 ret;
	// enable pppoe mode
	IINCHIP_WRITE(TMODE,IINCHIP_READ(TMODE) | TMODE_PPPOE);

	// open socket in pppoe mode
	IINCHIP_READ(INT_STATUS(0)); // first clear isr(0)
	IINCHIP_WRITE(OPT_PROTOCOL(0),SOCK_PPPOEM);
	IINCHIP_WRITE(COMMAND(0),CSOCKINIT);
	IINCHIP_WRITE(PPP_TIMEOUT,200); // 5sec timeout
	IINCHIP_WRITE(PPP_MAGIC,0x01); // magic number
	
	ret = pppinit_in(id, idlen, passwd, passwdlen);

	// close ppp connection socket
	IINCHIP_WRITE(COMMAND(0),CCLOSE);
	return ret;
}

uint8 pppinit_in(uint8 * id, uint8 idlen, uint8 * passwd, uint8 passwdlen)
{
	uint8 loop_idx = 0;
	uint8 isr = 0;
	uint8 buf[PPP_OPTION_BUF_LEN];
	uint16 len;
	uint8 str[PPP_OPTION_BUF_LEN];
	uint8 str_idx,dst_idx;

	printf("1. Get PPPoE server information ");
	// start to connect pppoe connection
	IINCHIP_WRITE(COMMAND(0),CPPPCON);
	wait_10ms(100);

	loop_idx = 0;
	while (!(IINCHIP_READ(INT_STATUS(0)) & ISR_PPP_NXT))
	{
		printf(".");
		if (loop_idx++ == 10) // timeout
		{
			printf("timeout before LCP\r\n"); 
			return 3;
		}
		wait_10ms(100);
	}
	printf("\r\n");

	printf("2. LCP mode : ");
	// send LCP Request
	{
		// Magic number option (kind+len+data)
	   // using PPP_MAGIC value
		buf[0] = 0x05; buf[1] = 0x06; buf[2] = 0x01; buf[3] = 0x01; buf[4] = 0x01; buf[5]= 0x01;
		// for MRU option, 1492 0x05d4  
		// buf[6] = 0x01; buf[7] = 0x04; buf[8] = 0x05; buf[9] = 0xD4;
	}
	send_data_processing(0, buf, 0x06);
	IINCHIP_WRITE(COMMAND(0),CPPPCR);
	wait_10ms(100);

	while (!((isr = IINCHIP_READ(INT_STATUS(0))) & ISR_PPP_NXT))
	{
		if (isr & ISR_PPP_RECV) // Not support option
		{
			len = select(0, SEL_RECV);
      	if ( len > 0 )
      	{
      		recv_data_processing(0, str, len);
      		IINCHIP_WRITE(COMMAND(0),CRECV);
   			// for debug
   			//printf("LCP proc\r\n"); for (i = 0; i < len; i++) printf ("%02x ", str[i]); printf("\r\n");
   			// get option length
   			len = str[4]; len = ((len & 0x00ff) << 8) + str[5];
   			len += 2;
   			str_idx = 6; dst_idx = 0; // ppp header is 6 byte, so starts at 6.
   			do 
   			{
   				if ((str[str_idx] == 0x01) || (str[str_idx] == 0x02) || (str[str_idx] == 0x03) || (str[str_idx] == 0x05))
   				{
   					// skip as length of support option. str_idx+1 is option's length.
   					str_idx += str[str_idx+1];
   				}
   				else
   				{
   					// not support option , REJECT
   					memcpy((uint8 *)(buf+dst_idx), (uint8 *)(str+str_idx), str[str_idx+1]);
   					dst_idx += str[str_idx+1]; str_idx += str[str_idx+1];
   				}
   				// for debug
   				//printf("s: %d, d: %d, l: %d\r\n", str_idx, dst_idx, len);
   			} while (str_idx != len);
   			// for debug
   			// printf("LCP dst proc\r\n"); for (i = 0; i < dst_idx; i++) printf ("%02x ", dst[i]); printf("\r\n");
   
   			// send LCP REJECT packet
   			send_data_processing(0, buf, dst_idx);
   			IINCHIP_WRITE(COMMAND(0),CPPPCJ);
      	}
		}
		printf(".");
		if (loop_idx++ == 10) // timeout
		{
			printf("timeout after LCP\r\n");
			return 3;
		}
		wait_10ms(100);
	}
	printf("ok\r\n");

	printf("3. Enter PPPoE Authentication mode %.2x %.2x: ", IINCHIP_READ(PPPAUTH), IINCHIP_READ(PPPAUTH+1));
	loop_idx = 0;
	if (IINCHIP_READ(PPPAUTH) == 0xc0 && IINCHIP_READ(PPPAUTH+1) == 0x23)
	{
		printf("PAP"); // in case of adsl normally supports PAP.
		// send authentication data
		// copy (idlen + id + passwdlen + passwd)
		buf[loop_idx] = idlen; loop_idx++;
		memcpy((uint8 *)(buf+loop_idx), (uint8 *)(id), idlen); loop_idx += idlen;
		buf[loop_idx] = passwdlen; loop_idx++;
		memcpy((uint8 *)(buf+loop_idx), (uint8 *)(passwd), passwdlen); loop_idx += passwdlen;
		send_data_processing(0, buf, loop_idx);
		IINCHIP_WRITE(COMMAND(0),CPPPCR);
		wait_10ms(100);
	}
	
#ifdef _20051121_ /* chap included */
	else if (IINCHIP_READ(PPPAUTH) == 0xc2 && IINCHIP_READ(PPPAUTH+1) == 0x23)
	{
		uint8 chal_len;
   	md5_ctx context;
   	uint8  digest[16];

		len = select(0, SEL_RECV);
   	if ( len > 0 )
   	{
   		recv_data_processing(0, str, len);
   		IINCHIP_WRITE(COMMAND(0),CRECV);
#ifdef __DEF_IINCHIP_DBG__
		printf("recv CHAP\r\n");
		int16 i;
		for (i = 0; i < 32; i++) printf ("%02x ", str[i]);
		printf("\r\n");
#endif
// str is C2 23 xx CHAL_ID xx xx CHAP_LEN CHAP_DATA
// index  0  1  2  3       4  5  6        7 ...

   		memset(buf,0x00,64);
   		buf[loop_idx] = str[3]; loop_idx++; // chal_id
   		memcpy((uint8 *)(buf+loop_idx), (uint8 *)(passwd), passwdlen); loop_idx += passwdlen; //passwd
   		chal_len = str[6]; // chal_id
   		memcpy((uint8 *)(buf+loop_idx), (uint8 *)(str+7), chal_len); loop_idx += chal_len; //challenge
		   buf[loop_idx] = 0x80;
#ifdef __DEF_IINCHIP_DBG__
		printf("CHAP proc d1\r\n");
		//int16 i;
		for (i = 0; i < 64; i++) printf ("%02x ", buf[i]);
		printf("\r\n");
#endif

   		md5_init(&context);
   		md5_update(&context, buf, loop_idx);
   		md5_final(digest, &context);

#ifdef __DEF_IINCHIP_DBG__
		printf("CHAP proc d1\r\n");
		for (i = 0; i < 16; i++) printf ("%02x", digest[i]);
		printf("\r\n");
#endif
   		loop_idx = 0;
   		buf[loop_idx] = 16; loop_idx++; // hash_len
     		memcpy((uint8 *)(buf+loop_idx), (uint8 *)(digest), 16); loop_idx += 16; // hashed value
     		memcpy((uint8 *)(buf+loop_idx), (uint8 *)(id), idlen); loop_idx += idlen; // id
   		send_data_processing(0, buf, loop_idx);
   		IINCHIP_WRITE(COMMAND(0),CPPPCR);
   		wait_10ms(100);
      }
   }
#endif

	else
	{
		printf("Not support\r\n");
#ifdef __DEF_IINCHIP_DBG__
		printf("Not support PPP Auth type: %.2x%.2x\r\n",IINCHIP_READ(PPPAUTH), IINCHIP_READ(PPPAUTH+1));
#endif
		return 4;
	}
	printf("\r\n");

	printf("4. Wait PPPoE server's admission : ");
	loop_idx = 0;
	while (!((isr = IINCHIP_READ(INT_STATUS(0))) & ISR_PPP_NXT))
	{
		if (isr & ISR_PPP_FAIL)
		{
			printf("failed\r\nReinput id, password..\r\n");
			return 2;
		}
		printf(".");
		if (loop_idx++ == 10) // timeout
		{
			printf("timeout after PAP\r\n");
			return 3;
		}
		wait_10ms(100);
	}
	printf("ok\r\n");

	printf("5. Get IP Address : ");
	// IP Address
	buf[0] = 0x03; buf[1] = 0x06; buf[2] = 0x00; buf[3] = 0x00; buf[4] = 0x00; buf[5] = 0x00;
	send_data_processing(0, buf, 6);
	IINCHIP_WRITE(COMMAND(0),CPPPCR);
	wait_10ms(100);

	loop_idx = 0;
	while (1)
	{
		if (IINCHIP_READ(INT_STATUS(0)) & ISR_PPP_RECV)
		{
			len = select(0, SEL_RECV);
      	if ( len > 0 )
      	{
      		recv_data_processing(0, str, len);
      		IINCHIP_WRITE(COMMAND(0),CRECV);
   			//for debug
   			//printf("IPCP proc\r\n"); for (i = 0; i < len; i++) printf ("%02x ", str[i]); printf("\r\n");
   			str_idx = 6; dst_idx = 0;
   			if (str[2] == 0x03) // in case of NAK
   			{
   				do 
   				{
   					if (str[str_idx] == 0x03) // request only ip information
   					{
   						memcpy((uint8 *)(buf+dst_idx), (uint8 *)(str+str_idx), str[str_idx+1]);
   						dst_idx += str[str_idx+1]; str_idx += str[str_idx+1];
   					}
   					else
   					{
   						// skip byte
   						str_idx += str[str_idx+1];
   					}
   					// for debug
   					//printf("s: %d, d: %d, l: %d", str_idx, dst_idx, len);
   				} while (str_idx != len);
   				send_data_processing(0, buf, dst_idx);
   				IINCHIP_WRITE(COMMAND(0),CPPPCR); // send ipcp request
   				wait_10ms(100);
   				break;
   			}
      	}
		}
		printf(".");
		if (loop_idx++ == 10) // timeout
		{
			printf("timeout after IPCP\r\n");
			return 3;
		}
		wait_10ms(100);
		send_data_processing(0, buf, 6);
		IINCHIP_WRITE(COMMAND(0),CPPPCR); //ipcp re-request
	}

	loop_idx = 0;
	while (!(IINCHIP_READ(INT_STATUS(0)) & ISR_PPP_NXT))
	{
		printf(".");
		if (loop_idx++ == 10) // timeout
		{
			printf("timeout after IPCP NAK\r\n");
			return 3;
		}
		wait_10ms(100);
		IINCHIP_WRITE(COMMAND(0),CPPPCR); // send ipcp request
	}
	printf("ok\r\n");

	return 1;
	// after this function, User must save the pppoe server's mac address and pppoe session id in current connection
}

/*
* terminate PPPoE connection
*/
uint8 pppterm(uint8 * mac, uint8 * sessionid)
{
	uint16 i;
	uint8 isr;
#ifdef __DEF_IINCHIP_DBG__
	printf("pppterm()\r\n");
#endif
	// enable pppoe mode
	IINCHIP_WRITE(TMODE,IINCHIP_READ(TMODE) | TMODE_PPPOE);
	
	// write pppoe server's mac address and session id 
	// must be setted these value.
	for (i = 0; i < 6; i++) IINCHIP_WRITE((DST_HA_PTR(0)+i),mac[i]);
	for (i = 0; i < 2; i++) IINCHIP_WRITE((DST_PORT_PTR(0)+i),sessionid[i]);
	isr = IINCHIP_READ(INT_STATUS(0));
	
	//open socket in pppoe mode
	IINCHIP_WRITE(OPT_PROTOCOL(0),SOCK_PPPOEM);
	IINCHIP_WRITE(COMMAND(0),CSOCKINIT);
	wait_1us(1);
	// close pppoe connection
	IINCHIP_WRITE(COMMAND(0),CPPPDISCON);
	wait_10ms(100);
	// close socket
	IINCHIP_WRITE(COMMAND(0),CCLOSE);

#ifdef __DEF_IINCHIP_DBG__
	printf("pppterm() end ..\r\n");
#endif

	return 1;
}
#endif
