#ifndef _TYPE_H_
#define _TYPE_H_

#ifndef NULL
#define NULL    ((void *) 0)
#endif

#ifndef _SIZE_T
#define _SIZE_T
// typedef unsigned int size_t;
#endif

/**
 * The 8-bit signed data type.
 */
typedef char int8;
typedef int8 int8_t;

/**
 * The volatile 8-bit signed data type.
 */
typedef volatile char vint8;

/**
 * The 8-bit unsigned data type.
 */
typedef unsigned char uint8;
typedef uint8 uint8_t;

/**
 * The volatile 8-bit unsigned data type.
 */
typedef volatile unsigned char vuint8;

/**
 * The 16-bit signed data type.
 */
typedef short int16;
typedef int16 int16_t;

/**
 * The volatile 16-bit signed data type.
 */
typedef volatile short vint16;

/**
 * The 16-bit unsigned data type.
 */
typedef unsigned short uint16;
typedef uint16 uint16_t;

/**
 * The volatile 16-bit unsigned data type.
 */
typedef volatile unsigned short vuint16;

/**
 * The 32-bit signed data type.
 */
typedef long int32;
typedef int32 int32_t;

/**
 * The volatile 32-bit signed data type.
 */
typedef volatile long vint32;

/**
 * The 32-bit unsigned data type.
 */
typedef unsigned long uint32;
typedef uint32 uint32_t;

/**
 * The volatile 32-bit unsigned data type.
 */
typedef volatile unsigned long vuint32;

/* bsd */
typedef uint8			u_char;		/* 8-bit value */
typedef uint8 			SOCKET;
typedef uint16			u_short;	/* 16-bit value */
typedef uint32			u_int;		/* 32-bit value */
typedef uint32			u_long;		/* 32-bit value */

typedef union _un_l2cval {
	u_long	lVal;
	u_char	cVal[4];
}un_l2cval;

typedef union _un_i2cval {
	u_int	iVal;
	u_char	cVal[2];
}un_i2cval;


/* Sn_SR values */
/**
 * @brief SOCKETn Closed status
 * @details @ref SOCK_CLOSED indicates that SOCKETn is closed and released.\n
 *          It is set when @ref Sn_CR_DISCON , @ref Sn_CR_CLOSE is performed, or when @ref Sn_IR_TIMEOUT is set.\n
 *          It can be changed to @ref SOCK_CLOSED regardless of previous status.
 * @sa _Sn_SR_, _Sn_CR_, _Sn_IR_, _Sn_IRCLR_, Sn_IR_TIMEOUT
 * @sa getSn_SR(), getSn_CR(), setSn_CR(), getSn_IR(), setSn_IRCLR()
 */
#define SOCK_CLOSED          (0x00)

/**
 * @brief TCP SOCKETn initialized status
 * @details @ref SOCK_INIT indicates SOCKETn is opened with TCP mode such as @ref Sn_MR_TCP4, @ref Sn_MR_TCP6, and @ref Sn_MR_TCP6.\n
 *          @ref _Sn_SR_ is changed from @ref SOCK_CLOSED to @ref SOCK_INIT when @ref Sn_CR_OPEN is performed in TCP mode.\n
 *          In @ref SOCK_INIT status, @ref Sn_CR_LISTEN for operating a <b>TCP SERVER</b> \n
 *          or @ref Sn_CR_CONNECT / @ref Sn_CR_CONNECT6 for operating a <b>TCP CLIENT</b> can be performed.
 * @note It is valid only in TCP mode.
 * @sa _Sn_SR_, _Sn_CR_, _Sn_MR_
 * @sa getSn_SR, getSn_CR(), setSn_CR(), getSn_MR(), setSn_MR()
 */
#define SOCK_INIT            (0x13)

/**
 * @brief TCP SOCKETn Listen status
 * @details @ref SOCK_LISTEN indicates SOCKETn is operating as <b>TCP SERVER</b> mode \n
 *          and waiting for connection-request (SYN packet) from a peer (<b>TCP CLIENT</b>).\n
 *          @ref _Sn_SR_ is changed to @ref SOCK_SYNRECV when the connection-request(SYN packet) is successfully accepted \n
 *          and It is changed from @ref SOCK_SYNRECV to @ref SOCK_ESTABLISHED \n
 *          when the SYN/ACK packet is sent successfully to the peer and then the ACK packet of SYN/ACK is received successfully.\n
 *          Otherwise, it is changed to @ref SOCK_CLOSED and @ref Sn_IR_TIMEOUT is set.
 * @note It is valid only in TCP mode such as @ref Sn_MR_TCP4, @ref Sn_MR_TCP6, and @ref Sn_MR_TCP6..
 * @sa _Sn_SR_, _Sn_CR_, _Sn_IR_, _Sn_IRCLR_, _Sn_MR_ 
 * @sa getSn_SR, getSn_CR(), getSn_IR(), setSn_IRCLR(), setSn_CR(), getSn_MR(), setSn_MR()
 */
#define SOCK_LISTEN          (0x14)

/**
 * @brief TCP Connection Request status
 * @details @ref SOCK_SYNSENT indicates TCP SOCKETn sent the connect-request packet(SYN packet)\n
 *          to the peer specified by @ref _Sn_DIPR_ / @ref _Sn_DIP6R_ and @ref _Sn_DPORTR_.\n
 *          It is temporarily shown when @ref _Sn_SR_ is changing from @ref SOCK_INIT to @ref SOCK_ESTABLISHED by @ref Sn_CR_CONNECT or @ref Sn_CR_CONNECT6.\n
 *          When the connect-accept packet (SYN/ACK packet) is received from the peer at @ref SOCK_SYNSENT and the ACK packet of SYN/ACK is sent successfully, \n
 *          @ref _Sn_SR_ is changed to @ref SOCK_ESTABLISHED.\n
 *          Otherwise, it is changed to @ref SOCK_CLOSED and @ref Sn_IR_TIMEOUT is set.
 * @note It is valid only in TCP mode such as @ref Sn_MR_TCP4, @ref Sn_MR_TCP6, and @ref Sn_MR_TCP6..
 * @sa _Sn_SR_, _Sn_CR_, _Sn_IR_, _Sn_IRCLR_, _Sn_MR_  
 * @sa getSn_SR, getSn_CR(), getSn_IR(), setSn_IRCLR(), setSn_CR(), getSn_MR(), setSn_MR()
 */
#define SOCK_SYNSENT         (0x15)

/**
 * @brief TCP Connection Accept status
 * @details @ref SOCK_SYNRECV indicates TCP SOCKETn is successfully received the connect-request packet (SYN packet) from a peer.\n
 *          It is temporarily shown when @ref _Sn_SR_ is changing from @ref SOCK_LISTEN to @ref SOCK_ESTABLISHED by the SYN packet\n
 *          If SOCKETn sends the response (SYN/ACK  packet) to the peer successfully and the ACK packet of SYS/ACK is received successfully,\n
 *          @ref _Sn_SR_ is changed to @ref SOCK_ESTABLISHED. \n
 *          Otherwise, @ref _Sn_SR_ is changed to @ref SOCK_CLOSED and @ref Sn_IR_TIMEOUT is set.
 * @note It is valid only in TCP mode such as @ref Sn_MR_TCP4, @ref Sn_MR_TCP6, and @ref Sn_MR_TCP6..
 * @sa _Sn_SR_, _Sn_CR_, _Sn_IR_, _Sn_IRCLR_, _Sn_MR_
 * @sa getSn_SR, getSn_CR(), getSn_IR(), setSn_IRCLR(), setSn_CR(), getSn_MR(), setSn_MR()
 */
#define SOCK_SYNRECV         (0x16)

/**
 * @brief TCP SOCKETn Established status
 * @details @ref SOCK_ESTABLISHED indicates TCP SOCKETn is connected successfully with a peer.\n
 *          when the <b>TCP SERVER</b> processes the SYN packet from the <b>TCP CLIENT</b> during @ref SOCK_LISTEN or \n
 *          when the <b>TCP CLIENT</b> performs successfully @ref Sn_CR_CONNECT or @ref Sn_CR_CONNECT6,\n
 *          @ref _Sn_SR_ is changed to @ref SOCK_ESTABLISHED and @ref Sn_IR_CON is set. \n
 *          During @ref SOCK_ESTABLISHED, a DATA packet can be sent to or received from the peer by @ref Sn_CR_SEND or @ref Sn_CR_RECV.  \n
 *          If the DATA/ACK packet is not received from the peer during data re-transmission, @ref Sn_IR_TIMEOUT is set and @ref _Sn_SR_ is changed to @ref SOCK_CLOSED.\n
 *          Otherwise, @ref _Sn_SR_ is still at @ref SOCK_ESTABLISHED.
 * @note In <b>TCP SERVER</b>, \n
 *       You can check the IPv4/IPv6 address and port number of connected peer thru @ref _Sn_DIPR_, @ref _Sn_DIP6R_, and @ref _Sn_DPORTR_ respectively.
 * @note It is valid only in TCP mode such as @ref Sn_MR_TCP4, @ref Sn_MR_TCP6, and @ref Sn_MR_TCP6.
 * @sa _Sn_SR_, _Sn_CR_, _Sn_IR_, _Sn_IRCLR_, _Sn_MR_, _Sn_DIPR_, _Sn_DIP6R_, _Sn_DPORTR_
 * @sa getSn_SR, getSn_CR(), getSn_IR(), setSn_IRCLR(), setSn_CR(), getSn_MR(), setSn_MR(),
 *     getSn_DIPR(), setSn_DIPR(), getSn_DIP6R(), setSn_DIP6R(), getSn_DPORTR(), setSn_DPORTR().
 */
#define SOCK_ESTABLISHED     (0x17)

/**
 * @brief TCP SOCKETn Closing status
 * @details @ref SOCK_FIN_WAIT indicates TCP mode SOCKETn waits until the disconnect-process is completed. \n
 *          It is temporarily shown in disconnect-process such as active-close. \n
 *          When the disconnect-process is successfully completed or when @ref Sn_IR_TIMEOUT is set,\n
 *          @ref _Sn_SR_ is changed to @ref SOCK_CLOSED.
 * @note It is valid only in TCP mode such as @ref Sn_MR_TCP4, @ref Sn_MR_TCP6, and @ref Sn_MR_TCPD.
 * @sa _Sn_SR_, _Sn_CR_, _Sn_IR_, _Sn_IRCLR_, _Sn_MR_, SOCK_TIME_WAIT, SOCK_LAST_ACK
 * @sa getSn_SR, getSn_CR(), getSn_IR(), setSn_IRCLR(), setSn_CR(), getSn_MR(), setSn_MR()
 *
 */
#define SOCK_FIN_WAIT        (0x18)

#define SOCK_FIN_WAIT2       0x19   // closing state
#define SOCK_CLOSING         0x1A   // closing state

/**
 * @brief TCP SOCKETn Closing status
 * @details @ref SOCK_TIME_WAIT indicates TCP SOCKETn waits until the disconnect-process is completed.\n
 *          It is temporarily shown in disconnect-process such as active-close. \n
 *          When the disconnect-process is successfully completed or when @ref Sn_IR_TIMEOUT is set,\n
 *          @ref _Sn_SR_ is changed to @ref SOCK_CLOSED.
 * @note It is valid only in TCP mode such as @ref Sn_MR_TCP4, @ref Sn_MR_TCP6, and @ref Sn_MR_TCPD.
 * @sa _Sn_SR_, _Sn_CR_, _Sn_IR_, _Sn_IRCLR_, _Sn_MR_, SOCK_FIN_WAIT, SOCK_LAST_ACK 
 * @sa getSn_SR, getSn_CR(), getSn_IR(), setSn_IRCLR(), setSn_CR(), getSn_MR(), setSn_MR()
 */
#define SOCK_TIME_WAIT       (0x1B)

/**
 * @brief TCP SOCKETn Half Closing staus
 * @details @ref SOCK_CLOSE_WAIT indicates TCP SOCKETn receives the disconnect-request (FIN packet) from the connected peer.\n
 *          It is a half-closing status, and a DATA packet can be still sent or received by @ref Sn_CR_SEND or @ref Sn_CR_RECV.\n
 *          If you do not have any more need to send or received a DATA packet, You can perform @ref Sn_CR_DISCON for a full-closing.
 * @note If you have no need the successful closing, You maybe perform @ref Sn_CR_CLOSE.
 * @note It is valid only in TCP mode such as @ref Sn_MR_TCP4, @ref Sn_MR_TCP6, and @ref Sn_MR_TCPD.
 * @sa _Sn_SR_, _Sn_CR_, _Sn_IR_, _Sn_IRCLR_, _Sn_MR_ 
 * @sa getSn_SR, getSn_CR(), getSn_IR(), setSn_IRCLR(), setSn_CR(), getSn_MR(), setSn_MR()
 */
#define SOCK_CLOSE_WAIT      (0x1C)

/**
 * @brief TCP SOCKETn Closing status
 * @details @ref SOCK_LAST_ACK indicates TCP SOCKETn waits until the disconnect-process is completed.\n
 *          It is temporarily shown in disconnect-process such as active-close and passive-close.\n
 *          When the disconnect-process is successfully completed or when @ref Sn_IR_TIMEOUT is set,\n
 *          @ref _Sn_SR_ is changed to @ref SOCK_CLOSED.
 * @note It is valid only in TCP mode such as @ref Sn_MR_TCP4, @ref Sn_MR_TCP6, and @ref Sn_MR_TCPD.
 * @sa _Sn_SR_, _Sn_CR_, _Sn_IR_, _Sn_IRCLR_, _Sn_MR_, SOCK_FIN_WAIT, SOCK_TIME_WAIT 
 * @sa getSn_SR, getSn_CR(), getSn_IR(), setSn_IRCLR(), setSn_CR(), getSn_MR(), setSn_MR()
 */
#define SOCK_LAST_ACK        (0x1D)

/**
 * @brief UDP SOCKETn status
 * @details @ref SOCK_UDP indicates SOCKETn is opened in UDP mode such as @ref Sn_MR_UDP4, @ref Sn_MR_UDP6, and @ref Sn_MR_UDPD.\n
 *          @ref _Sn_SR_ is changed from @ref SOCK_CLOSED to @ref SOCK_INIT when @ref Sn_CR_OPEN is performed in UDP mode.\n
 *          Unlike TCP mode, during @ref SOCK_UDP, \n
 *          a DATA packet can be sent to or received from a peer by @ref Sn_CR_SEND / @ref Sn_CR_SEND6 or @ref Sn_CR_RECV without a connect-process.\n
 *          Before a DATA packet is sent by @ref Sn_CR_SEND / @ref Sn_CR_SEND6,\n
 *          the ARP is requested to the peer specified by @ref _Sn_DIPR_ / @ref _Sn_DIP6R_.\n
 *          In ARP processing, @ref _Sn_SR_ is stll at @ref SOCK_UDP even if @ref Sn_IR_TIMEOUT is set.\n
 *          If you do not have any more need to send or received a DATA packet, \n
 *          You can perform @ref Sn_CR_CLOSE and @ref _Sn_SR_ is changed to @ref SOCK_CLOSED.
 * @note It is valid only in UDP mode such as @ref Sn_MR_UDP4, @ref Sn_MR_UDP6, and @ref Sn_MR_UDPD.
 * @sa _Sn_SR_, _Sn_CR_, _Sn_IR_, _Sn_IRCLR_, _Sn_MR_, _Sn_DIPR_, _Sn_DIP6R_
 * @sa getSn_SR, getSn_CR(), getSn_IR(), setSn_IRCLR(), setSn_CR(), getSn_MR(), setSn_MR(),
 *     getSn_DIPR(), setSn_DIPR(), getSn_DIP6R(), setSn_DIP6R()
 */
#define SOCK_UDP             (0x22)

/**
 * @brief IPRAW4 SOCKETn mode
 * @details @ref SOCK_IPRAW4(= @ref SOCK_IPRAW) SOCKETn indicates SOCKETn is opened as IPv4 RAW mode.\n
 *          @ref _Sn_SR_ is changed from @ref SOCK_CLOSED to @ref SOCK_IPRAW4 when @ref Sn_CR_OPEN is performed with @ref Sn_MR_IPRAW4. \n
 *          A DATA packet can be send to or received from a peer without a connection like as @ref SOCK_UDP. \n
 *          Before a DATA packet is sent by @ref Sn_CR_SEND, \n
 *          the ARP is requested to the peer specified by @ref _Sn_DIPR_.\n
 *          In ARP processing, @ref _Sn_SR_ is still at @ref SOCK_IPRAW4 even if @ref Sn_IR_TIMEOUT is set.\n
 *          IPRAW4 SOCKETn can receive only the packet specified by @ref _Sn_PNR_, and it discards the others packets.\n
 *          If you do not have any more need to send or received a DATA packet, \n
 *          You can perform @ref Sn_CR_CLOSE and @ref _Sn_SR_ is changed to @ref SOCK_CLOSED.
 * @note It is valid only in IPRAW4 mode such as @ref Sn_MR_IPRAW4.
 * @sa _Sn_SR_, _Sn_CR_, _Sn_IR_, _Sn_IRCLR_, _Sn_MR_, _Sn_DIPR_, _Sn_PNR_
 * @sa getSn_SR, getSn_CR(), getSn_IR(), setSn_IRCLR(), setSn_CR(), getSn_MR(), setSn_MR(),
 *     getSn_DIPR(), setSn_DIPR(), getSn_PNR(), setSn_PNR() 
 */
#define SOCK_IPRAW4          (0x32)
#define SOCK_IPRAW           (SOCK_IPRAW4)   ///< Refer to @ref SOCK_IPRAW4.

/**
 * @brief IPRAW6 SOCKETn mode
 * @details @ref SOCK_IPRAW6 SOCKETn indicates SOCKETn is opened as IPv6 RAW mode.\n
 *          @ref _Sn_SR_ is changed from @ref SOCK_CLOSED to @ref SOCK_IPRAW6 when @ref Sn_CR_OPEN is performed with @ref Sn_MR_IPRAW6. \n
 *          A DATA packet can be send to or received from a peer without a connection like as @ref SOCK_UDP.\n
 *          Before a DATA packet is sent by @ref Sn_CR_SEND6, \n
 *          the ICMPv6 NS is requested to the peer specified by @ref _Sn_DIPR_ or @ref _Sn_DIP6R_.\n
 *          In ND(Neighbor Discovery) is processing,\n
 *          @ref _Sn_SR_ is still at @ref SOCK_IPRAW6 even if @ref Sn_IR_TIMEOUT is set.\n
 *          IPRAW6 SOCKETn can receive only the packet specified by @ref _Sn_PNR_, and it discards the others packets.\n
 *          If you do not have any more need to send or received a DATA packet, \n
 *          You can perform @ref Sn_CR_CLOSE and @ref _Sn_SR_ is changed to @ref SOCK_CLOSED.
 * @note It is valid only in IPRAW6 mode such as @ref Sn_MR_IPRAW6.
 * @sa _Sn_SR_, _Sn_CR_, _Sn_IR_, _Sn_IRCLR_, _Sn_MR_, _Sn_DIP6R_, _Sn_PNR_
 * @sa getSn_SR, getSn_CR(), getSn_IR(), setSn_IRCLR(), setSn_CR(), getSn_MR(), setSn_MR(),
 *     getSn_DIP6R(), setSn_DIP6R(), getSn_PNR(), setSn_PNR() 
 */
#define SOCK_IPRAW6          (0x33)

/**
 * @brief MACRAW SOCKETn mode
 * @details @ref SOCK_MACRAW indicates SOCKET0 is opened as MACRAW mode.\n
 *          @ref _Sn_SR_ is changed from @ref SOCK_CLOSED to @ref SOCK_MACRAW when @ref Sn_CR_OPEN command is ordered with @ref Sn_MR_MACRAW.\n
 *          MACRAW SOCKET0 can be sent or received a pure Ethernet frame packet to/from any peer.
 * @note  It is valid only in SOCKET0.
 * @sa _Sn_SR_, _Sn_CR_, _Sn_IR_, _Sn_IRCLR_, _Sn_MR_
 * @sa getSn_SR, getSn_CR(), getSn_IR(), setSn_IRCLR(), setSn_CR(), getSn_MR(), setSn_MR(),
 */
#define SOCK_MACRAW          (0x42)

#define SOCK_PPPOE           0x5F   // pppoe socket

#endif		/* _TYPE_H_ */

