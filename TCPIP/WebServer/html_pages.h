/*
	FreeRTOS V2.5.4 - Copyright (C) 2003, 2004 Richard Barry.

	This file is part of the FreeRTOS distribution.

	FreeRTOS is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	FreeRTOS is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with FreeRTOS; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

	A special exception to the GPL can be applied should you wish to distribute
	a combined work that includes FreeRTOS, without being obliged to provide
	the source code for any proprietary components.  See the licensing section 
	of http://www.FreeRTOS.org for full details of how and when the exception
	can be applied.

	***************************************************************************
	See http://www.FreeRTOS.org for documentation, latest information, license 
	and contact details.  Please ensure to read the configuration and relevant 
	port sections of the online documentation.
	***************************************************************************
*/

#ifndef HTML_PAGES_H
#define HTML_PAGES_H

/* Simply defines some strings that get sent as HTML pages. */

const unsigned portCHAR * const cSamplePageFirstPart =
"HTTP/1.0 200 OK\r\n"
"Content-type: text/html\r\n"
"\r\n"																				 
"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n"
"<html>\r\n"
"<head>\r\n"
"<META HTTP-EQUIV=\"Refresh\" Content=\"5; URL=/\">\r\n"
"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1251\">"
"<meta http-equiv=\"Content-Language\" content=\"ru\">"
//"<title>�� �����-�</title>\r\n"
"<title>Main page</title>\r\n"
"</head>\r\n"
"<body BGCOLOR=\"#CCCCFF\">\r\n"
"<font face=\"arial\">\r\n"
//"<h2>���������� WEB-������ �� �����-�<br><br><small>SINFO forever!!!</small></h2>\r\n"
//"<h2>���������� WEB-������ �� �����-�<br></h2>\r\n"
"<h2>���������� WEB-������<br></h2>\r\n"
"<p>\r\n"
"��� �������� �������� ���������� WEB-��������, ���������� �� ���������� ADSP-BF533.\r\n<pre>";

const unsigned portCHAR * const cSamplePageSecondPart =
"</pre>"
"�������� �������� (F5 �� Internet Explorer) ����� ������� ��������� �������� �������."
"</font>\r\n"
"</body>\r\n"
"</html>\r\n";

#endif

