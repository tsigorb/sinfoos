#include "BF533 Flags.h"
#include <ccblkfn.h>
// ########################
// ����������� WizNet3150A
// ########################

/* RTOS Scheduler include files. */
#include "FreeRTOS/FreeRTOS.h"
#include "FreeRTOS/queue.h"
#include "FreeRTOS/task.h"

#include "Sinfo.h"
#include <signal.h>
#include <string.h> 
#include <stdio.h> 
#include "TCPIP.h"
#include "html_pages_new.h"
#include "../RS485/keyboard.h"
#include "SNMP\Snmp.h"
#include "Led.h"
#include "../FILES/FileSys.h"

#define	NumbMKC		  	4		// ������������ ����� ������� ���

extern xQueueHandle	xQueueReply[];
extern xQueueHandle	xQueueCommand;
extern int MallocSize[];
extern int FreeSize[];
extern int TotalTime[];
extern int Percent[];
extern short CountErrMalloc; // ������� ������ ��� ��������� ������
extern unsigned portCHAR MassM1[NumberKeyboard];//     ������ �������� ������� ���������
extern unsigned portCHAR MassInclKey[NumberKeyboard]; //������ ���������� ���������

static unsigned int TotalTimeMs[32]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
static unsigned int PercentMy[32]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
char NameWebF[50];

//-----------------------------------------------------------------------
// ����� ������ ����� � ���� �������
//-----------------------------------------------------------------------
void vTaskListTab(unsigned portCHAR s, unsigned portCHAR *ListBuf)
{
unsigned portCHAR *TRInfo;
bool TR, TD;
short i, TRIndex, ln;
 TR = TD = false;
 TRIndex = 0;
 ln = strlen((char *)ListBuf);
 TRInfo = pvPortMalloc( 70 ); // �������� �������
 if (TRInfo)
 {
	 for (i = 0; i < ln; i++)
	 {
	 	if ((*ListBuf == 0x0A) || (*ListBuf == 0x0D))
	 	{
	 		TR = true;
	 		TD = true; 
		}
	 	else if (*ListBuf == 0x09)  TD = true; // ��������� - ��������� ������ �������
	 	else // ������ 
	 	{
	  		if (TR)
	 		{
	 			if (TRIndex) 
	 			{
	  				TRInfo[TRIndex++] = '\0';			
	 				send(s, TRInfo, strlen((portCHAR *)TRInfo));// ���� ������ - ����� �������������� ������ 		
	 			}
	 			TRIndex = 0;
	 			strcpy((portCHAR *)TRInfo, "<TR>"); // ������������ �����
	 			TRIndex = 4;
	 			TR = false;
	 		}
	 		if (TD)
	 		{
	 			strcpy((portCHAR *)(TRInfo) + TRIndex, "<TD>"); // ������������ ����� ������ 
	 			TRIndex += 4; 		
	 			TRInfo[TRIndex++] = *ListBuf;
	 			TD = false;	
	 		}
			else
	 		{
	 			// �����  �������� � ������ �������
	 			TRInfo[TRIndex++] = *ListBuf;
	 		}
	 	}
	 	ListBuf++;	
	 }
	 TRInfo[TRIndex++] = '\0';			
	 send(s, TRInfo, strlen((portCHAR *)TRInfo));// ����� ��������� �������������� ������ 
	 vPortFree( TRInfo );	// �������� ������������� ������� 
	}
	else CountErrMalloc += 1;
}
//-----------------------------------------------------------------------
// ����� ���������� � ������������ ������ � �������� ����������
//-----------------------------------------------------------------------
void FindTaskName(unsigned portCHAR s, unsigned portCHAR *ListBuf)
{
unsigned portCHAR *TRInfo;
unsigned portCHAR sInfo[10], sInfoName[7];
bool Name, ID;
short i, Index, ln;
int IdTask;

 Name = ID = false;
 Index = 0;
 ln = strlen((char *)ListBuf);
 TRInfo = pvPortMalloc( 70 ); // �������� �������
 if (TRInfo)
 {
	 for (i = 0; i < ln; i++)
	 {
	 	if ((*ListBuf == 0x0A) || (*ListBuf == 0x0D))
	 	{
	 		if (ID) // ����� ID
	 		{
	  			sInfo[Index++] = '\0';	
	  			ID = false;
				Index = 0;
	  			IdTask = atoi((char *)sInfo);	// ����� ������			
				TotalTimeMs[IdTask] += TotalTime[IdTask]/1000; TotalTime[IdTask]=0;
				PercentMy[IdTask]= Percent[IdTask];										
				sprintf( (char *)TRInfo,"<TR><TD>%s <TD> %X <TD> %X <TD> %u <TD> %u",sInfoName,MallocSize[IdTask],FreeSize[IdTask],TotalTimeMs[IdTask],PercentMy[IdTask]);				
				send(s, TRInfo, strlen((portCHAR *)TRInfo));
	  				
	 		}
	 		Name = true; // ������  ����� ������
		}
	 	else if ((*ListBuf == 0x09) || (*ListBuf == 0x20)) 
	 	{
	  		if (Name)// ����� �����
	 		{
				sInfoName[Index++] = '\0';			
		 		Name = false;
	 		}
			Index = 0;
	 		ID = true; 
	 	}
	 	else // ������ 
	 	{
	  		if (Name)	sInfoName[Index++] = *ListBuf;
	 		if (ID) sInfo[Index++] = *ListBuf;
	 	}
	 	ListBuf++;	
	 }
	 vPortFree( TRInfo );	// �������� ������������� ������� 
 }
 else CountErrMalloc += 1;
}

//-----------------------------------------------------------------------
//  ����������� WEB ������
//-----------------------------------------------------------------------
void WEBTask(void)
{
short csel;
char *ptr2; 
int len_ptr2;
static int RecvSize;
 int i, j, j1;
 static HANDLE hFile;
 static MYFILE stFile1;
 unsigned int lIndex;
 static sCommand stComa; // ��������� ��� ���������� � ������� ��������� xQueueCommand
 static sInfoKadr stReply; // ��������� ��� ������ �� ������� ��������
static unsigned char Coma[4];
 unsigned int Mask;
 unsigned short MaskShift;
 char *td1, Pos;
 char NumChan[3];
 short InCall; // ����. ������
static  struct tm *timep;
static time_t t1;
unsigned char NumberCh, NumberMod;
 
	csel= select(HTTPSocket,SEL_CONTROL);
		switch(csel)
		{
			case SOCK_ESTABLISHED:						// If client is connected 
				LedOn(ledUpr); // �������� ���������
				if ((RecvSize = select(HTTPSocket, SEL_RECV)) > 0) 			// Confirm if received the data
				{
					j1= xTaskGetTickCount();
					if (RecvSize > MAX_BUF_SIZE) RecvSize = MAX_BUF_SIZE;	// Handle as much as buffer size first when receiving data more than system buffer
					RecvSize = recv(HTTPSocket, tcpRecieveBuf, RecvSize);			// Loopback received data from client 
					strncpy(NameWebF, (char *)tcpRecieveBuf, 5);
					NameWebF[5] = '\0';
					if (strcmp(NameWebF, "GET /")) // ����� ����� �������
					{
						// ������ �������� ��������
						send(HTTPSocket, (unsigned char *)cSamplePageBadRequest, strlen((portCHAR *)cSamplePageBadRequest));
					}
					else // ������ ��������
					{
						if (tcpRecieveBuf[5] == ' ')	// ����� ����� ����� � �������
						{
							send(HTTPSocket, (unsigned char *)cSamplePageOK, strlen((portCHAR *)cSamplePageOK));
							send(HTTPSocket, (unsigned char *)cSamplePageFirst, strlen((portCHAR *)cSamplePageFirst));
						}
						else 	
						{ // ������ �� ����
							ptr2 = strchr ((char* )tcpRecieveBuf+5, ' '); 	
							if (ptr2) // ������ ������
							{
								len_ptr2 = ptr2-((char* )tcpRecieveBuf+5);
								if (len_ptr2 > 	50) len_ptr2 = 50;
								strncpy(NameWebF, (char* )tcpRecieveBuf+5, len_ptr2);
								NameWebF[len_ptr2] = '\0';
								hFile = CreateFile(NameWebF, OPEN_EXISTING | GENERIC_READ, 0);// �������� �����
								if(hFile >= 0) 
								{
									// ���� ����������
									send(HTTPSocket, (unsigned char *)cSamplePageOK, strlen((portCHAR *)cSamplePageOK));
									do 
									{
										len_ptr2 = ReadFile(hFile, tcpRecieveBuf, MAX_BUF_SIZE );	// ������ ����������� ���� � ����
										send(HTTPSocket, tcpRecieveBuf, len_ptr2); // ������ ����������� ����
									}
									while (len_ptr2 == MAX_BUF_SIZE);
									CloseHandle( hFile);																		
								}
								else
								{
									if (!strcmp(NameWebF,NameFile1))	// P1.html "������ �����"
									{
										strcpy( (char *)tcpRecieveBuf, (char *)cSamplePageOK );
										strcat( (char *)tcpRecieveBuf, (char *)cHide1);
										strcat( (char *)tcpRecieveBuf, (char *)cNamePage1);
										strcat( (char *)tcpRecieveBuf, (char *)cHide2);
										send(HTTPSocket, (unsigned char *)tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));
										//	����� ������ ����� � ���� �������
										strcpy((portCHAR *)tcpRecieveBuf, "<TABLE  RULES=COLS WIDTH=70%%>");
										strcat((portCHAR *)tcpRecieveBuf, "<TR BGCOLOR=CADETBLUE>");
										strcat((portCHAR *)tcpRecieveBuf, "<TH>��� <TH>���������<TH>���������<TH>����<TH>ID ������");
										send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
										vTaskList((signed char *)tcpRecieveBuf); // ������ ������ �����
										vTaskListTab(HTTPSocket, tcpRecieveBuf); // ����� ������ ����� � ���� �������
										strcpy((portCHAR *)tcpRecieveBuf, "</table><p align=center><a href=\"");
										strcat((portCHAR *)tcpRecieveBuf, NameWebF);
										strcat((portCHAR *)tcpRecieveBuf,"\"><br> ��������  </a> </body></html>");
										send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
									}
									else if (!strcmp(NameWebF,NameFile2))	// P2.html - ������ ������
									{
										strcpy( (char *)tcpRecieveBuf, (char *)cSamplePageOK );
										strcat( (char *)tcpRecieveBuf, (char *)cHide1);
										strcat( (char *)tcpRecieveBuf, (char *)cNamePage2);
										strcat( (char *)tcpRecieveBuf, (char *)cHide2);
										send(HTTPSocket, (unsigned char *)tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));
										strcpy((portCHAR *)tcpRecieveBuf, "<TABLE  RULES=COLS WIDTH=70%%>");
										strcat((portCHAR *)tcpRecieveBuf, "<TR BGCOLOR=CADETBLUE>");
										strcat((portCHAR *)tcpRecieveBuf, "<TH>��� <TH>������(����)<TH>����<TH>");
										send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
					
										i= 0;			
										while (FindNextFile(i, &stFile1) != (unsigned long)-1)
										{
											strcpy( (char *)tcpRecieveBuf, "<TR><TD>");
											lIndex = strlen( (char *)tcpRecieveBuf );
											memcpy(&( tcpRecieveBuf[ lIndex ] ), stFile1.szFileName, 16);
											tcpRecieveBuf[ lIndex + 16]= 0;
											lIndex = strlen( (char *)tcpRecieveBuf );
											sprintf( &( (char)tcpRecieveBuf[ lIndex ] ),"<TD>%6d ", stFile1.FileLen);
											t1= stFile1.FileTime;  // ����� �������� �����
										 	timep= gmtime(&t1);
											lIndex = strlen( (char *)tcpRecieveBuf );
											sprintf( &( (char)tcpRecieveBuf[ lIndex ] ),
												"<TD>%2.2d.%2.2d.%4.4d %2.2d:%2.2d:%2.2d ",
												timep->tm_mday, timep->tm_mon+1, timep->tm_year+1900,
												timep->tm_hour, timep->tm_min, timep->tm_sec
												);
											send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));
											i++;
										}
										strcpy((portCHAR *)tcpRecieveBuf, "</table><p align=center><a href=\"");
										strcat((portCHAR *)tcpRecieveBuf, NameWebF);
										strcat((portCHAR *)tcpRecieveBuf,"\"><br> ��������  </a> </body></html>");
										send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
									}
									else if (!strcmp(NameWebF,NameFile3))	// P3.html "����������"
									{
										strcpy( (char *)tcpRecieveBuf, (char *)cSamplePageOK );
										strcat( (char *)tcpRecieveBuf, (char *)cHide1);
										strcat( (char *)tcpRecieveBuf, (char *)cNamePage3);
										strcat( (char *)tcpRecieveBuf, (char *)cHide2);
										send(HTTPSocket, (unsigned char *)tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));
					
										strcpy( (char *)tcpRecieveBuf, "<p> �������� ���������� ������� = " );
										lIndex = strlen( (char *)tcpRecieveBuf );
										sprintf( &( (char)tcpRecieveBuf[ lIndex ] ),"%6.6d",( unsigned portLONG ) xTaskGetTickCount());
										strcat( (char *)tcpRecieveBuf, "<br>��������� ���������� ������ = 0x");
										lIndex = strlen( (char *)tcpRecieveBuf );
										ultoa( ( unsigned portLONG ) space_unused(), &( (char)tcpRecieveBuf[ lIndex ] ), 0 );
										strcat( (char *)tcpRecieveBuf, "<br>��������� ������� ������ = 0x");
										lIndex = strlen( (char *)tcpRecieveBuf );
										ultoa( ( unsigned portLONG ) heap_space_unused(1), &( (char)tcpRecieveBuf[ lIndex ] ), 0 );
										strcat( (char *)tcpRecieveBuf, "<br>" );
										send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));
										
										strcpy( (char *)tcpRecieveBuf, "������ ��:");
										send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));
										sprintf( (char *)tcpRecieveBuf,
											" %8s  %12s %8s %12s\r\n",
											(unsigned portCHAR *)FwVersionInfo[2],	(unsigned portCHAR *)FwVersionInfo[0],
											(unsigned portCHAR *)FwVersionInfo[1],	(unsigned portCHAR *)FwVersionInfo[3]
											);
										send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));
										
										strcpy( (char *)tcpRecieveBuf, "<br>\r\n<h7>������������ ������ � �������� ����������:</h7><TABLE COLS=5  RULES=COLS WIDTH=80%><TR BGCOLOR=CADETBLUE>" );
										lIndex = strlen( (char *)tcpRecieveBuf );
										strcat( &( (char)tcpRecieveBuf[ lIndex ] ), "<TH>	TaskId<TH>malloc<TH>free<TH>Total<TH>%" );
										send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));
										vTaskList((signed char *)tcpRecieveBuf); // ������ ������ �����
										FindTaskName(HTTPSocket, tcpRecieveBuf); // ����� ����������									
										strcpy((portCHAR *)tcpRecieveBuf, "</table><p align=center><a href=\"");
										strcat((portCHAR *)tcpRecieveBuf, NameWebF);
										strcat((portCHAR *)tcpRecieveBuf,"\"><br> ��������  </a> </body></html>");
										send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
									}
									else if (!strcmp(NameWebF,NameFile4))	// P4.html - ���������� � ������� � ����������
									{
										strcpy( (char *)tcpRecieveBuf, (char *)cSamplePageOK );
										strcat( (char *)tcpRecieveBuf, (char *)cHide1);
										strcat( (char *)tcpRecieveBuf, (char *)cNamePage4);
										strcat( (char *)tcpRecieveBuf, (char *)cHide2);
										send(HTTPSocket, (unsigned char *)tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));
										strcpy((portCHAR *)tcpRecieveBuf, "<TABLE  RULES=COLS WIDTH=70%%>");
										strcat((portCHAR *)tcpRecieveBuf, "<TR BGCOLOR=CADETBLUE>");
										strcat((portCHAR *)tcpRecieveBuf, "<TH>����� <TH>������");
										strcat((portCHAR *)tcpRecieveBuf,"<COLGROUP ALIGN=CENTER><COLGROUP ALIGN=CENTER><TBODY>");											
										send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
										stComa.Source= UserUDP; //�������� �������
										stComa.Length = 2;
										stComa.BufCommand = pvPortMalloc( 2 ); // �������� �������
										if (stComa.BufCommand)
										{
											stComa.BufCommand[0] = 64; // ������ ��������� ����
											stComa.BufCommand[1] = 0;
											if (  xQueueSend( xQueueCommand, &stComa, 1 ) == errQUEUE_FULL) // ���������� � ������� 
											{   // ������� �����������
												vPortFree(stComa.BufCommand);	// �������� ������������� �������
											}
											else if( xQueueReceive( xQueueReply[stComa.Source], &stReply, 110 ) ) // 
											{
												td1= pvPortMalloc( 200 ); // �������� �������
												if (td1)
												{
													for (i = 0; i < NumberBoard + NumbMKC; i++)			// ���������� � �������� �����	
													{
														Mask = (stReply.ByteInfoKadr[4] | (stReply.ByteInfoKadr[3]<<8) | 
															(stReply.ByteInfoKadr[2]<<16) | (stReply.ByteInfoKadr[1]<<24)); 
														MaskShift = i;
														Mask >>=  MaskShift * 2; 
														Mask &= 0x00000003;
														if ((Mask == 0x00000001) || (Mask == 0x00000002))
														{
															if (Mask == 0x00000001)
															{
																strcpy((portCHAR *)tcpRecieveBuf,"<tr><td BGCOLOR= tomato >"); // � ����������
															}
															else if (Mask == 0x00000002)
															{
																strcpy((portCHAR *)tcpRecieveBuf,"<tr><td BGCOLOR= lightgreen>"); // � ������� ���������
															}
															if (i < 12)
															{
																strcat((portCHAR *)tcpRecieveBuf, " ��� ");						
																lIndex = strlen( (char *)tcpRecieveBuf );
																sprintf( &( (char)tcpRecieveBuf[ lIndex ] ),"%2u", i+1);
																strcpy((portCHAR *)td1, "<td><a  href=\"TuneChan");
																lIndex = strlen( (char *)td1 );
																sprintf( &( (char)td1[ lIndex ] ),"%u", (i*4)+1);
																strcat((portCHAR *)td1, ".htm\" title=\"��������� ������\">�����  ");	
																lIndex = strlen( (char *)td1 );
																sprintf( &( (char)td1[ lIndex ] ),"%2.2u", (i*4)+1);
															}
															else
															{
																strcat((portCHAR *)tcpRecieveBuf, "<a  href=\"TuneMKC");
																
																lIndex = strlen( (char *)tcpRecieveBuf );
																sprintf( &( (char)tcpRecieveBuf[ lIndex ] ),"%u", (i+1)-12);
																strcat((portCHAR *)tcpRecieveBuf, ".html\" title=\"��������� ������\"> ��� ");						
																lIndex = strlen( (char *)tcpRecieveBuf );
																sprintf( &( (char)tcpRecieveBuf[ lIndex ] ),"%2u", (i+1)-12);
																strcpy((portCHAR *)td1, "</a><td><a  href=\"TuneChan");
																lIndex = strlen( (char *)td1 );
																sprintf( &( (char)td1[ lIndex ] ),"%u", (i*4)+1);
																strcat((portCHAR *)td1, ".htm\" title=\"������������ ������\">�����  ");	
																lIndex = strlen( (char *)td1 );
																sprintf( &( (char)td1[ lIndex ] ),"%2.2u", ((i-12)*4)+1);
															}
															strcat((portCHAR *)td1, "</a>");						
															strcat((portCHAR *)tcpRecieveBuf, td1);						
															send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf)); // 	
															for (j = i*4+2; j<  i*4+5; j++)
															{
																strcpy((portCHAR *)td1, "<td><a  href=\"TuneChan");
																lIndex = strlen( (char *)td1 );
																sprintf( &( (char)td1[ lIndex ] ),"%u", j);
																if (i < 12)
																{
																	strcat((portCHAR *)td1, ".htm\" title=\"��������� ������\">�����  ");	
																	lIndex = strlen( (char *)td1 );
																	sprintf( &( (char)td1[ lIndex ] ),"%2.2u", j);
																}
																else
																{
																	strcat((portCHAR *)td1, ".htm\" title=\"������������ ������\">�����  ");	
																	lIndex = strlen( (char *)td1 );
																	sprintf( &( (char)td1[ lIndex ] ),"%2.2u", j-48);
																}
																strcat((portCHAR *)td1, "</a>");						
																strcat((portCHAR *)tcpRecieveBuf, td1);						
																strcpy((portCHAR *)tcpRecieveBuf,"<tr><td>&nbsp"); // 
																lIndex = strlen( (char *)td1 );
																if (i < 12)	sprintf( &( (char)td1[ lIndex-6 ] ),"%2.2u", j);
																else sprintf( &( (char)td1[ lIndex-6 ] ),"%2.2u", j-48);
																strcat((portCHAR *)td1, "</a>");						
																strcat((portCHAR *)tcpRecieveBuf, td1);						
																send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf)); // 	
															}
														}	
													}
													vPortFree(td1);	// �������� ������������� �������
												}
												vPortFree(stReply.ByteInfoKadr);	// �������� ������������� �������
											}
										}
										strcpy((portCHAR *)tcpRecieveBuf, "</FONT></CENTER></TABLE></BODY>");
										send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf)); // 
									}
									else if (!strncmp(NameWebF,NameFileTune,8))	// TuneChanX.htm - �������� �������� ������
									{
										Pos = (char) (strchr(NameWebF,'.') - NameWebF);
										strncpy(NumChan, NameWebF+8, Pos-8);
										NumChan[Pos-8] = '\0';
										NumberCh = (unsigned char)atoi(NumChan);
										if (NumberCh < 48)
										{
											strcpy( (char *)tcpRecieveBuf, (char *)cSamplePageOK );
											strcat( (char *)tcpRecieveBuf, (char *)cHide1);
											strcat( (char *)tcpRecieveBuf, (char *)cNameTuneChan);
											strcat( (char *)tcpRecieveBuf, (char *)NumChan);
											strcat( (char *)tcpRecieveBuf, (char *)cHide2);
											send(HTTPSocket, (unsigned char *)tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));
											
											strcpy((portCHAR *)tcpRecieveBuf, "<TABLE WIDTH=70% BORDER=1 FRAME=BOX>");
											strcat((portCHAR *)tcpRecieveBuf, "<TR BGCOLOR=CADETBLUE>");
											strcat((portCHAR *)tcpRecieveBuf, "<TH ALIGN=CENTER>�������� <TH>��������");
											strcat((portCHAR *)tcpRecieveBuf,"<COLGROUP ALIGN=LEFT><COLGROUP ALIGN=CENTER><TBODY>");											
											send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
											
											stComa.Source= UserUDP; //�������� �������
											stComa.Length = 3;
											stComa.BufCommand = pvPortMalloc( stComa.Length ); // �������� �������
											if (stComa.BufCommand)
											{
												stComa.BufCommand[0] = 15; // ������ ���������� ������ ���
												stComa.BufCommand[1] = 0;
												stComa.BufCommand[2] = NumberCh-1;
												if (  xQueueSend( xQueueCommand, &stComa, 1 ) == errQUEUE_FULL) // ���������� � ������� 
												{   // ������� �����������
													vPortFree(stComa.BufCommand);	// �������� ������������� �������
												}
												else if( xQueueReceive( xQueueReply[stComa.Source], &stReply, 110 ) ) // 
												{
													if (stReply.L != 2) // �� ������
													{
														strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ������� ����������� �������  0..28<td>");
														lIndex = strlen( (char *)tcpRecieveBuf );
														sprintf( &( (char)tcpRecieveBuf[ lIndex ] ),"%g", (4.5-(stReply.ByteInfoKadr[3] * 1.5)));
														strcat((portCHAR *)tcpRecieveBuf,", ��");
														send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
																		
														strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> �������� ����������� ������� 1..28<td>");
														lIndex = strlen( (char *)tcpRecieveBuf );
														sprintf( &( (char)tcpRecieveBuf[ lIndex ] ),"%g", (9-(stReply.ByteInfoKadr[4] * 1.5)));
														strcat((portCHAR *)tcpRecieveBuf,", ��");
														send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
																		
														strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ������ �������� �� ������� 3000 ��<td>");
														lIndex = strlen( (char *)tcpRecieveBuf );
														sprintf( &( (char)tcpRecieveBuf[ lIndex ] ),"%d", stReply.ByteInfoKadr[5]);
														send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
																		
														strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ���������<td> &nbsp");
														strcat((portCHAR *)tcpRecieveBuf, " <tr><td>  <pre>	����������");
														if  (stReply.ByteInfoKadr[7] & 2)	strcat((portCHAR *)tcpRecieveBuf, " <td> <pre>��������");
														else strcat((portCHAR *)tcpRecieveBuf, " <td><pre> ���������");
														
														strcat((portCHAR *)tcpRecieveBuf, " <tr><td> <pre>	���������");
														if  (stReply.ByteInfoKadr[7] & 4)	strcat((portCHAR *)tcpRecieveBuf, " <td><pre>  �������");
														else strcat((portCHAR *)tcpRecieveBuf, " <td><pre> ��������");
														
														strcat((portCHAR *)tcpRecieveBuf, " <tr><td> <pre>	�������");
														if  (stReply.ByteInfoKadr[7] & 8)	strcat((portCHAR *)tcpRecieveBuf, " <td><pre> �������");
														else strcat((portCHAR *)tcpRecieveBuf, " <td><pre> ��������");
														
														strcat((portCHAR *)tcpRecieveBuf, " <tr><td><pre>	��� ������");
														if  (stReply.ByteInfoKadr[7] & 16)	strcat((portCHAR *)tcpRecieveBuf, " <td><pre> 600 ��");
														else strcat((portCHAR *)tcpRecieveBuf, " <td><pre> ����");
														
														strcat((portCHAR *)tcpRecieveBuf, " <tr><td> <pre>	���");
														if  (stReply.ByteInfoKadr[7] & 32)	strcat((portCHAR *)tcpRecieveBuf, " <td><pre>  ��������");
														else strcat((portCHAR *)tcpRecieveBuf, " <td><pre> ���������");
														
														strcat((portCHAR *)tcpRecieveBuf, " <tr><td> <pre>	��������� ������ ���");
														if  (stReply.ByteInfoKadr[7] & 64)	strcat((portCHAR *)tcpRecieveBuf, " <td><pre> �����");
														else strcat((portCHAR *)tcpRecieveBuf, " <td><pre> ��������");
														send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
																		
														
														strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ��� ��������� ������");
														InCall = (stReply.ByteInfoKadr[9] << 8) | stReply.ByteInfoKadr[8];
														if (InCall)
														{
															for (i = 1; i < 0x6400; i*=2)
															{
																if (InCall & i)
																{
																	switch (i)
																	{
																		case 1: strcat( (portCHAR *)tcpRecieveBuf, "<td><pre>���");	
																		break;													
																		case 2: strcat( (portCHAR *)tcpRecieveBuf, "<td><pre>���");	
																		break;													
																		case 4: strcat( (portCHAR *)tcpRecieveBuf, "<td><pre>��������");	
																		break;													
																		case 8: strcat( (portCHAR *)tcpRecieveBuf, "<td><pre>����");	
																		break;													
																		case 0x10: strcat( (portCHAR *)tcpRecieveBuf, "<td><pre>�� ������");	
																		break;													
																		case 0x20: strcat( (portCHAR *)tcpRecieveBuf, "<td><pre>��������� �����������");			
																		break;													
																		case 0x40: strcat( (portCHAR *)tcpRecieveBuf, "<td><pre>��������");	
																		break;													
																		case 0x80: strcat( (portCHAR *)tcpRecieveBuf, "<td><pre>�����������");	
																		break;													
																		case 0x100: strcat( (portCHAR *)tcpRecieveBuf, "<td><pre>��");	
																		break;													
																		case 0x200: strcat( (portCHAR *)tcpRecieveBuf, "<td><pre>����������");	
																		break;													
																		case 0x400: strcat( (portCHAR *)tcpRecieveBuf, "<td><pre>�������� ��������");	
																		break;													
																		case 0x800: strcat( (portCHAR *)tcpRecieveBuf, "<td><pre>�������� ���");	
																		break;													
																		case 0x1000: strcat( (portCHAR *)tcpRecieveBuf, "<td><pre>�������� ����");	
																		break;													
																		case 0x2000: strcat( (portCHAR *)tcpRecieveBuf, "<td><pre>����");	
																		break;													
																	}
																	send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
																	strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> &nbsp");
																}
															}
														}
														if (!(InCall & 32))
														{
															strcat( (portCHAR *)tcpRecieveBuf, "<td><pre> ���������� �����������");														
															send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
														}
																		
														strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ������� ������� �� ������<td>");
														lIndex = strlen( (char *)tcpRecieveBuf );
														sprintf( &( (char)tcpRecieveBuf[ lIndex ] ),"%d", stReply.ByteInfoKadr[10]);
														send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
																		
														strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ������� ��������<td>");
														lIndex = strlen( (char *)tcpRecieveBuf );
														sprintf( &( (char)tcpRecieveBuf[ lIndex ] ),"%d", stReply.ByteInfoKadr[11]);
														send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
																		
														strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ����������� ����������� ������� ���<td>");
														lIndex = strlen( (char *)tcpRecieveBuf );
														sprintf( &( (char)tcpRecieveBuf[ lIndex ] ),"%g", stReply.ByteInfoKadr[12] * 1.5);
														strcat((portCHAR *)tcpRecieveBuf,", ��");
														send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
																		
														strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ����� ������������� 0..100<td>");
														lIndex = strlen( (char *)tcpRecieveBuf );
														sprintf( &( (char)tcpRecieveBuf[ lIndex ] ),"%d", stReply.ByteInfoKadr[13]);
														send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
																
														switch (stReply.ByteInfoKadr[15])
														{
															case 1 :
															strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ����� ������ ��������� <td> ����������������");
															break;
															case 2 :
															strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ����� ������ ��������� <td> �������������");
															break;
															case 3 :
															strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ����� ������ ��������� <td> ��");
															break;
															case 4 :
															strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ����� ������ ��������� <td> ���");
															break;
														}
														send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
																		
														strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ���������� ������� ��������������<td>");
														lIndex = strlen( (char *)tcpRecieveBuf );
														sprintf( &( (char)tcpRecieveBuf[ lIndex ] ),"%d", stReply.ByteInfoKadr[16]);
														send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
				
														strcpy((portCHAR *)tcpRecieveBuf, "</table><p align=center> <a href=\"");
														strcat((portCHAR *)tcpRecieveBuf, NameWebF);
														strcat((portCHAR *)tcpRecieveBuf,"\"><br> ��������  </a> </body></html>");
														send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
														
													}
													vPortFree(stReply.ByteInfoKadr);	// �������� ������������� �������
												}
											}
										}
										else // NumberCh > 48 -> ���
										{
											NumberMod = NumberCh / 4; 
											if (NumberMod % 4) NumberMod++; // ����� ��� 13..16[]
											NumberCh -= 48;
											strcpy( (char *)tcpRecieveBuf, (char *)cSamplePageOK );
											strcat( (char *)tcpRecieveBuf, (char *)cHide1);
											strcat( (char *)tcpRecieveBuf, (char *)cNameTuneStream);
											lIndex = strlen( (char *)tcpRecieveBuf );
											sprintf( &( (char)tcpRecieveBuf[ lIndex ] ),"%d", NumberCh);		// ����� ������ [1..16]									
											strcat( (char *)tcpRecieveBuf, (char *)cHide2);
											send(HTTPSocket, (unsigned char *)tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));
											
											strcpy((portCHAR *)tcpRecieveBuf, "<TABLE WIDTH=60% BORDER=1 FRAME=BOX>");
											strcat((portCHAR *)tcpRecieveBuf, "<TR BGCOLOR=CADETBLUE>");
											strcat((portCHAR *)tcpRecieveBuf, "<TH ALIGN=CENTER>�������� <TH>��������");
											strcat((portCHAR *)tcpRecieveBuf,"<COLGROUP ALIGN=LEFT><COLGROUP ALIGN=CENTER><TBODY>");											
											send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
											
											stComa.Source= UserUDP; //�������� �������
											stComa.Length = 3;
											stComa.BufCommand = pvPortMalloc( stComa.Length ); // �������� �������
											if (stComa.BufCommand)
											{
												stComa.BufCommand[0] = 10; // ������ ���������� ������ ���
												stComa.BufCommand[1] = 107;
												stComa.BufCommand[2] = NumberMod;
												if (  xQueueSend( xQueueCommand, &stComa, 1 ) == errQUEUE_FULL) // ���������� � ������� 
												{   // ������� �����������
													vPortFree(stComa.BufCommand);	// �������� ������������� �������
												}
												else if( xQueueReceive( xQueueReply[stComa.Source], &stReply, 110 ) ) // 
												{
													if (stReply.ByteInfoKadr[2] == 1) // �� ������
													{	// ������� ����������� 
														NumberCh = NumberCh % 4; //�������� 
														if (!(NumberCh % 4)) NumberCh += 4; // ����� ��� [13..16]
														strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ����� �1<td>");
														if (((MKC_C0_C3 *)&stReply.ByteInfoKadr[NumberCh+2])->FlowE1) strcat((portCHAR *)tcpRecieveBuf,"�������  ");
														else strcat((portCHAR *)tcpRecieveBuf,"��������  ");
														send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
														
														strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ��� ������<td>");
														if (((MKC_C0_C3 *)&stReply.ByteInfoKadr[NumberCh+2])->Pcm30) strcat((portCHAR *)tcpRecieveBuf,"���31 ");
														else strcat((portCHAR *)tcpRecieveBuf,"���30  ");
														send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
														
														strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ��������� CRC ������ �1<td>");
														if (((MKC_C0_C3 *)&stReply.ByteInfoKadr[NumberCh+2])->StateCrc) strcat((portCHAR *)tcpRecieveBuf,"�������  ");
														else strcat((portCHAR *)tcpRecieveBuf,"��������  ");
														send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
														
														strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ����� \"�� ��������� �������\"<td>");
														if (((MKC_C0_C3 *)&stReply.ByteInfoKadr[NumberCh+2])->ExternalTrain) strcat((portCHAR *)tcpRecieveBuf,"�������  ");
														else strcat((portCHAR *)tcpRecieveBuf,"��������  ");
														send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
														
														strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ����� \"�� ����\"<td>");
														if (((MKC_C0_C3 *)&stReply.ByteInfoKadr[NumberCh+2])->InternalTrain) strcat((portCHAR *)tcpRecieveBuf,"������� ");
														else strcat((portCHAR *)tcpRecieveBuf,"��������  ");
														send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
														
														switch (((MKC_C0_C3 *)&stReply.ByteInfoKadr[NumberCh+2])->KoeffErr)
														{
															case 0 :
															strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ����� ������������ ������ ��� <td> 10-3");
															break;
															case 1 :
															strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ����� ������������ ������ ��� <td> 10-4");
															break;
															case 2 :
															strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ����� ������������ ������ ��� <td> 10-5");
															break;
															case 3 :
															strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ����� ������������ ������ ��� <td> 10-6");
															break;
														}
														send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
														if (((MKC_C6_CC *)&stReply.ByteInfoKadr[NumberCh+11])->ExcessKoeffErr) 
														{
															strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ���������� ��������� ������ ������<td>&nbsp");
															send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
														}
														if (((MKC_C5_CB *)&stReply.ByteInfoKadr[NumberCh+7])->LossInputSignal) 
														{
															strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ������ �������� �������<td>&nbsp");
															send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
														}
														if (((MKC_C5_CB *)&stReply.ByteInfoKadr[NumberCh+7])->LossCycleSynchr) 
														{
															strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ������ �������� �������������<td>&nbsp");
															send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
														}
														if (((MKC_C6_CC *)&stReply.ByteInfoKadr[NumberCh+11])->SingleErr) 
														{
															strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ������� ��������� ������	<td>&nbsp");
															send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
														}
														if (((MKC_C5_CB *)&stReply.ByteInfoKadr[NumberCh+7])->LossSynchrCRC) 
														{
															strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ������ ������������� �R�<td>&nbsp");
															send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
														}
														if (((MKC_C6_CC *)&stReply.ByteInfoKadr[NumberCh+11])->ErrES) 
														{
															strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ������� ������ ES<td>&nbsp");
															send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
														}
														if (((MKC_C6_CC *)&stReply.ByteInfoKadr[NumberCh+11])->ErrSES) 
														{
															strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ������� ������ SES<td>&nbsp");
															send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
														}
														if (((MKC_C5_CB *)&stReply.ByteInfoKadr[NumberCh+7])->ErrAIS) 
														{
															strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ������� ������ AIS<td>&nbsp");
															send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
														}
														if (((MKC_C5_CB *)&stReply.ByteInfoKadr[NumberCh+7])->ErrAIS_KI16) 
														{
															strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ������� ������	AIS � ��16<td>&nbsp");
															send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
														}
														if (((MKC_C5_CB *)&stReply.ByteInfoKadr[NumberCh+7])->ErrIAS) 
														{	
															strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ������� ������	IAS<td>&nbsp");
															send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
														}
														if (((MKC_C5_CB *)&stReply.ByteInfoKadr[NumberCh+7])->DamageRemovedSide) 
														{
															strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ������� �� ��������� �������<td>&nbsp");
															send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
														}
														if (((MKC_C5_CB *)&stReply.ByteInfoKadr[NumberCh+7])->DamageRemovedSide16) 
														{
															strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ������� �� ��������� ������� � ��-16<td>&nbsp");
															send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
														}
														strcpy((portCHAR *)tcpRecieveBuf, "</table><p align=center><a href=\"");
														strcat((portCHAR *)tcpRecieveBuf, NameWebF);
														strcat((portCHAR *)tcpRecieveBuf,"\"><br> ��������  </a> </body></html>");
														send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
													}
													vPortFree(stReply.ByteInfoKadr);	// �������� ������������� �������
												}
											}
											
										}
									}
									else if (!strncmp(NameWebF,NameFileMKC,strlen(NameFileMKC)))	// TuneMKCX.htm - �������� �������� ������ ���
									{
										Pos = (char) (strchr(NameWebF,'.') - NameWebF);
										strncpy(NumChan, NameWebF+strlen(NameFileMKC), Pos-strlen(NameFileMKC));
										NumChan[Pos-strlen(NameFileMKC)] = '\0';
										strcpy( (char *)tcpRecieveBuf, (char *)cSamplePageOK );
										strcat( (char *)tcpRecieveBuf, (char *)cHide1);
										strcat( (char *)tcpRecieveBuf, (char *)cNameTuneMod);
										strcat( (char *)tcpRecieveBuf, (char *)NumChan);
										strcat( (char *)tcpRecieveBuf, (char *)cHide2);
										send(HTTPSocket, (unsigned char *)tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));
										
										strcpy((portCHAR *)tcpRecieveBuf, "<TABLE WIDTH=60% BORDER=1 FRAME=BOX>");
										strcat((portCHAR *)tcpRecieveBuf, "<TR BGCOLOR=CADETBLUE>");
										strcat((portCHAR *)tcpRecieveBuf, "<TH ALIGN=CENTER>�������� <TH>��������");
										strcat((portCHAR *)tcpRecieveBuf,"<COLGROUP ALIGN=LEFT><COLGROUP ALIGN=CENTER><TBODY>");											
										send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
									
										stComa.Length = 3;
										stComa.BufCommand = pvPortMalloc( 3 ); // �������� �������
										if (stComa.BufCommand)
										{
											stComa.BufCommand[0] = 10;
											stComa.BufCommand[1] = 107;
											stComa.BufCommand[2] = (unsigned char)(atoi(NumChan)+12);	// ����� ������ ��� 13..16		
											if (  xQueueSend( xQueueCommand, &stComa, 1 ) == errQUEUE_FULL) // ���������� � ������� �������
											{   // ������� �����������
												vPortFree(stComa.BufCommand);	// �������� ������������� �������
											}			
											if( xQueueReceive( xQueueReply[stComa.Source], &stReply, 110 ) ) // 
											{
												if (stReply.ByteInfoKadr[2] == 1) // �� ������
												{	// ������� ����������� 
													strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> �������������<td>");
													if (((MKC_C4 *)&stReply.ByteInfoKadr[7])->Leader) strcat((portCHAR *)tcpRecieveBuf,"������� ");
													else strcat((portCHAR *)tcpRecieveBuf,"�������  ");
													send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
																	
													strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> �������� ������������� � 1-� �����������<td>");
													switch(((MKC_C4 *)&stReply.ByteInfoKadr[7])->Source1)
													{
														case 0 : strcat((portCHAR *)tcpRecieveBuf,"�������� ");
														break;
														case 1 : strcat((portCHAR *)tcpRecieveBuf,"������� ����");
														break;
														case 2 : strcat((portCHAR *)tcpRecieveBuf,"���� 1");
														break;
														case 3 : strcat((portCHAR *)tcpRecieveBuf,"���� 2 ");
														break;
														case 4 : strcat((portCHAR *)tcpRecieveBuf,"���� 3 ");
														break;
														case 5 : strcat((portCHAR *)tcpRecieveBuf,"���� 4 ");
														break;
														default : strcat((portCHAR *)tcpRecieveBuf,"�������� ");
													
													}
													send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
																	
													strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> �������� ������������� �� 2-� �����������<td>");
													switch(((MKC_C4 *)&stReply.ByteInfoKadr[7])->Source2)
													{
														case 0 : strcat((portCHAR *)tcpRecieveBuf,"�������� ");
														break;
														case 1 : strcat((portCHAR *)tcpRecieveBuf,"������� ����");
														break;
														case 2 : strcat((portCHAR *)tcpRecieveBuf,"���� 1");
														break;
														case 3 : strcat((portCHAR *)tcpRecieveBuf,"���� 2 ");
														break;
														case 4 : strcat((portCHAR *)tcpRecieveBuf,"���� 3 ");
														break;
														case 5 : strcat((portCHAR *)tcpRecieveBuf,"���� 4 ");
														break;
														default : strcat((portCHAR *)tcpRecieveBuf,"�������� ");
													
													}
													send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
																	
													if (((MKC_CD *)&stReply.ByteInfoKadr[16])->Damage1)
													{
														strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ������ ������������� 1-�� ������<td>&nbsp");
														send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
													}
													if (((MKC_CD *)&stReply.ByteInfoKadr[16])->Damage2)
													{
														strcpy((portCHAR *)tcpRecieveBuf, " <tr><td> ������ ������������� 2-�� ������<td>&nbsp");
														send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
													}
														
													strcpy((portCHAR *)tcpRecieveBuf, "</table><p align=center> <a href=\"");
													strcat((portCHAR *)tcpRecieveBuf, NameWebF);
													strcat((portCHAR *)tcpRecieveBuf,"\"><br> ��������  </a> </body></html>");
													send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
												}
												vPortFree(stReply.ByteInfoKadr);	// �������� ������������� �������
											}
										}
									}
									else if (!strcmp(NameWebF,NameFile5))	// P5.html - ������ ������� � �� ������� �� �����
									{
										strcpy( (char *)tcpRecieveBuf, (char *)cSamplePageOK );
										strcat( (char *)tcpRecieveBuf, (char *)cHide1);
										strcat( (char *)tcpRecieveBuf, (char *)cNamePage5);
										strcat( (char *)tcpRecieveBuf, (char *)cHide2);
										send(HTTPSocket, (unsigned char *)tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));
										strcpy((portCHAR *)tcpRecieveBuf, "<TABLE  RULES=COLS WIDTH=60%%>");
										strcat((portCHAR *)tcpRecieveBuf, "<TR BGCOLOR=CADETBLUE>");
										strcat((portCHAR *)tcpRecieveBuf, "<TH>������ <TH>������");
										strcat((portCHAR *)tcpRecieveBuf,"<COLGROUP ALIGN=CENTER><COLGROUP ALIGN=CENTER><TBODY>");											
										send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
										for (i = 0; i < NumberKeyboard; i++)
											if (MassM1[i]) // ���� �����
											{
												strcpy((portCHAR *)tcpRecieveBuf, " <tr><td>");
												lIndex = strlen( (char *)tcpRecieveBuf );
												sprintf( &( (char)tcpRecieveBuf[ lIndex ] ),"%2.2d", MassM1[i]);
												if (MassInclKey[i]) // ����� ��������
													strcat((portCHAR *)tcpRecieveBuf,"<td>��������");
												else // ����� �� ��������
													strcat((portCHAR *)tcpRecieveBuf,"<td>�� ��������");
												send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
											}
										strcpy((portCHAR *)tcpRecieveBuf, "</table><p align=center><a href=\"");
										strcat((portCHAR *)tcpRecieveBuf, NameWebF);
										strcat((portCHAR *)tcpRecieveBuf,"\"><br> ��������  </a> </body></html>");
										send(HTTPSocket, tcpRecieveBuf, strlen((portCHAR *)tcpRecieveBuf));// 
									}
									else
									// ���� �� ����������
									send(HTTPSocket, (unsigned char *)cSamplePageNotFound, strlen((portCHAR *)cSamplePageNotFound));
								}
							}
						}
					}
					disconnect(HTTPSocket);
				}
				break;
			case SOCK_CLOSE_WAIT:                           		// If the client request to close connection and wait for closing successfully
				if ((RecvSize = select(HTTPSocket, SEL_RECV)) > 0) 	// Check to receive data when SOCK_CLOSE_WAIT
				{
					if (RecvSize > MAX_BUF_SIZE)		// When receiving larger data than system buffer,it's processed buffer sized data first.
					RecvSize = MAX_BUF_SIZE;
					RecvSize = recv(HTTPSocket, tcpRecieveBuf, RecvSize);
				}
				close(HTTPSocket);	       					// Close the appropriate channel connected to client
				break;
			case SOCK_CLOSED:                                               // If the socket connected to client already has been closed
				// To do loopback service again in closed channel, wait in initialization and server mode
				LedOff(ledUpr); // ����� ���������
				// ReadISRReg(HTTPSocket);
				if( lTCPCreateSocket(HTTPSocket, SOCK_STREAM, ucPortHTTP, 0) );
				listen(HTTPSocket);
				j= xTaskGetTickCount() - j1;
				break;
		}
}
