/*
	FreeRTOS V2.5.4 - Copyright (C) 2003, 2004 Richard Barry.

	This file is part of the FreeRTOS distribution.

	FreeRTOS is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	FreeRTOS is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with FreeRTOS; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

	A special exception to the GPL can be applied should you wish to distribute
	a combined work that includes FreeRTOS, without being obliged to provide
	the source code for any proprietary components.  See the licensing section 
	of http://www.FreeRTOS.org for full details of how and when the exception
	can be applied.

	***************************************************************************
	See http://www.FreeRTOS.org for documentation, latest information, license 
	and contact details.  Please ensure to read the configuration and relevant 
	port sections of the online documentation.
	***************************************************************************
*/

// �� ���� ������ ���� �������� ����� PageMain.html, DvText.html, Str1.html, UL.html

#ifndef HTML_PAGES_H
#define HTML_PAGES_H

/* Simply defines some strings that get sent as HTML pages. */

const unsigned portCHAR * const cSamplePageOK = // ����� �� ������
"HTTP/1.0 200 OK\r\n"
"Content-type: text/html\r\n"
"\r\n"																				 
"<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 3.2//EN\">\r\n";

const unsigned portCHAR * const cSamplePageNotFound = // ���� �� ������
"HTTP/1.0 404 Not Found\r\n";

const unsigned portCHAR * const cSamplePageBadRequest = // �� ������ ������
"HTTP/1.0 400 Bad Request\r\n";

const  portCHAR * const NameFile1 = "P1.html";
const  portCHAR * const NameFile2 = "P2.html";
const  portCHAR * const NameFile3 = "P3.html";
const  portCHAR * const NameFile4 = "P4.html";
const  portCHAR * const NameFile5 = "P5.html";
const  portCHAR * const NameFileTune = "TuneChan";
const  portCHAR * const NameFileMKC = "TuneMKC";

const unsigned portCHAR * const cSamplePageFirst = // ������� ��������
"<HTML><HEAD><TITLE>���������� ������������� �����</TITLE>\r\n"
"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1251\">"
"<meta http-equiv=\"Content-Language\" content=\"ru\">"
"<FRAMESET FRAMEBORDER=0 ROWS=\"10%, *\">\r\n"
"<FRAME SRC=\"DvText.html\"SCROLLING=NO NORESIZE>\r\n"
"<FRAMESET COLS=\"25%,*\">\r\n"
"<FRAME SRC=\"UL.html\"NAME=\"menu\" NORESIZE>\r\n"
"<FRAME SRC=\"Str1.html\"NAME=\"Str1\" >\r\n"
"</FRAMESET>\r\n"
"</FRAMESET>\r\n"
"</HTML>\r\n";

const unsigned portCHAR * const cHide1 = // ������ ��������� ��������
"<BODY BGCOLOR=ANTIQUEWHITE>\r\n"
"<FONT COLOR=DARKGREEN>\r\n"
"<h3><CENTER>";

const unsigned portCHAR * const cHide2 = // ��������� ��������� ��������
"</h3></FONT>\r\n"
"<P><HR NOSHADE SIZE=2></P>\r\n";

// �������� �������
const unsigned portCHAR * const cNamePage1 = "������ �����";
const unsigned portCHAR * const cNamePage2 = "������ ������";
const unsigned portCHAR * const cNamePage3 = "����������";
const unsigned portCHAR * const cNamePage4 = "������ ���� � �������";
const unsigned portCHAR * const cNamePage5 = "������ �������";
const unsigned portCHAR * const cNameTuneChan = "��������� ������ ";
const unsigned portCHAR * const cNameTuneStream = "������������ ������ ";
const unsigned portCHAR * const cNameTuneMod = "��������� ��� ";
#endif

