/*
 * (c)COPYRIGHT
 * ALL RIGHT RESERVED
 *
 * FileName : socket.c
 * Revision History :
 * ----------  -------  -----------  ---------------------------------------------
 * Date        version  Name         Description
 * ----------  -------  -----------  ---------------------------------------------
 * 09/23/2005  1.0      Bong         Create version
 * ----------  -------  -----------  ---------------------------------------------
 * 10/12/2005  2.0      Woo          Release version
 * ----------  -------  -----------  ---------------------------------------------
 * 10/13/2005  2.0.1    Bong         modify sendto() function for UDP defection 
 * ----------  -------  -----------  ---------------------------------------------
 * 10/25/2005  2.0.2    Bong         modify connect(),sendto() function fix subnet bug.
 * ----------  -------  -----------  ---------------------------------------------
 * 11/03/2005  2.0.3    Bong         modify send() function fix send completion bug.
 * ----------  -------  -----------  ---------------------------------------------
 * 11/11/2005  2.0.4    Bong         #define _20051111_
 *                                   modify send() function fix send completion bug.  
 * ----------  -------  -----------  ---------------------------------------------
 * 
 * last update : 11/03/2005 
 */
//#define _20051111_ // version 2.0.4

#include "ccblkfn.h"
#include <string.h>
#include "types.h"
#include "w3150a.h"

#ifdef _20051111_
   #include "timer.h" // for wait function
#endif

/////////////////////////////////////////////////////////////////////
#define d_SendtoTOut    200

/////////////////////////////////////////////////////////////////////
extern unsigned short DataSave1[];

/////////////////////////////////////////////////////////////////////
unsigned int xTaskGetTickCount(void);
void vTaskDelay(unsigned int dwDelay);
bool IsCurrentTickCountLT(unsigned int dwTick);
bool GetMACAddrFromIp(unsigned int dwIP,unsigned char *MACAddr);
unsigned char IsCurrentTickCountGT(unsigned int dwTick0);

void closeW3150a(SOCKET s);

/////////////////////////////////////////////////////////////////////
extern uint8 GW_MAC[6]; // gateway mac address defined in w3150a.c
extern uint8 is_gw_samenet;

uint16 local_port;

extern unsigned char g_SockSendBusy[];
extern unsigned int g_dwSockTOut[];

unsigned int  g_dwTmWait_W3150[5];
bool s_bSendtoFirst=true,s_bW3150Plus=true;

//volatile static unsigned int s_dwUdpBetween=0;

/////////////////////////////////////////////////////////////////////
/**
* Internal Functions
*/

/**
 * \brief socket initialization
 * This function initializes a specified socket and waits until the W3150 has done.
 * \param s socket number
 * \param protocol the protocol for the socket
 * \param port the source port for the socket
 * \param flag the option for the socket
 * \return When succeeded : 1, failed :0
*/
uint8 socketW3150a(SOCKET s, uint8 protocol, uint16 port, uint8 flag)
{
	uint8 ret;
	g_SockSendBusy[s]=0;
	if ((protocol == SOCK_STREAM) || (protocol == SOCK_DGRAM) || (protocol == SOCK_ICMPM) || (protocol == SOCK_IPL_RAWM) || (protocol == SOCK_MACL_RAWM) || (protocol == SOCK_PPPOEM))
	{
		ret=1;
		if (IINCHIP_READ(SOCK_STATUS(s)) != SOCK_CLOSED) closeW3150a(s);
		IINCHIP_WRITE(OPT_PROTOCOL(s),protocol | flag);
		if(port != 0)
		{
			IINCHIP_WRITE(SRC_PORT_PTR(s),(uint8)((port & 0xff00) >> 8));
			IINCHIP_WRITE((SRC_PORT_PTR(s)+1),(uint8)(port & 0x00ff));
		}
		else
		{
			local_port++; // if don't set the source port, set local_port number.
			IINCHIP_WRITE(SRC_PORT_PTR(s),(uint8)((local_port & 0xff00) >> 8));
			IINCHIP_WRITE((SRC_PORT_PTR(s)+1),(uint8)(local_port & 0x00ff));
		}
		IINCHIP_WRITE(COMMAND(s),CSOCKINIT);
	}
	else ret=0;
	return ret;
}

/**
 * \brief close socket
 * This function is to close the socket.
 * \param s socket number
*/
void closeW3150a(SOCKET s)
{ IINCHIP_WRITE(COMMAND(s),CCLOSE); }

/**
 * \brief send udp data packet
 * This function sends UDP/EtherCAT data.
 * The send() function is an application I/F function.
 * It continues to call the send_in() function to complete the sending of the data up to the size of the data to be sent
 * when the application is called. Unlike TCP transmission, The peer's destination address and the port is need.
 * \param s socket number
 * \param buf a pointer to data
 * \param len the data size to send
 * \param addr the peer's destination IP address
 * \param port the peer's destination port number
 * \return Succeed: sent data size, Failed:  0
*/
uint16 sendtoW3150a(SOCKET s, const uint8 *buf, uint16 len, uint8 *addr, uint16 port)
{
  uint8 bMAC=0;
  unsigned int A1;
  static uint8 MACAddr[6];

  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  if((len==0) || (len>getIINCHIP_TxMAX(s))) return(0); // check size not to exceed MAX size or !=0
  if(port==0)
  { // EtherCAT
/*    IINCHIP_WRITE(DST_HA_PTR(s),0xff);
    IINCHIP_WRITE((DST_HA_PTR(s)+1),0xff);
    IINCHIP_WRITE((DST_HA_PTR(s)+2),0xff);
    IINCHIP_WRITE((DST_HA_PTR(s)+3),0xff);
    IINCHIP_WRITE((DST_HA_PTR(s)+4),0xff);
    IINCHIP_WRITE((DST_HA_PTR(s)+5),0xff);

    IINCHIP_WRITE(DST_IP_PTR(s),0xff);
    IINCHIP_WRITE((DST_IP_PTR(s)+1),0xff);
    IINCHIP_WRITE((DST_IP_PTR(s)+2),0xff);
    IINCHIP_WRITE((DST_IP_PTR(s)+3),0xff);

    IINCHIP_WRITE(DST_PORT_PTR(s),0xff);
    IINCHIP_WRITE((DST_PORT_PTR(s)+1),0xff);*/

    bMAC=1;
  }
  else
  { // UDP
    if((addr[0]==0x00) && (addr[1]==0x00) && (addr[2]==0x00) && (addr[3]==0x00)) return 0;
    A1=*((uint32 *)addr);
    if(GetMACAddrFromIp(A1,MACAddr))
    {
      IINCHIP_WRITE(DST_HA_PTR(s),MACAddr[0]);
      IINCHIP_WRITE((DST_HA_PTR(s)+1),MACAddr[1]);
      IINCHIP_WRITE((DST_HA_PTR(s)+2),MACAddr[2]);
      IINCHIP_WRITE((DST_HA_PTR(s)+3),MACAddr[3]);
      IINCHIP_WRITE((DST_HA_PTR(s)+4),MACAddr[4]);
      IINCHIP_WRITE((DST_HA_PTR(s)+5),MACAddr[5]);
      bMAC=1;
    }
    else
    {
      if(issubnet(addr)==0)
      {
        IINCHIP_WRITE(DST_HA_PTR(s),GW_MAC[0]);
        IINCHIP_WRITE((DST_HA_PTR(s)+1),GW_MAC[1]);
        IINCHIP_WRITE((DST_HA_PTR(s)+2),GW_MAC[2]);
        IINCHIP_WRITE((DST_HA_PTR(s)+3),GW_MAC[3]);
        IINCHIP_WRITE((DST_HA_PTR(s)+4),GW_MAC[4]);
        IINCHIP_WRITE((DST_HA_PTR(s)+5),GW_MAC[5]);
        bMAC=1;
      }
      else
      {
        if(is_gw_samenet==0)
        { // 2005.10.25 added fix subnet check error
          IINCHIP_WRITE((GATEWAY_PTR+0),addr[0]);
          IINCHIP_WRITE((GATEWAY_PTR+1),addr[1]);
          IINCHIP_WRITE((GATEWAY_PTR+2),addr[2]);
          IINCHIP_WRITE((GATEWAY_PTR+3),addr[3]);
        }
      }
    }
    IINCHIP_WRITE(DST_IP_PTR(s),addr[0]);
    IINCHIP_WRITE((DST_IP_PTR(s)+1),addr[1]);
    IINCHIP_WRITE((DST_IP_PTR(s)+2),addr[2]);
    IINCHIP_WRITE((DST_IP_PTR(s)+3),addr[3]);
    IINCHIP_WRITE(DST_PORT_PTR(s),(uint8)((port & 0xff00) >> 8));
    IINCHIP_WRITE((DST_PORT_PTR(s)+1),(uint8)(port & 0x00ff));
  }

  // copy data
  send_data_processing(s,(uint8 *)buf,len);

  // for UDP defection
  if(bMAC) IINCHIP_WRITE(COMMAND(s),CSENDMAC);
  else IINCHIP_WRITE(COMMAND(s),CSEND);
  g_SockSendBusy[s]=1;
  g_dwSockTOut[s]=xTaskGetTickCount()+d_SendtoTOut;
  if(!s_bW3150Plus) g_dwTmWait_W3150[s]=xTaskGetTickCount()+20;
  return(len);
}

/**
 * \brief receive udp data packet
 * The function is to receive the UDP or IP layer RAW mode data, and handling the data header.
 * \param s socket number
 * \param buf a pointer to copy the data to be received
 * \param len the size of the data to read
 * \param addr a pointer to store the peer's IP address
 * \param port a pointer to store the peer's port number
 * \return Succeed: received data size, Failed:  -1
*/
uint16 recvfromW3150a(SOCKET s, uint8 *buf, uint16 len, uint8 *addr, uint16 *port)
{
  uint8 isr,head[8];
  uint16 data_len=0;
  uint16 total_len=0;
  uint32 ptr=0;

  if(len>0)
  {
    //while(IsCurrentTickCountLT(s_dwUdpBetween)) vTaskDelay(1);
    ptr=IINCHIP_READ(RX_RD_PTR(s));
    ptr=((ptr & 0x00ff) << 8)+IINCHIP_READ(RX_RD_PTR(s)+1);
    switch(IINCHIP_READ(OPT_PROTOCOL(s)) & 0x07)
    {
      case SOCK_DGRAM:
		read_data(s,ptr,head,0x08);
		ptr+=8;
		// read peer's IP address, port number.
		addr[0]=head[0];
		addr[1]=head[1];
		addr[2]=head[2];
		addr[3]=head[3];
		*port=head[4];
		*port=(*port << 8)+head[5];
		data_len=head[6];
		data_len=(data_len << 8)+head[7];
		if(data_len>1472 )
		{ // invalid size -> correction error
			total_len=IINCHIP_READ(RX_RECV_SIZE_PTR(s));
			total_len=(total_len << 8)+IINCHIP_READ(RX_RECV_SIZE_PTR(s)+1);
			data_len=0;
			ptr+=total_len;
		}
		else
		{
           if(data_len>len) data_len=len;
		   read_data(s, ptr, buf, data_len); // data copy.
		   ptr+=data_len;
		}
        IINCHIP_WRITE(RX_RD_PTR(s),(uint8)((ptr & 0xff00) >> 8));
        IINCHIP_WRITE((RX_RD_PTR(s)+1),(uint8)(ptr & 0x00ff));
		break;
   
      case SOCK_IPL_RAWM:
		read_data(s, ptr, head, 0x06);
		ptr+=6;
		addr[0]=head[0];
		addr[1]=head[1];
		addr[2]=head[2];
		addr[3]=head[3];
		data_len=head[4];
		data_len=(data_len << 8)+head[5];
		read_data(s, ptr, buf, data_len); // data copy
		ptr+=data_len;
        IINCHIP_WRITE(RX_RD_PTR(s),(uint8)((ptr & 0xff00) >> 8));
        IINCHIP_WRITE((RX_RD_PTR(s)+1),(uint8)(ptr & 0x00ff));
		break;

      case SOCK_MACL_RAWM:
		read_data(s, ptr, head, 0x02);
		ptr+=2;
		data_len=head[0];
		data_len=(data_len << 8)+head[1]-2;
		read_data(s, ptr, buf, data_len); // data copy
		ptr+=data_len;
        IINCHIP_WRITE(RX_RD_PTR(s),(uint8)((ptr & 0xff00) >> 8));
        IINCHIP_WRITE((RX_RD_PTR(s)+1),(uint8)(ptr & 0x00ff));
		break;
   
      default :
		break;
    }
    IINCHIP_WRITE(COMMAND(s),CRECV);
	while(IINCHIP_READ(COMMAND(s)))
	{
    	isr=(IINCHIP_READ(INT_STATUS(s)) & ISR_TIMEOUT) || (getISR(s) & ISR_TIMEOUT);
		if((IINCHIP_READ(SOCK_STATUS(s))==SOCK_CLOSED) || isr)
		{
          if(isr) IINCHIP_WRITE(INT_STATUS(s),ISR_TIMEOUT | ISR_RECV);
		  return 0;
		}
	}
  }
  return data_len;
}

