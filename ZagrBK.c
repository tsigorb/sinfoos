#include "..\Sinfo.h"
#include "FunctionsPrototypes.h"
#include "GlobalData.h"

/////////////////////////////////////////////////////////////////////
bool IsProcessRestartMKA(void);

/////////////////////////////////////////////////////////////////////
unsigned char g_btErrCode=0;

/////////////////////////////////////////////////////////////////////
bool SendCmd2UartWithAnswer(sCommand *psCmd,unsigned char btOffAnswer)
{
  unsigned char i,Result=CodeStatusFalse;
  static sInfoKadr sInfo;

  for(i=0;i<3;i++)
  {
     if(xQueueSendCmd(psCmd,0)!=errQUEUE_FULL)
     {
       if(xQueueReceive(xQueueReply[UserLoaderMKA],&sInfo,400))
       {
         LedToggle(ledUpr);
         if(btOffAnswer==5)
         {
           if(psCmd->BufCommand[0]==82) return(true);
           if(sInfo.L==6) // ����� �� ���
           { Result=sInfo.ByteInfoKadr[5]; g_btErrCode=Result; }
else
{
g_btErrCode=0xfd;
/*  DataSave1[14]++; DataSave1[15]=sInfo.L;
  DataSave1[16]=sInfo.ByteInfoKadr[0];
  DataSave1[17]=sInfo.ByteInfoKadr[1];*/
}
         }
         else
         { Result=sInfo.ByteInfoKadr[1]; g_btErrCode=Result; } // ���� Result==0xfc - ��� ������ �� UART
       }
       else
       { CountErrQueueReply++; g_btErrCode=0xff; }
     }
     else
     { CountErrSendQueueCommand++; g_btErrCode=0xfe; }
     if(Result==CodeStatusTrue) return(true);
  }
  return(false);
}

// �������� ��������� ��� �� ����
bool ZagrPgm2MKA(HANDLE hFileZ,unsigned char btNumbMKA)
{
  unsigned int AdrMem,ReadByte,MaxReadByte;
  int CountWord;
  unsigned short FlagBoot;
  unsigned char i,j,btDiv,btMult;
  static sCommand CommandZagr;
  static unsigned char MemMKA[260];

  CommandZagr.Source=UserLoaderMKA;
  CommandZagr.A1=0;
  if(btNumbMKA!=0xff) btNumbMKA<<=2;
  if(ReadFile(hFileZ,MemMKA,2)<2) // ������ Control word
  { CountErrLoadPgmBK++; return(false); }
  do
  {
    if(ReadFile(hFileZ,MemMKA,8)<8) // ������ header (4 ����� - 8 ����)
    { ErrReadF++; return(false); }
    FlagBoot=MemMKA[1];
    FlagBoot=(FlagBoot << 8) | MemMKA[0];
    AdrMem=MemMKA[4];
    AdrMem=(AdrMem << 8) | MemMKA[3];
    AdrMem=(AdrMem << 8) | MemMKA[2];
    CountWord=MemMKA[7];
    CountWord=(CountWord << 8) | MemMKA[6]; // ���������� ����
    CommandZagr.BufCommand[0] = 78;
    CommandZagr.BufCommand[1] = (FlagBoot & 0x01) ^ 0x01;
    CommandZagr.BufCommand[2] = btNumbMKA;
    if(CommandZagr.BufCommand[1]) 
    { // ������ ��������
      btMult=3;
      btDiv=4;
      MaxReadByte=240;
      CommandZagr.BufCommand[1]=1;
    }
    else
    { // ������ ������
      btMult = 2; 
      btDiv = 2;
      MaxReadByte=180;
      CommandZagr.BufCommand[1]=0;
    }
    do
    {
      ReadByte=CountWord*btDiv;
      if(ReadByte>MaxReadByte) ReadByte=MaxReadByte;
      if(ReadByte==0) break;
      memset(MemMKA,0,ReadByte);
      if(FlagBoot<4)
      {
        if(ReadFile(hFileZ,MemMKA,ReadByte)<0)
        { ErrReadF++; return(false); }
        if(CommandZagr.BufCommand[1] && (AdrMem<0x200)) AdrMem+=0x200; // ������� ����������
      }
      CommandZagr.L=ReadByte+6;
      CommandZagr.BufCommand[3] = ReadByte / btDiv * btMult; // ���������� ����
      CommandZagr.BufCommand[4] = (unsigned short)AdrMem >> 8;
      CommandZagr.BufCommand[5] = (unsigned short)AdrMem & 0xFF;
      if(CommandZagr.BufCommand[1])
      { // ������ ��������
        for(i=0,j=6;i<ReadByte;i+=btDiv,j+=btMult)
        {
          CommandZagr.BufCommand[j]=MemMKA[i+3];
          CommandZagr.BufCommand[j+1]=MemMKA[i+2];
          CommandZagr.BufCommand[j+2]=MemMKA[i+1];
        }
      }
      else
      { // ������ ������
        for(i=0;i<ReadByte;i+=btDiv)
        {
          CommandZagr.BufCommand[i+6]=MemMKA[i+1];
          CommandZagr.BufCommand[i+7]=MemMKA[i];
        }
      }
      if(!SendCmd2UartWithAnswer(&CommandZagr,5))
      { CountErrLoadPgmBK++; return(false); }
      i=ReadByte/btDiv;
      CountWord-=i;
      AdrMem+=i;
    }
    while(CountWord>0);
    LedOff(ledUpr);
    if(FlagBoot & 0x2)
    { // Final
      CommandZagr.L=2 ;
      CommandZagr.BufCommand[0]=82;
      CommandZagr.BufCommand[1]=btNumbMKA;
      if(SendCmd2UartWithAnswer(&CommandZagr,5)) return(true);
      CountErrLoadPgmBK++;
      return(false);
    }
    vTaskDelay(10);
  }
  while(1);
}

/////////////////////////////////////////////////////////////////////
// �������� �������� ��
#define NumbCmdMKA       15  // ���������� ������ ��������

static unsigned char s_MasCmdMKA[NumbCmdMKA] = {20,21,22,23,24,25,26,27,28,29,30,33,34,37,38}; // ������ ������

unsigned char IsLoadParamCmdMKA(unsigned char btCmd)
{
  char btIndCmd;

  for(btIndCmd=0;btIndCmd<NumbCmdMKA;btIndCmd++)
  {
    if(btCmd==s_MasCmdMKA[btIndCmd]) return(1);
  }
  return(0);
}

bool LoadParamMKA(unsigned char btNumbMKA)
{
  char btIndCmd,btChan;
  static sCommand sCmdMKA;

  for(btIndCmd=0;btIndCmd<NumbCmdMKA;btIndCmd++)
  {
    for(btChan=0;btChan<NumbChanMKA;btChan++)
    {
      sCmdMKA.Source=UserLoaderMKA;
      sCmdMKA.A1=0;
      sCmdMKA.L=3;
      sCmdMKA.BufCommand[0]=s_MasCmdMKA[btIndCmd];
      sCmdMKA.BufCommand[1]=0;
      sCmdMKA.BufCommand[2]=btNumbMKA*NumbChanMKA+btChan; // N ������ 
      if(!SendCmd2UartWithAnswer(&sCmdMKA,1))
      {
        CountErrLoadCoeffBK++;
SendStr2IpLogger("$ERROR$LoadParamMKA: block #%d, btChan=%d, btIndCmd=%d, error code=0x%x",
                 btNumbMKA,btChan,btIndCmd,g_btErrCode);
        return(false);
      }
    }
  }
SendStr2IpLogger("$INFO$LoadParamMKA: parameters is loaded to block #%d",btNumbMKA);
  return(true);
}

/////////////////////////////////////////////////////////////////////
static unsigned char s_btLoadedMKA=0;
static unsigned short s_wCoefLoadedMKA=0xffff,
                      s_wFullLoadMKA=0;

unsigned char IsLoadedMKA(void)
{
  unsigned char btLoadedMKA;

  vPortEnableSwitchTask(false);
  btLoadedMKA=s_btLoadedMKA;
  vPortEnableSwitchTask(true);
  return(btLoadedMKA);
}

unsigned char IsFullLoadMKA(unsigned short wMKA)
{
  return ((s_wFullLoadMKA & (1 << wMKA))!=0);
}

// �������� ��������� � �������� MKA
void LoadedMKA(void)
{
  //�� ���� �� ������ �������!!
  //char chLoadMKAFromPC=RdFlash(AdrZagrBK); // ������ ��������: 0 - � ����, 1 - � ����������

  unsigned short i;
  HANDLE hFileZ;
  bool bLoadAllMKA=false;

  for(i=0;i<MAX_MKA_BOARD;i++) 
  {
    if(((TestBoard >> (i<<1)) & 0x03)==0x02)
    {
      if(!(s_wCoefLoadedMKA & (1 << i)))
      { // �������� �������� ��
        vPortEnableSwitchTask(false);
        s_btLoadedMKA=1;
        vPortEnableSwitchTask(true);
        while(IsProcessRestartMKA()) vTaskDelay(100);
        LedToggle(ledUpr);
        if(LoadParamMKA(i))
        {
          LedOff(ledUpr);
          s_wCoefLoadedMKA|=(1 << i);
          s_wFullLoadMKA|=(1 << i);
        }
        else
        { // ������ �������� ����������. ��� ������ � ����???
        }
        vPortEnableSwitchTask(false);
        s_btLoadedMKA=0;
        vPortEnableSwitchTask(true);
        vTaskDelay(10);
      }
    }
    else s_wFullLoadMKA&=~(1 << i);
  }
  for(i=0;i<MAX_MKA_BOARD;i++)
  {
    if(((TestBoard >> (i<<1)) & 0x03)==0x01)
    { // �������� ��������� ���
      hFileZ=CreateFile("SinfoBK16b.bin",GENERIC_READ,0);
      if(hFileZ!=INVALID_HANDLE_VALUE)
      {
        vPortEnableSwitchTask(false);
        s_btLoadedMKA=1;
        if(i)
        { s_wFullLoadMKA&=~(1 << i); bLoadAllMKA=false; }
        else
        { s_wFullLoadMKA=0; bLoadAllMKA=true; }
        vPortEnableSwitchTask(true);
SendStr2IpLogger("$INFO$LoadedMKA: load programm to MKA (s_wFullLoadMKA=%x)!",s_wFullLoadMKA);
        while(IsProcessRestartMKA()) vTaskDelay(100);
//SendStr2IpLogger("$DEBUG$LoadedMKA: MKAUARTSTATE=0x%08x",MKAUARTSTATE);
        if(ZagrPgm2MKA(hFileZ,i ? i : 0xff))
        {
          if(i)
          {
SendStr2IpLogger("$INFO$LoadedMKA: programm is loaded to block #%d",i);
            s_wCoefLoadedMKA&=~(1 << i);
          }
          else
          {
            s_wCoefLoadedMKA=0;
SendStr2IpLogger("$INFO$LoadedMKA: programm is loaded to MKA!");
          }
        }
        CloseHandle(hFileZ);
        vPortEnableSwitchTask(false);
        s_btLoadedMKA=0;
        vPortEnableSwitchTask(true);
      }
      else
      {
SendStr2IpLogger("$INFO$LoadedMKA: error open SinfoBK16b.bin!!");
        return;
      }
    }
    if(bLoadAllMKA)
    {
      LedOff(ledUpr);
      return;
    }
  }
}

/////////// ������ ������ � ��� �� UART � �������������� ������ �� ��� �� SPORT � ���� ��.������ �35 /////////////
unsigned int MKAUARTSTATE=0;                // ���� 0..11 == 1 - ������ ������ �� � ��� �� UART
                                            // ���� 16..27 == 1 - ���� ��.����� �35 �� ���
static unsigned int s_dwTmMKACmd200[MAX_MKA_BOARD]={0,0,0,0,0,0,0,0,0,0,0,0};
static unsigned char s_btUartErr[MAX_MKA_BOARD]={0,0,0,0,0,0,0,0,0,0,0,0};

void _xClearMKAUartState(unsigned char btMKA)
{
  MKAUARTSTATE&=~(0x10001 << btMKA);
  s_dwTmMKACmd200[btMKA]=0;
}

void ClearMKAUartState(unsigned char btMKA)
{
  if(btMKA>=MAX_MKA_BOARD) return;
  vPortEnableSwitchTask(false);
DataSave1[1]++;
  _xClearMKAUartState(btMKA);
  s_btUartErr[btMKA]=0;
  vPortEnableSwitchTask(true);
}

unsigned char ClrErrUART4SixMKA(unsigned char btMKA)
{
  unsigned char btMaxMKA;
  if(btMKA<MAX_MKA_BOARD/2)
  { btMKA=0; btMaxMKA=MAX_MKA_BOARD/2; }
  else
  { btMKA=MAX_MKA_BOARD/2; btMaxMKA=MAX_MKA_BOARD; }
  vPortEnableSwitchTask(false);
  for(;btMKA<btMaxMKA;btMKA++) _xClearMKAUartState(btMKA);
  vPortEnableSwitchTask(true);
  return (btMaxMKA-1);
}

unsigned char SetErrUART4SixMKA(unsigned char btMKA)
{
  unsigned char btMaxMKA;

  s_btUartErr[btMKA]++;
  if(s_btUartErr[btMKA]<2)
  {
    s_dwTmMKACmd200[btMKA]=0;
DataSave1[3]++;
    return btMKA;
  }
  g_sErrCntrs.MKA_UART[btMKA]++;
SendStr2IpLogger("$ERROR$SetErrUART4SixMKA: TestBoard=%08x, block #%d\r\ng_sErrCntrs.MKA_UART=%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d",
          TestBoard,btMKA,
          g_sErrCntrs.MKA_UART[0],g_sErrCntrs.MKA_UART[1],g_sErrCntrs.MKA_UART[2],g_sErrCntrs.MKA_UART[3],
          g_sErrCntrs.MKA_UART[4],g_sErrCntrs.MKA_UART[5],g_sErrCntrs.MKA_UART[6],g_sErrCntrs.MKA_UART[7],
          g_sErrCntrs.MKA_UART[8],g_sErrCntrs.MKA_UART[9],g_sErrCntrs.MKA_UART[10],g_sErrCntrs.MKA_UART[11]);
  if(btMKA<MAX_MKA_BOARD/2)
  { btMKA=0; btMaxMKA=MAX_MKA_BOARD/2; }
  else
  { btMKA=MAX_MKA_BOARD/2; btMaxMKA=MAX_MKA_BOARD; }
  vPortEnableSwitchTask(false);
  for(;btMKA<btMaxMKA;btMKA++)
  {
    MKAUARTSTATE|=(0x10001 << btMKA);
    s_dwTmMKACmd200[btMKA]=0;
    s_btUartErr[btMKA]=0;
  }
  vPortEnableSwitchTask(true);
  return (btMaxMKA-1);
}

bool IsProcessRestartMKA(void)
{
  unsigned char btMKA;

  for(btMKA=0;btMKA<MAX_MKA_BOARD;btMKA++)
  {
    if(((TestBoard >> (btMKA<<1)) & 0x03)==2)
    {
      if(MKAUARTSTATE & (0x10001 << btMKA)) return(true);
    }
  }
  return(false);
}

void TestUart4MKA(unsigned char btMKA)
{
  unsigned char CmdMKA[3];

//SendStr2IpLogger("$DEBUG$TestUart4MKA: block #%d",btMKA);
  CmdMKA[0]=200;           // ��� �������
  CmdMKA[1]=3;             // ����� �������
  CmdMKA[2]=btMKA << 2;    // ����� ������
  vPortEnableSwitchTask(false);
  _xClearMKAUartState(btMKA);
  xUARTPutCmd(CmdMKA);
  s_dwTmMKACmd200[btMKA]=xTaskGetTickCount()+1000;
DataSave1[0]++;
  vPortEnableSwitchTask(true);
}

void PeriodicalSendUartMKACmd200(void)
{
  unsigned char btMKA;
  static unsigned char s_btMKA=0;
  static unsigned int s_dwWait=0;

  for(btMKA=0;btMKA<MAX_MKA_BOARD;btMKA++)
  {
    if(MKAUARTSTATE & (0x10000 << btMKA))
    { // ��� ������ �� ������� 200 �� UART 
      if(!(MKAUARTSTATE & (0x1 << btMKA)))
      { // ����� ��� ��������������������
        btMKA=ClrErrUART4SixMKA(btMKA);
        s_dwWait=xTaskGetTickCount()+5000;
      }
    }
    else
    { 
      if(s_dwTmMKACmd200[btMKA] &&
         IsCurrentTickCountGT(s_dwTmMKACmd200[btMKA]))
      { // ��� ������ �� ������� 200 �� UART
        btMKA=SetErrUART4SixMKA(btMKA);
      }
    }
  }
  if(!s_dwWait) s_dwWait=xTaskGetTickCount()+50;
  if(IsCurrentTickCountGT(s_dwWait))
  {
    if(((TestBoard >> (s_btMKA<<1)) & 0x03)==2)
    { // ����� ��� ��������
      if(!(MKAUARTSTATE & (0x10000 << s_btMKA)))
      {
        if(!s_dwTmMKACmd200[s_btMKA])
        {
          if(IsFullLoadMKA(s_btMKA))
          { // ����� �������� ������� 200 �� UART
            TestUart4MKA(s_btMKA);
            s_dwWait=xTaskGetTickCount()+200;
          }
          else
          { s_dwWait=xTaskGetTickCount()+20000; return; }
        }
      }
    }
    s_btMKA++;
    if(s_btMKA>=MAX_MKA_BOARD) s_btMKA=0;
  }
}

/////////////////////////////////////////////////////////////////////
void vCheckAndMKATask(void *pvParameters)
{
  portTickType xTimeToWake=0;

  xTimeToWake=xTaskGetTickCount()+10000;
  while(1)
  {
    if(IsCurrentTickCountGT(xTimeToWake))
    {
/*      extern int g_iHeapIndex;
      static bool SmallUnused=false;

      if(SmallUnused)
      {
        if(_heap_space_unused(g_iHeapIndex)>0x500)
        {
          SmallUnused=false;
          AddToLog(lt_Notification,ls_Task,lc_MemoryManager,7);  // ������ � ��������� ������
        }
      }
      else
      {
        if(_heap_space_unused(g_iHeapIndex)<0x300)
        {
          SmallUnused=true;
          AddToLog(lt_Error,ls_Task,lc_MemoryManager,6);  // ������ � ��������� ������
        }
      }*/

      if(TestBoard!=0xFFFFFFFF) LoadedMKA();
      xTimeToWake=xTaskGetTickCount()+2000;
    }
    SetWDState(d_WDSTATE_ChecTask);
    vTaskDelay(100);
  }
}

