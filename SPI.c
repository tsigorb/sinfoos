#include "..\Sinfo.h"

#define SPI_Err             (MODF | TXE | RBSY | TXCOL)

// !!������ �������� ��� �������� �������, �� �/�� AT45DB161 �������� � ������ 528 ���� �� ��������!!
#define d_FlashPageSize     512 

// ������������� ������� Flash-������ 45DB161D �� ����������� � ��������

//   0x000000 - 0x1FFFFF ������ ���������� (SF1-PF2) (��� �� ��������� � �������� ���������)
//   0x000000 - 0x00FFFF ��������� ���������
//   0x010000 - 0x0FFFFF �������� ��������� ������
//   0x100000 - 0x1FFFFF �������� �������

//   0x200000 - 0x3FFFFF ������ ���������� PF5

#define Fl_PF5      0xDF34
#define Fl_PF4      0xEF34
#define Fl_PF2      0xFB34

unsigned char g_btSockOffset=0;

short Err_Flash=0;

static bool s_bEnWr2PgmFlash=false;

xSemaphoreHandle g_SPILock=NULL;

//--------------------------------------------------------------------------//
void CS4Down(void)
{
  int i;
  *pSPI_FLG=Fl_PF4;
  ssync();
  for(i=0;i<25;i++); // �������� ~80 ��
}

void CS4Up(void)
{
  *pSPI_FLG=0xFF34;
  ssync();
}

void CSDown(unsigned int Adr)
{ // ��������� �������
  int i;
  switch(Adr >> 20)
  {
    case 0: case 1:
      *pSPI_FLG=Fl_PF2;
      break;
    case 2: case 3:
      *pSPI_FLG=Fl_PF5;
      break;
  }
  ssync();
  for(i=0;i<25;i++); // �������� ~80 ��
}

void CSUp(void)
{
  *pSPI_FLG=0xFF34;
  ssync();
}

void EnableWrite2ProgrammArea(bool bEn)
{ s_bEnWr2PgmFlash=bEn; }

bool IsEnableWrite2ProgrammArea(void)
{ return(s_bEnWr2PgmFlash); }

/*
CPOL=0 � ������ ������������� ���������� � ������� ������;
CPOL=1 � ������ ������������� ���������� � �������� ������;
CPHA=0 � ������� ������ ������������ �� ��������� ������ ������� �������������;
CPHA=1 � ������� ������ ������������ �� ������� ������ ������� �������������.

��� ����������� ������� ������ ���������� SPI ������� ��������� ����������:
����� 0 (CPOL=0, CPHA=0);
����� 1 (CPOL=0, CPHA=1);
����� 2 (CPOL=1, CPHA=0);
����� 3 (CPOL=1, CPHA=1).
*/

void InitSPI(void)
{
  while(!g_SPILock) vSemaphoreCreateBinary(g_SPILock);
  *pFIO_DIR |= (PF2 | PF4 | PF5); // ��������� �� �����
  *pFIO_FLAG_S=(PF2 | PF4 | PF5); // ������� �������
  *pSPI_BAUD=3; // SPI SCK=SCLK/(2*SPIBAUD0)=49152/6 ~ 8000 kHz
  *pSPI_CTL=0x5C05; // ��������� ������������ � ��������� SPI
  ssync();
}

void SPI_SpeedNormal(void)
{
  *pSPI_BAUD=3; // SPI SCK=SCLK/(2*SPIBAUD0)=49152/6 ~ 8000 kHz
  ssync();
}

void SPI_SpeedSlow(void)
{ // ��� ������ � sc16is740 (�������� � ������ 3!!!???)
  *pSPI_BAUD=7; // SPI SCK=SCLK/(2*SPIBAUD0)=49152/14 ~ 3511 kHz
  ssync();
}

bool SPIO_WrRd(unsigned short Instr,unsigned short *DanFlash)
{ // ��������/����� ����� �� SPI
  int i;

  *pSPI_STAT=SPI_Err; // ����� ������
  ssync();
  *pSPI_TDBR=Instr;
  ssync();
  for(i=0;i<100;i++)
  {
    if(*pSPI_STAT & RXS)
    {
      *DanFlash=*pSPI_RDBR;
      ssync();
      return(true);
    }
    ssync();
  }
  *pSPI_STAT=SPI_Err; // ����� ������
  ssync();
  return false; // ���������� SPI ���
}

bool SPIO_Wr(unsigned short Instr)
{ // �������� ����� �� SPI
  unsigned short DanFlash;

  if(SPIO_WrRd(Instr,&DanFlash)) return true;
  return false;
}

///// ������ � �/�� AT25256 /////////////////////////////////////////
bool _xAT25_ReadyWait(void)
{
  signed char ReadySPI=0;
  portTickType xTimeToWake;
  short i;
  unsigned short DanFlash;

  xTimeToWake=xTaskGetTickCount()+200; // �������� ���������� �� 200 ��
  while(IsCurrentTickCountLT(xTimeToWake) && (ReadySPI==0))
  {
    CS4Down(); // ��������� PF4
    if(!(SPIO_Wr(5))) ReadySPI=-1; // ������
    else
    { // �������� �������� � ������ �������� ������� ����
      if(!SPIO_WrRd(0,&DanFlash)) ReadySPI=-1; // ������ ������
      else
      {
        if(!(DanFlash & 1)) ReadySPI=1;
      }
    }
    CS4Up(); // ���������� PF4
    for(i=0;i<25;i++); // �������� ~80 ��
  }
  if(ReadySPI>0)
  { // ���������� ������
    if(DanFlash & 0x0c)
    { // clear WRITE PROTECT AREA
//SendStr2IpLogger("$DEBUG$AT25256 RDSR: DanFlash=%02x",DanFlash);
      CS4Down(); // ��������� PF4
      SPIO_Wr(0x01);
      SPIO_Wr(0x00);
      CS4Up(); // ���������� PF4
    }
    return true;
  }
  else return false ; // ���������� �� ������
}

unsigned short _xRdFlash(unsigned int AdrFl)
{ // AT25256
  short i;
  unsigned short  DanFlash;

  DanFlash=0;
  Err_Flash=1;
  if(_xAT25_ReadyWait())
  {
    CS4Down(); // ��������� PF4
    if(SPIO_Wr(3)) // ������� "������ ������ �� ������"?
    {
      *pSPI_CTL |= 0x100;  // ����� ����� 16 ���
      ssync();
      if(SPIO_Wr(AdrFl))
      { //  ����������� �������� ������ - �������� �������� � ������ ������ 
        if(SPIO_WrRd(0,&DanFlash)) Err_Flash=0;
      }
    }
    CS4Up(); // ���������� PF4
    *pSPI_CTL&=~0x100;  // ����� ����� 8 ���
    ssync();
    for(i=0;i<25;i++); // �������� ~80 ��
  }
  return DanFlash; // ��������� ������
}

bool _xAT25_WriteEnable(void)
{
  short i;
  bool ValRet=false;

  if(_xAT25_ReadyWait())
  {
    CS4Down(); // ��������� PF4
    ValRet=SPIO_Wr(6); // "���������� ������ �� ����"
    CS4Up(); // ���������� PF4
    for(i=0;i<25;i++); // �������� ~80 ��
  }
  return ValRet; 
}

bool _xWrFlash(unsigned int AdrFl,unsigned short DanFl)
{ // AT25256
  bool ValRet=false;
  short i,Cycle;

  for(Cycle=0;Cycle<2;Cycle++)
  { // ������� ������ � ��25256 ��-�� ������������ ������ SPI !!
    if(_xAT25_WriteEnable())
    { // ������ �� ���� ���������
      CS4Down(); // ��������� PF4
      ValRet=SPIO_Wr(2); // "������ ������ � ������"
      if(ValRet) 
      {
        *pSPI_CTL |= 0x100; // ����� ����� 16 ���
        ssync();
        ValRet=SPIO_Wr(AdrFl); // �������� ������
        if(ValRet) ValRet=SPIO_Wr(DanFl); // ������  ������ 
      }
      CS4Up(); // ���������� PF4
      *pSPI_CTL&=~0x100;  // ����� ����� 8 ���
      ssync();
      for(i=0;i<25;i++); // �������� ~80 ��
      if(ValRet) ValRet=_xAT25_ReadyWait(); // ������ ��������
    }
    if(ValRet)
    {
      unsigned short wRet;
      wRet=_xRdFlash(AdrFl);
      if(wRet==DanFl) return true;
      ValRet=false;
    }
  }
  return ValRet;
}

///// ������ � �/�� AT45DB161 ///////////////////////////////////////
// ����������� ����� ������ � ������ Flash (����� ��������, ����� ����� � ��������)
// �������: ����� � ������� Flash
// ���������: unsigned int Addr - ����� ������
unsigned int AdrToPageAT45DB161(unsigned int Addr)
{
  if(d_FlashPageSize==512)
  { return(((Addr << 1) & 0xFFFC00) + (Addr & 0x1ff)); }
  else
  { return ((Addr / 528)<<10)+(Addr%528); }
}

unsigned short _xGetStatusAT45DB161(unsigned int Addr)
{
  short i;
  unsigned short  DanFlash;

  DanFlash=0;
  CSDown(Addr); // ��������� �������
  if(SPIO_Wr(0xD7)) // ������� ������ �������� ���������
  { // ��, �������� �������� � ������ �������� ������� ����
    if(SPIO_WrRd(0,&DanFlash)) ;
  }
  CSUp(); // ���������� �������
  for (i=0; i < 25; i++); // �������� ~80 ��
  return DanFlash;
}

bool ResetFlash(void)
{
  return true;
}

// �������� ��������� Flash ������ ��� ���������� ������� ������\��������
// �������: TRUE - ���� �������� ��������� �������; FALSE - ���� �������� ��������� � �������
// ���������: unsigned short *lpAddr - ����� ������, ����������� �� ������� 2 ����
//			unsigned short Value - ������������ ������
bool _xCheckState(unsigned int Addr, unsigned short Value)
{
  portTickType xTimeToWake;

  xTimeToWake=xTaskGetTickCount()+200; // �������� �� 20 ��
  while(IsCurrentTickCountLT(xTimeToWake))
  {
    if((_xGetStatusAT45DB161(Addr) & 0x80)!=0) return true;
  }
  return false;
}

// ������� �������� ������� Flash ������.
// �������: TRUE - ���� �����, FALSE -���� ������
// ���������: unsigned int Addr - ����� ������, ����������� �� ������� �������� 528 (512) ����
//			 int How - ����� ��������� ������ � ������ ������������� �� ������� 528 (512) ����
bool WrDeviceErase(unsigned int Addr0, unsigned int How)
{
//  bool bRet;
  unsigned int Address,PageNum;
 
  for(Address=Addr0;Address<(Addr0 + How);Address+=d_FlashPageSize)
  {
    PageNum=AdrToPageAT45DB161(Address);
    xSemaphoreTake(g_SPILock);
    CSDown(Address); // ��������� �������
    SPIO_Wr(0x81); // ������� �������� ��������
    SPIO_Wr(PageNum>>16);
    SPIO_Wr(PageNum>>8);
    SPIO_Wr(PageNum);
    CSUp(); // ���������� �������
    if(!_xCheckState(Addr0,0))
    {
      xSemaphoreGive(g_SPILock);
      return(false);
    }
    xSemaphoreGive(g_SPILock);
  }
  return true;
/*  xSemaphoreTake(g_SPILock);
  bRet=_xCheckState(Addr0,0);
  xSemaphoreGive(g_SPILock);
  return bRet;*/
}

//////////////////////////////////////////////////////////////////////////
// ������ ������� ������ �� Flash.
// �������: TRUE - ���� �����, FALSE -���� ������
// ���������: unsigned int Addr - ����� ����-������, ����������� �� ������� 2 ����
//			unsigned short *lpInp - ��������� �� ������ ������� ������
//			int HowInp - ���������� ������������ ����
//////////////////////////////////////////////////////////////////////////
bool _xWrDeviceArray(unsigned int Addr0, unsigned short *lpInp, unsigned int HowInp)
{
  unsigned int Data;
  unsigned int Address,PageNum;
  unsigned int i,j,j_Start,j_End,
                 k,k_Start,k_End;

  if(Addr0 & 0x01) return(false);
  Address=Addr0 & 0x1FFFFF; // �������� ������� ����� ������, ������ ������ ����������
  if(!IsEnableWrite2ProgrammArea())
  {
    if(!(Addr0 & 0xF00000) && (Address<0x0FFFFF))
    {
      if((Address<0x10000) || (Address+HowInp>0x10004)) return(false);
    }
  }
  *pSPI_BAUD=2; // SPI SCK=SCLK/(2*SPIBAUD0)=49152/4 ~ 12288 kHz
  j_Start=Address/d_FlashPageSize;
  j_End=(Address+HowInp*2)/d_FlashPageSize;
  for(j=j_Start;j<=j_End;j++)
  { // �������� �� ���������� �������
    k_Start=(j==j_Start)?(Address%d_FlashPageSize):0;
    k_End=(j==j_End)?((Address+HowInp*2)-j_End*d_FlashPageSize):d_FlashPageSize;
    if(((j==j_Start) && (k_Start!=0)) ||
       ((j==j_End) && (k_End!=d_FlashPageSize)))
    { // ��������� ������ � ��������� �������� ���� � �����
      CSDown(Addr0); // ��������� �������
      SPIO_Wr(0x53); // ������� Memory Page to Buffer 1 Transfer
      SPIO_Wr((j<<10)>>16);
      SPIO_Wr((j<<10)>>8);
      SPIO_Wr(0);
      CSUp(); // ���������� �������
      if(!_xCheckState(Addr0,0)) break;
    }
	for(k=k_Start;k<k_End;k+=2)
	{ // �������� ������ ��������
		CSDown(Addr0); // ��������� �������
		SPIO_Wr(0x84); // ������� Write to Buffer 1
		SPIO_Wr(0);
		SPIO_Wr(k>>8);
		SPIO_Wr(k);
		Data=*lpInp++;
		SPIO_Wr(Data);
		SPIO_Wr(Data >> 8);
		CSUp(); // ���������� �������
		for (i=0; i < 25; i++); // �������� ~80 ��
	}
	CSDown(Addr0); // ��������� �������
	SPIO_Wr(0x83); // ������� Buffer1 To Memory Page without build-in erase
	SPIO_Wr((j<<10)>>16);
	SPIO_Wr((j<<10)>>8);
	SPIO_Wr(0);
	CSUp(); // ���������� �������
	if(!_xCheckState(Addr0,0)) break;
    Addr0+=d_FlashPageSize;
  }
  *pSPI_BAUD=3; // SPI SCK=SCLK/(2*SPIBAUD0)=49152/6 ~ 8000 kHz
  if(j>j_End) return true;
  return(false);
}

//////////////////////////////////////////////////////////////////////////
// ������ ����� �� Flash ������
// �������: TRUE - ���� �����, FALSE -���� ������
// ���������: unsigned int Addr - ����� ������, ����������� �� ������� 2 ����
//			unsigned short bInp - �������(������������) �����
//////////////////////////////////////////////////////////////////////////
bool _xWrDeviceShort(unsigned int Addr, unsigned short bInp)
{ return _xWrDeviceArray(Addr, &bInp, 1); }

/////////////////////////////////////////////////////////////////////////////////
// ������ *lpHowOut ���� �� Flash �� ���������� ������
// �������: ������ TRUE
// ���������: unsigned short *lpInpAddr - ����� ������. ������ ���� �������� �� ������� 2 ����
//			unsigned short *lpOut - ����� ��������� ������(�� ����� 2 ����)
// 			int *lpHowOut ���������� ���� ��� ������
/////////////////////////////////////////////////////////////////////////////////
bool _xRdFlashP(const unsigned short *lpInpAddr, unsigned short *lpOut, unsigned int *lpHowOut)
{
  unsigned int Index,Address,AddrCur;
  unsigned int PageNum,LastPage,FirstPage;
  unsigned int i,j,k;
  unsigned short DanFlash,Data;

  Address=(unsigned int)lpInpAddr & 0x1FFFFF; // �������� ������� ����� ������, ������ ������ ����������
  Index=0;
  LastPage=(Address+*(lpHowOut)*2)/d_FlashPageSize;
  FirstPage=Address/d_FlashPageSize;
  AddrCur=(unsigned int)lpInpAddr;
  *pSPI_BAUD=2; // SPI SCK=SCLK/(2*SPIBAUD0)=49152/4 ~ 12288 kHz
  for(j=FirstPage;j<=LastPage;j++)
  { // �������� �� ���������� �������
    PageNum=AdrToPageAT45DB161(j*d_FlashPageSize) + ((j==FirstPage)?(Address%d_FlashPageSize):0);
    CSDown(AddrCur); // ��������� �������
    SPIO_Wr(0x03); // ������� Continuous Array Read Low Frequency
    SPIO_Wr(PageNum>>16);
    SPIO_Wr(PageNum>>8);
    SPIO_Wr(PageNum);
    for(k=((j==FirstPage)?(Address%d_FlashPageSize):0);
        k<((j==LastPage)?((Address+ *(lpHowOut)*2)-LastPage*d_FlashPageSize):d_FlashPageSize);
        k+=2)
    { // �������� ������ ��������
      SPIO_WrRd(0,&DanFlash);
      Data=DanFlash;
      SPIO_WrRd(0,&DanFlash);
      Data=Data | (DanFlash << 8);
      lpOut[Index++]=Data;
    }
    CSUp(); // ���������� �������
    for(i=0;i<25;i++); // �������� ~80 ��
    AddrCur+=d_FlashPageSize;
  }
  *pSPI_BAUD=3; // SPI SCK=SCLK/(2*SPIBAUD0)=49152/4 ~ 8000 kHz
  *lpHowOut=Index;
  return true;
}

///////////////////////////////////////////////////////////////////////
// ��������� �����. ���������� ������ �������� � ������� �� Flash ������
// �������: TRUE - ���� ��������� ������; FALSE - ���� �� �����
// ���������: const unsigned short *lpInp - ��������� �� ������ � ������
//			const unsigned short *lpFlashInp - ����� ������ �� Flash ������
//			int Len - ����� ��������� 
///////////////////////////////////////////////////////////////////////
bool _xWithFlashMemCmp(const unsigned short *lpInp, const unsigned short *lpFlashInp,unsigned int Len)
{
  unsigned short wOut;
  unsigned int How, Index, bHowOut;

  bHowOut=1;
  if(Len & 0x1) How=Len/2 + 1;
  else How=Len/2;
  for(Index=0; Index < How; Index++)
  {
    _xRdFlashP((unsigned short*)(((unsigned int)(lpFlashInp)+Index*2)+FLASHADDRESS), &wOut, &bHowOut);
    if(((lpInp[Index] ^ wOut) & 0x00ff) != 0) return false;
    if((((lpInp[Index] ^ wOut) & 0xff00) != 0) && (Len < (Index *2))) return false;
  }
  return true;
}

// ���������� ������� ��� ������ � SPI-���� /////////////////////////
unsigned short RdFlash(unsigned short AdrFl)
{ // AT25256
  unsigned short wRet;

  xSemaphoreTake(g_SPILock);
//  vPortEnableSwitchTask(false);
  wRet=_xRdFlash(AdrFl);
//  vPortEnableSwitchTask(true);
  xSemaphoreGive(g_SPILock);
  return wRet;
}

bool WrFlash(unsigned short DanFl,unsigned short AdrFl)
{ // AT25256
  bool bRet;

  xSemaphoreTake(g_SPILock);
//  vPortEnableSwitchTask(false);
  bRet=_xWrFlash(DanFl,AdrFl);
//  vPortEnableSwitchTask(true);
  xSemaphoreGive(g_SPILock);
  return bRet;
}

bool RdFlashP(const unsigned short *lpInpAddr, unsigned short *lpOut,unsigned int *lpHowOut)
{ // AT45B161
  bool bRet;

  xSemaphoreTake(g_SPILock);
//  vPortEnableSwitchTask(false);
  bRet=_xRdFlashP(lpInpAddr,lpOut,lpHowOut);
//  vPortEnableSwitchTask(true);
  xSemaphoreGive(g_SPILock);
  return bRet;
}

bool WrDeviceArray(unsigned int Addr0, unsigned short *lpInp,unsigned int HowInp)
{ // AT45B161
  bool bRet;

  xSemaphoreTake(g_SPILock);
//  vPortEnableSwitchTask(false);
  bRet=_xWrDeviceArray(Addr0,lpInp,HowInp);
//  vPortEnableSwitchTask(true);
  xSemaphoreGive(g_SPILock);
  return bRet;
}

bool WithFlashMemCmp(const unsigned short *lpInp, const unsigned short *lpFlashInp,unsigned int Len)
{ // AT45B161
  bool bRet;

  xSemaphoreTake(g_SPILock);
//  vPortEnableSwitchTask(false);
  bRet=_xWithFlashMemCmp(lpInp,lpFlashInp,Len);
//  vPortEnableSwitchTask(true);
  xSemaphoreGive(g_SPILock);
  return bRet;
}

/*bool WrDeviceShort(unsigned int Addr, unsigned short bInp)
{
  bool bRet;

  xSemaphoreTake(g_SPILock);
  bRet=_xWrDeviceShort(Addr,bInp);
  xSemaphoreGive(g_SPILock);
  return bRet;
}*/


