#ifdef __ISDN__

#include "../sinfo.h"

#include "pri_config.h"
#include "pri_helpers.h"
#include "pri_debug.h"
#include "pri_internal.h"

/////////////////////////////////////////////////////////////////////
#define d_CMD_pri_call_setup    1
#define d_CMD_pri_call_alert    2
#define d_CMD_pri_call_answer   3
#define d_CMD_pri_call_busy     4
#define d_CMD_pri_call_hangup   5
#define d_CMD_pri_call_info     6

/////////////////////////////////////////////////////////////////////
extern unsigned short DataSave1[];  // ��� �������!!!

extern xQueueHandle xQueueCmd2Misul;
extern xQueueHandle xQueueAnswMisul;

extern int PRI_IS_WORKING;

/////////////////////////////////////////////////////////////////////
int pri_find_empty_chan(struct pri_trunk *pri);
int pri_find_principle(struct pri_trunk *pri, int channel);

/////////////////////////////////////////////////////////////////////
void SendAnswMisul(sCommand *psCmd,int iAnsw)
{
  psCmd->A1=iAnsw;
  xQueueSend(xQueueAnswMisul,psCmd,0);
}

/**
 *  ������ �� PRI
 *
 * \param src   ��� �������� ��� ����� ���������� ������ 
 * \param �     ����� ������ E1 (0-3)
 * \param dest  ��������� ����� � ������� type_numberb;
 * \param s     numcomplete (nomer B ���� ==1/==0 ��� �� ���� ����� ����� �����)
 * \param cid   ����� ��������� � ������� type_numbera
 * \param cname ��� ��������� 
 *
 * \return      ����� ������|� ������<<8
 *
 * \note ���� ���� ������������� �� ����� �������� ��� ������������� � �� ����� ������� redirect_reason, cid_rdnis
 */
int pri_call_setup(int src,int trunkno,char *dest,int s,char *cid,char *cname)
{
  static sCommand sCmd;

  sCmd.BufCommand[0]=d_CMD_pri_call_setup;
  SaveInt2Buf(sCmd.BufCommand+1,src);
  SaveInt2Buf(sCmd.BufCommand+5,trunkno);
  SaveInt2Buf(sCmd.BufCommand+9,(int)dest);
  SaveInt2Buf(sCmd.BufCommand+13,s);
  SaveInt2Buf(sCmd.BufCommand+17,(int)cid);
  SaveInt2Buf(sCmd.BufCommand+21,(int)cname);
  if(xQueueSend(xQueueCmd2Misul,&sCmd,0)==pdPASS)
  {
    if(xQueueReceive(xQueueAnswMisul,&sCmd,100)==pdPASS)
    {
      return(sCmd.A1);
    }
  }
  return(-1);
}

//int pri_call_setup(int src,int trunkno,char *dest,int s,char *cid,char *cname)
void _xpri_call_setup(sCommand *psCmd)
{
#ifdef SUPPORT_USERUSER
  const char *useruser;
#endif
  struct pri_trunk *ptrunk;
  struct pri_bchan_pvt *p;
  struct pri_sr *sr;
  int exclusive;
  int channel;
  
  int src,trunkno,s;
  char *dest,*cid,*cname;

  RestoreIntFromBuf(psCmd->BufCommand+1,&src);
  RestoreIntFromBuf(psCmd->BufCommand+5,&trunkno);
  RestoreIntFromBuf(psCmd->BufCommand+9,(int *)&dest);
  RestoreIntFromBuf(psCmd->BufCommand+13,&s);
  RestoreIntFromBuf(psCmd->BufCommand+17,(int *)&cid);
  RestoreIntFromBuf(psCmd->BufCommand+21,(int *)&cname);

if(!PRI_IS_WORKING)
{
  SendAnswMisul(psCmd,-1);
  return;
}
if (trunkno>NUM_TRUNKS)
{
  for (trunkno=0;trunkno < NUM_TRUNKS;trunkno++)
  {
    ptrunk = &pri_trunks[trunkno];
    PORT_MUTEX_LOCK(&ptrunk->lock);
    channel = pri_find_empty_chan(ptrunk);

	if (channel >=0) break;
	
   PORT_MUTEX_UNLOCK(&ptrunk->lock);
	}
	if (channel <0) {
       if (ptrunk->debug)
        pri_message(ptrunk->pri,"ERROR: No free channelX %d", trunkno);
	    PORT_MUTEX_UNLOCK(&ptrunk->lock);
	    SendAnswMisul(psCmd,-1); //��� ��������� �������
	    return;
	}
}
else
{
	ptrunk = pri_trunks+trunkno;
	PORT_MUTEX_LOCK(&ptrunk->lock);
	channel = pri_find_empty_chan(ptrunk);
	if (channel <0) {
       if (ptrunk->debug)
        pri_message(ptrunk->pri,"ERROR: No free channel %d", trunkno);
//***********
//	for (trunkno=0;trunkno < NUM_TRUNKS;trunkno++)
//	{
//   ptrunk = pri_trunks + trunkno;
//    for (channel=0;channel<MAX_CHANNELS;channel++)    {
//        pri_message(ptrunk->pri,"ERROR: owner channel %d", ptrunk->pvts[channel]->owner);
//    }
//	}
//***********/
    	PORT_MUTEX_UNLOCK(&ptrunk->lock);
    	SendAnswMisul(psCmd,-1); //��� ��������� �������
    	return;
	}
}

        p=ptrunk->pvts[channel];
		//PORT_MUTEX_LOCK(&p->lock);
    	p->alreadyhungup = 0;

		if (!(p->call = pri_new_call(ptrunk->pri))) {
             if (ptrunk->debug)
                    pri_message(ptrunk->pri,"Unable to create call on channel %d", p->channel);
    		//PORT_MUTEX_UNLOCK(&p->lock);
			PORT_MUTEX_UNLOCK(&ptrunk->lock);
          SendAnswMisul(psCmd,-1); //��� ��������� �������
          return;
        }

        if (!(sr = pri_sr_new())) 
        {
            if (ptrunk->debug)
                 pri_message(ptrunk->pri, "Failed to allocate setup request channel %d", p->channel);
            pri_destroycall(ptrunk->pri, p->call);
            p->call = NULL;
            //PORT_MUTEX_UNLOCK(&p->lock);
			PORT_MUTEX_UNLOCK(&ptrunk->lock);
          SendAnswMisul(psCmd,-1); //��� ��������� �������
          return;
        }

        p->dialdest_overlap[0]  = '\0';
		p->dialdest[0]  = '\0';
		p->dialing      = 1;
		p->proceeding      = 0;
		p->alerting      = 0;
		p->progress		=0;
        p->outgoing     = 1;
        p->owner        = src;  //����� �����
		p->sending_complite=s;	//sending complete

        /* Should the picked channel be used exclusively? */
        if (p->priexclusive || ptrunk->nodetype == PRI_NETWORK) 
        {
            exclusive = 1;
        } 
        else 
        {
            exclusive = 0;
        }
        pri_sr_set_channel(sr, PVT_TO_CHANNEL(p), exclusive, 1);
        pri_sr_set_bearer(sr, PRI_TRANS_CAP_SPEECH, PRI_LAYER_1_ALAW); 
        if (ptrunk->facilityenable)
            pri_facility_enable(ptrunk->pri);
		pri_copy_string(p->dialdest, dest, sizeof(p->dialdest));
		pri_copy_string(p->cid_name, cname,sizeof(p->cid_name));
		pri_copy_string(p->cid_num, cid,sizeof(p->cid_num));
		pri_sr_set_called(sr, dest, ptrunk->dialplan, s);
        pri_sr_set_caller(sr, cid, cname, ptrunk->localdialplan,p->callprogress);


#ifdef SUPPORT_USERUSER
        /* User-user info */
        useruser = pri_bchan_pvt->(p->owner, "USERUSERINFO");

        if (useruser)
            pri_sr_set_useruser(sr, useruser);
#endif
       if (ptrunk->debug)
            pri_message(ptrunk->pri, "Send Setup %d", p->channel);

        if (pri_setup(ptrunk->pri, p->call, sr)) {
            pri_sr_free(sr);
			//PORT_MUTEX_UNLOCK(&p->lock);
			PORT_MUTEX_UNLOCK(&ptrunk->lock);
          SendAnswMisul(psCmd,-1); //��� ��������� �������
          return;
        }
        pri_sr_free(sr);
		//PORT_MUTEX_UNLOCK(&p->lock);
        PORT_MUTEX_UNLOCK(&ptrunk->lock);
  SendAnswMisul(psCmd,PVT_TO_CHANNEL(p) | (trunkno<<8));
  return;
}


/**
 * ���������� ������� �������� � ��� ������� �����
 *
 * \param trunkno   ����� ������ E1 (0-3)
 * \param channel   ����� ������ � ������ E1 (0-3) (�� ���� ��� setup)
 * \param owner     ��� �������, ��� ����� ���������� ������
 *
 * \return 
 */
int pri_call_alert(int trunkno, int channel, int owner)
{
  static sCommand sCmd;

  sCmd.BufCommand[0]=d_CMD_pri_call_alert;
  SaveInt2Buf(sCmd.BufCommand+1,trunkno);
  SaveInt2Buf(sCmd.BufCommand+5,channel);
  SaveInt2Buf(sCmd.BufCommand+9,owner);
  if(xQueueSend(xQueueCmd2Misul,&sCmd,0))
  {
    if(xQueueReceive(xQueueAnswMisul,&sCmd,100)) return(sCmd.A1);
  }
  return(-1);
}

void _xpri_call_alert(sCommand *psCmd)
{
    int	chanpos;
    struct pri_trunk *pri;
    struct pri_bchan_pvt *p;
    int trunkno,channel,owner;

    if(!PRI_IS_WORKING)
    { SendAnswMisul(psCmd,-1); return; }
    
    RestoreIntFromBuf(psCmd->BufCommand+1,&trunkno);
    RestoreIntFromBuf(psCmd->BufCommand+5,&channel);
    RestoreIntFromBuf(psCmd->BufCommand+9,&owner);
    pri     = pri_trunks + trunkno;
    chanpos = pri_find_principle(pri, channel);
    if (chanpos < 0)
    { SendAnswMisul(psCmd,-1); return; }
    p = pri->pvts[chanpos];
    if(!p->call)
    { SendAnswMisul(psCmd,-1); return; }
    PORT_MUTEX_LOCK(&pri->lock);
    if(!p->owner) p->owner = owner;
    if ((!p->alerting) && (!p->outgoing )) 
    {
      pri_acknowledge(pri->pri,p->call, PVT_TO_CHANNEL(p), 1);
    }
    p->alerting = 1;
    PORT_MUTEX_UNLOCK(&pri->lock);
    SendAnswMisul(psCmd,0);
}

/**
 * ���������� ������� �������
 *
 * \param trunkno   ����� ������ E1 (0-3)
 * \param channel   ����� ������ � ������ E1 (0-3) (�� ���� ��� setup)
 * \param owner     ��� �������, ��� ����� ���������� ������
 *
 * \return 
 */
int pri_call_answer(int trunkno, int channel, int owner)
{
  static sCommand sCmd;

  sCmd.BufCommand[0]=d_CMD_pri_call_answer;
  SaveInt2Buf(sCmd.BufCommand+1,trunkno);
  SaveInt2Buf(sCmd.BufCommand+5,channel);
  SaveInt2Buf(sCmd.BufCommand+9,owner);
  if(xQueueSend(xQueueCmd2Misul,&sCmd,0))
  {
    if(xQueueReceive(xQueueAnswMisul,&sCmd,100)) return(sCmd.A1);
  }
  return(-1);
}

void _xpri_call_answer(sCommand *psCmd)
{
    int chanpos;
    struct pri_trunk *pri;
    struct pri_bchan_pvt *p;

    int trunkno,channel,owner;

    if(!PRI_IS_WORKING)
    { SendAnswMisul(psCmd,-1); return; }
    
    RestoreIntFromBuf(psCmd->BufCommand+1,&trunkno);
    RestoreIntFromBuf(psCmd->BufCommand+5,&channel);
    RestoreIntFromBuf(psCmd->BufCommand+9,&owner);
    
    pri     = pri_trunks + trunkno;
    chanpos = pri_find_principle(pri, channel);
    if (chanpos < 0) 
    { SendAnswMisul(psCmd,-1); return; }

    p = pri->pvts[chanpos];
    if(!p->call)	
    { SendAnswMisul(psCmd,-1); return; }
    
    PORT_MUTEX_LOCK(&pri->lock);
    if(!p->owner) p->owner=owner;
    p->proceeding = 1;
    p->dialing = 0;
    pri_answer(pri->pri, p->call, 0, 1);
    PORT_MUTEX_UNLOCK(&pri->lock);
    SendAnswMisul(psCmd,0);
}

/**
 * ���������� ������� �� ����� ���� ������ �� ������� ��
 *
 * \param trunkno   ����� ������ E1 (0-3)
 * \param channel   ����� ������ � ������ E1 (0-3) (�� ���� ��� setup)
 * \param owner     ��� �������, ��� ����� ���������� ������
 * \param cause     ������� �����, ������������ ����� � �.�. ������ ���� ������
 *
 * \return 
 */
int pri_call_busy(int trunkno, int channel, int owner, int cause)
{
  static sCommand sCmd;

  sCmd.BufCommand[0]=d_CMD_pri_call_busy;
  SaveInt2Buf(sCmd.BufCommand+1,trunkno);
  SaveInt2Buf(sCmd.BufCommand+5,channel);
  SaveInt2Buf(sCmd.BufCommand+9,owner);
  SaveInt2Buf(sCmd.BufCommand+13,cause);
  if(xQueueSend(xQueueCmd2Misul,&sCmd,0))
  {
    if(xQueueReceive(xQueueAnswMisul,&sCmd,100)) return(sCmd.A1);
  }
  return(-1);
}

void _xpri_call_busy(sCommand *psCmd)
{
    int chanpos;
    struct pri_trunk *pri;
    struct pri_bchan_pvt *p;
    int trunkno,channel,owner,cause;

    if(!PRI_IS_WORKING)
    { SendAnswMisul(psCmd,-1); return; }
    
    RestoreIntFromBuf(psCmd->BufCommand+1,&trunkno);
    RestoreIntFromBuf(psCmd->BufCommand+5,&channel);
    RestoreIntFromBuf(psCmd->BufCommand+9,&owner);
    RestoreIntFromBuf(psCmd->BufCommand+9,&cause);

    pri     = pri_trunks+trunkno;
    chanpos = pri_find_principle(pri, channel);
    if (chanpos < 0) 
    { SendAnswMisul(psCmd,-1); return; }

    p = pri->pvts[chanpos];
    if(!p->call)
    { SendAnswMisul(psCmd,-1); return; }

    PORT_MUTEX_LOCK(&pri->lock);
    if (p->priindication_oob) 
    {
        // ������ ��������� outofband=1
        pri_hangup(pri->pri, p->call, cause);
        p->call = NULL;
        p->alreadyhungup = 1;
        if(p->owner) p->owner=0;

    }
    else if (!p->progress && p->pri && !p->outgoing) 
    {
        pri_progress_with_cause(pri->pri,p->call, 0, 1, cause);

        //������� ��������� � ���������� ���� � ����� (�������������� ��� ������)
        p->progress = 1;
    }
    PORT_MUTEX_UNLOCK(&pri->lock);
    SendAnswMisul(psCmd,0);
}

/**
 * ����� �������� �� ������� ��
 *
 * \param trunkno   ����� ������ E1 (0-3)
 * \param channel   ����� ������ � ������ E1 (0-3) (�� ���� ��� setup)
 * \param owner     ��� �������, ��� ����� ���������� ������
 * \param cause     �������
 *
 * \return 
 */
int pri_call_hangup(int trunkno, int channel, int owner, int cause)
{
  static sCommand sCmd;

  sCmd.BufCommand[0]=d_CMD_pri_call_hangup;
  SaveInt2Buf(sCmd.BufCommand+1,trunkno);
  SaveInt2Buf(sCmd.BufCommand+5,channel);
  SaveInt2Buf(sCmd.BufCommand+9,owner);
  SaveInt2Buf(sCmd.BufCommand+13,cause);
  if(xQueueSend(xQueueCmd2Misul,&sCmd,0))
  {
//DataSave1[1]++;
    if(xQueueReceive(xQueueAnswMisul,&sCmd,100)) return(sCmd.A1);
  }
  return(-1);
}

void _xpri_call_hangup(sCommand *psCmd)
{
    int chanpos;
    struct pri_trunk *pri;
    struct pri_bchan_pvt *p;

    int trunkno,channel,owner,cause;

    if(!PRI_IS_WORKING)
    { SendAnswMisul(psCmd,-1); return; }
    
    RestoreIntFromBuf(psCmd->BufCommand+1,&trunkno);
    RestoreIntFromBuf(psCmd->BufCommand+5,&channel);
    RestoreIntFromBuf(psCmd->BufCommand+9,&owner);
    RestoreIntFromBuf(psCmd->BufCommand+13,&cause);
    pri     = pri_trunks+trunkno;
    chanpos = pri_find_principle(pri,channel);
    if (chanpos<0)
    { SendAnswMisul(psCmd,-1); return; }
    p=pri->pvts[chanpos];
    if(!p->call)
    { SendAnswMisul(psCmd,-1); return; }
    PORT_MUTEX_LOCK(&pri->lock);
    if(p->owner) p->owner=0;
    pri_hangup(pri->pri, p->call, cause);
    p->alreadyhungup = 1;
    PORT_MUTEX_UNLOCK(&pri->lock);
    SendAnswMisul(psCmd,0);
}


/**
 * ����� �� PRI
 *
 * \param trunkno   ����� ������ E1 (0-3)
 * \param channel   ����� ������ � ������ E1 (0-3) (�� ���� ��� setup)
 * \param owner     ��� �������, ��� ����� ���������� ������
 * \param dest      ��������� ����� 
 * \param s         numcomplete (nomer B ���� ==1/==0 ��� �� ���� ����� ����� �����)
 *
 * \return 
 */
int pri_call_info(int trunkno, int channel, int owner, char *dest,int s)
{
  static sCommand sCmd;

  sCmd.BufCommand[0]=d_CMD_pri_call_info;
  SaveInt2Buf(sCmd.BufCommand+1,trunkno);
  SaveInt2Buf(sCmd.BufCommand+5,channel);
  SaveInt2Buf(sCmd.BufCommand+9,owner);
  SaveInt2Buf(sCmd.BufCommand+13,(int)dest);
  SaveInt2Buf(sCmd.BufCommand+17,s);
  if(xQueueSend(xQueueCmd2Misul,&sCmd,0))
  {
    if(xQueueReceive(xQueueAnswMisul,&sCmd,100)) return(sCmd.A1);
  }
  return(-1);
}

void _xpri_call_info(sCommand *psCmd)
{
    int chanpos;
    struct pri_trunk *pri;
    struct pri_bchan_pvt *p	;
    int x, len;
    char ukz;

    int trunkno,channel,owner,s;
    char *dest;

    if(!PRI_IS_WORKING)
    { SendAnswMisul(psCmd,-1); return; }
    
    RestoreIntFromBuf(psCmd->BufCommand+1,&trunkno);
    RestoreIntFromBuf(psCmd->BufCommand+5,&channel);
    RestoreIntFromBuf(psCmd->BufCommand+9,&owner);
    RestoreIntFromBuf(psCmd->BufCommand+13,(int *)&dest);
    RestoreIntFromBuf(psCmd->BufCommand+17,&s);

    pri = pri_trunks + trunkno;
    if(!channel)
    {
        for (x = 0; x < pri->numchans; x++) 
        {
            if (!pri->pvts[x]) continue;
            if ((pri->pvts[x]->owner == owner) && (pri->pvts[x]->call)) 
            {
                channel = x;
                p=pri->pvts[channel];
                break;
            }
        }
    }
    else
    {
        chanpos = pri_find_principle(pri, channel);
        if(chanpos < 0) 
        { SendAnswMisul(psCmd,-1); return; }
        p=pri->pvts[chanpos];
    }
    if(!p->call)
    { SendAnswMisul(psCmd,-1); return; }
    if(p->owner!=owner)
    { SendAnswMisul(psCmd,-1); return; }
    PORT_MUTEX_LOCK(&pri->lock);
    pri_copy_string(p->dialdest_overlap, dest, sizeof(p->dialdest_overlap));
    p->sending_complite=s;
    if (p->setup_ack) 
    {
        len = strlen(p->dialdest_overlap);
        ukz = pri_skip_nonblanks(p->dialdest);
        for (x = 0; x < len; x++) 
        {
            pri_information(pri->pri, p->call, p->dialdest_overlap[x]);
			p->dialdest[ukz]= p->dialdest_overlap[x];
			ukz++;
        }

    } 
    PORT_MUTEX_UNLOCK(&pri->lock);
    SendAnswMisul(psCmd,0);
}

void ExecuteCmdFromYuri(void *pvParameters)
{
  static sCommand sCmd;

  while(1)
  {
    if(xQueueReceive(xQueueCmd2Misul,&sCmd,0))
    {
      switch(sCmd.BufCommand[0])
      {
        case d_CMD_pri_call_setup:
          _xpri_call_setup(&sCmd);
          break;
        case d_CMD_pri_call_alert:
          _xpri_call_alert(&sCmd);
          break;
        case d_CMD_pri_call_answer:
          _xpri_call_answer(&sCmd);
          break;
        case d_CMD_pri_call_busy:
          _xpri_call_busy(&sCmd);
          break;
        case d_CMD_pri_call_hangup:
          _xpri_call_hangup(&sCmd);
          break;
        case d_CMD_pri_call_info:
          _xpri_call_info(&sCmd);
          break;
      }
    }
//    vTaskDelay(10);
//    taskYIELD();
  }
}

#endif

