#ifndef __COMPAT_H
#define __COMPAT_H

#ifdef SOLARIS
typedef	unsigned char	u_int8_t;
typedef	unsigned short	u_int16_t;
typedef	unsigned int	u_int32_t;
#endif

#include "pri_port.h"

int mt_sprintf(char *buf, const char *fmt, ...);
int mt_snprintf(char *_s, size_t _n, const char *_format, ...);
int mt_vsnprintf(char *_s, size_t _n, const char *_format, char *ap);

#endif
