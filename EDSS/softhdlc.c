#ifdef __ISDN__

//#define _HDLC_DEBUG

// ����������� ���������� HDLC
#include "softhdlc.h"
#include <string.h>

///////////////////////////////////////////////////////////////////////////////
void SendStr2IpLogger(const char *fmt,...);

extern unsigned short DataSave1[]; // ��� �������

///////////////////////////////////////////////////////////////////////////////
// HDLC data
#define RETURN_COMPLETE_FLAG    (0x1000)
#define RETURN_DISCARD_FLAG     (0x2000)
#define RETURN_EMPTY_FLAG       (0x4000)

// ��������� hdlc �����������
#define FRAME_SEARCH        0
#define PROCESS_FRAME       1

#define CONTROL_COMPLETE    1
#define CONTROL_ABORT       2

#define STATUS_MASK         (1 << 15)
#define STATUS_VALID        (1 << 15)
#define STATUS_CONTROL      (0 << 15)
#define STATE_MASK          (1 << 15)
#define ONES_MASK           (7 << 12)
#define DATA_MASK           (0xff)

static unsigned char hdlc_search[256];
static unsigned short hdlc_frame[6][1024];
static unsigned int hdlc_encode[6][256];

static int minbits[2] = { 8, 10 };

PORT_SEMAPHORE hdlc_sem = PORT_INVALID_SEMAPHORE;

// CRC data 
#define HDLC_CRC_INITFCS 0xffff
#define HDLC_CRC_GOODFCS 0xf0b8
unsigned short const soft_hdlc_crc_table[256] = {
    0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
    0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
    0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
    0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
    0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
    0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
    0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
    0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
    0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
    0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
    0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
    0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
    0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
    0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
    0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
    0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
    0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
    0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
    0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
    0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
    0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
    0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
    0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
    0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
    0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
    0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
    0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
    0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
    0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
    0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
    0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
    0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78
};

static void soft_hdlc_chan_rd_wake_up(struct hdlc_chan *chan);
static void soft_hdlc_chan_wr_wake_up(struct hdlc_chan *chan);

/**
 * HDLC internal functions
 */
static char soft_hdlc_search_precalc(unsigned char c)
{  // ����� �����
  unsigned char x,p;

  if(c == 0x7e) return 0x18;
  if(c == 0x7f) return 0x08;
  if(c == 0xff) return 0x08;
  x=1;
  p=7;
  while(p && (c & x)) 
  { x <<= 1; p--; }
  return p;
}

#define GEN_ST(status, ones, bits, data) ((status) | ((ones) << 12) | ((bits) << 8) | (data))

static unsigned int soft_hdlc_frame_precalc(unsigned char x, unsigned short c)
{
    unsigned char ones=x,data=0;
    char bits=0,consumed=0;

    while(bits < 8) 
    {
        data >>=1;
        consumed++;
        if (ones == 5) 
        {
            if (c & 0x0200) 
            {
                if ((!(c & 0x0100)) && (bits == 6)) 
                {
                    return GEN_ST(0, 0, 8, CONTROL_COMPLETE);
                } 
                else 
                {
                    return GEN_ST(0, 0, consumed + 1, CONTROL_ABORT);
                }
            }
            else
            {
                ones = 0;
                data <<= 1;
            }
        }
        else 
        {
            if (c & 0x0200) 
            {
                data |= 0x80;
                ones++;
            } 
            else 
                ones=0;
            bits++;
        }
        c <<= 1;
    }

    if (ones == 5) 
    {
        ones = 0;
        consumed++;
    }
    return GEN_ST(STATUS_VALID, ones, consumed, data);
}

static unsigned int soft_hdlc_encode_precalc(int x, unsigned char y)
{
    int bits = 0;
    int ones = x;
    unsigned short data=0;
    char z;
    for (z=0; z<8; z++) 
    {
        /* ����������� */
        if (ones == 5) 
        {
            data <<= 1;
            ones=0;
            bits++;
        }

        if (y & 0x01) 
        {
            data <<= 1;
            data |= 0x1;
            ones++;
            bits++;
        }
        else 
        {
            data <<= 1;
            ones = 0;
            bits++;
        }
        y >>= 1;
    }

    if (ones == 5) 
    {
        data <<= 1;
        ones=0;
        bits++;
    }

    data <<= (10-bits);
    return (data << 22) | (ones << 8) | (bits);
}

static void soft_hdlc_lut_precalc(void)
{
  char x;
  short int y;
    
  for(y = 0; y < 256; y++) 
  { // ��� ������
//csync();
    hdlc_search[y] = soft_hdlc_search_precalc(y);
  }

	for (x = 0; x < 6; x++) 
    { // ��� �������
		for (y = 0; y < 1024; y++) 
        {
			hdlc_frame[x][y] = soft_hdlc_frame_precalc(x, y);
		}
	}
	for (x = 0;x < 6; x++) 
    { // ��� �������������
		for (y = 0; y < 256; y++) 
        {
			hdlc_encode[x][y] = soft_hdlc_encode_precalc(x,y);
		}
	}
}

// ������������� ��������� hdlc �����������
static void soft_hdlc_state_init(struct soft_hdlc_state *h)
{
    h->state = 0;
    h->bits = 0;
    h->data = 0;
    h->ones = 0;
    h->minbits = 8;
}

static int soft_hdlc_tx_load_nocheck(struct soft_hdlc_state *h, unsigned char c)
{
    unsigned int res;

    res = hdlc_encode[h->ones][c];

    h->ones = (res & 0xf00) >> 8;
    h->data |= (res & 0xffc00000) >> h->bits;
    h->bits += (res & 0xf);

    return 0;
}

static int soft_hdlc_tx_load(struct soft_hdlc_state *h, unsigned char c)
{
	if (h->bits > 22) return -1;
	return soft_hdlc_tx_load_nocheck(h, c);
}

static int soft_hdlc_tx_frame_nocheck(struct soft_hdlc_state *h)
{
    h->ones = 0;
    h->data |= ( 0x7e000000 >> h->bits);
    h->bits += 8;

    return 0;
}

static int soft_hdlc_tx_frame(struct soft_hdlc_state *h)
{
    if (h->bits > 24)  return -1;
    return soft_hdlc_tx_frame_nocheck(h);
}

static int soft_hdlc_tx_need_data(struct soft_hdlc_state *h)
{
    if (h->bits < 8)  return 1;
    return 0;
}

static int soft_hdlc_tx_run_nocheck(struct soft_hdlc_state *h)
{
    unsigned char b;

    b = h->data >> 24;
    h->bits -= 8;
    h->data <<= 8;

    return b;
}

static int soft_hdlc_tx_run(struct soft_hdlc_state *h)
{
    if (h->bits < (int)h->minbits) return -1;
    return soft_hdlc_tx_run_nocheck(h);
}

static int soft_hdlc_rx_load_nocheck(struct soft_hdlc_state *h, unsigned char b)
{
    h->data |= b << (24-h->bits);
    h->bits += 8;
    return 0;
}

static int fast_hdlc_rx_load(struct soft_hdlc_state *h, unsigned char b)
{
    if (h->bits > 24) return -1;
    return soft_hdlc_rx_load_nocheck(h, b);
}

/**
 * \return ���� ������ ��������� � �������
 *          RETURN_COMPLETE_FLAG - ������ �����
 *          RETURN_DISCARD_FLAG  - ����������� �����
 *          RETURN_EMPTY_FLAG    - ���������� ������
 */
static int soft_hdlc_rx_run(struct soft_hdlc_state *h)
{
    unsigned short next;
    int retval = RETURN_EMPTY_FLAG;

    while ((h->bits >= minbits[h->state]) && (retval == RETURN_EMPTY_FLAG)) 
    {
        switch(h->state) 
        {
            case FRAME_SEARCH:
                next = hdlc_search[h->data >> 24];
                h->bits -= next & 0x0f;
                h->data <<= next & 0x0f;
                h->state = next >> 4;
                h->ones = 0;
                break;

            case PROCESS_FRAME:

                next = hdlc_frame[h->ones][h->data >> 22];
                h->bits  -= ((next & 0x0f00) >> 8);
                h->data <<= ((next & 0x0f00) >> 8);
                h->state = (next & STATE_MASK) >> 15;
                h->ones = (next & ONES_MASK) >> 12;

                switch(next & STATUS_MASK) 
                {
                    case STATUS_CONTROL:
                        if (next & CONTROL_COMPLETE) 
                        {
                            retval = (RETURN_COMPLETE_FLAG);
                            h->state = 1;
                        }
                        else
                        {
                            retval = (RETURN_DISCARD_FLAG);
                        }
                        break;
                    case STATUS_VALID:
                        retval = (next & DATA_MASK);
                }
        }
    }
    return retval;
}

// CRC internal functions
static unsigned short soft_hdlc_crc_byte(unsigned short crc, unsigned char c)
{
    return (crc >> 8) ^ soft_hdlc_crc_table[(crc ^ c) & 0xff];
}

/**
 * ������ ����������� ����� ��� ������ ������.
 * /note � ������ ��� �������� �����(2 �����) ��� ����������� �����.
 */
static void soft_hdlc_calc_fcs(struct hdlc_chan *hdlc, int inwritebuf)
{
    short int x;
    unsigned int fcs	= HDLC_CRC_INITFCS;
    unsigned char *data = hdlc->wrbuf[inwritebuf].data;
    int len				= hdlc->wrbuf[inwritebuf].size;

    if(len<2) return; // ��� ���������� ����� ��� ������� FCS
    for(x=0;x<len-2;x++) fcs=soft_hdlc_crc_byte(fcs,data[x]);
    fcs^=0xffff;

    // ��������� FCS
    data[len-2]=(fcs & 0xff);
    data[len-1]=(fcs >> 8) & 0xff;
}

// ������������� ������
void soft_hdlc_init()
{
  soft_hdlc_lut_precalc();
  if(PORT_INVALID_SEMAPHORE==hdlc_sem) PORT_SEMAPHORE_INIT(&hdlc_sem);
}

// ������������� ���������� ������ hdlc �����������.
void soft_hdlc_chan_init(struct hdlc_chan *chan, unsigned int mode)
{
  memset(chan, 0, sizeof(struct hdlc_chan));

  PORT_CRITICAL_SECTION_INIT(&chan->lock);
  PORT_SEMAPHORE_INIT(&chan->rdsem);
  PORT_SEMAPHORE_INIT(&chan->wrsem);

  /* HDLC & FCS stuff */
  soft_hdlc_state_init(&chan->rxhdlc);
  soft_hdlc_state_init(&chan->txhdlc);
  chan->infcs = HDLC_CRC_INITFCS;
  chan->inrdbuf   = 0;
  chan->inwrbuf   = 0;
  chan->outrdbuf  = -1;
  chan->outwrbuf  = -1;
  chan->numbufs   = HDLC_BUFS_MAX_NUM;
  chan->blocksize = HDLC_FRAME_MAX_SIZE;
  if(mode & HDLC_MODE_NONBLOCK) chan->nonblock = 1;
  chan->isopened = 1;
}

void soft_hdlc_chan_uninit(struct hdlc_chan *chan)
{
  chan->isopened = 0;
  PORT_CRITICAL_SECTION_DELETE(&chan->lock);
}

/**
 * TODO: �������� ����������� ������� ���������
 * ������ ������� ������ ��������������� ��������
 */
static inline void soft_hdlc_chan_rd_wake_up(struct hdlc_chan *chan)
{
  PORT_SEMAPHORE_GIVE(&hdlc_sem);
  PORT_SEMAPHORE_GIVE(&chan->rdsem);
}

static inline void soft_hdlc_chan_wr_wake_up(struct hdlc_chan *chan)
{
  PORT_SEMAPHORE_GIVE(&chan->wrsem);
}

/**
 * ���������� ������� � ����� � ��������� ���������.
 *
 * \note ������ � ������ ������ ���� ������������ �������
 */
void soft_hdlc_chan_qevent(struct hdlc_chan *chan, int event)
{
    /* ���� ����� ������, ����� */
    if((chan->events.head == 0) && (chan->events.tail == (HDLC_EVENTBUF_MAX_SIZE -1))) return;
    if(chan->events.tail == (chan->events.head - 1)) return;
    /* ��������� ������� */
    chan->events.buffer[chan->events.tail++] = event;
    if(chan->events.tail >= HDLC_EVENTBUF_MAX_SIZE) chan->events.tail = 0;
    soft_hdlc_chan_rd_wake_up(chan);
    return;
}

//#define WAIT() {}

/**
 * �������� ������ ��� ��������
 * TODO: ������� �������� 
 */
int soft_hdlc_chan_wait_wrbuf(struct hdlc_chan *chan)
{
    if (chan->inwrbuf < 0) return HDLC_E_AGAIN; // WAIT();
    return 0;
}

/**
 * �������� ������ ��� ������
 * TODO: ������� ��������
 */
int soft_hdlc_chan_wait_rdbuf(struct hdlc_chan *chan)
{
    if (chan->outrdbuf < 0) return HDLC_E_AGAIN; // WAIT();
    return 0;
}

/**
 * ������ �������� �� ������ ������ � hdlc ����������
 * \return >= 0, ���������� ���������� ������
 * \return <0, ��� ������
 *
 * \info ������ ������ ������ ���� �������� �� ������ ���� �������
 */
static int __soft_hdlc_chan_put_data(struct hdlc_chan *chan, const unsigned char* rxb, unsigned int size)
{
    register int bytes = size;
    register unsigned char *buf;     /* �������� �����*/
    register int oldbuf;
    register int left, x;
    int eof=0;
    int abort=0;
    int res;

    while (bytes) 
    {
        abort = 0;
        eof = 0;
        if (chan->inrdbuf > -1) 
        { /*���� ������� ����� */

            register struct hdlc_frame_buffer *rdbuf = chan->rdbuf + chan->inrdbuf;

            /* ��������� � ������� ����� */
            buf = rdbuf->data; 
            left = chan->blocksize - rdbuf->offset;
            if (left > bytes) left = bytes;
            for (x=0; x<left; x++) 
            {
                /* ������������� HDLC ������� */
                soft_hdlc_rx_load_nocheck(&chan->rxhdlc, *(rxb++));
                bytes--;
                res = soft_hdlc_rx_run(&chan->rxhdlc);

                /* ���� ������ ���, ���������� */
                if (res & RETURN_EMPTY_FLAG) continue;
                else
                  if (res & RETURN_COMPLETE_FLAG) 
                  {
                    if (rdbuf->offset) 
                    { // ����������� ��� ������ ���� ���� ������ � ������
                        if(chan->infcs!=HDLC_CRC_GOODFCS) abort = HDLC_EVENT_BADFCS;
                        else eof=1;
                        break;
                    }
                    continue;
                  } 
                  else
                    if (res & RETURN_DISCARD_FLAG) 
                    { // "idle" ?
                      if (!rdbuf->offset) continue;
                      abort = HDLC_EVENT_ABORT;
                      break;
                    } 
                    else 
                    {
                      unsigned char rxc;
                      rxc = res;
                      chan->infcs = soft_hdlc_crc_byte(chan->infcs, rxc);
                      buf[rdbuf->offset++] = rxc;

                      // �������� �� ������������
                      if(rdbuf->offset >= chan->blocksize) 
                      {
#ifdef _HDLC_DEBUG
                         SendStr2IpLogger("HDLC Receiver overrun on channel %d ", chan->id);
#endif 
                         abort = HDLC_EVENT_OVERRUN;
                         // ��������� HDLC �������� � ��������� ������ ������
                         chan->rxhdlc.state  = 0;
                         chan->rxhdlc.bits   = 0;
                         rdbuf->offset       = 0;
                         break;
                      }
                    }
            }

            if(eof)  
            { // ��������� �������� ������ ��������, ����� ���������
                oldbuf = chan->inrdbuf;
                chan->infcs = HDLC_CRC_INITFCS;
                chan->rdbuf[chan->inrdbuf].size = chan->rdbuf[chan->inrdbuf].offset;

#ifdef _HDLC_DEBUG
                SendStr2IpLogger("EOF, len is %d", rdbuf->size);
#endif

                chan->inrdbuf = (chan->inrdbuf + 1) % chan->numbufs;
                if (chan->inrdbuf == chan->outrdbuf) 
                {
                    /* ���, ������ ��� ��������� �������, 
                       ���������� ������ ���� �� �������� ����� */
#ifdef _HDLC_DEBUG
                    SendStr2IpLogger("Out of storage space");
#endif
                    chan->inrdbuf = -1;
                }

                if (chan->outrdbuf < 0) 
                { /* �������� � ����� ������ � ������� */
                   chan->outrdbuf = oldbuf;
                   soft_hdlc_chan_rd_wake_up(chan);
                }
                // TODO: ����� ��� ��� �������� (���� ��� �� ��������������)
                //soft_hdlc_chan_rd_wake_up(chan);
            }
            if (abort) 
            {
                /* �������� ����� ������ ������ */
                chan->rdbuf[chan->inrdbuf].offset = 0;
                chan->infcs = HDLC_CRC_INITFCS;

                if (chan->isopened) 
                { /* �������� ��������� */
                    soft_hdlc_chan_qevent(chan, abort);
                }
            }
        }
        else 
            /* ��� ����� ��� ������. ���������� ������*/
            break;
    }

    if (bytes) 
    {
        if (!chan->rxoverrun) 
        { /* �������� ��������� */
          soft_hdlc_chan_qevent(chan, abort);
          chan->rxoverrun = 1;
        }
    }
    else chan->rxoverrun = 0;
    return size - bytes;
}

/**
 * ������ ������ ��� ������ �������� �� hdlc �����������
 * \return >= 0, ���������� ����������� ������
 * \return <0, ��� ������
 * \note ������ ������ ������ ���� �������� �� ������ ���� �������
 */
static int __soft_hdlc_chan_get_data(struct hdlc_chan *chan, unsigned char* txb, unsigned int size)
{
    register unsigned char *buf;     /* ��� ����� � ������� */
    register int oldbuf;
    register int bytes = size;       /* ������� ���� ����� ����������*/
    register int left;
    register int x;

    while(bytes) 
    {
        if (chan->outwrbuf > -1) 
        {
            register struct hdlc_frame_buffer *wrbuf = chan->wrbuf + chan->outwrbuf;

            buf = wrbuf->data;
            left = wrbuf->size - wrbuf->offset;

            if (left > bytes) left = bytes;

            /* �������� ������ �� HDLC ����������� */
            for (x=0; x<left; x++) 
            {
                if (soft_hdlc_tx_need_data(&chan->txhdlc))
                    /* ��������� ���� ������ ������ ���� ���� */
                    soft_hdlc_tx_load_nocheck(&chan->txhdlc, buf[wrbuf->offset++]);
                *(txb++) = soft_hdlc_tx_run_nocheck(&chan->txhdlc);
            }
            bytes -= left;

            /* �������� ��������� ������ */
            if (wrbuf->offset >= wrbuf->size) 
            {
                /* �������� ����� ������, ��������� � ���������� */
                oldbuf = chan->outwrbuf;
                
                /* ���������� ������ � ������� ������ */
                wrbuf->offset = 0;
                chan->outwrbuf = (chan->outwrbuf + 1) % chan->numbufs;

                wrbuf->size = 0;

                if (chan->outwrbuf == chan->inwrbuf) 
                {
                    /* ���, � ��� ��� ������� � �������. �������� ��� ��� -1 �
                       ���� ���� ��� �������� ��� ���� ������ ��� ������ */
                    chan->outwrbuf = -1;

                    //TODO: ���� �� ���� �� ������?
                    //    soft_hdlc_wake_up(chan);
                }

                if (chan->inwrbuf < 0) 
                { // �������� �� ���� ����� ��� ������ ������, ������� ��� �������������� �����
                    chan->inwrbuf = oldbuf;
                }

                /* ���������� ��������� */
                soft_hdlc_chan_wr_wake_up(chan);

                /* �������� ���� */
                soft_hdlc_tx_frame_nocheck(&chan->txhdlc);
            }
        } 
        else
        {
            for (x = 0; x < bytes; x++) 
            {
                /* �� ��������� �������� ����� */
                if (soft_hdlc_tx_need_data(&chan->txhdlc)) soft_hdlc_tx_frame_nocheck(&chan->txhdlc);
                *(txb++) = soft_hdlc_tx_run_nocheck(&chan->txhdlc);
            }
            bytes = 0;
        }
    }
    return size - bytes;
}

/**
 * ������ �������� �� ������ ������ � hdlc ����������
 * \return >= 0, ���������� ���������� ������
 * \return <0, ��� ������
 */
int soft_hdlc_chan_put_data(struct hdlc_chan *chan, const unsigned char* rxb, unsigned int size)
{
    int result;
    
    PORT_CRITICAL_SECTION_ENTER(&chan->lock);
    result = __soft_hdlc_chan_put_data(chan, rxb, size);
    PORT_CRITICAL_SECTION_LEAVE(&chan->lock);
    return result;
}

/**
 * ������ ������ ��� ������ �������� �� hdlc �����������
 * \return >= 0, ���������� ����������� ������
 * \return <0, ��� ������
 * \note ������ ������ ������ ���� �������� �� ������ ���� �������
 */
int soft_hdlc_chan_get_data(struct hdlc_chan *chan, unsigned char* txb, unsigned int size)
{
    int result;

    PORT_CRITICAL_SECTION_ENTER(&chan->lock);
    result = __soft_hdlc_chan_get_data(chan, txb, size);
    PORT_CRITICAL_SECTION_LEAVE(&chan->lock);
    return result;
}

// ������ ��������������� ������ �� hdlc �����������.
int soft_hdlc_chan_read(struct hdlc_chan *chan, unsigned char *rxb, unsigned int count)
{
    int amnt;
    int res, rv;
    int oldbuf;

    //assert(chan!=NULL);
    if (!count) return 0;
    for (;;)
    {

        if (chan->events.tail != chan->events.head) return HDLC_E_EVENT; // ���� ������� � ������
        res = chan->outrdbuf;
        if (res >= 0) break;
        if (chan->nonblock) return HDLC_E_AGAIN;
        rv = soft_hdlc_chan_wait_rdbuf(chan);
        if (rv) return rv;
    }
    amnt = count;
    if (amnt > chan->rdbuf[res].size) amnt = chan->rdbuf[res].size;
    if (amnt) memcpy(rxb, chan->rdbuf[res].data, amnt);
    PORT_CRITICAL_SECTION_ENTER(&chan->lock);
    chan->rdbuf[res].offset = 0;
    chan->rdbuf[res].size   = 0;
    oldbuf = res;
    chan->outrdbuf = (res + 1) % chan->numbufs;
    if (chan->outrdbuf == chan->inrdbuf) chan->outrdbuf = -1; // ������ ��� ������
    if (chan->inrdbuf < 0) chan->inrdbuf = oldbuf; // �������� ����������� ���������� ��� � ��� ���� ��������� �����
    PORT_CRITICAL_SECTION_LEAVE(&chan->lock);
    return amnt;
}


/**
 * ������ ������ ������ ��� ��������. ������ ��������, �������������� 2 ����� ��� ����������� �����.
 * ����������� ����� ��������� �������������.
 */
int soft_hdlc_chan_write(struct hdlc_chan *chan, const unsigned char *txb, unsigned int count)
{
    int res, amnt, oldbuf, rv;

    //assert(chan!=NULL)

    if(!count) return 0;

    for(;;) 
    {
        if (chan->events.tail != chan->events.head) return HDLC_E_EVENT;
        res = chan->inwrbuf;
        if (res >= 0) break;
        if (chan->nonblock) return HDLC_E_AGAIN;
        /* ������� ���� �� �������� ��������� ����� */
        rv = soft_hdlc_chan_wait_wrbuf(chan);
        if (rv) return rv;
    }

    amnt = count;
    if (amnt > chan->blocksize) amnt = chan->blocksize;

#ifdef _HDLC_DEBUG
    SendStr2IpLogger("soft_hdlc_chan_write: chan=%d, res=%d, outwrbuf=%d amnt=%d",
                     chan->id, res, chan->outwrbuf, amnt);
#endif

    if(amnt) 
    {
        memcpy(chan->wrbuf[res].data, txb, amnt);
        chan->wrbuf[res].size = amnt;
    }
    chan->wrbuf[res].offset = 0;

    soft_hdlc_calc_fcs(chan, res);
    oldbuf = res;

    PORT_CRITICAL_SECTION_ENTER(&chan->lock);
    chan->inwrbuf = (res + 1) % chan->numbufs;
    if (chan->inwrbuf == chan->outwrbuf) 
    { // ��������� ������ ����� ������, ���� ���������� �� ��������
      chan->inwrbuf = -1;
    }
    if (chan->outwrbuf < 0) 
    { // ���������� ���� ������, ����� �����
      chan->outwrbuf = oldbuf;
    }
    PORT_CRITICAL_SECTION_LEAVE(&chan->lock);
    return amnt;
}

// ������ ������� � hdlc �����������
int soft_hdlc_chan_read_event(struct hdlc_chan *chan)
{
    /* set up for no event */
    int event = HDLC_EVENT_NONE;

    PORT_CRITICAL_SECTION_ENTER(&chan->lock);
    /* if some event in queue */
    if (chan->events.tail != chan->events.head)
    { // ��������� ��� �������, ��������� ��������� */
      event = chan->events.buffer[chan->events.head++];
      if (chan->events.head >= HDLC_EVENTBUF_MAX_SIZE) chan->events.head = 0;
    }
    PORT_CRITICAL_SECTION_LEAVE(&chan->lock);
    return event;
}

/**
 * �������� ���������� �������
 * TODO: �����������
 */
/*int soft_hdlc_chan_wait_what(struct hdlc_chan *chan, int what, unsigned long timeout)
{
    int gotevent = 0;

    if (!what) return HDLC_E_INVALID;
    // �������� �������� ������ ��� �������
    if (what == HDLC_WAIT_READ)
    {
        PORT_CRITICAL_SECTION_ENTER(&chan->lock);
        // if some event in queue
        if (chan->events.tail != chan->events.head) gotevent = 1;
        PORT_CRITICAL_SECTION_LEAVE(&chan->lock);
        
        if (gotevent) return HDLC_E_EVENT;
        if (chan->outrdbuf >= 0) return 1;
        if (PORT_SEMAPHORE_TAKEN != PORT_SEMAPHORE_TAKE(&chan->rdsem, timeout)) return HDLC_E_TIMEOUT;
        
        PORT_CRITICAL_SECTION_ENTER(&chan->lock);
        // if some event in queue
        if (chan->events.tail != chan->events.head) gotevent = 1;
        PORT_CRITICAL_SECTION_LEAVE(&chan->lock);
        if (gotevent) return HDLC_E_EVENT;
        return 1;
    }
    return HDLC_E_INVALID;
}*/

#define CHAN_WM_MAKE_RESULT(chan, res)      ((((chan) & 0xFFFF) << 16) | ((res) & 0xFFFF))

// �������� �������� ������ ��� ������� ��� ��������� �������
/*unsigned int soft_hdlc_chan_wait_multiple(struct hdlc_chan *chans[], unsigned int chcount,  int what, unsigned long timeout)
{
    unsigned int i;
    int gotevent = 0;
    struct hdlc_chan *chan;

    if (!what) return CHAN_WM_MAKE_RESULT(-1, HDLC_E_INVALID);
    if (what!=HDLC_WAIT_READ) return CHAN_WM_MAKE_RESULT(-1, HDLC_E_INVALID);
    
    for(i=0;i<chcount;i++)
    { // �������� �������� ������ ��� �������
        chan = chans[i];
        if(!chan) continue;
        PORT_CRITICAL_SECTION_ENTER(&chan->lock);
        // if some event in queue
        if (chan->events.tail != chan->events.head) gotevent = 1;
        PORT_CRITICAL_SECTION_LEAVE(&chan->lock);
        if (gotevent) return CHAN_WM_MAKE_RESULT(i, HDLC_E_EVENT);
        if (chan->outrdbuf>=0) return CHAN_WM_MAKE_RESULT(i, 1);
    }
    if(PORT_SEMAPHORE_TAKEN!=PORT_SEMAPHORE_TAKE(&hdlc_sem,timeout)) return CHAN_WM_MAKE_RESULT(-1,HDLC_E_TIMEOUT);
    for (i = 0; i < chcount; i++)
    {
        chan = chans[i];
        if(!chan) continue;
        PORT_CRITICAL_SECTION_ENTER(&chan->lock);
        // if some event in queue
        if (chan->events.tail != chan->events.head) gotevent = 1;
        PORT_CRITICAL_SECTION_LEAVE(&chan->lock);
        if (gotevent) return CHAN_WM_MAKE_RESULT(i, HDLC_E_EVENT);
        if (chan->outrdbuf >= 0) return CHAN_WM_MAKE_RESULT(i, 1);
    }
    return CHAN_WM_MAKE_RESULT(-1, HDLC_E_INVALID);
}
*/

// �������� �������� ������ ��� ������� ��� ��������� �������
unsigned int soft_hdlc_chan_wait(struct hdlc_chan *chan, unsigned long timeout)
{
    int gotevent = 0;

    if(!chan) return CHAN_WM_MAKE_RESULT(-1, HDLC_E_INVALID);
    PORT_CRITICAL_SECTION_ENTER(&chan->lock);
    // if some event in queue
    if (chan->events.tail != chan->events.head) gotevent = 1;
    PORT_CRITICAL_SECTION_LEAVE(&chan->lock);
    if (gotevent) return CHAN_WM_MAKE_RESULT(0, HDLC_E_EVENT);
    if (chan->outrdbuf>=0) return CHAN_WM_MAKE_RESULT(0, 1);
    if(PORT_SEMAPHORE_TAKEN!=PORT_SEMAPHORE_TAKE(&hdlc_sem,timeout)) return CHAN_WM_MAKE_RESULT(-1,HDLC_E_TIMEOUT);
/*
    PORT_CRITICAL_SECTION_ENTER(&chan->lock);
    // if some event in queue
    if (chan->events.tail != chan->events.head) gotevent = 1;
    PORT_CRITICAL_SECTION_LEAVE(&chan->lock);
    if (gotevent) return CHAN_WM_MAKE_RESULT(0, HDLC_E_EVENT);
    if (chan->outrdbuf >= 0) return CHAN_WM_MAKE_RESULT(0, 1);*/
    return CHAN_WM_MAKE_RESULT(-1, HDLC_E_INVALID);
}

// ���������� ������� � ��������� ������ � �������������� �� ����.
void soft_hdlc_chan_alarm(struct hdlc_chan *chan, int alarms)
{
    PORT_CRITICAL_SECTION_ENTER(&chan->lock);
    soft_hdlc_chan_qevent(chan, alarms ? HDLC_EVENT_ALARM : HDLC_EVENT_NOALARM);
    PORT_CRITICAL_SECTION_LEAVE(&chan->lock);
}

#endif

