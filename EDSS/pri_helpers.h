#if !defined(__PRI_HELPERS__)
#define __PRI_HELPERS__

#include "pri_port.h"





static force_inline int pri_strlen_zero(const char *s)
{
    return (!s || (*s == '\0'));
}

void pri_shrink_phone_number(char *n);
void pri_copy_string(char *dst, const char *src, size_t size);
int pri_skip_nonblanks(char *str);


#endif /* #if !defined(__PRI_HELPERS__) */
