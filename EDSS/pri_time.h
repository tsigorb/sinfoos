/**
 * ������� ��� ������ �� ��������
 */


#if !defined(__PRI_TIME_H__)
#define __PRI_TIME_H__



#include "pri_port.h"

typedef time_t pri_time_t;
typedef long pri_suseconds_t;


/**
 * �������� timeval
 */
static struct timeval pri_tvmake(pri_time_t sec, pri_suseconds_t usec)
{
    struct timeval t;
    t.tv_sec = (long)sec;
    t.tv_usec = usec;
    return t;
}

/**
 * ��������� �������� �������
 */
static struct timeval pri_tvnow(void)
{
    struct timeval t;
    gettimeofday(&t, NULL);
    return t;
}

/**
 * ��������� timeval
 */
static int pri_tvcmp(struct timeval _a, struct timeval _b)
{
    if (_a.tv_sec < _b.tv_sec)
        return -1;
    if (_a.tv_sec > _b.tv_sec)
        return 1;

    /* now seconds are equal */
    if (_a.tv_usec < _b.tv_usec)
        return -1;
    if (_a.tv_usec > _b.tv_usec)
        return 1;
    return 0;
}


/**
 * 
 */
static int pri_tvzero(const struct timeval t)
{
	return (t.tv_sec == 0 && t.tv_usec == 0);
}


#define ONE_MILLION	1000000


/**
 * put timeval in a valid range. usec is 0..999999
 * negative values are not allowed and truncated.
 */
static struct timeval pri_tvfix(struct timeval a)
{
    if (a.tv_usec >= ONE_MILLION) 
    {
        a.tv_sec += a.tv_usec / ONE_MILLION;
        a.tv_usec %= ONE_MILLION;
    }
    else if (a.tv_usec < 0) 
    {
        a.tv_usec = 0;
    }
    return a;
}

static struct timeval pri_tvadd(struct timeval a, struct timeval b)
{
    /* consistency checks to guarantee usec in 0..999999 */
    a = pri_tvfix(a);
    b = pri_tvfix(b);
    a.tv_sec += b.tv_sec;
    a.tv_usec += b.tv_usec;
    if (a.tv_usec >= ONE_MILLION) {
        a.tv_sec++;
        a.tv_usec -= ONE_MILLION;
    }
    return a;
}

static struct timeval pri_tvsub(struct timeval a, struct timeval b)
{
    /* consistency checks to guarantee usec in 0..999999 */
    a = pri_tvfix(a);
    b = pri_tvfix(b);

    a.tv_sec -= b.tv_sec;
    a.tv_usec -= b.tv_usec;

    if (a.tv_usec < 0) 
    {
        a.tv_sec-- ;
        a.tv_usec += ONE_MILLION;
    }
    return a;
}


#endif /* !defined(__PRI_TIME_H__) */
