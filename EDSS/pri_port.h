#if !defined(__PRI_PORT_H__)
#define __PRI_PORT_H__


//#define _WINDOWS
#define __FREERTOS_USED__


#define __BIG_ENDIAN        0
#define __LITTLE_ENDIAN     1
#define __BYTE_ORDER        __LITTLE_ENDIAN

#define __attribute__(s)     
#define u_int8_t unsigned char
#define u_int32_t unsigned int
#define u_int16_t unsigned short
#define int16_t short
#define int8_t char
#define int32_t int


#if defined(__FREERTOS_USED__)

typedef long int __time_t;

struct timespec
{
    __time_t tv_sec;		/* Seconds.  */
    long int tv_nsec;		/* Nanoseconds.  */
};


/* A time value that is accurate to the nearest
microsecond but also has a range of years.  */
struct timeval
{
    __time_t tv_sec;		/* Seconds.  */
    long int tv_usec;	/* Microseconds.  */
};

struct timezone {
    int tz_minuteswest; /* minutes W of Greenwich */
    int tz_dsttime;     /* type of dst correction */
};


#include "..\rtc.h"

#define time(t)     {*(t) = ReadClock();}
#define localtime_r(t,tm)   {*(tm) = *gmtime(t);}

int gettimeofday(struct timeval *tv, struct timezone *tz);

void *pvPortCalloc(size_t nmemb, size_t size);


void *pvPortBigMalloc(size_t size);
void *pvPortBigCalloc(size_t nmemb, size_t size);
void vPortBigFree(void *pv);

#define random rand

#define force_inline inline

//#define PORT_MALLOC pvPortMalloc
//#define PORT_CALLOC pvPortCalloc
//#define PORT_FREE   vPortFree

#define PORT_MALLOC pvPortBigMalloc
#define PORT_CALLOC pvPortBigCalloc
#define PORT_FREE   vPortBigFree



#define PORT_BIG_MALLOC     pvPortBigMalloc
#define PORT_BIG_CALLOC     pvPortBigCalloc
#define PORT_BIG_FREE       pvPortBigFree

#define free(x)     xxx()


#else // WIN

    #include <windows.h>

    void DbgInitializeCriticalSection(LPCRITICAL_SECTION lock);
    void DbgEnterCriticalSection(LPCRITICAL_SECTION lock);
    void DbgLeaveCriticalSection(LPCRITICAL_SECTION lock);

    #define PORT_MUTEX                          CRITICAL_SECTION
    #define PORT_MUTEX_INIT(lock)               DbgInitializeCriticalSection(lock)
    #define PORT_MUTEX_DELETE(lock)             DeleteCriticalSection(lock)
    #define PORT_MUTEX_LOCK(lock)               DbgEnterCriticalSection(lock)
    #define PORT_MUTEX_UNLOCK(lock)             DbgLeaveCriticalSection(lock)



//static void DbgInitSemaphore(HANDLE *s)
//{
//    *(s) = CreateSemaphore(NULL, 1, 1, NULL);
//}
//
//static void DbgLockSemaphore(HANDLE *s)
//{
//    WaitForSingleObject(*(s), INFINITE);
//}
//static void DbgUnlockSemaphore(HANDLE *s)
//{
//    ReleaseSemaphore(*(s), 1, NULL);
//}
//
//
//#define PORT_MUTEX                      HANDLE
//#define PORT_MUTEX_INIT(s)              DbgInitSemaphore(s)
//#define PORT_MUTEX_DELETE(s)            CloseHandle(*(s))             
//#define PORT_MUTEX_LOCK(s)              DbgLockSemaphore(s)
//#define PORT_MUTEX_UNLOCK(s)            DbgUnlockSemaphore(s)




    int gettimeofday(struct timeval *tv, struct timezone *tz);

    #define localtime_r(t,tm)   {*(tm) = *localtime(t);}

    #define snprintf _snprintf

    #define random rand

    #define inline

    #define force_inline

    #define _CRT_SECURE_NO_DEPRECATE

    #define PORT_MALLOC malloc
    #define PORT_CALLOC calloc
    #define PORT_FREE   free


    #define section(x)

#endif // defined(__FREERTOS_USED__)


#define __PRETTY_FUNCTION__ __FUNCTION__


#if !defined HAVE_STRCASECMP
int strcasecmp (const char *s1, const char *s2);
#endif



#endif // !defined(__PRI_PORT_H__)

