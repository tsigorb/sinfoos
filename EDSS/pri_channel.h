#if !defined(__CHAN_PRI_H__)
#define __CHAN_PRI_H__

#include "pri_config.h"

//#if !defined(__ISDN__)
//#define __ISDN__
//#endif

extern struct hdlc_chan hdlc_ctrl[NUM_TRUNKS];

void *pri_dchannel(void *vpri);
int pri_chan_start(int reload,unsigned char *pMasMaxIsdnChannels);
void pri_all_dchannel(void *pvParameters);

#endif /* !defined(__CHAN_PRI_H__) */

