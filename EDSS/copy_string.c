 
#ifdef LINUX
#include <unistd.h>
#endif
#ifdef __ISDN__


#include <string.h>

#include <stdlib.h>

#include "compiler.h"
#include "libpri.h"
#include "pri_internal.h"

void libpri_copy_string(char *dst, const char *src, size_t size)
{
	while (*src && size) {
		*dst++ = *src++;
		size--;
	}
	if (__builtin_expect(!size, 0))
		dst--;
	*dst = '\0';
}
#endif
