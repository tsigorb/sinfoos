#ifdef __ISDN__

#include "pri_helpers.h"
#include <string.h>

/*! \brief Shrink a phone number in place to just digits (more accurately it just removes ()'s, .'s, and -'s... */
/*!
 * \param n The number to be stripped/shrunk
 * \return Returns nothing important
 */
/*! \brief Clean up phone string
 * remove '(', ' ', ')', non-trailing '.', and '-' not in square brackets.
 * Basically, remove anything that could be invalid in a pattern.
 */
void pri_shrink_phone_number(char *n)
{
    int x, y=0;
    int bracketed = 0;

    for (x=0; n[x]; x++) {
        switch(n[x]) {
        case '[':
            bracketed++;
            n[y++] = n[x];
            break;
        case ']':
            bracketed--;
            n[y++] = n[x];
            break;
        case '-':
            if (bracketed)
                n[y++] = n[x];
            break;
        case '.':
            if (!n[x+1])
                n[y++] = n[x];
            break;
        default:
            if (!strchr("( )", n[x]))
                n[y++] = n[x];
        }
    }
    n[y] = '\0';
}

void pri_copy_string(char *dst, const char *src, size_t size)
{
    while (*src && size) {
        *dst++ = *src++;
        size--;
    }
    if (!size)
        dst--;
    *dst = '\0';
}

/*!
 * \brief Gets a pointer to first whitespace character in a string.
 *
 * \param str the input string
 * \return a pointer to the first whitespace character
 */
int pri_skip_nonblanks(char *str)
{
	int x=0;
	while (*str && (str[x] != 0))
		x++;
	return x;
}
#endif

