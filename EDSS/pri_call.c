#ifdef __ISDN__

#include "..\Sinfo.h"

#include "pri_config.h"
#include "pri_helpers.h"
#include "pri_debug.h"
#include "pri_internal.h"


/////////////////////////////////////////////////////////////////////
extern unsigned short DataSave1[];  // ��� �������!!!

extern int PRI_IS_WORKING;

/////////////////////////////////////////////////////////////////////
int pri_find_empty_chan(struct pri_trunk *pri);
int pri_find_principle(struct pri_trunk *pri, int channel);

/////////////////////////////////////////////////////////////////////
/**
 *  ������ �� PRI
 *
 * \param src   ��� �������� ��� ����� ���������� ������ 
 * \param �     ����� ������ E1 (0-3)
 * \param dest  ��������� ����� � ������� type_numberb;
 * \param s     numcomplete (nomer B ���� ==1/==0 ��� �� ���� ����� ����� �����)
 * \param cid   ����� ��������� � ������� type_numbera
 * \param cname ��� ��������� 
 *
 * \return      ����� ������|� ������<<8
 *
 * \note ���� ���� ������������� �� ����� �������� ��� ������������� � �� ����� ������� redirect_reason, cid_rdnis
 */
int pri_call_setup(int src,int trunkno,char *dest,int s,char *cid,char *cname)
{
#ifdef SUPPORT_USERUSER
  const char *useruser;
#endif
  struct pri_trunk *ptrunk;
  struct pri_bchan_pvt *p;
  struct pri_sr *sr;
  int exclusive;
  int channel;

  if(!PRI_IS_WORKING) return(-1);
  if (trunkno==0xff)
  {
    for (trunkno=0;trunkno < NUM_TRUNKS;trunkno++)
    {
      ptrunk = pri_trunks+trunkno;
      PORT_MUTEX_LOCK(&ptrunk->lock);
      channel=pri_find_empty_chan(ptrunk);
      if(channel>=0) break;
      PORT_MUTEX_UNLOCK(&ptrunk->lock);
	}
	if(channel<0)
	{
       if(ptrunk->debug) pri_message(ptrunk->pri,"ERROR: No free channelX %d", trunkno);
	   return(-1);
	}
  }
  else
  {
    if(trunkno>NUM_TRUNKS) return(-1);
	ptrunk = pri_trunks+trunkno;
	PORT_MUTEX_LOCK(&ptrunk->lock);
	channel = pri_find_empty_chan(ptrunk);
	if(channel<0)
	{
      if (ptrunk->debug) pri_message(ptrunk->pri,"ERROR: No free channel %d", trunkno);
      PORT_MUTEX_UNLOCK(&ptrunk->lock);
      return(-1);
	}
  }
        p=ptrunk->pvts[channel];
    	p->alreadyhungup = 0;

		if (!(p->call = pri_new_call(ptrunk->pri)))
		{
          if(ptrunk->debug) pri_message(ptrunk->pri,"Unable to create call on channel %d", p->channel);
          PORT_MUTEX_UNLOCK(&ptrunk->lock);
          return(-1);
        }
        if (!(sr = pri_sr_new())) 
        {
            if (ptrunk->debug) pri_message(ptrunk->pri, "Failed to allocate setup request channel %d", p->channel);
            pri_destroycall(ptrunk->pri, p->call);
            p->call = NULL;
			PORT_MUTEX_UNLOCK(&ptrunk->lock);
          return(-1); //��� ��������� �������
        }
        p->dialdest_overlap[0]  = '\0';
		p->dialdest[0]  = '\0';
		p->dialing      = 1;
		p->proceeding      = 0;
		p->alerting      = 0;
		p->progress		=0;
        p->outgoing     = 1;
        p->owner        = src;  //����� �����
		p->sending_complite=s;	//sending complete
        /* Should the picked channel be used exclusively? */
        if (p->priexclusive || ptrunk->nodetype == PRI_NETWORK) exclusive = 1;
        else exclusive = 0;
        pri_sr_set_channel(sr, PVT_TO_CHANNEL(p), exclusive, 1);
        pri_sr_set_bearer(sr, PRI_TRANS_CAP_SPEECH, PRI_LAYER_1_ALAW); 
        if (ptrunk->facilityenable) pri_facility_enable(ptrunk->pri);
		pri_copy_string(p->dialdest, dest, sizeof(p->dialdest));
		pri_copy_string(p->cid_name, cname,sizeof(p->cid_name));
		pri_copy_string(p->cid_num, cid,sizeof(p->cid_num));
		pri_sr_set_called(sr, dest, ptrunk->dialplan, s);
        pri_sr_set_caller(sr, cid, cname, ptrunk->localdialplan,p->callprogress);

#ifdef SUPPORT_USERUSER
        /* User-user info */
        useruser = pri_bchan_pvt->(p->owner, "USERUSERINFO");
        if (useruser) pri_sr_set_useruser(sr, useruser);
#endif
       if (ptrunk->debug) pri_message(ptrunk->pri, "Send Setup %d", p->channel);
        if (pri_setup(ptrunk->pri, p->call, sr))
        {
            pri_sr_free(sr);
			PORT_MUTEX_UNLOCK(&ptrunk->lock);
          return(-1);
        }
        pri_sr_free(sr);
        PORT_MUTEX_UNLOCK(&ptrunk->lock);
  return(PVT_TO_CHANNEL(p) | (trunkno<<8));
}

/**
 * ���������� ������� �������� � ��� ������� �����
 *
 * \param trunkno   ����� ������ E1 (0-3)
 * \param channel   ����� ������ � ������ E1 (0-3) (�� ���� ��� setup)
 * \param owner     ��� �������, ��� ����� ���������� ������
 *
 * \return 
 */
int pri_call_alert(int trunkno, int channel, int owner)
{
    int	chanpos;
    struct pri_trunk *pri;
    struct pri_bchan_pvt *p;

    if(!PRI_IS_WORKING)
    { return(-1); }
    
    pri     = pri_trunks+trunkno;
    chanpos = pri_find_principle(pri, channel);
    if (chanpos < 0)
    { return(-1); }
    p = pri->pvts[chanpos];
    if(!p->call)
    { return(-1); }
    PORT_MUTEX_LOCK(&pri->lock);
    if(!p->owner) p->owner = owner;
    if ((!p->alerting) && (!p->outgoing )) 
    {
      pri_acknowledge(pri->pri,p->call, PVT_TO_CHANNEL(p), 1);
    }
    p->alerting = 1;
    PORT_MUTEX_UNLOCK(&pri->lock);
    return(0);
}

/**
 * ���������� ������� �������
 *
 * \param trunkno   ����� ������ E1 (0-3)
 * \param channel   ����� ������ � ������ E1 (0-3) (�� ���� ��� setup)
 * \param owner     ��� �������, ��� ����� ���������� ������
 *
 * \return 
 */
int pri_call_answer(int trunkno, int channel, int owner)
{
    int chanpos;
    struct pri_trunk *pri;
    struct pri_bchan_pvt *p;

    if(!PRI_IS_WORKING)
    { return(-1); }
    
    pri     = pri_trunks+trunkno;
    chanpos = pri_find_principle(pri, channel);
    if (chanpos < 0) 
    { return(-1); }

    p = pri->pvts[chanpos];
    if(!p->call)	
    { return(-1); }
    
    PORT_MUTEX_LOCK(&pri->lock);
    if(!p->owner) p->owner=owner;
    p->proceeding = 1;
    p->dialing = 0;
    pri_answer(pri->pri, p->call, 0, 1);
    PORT_MUTEX_UNLOCK(&pri->lock);
    return(0);
}

/**
 * ���������� ������� �� ����� ���� ������ �� ������� ��
 *
 * \param trunkno   ����� ������ E1 (0-3)
 * \param channel   ����� ������ � ������ E1 (0-3) (�� ���� ��� setup)
 * \param owner     ��� �������, ��� ����� ���������� ������
 * \param cause     ������� �����, ������������ ����� � �.�. ������ ���� ������
 *
 * \return 
 */
int pri_call_busy(int trunkno, int channel, int owner, int cause)
{
    int chanpos;
    struct pri_trunk *pri;
    struct pri_bchan_pvt *p;

    if(!PRI_IS_WORKING)
    { return(-1); }
    
    pri=pri_trunks+trunkno;
    chanpos = pri_find_principle(pri, channel);
    if (chanpos < 0) 
    { return(-1); }

    p = pri->pvts[chanpos];
    if(!p->call)
    { return(-1); }

    PORT_MUTEX_LOCK(&pri->lock);
    if (p->priindication_oob) 
    {
        // ������ ��������� outofband=1
        pri_hangup(pri->pri, p->call, cause);
        p->call = NULL;
        p->alreadyhungup = 1;
        if(p->owner) p->owner=0;

    }
    else if (!p->progress && p->pri && !p->outgoing) 
    {
        pri_progress_with_cause(pri->pri,p->call, 0, 1, cause);

        //������� ��������� � ���������� ���� � ����� (�������������� ��� ������)
        p->progress = 1;
    }
    PORT_MUTEX_UNLOCK(&pri->lock);
    return(0);
}

/**
 * ����� �������� �� ������� ��
 *
 * \param trunkno   ����� ������ E1 (0-3)
 * \param channel   ����� ������ � ������ E1 (0-3) (�� ���� ��� setup)
 * \param owner     ��� �������, ��� ����� ���������� ������
 * \param cause     �������
 *
 * \return 
 */
int pri_call_hangup(int trunkno, int channel, int owner, int cause)
{
    int chanpos;
    struct pri_trunk *pri;
    struct pri_bchan_pvt *p;

    if(!PRI_IS_WORKING)
    { return(-1); }
    
    pri=pri_trunks+trunkno;
    chanpos = pri_find_principle(pri,channel);
    if (chanpos<0)
    { return(-1); }
    p=pri->pvts[chanpos];
    if(!p->call)
    { return(-1); }
    PORT_MUTEX_LOCK(&pri->lock);
    if(p->owner) p->owner=0;
    pri_hangup(pri->pri, p->call, cause);
    p->alreadyhungup = 1;
    PORT_MUTEX_UNLOCK(&pri->lock);
    return(0);
}

/**
 * ����� �� PRI
 *
 * \param trunkno   ����� ������ E1 (0-3)
 * \param channel   ����� ������ � ������ E1 (0-3) (�� ���� ��� setup)
 * \param owner     ��� �������, ��� ����� ���������� ������
 * \param dest      ��������� ����� 
 * \param s         numcomplete (nomer B ���� ==1/==0 ��� �� ���� ����� ����� �����)
 *
 * \return 
 */
int pri_call_info(int trunkno, int channel, int owner, char *dest,int s)
{
    int chanpos;
    struct pri_trunk *pri;
    struct pri_bchan_pvt *p	;
    int x, len;
    char ukz;


    if(!PRI_IS_WORKING)
    { return(-1); }
    
    pri = pri_trunks+trunkno;
    if(!channel)
    {
        for (x = 0; x < pri->numchans; x++) 
        {
            if (!pri->pvts[x]) continue;
            if ((pri->pvts[x]->owner == owner) && (pri->pvts[x]->call)) 
            {
                channel = x;
                p=pri->pvts[channel];
                break;
            }
        }
    }
    else
    {
        chanpos = pri_find_principle(pri, channel);
        if(chanpos < 0) 
        { return(-1); }
        p=pri->pvts[chanpos];
    }
    if(!p->call)
    { return(-1); }
    if(p->owner!=owner)
    { return(-1); }
    PORT_MUTEX_LOCK(&pri->lock);
    pri_copy_string(p->dialdest_overlap, dest, sizeof(p->dialdest_overlap));
    p->sending_complite=s;
    if (p->setup_ack) 
    {
        len = strlen(p->dialdest_overlap);
        ukz = pri_skip_nonblanks(p->dialdest);
        for (x = 0; x < len; x++) 
        {
            pri_information(pri->pri, p->call, p->dialdest_overlap[x]);
			p->dialdest[ukz]= p->dialdest_overlap[x];
			ukz++;
        }

    } 
    PORT_MUTEX_UNLOCK(&pri->lock);
    return(0);
}

#endif

