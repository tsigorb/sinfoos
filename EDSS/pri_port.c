#ifdef __ISDN__

// ���������� �������������� �������
#include "pri_port.h"
#include "pri_debug.h"

#include <string.h>
#include <ctype.h>

void *pvPortMalloc( size_t xSize );
void vPortFree( void *pv );

#if !defined HAVE_STRCASECMP
//��������� ���� ����� ��� ����� ��������
int strcasecmp (const char *s1, const char *s2)
{
  while((*s1!='\0') && (*s2!='\0') && (tolower(*s1)==tolower(*s2)))
  { s1++; s2++; }
  return tolower(*((unsigned char *)s1))-tolower((*(unsigned char *)s2));
}
#endif

#ifdef __FREERTOS_USED__
#include "../FreeRTOS/FreeRTOS.h"
#include "../FreeRTOS/task.h"

#ifndef HAVE_GETTIMEOFDAY
// TODO: ��������� ����������� �������� �������
int gettimeofday(struct timeval *tv, struct timezone *tz)
{
  portTickType t=xTaskGetTickCount();
  if(tv)
  {
    tv->tv_sec=(long)(t / 1000);
    tv->tv_usec=(long)(t % 1000) * 1000;        
  }
  return 0;
}
#endif /* HAVE_GETTIMEOFDAY */

#if defined __ISDN_DEBUG__

void SendStr2IpLogger(const char *fmt,...);

void pri_dbg_udp_message(char *msg)
{ // ����� ���������� ���������
  if(msg && strlen(msg)) SendStr2IpLogger("$ISDN_EVENT$%s",msg);
}

#else
void pri_dbg_udp_message(char *msg)
{
}
#endif

#if defined(__DEBUG_PORT_MALLOC__)

#include <stdio.h>
static char msg[256];
static unsigned int malloced = 0;
static unsigned int freed = 0;
#endif /* _DEBUG_PORT_MALLOC__ */


/**
 * ������������� size ������. ������ ��� ���� �� ���������.
 * 
 * \return ��������� �� ��������������� ������
 *
 * \������������� ������ �� SDRAM.
 */
void *pvPortBigMalloc(size_t size)
{
    void *ptr;
    
    ptr = pvPortMalloc(size);
/*
#ifdef __DEBUG_PORT_MALLOC__
    vTaskSuspendAll();
    malloced += 1;
    sprintf(msg, "pvPortBigMalloc: p:%X size:%d total:%d m:%d f:%d\r", ptr, size, malloced - freed, malloced, freed);
    pri_dbg_udp_message(msg);
    xTaskResumeAll();
#endif
*/
    return ptr;
}


/**
 * ������������� ������ ��� ������� �������� nmemb, ������ ������� �������� ����� size ������.
 * ������ ��� ���� ���������.
 * 
 * \return ��������� �� ��������������� ������
 *
 * \������������� ������ �� SDRAM.
 */
void *pvPortBigCalloc(size_t nmemb, size_t size)
{
    void *ptr;
    size_t bytes = nmemb * size;
    
    ptr=pvPortMalloc(bytes);
    if(ptr!=NULL) memset(ptr, 0, bytes);
/*
#ifdef __DEBUG_PORT_MALLOC__ 
    vTaskSuspendAll();
    malloced += 1;
    sprintf(msg, "pvPortBigCalloc: p:%X size:%d total:%d m:%d f:%d\r", ptr, bytes, malloced - freed, malloced, freed);
    pri_dbg_udp_message(msg);
    xTaskResumeAll();
#endif
*/
    return ptr;
}
/**
 * ����������� ����� � ������, �� ������� ��������� pv, �������������� �������� pvPortBigMalloc ��� pvPortBig�alloc.
 * 
 */
void vPortBigFree(void *pv)
{
#ifdef __DEBUG_PORT_MALLOC__     
    size_t size;
#endif

#ifdef __DEBUG_PORT_MALLOC__         
        size = *(int *)((int)pv-8);
#endif

    vPortFree(pv);
/*
#ifdef __DEBUG_PORT_MALLOC__ 
    vTaskSuspendAll();
    freed += 1;
    sprintf(msg, "pvPortBigFree: p:%X size:%d total:%d m:%d f:%d\r", pv, size, malloced - freed, malloced, freed);
    pri_dbg_udp_message(msg);
    xTaskResumeAll();
#endif
*/
}

/**
 * ������������� ������ ��� ������� �������� nmemb, ������ ������� �������� ����� size ������.
 * ������ ��� ���� ���������.
 * 
 * \return ��������� �� ��������������� ������
 *
 * \������������� ������ �� SRAM. �������� ����� ����������.
 */
void *pvPortCalloc(size_t nmemb, size_t size)
{
    void *ptr;
    size_t bytes = nmemb * size;
    
    ptr = pvPortMalloc(bytes);

    if (ptr != NULL) memset(ptr, 0, bytes);
/*
#ifdef __DEBUG_PORT_MALLOC__    
    sprintf(msg, "pvPortCalloc: p:%X size:%d\r", ptr, bytes);
    pri_dbg_udp_message(msg);
#endif
*/
    return ptr;
}
#endif /* __FREERTOS_USED__ */

#ifdef _WINDOWS
#include <windows.h>

#ifndef HAVE_GETTIMEOFDAY

#include <time.h>

#define EPOCHFILETIME (116444736000000000i64)

struct timezone {
    int tz_minuteswest; /* minutes W of Greenwich */
    int tz_dsttime;     /* type of dst correction */
};

/**
 *   Windows gettimeofday
 */
int gettimeofday(struct timeval *tv, struct timezone *tz)
{

    FILETIME        ft;
    LARGE_INTEGER   li;
    __int64         t;
    static int      tzflag;

    if (tv) 
    {
	    GetSystemTimeAsFileTime(&ft);
	    li.LowPart  = ft.dwLowDateTime;
	    li.HighPart = ft.dwHighDateTime;
	    t  = li.QuadPart;       /* In 100-nanosecond intervals */
	    t -= EPOCHFILETIME;     /* Offset to the Epoch time */
	    t /= 10;                /* In microseconds */
	    tv->tv_sec  = (long)(t / 1000000);
	    tv->tv_usec = (long)(t % 1000000);
    }

    if (tz) 
    {
	    if (!tzflag) 
        {
	        _tzset();
	        tzflag++;
	    }
	    tz->tz_minuteswest = _timezone / 60;
	    tz->tz_dsttime = _daylight;
    }

    return 0;
}

#endif /* HAVE_GETTIMEOFDAY */

#include <stdio.h>

unsigned char dbgbuffer[4096];
unsigned char *dbgdata = dbgbuffer;

void pri_dbg_udp_message(char *msg)
{
    fputs(msg, stdout);
}

void DbgEnterCriticalSection(LPCRITICAL_SECTION lock)
{

/*    if (lock->LockCount < -2)
        printf("lock->LockCount: %d", lock->LockCount);*/

    EnterCriticalSection(lock);

/*    if (lock->LockCount < -2)
        printf("lock->LockCount: %d", lock->LockCount);*/
}

void DbgInitializeCriticalSection(LPCRITICAL_SECTION lock)
{
    InitializeCriticalSection(lock);
}

void DbgLeaveCriticalSection(LPCRITICAL_SECTION lock)
{
    LeaveCriticalSection(lock);
}
#endif // defined(_WINDOWS)

#endif

