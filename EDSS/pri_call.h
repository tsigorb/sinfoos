//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

// ������� �� ����� ����� ��������� ��� PRI

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


/**
 *  ������ �� PRI
 *
 * \param src   ��� �������� ��� ����� ���������� ������ 
 * \param �     ����� ������ E1 (0-3)
 * \param dest  ��������� ����� � ������� type_numberb;
 * \param s     numcomplete (nomer B ���� ==1/==0 ��� �� ���� ����� ����� �����)
 * \param cid   ����� ��������� � ������� type_numbera
 * \param cname ��� ��������� 
 *
 * \return      ����� ������
 *
 * \note ���� ���� ������������� �� ����� �������� ��� ������������� � �� ����� ������� 	redirect_reason, cid_rdnis
 */
int pri_call_setup(int	src, int trunkno, char *dest,int s, char *cid, char *cname);

/**
 * ���������� ������� �������� � ��� ������� �����
 *
 * \param trunkno   ����� ������ E1 (0-3)
 * \param channel   ����� ������ � ������ E1 (0-3) (�� ���� ��� setup)
 * \param owner     ��� �������, ��� ����� ���������� ������
 *
 * \return 
 */
int pri_call_alert(int trunkno, int channel, int owner);



/**
 * ���������� ������� �������� � ��� ������� �����
 *
 * \param trunkno   ����� ������ E1 (0-3)
 * \param channel   ����� ������ � ������ E1 (0-3) (�� ���� ��� setup)
 * \param owner     ��� �������, ��� ����� ���������� ������
 *
 * \return 
 */
int pri_call_answer(int trunkno, int channel, int owner);


/**
 * ���������� ������� �� ����� ���� ������ �� ������� ��
 *
 * \param trunkno   ����� ������ E1 (0-3)
 * \param channel   ����� ������ � ������ E1 (0-3) (�� ���� ��� setup)
 * \param owner     ��� �������, ��� ����� ���������� ������
 * \param cause     ������� �����, ������������ ����� � �.�. ������ ���� ������
 *
 * \return 
 */
int pri_call_busy(int trunkno, int channel, int owner,int cause);


/**
 * ����� �������� �� ������� ��
 *
 * \param trunkno   ����� ������ E1 (0-3)
 * \param channel   ����� ������ � ������ E1 (0-3) (�� ���� ��� setup)
 * \param owner     ��� �������, ��� ����� ���������� ������
 * \param cause     �������
 *
 * \return 
 */
int pri_call_hangup(int trunkno, int channel, int owner,int cause);

/**
 * ����� �� PRI
 *
 * \param trunkno   ����� ������ E1 (0-3)
 * \param channel   ����� ������ � ������ E1 (0-3) (�� ���� ��� setup)
 * \param owner     ��� �������, ��� ����� ���������� ������
 * \param dest      ��������� ����� 
 * \param s         numcomplete (nomer B ���� ==1/==0 ��� �� ���� ����� ����� �����)
 *
 * \return 
 */
int pri_call_info( int trunkno, int channel, int owner, char *dest,int s);

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

// ������� �� PRI � ���� �������

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


/**
 * ������� �������� ����� � EDSS
 *
 * \param trunkno       ����� ������ E1 (0-3)
 * \param channel       ����� ������ � ������ E1 (0-3) (�� ���� ��� setup) 
 * \param cid_num       ����� ���� ��� ������ (�������������)
 * \param cid_name      ��� ���� ��� ������ (�������������)
 * \param rdnis         ����� ���������������� (�������������)
 * \param callednum     ����� ���� ������
 * \param complete		Have we seen "Complete" i.e. no more number?
 *
 * \note Some PRI circuits are set up to send _no_ digits.  Handle them as 's'.
 *          callednum[0] = 's';
 *          callednum[1] = '\0';
 *
 * \return ��� ������ ����� �������� ������� int  owner- ��� ������� ��� ����� ���������� ������
 *  ���  0 - ����� ������ �� �������
 *	
 */
int pri_call_on_incoming(int trunkno, int channel, char *cid_num, char *cid_name, char *rdnis, char *callednum,	int complete);



/**
 * �������� �������������� ����� � EDSS
 *
 * \param trunkno       ����� ������ E1 (0-3)
 * \param channel       ����� ������ � ������ E1 (0-3) (�� ���� ��� setup) 
 * \param owner         ������� �������� �� ����� �� ������ � � �������� � pri_call_alert()
 * \param dest          ��������� ����� char[256]
 *
 * \return  ��� ������ ��� ����� ������ �� ������� (������������ ����� � �� ����� �������)
 *
 */
int pri_call_on_info(int trunkno, int channel, int owner, char *dest);


/**
 * ������� Alert � EDSS (���������� ������� ��������-����� �������� �������)(����� ���������� ���� �� ��� ����� ��� inband==1)
 *
 * \param trunkno       ����� ������ E1 (0-3)
 * \param channel       ����� ������ � ������ E1 (0-3) (�� ���� ��� setup) �� ����� ���� ������������ � � ���� ���������� ������ ������ 
 * \param owner         ������� �� �������� ������ pri_call_setup
 * \param inband        �������� ����� ������������ ��� ���
 *
 * \return  ��� ������ ��� ����� ������ �� ������� (������������ ����� � �� ����� �������)
 *
 */
int pri_call_on_alert(int trunkno, int channel, int owner, int inband);



/**
 * ������� Progress � EDSS (���������� ������� �����-����� ���������� ���� �� ��� ����� ������ inband==1)
 *
 * \param trunkno       ����� ������ E1 (0-3)
 * \param channel       ����� ������ � ������ E1 (0-3) (�� ���� ��� setup) �� ����� ���� ������������ � � ���� ���������� ������ ������ 
 * \param owner         ������� �� �������� ������ pri_call_setup
 * \param cause         ������� �����, ������������ ����� � �.�. ������ ���� ������
 * \param inband        �������� ����� ������������ ��� ���
 *
 * \return  ��� ������ ��� ����� ������ �� ������� (������������ ����� � �� ����� �������)
 *
 */
int pri_call_on_progress(int trunkno, int channel, int owner, int cause, int inband);



/**
 * �������� ���������  � EDSS (����� ���������� ���� �� ��� ����� ��� ��� ���-�� ��������)
 *
 * \param trunkno       ����� ������ E1 (0-3)
 * \param channel       ����� ������ � ������ E1 (0-3) (�� ���� ��� setup) �� ����� ���� ������������ � � ���� ���������� ������ ������ 
 * \param owner         ������� �� �������� ������ pri_call_setup
 *
 * \return  ��� ������ ��� ����� ������ �� ������� (������������ ����� � �� ����� �������)
 *
 */
int pri_call_on_speaker_enable(int trunkno, int channel, int owner);



/**
 * ������� ����� � EDSS (���������� ������� - ���������� ����
 *
 * \param trunkno       ����� ������ E1 (0-3)
 * \param channel       ����� ������ � ������ E1 (0-3) (�� ���� ��� setup) �� ����� ���� ������������ � � ���� ���������� ������ ������ 
 * \param owner         ������� �� �������� ������ pri_call_setup
 *
 * \return  ��� ������ ��� ����� ������ �� ������� (������������ ����� � �� ����� �������)
 *
 */
int	pri_call_on_answer(int trunkno, int channel,int owner);



/**
 * ������� ����� � EDSS (���������� ������� - ������� ������ � �� ����� ������� ������
 *
 * \param trunkno           ����� ������ E1 (0-3)
 * \param channel           ����� ������ � ������ E1 (0-3) (�� ���� ��� setup) �� ����� ���� ������������ � � ���� ���������� ������ ������ 
 * \param owner             ������� �� �������� ������ pri_call_setup
 * \param inbanddisconnect  ������� � ������ �������(TRUE)/������ ��������� � ������������ ������ E1(FALSE)
 * \param cause             ������� �����, ������������ ����� � �.�. ������ ���� ������
 *
 * \return  ��� ������ ��� ����� ������ �� ������� (������������ ����� � �� ����� �������)
 *
 */
int pri_call_on_disconnect(int trunkno, int channel,int owner, unsigned int inbanddisconnect,int cause);


/**
 * ������� ����� � EDSS (����� ��� ���������)
 *
 * \param trunkno           ����� ������ E1 (0-3)
 * \param channel           ����� ������ � ������ E1 (0-3) (�� ���� ��� setup) �� ����� ���� ������������ � � ���� ���������� ������ ������ 
 * \param owner             ������� �� �������� ������ pri_call_setup
 * \param cause             ������� �����, ������������ ����� � �.�. ������ ���� ������
 *
 * \return  ��� ������ ��� ����� ������ �� ������� (������������ ����� � �� ����� �������)
 *
 */
int pri_call_on_release(int trunkno, int channel,int owner,int cause);
