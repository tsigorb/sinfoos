/*
 * Asterisk -- A telephony toolkit for Linux.
 */

#ifndef _ASTERISK_COMPILER_H
#define _ASTERISK_COMPILER_H

#if __GNUC__ < 2 || (__GNUC__ == 2 && __GNUC_MINOR__ < 96)
#define __builtin_expect(exp, c) (exp)
#endif

#endif /* _ASTERISK_COMPILER_H */
