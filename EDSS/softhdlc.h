/**
 * ����������� ���������� HDLC
 */

#if !defined (__SOFTHDLC_H__)
#define __SOFTHDLC_H__

#include "softhdlc_port.h"




#define HDLC_FRAME_MAX_SIZE		1024
#define HDLC_BUFS_MAX_NUM		16

#define HDLC_EVENTBUF_MAX_SIZE      64

struct hdlc_frame_buffer
{
    unsigned char   data[HDLC_FRAME_MAX_SIZE];
    int             size;
    int             offset;
};

struct hdlc_event_buffer
{
    unsigned int buffer[HDLC_EVENTBUF_MAX_SIZE];
    int head;
    int tail;
};

struct soft_hdlc_state 
{
    int state;		    /* ������� ��������� */
    unsigned int data;	/* ������� ������ */
    int bits;		    /* ���������� ��� � ������� */
    int ones;		    /* ���������� ������ */
    unsigned int minbits;
};




struct hdlc_chan
{

    PORT_CRITICAL_SECTION  lock;

    int id;
    struct hdlc_frame_buffer rdbuf[HDLC_BUFS_MAX_NUM];
    int inrdbuf;
    int outrdbuf;


    struct hdlc_frame_buffer wrbuf[HDLC_BUFS_MAX_NUM];
    int inwrbuf;
    int outwrbuf;

    PORT_SEMAPHORE              rdsem;
    PORT_SEMAPHORE              wrsem;
    

    int blocksize;		        /**< ������ ������ ��� ������ ������ */
    int numbufs;		        /**< ���������� ������� ��� ������ ������*/

    struct hdlc_event_buffer   events; /**< ��������� ����� ��� ��������� */

    struct soft_hdlc_state txhdlc;
    struct soft_hdlc_state rxhdlc;
    unsigned short infcs;

    int     isopened:1;
    int     rxoverrun:1;
    int     nonblock:1;
};


#define HDLC_MODE_NONBLOCK      (1<<0)

/* ���� ������� � ������ ������� */
#define HDLC_EVENT_NONE         0
#define HDLC_EVENT_BADFCS       1   /**< HDLC Bad FCS */
#define HDLC_EVENT_ABORT        2   /**< HDLC Abort frame */
#define HDLC_EVENT_OVERRUN      3   /**< HDLC frame overrun */
#define HDLC_EVENT_ALARM        4   /**< ������ ������*/
#define HDLC_EVENT_NOALARM      5   /**< ��� ����� ������ (����� ��������� ������)*/


/* ���� ������������ ������ */
#define HDLC_E_EVENT            -1 /**< ���� �������������� ������� � ������*/
#define HDLC_E_AGAIN            -2 /**< ��� ������, �������� ���*/
#define HDLC_E_TIMEOUT          -3 /**< ������� ����� �������� */
#define HDLC_E_INVALID          -2 /**<  */

/* ���� ��������� ������� */
#define HDLC_WAIT_READ          (1 << 0)
#define HDLC_WAIT_WRITE         (1 << 1)


#define TRUNK_ALARM_LOS        (1 << 8) /* Loss of Signal */
#define TRUNK_ALARM_LFA        (1 << 9) /* Loss of Frame Alignment */

void soft_hdlc_init();
void soft_hdlc_chan_init(struct hdlc_chan *chan, unsigned int mode);
void soft_hdlc_chan_uninit(struct hdlc_chan *chan);

void soft_hdlc_chan_alarm(struct hdlc_chan *chan, int alarms);
int soft_hdlc_chan_put_data(struct hdlc_chan *chan, const unsigned char* rxb, unsigned int size);
int soft_hdlc_chan_get_data(struct hdlc_chan *chan, unsigned char* txb, unsigned int size);
int soft_hdlc_chan_read(struct hdlc_chan *chan, unsigned char *rxb, unsigned int count);
int soft_hdlc_chan_write(struct hdlc_chan *chan, const unsigned char *txb, unsigned int count);
int soft_hdlc_chan_read_event(struct hdlc_chan *chan);
//int soft_hdlc_chan_wait_what(struct hdlc_chan *chan, int what, unsigned long timeout);
unsigned int soft_hdlc_chan_wait_multiple(struct hdlc_chan *chans[], unsigned int chcount,  int what, unsigned long timeout);
unsigned int soft_hdlc_chan_wait(struct hdlc_chan *chan, unsigned long timeout);

#endif // !defined (__SOFTHDLC_H__)
