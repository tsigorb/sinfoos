#if !defined(__PRI_CONF_H__)
#define __PRI_CONF_H__

#include "libpri.h"
#include "softhdlc.h"
#include "pri_port.h"
#include "pri_mutex.h"

#define NUM_TRUNKS 		4	// 2 ��� 4 ������ E1
#define NUM_DCHANS		1	// No more than 4 d-channels
#define MAX_CHANNELS	32  // No more than a DS3 per trunk group
 
#define PRI_MAX_EXTENSION	80	/*!< Max length of an extension */
#define PRI_MAX_CONTEXT		80	/*!< Max length of a context */

#define FROM_BEGIN_FIRST	1		//����� ������ - ������ �������
#define FROM_END_FIRST		2		//����� ������ - ������ ������
#define FROM_BEGIN_NEXT		3		//����� ������ - ������� � ��������� (1,2, ...)
#define FROM_END_NEXT		4		//����� ������ - ������ � ���������	(31,30,29,...)

// ������������ ��� ������ pri
struct pri_chan_config 
{
    int nodetype;
    int switchtype;
    int channels_map[MAX_CHANNELS]; // ����� ������� ����������\�������������\���������� �-������: 0-��������, 1-� �����,
                                    // 2-�� ������������, 3...-���-�� ���
    int type_number_a;
    int type_number_b;
    int number_plan_a;
    int number_plan_b;
    int presentation_a;
    int overlap;            /**< ��������� ������ � ����������� ��� ��� ��� ��������� ��� ��� �������� ������� ��������������. */
    int type_search;        /**< ��� ������ ������� �������� ������������� � ������������ � ������������� ���� ���������� */
    unsigned int priexclusive; 
    unsigned int priindication;    /**< -��������� � ������� ������(inband=0, ��� ��� ��������������)��� ��� (������ ��������� outofband=1)*/
    unsigned int inbanddisconnect; /**< ������� � ������ �������(TRUE)/������ ��������� � ������������ ������ E1(FALSE) */
    unsigned int NUM_DIGIT_in;     /**���������� ���� ����������� ��� �� �������� ����� */
};

struct pri_trunk
{
    //	pthread_t master;           /**< Thread of master */
    PORT_MUTEX lock;                /**< Mutex */
    int nodetype;                   /**< Node type */
    int switchtype;	                /**< Type of switch to emulate */
    int nsf;                        /**< Network-Specific Facilities */
    int dialplan;                   /**< Dialing plan */
    int localdialplan;              /**< Local dialing plan */
    char internationalprefix[10];   /**< country access code ('00' for european dialplans) */
    char nationalprefix[10];        /**< area access code ('0' for european dialplans) */
    char localprefix[20];           /**< area access code + area code ('0'+area code for european dialplans) */
    char privateprefix[20];         /**< for private dialplans */
    char unknownprefix[20];         /**< for unknown dialplans */
    int dchannels[NUM_DCHANS];      /**< What channel are the dchannels on */
    int numchans;                   /**< Num of channels we represent */
    int overlapdial;                /**< In overlap dialing mode */
    int facilityenable;             /**< Enable facility IEs */
    int	type_search_chan;           /**< ������ ������ ������� */

    struct pri *dchans[NUM_DCHANS]; /**< Actual d-channels */
    int dchanavail[NUM_DCHANS];     /**< Whether each channel is available */
    struct pri *pri;                /**< Currently active D-channel */

    /** \brief TRUE if to dump PRI event info (Tested but never set) */
    int debug;

    /** \brief Span number put into user output messages */
    int span;
    /** \brief TRUE if span is being reset/restarted */
    int resetting;
    /** \brief Current position during a reset (-1 if not started) */
    int resetpos;
    /** \brief  ��������� ������� ����� */
    int lastchan;  
    unsigned int inbanddisconnect:1;				/*!< Should we support inband audio after receiving DISCONNECT? */
    /*! TRUE if we have already whined about no D channels available. */
    unsigned int no_d_channels:1;
    time_t lastreset;						/*!< time when unused channels were last reset */
    long resetinterval;						/*!< Interval (in seconds) for resetting unused channels */
    struct pri_bchan_pvt *pvts[MAX_CHANNELS];				/*!< Member channel pvt structs */
    struct hdlc_chan *hdlc_chan;            /**< ��������� �� hdlc ���������� */
#if defined(__FREERTOS_USED__)
    xTaskHandle htask;
#endif    
};

struct pri_bchan_pvt 
{
    PORT_MUTEX lock;			/*!< Channel private lock. */

	int	owner;					// ��� ����� ����� !!! ��� ������ � ��������� !!!!

	struct pri_bchan_pvt *master;				/*!< Master to us (we follow their conferencing) */
	int inconference;				/*!< If our real should be in the conference */
	int sig;					/*!< Signalling style */
	/*!
	 * \brief Nonzero if the signaling type is sent over a radio.
	 * \note Set to a couple of nonzero values but it is only tested like a boolean.
	 */
//	struct pri_bchan_pvt *next;				/*!< Next channel in list */
//	struct pri_bchan_pvt *prev;				/*!< Prev channel in list */

	/* flags */
	unsigned int destroy:1;
	/*! \brief TRUE if in the process of dialing digits or sending something. */
	unsigned int dialing:1;
	/*! \brief TRUE if the transfer capability of the call is digital. */
	unsigned int digital:1;
	/*!
	 * \brief TRUE if the channel should be answered immediately
	 * without attempting to gather any digits.
	 */
	unsigned int immediate:1;
	/*! \brief TRUE if in an alarm condition. */
	unsigned int inalarm:1;
	unsigned int unknown_alarm:1;
	/*! \brief TRUE if we originated the call leg. */
	unsigned int outgoing:1;
	/* unsigned int overlapdial:1; 			unused and potentially confusing */
	/*!
	 * \brief TRUE if PRI congestion/busy indications are sent out-of-band.
	 */
	unsigned int priindication_oob:1;
	/*!
	 * \brief TRUE if PRI B channels are always exclusively selected.
	 */
	unsigned int priexclusive:1;
	unsigned int restartpending:1;		/*!< flag to ensure counted only once for restart */
	/*!
	 * \brief TRUE if caller ID is restricted.
	 * \note Set but not used.  Should be deleted.  Redundant with permhidecallerid.
	 */
	unsigned int restrictcid:1;
	/*!
	 * \brief TRUE if call transfer is enabled
	 * \note For FXS ports (either direct analog or over T1/E1):
	 *   Support flash-hook call transfer
	 * \note For digital ports using ISDN PRI protocols:
	 *   Support switch-side transfer (called 2BCT, RLT or other names)
	 */
	unsigned int transfer:1;
	/*!
	 * \brief TRUE if caller ID is used on this channel.
	 * \note PRI spans will save caller ID from the networking peer.
	 */
	unsigned int use_callerid:1;
	/*!
	 * \brief TRUE if we will use the calling presentation setting
	 * from the Asterisk channel for outgoing calls.
	 * \note Only applies to PRI channels.
	 */
	unsigned int use_callingpres:1;
	
	/*! \brief TRUE if channel is alerting/ringing */
	unsigned int alerting:1;
	/*! \brief TRUE if the call has already gone/hungup */
	unsigned int alreadyhungup:1;
	/*!
	 * \brief TRUE if this is an idle call
	 * \note Applies to PRI channels.
	 */
	unsigned int isidlecall:1;
	/*!
	 * \brief TRUE if call is in a proceeding state.
	 * The call has started working its way through the network.
	 */
	unsigned int proceeding:1;
	/*! \brief TRUE if the call has seen progress through the network. */
	unsigned int progress:1;
	/*!
	 * \brief TRUE if this channel is being reset/restarted
	 * \note Applies to PRI channels.
	 */
	unsigned int resetting:1;
	/*!
	 * \brief TRUE if this channel has received a SETUP_ACKNOWLEDGE
	 * \note Applies to PRI channels.
	 */
	unsigned int setup_ack:1;
	/*!
	*\�������� ��� ���� �����!!
	*/
	unsigned int sending_complite:1;
	
	/*! \brief Automatic Number Identification number (Alternate PRI caller ID number) */
	char cid_ani[PRI_MAX_EXTENSION];
	/*! \brief Caller ID number from an incoming call. */
	char cid_num[PRI_MAX_EXTENSION];
	/*! \brief Caller ID Q.931 TON/NPI field values.  Set by PRI. Zero otherwise. */
	int cid_ton;
	/*! \brief Caller ID name from an incoming call. */
	char cid_name[PRI_MAX_EXTENSION];
	/*! \brief Last Caller ID number from an incoming call. */
	char lastcid_num[PRI_MAX_EXTENSION];
	/*! \brief Last Caller ID name from an incoming call. */
	char lastcid_name[PRI_MAX_EXTENSION];
	/*! \brief Call waiting number. */
//	char callwait_num[PRI_MAX_EXTENSION];
	/*! \brief Call waiting name. */
//	char callwait_name[PRI_MAX_EXTENSION];
	/*! \brief Redirecting Directory Number Information Service (RDNIS) number */
	char rdnis[PRI_MAX_EXTENSION];
	/*! \brief Dialed Number Identifier */
	char dnid[PRI_MAX_EXTENSION];
	/*! \brief Extension to use in the dialplan. *///����� � ���� ������!!!
	char exten[PRI_MAX_EXTENSION];
//	int	len_N_B;		//����� ������ �
	int cause;   // ������� ���������� ��� �� ��������

	int channel;					/*!< Channel Number or CRV */
	int span;					/*!< Span number */
//	time_t guardtime;				/*!< Must wait this much time before using for new call */
	int callingpres;				/*!< The value of callling presentation that we're going to use when placing a PRI call */

	/*!
	 * \brief Number of most significant digits/characters to strip from the dialed number.
	 * \note Feature is deprecated.  Use dialplan logic.
	 * \note The characters are stripped before the PRI TON/NPI prefix
	 * characters are processed.
	 */
//	int stripmsd;
	int callprogress;
	/*! \brief Accumulated call forwarding number. */
	char dialdest[PRI_MAX_EXTENSION];
	/*! \brief Delayed dialing for E911.  Overlap digits for ISDN. */
	char dialdest_overlap[PRI_MAX_EXTENSION];
	/*! \brief PRI control parameters */
	struct pri_trunk *pri;
	/*! \brief Opaque call control structure */
	q931_call *call;
};

#define PRI_CHANNEL(p) ((p) & 0xff)
#define PVT_TO_CHANNEL(p) ((p)->channel)
#define PRI_TRUNK(p) (((p) >> 8) & 0xff)

extern struct pri_chan_config pri_chan_config[NUM_TRUNKS];
extern struct pri_trunk pri_trunks[NUM_TRUNKS];

#endif /* !defined(__PRI_CONF_H__) */

