#if !defined(__SOFTHDLC_PORT_H__)
#define __SOFTHDLC_PORT_H__


#define __FREERTOS_USED__

/* OS specific*/
#ifdef __FREERTOS_USED__
    #include "../FreeRTOS/FreeRTOS.h" 
    #include "../FreeRTOS/task.h"
    #include "../FreeRTOS/semphr.h"

    #define PORT_INVALID_SEMAPHORE              ((xSemaphoreHandle)0)
    #define PORT_SEMAPHORE                      xSemaphoreHandle
    #define PORT_SEMAPHORE_INIT(s)              vSemaphoreCreateBinary(*(s))
    #define PORT_SEMAPHORE_DELETE(s)            
    #define PORT_SEMAPHORE_TAKE(s, t)           xSemaphoreTakeTm(*(s),(t))
    #define PORT_SEMAPHORE_GIVE(s)              xSemaphoreGive(*(s))

    #define PORT_SEMAPHORE_TAKEN                pdPASS
    #define PORT_SEMAPHORE_TIMEOUT              pdFAIL

    #define PORT_CRITICAL_SECTION              xSemaphoreHandle
    #define PORT_CRITICAL_SECTION_DELETE(cs)   
    #define PORT_CRITICAL_SECTION_INIT(cs)     vSemaphoreCreateBinary(*(cs))
    #define PORT_CRITICAL_SECTION_ENTER(cs)    xSemaphoreTake(*(cs))
    #define PORT_CRITICAL_SECTION_LEAVE(cs)    xSemaphoreGive(*(cs))
//    #define PORT_CRITICAL_SECTION_ENTER(cs)    vPortEnableSwitchTask(false)
//    #define PORT_CRITICAL_SECTION_LEAVE(cs)    vPortEnableSwitchTask(true)
    
    #define INFINITE
    
#else
    #include <windows.h>
    

//     #define PORT_MUTEX                          CRITICAL_SECTION
//     #define PORT_MUTEX_INIT(lock)               InitializeCriticalSection(lock)
//     #define PORT_MUTEX_DELETE(lock)             DeleteCriticalSection(lock)
//     #define PORT_MUTEX_LOCK(lock)               EnterCriticalSection(lock)
//     #define PORT_MUTEX_UNLOCK(lock)             LeaveCriticalSection(lock)


    // ��� ����� ������������ ��� ���������� ��������
    #define PORT_INVALID_SEMAPHORE              (INVALID_HANDLE_VALUE)
    #define PORT_SEMAPHORE                      HANDLE
    #define PORT_SEMAPHORE_INIT(s)              {*(s) = CreateSemaphore(NULL, 0, 1, NULL);}
    #define PORT_SEMAPHORE_DELETE(s)            CloseHandle(*(s))             
    #define PORT_SEMAPHORE_TAKE(s, t)           WaitForSingleObject(*(s), t)
    #define PORT_SEMAPHORE_GIVE(s)              ReleaseSemaphore(*(s), 1, NULL)

    #define PORT_SEMAPHORE_TAKEN                WAIT_OBJECT_0
    #define PORT_SEMAPHORE_TIMEOUT              WAIT_TIMEOUT

    #define PORT_CRITICAL_SECTION               CRITICAL_SECTION
    #define PORT_CRITICAL_SECTION_INIT(cs)      InitializeCriticalSection(cs)
    #define PORT_CRITICAL_SECTION_DELETE(cs)    DeleteCriticalSection(cs)
    #define PORT_CRITICAL_SECTION_ENTER(cs)     EnterCriticalSection(cs)
    #define PORT_CRITICAL_SECTION_LEAVE(cs)     LeaveCriticalSection(cs)

    #define inline

    #define section(x)
#endif


#endif /*!defined(__SOFTHDLC_PORT_H__)*/
