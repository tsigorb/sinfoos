#include "..\Sinfo.h"

#ifdef __ISDN__

#define _CRT_SECURE_NO_DEPRECATE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include "pri_config.h"
#include "pri_debug.h"
#include "pri_helpers.h"
#include "pri_channel.h"
#include "pri_time.h"
#include "pri_call.h"
#include "rose.h"
#include "pri_port.h"
#include "softhdlc.h"
#include "pri_internal.h"
#include "pri_facility.h"

////////////////////////////////////////////////
void SimplePiskPult(unsigned char SN);
extern unsigned short DataSave1[];

////////////////////////////////////////////////
/*
����� ��� ��� �� �����, �.�. ���������� ���� � ������???

void ExecuteCmdFromYuri(void *pvParameters);
xQueueHandle xQueueCmd2Misul;
xQueueHandle xQueueAnswMisul;*/

////////////////////////////////////////////////
static MYFILE stFile;
static HANDLE hFileIn;
static unsigned int MassIn[1024]; // ������ ��� ������ ������

////////////////////////////////////////////////
// ���������� ���� ����� ��� ���� ����� ������ ����� ���������� � ����� ������
//#define __TASK_FOR_EACH_DCHAN__

// define this to send PRI user-user information elements
#undef SUPPORT_USERUSER

// ��������� d ������
#define DCHAN_PROVISIONED (1 << 0)
#define DCHAN_NOTINALARM  (1 << 1)
#define DCHAN_UP          (1 << 2)

#define DCHAN_AVAILABLE	(DCHAN_PROVISIONED | DCHAN_NOTINALARM | DCHAN_UP)


/* Overlap dialing option types */
#define PRI_OVERLAPDIAL_NONE        0
#define PRI_OVERLAPDIAL_OUTGOING    1
#define PRI_OVERLAPDIAL_INCOMING    2
#define PRI_OVERLAPDIAL_BOTH        (PRI_OVERLAPDIAL_INCOMING|PRI_OVERLAPDIAL_OUTGOING)


#if 0
#   define DEFAULT_PRI_DEBUG (PRI_DEBUG_Q931_DUMP | PRI_DEBUG_Q921_RAW  ) //| PRI_DEBUG_Q921_DUMP | PRI_DEBUG_Q921_RAW | PRI_DEBUG_Q921_STATE)
#else
#   define DEFAULT_PRI_DEBUG 0
#endif

//#define MAX_CHANLIST_LEN 80

struct pri_chan_config pri_chan_config[NUM_TRUNKS];
struct pri_trunk pri_trunks[NUM_TRUNKS];
struct hdlc_chan hdlc_ctrl[NUM_TRUNKS];

//static struct pri_bchan_pvt  *iflist = NULL, *ifend = NULL;
#if defined(__FREERTOS_USED__)
static xTaskHandle pxCreatedTaskISDN = NULL;
#endif

// PRIVATE FUNCTION DECLARATION
static int pri_read(struct pri *pri, void *buf, int buflen);
static int pri_write(struct pri *pri, void *buf, int buflen);
static void __pri_message(struct pri *pri, char *msg);

static int pri_is_up(struct pri_trunk *pri);
static struct pri_bchan_pvt *mkintf(int channel, int span, int reloading);
static int start_pri(struct pri_trunk *pri,int reload);

int PRI_IS_WORKING=0;

static int pri_is_up(struct pri_trunk *pri)
{
	int x;
	for (x = 0; x < NUM_DCHANS; x++)
	{
	  if (pri->dchanavail[x] == DCHAN_AVAILABLE) return 1;
	}
	return 0;
}

/**
 * ��������� ���������� � ��������� ������ E1
 * \return 0 ���� ����� ��������, ������ ���
 */
int pri_get_trunk_alarms(int trunk)
{
    return 0;
}

/*bool IsDChannelInAlarm(int iTrunk)
{
  bool bAlarm;
  struct pri_trunk *pri=pri_trunks+iTrunk;

  if(!PRI_IS_WORKING) return(true);
  PORT_CRITICAL_SECTION_ENTER(&pri->lock);
  bAlarm=(pri->dchanavail[0] & DCHAN_NOTINALARM) ? false : true;
  PORT_CRITICAL_SECTION_LEAVE(&pri->lock);
  return(bAlarm);
}*/

void destroy_all_chan(int trunk)
{
	int x;
	for (x = 0; x < MAX_CHANNELS; x++) 
	{
		if (pri_trunks[trunk].pvts[x])  
		{
			if (pri_trunks[trunk].pvts[x]->call)  pri_destroycall(pri_trunks[trunk].pri, pri_trunks[trunk].pvts[x]->call);
			//if (pri_trunks[trunk].pvts[x]->owner)	pri_call_on_release(trunk,pri_trunks[trunk].pvts[x]->channel,pri_trunks[trunk].pvts[x]->owner,0);
			PORT_FREE(pri_trunks[trunk].pvts[x]);
			pri_trunks[trunk].pvts[x]=0;
		}
	}
}

static struct pri_bchan_pvt *mkintf(int channel, int span, int reloading)
{
    int numchans;
    struct pri_bchan_pvt *tmp = NULL;

    if(!(tmp=PORT_CALLOC(1,sizeof(struct pri_bchan_pvt))))
    {
      PRI_DBG("mkintf PORT_CALLOC failed \r");                
      return NULL;
    }
    PORT_CRITICAL_SECTION_INIT(&tmp->lock);
    tmp->channel            = channel;
    tmp->priindication_oob  = pri_chan_config[span].priindication; // �� ������������
    tmp->callprogress       = pri_chan_config[span].presentation_a;

    tmp->pri            = &pri_trunks[span];
    tmp->span           = span;
    tmp->call           = NULL;
    tmp->priexclusive   = pri_chan_config[span].priexclusive;
    tmp->destroy        = 0;
    tmp->use_callerid   = 1;
    tmp->cid_num[0]     = '\0';
    tmp->cid_name[0]    = '\0';
    // !!! ��� ������� inalarm ������ ==1 !!! //if (tmp->pri && !pri_is_up(tmp->pri)) tmp->inalarm = 1;
    tmp->inalarm        = 1;
    numchans=pri_trunks[span].numchans;
    pri_trunks[span].pvts[numchans] = tmp;
    pri_trunks[span].numchans++;
    return tmp;
}

/*
void PrintSizes()
{
    printf("sizeof(struct pri_sr): %d", sizeof(struct pri_sr));
    printf("sizeof(struct q931_party_id): %d", sizeof(struct q931_party_id));
    printf("sizeof(struct q931_party_redirecting): %d", sizeof(struct q931_party_redirecting));
    printf("sizeof(struct q931_party_number): %d", sizeof(struct q931_party_number));
    printf("sizeof(struct rose_msg_invoke): %d", sizeof(struct rose_msg_invoke));
    printf("sizeof(struct rose_msg_result): %d", sizeof(struct rose_msg_result));
    printf("sizeof(struct rose_msg_error): %d", sizeof(struct rose_msg_error));
    printf("sizeof(struct q931_party_address): %d", sizeof(struct q931_party_address));
    printf("sizeof(struct fac_extension_header): %d", sizeof(struct fac_extension_header));
    printf("sizeof(struct rose_message): %d", sizeof(struct rose_message));
    printf("sizeof(struct apdu_callback_data): %d", sizeof(struct apdu_callback_data));
}
*/

void InitISDN(void);
void pri_ctrl_destroy(struct pri *ctrl);

int pri_chan_start(int reload,unsigned char *pMasMaxIsdnChannels)
{
    int x,y;
    int found_d;
    char chFName[17];
	PORT_CRITICAL_SECTION temp;
	xTaskHandle tempTaskISDN;

    //**************************************
	if(reload)
	{
	   for (y = 0; y < NUM_TRUNKS; y++) PORT_CRITICAL_SECTION_ENTER(&pri_trunks[y].lock);
	}
    PRI_IS_WORKING=0;  //�� ��������!!!
    for(x=0;x<NUM_TRUNKS;x++)
    {
      memset(pri_chan_config+x,0,sizeof(struct pri_chan_config));
      strcpy(chFName,"ISDN1.opt");
      chFName[4]+=(char)x;

      hFileIn=CreateFile(chFName, GENERIC_READ,0);
      if(hFileIn>=0)
      {
        ReadFile(hFileIn,MassIn,45*4);
        CloseHandle(hFileIn);
        pri_chan_config[x].nodetype=MassIn[0];
        pri_chan_config[x].switchtype=MassIn[1];
        for (y = 2; y<34; y++) pri_chan_config[x].channels_map[y-2]=MassIn[y];
        pri_chan_config[x].type_number_a=MassIn[y++];
        pri_chan_config[x].type_number_b=MassIn[y++];
        pri_chan_config[x].number_plan_a=MassIn[y++];
        pri_chan_config[x].number_plan_b=MassIn[y++];
        pri_chan_config[x].presentation_a=MassIn[y++];
        pri_chan_config[x].overlap=MassIn[y++];
        pri_chan_config[x].type_search=MassIn[y++]; 
        pri_chan_config[x].priexclusive=MassIn[y++];
        pri_chan_config[x].priindication=MassIn[y++];
        pri_chan_config[x].inbanddisconnect=MassIn[y++];
        pri_chan_config[x].NUM_DIGIT_in=MassIn[y];
      }
      else 
      {
        pri_chan_config[x].channels_map[0] =2;
        pri_chan_config[x].channels_map[16]=2; // ��� D-������!
        pri_chan_config[x].nodetype=PRI_CPE;
        pri_chan_config[x].switchtype=PRI_SWITCH_EUROISDN_E1;
        pri_chan_config[x].overlap=PRI_OVERLAPDIAL_BOTH; 
        pri_chan_config[x].type_search=4;
        pri_chan_config[x].priexclusive=0;
        pri_chan_config[x].NUM_DIGIT_in=6;
      }
    }
    for(y=0;y<NUM_TRUNKS;y++) 
    {
		if(reload)
		{
		  destroy_all_chan(y);
		  temp=pri_trunks[y].lock;
		  tempTaskISDN=pri_trunks[y].htask;
	      for(x=0;x<NUM_DCHANS;x++)
          {
	        if(pri_trunks[y].dchans[x]) pri_ctrl_destroy(pri_trunks[y].dchans[x]);
	      }
		}
		memset(pri_trunks+y,0,sizeof(struct pri_trunk));
		if(reload)
		{
          pri_trunks[y].lock=temp;
          pri_trunks[y].htask=tempTaskISDN;
		}
		else PORT_CRITICAL_SECTION_INIT(&pri_trunks[y].lock);
        for(x=1;x<MAX_CHANNELS;x++) 
        {
            if(pri_chan_config[y].channels_map[x]==1)
            { //D-�����
                pri_trunks[y].dchannels[0]    = x;
                pri_trunks[y].dchanavail[0]   |= DCHAN_PROVISIONED;
                pri_trunks[y].span            = y ;
                pri_trunks[y].resetinterval   = 3600;
                pri_trunks[y].nodetype        = pri_chan_config[y].nodetype;
                pri_trunks[y].switchtype      = pri_chan_config[y].switchtype;
                pri_trunks[y].nsf             = PRI_NSF_NONE;
                pri_trunks[y].dialplan        =  pri_chan_config[y].number_plan_b;
                pri_trunks[y].localdialplan   =  pri_chan_config[y].number_plan_a;
                pri_trunks[y].numchans        = 0;
                pri_trunks[y].overlapdial     = pri_chan_config[y]. overlap ;
                pri_trunks[y].type_search_chan= pri_chan_config[y].type_search;

#ifdef HAVE_PRI_INBANDDISCONNECT
                pri_trunks[y].inbanddisconnect = pri_chan_config[y].inbanddisconnect;
#endif
                pri_trunks[y].facilityenable = 0;
                if(pri_trunks[y].type_search_chan==FROM_BEGIN_NEXT) pri_trunks[y].lastchan = 0;
                else pri_trunks[y].lastchan = pMasMaxIsdnChannels[y]-1;
                break;
            }
        }
        if(pri_trunks[y].dchannels[0])
        {
          if(pMasMaxIsdnChannels[y]<MAX_CHANNELS)
          { // ��� ���������� ��� ������������ ������ 14 b-channels
            for(x=15;x<MAX_CHANNELS;x++)
            {
              if(x!=16) pri_chan_config[y].channels_map[x]=2;
            }
          }
          for(x=1;x<MAX_CHANNELS;x++) 
          {
            if(pri_chan_config[y].channels_map[x]==0) mkintf(x,y,reload);
          }
        }
    }
	if(!reload)
	{
      pri_set_message(__pri_message);    
      pri_set_error(__pri_message);
	}
    //PRI_DBG("soft_hdlc_init()");
    found_d=0;
    for(x = 0; x < NUM_TRUNKS; x++) 
    {
      if(pri_trunks[x].dchannels[0]) 
      { // ������ hdlc �����������
        hdlc_ctrl[x].id = x;
        pri_trunks[x].hdlc_chan = hdlc_ctrl+x;
        if(start_pri(pri_trunks+x,reload)==0) found_d++; //���� ������� D-�����
      }
    }
    if(reload)
    {
      for (y = 0; y < NUM_TRUNKS; y++) PORT_CRITICAL_SECTION_LEAVE(&pri_trunks[y].lock);
      return(0);
    }
    // �������� �������� ������ ��� ���� D �������
    if(found_d && (thISDN==NULL))
    { // ���� ������� D-�����
      PRI_DBG("--- vDChannelsTask started ");    
      if(xTaskCreate(pri_all_dchannel,"Dchan",configMINIMAL_STACK_SIZE+400,NULL,tskIDLE_PRIORITY+3,&thISDN)!=pdPASS)
      { BlockWatchDog(); }
    }
    return 0;
}

static int start_pri(struct pri_trunk *pri,int reload)
{
    int x;
    int i;
#if !defined(__FREERTOS_USED__)
	DWORD dwread;
#endif
    PRI_DBG("start_pri() \r");    

    for (i = 0; i < NUM_DCHANS; i++)
    {
        if (!pri->dchannels[i]) break;

        if (pri_get_trunk_alarms(pri->span)) pri->dchanavail[i] &= ~DCHAN_NOTINALARM;
        else pri->dchanavail[i] |= DCHAN_NOTINALARM;
            
		pri->dchans[i] = pri_new_cb(pri->span, pri->nodetype, pri->switchtype, pri_read, pri_write, pri->hdlc_chan);
        if (pri->dchans[i] == NULL)
        {
            PRI_DBG("pri_new_cb() failed \r");
            return -1;
        }

        pri_set_overlapdial(pri->dchans[i], pri->overlapdial);
        pri_set_inbanddisconnect(pri->dchans[i], pri->inbanddisconnect);
        
        pri_set_nsf(pri->dchans[i], pri->nsf);
        
#ifdef __ISDN_DEBUG__
	    pri_set_debug(pri->dchans[i], DEFAULT_PRI_DEBUG);
#endif

#ifdef PRI_GETSET_TIMERS
		memset(&MassIn, 0, 20*4);
		hFileIn = CreateFile("ISDNTimer.opt", GENERIC_READ,0);
		if (hFileIn >= 0)
		{
			ReadFile(hFileIn,MassIn,17*4);
			CloseHandle(hFileIn);
		}  
        for (x = 0; x < 10; x++)
        {
            if (MassIn[x] != 0) pri_set_timer(pri->dchans[i], PRI_TIMER_T301+x, MassIn[x]*1000);
        }
		if (MassIn[10] != 0) pri_set_timer(pri->dchans[i], PRI_TIMER_T312, MassIn[10]*1000);
		if (MassIn[11] != 0) pri_set_timer(pri->dchans[i], PRI_TIMER_T313, MassIn[11]*1000);
		if (MassIn[12] != 0) pri_set_timer(pri->dchans[i], PRI_TIMER_T314, MassIn[12]*1000);
		if (MassIn[13] != 0) pri_set_timer(pri->dchans[i], PRI_TIMER_T316, MassIn[13]*1000);
		if (MassIn[14] != 0) pri_set_timer(pri->dchans[i], PRI_TIMER_T317, MassIn[14]*1000);
		if (MassIn[15] != 0) pri_set_timer(pri->dchans[i], PRI_TIMER_T321, MassIn[15]*1000);
		if (MassIn[16] != 0) pri_set_timer(pri->dchans[i], PRI_TIMER_T322, MassIn[16]*1000);
#endif
    }
    /* Assume primary is the one we use */
    pri->pri = pri->dchans[0];
    pri->resetpos = -1;

#ifdef __ISDN_DEBUG__
    pri->debug=1; //DEFAULT_PRI_DEBUG;
#endif
    return 0;
}

static int pri_find_dchan(struct pri_trunk *pri)
{
    int x,newslot = -1,oldslot = -1;
    struct pri *old=pri->pri;

    for(x = 0; x < NUM_DCHANS; x++) 
    {
      if((pri->dchanavail[x]==DCHAN_AVAILABLE) && (newslot<0)) newslot = x;
      if(pri->dchans[x]==old) oldslot = x;
    }
    if(newslot < 0) 
    {
      newslot = 0;
      if(!pri->no_d_channels) pri->no_d_channels = 1;
    }
    else pri->no_d_channels = 0;
    pri->pri = pri->dchans[newslot];
    return 0;
}

int pri_find_principle(struct pri_trunk *pri, int channel)
{
	int x;
	int principle = -1;
	channel = PRI_CHANNEL(channel);

	for(x=0;x<pri->numchans;x++) 
    {
		if(pri->pvts[x] && (pri->pvts[x]->channel==channel))  
        {
			principle = x;
			break;
		}
	}
	
	return principle;
}

static int pri_fixup_principle(struct pri_trunk *pri, int principle, q931_call *c)
{
	struct pri_bchan_pvt *pnew,*old;
	int x;
	if (!c) {
		if (principle < 0)
			return -1;
		return principle;
	}
	if ((principle > -1) && 
		(principle < pri->numchans) && 
		(pri->pvts[principle]) && 
		(pri->pvts[principle]->call == c))
		return principle;
	/* First, check for other bearers */
	for (x = 0; x < pri->numchans; x++) {
		if (!pri->pvts[x])
			continue;
		if (pri->pvts[x]->call == c) {
			/* Found our call */
			if (principle != x) {
				pnew = pri->pvts[principle], old = pri->pvts[x];
				if(pri->debug)pri_message(pri->pri,"Moving call from channel %d to channel %d", old->channel, pnew->channel);
				if (pnew->owner) 
                {
				pri_hangup(pri->pri, c, PRI_CAUSE_CHANNEL_UNACCEPTABLE);
//wait rel_comp
// +++		pri_call_on_release(old->span, old->channel,old->owner,PRI_CAUSE_CHANNEL_UNACCEPTABLE);
//				old->owner=NULL;
//				old->call = NULL;
					return -1;
				}
				/* Fix it all up now */
				pnew->owner = old->owner;
				old->owner = 0;	// NULL;
				if (pnew->owner) {

// 
// TODO: �������� ��� ������ ����� �����, (����� �� ��������) �� ����� �������������� � �������� ���������� alert? connect?
//insert func

				} 
				pnew->call = old->call;
				old->call = NULL;

				/* Copy any DSP that may be present */
				pnew->proceeding =old->proceeding  ;
				pnew->alerting =old->alerting;
				pnew->progress =old->progress ;
				pnew->sending_complite=old->sending_complite;	//sending complete

				pnew->dialdest_overlap[0]  = '\0';
				pnew->dialdest[0]  = '\0';
				pnew->dialing      = 1;
				pnew->outgoing     = 1;
				pri_copy_string(pnew->dialdest,old->dialdest, sizeof(pnew->dialdest));
				pri_copy_string(pnew->cid_name, old->cid_name, sizeof(pnew->cid_name));
				pri_copy_string(pnew->cid_num, old->cid_num, sizeof(pnew->cid_num));

			}
			return principle;
		}
	}
	return -1;
}

int pri_find_empty_chan(struct pri_trunk *pri)
{
  int x;
  switch(pri->type_search_chan)
  {
	case FROM_BEGIN_FIRST:  //����� ������ - ������ �������
		for(x=0;x<pri->numchans;x++)
		{
		  if(pri->pvts[x] && !pri->pvts[x]->inalarm && !pri->pvts[x]->owner && !pri->pvts[x]->resetting) return x;
		}
		break;
	case FROM_END_FIRST:  //����� ������ - ������ � �����
		for(x = pri->numchans-1; x>=0; x--)
		{
			if(pri->pvts[x] && !pri->pvts[x]->inalarm && !pri->pvts[x]->owner && !pri->pvts[x]->resetting) return x;
		}
		break;
    case FROM_END_NEXT:  //����� ������ - � ����� � ��������� (MAX_CHANNELS-1,...,1)
        for(x = pri->lastchan-1; x!=pri->lastchan;x--)
        {
			if(x<0)
			{
				x=pri->numchans-1;
				if(pri->lastchan==x) break;
			}
			if(pri->pvts[x] && !pri->pvts[x]->inalarm && !pri->pvts[x]->owner&&!pri->pvts[x]->resetting)
			{
				pri->lastchan=x;
				return x;
			}
		}
		break;        
	case FROM_BEGIN_NEXT: // ����� ������ - ������� � ��������� (1,2, ...)
    default:
		for(x=pri->lastchan+1;x!=pri->lastchan;x++)
		{
			if(x>=pri->numchans)
			{
			  x=0;
			  if(pri->lastchan==x) break;
			}
			if(pri->pvts[x])
			{
			  if(!pri->pvts[x]->inalarm && !pri->pvts[x]->owner && !pri->pvts[x]->resetting)
			  {
				pri->lastchan=x;
				return x;
			  }
			}
		}
		break;        
	}
	return -1;
}

static void apply_plan_to_number(char *buf, size_t size, const struct pri_trunk *pri, const char *number, const int plan)
{
  if(pri_strlen_zero(number))
  { /* make sure a number exists so prefix isn't placed on an empty string */
    if(size) *buf = '\0';
	return;
  }

  switch (plan)
  {
	case PRI_INTERNATIONAL_ISDN:		/* Q.931 dialplan == 0x11 international dialplan => prepend international prefix digits */
		snprintf(buf, size, "%s%s", pri->internationalprefix, number);
		break;
	case PRI_NATIONAL_ISDN:			/* Q.931 dialplan == 0x21 national dialplan => prepend national prefix digits */
		snprintf(buf, size, "%s%s", pri->nationalprefix, number);
		break;
	case PRI_LOCAL_ISDN:			/* Q.931 dialplan == 0x41 local dialplan => prepend local prefix digits */
		snprintf(buf, size, "%s%s", pri->localprefix, number);
		break;
	case PRI_PRIVATE:			/* Q.931 dialplan == 0x49 private dialplan => prepend private prefix digits */
		snprintf(buf, size, "%s%s", pri->privateprefix, number);
		break;
	case PRI_UNKNOWN:			/* Q.931 dialplan == 0x00 unknown dialplan => prepend unknown prefix digits */
		snprintf(buf, size, "%s%s", pri->unknownprefix, number);
		break;
	default:				/* other Q.931 dialplan => don't twiddle with callingnum */
		snprintf(buf, size, "%s", number);
		break;
  }
}

void pri_check_restart(struct pri_trunk *pri)
{
    do
    { pri->resetpos++; }
    while((pri->resetpos < pri->numchans) &&
          (!pri->pvts[pri->resetpos] ||
          pri->pvts[pri->resetpos]->call ||
          pri->pvts[pri->resetpos]->resetting));
    if(pri->resetpos < pri->numchans)
    {
        /* Mark the channel as resetting and restart it */
        pri->pvts[pri->resetpos]->resetting = 1;
        pri_reset(pri->pri, PVT_TO_CHANNEL(pri->pvts[pri->resetpos]));
    }
    else
    {
        pri->resetting = 0;
        time(&pri->lastreset);
    }
}

static void pri_handle_events(struct pri_trunk *pri, char *inrawbuf, int res)
{
	pri_event *e;
	int chanpos = 0;
	int x;
	int c;
	int i, which=-1;
	int ukz ;
	int cause=0;

	e = NULL;
	if(res==HDLC_E_TIMEOUT)
	{
		for(which = 0; which < NUM_DCHANS; which++) 
		{
			if (!pri->dchans[which]) break;
			/* Just a timeout, run the scheduler */
			e = pri_schedule_run(pri->dchans[which]);
			if(e) break;
		}
	} 
	else
	  if (((res > 0) || (res == HDLC_E_EVENT))) 
	  {
		for (which = 0; which < NUM_DCHANS; which++) 
		{
			if (!pri->dchans[which]) break;
			if (res == HDLC_E_EVENT) 
			{
				x = soft_hdlc_chan_read_event(pri->hdlc_chan); // Check for an event
				if (x == HDLC_EVENT_ALARM) 
				{ // Keep track of alarm state
					pri->dchanavail[which] &= ~(DCHAN_NOTINALARM | DCHAN_UP);
					pri_find_dchan(pri);
				} 
				else
				  if (x == HDLC_EVENT_NOALARM) 
				  {
					pri->dchanavail[which] |= DCHAN_NOTINALARM;
					pri_restart(pri->dchans[which]);
				  }
			}
			else
			  if(res>0) e=pri_check_event_buf(pri->dchans[which],inrawbuf,1024); // ������ ���������� �� LIB PRI
			if(e) break;
		}
	  } 
	if(e)
	{
		if (pri->debug)
		{
			pri_message(pri->pri, "TRANK NOM= %d ", pri->span);
			pri_dump_event(pri->dchans[which], e);
		}
		if(e->e!=PRI_EVENT_DCHAN_DOWN) pri->dchanavail[which] |= DCHAN_UP;
		else pri->dchanavail[which] &= ~DCHAN_UP;

		if((e->e != PRI_EVENT_DCHAN_UP) && (e->e != PRI_EVENT_DCHAN_DOWN) && (pri->pri != pri->dchans[which]))
		{ /* Must be an NFAS group that has the secondary dchan active */
			pri->pri = pri->dchans[which];
		}

		switch (e->e)
		{

			case PRI_EVENT_DCHAN_UP:
				pri->no_d_channels = 0;
				if (!pri->pri) pri_find_dchan(pri);

				/* Note presense of D-channel */
				time(&pri->lastreset);

				/* Restart in 5 seconds */
				if (pri->resetinterval > -1) {
					pri->lastreset -= pri->resetinterval;
					pri->lastreset += 5;
				}
				pri->resetting = 0;
				/* Take the channels from inalarm condition */
				for (i = 0; i < pri->numchans; i++)
				{ 
				  if(pri->pvts[i]) pri->pvts[i]->inalarm = 0;
				}
				break;

			case PRI_EVENT_DCHAN_DOWN:
				pri_find_dchan(pri);
				if(!pri_is_up(pri))
//if(pri_is_up(pri)) //!!!
				{
					pri->resetting = 0;
					/* Hangup active channels and put them in alarm mode */
					for (i = 0; i < pri->numchans; i++)
					{
						struct pri_bchan_pvt *p = pri->pvts[i];
						if(p)
						{
							if (!p->pri || !p->pri->pri || pri_get_timer(p->pri->pri, PRI_TIMER_T309) < 0)
							{
								/* T309 is not enabled : hangup calls when alarm occurs */
								if (p->call)
								{
									if (p->pri && p->pri->pri)
									{
										pri_hangup(p->pri->pri, p->call, -1);
										pri_destroycall(p->pri->pri, p->call);
										p->call = NULL;
									} 
								}
								if (p->owner)
								{
									// �������� ��������� ������ ??????
									pri_call_on_release(p->span, p->channel,p->owner,PRI_CAUSE_NETWORK_OUT_OF_ORDER ); //������� ����� ��� ���
									p->owner=0;
								}
							}
							p->inalarm = 1;
						}
					}
				}
				break;

			case PRI_EVENT_RESTART:
//SimplePiskPult(1);
//DataSave1[0]=PRI_EVENT_RESTART;
				if (e->restart.channel > -1) {
					chanpos = pri_find_principle(pri, e->restart.channel);
					if (chanpos >= 0)
					{
						if (pri->pvts[chanpos]->call) {
							pri_destroycall(pri->pri, pri->pvts[chanpos]->call);
							pri->pvts[chanpos]->call = NULL;
						}
						/* Force soft hangup if appropriate */
						if (pri->pvts[chanpos]->owner){
							pri_call_on_release(pri->pvts[chanpos]->span, pri->pvts[chanpos]->channel,pri->pvts[chanpos]->owner,pri->pvts[chanpos]->cause);
							// ������� �����			
							pri->pvts[chanpos]->owner=0;
						}
					}
				} else {
					for (x = 0; x < pri->numchans; x++)
						if (pri->pvts[x]) {
							if (pri->pvts[x]->call) {
								pri_destroycall(pri->pri, pri->pvts[x]->call);
								pri->pvts[x]->call = NULL;
							}
							if (pri->pvts[x]->owner){
								pri_call_on_release(pri->pvts[chanpos]->span, pri->pvts[chanpos]->channel,pri->pvts[chanpos]->owner,pri->pvts[chanpos]->cause);
								// ������� �����			
								pri->pvts[x]->owner=0;
							}
						}
				}
				break;

				//no check
			case PRI_EVENT_KEYPAD_DIGIT:	/* When we receive during ACTIVE state (INFORMATION) */  //������ ��� ��� �� �����
				chanpos = pri_find_principle(pri, e->digit.channel);
				if (chanpos >= 0){
					chanpos = pri_fixup_principle(pri, chanpos, e->digit.call);
					if (chanpos > -1)
					{
						if((pri->overlapdial & PRI_OVERLAPDIAL_INCOMING) &&
						   (pri->pvts[chanpos]->call==e->digit.call) && pri->pvts[chanpos]->owner)
						{
							int digitlen = strlen(e->digit.digits);
							char digit[32];
							int i;
							for (i = 0; i < digitlen; i++) digit[i] = e->digit.digits[i];
							// �������� ������� ����� �� ����� ���������
							pri_call_on_info(pri->pvts[chanpos]->span,pri->pvts[chanpos]->channel,pri->pvts[chanpos]->owner,digit );
							if (pri->debug)	pri_message(pri->pri, "pri_call_on_info inAktiv owner %d ",pri->pvts[chanpos]->owner );
						}
					}
				}
				break;

			case PRI_EVENT_INFO_RECEIVED:		/* Additional info (digits) received (INFORMATION) */
				chanpos = pri_find_principle(pri, e->ring.channel);
				if (chanpos >= 0)
				{
					chanpos = pri_fixup_principle(pri, chanpos, e->ring.call);
					if (chanpos > -1)
					{
						if((pri->overlapdial & PRI_OVERLAPDIAL_INCOMING) &&
						   pri->pvts[chanpos]->call==e->ring.call && pri->pvts[chanpos]->owner)
						{
							int i,digitlen=strlen(e->ring.callednum);
							char digit[32];
							for (i = 0; i < digitlen; i++) digit[i] = e->ring.callednum[i]; // ��� �������� ����� � char exten[PRI_MAX_EXTENSION];
							digit[i] = '\0';
							if(strlen(pri->pvts[chanpos]->exten)<sizeof(pri->pvts[chanpos]->exten)- 1) strcat(pri->pvts[chanpos]->exten, digit);
							if(pri->pvts[chanpos]->owner!=0xFFFF )
							{//  info get ��� ���� ���������� ���������� ���� ������� �� �����
							  pri_call_on_info(pri->pvts[chanpos]->span,pri->pvts[chanpos]->channel,pri->pvts[chanpos]->owner, digit);
							}
							else
							{
 							//	if (pri->debug)	pri_message(pri->pri, "Get %d digit ",strlen(pri->pvts[chanpos]->exten) );
 							//	if (pri->debug)	pri_message(pri->pri, "Waiting %d digit ",pri_chan_config[pri->pvts[chanpos]->span].NUM_DIGIT_in);

								if(strlen(pri->pvts[chanpos]->exten)>=pri_chan_config[pri->pvts[chanpos]->span].NUM_DIGIT_in)
								{ //  incoming call
									c=pri_call_on_incoming(pri->pvts[chanpos]->span, pri->pvts[chanpos]->channel,
									                       pri->pvts[chanpos]->cid_num, pri->pvts[chanpos]->cid_name,
														   pri->pvts[chanpos]->rdnis, pri->pvts[chanpos]->exten,1);
									if(pri->debug)
									{
										pri_message(pri->pri, "pri_call_on_incoming Get owner %d ",c );
					                    for(i=0;i<strlen(pri->pvts[chanpos]->exten); i++)
					                    { pri_message(pri->pri, "%d ",pri->pvts[chanpos]->exten[i]); }
									}
									if (c) pri->pvts[chanpos]->owner = c;
                                    else
                                    {
										pri_hangup(pri->pri, e->ring.call, PRI_CAUSE_SWITCH_CONGESTION);
										pri->pvts[chanpos]->call = NULL;
									}
								}
							}
						}
					}
				}
				break;
			case PRI_EVENT_RING: // Incoming call (SETUP)
				if (e->ring.channel == -1) chanpos = pri_find_empty_chan(pri);
				else chanpos = pri_find_principle(pri, e->ring.channel);
				/* if no channel specified find one empty */
				if(chanpos >= 0)
				{
					if (pri->pvts[chanpos]->owner) {
						if (pri->pvts[chanpos]->call == e->ring.call) {
							// Duplicate setup requested on channel %d/%d already in use on span %d
							break;
						} else {
							/* This is where we handle initial glare */
							// "Ring requested on channel %d/%d already in use or previously requested on span %d.  Attempting to renegotiating channel.
							chanpos = -1;
						}
					}
					//if (chanpos > -1)
				}
				if ((chanpos < 0) && (e->ring.flexible)) chanpos = pri_find_empty_chan(pri);
				if (chanpos > -1) 
				{
					pri->pvts[chanpos]->call = e->ring.call;
					pri->pvts[chanpos]->alreadyhungup = 0;
					pri->pvts[chanpos]->proceeding      = 0;
					pri->pvts[chanpos]->alerting      = 0;
					pri->pvts[chanpos]->progress		=0;
					pri->pvts[chanpos]->outgoing		=0;

					apply_plan_to_number(pri->pvts[chanpos]->cid_num, sizeof(pri->pvts[chanpos]->cid_num), pri,
					                     e->ring.callingnum, e->ring.callingplan);
					if (pri->pvts[chanpos]->use_callerid)
					{
						pri_shrink_phone_number(pri->pvts[chanpos]->cid_num);
						//ANI ������������ � ������� � ��� ���� �����
						//						if (!pri_strlen_zero(e->ring.callingani)) {
						//							apply_plan_to_number(plancallingani, sizeof(plancallingani), pri, e->ring.callingani, e->ring.callingplanani);
						//							pri_shrink_phone_number(plancallingani);
						//							pri_copy_string(pri->pvts[chanpos]->cid_ani, plancallingani, sizeof(pri->pvts[chanpos]->cid_ani));
						//						} else {
						pri->pvts[chanpos]->cid_ani[0] = '\0';
						//						}
						pri_copy_string(pri->pvts[chanpos]->cid_name, e->ring.callingname, sizeof(pri->pvts[chanpos]->cid_name));
						pri->pvts[chanpos]->cid_ton = e->ring.callingplan; /* this is the callingplan (TON/NPI), e->ring.callingplan>>4 would be the TON */
					}
					else
					{
						pri->pvts[chanpos]->cid_num[0] = '\0';
						pri->pvts[chanpos]->cid_ani[0] = '\0';
						pri->pvts[chanpos]->cid_name[0] = '\0';
						pri->pvts[chanpos]->cid_ton = 0;
					}
					apply_plan_to_number(pri->pvts[chanpos]->rdnis, sizeof(pri->pvts[chanpos]->rdnis), pri,
						                 e->ring.redirectingnum, e->ring.callingplanrdnis);
					/* Set DNID on all incoming calls -- even immediate */
					pri_copy_string(pri->pvts[chanpos]->dnid, e->ring.callednum, sizeof(pri->pvts[chanpos]->dnid));
					/* If immediate=yes go to s|1 */
					if (pri->pvts[chanpos]->immediate)
					{
						// "Going to extension s|1 because of immediate=yes");
						pri->pvts[chanpos]->exten[0] = 's';
						pri->pvts[chanpos]->exten[1] = '\0';
					}
					else // Get called number
					  if(!pri_strlen_zero(e->ring.callednum))
					  {
						pri_copy_string(pri->pvts[chanpos]->exten, e->ring.callednum, sizeof(pri->pvts[chanpos]->exten));
					  }
					  else
					    if(pri->overlapdial) pri->pvts[chanpos]->exten[0] = '\0';
					   else
					   { // Some PRI circuits are set up to send _no_ digits.  Handle them as 's'
						 pri->pvts[chanpos]->exten[0] = 's';
						 pri->pvts[chanpos]->exten[1] = '\0';
					   }
					/* No number yet, but received "sending complete"? */
					if (e->ring.complete && (pri_strlen_zero(e->ring.callednum)))
					{ //Going to extension s|1 because of Complete received");
						pri->pvts[chanpos]->exten[0] = 's';
						pri->pvts[chanpos]->exten[1] = '\0';
					}
					/* Make sure extension exists (or in overlap dial mode, can exist) */
					if ((pri->overlapdial & PRI_OVERLAPDIAL_INCOMING)||e->ring.complete )
					{
						if (e->ring.complete || !(pri->overlapdial & PRI_OVERLAPDIAL_INCOMING))
						{
							/* Just announce proceeding */
							pri->pvts[chanpos]->proceeding = 1;
							pri_proceeding(pri->pri, e->ring.call, PVT_TO_CHANNEL(pri->pvts[chanpos]), 0);
						}
						else pri_need_more_info(pri->pri, e->ring.call, PVT_TO_CHANNEL(pri->pvts[chanpos]), 1);
						/* Get the use_callingpres state */
						pri->pvts[chanpos]->callingpres = e->ring.callingpres;

						/* Start PBX */
						if(!e->ring.complete && (pri->overlapdial & PRI_OVERLAPDIAL_INCOMING)) 
						{
						  // Release the PRI lock while we create the channel so other tasks can send D channel messages.
							if(strlen(pri->pvts[chanpos]->exten)>=pri_chan_config[pri->pvts[chanpos]->span].NUM_DIGIT_in)
							{
								// incoming call
								c=pri_call_on_incoming(pri->pvts[chanpos]->span, pri->pvts[chanpos]->channel,
								                       pri->pvts[chanpos]->cid_num, pri->pvts[chanpos]->cid_name,
													   pri->pvts[chanpos]->rdnis, pri->pvts[chanpos]->exten,1);
								if(pri->debug) pri_message(pri->pri, "pri_call_on_incoming Get owner %d ",c );
							}
							else c=0xFFFF; // ---- ����� ����� � ���� �� �������� �����
							if(pri->debug) pri_message(pri->pri, "Wait INFO owner %d", c);
							if(c) pri->pvts[chanpos]->owner=c;
							else
							{
								//SendStr2IpLogger("Unable to start PBX on channel %d/%d, span %d");
								pri_hangup(pri->pri, e->ring.call, PRI_CAUSE_SWITCH_CONGESTION);
								pri->pvts[chanpos]->call = NULL;
							}
						}
						else
						{ // Release the PRI lock while we create the channel so other threads can send D channel messages.
							//  incoming call
							// ������� ����� � (����� ����� ��� ����� ����������)
							c=pri_call_on_incoming(pri->pvts[chanpos]->span,
							                       pri->pvts[chanpos]->channel,pri->pvts[chanpos]->cid_num,
							                       pri->pvts[chanpos]->cid_name,
												   pri->pvts[chanpos]->rdnis, pri->pvts[chanpos]->exten,e->ring.complete);
							if(pri->debug) pri_message(pri->pri, "pri_call_on_incoming Get owner %d ",c );
							if(c) pri->pvts[chanpos]->owner = c;
							else
							{
								//SendStr2IpLogger("Unable to start PBX on channel %d/%d, span %d");
								pri_hangup(pri->pri, e->ring.call, PRI_CAUSE_SWITCH_CONGESTION);
								pri->pvts[chanpos]->call = NULL;
							}
						}
					}
					else
					{
						//SendStr2IpLogger("Extension '%s' in context '%s' from '%s' does not exist.  Rejecting call on channel %d/%d, span %d");
						pri_hangup(pri->pri, e->ring.call, PRI_CAUSE_UNALLOCATED);
						pri->pvts[chanpos]->call = NULL;
						pri->pvts[chanpos]->exten[0] = '\0';
					}
				}
				else
				{
					if (e->ring.flexible) pri_hangup(pri->pri, e->ring.call, PRI_CAUSE_NORMAL_CIRCUIT_CONGESTION);
					else pri_hangup(pri->pri, e->ring.call, PRI_CAUSE_REQUESTED_CHAN_UNAVAIL);
				}
				break;

			case PRI_EVENT_RINGING: /* Call is ringing (ALERTING)get */
//SimplePiskPult(1);
//DataSave1[0]=PRI_EVENT_RINGING;
				chanpos = pri_find_principle(pri, e->ringing.channel);
				if (chanpos >= 0)
				{
					chanpos = pri_fixup_principle(pri, chanpos, e->ringing.call);
					if (chanpos >= 0)
					{
						pri->pvts[chanpos]->alerting = 1;

						if (pri->debug){
							pri_message(pri->pri, "  owner =%d", pri->pvts[chanpos]->owner);
						}

						if (e->ringing.progressmask & PRI_PROG_INBAND_AVAILABLE)
						{
							/* Now we can do call progress detection */
							//  got alert
							pri_call_on_alert(pri->pvts[chanpos]->span, pri->pvts[chanpos]->channel,pri->pvts[chanpos]->owner,1);
							if (pri->debug)	pri_message(pri->pri, "pri_call_on_alert INBAND=1 owner %d ",pri->pvts[chanpos]->owner );
							//	�������� ��� ���������� ������� �������� (���)	
						}
						else 
						{
							pri_call_on_alert(pri->pvts[chanpos]->span, pri->pvts[chanpos]->channel,pri->pvts[chanpos]->owner,0); //no inband info
							if (pri->debug)	pri_message(pri->pri, "pri_call_on_alert INBAND=0 owner %d ",pri->pvts[chanpos]->owner );
						}

#ifdef SUPPORT_USERUSER
						if (!pri_strlen_zero(e->ringing.useruserinfo)) { }
#endif

					}
				}
				break;

			case PRI_EVENT_PROGRESS: /* When we get PROGRESS */
//SimplePiskPult(1);
//DataSave1[0]=PRI_EVENT_PROGRESS;
				/* Get chan value if e->e is not PRI_EVNT_RINGING */
				chanpos = pri_find_principle(pri, e->proceeding.channel);
				if (chanpos > -1)
				{
					if (pri->debug) pri_message(pri->pri, "  owner= %d", pri->pvts[chanpos]->owner);
					if (e->proceeding.cause > -1)  pri->pvts[chanpos]->cause = e->proceeding.cause;
					if (!pri->pvts[chanpos]->progress)
					{
						if (e->proceeding.progressmask & PRI_PROG_INBAND_AVAILABLE)
						{
							/* Bring voice path up */
							//	�������� ��� ���������� ������� ����� �� ��� ����������� (���)
							pri_call_on_progress(pri->pvts[chanpos]->span, pri->pvts[chanpos]->channel,pri->pvts[chanpos]->owner,pri->pvts[chanpos]->cause,1);
							if (pri->debug)	pri_message(pri->pri, "pri_call_on_progress inband=1 owner %d ",pri->pvts[chanpos]->owner );
						}
						else
						{
							pri_call_on_progress(pri->pvts[chanpos]->span, pri->pvts[chanpos]->channel,pri->pvts[chanpos]->owner,pri->pvts[chanpos]->cause,0); //no voice
							if (pri->debug)	pri_message(pri->pri, "pri_call_on_progress inband=0 owner %d ",pri->pvts[chanpos]->owner );
						}
						pri->pvts[chanpos]->progress = 1;
						pri->pvts[chanpos]->dialing = 0;
					}
				}
				break;

			case PRI_EVENT_PROCEEDING:/* When we get CALL_PROCEEDING */
				chanpos = pri_find_principle(pri, e->proceeding.channel);
				if (chanpos > -1) 
				{
					chanpos = pri_fixup_principle(pri, chanpos, e->proceeding.call);
					if (chanpos >= 0)
					{
						if (pri->debug){
							pri_message(pri->pri, "  owner=%d", pri->pvts[chanpos]->owner);
						}
						if (!pri->pvts[chanpos]->proceeding) 
						{
							if (e->proceeding.progressmask & PRI_PROG_INBAND_AVAILABLE) {
								/* Now we can do call progress detection */
								/* Bring voice path up */
								// speaker_on
								// A ����� �� ������ �����? ����� ������� ������� ���������� ����?
								pri_call_on_speaker_enable(pri->pvts[chanpos]->span, pri->pvts[chanpos]->channel,pri->pvts[chanpos]->owner);
								if (pri->debug)	pri_message(pri->pri, "pri_call_on_speaker_enable owner %d ",pri->pvts[chanpos]->owner );
							} //else �������� ������ �� ������ 
							if (pri->debug)	pri_message(pri->pri, " INBAND=0 ",pri->pvts[chanpos]->owner );
							pri->pvts[chanpos]->proceeding = 1;
							pri->pvts[chanpos]->dialing = 0;
						}
					}
				}
				break;

				//not checked

			case PRI_EVENT_FACNAME:	/* Caller*ID Name received on Facility (DEPRECATED) *///QSIG
				chanpos = pri_find_principle(pri, e->facname.channel);
				if (chanpos >= 0){
					chanpos = pri_fixup_principle(pri, chanpos, e->facname.call);
					if (chanpos >= 0)
						if (pri->pvts[chanpos]->use_callerid) 
						{
							/* Re-use *69 field for PRI */
							pri_copy_string(pri->pvts[chanpos]->lastcid_num, e->facname.callingnum, sizeof(pri->pvts[chanpos]->lastcid_num));
							pri_copy_string(pri->pvts[chanpos]->lastcid_name, e->facname.callingname, sizeof(pri->pvts[chanpos]->lastcid_name));
						}
				}
				break;

			case PRI_EVENT_ANSWER:	/* Call has been answered (CONNECT) */
//SimplePiskPult(1);
//DataSave1[0]=PRI_EVENT_ANSWER;
				chanpos = pri_find_principle(pri, e->answer.channel);
				if (chanpos >= 0)
				{
					chanpos = pri_fixup_principle(pri, chanpos, e->answer.call);
					if (chanpos >= 0) 
					{
						if (pri->debug){
							pri_message(pri->pri, "  owner= %d", pri->pvts[chanpos]->owner);
						}
						/* Now we can do call progress detection */
						pri->pvts[chanpos]->dialing = 0;
						//  �������� ��� ���������� ������� ������� (���������� ����)
						pri_call_on_answer(pri->pvts[chanpos]->span, pri->pvts[chanpos]->channel,pri->pvts[chanpos]->owner);
						if (pri->debug)	pri_message(pri->pri, "pri_call_on_answer owner %d ",pri->pvts[chanpos]->owner );

#ifdef SUPPORT_USERUSER
						if (!pri_strlen_zero(e->answer.useruserinfo)) {
						}
#endif

					}
				}
				break;

			case PRI_EVENT_HANGUP: /* Call got hung up RELEASE */
//SimplePiskPult(1);
//DataSave1[0]=PRI_EVENT_HANGUP;
				chanpos = pri_find_principle(pri, e->hangup.channel);
				if (chanpos >= 0)
				{
					chanpos = pri_fixup_principle(pri, chanpos, e->hangup.call);
					if (chanpos > -1) 
					{
						if (pri->debug){
							pri_message(pri->pri, "  owner= %d", pri->pvts[chanpos]->owner);
						}
						c=0xFFFF;
						pri->pvts[chanpos]->cause = e->hangup.cause;
						if (pri->pvts[chanpos]->owner) {
							/* Queue a BUSY instead of a hangup if our cause is appropriate */
							if (!pri->pvts[chanpos]->outgoing)
							{
								/* The incoming call leg hung up before getting
								 * connected so just hangup the call */
							}
							else{
								if ((!pri->pvts[chanpos]->alreadyhungup) && (!pri->pvts[chanpos]->proceeding)&&(!pri->pvts[chanpos]->setup_ack )&&(e->hangup.cause == PRI_CAUSE_REQUESTED_CHAN_UNAVAIL))
								{
									//� ����� �� ����� ������ ��� �� �������� ������ ����� �� ������� ������
									c=pri_call_setup(pri->pvts[chanpos]->owner, pri->pvts[chanpos]->span, pri->pvts[chanpos]->dialdest,
										pri->pvts[chanpos]->sending_complite,pri->pvts[chanpos]->cid_num, pri->pvts[chanpos]->cid_name);
									if (pri->debug)	pri_message(pri->pri, "pri_call_setup owner %d ",pri->pvts[chanpos]->owner );
									
								}
							}	
							if ((!pri->pvts[chanpos]->alreadyhungup)&&(c==0xFFFF)) {
								//��� �������� �� ��� ��� ������ �������� ?
								//	�������� ��� ������� ����� (�������� ��������� ������� �� ���� � ����� �����)
								if(pri->pvts[chanpos]->owner!=0xFFFF){
									pri_call_on_release(pri->pvts[chanpos]->span, pri->pvts[chanpos]->channel,pri->pvts[chanpos]->owner,pri->pvts[chanpos]->cause);
									if (pri->debug)	pri_message(pri->pri, "pri_call_on_release owner %d ",pri->pvts[chanpos]->owner );
								}
							}
							pri->pvts[chanpos]->owner=0;
						}

						pri_hangup(pri->pri, e->hangup.call, e->hangup.cause);
						if (pri->pvts[chanpos]->call)
						{
							// pri_hangup(pri->pri, pri->pvts[chanpos]->call, e->hangup.cause);
							pri->pvts[chanpos]->call = NULL;
						}

						if (e->hangup.cause == PRI_CAUSE_REQUESTED_CHAN_UNAVAIL) {
							pri_reset(pri->pri, PVT_TO_CHANNEL(pri->pvts[chanpos]));
							pri->pvts[chanpos]->resetting = 1;
						}

#ifdef SUPPORT_USERUSER
						if (pri->pvts[chanpos]->owner && !pri_strlen_zero(e->hangup.useruserinfo)) {
						}
#endif

					}
					else pri_hangup(pri->pri, e->hangup.call, e->hangup.cause);
				} 
				break;

			case PRI_EVENT_HANGUP_REQ:	/* Requesting the higher layer to hangup (DISCONNECT) */
				chanpos = pri_find_principle(pri, e->hangup.channel);
				if (chanpos >= 0)
				{
					chanpos = pri_fixup_principle(pri, chanpos, e->hangup.call);

					if (chanpos > -1) 
					{
						if (pri->debug){
							pri_message(pri->pri, "  owner= %d", pri->pvts[chanpos]->owner);
						}
						pri->pvts[chanpos]->cause = e->hangup.cause;
						if (pri->pvts[chanpos]->owner) {

							if (!pri->pvts[chanpos]->outgoing) {
								/*
								* The incoming call leg hung up before getting
								* connected so just hangup the call.
								*/
								if((!pri->pvts[chanpos]->proceeding)&&(!pri->pvts[chanpos]->alerting)&&(pri->pvts[chanpos]->owner==0XFFFF))
								{
									pri_hangup(pri->pri, pri->pvts[chanpos]->call, e->hangup.cause);
									pri->pvts[chanpos]->call = NULL;
									pri->pvts[chanpos]->owner= 0;

								}
								else
								{
									// �������� ��� ������� ����� (�������� ��������� ������� �� ���� � ����� �����)
									//���������� ���� ��� �������� ���������� � ����� ���� ������ ������ ��������
									pri_call_on_disconnect(pri->pvts[chanpos]->span, pri->pvts[chanpos]->channel,pri->pvts[chanpos]->owner, pri->inbanddisconnect,pri->pvts[chanpos]->cause);
									if (pri->debug)	pri_message(pri->pri, "pri_call_on_disconnect owner %d ",pri->pvts[chanpos]->owner );
								}

							}
							else
							{
								/*
								*��������� ����� ����� ����� ���� �� ������� ������
								*/
								// �������� ��� ������� ����� (�������� ��������� ������� �� ���� � ����� �����)
								//���������� ���� ��� �������� ���������� � ����� ���� ������ ������ ��������
								pri_call_on_disconnect(pri->pvts[chanpos]->span, pri->pvts[chanpos]->channel,pri->pvts[chanpos]->owner, pri->inbanddisconnect,pri->pvts[chanpos]->cause);
								if (pri->debug)	pri_message(pri->pri, "pri_call_on_disconnect owner %d ",pri->pvts[chanpos]->owner );
							}
						} else {
							pri_hangup(pri->pri, pri->pvts[chanpos]->call, e->hangup.cause);
							pri->pvts[chanpos]->call = NULL;
						}
						if (e->hangup.cause == PRI_CAUSE_REQUESTED_CHAN_UNAVAIL) {
							pri_reset(pri->pri, PVT_TO_CHANNEL(pri->pvts[chanpos]));
							pri->pvts[chanpos]->resetting = 1;
						}

#ifdef SUPPORT_USERUSER
						if (!pri_strlen_zero(e->hangup.useruserinfo)) {
						}
#endif

					}
				} 
				break;

			case PRI_EVENT_HANGUP_ACK: ///RELEASE_COMPLETE/other)
				chanpos = pri_find_principle(pri, e->hangup.channel);
				if (chanpos >= 0)
				{
					chanpos = pri_fixup_principle(pri, chanpos, e->hangup.call);
					if (chanpos > -1) 
					{
						if (pri->debug){
							pri_message(pri->pri, "  owner= %d", pri->pvts[chanpos]->owner);
						}
						pri->pvts[chanpos]->call = NULL;
						pri->pvts[chanpos]->resetting = 0;
						if (pri->pvts[chanpos]->owner) 
						{
							if (!pri->pvts[chanpos]->alreadyhungup) {
								//��� �������� �� ��� ��� ������ ��������
								//	�������� ��� ������� ����� (�������� ��������� ������� �� ���� � ����� �����)
								pri_call_on_release(pri->pvts[chanpos]->span, pri->pvts[chanpos]->channel,pri->pvts[chanpos]->owner,pri->pvts[chanpos]->cause);
								if (pri->debug)	pri_message(pri->pri, "pri_call_on_release owner %d ",pri->pvts[chanpos]->owner );
							}
							pri->pvts[chanpos]->owner=0;
						}

#ifdef SUPPORT_USERUSER
						if (!pri_strlen_zero(e->hangup.useruserinfo)) {
						}
#endif
					}
				}
				break;

			case PRI_EVENT_CONFIG_ERR:
				SendStr2IpLogger("PRI Error on span %d: %s", pri->span, e->err.err);
				break;

			case PRI_EVENT_RESTART_ACK:
				chanpos = pri_find_principle(pri, e->restartack.channel);
				if (chanpos < 0) 
				{
					/* Sometime switches (e.g. I421 / British Telecom) don't give us the
					channel number, so we have to figure it out...  This must be why
					everybody resets exactly a channel at a time. */
					for (x = 0; x < pri->numchans; x++)
					{
						if (pri->pvts[x] && pri->pvts[x]->resetting)
						{
							chanpos = x;
							if (pri->pvts[chanpos]->owner) 
							{
								// ������� ������ �����
								pri_call_on_release(pri->pvts[chanpos]->span, pri->pvts[chanpos]->channel,pri->pvts[chanpos]->owner,0);
								if (pri->debug)	pri_message(pri->pri, "pri_call_on_release owner %d ",pri->pvts[chanpos]->owner );
								pri->pvts[chanpos]->owner=0;
							}
							pri->pvts[chanpos]->resetting = 0;
							if (pri->resetting) pri_check_restart(pri);
							break;
						}
					}
				} 
				else
				{
					if (pri->pvts[chanpos])
					{
						if (pri->pvts[chanpos]->owner) 
						{
							// ������� ������ �����
							pri_call_on_release(pri->pvts[chanpos]->span, pri->pvts[chanpos]->channel,pri->pvts[chanpos]->owner,0);
							if (pri->debug)	pri_message(pri->pri, "pri_call_on_release owner %d ",pri->pvts[chanpos]->owner );
							pri->pvts[chanpos]->owner=0;
						}
						pri->pvts[chanpos]->resetting = 0;
						if (pri->resetting) pri_check_restart(pri);
					}
				}
				break;

			case PRI_EVENT_SETUP_ACK:
				chanpos = pri_find_principle(pri, e->setup_ack.channel);
				if (chanpos >= 0){
					chanpos = pri_fixup_principle(pri, chanpos, e->setup_ack.call);
					if (chanpos > -1) {
						pri->pvts[chanpos]->setup_ack = 1;
						/* Send any queued digits */
						ukz = pri_skip_nonblanks(pri->pvts[chanpos]->dialdest);

						for (x = 0;x < (int)strlen(pri->pvts[chanpos]->dialdest_overlap); x++)
						{
							SendStr2IpLogger("Sending pending digit '%c'", pri->pvts[chanpos]->dialdest[x]);
							pri_information(pri->pri, pri->pvts[chanpos]->call, pri->pvts[chanpos]->dialdest_overlap[x]);
							pri->pvts[chanpos]->dialdest[ukz]= pri->pvts[chanpos]->dialdest_overlap[x];
							ukz++;
						}
					}
				}
				break;

// not checked
			case PRI_EVENT_NOTIFY:/* Notification received (NOTIFY) */
				chanpos = pri_find_principle(pri, e->notify.channel);
				if (chanpos >= 0)
				{
					switch (e->notify.info) 
					{
			          case PRI_NOTIFY_REMOTE_HOLD:
				        //insert func
				        break;
			          case PRI_NOTIFY_REMOTE_RETRIEVAL:
				        //insert func
				        break;
					}
				}
				break;
			default:
/*SimplePiskPult(2);
DataSave1[0]++;
DataSave1[1]=e->e;*/
				SendStr2IpLogger("Event: %d", e->e);
				break;
		}  //switch
	} //if (e)
}

///////////////////////////////////////////////////////////////////////////////
void pri_all_dchannel(void *pvParameters)
{
  int i,res,ipri,chan;
  unsigned int ures;
  struct pri_trunk *pri;
  time_t t;
  static struct timeval tv,lowest,*next,lastidle = { 0, 0 };
  static struct hdlc_chan *hdlc_chans[NUM_TRUNKS];
  static char inrawbuf[1024];

  PRI_IS_WORKING=1;
  for(i=0;i<NUM_TRUNKS;i++) hdlc_chans[i]=NULL;
  gettimeofday(&lastidle, NULL);
  while(1) 
  {
    lowest = pri_tvmake(60, 0); // Start with reasonable max 
    for (ipri = 0; ipri < NUM_TRUNKS; ipri++)
    {
        pri = &pri_trunks[ipri];
        PORT_CRITICAL_SECTION_ENTER(&pri->lock);
        hdlc_chans[ipri] = pri->hdlc_chan;
        time(&t);
        // ������� �������������� ������� ������ 3600 ������!!! ����� ������ ���� pri->resetinterval==0 �� ������������
        if((pri->switchtype!=PRI_SWITCH_GR303_TMC) && (pri->resetinterval > 0))
        {
            if(pri->resetting && pri_is_up(pri))
            {
                if(pri->resetpos<0) pri_check_restart(pri);
            }
            else
            {
                if(!pri->resetting && (t-pri->lastreset >= pri->resetinterval))
                {
                  pri->resetting = 1;
                  pri->resetpos = -1;
                }
            }
        }
        for (i = 0; i < NUM_DCHANS; i++)
        { // Find lowest available d-channel
            if (!pri->dchannels[i]) break;
            if ((next = pri_schedule_next(pri->dchans[i])))
            { // We need relative time here
                tv = pri_tvsub(*next, pri_tvnow());
                if (tv.tv_sec < 0) tv = pri_tvmake(0,0);
                if (pri->resetting)
                {
                    if (tv.tv_sec > 1) tv = pri_tvmake(1, 0);
                }
                else
                {
                    if (tv.tv_sec > 60) tv = pri_tvmake(60, 0);
                }
            }
            else
              if (pri->resetting)
              {
                // Make sure we stop at least once per second if we're monitoring idle channels
                tv = pri_tvmake(1,0);
              }
              else
              {
                // Don't poll for more than 60 seconds
                tv = pri_tvmake(60, 0);
              }
            if(!i || pri_tvcmp(tv, lowest) < 0) lowest = tv;
        }
        PORT_CRITICAL_SECTION_LEAVE(&pri->lock);
    }
    vTaskSuspend(NULL); // ����� ISDN ��������
    for (ipri = 0; ipri < NUM_TRUNKS; ipri++)
    {
      pri=pri_trunks+ipri;
      ures=soft_hdlc_chan_wait(pri->hdlc_chan,10);
      res=(short int)(ures & 0xFFFF);
      if((short int)(ures >> 16)==0xffff) continue;
      PORT_CRITICAL_SECTION_ENTER(&pri->lock);
      pri_handle_events(pri, inrawbuf, res);
      PORT_CRITICAL_SECTION_LEAVE(&pri->lock);
    } // for_each(pri in pri_tranks)
    SetWDState(d_WDSTATE_ISDN);
  } //for(;;)
}

///////////////////////////////////////////////////////////////////////////////
// PRIVATE FUNCTIONS IMPLEMENTATION
// ������ ������ �� hdlc �����������
static int pri_read(struct pri *pri, void *buf, int buflen)
{

    struct hdlc_chan * hc = (struct hdlc_chan *)pri_get_userdata(pri);
    int res;

    res = soft_hdlc_chan_read(hc, (unsigned char *)buf, buflen);
    if(res < 0)
    {
      if (res != HDLC_E_AGAIN) pri_message(pri, "pri_read on %d failed: %d", hc->id, res);
      return 0;
    }
    return res;
}

// ������ ������ � hdlc ����������
static int pri_write(struct pri *pri, void *buf, int buflen)
{

    struct hdlc_chan *hc=(struct hdlc_chan *)pri_get_userdata(pri);
    int res;

    res = soft_hdlc_chan_write(hc, (unsigned char *)buf, buflen);
    if(res < 0)
    {
      if(res!=HDLC_E_AGAIN) pri_message(pri, "pri_write to %d failed: %d", hc->id, res);
      return 0;
    }
    return res;
}

static void __pri_message(struct pri *pri, char *msg)
{ // ����� ���������� ���������
#ifdef __ISDN_DEBUG__
  pri_dbg_udp_message(msg);
#endif
}

#endif

