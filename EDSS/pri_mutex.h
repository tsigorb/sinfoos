#if !defined (__PRI_MUTEX_H__)
#define __PRI_MUTEX_H__

#include "pri_port.h"

#if defined(__FREERTOS_USED__)
    #include "../FreeRTOS/FreeRTOS.h" 
    #include "../FreeRTOS/task.h"
    #include "libpri.h"
    #include "pri_internal.h"





    #define PORT_MUTEX                          xSemaphoreHandle
    #define PORT_MUTEX_INIT(lock)               vSemaphoreCreateBinary(*(lock))
    #define PORT_MUTEX_DELETE(lock)             
    //#define PORT_MUTEX_LOCK(lock)               while (! xSemaphoreTake(*(lock), portMAX_DELAY));
    //#define PORT_MUTEX_LOCK(lock)               {if (!xSemaphoreTake(*(lock), 5000)) {pri_message(NULL,"!!!!!!!!!!!!!");}}
    #define PORT_MUTEX_LOCK(lock)               xSemaphoreTake(*(lock))
    #define PORT_MUTEX_UNLOCK(lock)             xSemaphoreGive(*(lock))


#if 0        
    void pri_dbg_udp_message(char *msg);
    extern volatile unsigned portBASE_TYPE uxSchedulerSuspended;
    typedef struct _tskTCB tskTCB;
    extern volatile tskTCB * volatile pxCurrentTCB;	
    extern volatile int lockCount;
    
static void DbgMutexLock(int *lock)
{
    
    vTaskSuspendAll();
    //portENTER_CRITICAL();
    lockCount++;
    pri_message(NULL," +++++++++++ Lock   %d c:%X", lockCount, pxCurrentTCB);
    
}    
        
static void DbgMutexUnlock()
{
    lockCount--;
    pri_message(NULL," ----------- Unlock %d c:%X", lockCount, pxCurrentTCB);    
    xTaskResumeAll();
    //portEXIT_CRITICAL();
}    
    
#endif    

    
    //#define PORT_MUTEX                          int
    //#define PORT_MUTEX_INIT(lock)               
    //#define PORT_MUTEX_DELETE(lock)             
    //#define PORT_MUTEX_LOCK(lock)               DbgMutexLock(lock)
    //#define PORT_MUTEX_UNLOCK(lock)             DbgMutexUnlock()

    
    
 //   #define PORT_MUTEX                          int
 //   #define PORT_MUTEX_INIT(lock)               
 //   #define PORT_MUTEX_DELETE(lock)             
 //   #define PORT_MUTEX_LOCK(lock)               vTaskSuspendAll()
 //   #define PORT_MUTEX_UNLOCK(lock)             xTaskResumeAll()
    
        
    //#define PORT_MUTEX                          int
    //#define PORT_MUTEX_INIT(lock)               
    //#define PORT_MUTEX_DELETE(lock)             
    //#define PORT_MUTEX_LOCK(lock)               portENTER_CRITICAL()
    //#define PORT_MUTEX_UNLOCK(lock)             portEXIT_CRITICAL()
    
 

#else // WIN
    #include <windows.h>

    void DbgInitializeCriticalSection(LPCRITICAL_SECTION lock);
    void DbgEnterCriticalSection(LPCRITICAL_SECTION lock);
    void DbgLeaveCriticalSection(LPCRITICAL_SECTION lock);

    #define PORT_MUTEX                          CRITICAL_SECTION
    #define PORT_MUTEX_INIT(lock)               DbgInitializeCriticalSection(lock)
    #define PORT_MUTEX_DELETE(lock)             DeleteCriticalSection(lock)
    #define PORT_MUTEX_LOCK(lock)               DbgEnterCriticalSection(lock)
    #define PORT_MUTEX_UNLOCK(lock)             DbgLeaveCriticalSection(lock)


//static void DbgInitSemaphore(HANDLE *s)
//{
//    *(s) = CreateSemaphore(NULL, 1, 1, NULL);
//}
//
//static void DbgLockSemaphore(HANDLE *s)
//{
//    WaitForSingleObject(*(s), INFINITE);
//}
//static void DbgUnlockSemaphore(HANDLE *s)
//{
//    ReleaseSemaphore(*(s), 1, NULL);
//}
//
//
//#define PORT_MUTEX                      HANDLE
//#define PORT_MUTEX_INIT(s)              DbgInitSemaphore(s)
//#define PORT_MUTEX_DELETE(s)            CloseHandle(*(s))             
//#define PORT_MUTEX_LOCK(s)              DbgLockSemaphore(s)
//#define PORT_MUTEX_UNLOCK(s)            DbgUnlockSemaphore(s)


#endif // defined(__FREERTOS_USED__)

#endif /* !defined(__PRI_MUTEX_H__) */
