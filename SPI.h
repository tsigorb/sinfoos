void EnableWrite2ProgrammArea(bool bEn);

// 					InitSPI													//
// ��������� ������������� ����������� SPI ��� �������������� � Flash-�������
void InitSPI(void);

// 	RdFlash	- ������ ������ 		    									//
// �� ��������� �������� Flash-������ AT25256
unsigned short  RdFlash( unsigned short AdrFl);

// 	WrFlash	- ������ ������ 						//
// � ��������� �������� Flash-������ AT25256
bool WrFlash(unsigned short DanFl,unsigned short AdrFl);

// ������� �������� ������� Flash ������ AT45DB161
// �������: TRUE - ���� �����, FALSE -���� ������
// ���������: unsigned int Addr - ����� ������, ����������� �� ������� �������� 528 (512) ����
//			 unsigned int How - ����� ��������� ������ � ������ ������������� �� ������� 528 (512) ����
bool WrDeviceErase(unsigned int Addr,unsigned int How);

// ������ ������� ������ �� Flash  AT45DB161
// �������: TRUE - ���� �����, FALSE -���� ������
// ���������: unsigned int Addr - ����� ������, ����������� �� ������� 2 ����
//			unsigned short *lpInp - ��������� �� ������ ������� ������
//			unsigned int HowInp - ���������� ������������ ����
bool WrDeviceArray(unsigned int Addr, unsigned short *lpInp,unsigned int HowInp);

// ������ ����� �� Flash ������ AT45DB161
// �������: TRUE - ���� �����, FALSE -���� ������
// ���������: unsigned int Addr - ����� ������, ����������� �� ������� 2 ����
//			unsigned short bInp - �������(������������) �����
bool WrDeviceShort(unsigned int Addr, unsigned short bInp);

// ������ *lpHowOut ���� �� Flash �� ���������� ������ ��  AT45DB161
// �������: ������ TRUE
// ���������: unsigned short *lpInpAddr - ����� ������. ������ ���� �������� �� ������� 2 ������
//			unsigned short *lpOut - ����� ��������� ������(�� ����� 2 ����)
// 			unsigned int *lpHowOut ���������� ���� ��� ������
bool RdFlashP(const unsigned short *lpInpAddr, unsigned short *lpOut,unsigned int *lpHowOut);

bool WithFlashMemCmp(const unsigned short *lpInp, const unsigned short *lpFlashInp,unsigned int Len );

